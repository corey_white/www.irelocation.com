<?

#   current_campaign = directlead_id
#   current_campaign_type = new,past3,old

#   limit = 30,60,90,120,150
#   page = 1,2,3...

# listings per page
if(!isset($limit))
{
  $limit=30;
}

# category was selected from dropdown
if(isset($category))
{
  list($current_cat,$current_cat_type)=explode(",",$category);
}
else
{
  $current_cat="1";
  $current_cat_type="past3";
  $category="$current_cat,$current_cat_type";
}

if(!isset($bad_lead_filter))
{
  $bad_lead_filter="off";
}

# get all categories

$rs_cats=new mysql_recordset("select * from directleadscategory");
while($rs_cats->fetch_array())
{
  $category_name[]=$rs_cats->myarray["name"]."";
  $category_id[]=$rs_cats->myarray["cat_id"].",past3";
  $category_name[]=$rs_cats->myarray["name"]." - Archived";
  $category_id[]=$rs_cats->myarray["cat_id"].",old";
}

$rs_cats=nothing;

# starting point for leads
if(isset($page))
{
  $start = ($page-1) * $limit;
}
else
{
  $start=0;
}

# create sql statement to display leads
if($order==''){$order="received desc";}

if($current_cat_type=='old')
{
  $current_db="quotes_old";
  $main_sql="select * from quotes_old where cat_id='".$current_cat."'";
}
else
{
  $current_db="quotes";
  $main_sql="select * from quotes where cat_id='".$current_cat."'";
}

if($received!=''){$sql.=" and received like '$received%'";}
if($name!=''){$sql.=" and lower(name) like '%".strtolower($name)."%'";}
if($email!=''){$sql.=" and lower(email) like '%".strtolower($email)."%'";}
if($phone_home!=''){$sql.=" and phone_home like '%$phone_home%'";}
if($contact!=''){$sql.=" and lower(contact) like '%".strtolower($contact)."%'";}
if($est_move_date!=''){$sql.=" and est_move_date like '%$est_move_date%'";}
if($origin_city!=''){$sql.=" and lower(origin_city) like '%".strtolower($origin_city)."%'";}
if($origin_state!=''){$sql.=" and lower(origin_state) like '%".strtolower($origin_state)."%'";}
if($origin_zip!=''){$sql.=" and lower(origin_zip) like '%".strtolower($origin_zip)."%'";}
if($destination_city!=''){$sql.=" and lower(destination_city) like '%".strtolower($destination_city)."%'";}
if($destination_state!=''){$sql.=" and lower(destination_state) like '%".strtolower($destination_state)."%'";}
if($source!=''){$sql.=" and lower(source) like '%".strtolower($source)."%'";}
if($keyword!=''){$sql.=" and lower(keyword) like '%".strtolower($keyword)."%'";}

# Bad lead filter...
if($bad_lead_filter=='duplicate')
{
  $rs=new mysql_recordset("select count(name) as mycount,name from $current_db where cat_id='$current_cat' group by name order by mycount desc");
  while($rs->fetch_array())
  {
    if($rs->myarray["mycount"]>1)
    {
      $temp_sql.="name='".$rs->myarray["name"]."' or ";
    }
    else
    {
      break;
    }
  }
  $temp_sql=substr($temp_sql,0,-4);
  if($temp_sql!='')
  {
    $sql.=" and ($temp_sql)";
  }
  else
  {
    $sql.=" and 1=2";
  }
}
if($bad_lead_filter=='potential')
{
  $sql.=" and (origin_state='' or origin_city='' or destination_state='' or destination_city='' or origin_zip='' or name='' or email='' or phone_home='')";
}

$rs_leads=new mysql_recordset($main_sql.$sql);
$row_count=$rs_leads->rowcount();

$pages=intval(($row_count/$limit) + 0.99);

$sql.=" order by $order limit $start,$limit";

$rs_leads=new mysql_recordset($main_sql.$sql);
$row_count2=$rs_leads->rowcount();

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Manage Leads</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script language=JavaScript>
var checkboxName = 'checkbox'
var emailName = 'emailto'
var isCheck = true;

function checkall(form) {
  for (var i = 1; true; i++){
    if(form.elements[checkboxName+i] == null)
      break;
    form.elements[checkboxName+i].checked = isCheck;
  }
  isCheck = !isCheck;
}

function open_action_win(url) {
  window.open(url,"moving_quotes",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=680,height=500');
}

function orderby(form,myorder)
{
  var hiddenName = 'order'
  form.elements[hiddenName].value = myorder
  form.submit()
}

function popup(form, page) {
  var url = page
  var somethingChecked = false
  var nothingMsg = 'No listings selected.'
  for (var i = 1; true; i++) {
    if(form.elements[checkboxName+i] == null){
      break;
    }
    if (form.elements[checkboxName+i].checked == true) {
      if (somethingChecked) {
        url += "," + form.elements[checkboxName+i].value
      }
      else {
        url += "?checkedleads=" + form.elements[checkboxName+i].value
        somethingChecked = true
      }
    }
  }
  if (somethingChecked) {
    //alert(url)
    url += "&emailto=" + form.elements[emailName].value + "&campaign=<?echo $current_campaign;?>"
    open_action_win(url)
  }
  else {
    alert(nothingMsg)
  }
}
</script>
</head>

<body>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"></script>
<p><font face="verdana" color="#2360A5" size="3"><b>
    Manage Leads</b></font></p>
<form name="quoteform" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_quotes">
<table border="0" cellspacing="3" width="100%" cellpadding="3">
  <tr>
    <td width="80%"><font face="verdana" color="#333333" size="1">View
        Lead Category:<br>
        <select onchange="submit();" name="category" class="quote_box">
      <?
        $i=0;
        while($category_name[$i]!='')
        {
          if($category_id[$i]==$category)
          {
            echo "<option value=\"".$category_id[$i]."\" selected>".$category_name[$i]."</option>\n";
          }
          else
          {
            echo "<option value=\"".$category_id[$i]."\">".$category_name[$i]."</option>\n";
          }
          $i++;
        }
      ?>
        </select>&nbsp;&nbsp;&nbsp;&nbsp;
        <span class="notice">&nbsp;BAD LEAD FILTER:
        <input type="radio" name="bad_lead_filter" onClick="quoteform.submit(); return true;" value="off"<?if($bad_lead_filter=='off'){echo " checked";}?>> off
        <input type="radio" name="bad_lead_filter" onClick="quoteform.submit(); return true;" value="duplicate"<?if($bad_lead_filter=='duplicate'){echo " checked";}?>> show duplicates
        <input type="radio" name="bad_lead_filter" onClick="quoteform.submit(); return true;" value="potential"<?if($bad_lead_filter=='potential'){echo " checked";}?>> show potential bad leads </span>
        </td>
    <td width="20%">
    <p align="right"><font face="verdana" size="1" color="#000000"><b><?echo $row_count2;?> of <?echo $row_count;?> leads shown</b></font></td>
  </tr>
</table>
<p></p>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="14">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="50%" valign="top">
            <img src="images/arrow_top.gif" width="25" height="16"><a href="1md_quotes_view.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bview.gif" width="36" height="15"></a><a href="#" onClick="quoteform.submit(); return true;"><img border="0" src="images/bupdate.gif" width="85" height="15"></a><a href="1md_quotes_delete.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bdelete.gif"></a><a href="1md_quotes_email.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bemail.gif" width="106" height="15"></a>
        <input type="text" size="30" name="emailto" value="<?echo $user["email"];?>" class="quote_box"></td>
            <td width="50%">
            <p align="right"><font face="verdana" size="1">Listings
            Per Page&nbsp;:
            <select onchange="submit();" name="limit" class="input_red">
            <?
            for($i=30;$i<=150;$i+=30)
            {
              if($i==$limit)
              {
                echo "<option value=\"$i\" selected>$i</option>";
              }
              else
              {
                echo "<option value=\"$i\">$i</option>";
              }
            }
            ?>
            </select></font></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="1%" bgcolor="#A9CAEB" rowspan="2" valign="bottom">
        <p align="center">
        <a onclick="checkall(document.quoteform); return false;" href="#">
        <img alt src="images/checkall.gif" border="0" width="16" height="24">
        </a></td>
        <td width="8%" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='received asc'){echo "received desc";}else{echo "received asc";}?>"); return false;'>
        <font color="#000000">received</font></a></b></font></td>
        <td width="11%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='name asc'){echo "name desc";}else{echo "name asc";}?>"); return false;'>
        <font color="#000000">name</font></a></b></font></td>
        <td width="13%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='email asc'){echo "email desc";}else{echo "email asc";}?>"); return false;'>
        <font color="#000000">email</font></a></b></font></td>
        <td width="9%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='phone_home asc'){echo "phone_home desc";}else{echo "phone_home asc";}?>"); return false;'>
        <font color="#000000">phone</font></a></b></font></td>
        <td width="7%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='est_move_date asc'){echo "est_move_date desc";}else{echo "est_move_date asc";}?>"); return false;'>
        <font color="#000000">move date</font></a></b></font></td>
        <td width="9%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='origin_city asc'){echo "origin_city desc";}else{echo "origin_city asc";}?>"); return false;'>
        <font color="#000000">origin city</font></a></b></font></td>
        <td width="6%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='origin_state asc'){echo "origin_state desc";}else{echo "origin_state asc";}?>"); return false;'>
        <font color="#000000">origin state</font></a></b></font></td>
        <td width="8%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='origin_zip asc'){echo "origin_zip desc";}else{echo "origin_zip asc";}?>"); return false;'>
        <font color="#000000">origin zip</font></a></b></font></td>
        <td width="10%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='destination_city asc'){echo "destination_city desc";}else{echo "destination_city asc";}?>"); return false;'>
        <font color="#000000">dest city</font></a></b></font></td>
        <td width="6%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='destination_state asc'){echo "destination_state desc";}else{echo "destination_state asc";}?>"); return false;'>
        <font color="#000000">dest state</font></a></b></font></td>
        <td width="6%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='source asc'){echo "source desc";}else{echo "source asc";}?>"); return false;'>
        <font color="#000000">source</font></a></b></font></td>
        <td width="7%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby(document.quoteform,"<?if($order=='keyword asc'){echo "keyword desc";}else{echo "keyword asc";}?>"); return false;'>
        <font color="#000000">keyword</font></a></b></font></td>
      </tr>
      <tr>
        <td width="8%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="12" name="received" value="<?echo $received;?>" class="quote_box"></td>
        <td width="11%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="15" name="name" value="<?echo $name;?>" class="quote_box"></td>
        <td width="13%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="20" name="email" value="<?echo $email;?>" class="quote_box"></td>
        <td width="9%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="12" name="phone_home" value="<?echo $phone_home;?>" class="quote_box"></td>
        <td width="7%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="11" name="est_move_date" value="<?echo $est_move_date;?>" class="quote_box"></td>
        <td width="9%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="13" name="origin_city" value="<?echo $origin_city;?>" class="quote_box"></td>
        <td width="6%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="3" name="origin_state" value="<?echo $origin_state;?>" class="quote_box"></td>
        <td width="8%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="8" name="origin_zip" value="<?echo $origin_zip;?>" class="quote_box"></td>
        <td width="10%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="13" name="destination_city" value="<?echo $destination_city;?>" class="quote_box"></td>
        <td width="6%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="3" name="destination_state" value="<?echo $destination_state;?>" class="quote_box"></td>
        <td width="6%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="12" name="source" value="<?echo $source;?>" class="quote_box"></td>
        <td width="7%" bgcolor="#E1ECFA" align="center">
        <input type="text" size="12" name="keyword" value="<?echo $keyword;?>" class="quote_box"></td>
      </tr>
      <?
      $i=0;
      while($rs_leads->fetch_array())
      {
       $i++;
      ?>
      <tr>
        <td width="1%" bgcolor="#A9CAEB"><font size="1" face="Arial">
<?
/*onmouseover="return overlib('<?echo str_replace('\r','',str_replace('\n','',str_replace('\n','',$rs_leads->myarray["comments"])));?>', STICKY, RIGHT);" onmouseout="nd();"
*/
?>
        <input type="checkbox" value="<?echo $rs_leads->myarray["quote_id"];?>" name="checkbox<?echo $i;?>"></font></td>
        <td width="8%" class="quote_col_s"><?echo $rs_leads->myarray["received"];?></td>
        <td width="11%" class="quote_col_s">&nbsp;<?if(trim($rs_leads->myarray["comments"])==''){echo "<font color='#FF0000'><b>x</b></font>";} echo $rs_leads->myarray["name"];?></td>
        <td width="13%" class="quote_col_s">&nbsp;<?echo $rs_leads->myarray["email"];?></td>
        <td width="9%" class="quote_col_s">&nbsp;<?echo $rs_leads->myarray["phone_home"];?></td>
        <td width="7%" class="quote_col_s"><?echo $rs_leads->myarray["est_move_date"];?></td>
        <td width="9%" class="quote_col_o">&nbsp;<?echo $rs_leads->myarray["origin_city"];?></td>
        <td width="6%" class="quote_col_o"><?echo $rs_leads->myarray["origin_state"];?></td>
        <td width="8%" class="quote_col_o"><?echo $rs_leads->myarray["origin_zip"];?></td>
        <td width="10%" class="quote_col_d">&nbsp;<?echo $rs_leads->myarray["destination_city"];?></td>
        <td width="6%" class="quote_col_d"><?echo $rs_leads->myarray["destination_state"];?></td>
        <td width="6%" class="quote_col_x">&nbsp;<?echo $rs_leads->myarray["source"];?></td>
        <td width="7%" class="quote_col_x" <?if($rs_leads->myarray["keyword"]!=''){?>onclick="return overlib('<?echo $rs_leads->myarray["keyword"];?>', STICKY, LEFT);" onmouseout="nd();"<?}?>><?echo substr($rs_leads->myarray["keyword"],0,20);?></td>
      </tr>
      <?
       if((($i%15)==0) && $i!=$row_count2)
       {
      ?>
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="14">
            <img src="images/arrow_middle.gif" width="25" height="17"><a href="quote_view.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bview.gif" width="36" height="15"></a><a href="#" onClick="quoteform.submit(); return true;"><img border="0" src="images/bupdate.gif" width="85" height="15"></a><a href="1md_quotes_delete.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
      </tr>
      <?
       }
      }
      ?>
      <tr>
        <td width="100%" colspan="14" bgcolor="#A9CAEB">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="50%">
            <img src="images/arrow_bottom.gif" width="25" height="16"><a href="1md_quotes_view.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bview.gif" width="36" height="15"></a><a href="#" onClick="quoteform.submit(); return true;"><img border="0" src="images/bupdate.gif" width="85" height="15"></a><a href="1md_quotes_delete.php" onClick="popup(document.quoteform,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
            <td width="50%">
<input type="hidden" name="order" value="">
</form>
<form name="nextPage" method="post" action="control.php">
<input type="hidden" name="action" value="1md_quotes">
            <p align="right"><font face="verdana" size="1">Page&nbsp;:
            <select onchange="submit();" name="page" class="input_red">
            <?
            for($p=1;$p<=$pages;$p++)
            {
              if($p==$page)
              {
                echo"<option value=\"$p\" selected>$p</option>\n";
              }
              else
              {
                echo"<option value=\"$p\">$p</option>\n";
              }
            }
            ?>
            </select></font></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
 <input type="hidden" name="category" value="<?echo $category;?>">
 <input type="hidden" name="limit" value="<?echo $limit;?>">
 <input type="hidden" name="emailto" value="<?echo $emailto;?>">
 <input type="hidden" name="order" value="<?echo $order;?>">
 <input type="hidden" name="received" value="<?echo $received;?>">
 <input type="hidden" name="name" value="<?echo $name;?>">
 <input type="hidden" name="email" value="<?echo $email;?>">
 <input type="hidden" name="phone_home" value="<?echo $phone_home;?>">
 <input type="hidden" name="contact" value="<?echo $contact;?>">
 <input type="hidden" name="est_move_date" value="<?echo $est_move_date;?>">
 <input type="hidden" name="origin_city" value="<?echo $origin_city;?>">
 <input type="hidden" name="origin_state" value="<?echo $origin_state;?>">
 <input type="hidden" name="origin_zip" value="<?echo $origin_zip;?>">
 <input type="hidden" name="destination_city" value="<?echo $destination_city;?>">
 <input type="hidden" name="destination_state" value="<?echo $destination_state;?>">
 <input type="hidden" name="source" value="<?echo $source;?>">
 <input type="hidden" name="keyword" value="<?echo $keyword;?>">
 <input type="hidden" name="bad_lead_filter" value="<?echo $bad_lead_filter;?>">
</form>
</table>
</body>

</html>