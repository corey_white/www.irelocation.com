<?php

	/*
	 SUPER CLASS FOR CREATIVES
	*/

	class APIlityCreative {
		// class attributes
	 	var $id;
		var $belongsToAdGroupId;
		var $isDeleted;
		var $isDisapproved;
		var $displayUrl;
		var $destinationUrl;

		// constructor
		function APIlityCreative ($id,	$belongsToAdGroupId, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved) {
			$this->id = $id;
			$this->belongsToAdGroupId = $belongsToAdGroupId;
			$this->displayUrl = $displayUrl;
			$this->destinationUrl = $destinationUrl;
			$this->isDeleted = convertBool($isDeleted);
			$this->isDisapproved = convertBool($isDisapproved);
		}

		// get functions
		function getId() {
			return $this->id;
		}

		function getBelongsToAdGroupId() {
			return $this->belongsToAdGroupId;
		}

		function getDestinationUrl() {
			return $this->destinationUrl ;
		}

		function getDisplayUrl() {
			return $this->displayUrl;
		}

		function getIsDeleted() {
			// return boolean type-casted to integer for making it readable
			return (integer) $this->isDeleted;
		}

		function getIsDisapproved() {
			 // return boolean type-casted to integer for making it readable
			 return (integer) $this->isDisapproved;
		}

		function getCreativeStats($startDate, $endDate, $inPST = false) {
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			if ($inPST) $inPST = "true"; else $inPST = "false";
			$soapParameters = "<getCreativeStats>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeIds>".$this->getId()."</creativeIds>
														<startDay>".$startDate."</startDay>
														<endDay>".$endDate."</endDay>
														<inPST>".$inPST."</inPST>
												 </getCreativeStats>";
			// query the google servers
			$creativeStats = $someSoapClient->call("getCreativeStats", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCreativeStats()", $soapParameters);
		    return false;
			}
			$creativeStats['getCreativeStatsReturn']['cost'] = $creativeStats['getCreativeStatsReturn']['cost'] / EXCHANGE_RATE;
			return $creativeStats['getCreativeStatsReturn'];
		}
	}

	/*
	 TEXT CREATIVES
	*/

	class APIlityTextCreative extends APIlityCreative {
		// class attributes
		var $headline;
		var $description1;
		var $description2;

		// constructor
		function APIlityTextCreative ($id,	$belongsToAdGroupId, $headline,	$description1, $description2, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved) {
			// we need to construct the superclass first, this is php-specific object-oriented behaviour
			APIlityCreative::APIlityCreative($id,	$belongsToAdGroupId, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
			// now construct the text creative which inherits all other creative attributes
			$this->headline = $headline;
			$this->description1 = $description1;
			$this->description2 = $description2;
		}

		// XML output
		function toXml() {
			if ($this->getIsDeleted()) $isDeleted = "true"; else $isDeleted = "false";
			if ($this->getIsDisapproved()) $isDisapproved = "true"; else $isDisapproved = "false";
			$xml = "<TextCreative>
	<id>".$this->getId()."</id>
	<belongsToAdGroupId>".$this->getBelongsToAdGroupId()."</belongsToAdGroupId>
	<headline>".$this->getHeadline()."</headline>
	<description1>".$this->getDescription1()."</description1>
	<description2>".$this->getDescription2()."</description2>
	<displayUrl>".$this->getDisplayUrl()."</displayUrl>
	<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
	<isDeleted>".$isDeleted."</isDeleted>
	<isDisapproved>".$isDisapproved."</isDisapproved>
</TextCreative>";
			return $xml;
		}

		// get functions
		function getHeadline() {
			return $this->headline;
		}

		function getDescription1() {
			return $this->description1;
		}

		function getDescription2() {
			return $this->description2;
		}

		function getDisplayUrl() {
			return $this->displayUrl;
		}

		function getDestinationUrl() {
			return $this->destinationUrl;
		}

		// report function
		function getCreativeData() {
			$creativeData = array(
												'id'=>$this->getId(),
												'belongsToAdGroupId'=>$this->getBelongsToAdGroupId(),
												'headline'=>$this->getHeadline(),
												'description1'=>$this->getDescription1(),
												'description2'=>$this->getDescription2(),
												'displayUrl'=>$this->getDisplayUrl(),
												'destinationUrl'=>$this->getDestinationUrl(),
												'isDeleted'=>$this->getIsDeleted(),
												'isDisapproved'=>$this->getIsDisapproved()
											);
			return $creativeData;
		}

		// set functions
		function setHeadline ($newHeadline) {
			// setting the headline is not provided by the api so emulating this by deleting and then re-creating the creative
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			// then recreate it with the new headline set
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<headline>".$newHeadline."</headline>
															<description1>".$this->getDescription1()."</description1>
															<description2>".$this->getDescription2()."</description2>
															<displayUrl>".$this->getDisplayUrl()."</displayUrl>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<deleted>".$this->getIsDeleted()."</deleted>
														</creative>
													</addCreative>";
			// add the creative to the google servers
			$someCreative = $someSoapClient->call("addCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setHeadline()", $soapParameters);
		    return false;
			}
			// first delete the current creative
			$soapParameters = "<deleteCreative>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeId>".$this->getId()."</creativeId>
												 </deleteCreative>";
			// delete the creative on the google servers
			$someSoapClient->call("deleteCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setHeadline()", $soapParameters);
		    return false;
			}
			// update local object
			$this->headline = $newHeadline;
			// changing the headline of a creative will change its id so update local object
			$this->id = $someCreative['addCreativeReturn']['id'];
			return true;
		}

		function setDescription1 ($newDescription1) {
			// update the google servers
			// setting the description1 is not provided by the api so emulating this by deleting and then re-creating the creative
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			// then recreate it with the new description1 set
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<headline>".$this->getHeadline()."</headline>
															<description1>".$newDescription1."</description1>
															<description2>".$this->getDescription2()."</description2>
															<displayUrl>".$this->getDisplayUrl()."</displayUrl>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<deleted>".$this->getIsDeleted()."</deleted>
														</creative>
													</addCreative>";
			// add the creative to the google servers
			$someCreative = $someSoapClient->call("addCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDescription1()", $soapParameters);
		    return false;
			}
			// first delete the current creative
			$soapParameters = "<deleteCreative>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeId>".$this->getId()."</creativeId>
												 </deleteCreative>";
			// remove the creative from the google servers
			$someSoapClient->call("deleteCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDescription1()", $soapParameters);
		    return false;
			}
			// update local object
			$this->description1 = $newDescription1;
			// changing the description1 of a creative will change its id so update local object
			$this->id = $someCreative['addCreativeReturn']['id'];
			return true;
		}

		function setDescription2 ($newDescription2) {
			// setting the description2 is not provided by the api so emulating this by deleting and then re-creating the creative
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			// then recreate it with the new description2 set
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<headline>".$this->getHeadline()."</headline>
															<description1>".$this->getDescription1()."</description1>
															<description2>".$newDescription2."</description2>
															<displayUrl>".$this->getDisplayUrl()."</displayUrl>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<deleted>".$this->getIsDeleted()."</deleted>
														</creative>
													</addCreative>";
			// add the creative to the google servers
			$someCreative = $someSoapClient->call("addCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDescription2()", $soapParameters);
		    return false;
			}
			// first delete the current creative
			$soapParameters = "<deleteCreative>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeId>".$this->getId()."</creativeId>
												 </deleteCreative>";
			// delete the keyword from the google servers
			$someSoapClient->call("deleteCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDescription2()", $soapParameters);
		    return false;
			}
			// update local object
			$this->description2 = $newDescription2;
			// changing the description2 of a creative will change its id so update local object
			$this->id = $someCreative['addCreativeReturn']['id'];
			return true;
		}

		function setDisplayUrl ($newDisplayUrl) {
			// setting the display url is not provided by the api so emulating this by deleting and then re-creating the creative
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			// then recreate it with the new display url set
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<headline>".$this->getHeadline()."</headline>
															<description1>".$this->getDescription1()."</description1>
															<description2>".$this->getDescription2()."</description2>
															<displayUrl>".$newDisplayUrl."</displayUrl>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<deleted>".$this->getIsDeleted()."</deleted>
														</creative>
													</addCreative>";
			// add the creative to the google servers
			$someCreative = $someSoapClient->call("addCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDisplayUrl()", $soapParameters);
		    return false;
			}
			// first delete the current creative
			$soapParameters = "<deleteCreative>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeId>".$this->getId()."</creativeId>
												 </deleteCreative>";
			// delete the creative from the google servers
			$someSoapClient->call("deleteCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDisplayUrl()", $soapParameters);
		    return false;
			}
			// update local object
			$this->displayUrl = $newDisplayUrl;
			// changing the display url of a creative will change its id so update local object
			$this->id = $someCreative['addCreativeReturn']['id'];
			return true;
		}

		function setDestinationUrl ($newDestinationUrl) {
			// setting the destination url is not provided by the api so emulating this by deleting and then re-creating the creative
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			// then recreate it with the new destination url set
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<headline>".$this->getHeadline()."</headline>
															<description1>".$this->getDescription1()."</description1>
															<description2>".$this->getDescription2()."</description2>
															<displayUrl>".$this->getDisplayUrl()."</displayUrl>
															<destinationUrl>".$newDestinationUrl."</destinationUrl>
															<deleted>".$this->getIsDeleted()."</deleted>
														</creative>
													</addCreative>";
			// add the creative to the google servers
			$someCreative = $someSoapClient->call("addCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDestinationUrl()", $soapParameters);
		    return false;
			}
			// first delete the current creative
			$soapParameters = "<deleteCreative>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<creativeId>".$this->getId()."</creativeId>
												 </deleteCreative>";
			// delete the creative from the google servers
			$someSoapClient->call("deleteCreative", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDestinationUrl()", $soapParameters);
		    return false;
			}
			// update local object
			$this->destinationUrl = $newDestinationUrl;
			// changing the destination url of a creative will change its id so update local object
			$this->id = $someCreative['addCreativeReturn']['id'];
			return true;
		}
	}

	/*
	 IMAGE CREATIVES
	*/

	class APIlityImageCreative extends APIlityCreative {
		// class attributes
	 	var $name;
	 	var $width;
		var $height;
		var $imageUrl;
		var $thumbnailUrl;
		var $mimeType;
		var $type;

		// constructor
		function APIlityImageCreative ($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved) {
			// we need to construct the superclass first, this is php-specific object-oriented behaviour
			APIlityCreative::APIlityCreative($id,	$belongsToAdGroupId, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);

			// now construct the image creative which inherits all other creative attributes
			$this->type = $type;
			$this->name = $name;
			$this->width = $width;
			$this->height = $height;
			$this->imageUrl = $imageUrl;
			$this->thumbnailUrl = $thumbnailUrl;
			$this->mimeType = $mimeType;
		}

		// XML output
		function toXml() {
			if ($this->getIsDeleted()) $isDeleted = "true"; else $isDeleted = "false";
			if ($this->getIsDisapproved()) $isDisapproved = "true"; else $isDisapproved = "false";
			$xml = "<ImageCreative>
	<id>".$this->getId()."</id>
	<belongsToAdGroupId>".$this->getBelongsToAdGroupId()."</belongsToAdGroupId>
	<type>".$this->getType()."</type>
	<name>".$this->getName()."</name>
	<width>".$this->getWidth()."</width>
	<height>".$this->getHeight()."</height>
	<imageUrl>".$this->getImageUrl()."</imageUrl>
	<thumbnailUrl>".$this->getThumbnailUrl()."</thumbnailUrl>
	<mimeType>".$this->getMimeType()."</mimeType>
	<displayUrl>".$this->getDisplayUrl()."</displayUrl>
	<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
	<isDeleted>".$isDeleted."</isDeleted>
	<isDisapproved>".$isDisapproved."</isDisapproved>
</ImageCreative>";
			return utf8_encode($xml);
		}

		// get functions
		function getName() {
			return $this->name;
		}

		function getType() {
			return $this->type;
		}

		function getWidth(){
			return $this->width;
		}

		function getHeight() {
			return $this->height;
		}

		function getImageUrl() {
			return $this->imageUrl;
		}

		function getThumbnailUrl() {
			return $this->thumbnailUrl;
		}

		function getMimeType() {
			return $this->mimeType;
		}

		// report function
		function getCreativeData() {
			$creativeData = array(
												'id' => $this->getId(),
												'belongsToAdGroupId' => $this->getBelongsToAdGroupId(),
												'name' => $this->getName(),
												'width' => $this->getWidth(),
												'height' => $this->getHeight(),
												'imageUrl' => $this->getImageUrl(),
												'thumbnailUrl' => $this->getThumbnailUrl(),
												'mimeType' => $this->getMimeType(),
												'type' => $this->getType(),
												'displayUrl'=>$this->getDisplayUrl(),
												'destinationUrl'=>$this->getDestinationUrl(),
												'isDeleted'=>$this->getIsDeleted(),
												'isDisapproved'=>$this->getIsDisapproved()
											);
			return $creativeData;
		}

		// set functions
		// none, as these functions would require the base64 data for uploading the image ad again after
		// deleting it (emulating changes by first deleting things and then recreating them)
	}

	/*
	  GENERIC CLASS FUNCTIONS FOR BOTH IMAGE AND TEXT CREATIVES
	*/

	function createCreativeObject($givenAdGroupId, $givenCreativeId) {
		// this creates a local creative object
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		// prepare soap parameters
		$soapParameters = "<getCreative>
													<adGroupId>".$givenAdGroupId."</adGroupId>
													<creativeId>".$givenCreativeId."</creativeId>
												</getCreative>";
		// execute soap call
		$someCreative = $someSoapClient->call("getCreative", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createCreativeObject()", $soapParameters);
	    return false;
		}
    // invalid ids are silently ignored. this is not what we want so put out a warning and return without doing anything.
		if (sizeOf($someCreative) == 0) {
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning: </b>Invalid Creative ID. No Creative found.";
			return false;
		}

		// populate class attributes
		$id = $someCreative['getCreativeReturn']['id'];
		$belongsToAdGroupId = $someCreative['getCreativeReturn']['adGroupId'];
		$headline = $someCreative['getCreativeReturn']['headline'];
		$description1 = $someCreative['getCreativeReturn']['description1'];
		$description2 = $someCreative['getCreativeReturn']['description2'];
		$displayUrl = $someCreative['getCreativeReturn']['displayUrl'];
		$destinationUrl = $someCreative['getCreativeReturn']['destinationUrl'];
		$isDeleted = $someCreative['getCreativeReturn']['deleted'];
		$isDisapproved = $someCreative['getCreativeReturn']['disapproved'];
		// these attributes apply just to image creatives, so just assign these attributes if we have an image ad
		if (isset($someCreative['getCreativeReturn']['image'])){
			$name = $someCreative['getCreativeReturn']['image']['name'];
			$width = $someCreative['getCreativeReturn']['image']['width'];
			$height = $someCreative['getCreativeReturn']['image']['height'];
			$imageUrl = $someCreative['getCreativeReturn']['image']['imageUrl'];
			$thumbnailUrl = $someCreative['getCreativeReturn']['image']['thumbnailUrl'];
			$mimeType = $someCreative['getCreativeReturn']['image']['mimeType'];
			$type = $someCreative['getCreativeReturn']['image']['type'];
		}
		// create object
		if (isset($description1)) {
			$creativeObject = new APIlityTextCreative ($id,	$belongsToAdGroupId, $headline,	$description1, $description2, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
		}
		else if (isset($width)) {
			$creativeObject = new APIlityImageCreative($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
		}
		return $creativeObject;
	}

	// add a creative to the server and create local object
	function addCreative() {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		// emulating overloading here because we do not know whether an exemption request is been made or not
		// expected argument sequence: $belongsToAdGroupId, $headline, $description1, $description2, $displayUrl, $destinationUrl, ($exemptionRequest)
		$belongsToAdGroupId = func_get_arg(0);
		$headline = func_get_arg(1);
		$description1 = func_get_arg(2);
		$description2 = func_get_arg(3);
		$displayUrl = func_get_arg(4);
		$destinationUrl = func_get_arg(5);

		// no exemption request
		if (func_num_args() == 6) {
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<headline>".$headline."</headline>
															<description1>".$description1."</description1>
															<description2>".$description2."</description2>
															<displayUrl>".$displayUrl."</displayUrl>
															<destinationUrl>".$destinationUrl."</destinationUrl>
														</creative>
												 </addCreative>";
		}
		// with exemption request
		else if (func_num_args() == 7) {
			$exemptionRequest = func_get_arg(6);
			$soapParameters = "<addCreative>
														<creative>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<headline>".$headline."</headline>
															<description1>".$description1."</description1>
															<description2>".$description2."</description2>
															<displayUrl>".$displayUrl."</displayUrl>
															<destinationUrl>".$destinationUrl."</destinationUrl>
															<exemptionRequest>".$exemptionRequest."</exemptionRequest>
														</creative>
													</addCreative>";
		}
		// add the creative to the google servers
		$someCreative = $someSoapClient->call("addCreative", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addCreative()", $soapParameters);
	    return false;
		}
		// populate object attributes
		$id = $someCreative['addCreativeReturn']['id'];
		$belongsToAdGroupId = $someCreative['addCreativeReturn']['adGroupId'];
		$headline = $someCreative['addCreativeReturn']['headline'];
		$description1 = $someCreative['addCreativeReturn']['description1'];
		$description2 = $someCreative['addCreativeReturn']['description2'];
		$displayUrl = $someCreative['addCreativeReturn']['displayUrl'];
		$destinationUrl = $someCreative['addCreativeReturn']['destinationUrl'];
		$isDeleted = $someCreative['addCreativeReturn']['deleted'];
		$isDisapproved = $someCreative['addCreativeReturn']['disapproved'];
		// create local object
		$creativeObject = new APIlityTextCreative($id,	$belongsToAdGroupId, $headline,	$description1, $description2, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
		return $creativeObject;
	}

	function addCreativeList($creatives) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		$soapParameters = "<addCreativeList>";
		foreach ($creatives as $creative) {
			if (isset($creative['exemptionRequest'])) {
				$soapParameters .= "<creatives>
															<adGroupId>".$creative['belongsToAdGroupId']."</adGroupId>
															<headline>".$creative['headline']."</headline>
															<description1>".$creative['description1']."</description1>
															<description2>".$creative['description2']."</description2>
															<displayUrl>".$creative['displayUrl']."</displayUrl>
															<destinationUrl>".$creative['destinationUrl']."</destinationUrl>
															<exemptionRequest>".$creative['exemptionRequest']."</exemptionRequest>
														</creatives>";
			}
			else {
				$soapParameters .= "<creatives>
															<adGroupId>".$creative['belongsToAdGroupId']."</adGroupId>
															<headline>".$creative['headline']."</headline>
															<description1>".$creative['description1']."</description1>
															<description2>".$creative['description2']."</description2>
															<displayUrl>".$creative['displayUrl']."</displayUrl>
															<destinationUrl>".$creative['destinationUrl']."</destinationUrl>
														</creatives>";
			}
		}
		$soapParameters .= "</addCreativeList>";
		// add the creatives to the google servers
		$someCreatives = $someSoapClient->call("addCreativeList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addCreativeList()", $soapParameters);
	    return false;
		}

		// when we have only one creative return a (one creative element) array  anyway
		if (isset($someCreatives['addCreativeListReturn']['id'])) {
			$saveArray = $someCreatives['addCreativeListReturn'];
			unset($someCreatives);
			$someCreatives['addCreativeListReturn'][0] = $saveArray;
		}
		// create local objects
		$creativeObjects = array();
		foreach($someCreatives['addCreativeListReturn'] as $someCreative) {
			$creativeObject = new APIlityTextCreative($someCreative['id'],	$someCreative['adGroupId'], $someCreative['headline'],	$someCreative['description1'], $someCreative['description2'], $someCreative['displayUrl'], $someCreative['destinationUrl'], $someCreative['deleted'], $someCreative['disapproved']);
			array_push($creativeObjects, $creativeObject);
		}
		return $creativeObjects;
	}

	// this won't fail completely if only one creative fails but will cause a lot of soap overhead
	function addCreativesOneByOne($creatives) {
		// this is just a wrapper to the addCreative function
		$creativeObjects = array();
		foreach ($creatives as $creative) {
			if (isset($creative['exemptionRequest'])) {
				// with exemption request
				$creativeObject = addCreative($creative['belongsToAdGroupId'], $creative['headline'], $creative['description1'], $creative['description2'], $creative['displayUrl'], $creative['destinationUrl'], $creative['exemptionRequest']);
			}
			else {
				// without exemption request
				$creativeObject = addCreative($creative['belongsToAdGroupId'], $creative['headline'], $creative['description1'], $creative['description2'], $creative['displayUrl'], $creative['destinationUrl']);
			}
			array_push($creativeObjects, $creativeObject);
		}
		return $creativeObjects;
	}

	function removeCreative(&$creativeObject) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		$soapParameters = "<deleteCreative>
													<adGroupId>".$creativeObject->getBelongsToAdGroupId()."</adGroupId>
													<creativeId>".$creativeObject->getId()."</creativeId>
											 </deleteCreative>";
		// remove the creative from the google servers
		$someSoapClient->call("deleteCreative", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":removeCreative()", $soapParameters);
	    return false;
		}
		// set isDeleted flag of the local object
		$creativeObject->isDeleted =  true;
		// delete remote calling object
		$creativeObject = @$GLOBALS['creativeObject'];
		unset($creativeObject);
		return true;
	}

	function getAllCreatives($adGroupId) {
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		$soapParameters = "<getAllCreatives>
													<adGroupId>".$adGroupId."</adGroupId>
											 </getAllCreatives>";
		// query the google servers for all creatives
		$allCreatives = array();
		$allCreatives = $someSoapClient->call("getAllCreatives", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCreatives()", $soapParameters);
	    return false;
		}

		// if only on creative then copy and create (one element) array of creatives
		if (isset($allCreatives['getAllCreativesReturn']['id'])) {
			$saveArray = $allCreatives['getAllCreativesReturn'];
			unset($allCreatives);
			$allCreatives['getAllCreativesReturn'][0] = $saveArray;
		}
		$allCreativeObjects = array();
		if (isset($allCreatives['getAllCreativesReturn'])) foreach($allCreatives['getAllCreativesReturn'] as $creative) {
			// return only non-deleted creatives
			if ((String)$creative['deleted']=="false") {
				$id = $creative['id'];
				$belongsToAdGroupId = $creative['adGroupId'];
				$headline = $creative['headline'];
				$description1 = $creative['description1'];
				$description2 = $creative['description2'];
				$displayUrl = $creative['displayUrl'];
				$destinationUrl = $creative['destinationUrl'];
				$isDeleted = $creative['deleted'];
				$isDisapproved = $creative['disapproved'];
				// these attributes apply just to image creatives, so just assign these attributes if we have an image ad
				if (isset($creative['image'])){
					$name = $creative['image']['name'];
					$width = $creative['image']['width'];
					$height = $creative['image']['height'];
					$imageUrl = $creative['image']['imageUrl'];
					$thumbnailUrl = $creative['image']['thumbnailUrl'];
					$mimeType = $creative['image']['mimeType'];
					$type = $creative['image']['type'];
				}
				// create object
				if (isset($description1)) {
					$creativeObject = new APIlityTextCreative ($id,	$belongsToAdGroupId, $headline,	$description1, $description2, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
				}
				else if (isset($width)) {
					$creativeObject = new APIlityImageCreative($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
				}
				array_push($allCreativeObjects, $creativeObject);
			}
		}
		return $allCreativeObjects;
	}

	// add an image creative to the server and create local object
	function addImageCreative($belongsToAdGroupId, $imageLocation, $name, $displayUrl, $destinationUrl) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		$soapParameters = "<addCreative>
											   <creative>
											     <adGroupId>".$belongsToAdGroupId."</adGroupId>
											     <image>
											       <data xsi:type='xsd:base64Binary'>".img2base64($imageLocation)."</data>
											       <name>".$name."</name>
											     </image>
											     <destinationUrl>".$destinationUrl."</destinationUrl>
											     <displayUrl>".$displayUrl."</displayUrl>
											   </creative>
											 </addCreative>";
		// add the creative to the google servers
		$someCreative = $someSoapClient->call("addCreative", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addImageCreative()", $soapParameters);
	    return false;
		}
		// populate object attributes
		$id = $someCreative['addCreativeReturn']['id'];
		$belongsToAdGroupId = $someCreative['addCreativeReturn']['adGroupId'];
		$name = $someCreative['addCreativeReturn']['image']['name'];
		$width = $someCreative['addCreativeReturn']['image']['width'];
		$height = $someCreative['addCreativeReturn']['image']['height'];
		$imageUrl = $someCreative['addCreativeReturn']['image']['imageUrl'];
		$thumbnailUrl = $someCreative['addCreativeReturn']['image']['thumbnailUrl'];
		$mimeType = $someCreative['addCreativeReturn']['image']['mimeType'];
		$type = $someCreative['addCreativeReturn']['image']['type'];
		$displayUrl = $someCreative['addCreativeReturn']['displayUrl'];
		$destinationUrl = $someCreative['addCreativeReturn']['destinationUrl'];
		$isDeleted = $someCreative['addCreativeReturn']['deleted'];
		$isDisapproved = $someCreative['addCreativeReturn']['disapproved'];
		// create local object
		$creativeObject = new APIlityImageCreative($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
		return $creativeObject;
	}

	// this won't fail completely if only one creative fails but will cause a lot of soap overhead
	function addImageCreativesOneByOne($creatives) {
		// this is just a wrapper to the addImageCreative function
		$creativeObjects = array();
		foreach ($creatives as $creative) {
			$creativeObject = addImageCreative($creative['belongsToAdGroupId'], $creative['imageLocation'], $creative['name'], $creative['displayUrl'], $creative['destinationUrl']);
			array_push($creativeObjects, $creativeObject);
		}
		return $creativeObjects;
	}

	function addImageCreativeList($creatives) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCreativeClient();
		$soapParameters = "<addCreativeList>";
		foreach ($creatives as $creative) {
			$soapParameters .= "<creatives>
														<adGroupId>".$creative['belongsToAdGroupId']."</adGroupId>
											      <image>
											        <data xsi:type='xsd:base64Binary'>".img2base64($creative['imageLocation'])."</data>
											        <name>".$creative['name']."</name>
											      </image>
											      <destinationUrl>".$creative['destinationUrl']."</destinationUrl>
											      <displayUrl>".$creative['displayUrl']."</displayUrl>
													</creatives>";
		}
		$soapParameters .= "</addCreativeList>";
		// add the creatives to the google servers
		$someCreatives = $someSoapClient->call("addCreativeList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addImageCreativeList()", $soapParameters);
	    return false;
		}
		// create local objects
		$creativeObjects = array();
		foreach($someCreatives['addCreativeListReturn'] as $someCreative) {
			// populate object attributes
			$id = $someCreative['id'];
			$belongsToAdGroupId = $someCreative['adGroupId'];
			$name = $someCreative['image']['name'];
			$width = $someCreative['image']['width'];
			$height = $someCreative['image']['height'];
			$imageUrl = $someCreative['image']['imageUrl'];
			$thumbnailUrl = $someCreative['image']['thumbnailUrl'];
			$mimeType = $someCreative['image']['mimeType'];
			$type = $someCreative['image']['type'];
			$displayUrl = $someCreative['displayUrl'];
			$destinationUrl = $someCreative['destinationUrl'];
			$isDeleted = $someCreative['deleted'];
			$isDisapproved = $someCreative['disapproved'];
			// now create the object
			$creativeObject = new APIlityImageCreative($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
			array_push($creativeObjects, $creativeObject);
		}
		return $creativeObjects;
	}
?>
