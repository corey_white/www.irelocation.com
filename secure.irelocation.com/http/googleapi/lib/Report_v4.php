<?php
	function getXmlReport($reportName, $reportType, $aggregationType, $startDate, $endDate, $sleepTime, $isCrossClient = false, $clientEmails = "", $customOptions = "", $includeZeroImpression = false) {
		global $soapClients;
		$someSoapClient = $soapClients->getReportClient();

    // custom report xml parameters
		$customOptionsXml = "";
		if ($customOptions != "") {
			// we are expecting custom options like this: array("customOption1", "customOption2")
			foreach($customOptions as $customOption) {
				$customOptionsXml .= "<customOptions>".trim($customOption)."</customOptions>";
			}
		}

		// client email xml parameters
		$clientEmailsXml = "";
		if ($clientEmails != "") {
			// we are expecting client emails like this: array("someone@somewhere.xyz", "anyone@anywhere.xyz")
			foreach($clientEmails as $clientEmail) {
				$clientEmailsXml .= "<clientEmails>".trim($clientEmail)."</clientEmails>";
			}
		}

		// prepare soap parameters
		if ($isCrossClient) $isCrossClient = "true"; else $isCrossClient = "false";
		if ($includeZeroImpression) $includeZeroImpression = "true"; else $includeZeroImpression = "false";
		$soapParameters = "<scheduleReportJob xmlns='".WSDL_URL."'>
													<job xsi:type='".$reportType."'>
														<name>".$reportName."</name>
														<aggregationType>".$aggregationType."</aggregationType>
														<startDay>".$startDate."</startDay>
														<endDay>".$endDate."</endDay>
														<crossClient>".$isCrossClient."</crossClient>
														<includeZeroImpression>".$includeZeroImpression."</includeZeroImpression>"
														.$customOptionsXml.""
														.$clientEmailsXml."
													</job>
												</scheduleReportJob>";
		// talk to the google servers and schedule report
		$someSchedule = $someSoapClient->call("scheduleReportJob", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getXmlReport()", $soapParameters);
	    return false;
		}
		$soapParameters = "<getReportJobStatus xmlns='".WSDL_URL."'>
													<reportJobId>".$someSchedule['scheduleReportJobReturn']."</reportJobId>
											 </getReportJobStatus>";
    // check the status of the scheduled report
		$reportStatus = $someSoapClient->call("getReportJobStatus", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getXmlReport()", $soapParameters);
	    return false;
		}
		// busy waiting till report is finished (or till creation fails)
		while  ((strcmp($reportStatus['getReportJobStatusReturn'], "InProgress") == 0) || (strcmp($reportStatus['getReportJobStatusReturn'], "Pending") == 0)) {
			$reportStatus = $someSoapClient->call("getReportJobStatus", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
	  		pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getXmlReport()", $soapParameters);
	    	return false;
			}
			// report failed :(
			if  (strcmp($reportStatus['getReportJobStatusReturn'], "Failed") == 0) {
  			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning:</b><br />Sorry, but for some mysterious reason I could not finish your report request.<p>";
  			return false;
			}
			// report succeeded :)
			if (strcmp($reportStatus['getReportJobStatusReturn'], "Completed") == 0) {
				$soapParameters  = "<getGzipReportDownloadUrl xmlns='".WSDL_URL."'> <reportJobId>" .$someSchedule['scheduleReportJobReturn']. "</reportJobId> </getGzipReportDownloadUrl>";
  			$reportUrl = $someSoapClient->call("getGzipReportDownloadUrl", $soapParameters);
  			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
  			if ($someSoapClient->fault) {
	  			pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getXmlReport()", $soapParameters);
	    		return false;
				}
				// quit busy waiting when the report is available
  			break;
  		}
  		// busy waiting with n seconds break
  		sleep($sleepTime);
		}
		// open connection to the Google server via cURL
		$curlConnection = curl_init();
		curl_setopt($curlConnection, CURLOPT_URL, $reportUrl['getGzipReportDownloadUrlReturn']);
		curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt ($curlConnection, CURLOPT_USERAGENT, generateCurlUserAgent());
		// buffer for downloading report xml
		ob_start();
			curl_exec($curlConnection);
			// buffer report
			// inflate the gzipped report we got
			$reportXml = gzinflate(substr(ob_get_contents(),10));
		ob_end_clean();
		// end buffering
		if (curl_errno($curlConnection)) {
   		if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning:</b><br />Sorry, there was a problem while downloading your report. The cURL error message is: <b>".curl_error($curlConnection)."</b>";
   		return false;
   	}
   	else curl_close($curlConnection);
		// note: xml won't be interpreted in any browser when displayed
		// from within a php file (browser hides "unknown" xml tags)
		// so use the htmlspecialchars() function to "see" the xml code

		// transform micros in normal currency units
		// e.g. EXCHANGE_RATE equals 1$
		// using DOM XML for downwards compatibility with PHP4
		//
		// structure of XML report is
		//
		// report
		//  |
		//  |-table
		//  |  |
		//  |  |-columns (contains column elements with the names of the report columns like "campaign", "adgroup", "keyword", ...
		//  |  |
		//  |  |-rows (contains row elements with the specified report data
		//  |
		//  |-totals
		//     |
		//     |-grandtotal (contains general statistic information of the report

	if (version_compare(phpversion(), "5.0.0", ">=")) {
			$xmlDomDocument = new DOMDocument();
			$xmlDomDocument->loadXML($reportXml);
			$singleRows = $xmlDomDocument->getElementsByTagName("row");

			foreach($singleRows as $row) {
					$currencyUnitsCost = $row->getAttribute("cost") / EXCHANGE_RATE;
					$currencyUnitsCpc = $row->getAttribute("cpc") / EXCHANGE_RATE;
					$currencyUnitsMaxCpc = $row->getAttribute("maxCpc") / EXCHANGE_RATE;
					$currencyUnitsMinCpc = $row->getAttribute("keywordMinCpc") / EXCHANGE_RATE;
					$currencyUnitsCpm = $row->getAttribute("cpm") / EXCHANGE_RATE;
					$row->setAttribute("cost", (String) $currencyUnitsCost);
					$row->setAttribute("cpc", (String) $currencyUnitsCpc);
					$row->setAttribute("maxCpc", (String) $currencyUnitsMaxCpc);
					$row->setAttribute("keywordMinCpc", (String) $currencyUnitsMinCpc);
					$row->setAttribute("cpm", (String) $currencyUnitsCpm);
			}
			// in grandtotal
			$grandtotals = $xmlDomDocument->getElementsByTagName("grandtotal");

			foreach ($grandtotals as $grandtotal) {
				$currencyUnitsCost = $grandtotal->getAttribute("cost") / EXCHANGE_RATE;
				$currencyUnitsCpc = $grandtotal->getAttribute("cpc") / EXCHANGE_RATE;
				$currencyUnitsCpm = $grandtotal->getAttribute("cpm") / EXCHANGE_RATE;
				$grandtotal->setAttribute("cost", (String) $currencyUnitsCost);
				$grandtotal->setAttribute("cpc", (String) $currencyUnitsCpc);
				$grandtotal->setAttribute("cpm", (String) $currencyUnitsCpm);
			}
			return $xmlDomDocument->saveXML();
		}
		// PHP version is <5, i.e. only DOM XML is available
		else {
			$xmlDomDocument = domxml_open_mem($reportXml);
			$report = $xmlDomDocument->document_element();
			$table =  $report->first_child();
			$rows = $table->last_child();
			$singleRows = $rows->child_nodes();
			// in rows
			foreach($singleRows as $row) {
					$currencyUnitsCost = $row->get_attribute("cost") / EXCHANGE_RATE;
					$currencyUnitsCpc = $row->get_attribute("cpc") / EXCHANGE_RATE;
					$currencyUnitsMaxCpc = $row->get_attribute("maxCpc") / EXCHANGE_RATE;
					$currencyUnitsMinCpc = $row->get_attribute("keywordMinCpc") / EXCHANGE_RATE;
					$currencyUnitsCpm = $row->get_attribute("cpm") / EXCHANGE_RATE;
					$row->set_attribute("cost", (String) $currencyUnitsCost);
					$row->set_attribute("cpc", (String) $currencyUnitsCpc);
					$row->set_attribute("maxCpc", (String) $currencyUnitsMaxCpc);
					$row->set_attribute("keywordMinCpc", (String) $currencyUnitsMinCpc);
					$row->set_attribute("cpm", (String) $currencyUnitsCpm);
			}
			// in grandtotal
			$totals = $report->last_child();
			$grandtotal = $totals->first_child();
			$currencyUnitsCost = $grandtotal->get_attribute("cost") / EXCHANGE_RATE;
			$currencyUnitsCpc = $grandtotal->get_attribute("cpc") / EXCHANGE_RATE;
			$currencyUnitsCpm = $grandtotal->get_attribute("cpm") / EXCHANGE_RATE;
			$grandtotal->set_attribute("cost", (String) $currencyUnitsCost);
			$grandtotal->set_attribute("cpc", (String) $currencyUnitsCpc);
			$grandtotal->set_attribute("cpm", (String) $currencyUnitsCpm);
			// finished conversion
			return $xmlDomDocument->dump_mem();
		}
	}

	function downloadXmlReport($reportId) {
		global $soapClients;
		$someSoapClient = $soapClients->getReportClient();
		$soapParameters  = "<getGzipReportDownloadUrl xmlns='".WSDL_URL."'> <reportJobId>".$reportId."</reportJobId> </getGzipReportDownloadUrl>";
		$reportUrl = $someSoapClient->call("getGzipReportDownloadUrl", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
			pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getXmlReport()", $soapParameters);
	 		return false;
		}
		// open connection to the Google server via cURL
		$curlConnection = curl_init();
		curl_setopt($curlConnection, CURLOPT_URL, $reportUrl['getGzipReportDownloadUrlReturn']);
		curl_setopt($curlConnection, CURLOPT_SSL_VERIFYPEER, 0);
		// buffer for downloading report xml
		ob_start();
			curl_exec($curlConnection);
			// buffer report
			// inflate the gzipped report we got
			$reportXml = gzinflate(substr(ob_get_contents(),10));
		ob_end_clean();
		// end buffering
		if (curl_errno($curlConnection)) {
	 		if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning:</b><br />Sorry, there was a problem while downloading your report. The cURL error message is:<p><font color='maroon'>".curl_error($curlConnection)."</font>";
	 		return false;
	 	}
	 	else curl_close($curlConnection);
		// PHP version is >= 5, i.e. only DOM is avalable
		if (version_compare(phpversion(), "5.0.0", ">=")) {
			$xmlDomDocument = new DOMDocument();
			$xmlDomDocument->loadXML($reportXml);
			$singleRows = $xmlDomDocument->getElementsByTagName("row");

			foreach($singleRows as $row) {
					$currencyUnitsCost = $row->getAttribute("cost") / EXCHANGE_RATE;
					$currencyUnitsCpc = $row->getAttribute("cpc") / EXCHANGE_RATE;
					$currencyUnitsMaxCpc = $row->getAttribute("maxCpc") / EXCHANGE_RATE;
					$currencyUnitsMinCpc = $row->getAttribute("keywordMinCpc") / EXCHANGE_RATE;
					$currencyUnitsCpm = $row->getAttribute("cpm") / EXCHANGE_RATE;
					$row->setAttribute("cost", (String) $currencyUnitsCost);
					$row->setAttribute("cpc", (String) $currencyUnitsCpc);
					$row->setAttribute("maxCpc", (String) $currencyUnitsMaxCpc);
					$row->setAttribute("keywordMinCpc", (String) $currencyUnitsMinCpc);
					$row->setAttribute("cpm", (String) $currencyUnitsCpm);
			}
			// in grandtotal
			$grandtotals = $xmlDomDocument->getElementsByTagName("grandtotal");

			foreach ($grandtotals as $grandtotal) {
				$currencyUnitsCost = $grandtotal->getAttribute("cost") / EXCHANGE_RATE;
				$currencyUnitsCpc = $grandtotal->getAttribute("cpc") / EXCHANGE_RATE;
				$currencyUnitsCpm = $grandtotal->getAttribute("cpm") / EXCHANGE_RATE;
				$grandtotal->setAttribute("cost", (String) $currencyUnitsCost);
				$grandtotal->setAttribute("cpc", (String) $currencyUnitsCpc);
				$grandtotal->setAttribute("cpm", (String) $currencyUnitsCpm);
			}
			return $xmlDomDocument->saveXML();
		}
		// PHP version is <5, i.e. only DOM XML is available
		else {
			$xmlDomDocument = domxml_open_mem($reportXml);
			$report = $xmlDomDocument->document_element();
			$table =  $report->first_child();
			$rows = $table->last_child();
			$singleRows = $rows->child_nodes();
			// in rows
			foreach($singleRows as $row) {
					$currencyUnitsCost = $row->get_attribute("cost") / EXCHANGE_RATE;
					$currencyUnitsCpc = $row->get_attribute("cpc") / EXCHANGE_RATE;
					$currencyUnitsMaxCpc = $row->get_attribute("maxCpc") / EXCHANGE_RATE;
					$currencyUnitsMinCpc = $row->get_attribute("keywordMinCpc") / EXCHANGE_RATE;
					$currencyUnitsCpm = $row->get_attribute("cpm") / EXCHANGE_RATE;
					$row->set_attribute("cost", (String) $currencyUnitsCost);
					$row->set_attribute("cpc", (String) $currencyUnitsCpc);
					$row->set_attribute("maxCpc", (String) $currencyUnitsMaxCpc);
					$row->set_attribute("keywordMinCpc", (String) $currencyUnitsMinCpc);
					$row->set_attribute("cpm", (String) $currencyUnitsCpm);
			}
			// in grandtotal
			$totals = $report->last_child();
			$grandtotal = $totals->first_child();
			$currencyUnitsCost = $grandtotal->get_attribute("cost") / EXCHANGE_RATE;
			$currencyUnitsCpc = $grandtotal->get_attribute("cpc") / EXCHANGE_RATE;
			$currencyUnitsCpm = $grandtotal->get_attribute("cpm") / EXCHANGE_RATE;
			$grandtotal->set_attribute("cost", (String) $currencyUnitsCost);
			$grandtotal->set_attribute("cpc", (String) $currencyUnitsCpc);
			$grandtotal->set_attribute("cpm", (String) $currencyUnitsCpm);
			// finished conversion
			return $xmlDomDocument->dump_mem();
		}
	}

	function deleteReport($reportJobId) {
		global $soapClients;
		$someSoapClient = $soapClients->getReportClient();
		$soapParameters = "<deleteReport>
													<reportJobId>".$reportJobId."</reportJobId>
											 </deleteReport>";
		// delete the report on the google servers
	  $someSoapClient->call("deleteReport", $soapParameters);
	  $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":deleteReport()", $soapParameters);
	    return false;
		}
		return true;
	}

	function getAllJobs() {
		global $soapClients;
		$someSoapClient = $soapClients->getReportClient();
		$soapParameters = "<getAllJobs></getAllJobs>";
		// query the google servers for all existing report jobs
		$allReportJobs = $someSoapClient->call("getAllJobs");
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllJobs()", $soapParameters);
	    return false;
		}
		return $allReportJobs['getAllJobsReturn'];
	}

  function getCsvReport($reportName, $reportType, $aggregationType, $startDate, $endDate, $sleepTime, $isCrossClient = false, $clientEmails = "", $customOptions = "", $includeZeroImpression = false) {
  	// this is just an XML2CSV wrapper
  	return xml2Csv(getXmlReport($reportName, $reportType, $aggregationType, $startDate, $endDate, $sleepTime, $isCrossClient = false, $clientEmails = "", $customOptions = "", $includeZeroImpression = false));
  }

  function downloadCsvReport($reportId) {
  	return xml2Csv(downloadXmlReport($reportId));
  }

	function xml2Csv($xmlString) {
		// define csv which will hold the csv string
		$csv = "";

		// an XML2CSV transformer
		// using DOM XML for downwards compatibility with PHP4
		//
		// structure of XML report is
		//
		// report
		//  |
		//  |-table
		//  |  |
		//  |  |-columns (contains column elements with the names of the report columns like "campaign", "adgroup", "keyword", ...
		//  |  |
		//  |  |-rows (contains row elements with the specified report data
		//  |
		//  |-totals
		//     |
		//     |-grandtotal (contains general statistic information of the report

		// PHP version is >= 5, i.e. only DOM is avalable
		if (version_compare(phpversion(), "5.0.0", ">=")) {
			$xmlDomDocument = new DOMDocument();
			$xmlDomDocument->loadXML($xmlString);

			$singleColumns = $xmlDomDocument->getElementsByTagName("column");
			// get attribute names (i.e. column names)
			$attributeNames = array();
			// first line of the csv report holds the column names
			foreach($singleColumns as $column) {
				$csv .= $column->getAttribute('name')."\t";
				array_push($attributeNames, $column->getAttribute('name'));
			}
			$csv .= "\n";

			// fill columns with report data
			$singleRows = $xmlDomDocument->getElementsByTagName("row");
			foreach($singleRows as $row) {
				foreach($attributeNames as $attributeName) {
					$csv .= $row->getAttribute($attributeName)."\t";
				}
				$csv .= "\n";
			}
			// and done
			return $csv;
		}
		// PHP version is <5, i.e. only DOM XML is available
		else {
			$xmlDomDocument = domxml_open_mem($xmlString);
			$report = $xmlDomDocument->document_element();
			$table =  $report->first_child();
			$totals = $report->last_child();
			$columns = $table->first_child();
			$rows = $table->last_child();
			// might add grandtotal but won't do this at present
			// uncomment the following line to do this anyhow
			//$grandtotal = $totals->first_child();

			$singleColumns = $columns->child_nodes();

			// get attribute names (i.e. column names)
			$attributeNames = array();
			// first line of the csv report holds the column names
			foreach($singleColumns as $column) {
				$csv .= $column->get_attribute('name')."\t";
				array_push($attributeNames, $column->get_attribute('name'));
			}
			$csv .= "\n";

			// fill columns with report data
			$singleRows = $rows->child_nodes();
			foreach($singleRows as $row) {
				foreach($attributeNames as $attributeName) {
					$csv .= $row->get_attribute($attributeName)."\t";
				}
				$csv .= "\n";
			}
			// and done
			return $csv;
		}
	}

	function generateCurlUserAgent() {
		// the google servers return the reports transparently (on the ISO/OSI transport layer) gzipped iff the header contains the string "gzip"
		$curlVersion = curl_version();
		// PHP version is >= 5
		if (version_compare(phpversion(), "5.0.0", ">="))	{
			$userAgent = "libcurl/".$curlVersion['version'].$curlVersion['ssl_version']." libz/".$curlVersion['libz_version'];
		}
		else {
		// PHP version is <5
			$userAgent = $curlVersion;
		}
		return $userAgent;
	}

?>
