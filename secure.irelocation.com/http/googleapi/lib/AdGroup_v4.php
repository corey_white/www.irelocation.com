<?php
	class AdGroup {
		// class attributes
		var $maxCpc;
		var $maxCpm;
		var $maxContentCpc;
	  var $name;
		var $id;
		var $belongsToCampaignId;
	  var $status;

		// constructor
		function AdGroup ($name, $id, $belongsToCampaignId, $maxCpc, $maxCpm, $maxContentCpc, $status) {
			$this->name = $name;
			$this->id = $id;
			$this->belongsToCampaignId = $belongsToCampaignId;
			$this->maxCpc = $maxCpc;
			$this->maxCpm = $maxCpm;
			$this->maxContentCpc = $maxContentCpc;
			// google server uses "enabled" as adgroup status. we overcome this inconsistency and call all
			// "active" states just active. anyway we catch user input when someone passes "enabled"
			if (strtolower($status) == "enabled") $status = "Active";
			$this->status = $status;
		}

		// XML output
		function toXml() {
			$xml = "<AdGroup>
	<name>".$this->getName()."</name>
	<id>".$this->getId()."</id>
	<status>".$this->getStatus()."</status>
	<belongsToCampaignId>".$this->getBelongsToCampaignId()."</belongsToCampaignId>
	<maxCpc>".$this->getMaxCpc()."</maxCpc>
	<maxCpm>".$this->getMaxCpm()."</maxCpm>
	<maxContentCpc>".$this->getMaxContentCpc()."</maxContentCpc>
</AdGroup>";
			return $xml;
		}

		// get functions
		function getName() {
			return $this->name;
		}

		function getId() {
			return $this->id;
		}

		function getBelongsToCampaignId() {
			return $this->belongsToCampaignId;
		}

		function getMaxCpc() {
			return $this->maxCpc;
		}

		function getMaxContentCpc() {
			return $this->maxContentCpc;
		}

		function getMaxCpm() {
			return $this->maxCpm;
		}

		function getStatus() {
			return $this->status;
		}

		function getEstimate() {
			// this function is located in TrafficEstimate.php
			return getAdGroupEstimate($this);
		}

		// report function
		function getAdGroupData() {
			$adGroupData = array(
												'name'=>$this->getName(),
												'id'=>$this->getId(),
												'belongsToCampaignId'=>$this->getBelongsToCampaignId(),
												'maxCpc'=>$this->getMaxCpc(),
												'maxCpm'=>$this->getMaxCpm(),
												'maxContentCpc'=>$this->getMaxContentCpc(),
												'status'=>$this->getStatus()
												);
			return $adGroupData;
		}

		function getAdGroupStats($startDate, $endDate, $inPST = false) {
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			if ($inPST) $inPST = "true"; else $inPST = "false";
			$soapParameters = "<getAdGroupStats>
														<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
														<adGroupIds>".$this->getId()."</adGroupIds>
														<startDay>".$startDate."</startDay>
														<endDay>".$endDate."</endDay>
														<inPST>".$inPST."</inPST>
												 </getAdGroupStats>";
			// query the google servers for the adgroup stats
			$adGroupStats = $someSoapClient->call("getAdGroupStats", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAdGroupStats()", $soapParameters);
		    return false;
			}
			// add the adgroup name to the stats for the sake of clarity
			$adGroupStats['getAdGroupStatsReturn']['name'] = $this->getName();
			// think in currency units here
			$adGroupStats['getAdGroupStatsReturn']['cost'] = $adGroupStats['getAdGroupStatsReturn']['cost'] / EXCHANGE_RATE;
			return $adGroupStats['getAdGroupStatsReturn'];
		}

		function getAllKeywords() {
		 	global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
		 	$soapParameters = "<getAllKeywords>
		 												<adGroupId>".$this->getId()."</adGroupId>
		 										 </getAllKeywords>";
		 	// query the google servers for all keywords of the adgroup
		 	$allKeywords = array();
		 	$allKeywords = $someSoapClient->call("getAllKeywords", $soapParameters);
		 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllKeywords()", $soapParameters);
		    return false;
			}

			// if only one keyword then copy and create array of keywords anyway
			if (isset($allKeywords['getAllKeywordsReturn']['id'])) {
				$saveArray = $allKeywords['getAllKeywordsReturn'];
				unset($allKeywords);
				$allKeywords['getAllKeywordsReturn'][0] = $saveArray;
			}
			$allKeywordObjects = array();
			if (isset($allKeywords['getAllKeywordsReturn'])) foreach ($allKeywords['getAllKeywordsReturn'] as $keyword) {
				// create keyword objects
			$keywordObject = new Keyword ($keyword['text'], $keyword['id'], $keyword['adGroupId'], $keyword['type'], $keyword['negative'], $keyword['maxCpc'] / EXCHANGE_RATE , $keyword['minCpc'] / EXCHANGE_RATE , $keyword['status'], $keyword['language'], $keyword['destinationUrl']);
			array_push($allKeywordObjects, $keywordObject);
			}
			return $allKeywordObjects;
		}

		function getAllActiveKeywords() {
		 	global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
		 	$soapParameters = "<getActiveKeywords>
		 												<adGroupId>".$this->getId()."</adGroupId>
		 										 </getActiveKeywords>";
		 	// query the google servers for all active keywords of the adgroup
		 	$allKeywords = array();
		 	$allKeywords = $someSoapClient->call("getActiveKeywords", $soapParameters);
		 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllActiveKeywords()", $soapParameters);
		    return false;
			}

			// if only one keyword then copy and create array of keywords anyway
			if (isset($allKeywords['getActiveKeywordsReturn']['id'])) {
				$saveArray = $allKeywords['getActiveKeywordsReturn'];
				unset($allKeywords);
				$allKeywords['getActiveKeywordsReturn'][0] = $saveArray;
			}
			$allKeywordObjects = array();
			if (isset($allKeywords['getActiveKeywordsReturn'])) foreach ($allKeywords['getActiveKeywordsReturn'] as $keyword) {
				// create keyword objects
			$keywordObject = new Keyword ($keyword['text'], $keyword['id'], $keyword['adGroupId'], $keyword['type'], $keyword['negative'], $keyword['maxCpc'] / EXCHANGE_RATE , $keyword['minCpc'] / EXCHANGE_RATE , $keyword['status'], $keyword['language'], $keyword['destinationUrl']);
			array_push($allKeywordObjects, $keywordObject);
			}
			return $allKeywordObjects;
		}

		function getAllCreatives() {
			global $soapClients;
			$someSoapClient = $soapClients->getCreativeClient();
			$soapParameters = "<getAllCreatives>
														<adGroupId>".$this->getId()."</adGroupId>
												 </getAllCreatives>";
			// query the google servers for all creatives of the adgroup
			$allCreatives = array();
			$allCreatives = $someSoapClient->call("getAllCreatives", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCreatives()", $soapParameters);
		    return false;
			}

			// if only on creative then copy and create array of arrays
			if (isset($allCreatives['getAllCreativesReturn']['id'])) {
				$saveArray = $allCreatives['getAllCreativesReturn'];
				unset($allCreatives);
				$allCreatives['getAllCreativesReturn'][0] = $saveArray;
			}
			$allCreativeObjects = array();
			if (isset($allCreatives['getAllCreativesReturn'])) foreach($allCreatives['getAllCreativesReturn'] as $creative) {
				// return only non-deleted creatives
				if ((String)$creative['deleted']=="false") {
					$id = $creative['id'];
					$belongsToAdGroupId = $creative['adGroupId'];
					$headline = $creative['headline'];
					$description1 = $creative['description1'];
					$description2 = $creative['description2'];
					$displayUrl = $creative['displayUrl'];
					$destinationUrl = $creative['destinationUrl'];
					$isDeleted = $creative['deleted'];
					$isDisapproved = $creative['disapproved'];
					// these attributes apply just to image creatives, so just assign these attributes if we have an image ad
					if (isset($creative['image'])){
						$name = $creative['image']['name'];
						$width = $creative['image']['width'];
						$height = $creative['image']['height'];
						$imageUrl = $creative['image']['imageUrl'];
						$thumbnailUrl = $creative['image']['thumbnailUrl'];
						$mimeType = $creative['image']['mimeType'];
						$type = $creative['image']['type'];
					}
					// create object
					if (isset($description1)) {
						$creativeObject = new TextCreative ($id,	$belongsToAdGroupId, $headline,	$description1, $description2, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
					}
					else if (isset($width)) {
						$creativeObject = new ImageCreative($id, $belongsToAdGroupId, $name, $width, $height, $imageUrl, $thumbnailUrl, $mimeType, $type, $displayUrl, $destinationUrl, $isDeleted, $isDisapproved);
					}
					array_push($allCreativeObjects, $creativeObject);
				}
			}
			return $allCreativeObjects;
		}

		function getAllCriteria() {
		 	global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
		 	$soapParameters = "<getAllCriteria>
		 												<adGroupId>".$this->getId()."</adGroupId>
		 										 </getAllCriteria>";
		 	// query the google servers for all criteria
		 	$allCriteria = array();
		 	$allCriteria = $someSoapClient->call("getAllCriteria", $soapParameters);
		 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCriteria()", $soapParameters);
		    return false;
			}

			// when we have only one criterion in the adgroup return a (one criterion element) array  anyway
			if (isset($allCriteria['getAllCriteriaReturn']['id'])) {
				$saveArray = $allCriteria['getAllCriteriaReturn'];
				unset($allCriteria);
				$allCriteria['getAllCriteriaReturn'][0] = $saveArray;
			}
			// when there are several criteria in the adgroup just return them normally	(i.e. array of criteria)
			$allCriterionObjects = array();
			if (isset($allCriteria['getAllCriteriaReturn'])) foreach ($allCriteria['getAllCriteriaReturn'] as $criterion) {
				// create generic object attributes
				$belongsToAdGroupId = $criterion['adGroupId'];
				$criterionType = $criterion['criterionType'];
				$destinationUrl = $criterion['destinationUrl'];
				$id = $criterion['id'];
				$language = $criterion['language'];
				$isNegative = $criterion['negative'];
				$status = $criterion['status'];

				// if we have a keyword criterion create its object attributes
				if (strcasecmp($criterion['criterionType'], "Keyword") == 0) {
					$maxCpc = $criterion['maxCpc'];
					$minCpc = $criterion['minCpc'];
		      $type = $criterion['type'];
		      $text = $criterion['text'];
		      $criterionObject = new KeywordCriterion($text, $id, $belongsToAdGroupId, $type, $criterionType, $isNegative, $maxCpc / EXCHANGE_RATE, $minCpc / EXCHANGE_RATE, $status, $language, $destinationUrl);
		    }
		    // else create the website criterion's object attributes
		    else {
	      	$maxCpm = $criterion['maxCpm'];
					$url = $criterion['url'];
					$criterionObject = new WebsiteCriterion($url, $id, $belongsToAdGroupId, $criterionType, $isNegative, $maxCpm / EXCHANGE_RATE, $status, $language, $destinationUrl);
	      }
				array_push($allCriterionObjects, $criterionObject);
			}
			return $allCriterionObjects;
		}

		// set functions
		function setName ($newName) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			$soapParameters = "<updateAdGroup>
														<changedData>
															<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
															<id>".$this->getId()."</id>
															<name>".$newName."</name>
														</changedData>
													</updateAdGroup>";
			// set the new name on the google servers
			$someSoapClient->call("updateAdGroup", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setName()", $soapParameters);
		    return false;
			}
			// update local object
			$this->name = $newName;
			return true;
		}

		function setMaxCpc ($newMaxCpc) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			// think in micros
			$soapParameters = "<updateAdGroup>
														<changedData>
															<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
															<id>".$this->getId()."</id>
															<maxCpc>".($newMaxCpc * EXCHANGE_RATE)."</maxCpc>
														</changedData>
												 </updateAdGroup>";
			// set the new maxcpc on the google servers
			$someSoapClient->call("updateAdGroup", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));

			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxCpc()", $soapParameters);
		    return false;
			}
			// update local object
			$this->maxCpc = $newMaxCpc;
			return true;
		}

		function setMaxContentCpc ($newMaxContentCpc) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			// think in micros
			$soapParameters = "<updateAdGroup>
														<changedData>
															<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
															<id>".$this->getId()."</id>
															<maxContentCpc>".($newMaxContentCpc * EXCHANGE_RATE)."</maxContentCpc>
														</changedData>
												 </updateAdGroup>";
			// set the new maxcpc on the google servers
			$someSoapClient->call("updateAdGroup", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));

			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxContentCpc()", $soapParameters);
		    return false;
			}
			// update local object
			$this->maxContentCpc = $newMaxContentCpc;
			return true;
		}

		function setMaxCpm ($newMaxCpm) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			// think in micros
			$soapParameters = "<updateAdGroup>
														<changedData>
															<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
															<id>".$this->getId()."</id>
															<maxCpm>".($newMaxCpm * EXCHANGE_RATE)."</maxCpm>
														</changedData>
												 </updateAdGroup>";
			// set the new maxcpc on the google servers
			$someSoapClient->call("updateAdGroup", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));

			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxCpm()", $soapParameters);
		    return false;
			}
			// update local object
			$this->maxCpm = $newMaxCpm;
			return true;
		}

		function matchMaxCpcsToMinCpcs() {
			$allCriteria = $this->getAllCriteria();
			if (sizeOf($allCriteria)>0) foreach($allCriteria as $criterion) {
				$criterion->matchMaxCpcToMinCpc();
			}
			return true;
		}

		function setStatus($newStatus) {
			// local object has status "active" and "paused"
			// google server object has "enabled" and "paused"
			// renaming all "enabled"-s to "active"-s for consistency reasons
			if (strtolower($newStatus) == "active") $newStatus = "Enabled";
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			$soapParameters = "<updateAdGroup>
														<changedData>
															<campaignId>".$this->getBelongsToCampaignId()."</campaignId>
															<id>".$this->getId()."</id>
															<status>".$newStatus."</status>
														</changedData>
												 </updateAdGroup>";
			// set the new status on the google servers
			$someSoapClient->call("updateAdGroup", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setStatus()", $soapParameters);
		    return false;
			}
			// update local object
			if (strtolower($newStatus) == "enabled") $newStatus = "Active";
			$this->status = $newStatus;
			return true;
		}
	}

	function addAdGroup($name, $campaignId, $status, $maxCpc, $maxCpm, $maxContentCpc = 0) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getAdGroupClient();
		// google server calls active adgroups enabled adgroups so rename if necessary
		if (strtolower($status) == "active") $status = "Enabled";
		// think in micros
		// danger: we need to have maxCpc XOR maxCpm, so we need to set either maxCpc or maxCpm to a value different from zero
		if ($maxCpc > 0) {
			$maxCpc = $maxCpc * EXCHANGE_RATE;
			$maxCpWhateverXml = "<maxCpc>".$maxCpc."</maxCpc>";
		}
		else {
			$maxCpm = $maxCpm * EXCHANGE_RATE;
			$maxCpWhateverXml = "<maxCpm>".$maxCpm."</maxCpm>";
		}
		$maxContentCpc = $maxContentCpc * EXCHANGE_RATE;
		if ($maxContentCpc > 0) $maxContentCpcXml = "<maxContentCpc>".$maxContentCpc."</maxContentCpc>"; else $maxContentCpcXml = "";

		$soapParameters = "<addAdGroup>
													<campaignId>".$campaignId."</campaignId>
													<newData>
														<status>".$status."</status>
														<name>".$name."</name>"
														.$maxCpWhateverXml.""
													  .$maxContentCpcXml."
													</newData>
												</addAdGroup>";
		// add the adgroup to the google servers
		$someAdGroup = $someSoapClient->call("addAdGroup", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addAdGroup()", $soapParameters);
	    return false;
		}
		// create local object
		// danger! think in currency units here
		if (isset($someAdGroup['addAdGroupReturn']['maxCpc'])) $maxCpc = $someAdGroup['addAdGroupReturn']['maxCpc'] / EXCHANGE_RATE; else $maxCpc = NULL;
		if (isset($someAdGroup['addAdGroupReturn']['maxCpm'])) $maxCpm = $someAdGroup['addAdGroupReturn']['maxCpm'] / EXCHANGE_RATE; else $maxCpm = NULL;
		if (isset($someAdGroup['addAdGroupReturn']['maxContentCpc'])) $maxContentCpc = $someAdGroup['addAdGroupReturn']['maxContentCpc'] / EXCHANGE_RATE; else $maxContentCpc = NULL;
		$adGroupObject = new AdGroup($someAdGroup['addAdGroupReturn']['name'], $someAdGroup['addAdGroupReturn']['id'], $someAdGroup['addAdGroupReturn']['campaignId'], $maxCpc, $maxCpm, $maxContentCpc, $someAdGroup['addAdGroupReturn']['status']);
		return $adGroupObject;
	}

	// this will fail completely if only one adgroup fails
	// but won't cause soap overhead
	function addAdGroupList($adgroups) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getAdGroupClient();

		$soapParameters = "<addAdGroupList>
												 <campaignId>".$adgroups[0]['belongsToCampaignId']."</campaignId>";
		foreach ($adgroups as $adgroup) {
			if (strtolower($adgroup['status']) == "active") $adgroup['status'] = "Enabled";
			// think in micros
			// danger: we need to have maxCpc XOR maxCpm, so we need to set either maxCpc or maxCpm to a value different from zero
			if ($adgroup['maxCpc'] > 0) {
				$adgroup['maxCpc'] = $adgroup['maxCpc'] * EXCHANGE_RATE;
				$maxCpWhateverXml = "<maxCpc>".$adgroup['maxCpc']."</maxCpc>";
			}
			else {
				$adgroup['maxCpm'] = $adgroup['maxCpm'] * EXCHANGE_RATE;
				$maxCpWhateverXml = "<maxCpm>".$adgroup['maxCpm']."</maxCpm>";
			}
			$adgroup['maxContentCpc'] = $adgroup['maxContentCpc'] * EXCHANGE_RATE;
			if ($adgroup['maxContentCpc'] > 0) $maxContentCpcXml = "<maxContentCpc>".$adgroup['maxContentCpc']."</maxContentCpc>"; else $maxContentCpcXml = "";
			$soapParameters .= "<newData>
															<status>".$adgroup['status']."</status>
														<name>".$adgroup['name']."</name>"
														.$maxCpWhateverXml.""
													  .$maxContentCpcXml."
													</newData>";
		}
		$soapParameters .= "</addAdGroupList>";
		// add adgroups to the google servers
		$someAdGroups = $someSoapClient->call("addAdGroupList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addAdGroupList()", $soapParameters);
	    return false;
		}

		// when we have only one adgroup return a (one adgroup element) array  anyway
		if (isset($someAdGroups['addAdGroupListReturn']['id'])) {
			$saveArray = $someAdGroups['addAdGroupListReturn'];
			unset($someAdGroups);
			$someAdGroups['addAdGroupListReturn'][0] = $saveArray;
		}

		// create local objects
		$adGroupObjects = array();
		foreach($someAdGroups['addAdGroupListReturn'] as $someAdGroup) {
			// danger! think in currency units here
			if (isset($someAdGroup['maxCpc'])) $maxCpc = $someAdGroup['maxCpc'] / EXCHANGE_RATE; else $maxCpc = NULL;
			if (isset($someAdGroup['maxCpm'])) $maxCpm = $someAdGroup['maxCpm'] / EXCHANGE_RATE; else $maxCpm = NULL;
			if (isset($someAdGroup['maxContentCpc'])) $maxContentCpc = $someAdGroup['maxContentCpc'] / EXCHANGE_RATE; else $maxContentCpc = NULL;
			$adGroupObject = new AdGroup($someAdGroup['name'], $someAdGroup['id'], $someAdGroup['campaignId'], $maxCpc, $maxCpm, $maxContentCpc, $someAdGroup['status']);
			array_push($adGroupObjects, $adGroupObject);
		}
		return $adGroupObjects;
	}

	// this will not fail completely if only one adgroup fails
	// but will cause soap overhead
	function addAdGroupsOneByOne($adGroups) {
		// this is just a wrapper to the addAdGroup function
		$adGroupObjects = array();
		foreach ($adGroups as $adGroup) {
			$adGroupObject = addAdGroup($adGroup['name'], $adGroup['belongsToCampaignId'], $adGroup['status'], $adGroup['maxCpc'], $adGroup['maxCpm'], $adGroup['maxContentCpc']);
			array_push($adGroupObjects, $adGroupObject);
		}
		return $adGroupObjects;
	}

	function removeAdGroup(&$adGroupObject) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getAdGroupClient();
		$soapParameters = "<updateAdGroup>
													<changedData>
														<campaignId>".$adGroupObject->getBelongsToCampaignId()."</campaignId>
														<id>".$adGroupObject->getId()."</id>
														<status>Deleted</status>
													</changedData>
										  </updateAdGroup>";
		// remove the adgroup from the google servers
		$someSoapClient->call("updateAdGroup", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":removeAdGroup()", $soapParameters);
	    return false;
		}
		// delete remote calling object
		$adGroupObject = @$GLOBALS['adGroupObject'];
		unset($adGroupObject);
		return true;
	}

	function getAllAdGroups($campaignId) {
		global $soapClients;
		$someSoapClient = $soapClients->getAdGroupClient();
		$soapParameters = "<getAllAdGroups>
													<id>".$campaignId."</id>
											 </getAllAdGroups>";
		// query the google servers for all adgroups
		$allAdGroups = array();
		$allAdGroups = $someSoapClient->call("getAllAdGroups", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllAdGroups()", $soapParameters);
	    return false;
		}

		// when we have only one adgroup in the campaign return a (one adgroup element) array  anyway
		if (isset($allAdGroups['getAllAdGroupsReturn']['id'])) {
			$saveArray = $allAdGroups['getAllAdGroupsReturn'];
			unset($allAdGroups);
			$allAdGroups['getAllAdGroupsReturn'][0] = $saveArray;
		}
		// return only active (google servers call this 'enabled') or paused adgroups
		$allAdGroupObjects = array();
		if (isset($allAdGroups['getAllAdGroupsReturn'])) foreach($allAdGroups['getAllAdGroupsReturn'] as $adGroup) {
			if (($adGroup['status'] == "Enabled") || ($adGroup['status'] == "Paused")) {
				if (isset($adGroup['maxCpc'])) $maxCpc = $adGroup['maxCpc'] / EXCHANGE_RATE; else $maxCpc = NULL;
				if (isset($adGroup['maxCpm'])) $maxCpm = $adGroup['maxCpm'] / EXCHANGE_RATE; else $maxCpm = NULL;
				if (isset($adGroup['maxContentCpc'])) $maxContentCpc = $adGroup['maxContentCpc'] / EXCHANGE_RATE; else $maxContentCpc = NULL;
				$adGroupObject = new AdGroup ($adGroup['name'], $adGroup['id'], $adGroup['campaignId'], $maxCpc, $maxCpm, $maxContentCpc, $adGroup['status']);
				array_push($allAdGroupObjects, $adGroupObject);
			}
		}

		return $allAdGroupObjects;
	}

	function createAdGroupObject($givenAdGroupId) {
		// creates a local adgroup object
		global $soapClients;
		$someSoapClient = $soapClients->getAdGroupClient();
		// prepare soap parameters
		$soapParameters = "<getAdGroup>
													<id>".$givenAdGroupId."</id>
											 </getAdGroup>";
		// execute soap call
		$someAdGroup = $someSoapClient->call("getAdGroup", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createAdGroupObject()", $soapParameters);
	    return false;
		}

		// invalid ids are silently ignored. this is not what we want so put out a warning and return without doing anything.
		if (sizeOf($someAdGroup) == 0) {
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning: </b>Invalid AdGroup ID. No AdGroup with the ID ".$givenAdGroupId." found.";
			return false;
		}
		// populate class attributes
		$name = $someAdGroup['getAdGroupReturn']['name'];
		$id = $someAdGroup['getAdGroupReturn']['id'];
		$belongsToCampaignId = $someAdGroup['getAdGroupReturn']['campaignId'];
		// think in currency units
		if (isset($someAdGroup['getAdGroupReturn']['maxCpc'])) $maxCpc = $someAdGroup['getAdGroupReturn']['maxCpc'] / EXCHANGE_RATE; else $maxCpc = NULL;
		if (isset($someAdGroup['getAdGroupReturn']['maxCpm'])) $maxCpm = $someAdGroup['getAdGroupReturn']['maxCpm'] / EXCHANGE_RATE; else $maxCpm = NULL;
		if (isset($someAdGroup['getAdGroupReturn']['maxContentCpc'])) $maxContentCpc = $someAdGroup['getAdGroupReturn']['maxContentCpc'] / EXCHANGE_RATE; else $maxContentCpc = NULL;
		$status = $someAdGroup['getAdGroupReturn']['status'];
		// create object
		$adGroupObject = new AdGroup ($name, $id, $belongsToCampaignId, $maxCpc, $maxCpm, $maxContentCpc, $status);
		return $adGroupObject;
	}
?>
