<?
include("session.php");

if(isset($send))
{
# send leads
  if(trim($checkedleads)=='')
  {
    exit;
  }
  if(trim($emailto)=='1')
  {
    exit;
  }
  $leads=explode(",",$checkedleads);

  $c=1;

  foreach($leads as $lead)
  {
    $purchased=1;
    $rs=new mysql_recordset("select * from quotes where quote_id='$lead'");
    if($rs->rowcount()==0)
    {
      $rs=new mysql_recordset("select * from quotes_old where quote_id='$lead'");
    }
    $rs->fetch_array();

    $r_year=substr($rs->myarray["received"],0,4);
    $r_month=substr($rs->myarray["received"],4,2);
    $r_day=substr($rs->myarray["received"],6,2);
    $r_hour=substr($rs->myarray["received"],8,2);
    $r_min=substr($rs->myarray["received"],10,2);
    
    $p_area=substr($rs->myarray["phone_home"],0,3);
    $p_prefix=substr($rs->myarray["phone_home"],3,3);
    $p_suffix=substr($rs->myarray["phone_home"],6,4);
    
    if($rs->myarray["cat_id"]=='1')
    {
      $mycomments=$rs->myarray["comments"];
      $mycomments=str_replace('Vehicle Type: ','',$mycomments);
      $mycomments=str_replace('Vehicle Year: ','',$mycomments);
      $mycomments=str_replace('Vehicle Make: ','',$mycomments);
      $mycomments=str_replace('Vehicle Model: ','',$mycomments);
      $mycomments=str_replace('Comments:','',$mycomments);
      $details=explode("\n",$mycomments);
      $newcomments="<tr><td width='200'>Vehicle Type: </td><td>$details[0]</td></tr>
      <tr><td width='200'>Vehicle Year: </td><td>$details[1]</td></tr>
      <tr><td width='200'>Vehicle Make: </td><td>$details[2]</td></tr>
      <tr><td width='200'>Vehicle Model: </td><td>$details[3]</td></tr>
      <tr><td width='200'>-----</td><td></td></tr>
      <tr><td width='200'>Comments: </td><td>$details[5]</td></tr>
	  ";
    }

	elseif($rs->myarray["cat_id"]=='2')
	{
      $mycomments=$rs->myarray["comments"];
      $mycomments=str_replace('Type of Move: ','',$mycomments);
      $mycomments=str_replace('Type of Home: ','',$mycomments);
      $mycomments=str_replace('Furnished Rooms: ','',$mycomments);
      $mycomments=str_replace('Elevator: ','',$mycomments);
      $mycomments=str_replace('Stairs: ','',$mycomments);
      $mycomments=str_replace('Comments/Unique Items: ','',$mycomments);
      $details=explode("\n",$mycomments);
      $newcomments="<tr><td width='200'>Type of Move: </td><td>$details[0]</td></tr>
      <tr><td width='200'>Type of Home: </td><td>$details[1]</td></tr>
      <tr><td width='200'>Furnished Rooms: </td><td>$details[2]</td></tr>
      <tr><td width='200'>Elevator: </td><td>$details[3]</td></tr>
      <tr><td width='200'>Stairs: </td><td>$details[4]</td></tr>
      <tr><td width='200'>-----</td><td></td></tr>
      <tr><td width='200'>Comments/Unique Items :</td><td>$details[6]</td></tr>
	  ";
	}
    //$htmlcomments=str_replace("\n","<br>",$rs->myarray["comments"]);

    if($format=='csv')
    {
      if($c=='1')
      {
        $csv="Received,Name,EMail,Phone,Contact,MoveDate,OriginCity,OriginState,OriginZip,DestinationCity,DestinationState,Comments\n";
      }
      $csv.="$r_month/$r_day/$r_year $r_hour:$r_min".",".$rs->myarray["name"].",".str_replace(",",".",$rs->myarray["email"]).",".$rs->myarray["phone_home"].",".$rs->myarray["contact"].",".$rs->myarray["est_move_date"].",".$rs->myarray["origin_city"].",".$rs->myarray["origin_state"].",".$rs->myarray["origin_zip"].",".$rs->myarray["destination_city"].",".$rs->myarray["destination_state"].",".str_replace("\n"," | ",str_replace(",","",$rs->myarray["comments"]))."\n";
    }
    else
    {
      $email_parts[]="<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
	  <tr><td width='200'>Received: </td><td>$r_month/$r_day/$r_year $r_hour:$r_min</td></tr>
	  <tr><td width='200'>Customer Name: </td><td>".$rs->myarray["name"]."</td></tr>
	  <tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$rs->myarray["email"]."'>".$rs->myarray["email"]."</a></td></tr>
	  <tr><td width='200'>Phone Number: </td><td>$p_area-$p_prefix-$p_suffix</td></tr>
	  <tr><td width='200'>Preferred Contact Method: </td><td>".$rs->myarray["contact"]."</td></tr>
	  <tr><td width='200'>Estimated Move Date: </td><td>".$rs->myarray["est_move_date"]."</td></tr>
	  <tr><td width='200'>Moving From: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["origin_city"]."&state=".$rs->myarray["origin_state"]."&zipcode=".$rs->myarray["origin_zip"]."&country=US&cid=lfmaplink'>".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."</a></td></tr>
	  <tr><td width='200'>Moving To: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["destination_city"]."&state=".$rs->myarray["destination_state"]."&country=US&cid=lfmaplink'>".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."</a></td></tr>
	  <tr><td width='200'><a href='http://www.mapquest.com/directions/main.adp?1c=".$rs->myarray["origin_city"]."&2c=".$rs->myarray["destination_city"]."&1s=".$rs->myarray["origin_state"]."&2s=".$rs->myarray["destination_state"]."&1z=".$rs->myarray["origin_zip"]."&1y=US&2y=US&cid=lfddlink'>Driving Directions</a></td><td></td></tr>
	  <tr><td width='200'>-----</td><td></td></tr>
	  $newcomments
	  </table>
	  ";
      
	  //Received: $r_month/$r_day/$r_year $r_hour:$r_min<br><br>Customer Name: ".$rs->myarray["name"]."<br>E-Mail Address: <a href='mailto:".$rs->myarray["email"]."'>".$rs->myarray["email"]."</a><br>Phone Number: $p_area-$p_prefix-$p_suffix<br>Preferred Contact Method: ".$rs->myarray["contact"]."<br>Estimated Move Date: ".$rs->myarray["est_move_date"]."<br><br>Moving From: <a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["origin_city"]."&state=".$rs->myarray["origin_state"]."&zipcode=".$rs->myarray["origin_zip"]."&country=US&cid=lfmaplink'>".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."</a><br>
	  //Moving To: <a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["destination_city"]."&state=".$rs->myarray["destination_state"]."&country=US&cid=lfmaplink'>".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."</a><br><a href='http://www.mapquest.com/directions/main.adp?1c=".$rs->myarray["origin_city"]."&2c=".$rs->myarray["destination_city"]."&1s=".$rs->myarray["origin_state"]."&2s=".$rs->myarray["destination_state"]."&1z=".$rs->myarray["origin_zip"]."&1y=US&2y=US&cid=lfddlink'>Driving Directions</a><br><br>$htmlcomments<br><br>(*All information based on user input*)";
      //$email_parts[]="received: ".$rs->myarray["received"]."\n"."name: ".$rs->myarray["name"]."\n"."email: ".$rs->myarray["email"]."\n"."phone: ".$rs->myarray["phone_home"]."\n"."fax: ".$rs->myarray["phone_fax"]."\n"."contact: ".$rs->myarray["contact"]."\n"."move date: ".$rs->myarray["est_move_date"]."\n"."origin city: ".$rs->myarray["origin_city"]."\n"."origin state: ".$rs->myarray["origin_state"]."\n"."origin country: ".$rs->myarray["origin_country"]."\n"."dest city: ".$rs->myarray["destination_city"]."\n"."dest state: ".$rs->myarray["destination_state"]."\n"."dest country: ".$rs->myarray["destination_country"]."\n"."comments: ".$rs->myarray["comments"];
    }
    $c++;
  }

  if($format=='csv')
  {
    # create csv file
    $filename="/home/httpd/vhosts/secure.irelocation.com/httpsdocs/tmp/".time().".csv";
    $fp=fopen($filename,"w");
    fputs($fp,$csv);
    fclose($fp);

    # email address is $emailto

    $mailtext = "Leads Attached!";

    include('/home/httpd/vhosts/secure.irelocation.com/httpsdocs/htmlMimeMail.php');
    $mail = new htmlMimeMail();
    //$mail->setCrlf("\n");
    $attachment = $mail->getFile($filename);
    $mail->setText($mailtext);
    $mail->addAttachment($attachment, 'directleads.csv', 'text/csv');
    $mail->setReturnPath('no_reply@1stmovingdirectory.com');
    $mail->setFrom('"1st Moving Directory" <no_reply@1stmovingdirectory.com>');
    $mail->setSubject('Leads - '.date("m-d-Y"));
    $mail->setHeader('X-Mailer', 'IRELO-MAIL');
    $result = $mail->send(array($emailto));

    $mail->setSubject('[COPY] Leads - '.date("m-d-Y")." ".$user["name"]);
    $mail->setText("Sent to $emailto\n".$mailtext);    
    $result = $mail->send(array("blog@irelocation.com"));

    # delete the csv files
    unlink($filename);
  }
  elseif($format=='separate')
  { 
    $s=0;
    foreach($email_parts as $email_part)
    {
      mail($emailto,"Lead - ".date("m-d-Y")."",$email_part,"From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"]."\nContent-Type: text/html; charset=iso-8859-15");
      $s++;
    }
    mail("blog@irelocation.com","[COPY] Lead - ".date("m-d-Y")." ".$user["name"],"$s leads sent to $emailto",'From: "1st Moving Directory" <no_reply@1stmovingdirectory.com>');
  }
  elseif($format=='together')
  {
    foreach($email_parts as $email_part)
    {
      $email.=$email_part."\r\n###################################\r\n";
    }
    mail($emailto,"Leads - ".date("m-d-Y")."",$email,"From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"]."\nContent-Type: text/html; charset=iso-8859-15");
    mail("blog@irelocation.com","[COPY] Lead - ".date("m-d-Y")." ".$user["name"],$email,'From: "1st Moving Directory" <no_reply@1stmovingdirectory.com>');
  }
# create javascript to close window
?>
<html>
  <head>
    <title>Close Window</title>
  </head>
  <SCRIPT LANGUAGE="JAVASCRIPT">
    function refreshParentAndExit() {
      //opener.location.reload(true)
      self.close()
    }
  </SCRIPT>
  <body bgcolor="#000000" onload="refreshParentAndExit();"> </body>
</html>
<?
}
else
{
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Email Leads</title>
</head>

<body>

<p><b><font face="verdana" color="#333333">Email Leads</font></b></p>
<p><font face="verdana" size="2" color="#333333">Please verify your
email address.&nbsp; If you would like to send to multiple emails, simply
separate each email by a comma ( , ).</font></p>
<form method="POST" action="1md_quotes_email.php" name="email_leads">
<table border="0" cellpadding="0" cellspacing="0" width="400">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellpadding="0" cellspacing="1" width="100%" height="82">
      <tr>
        <td width="33%" bgcolor="#DCDCDC" valign="top" height="29">
        <font face="verdana" color="#333333" size="2"><b>&nbsp;</b>Email
        To:</font></td>
        <td width="67%" bgcolor="#DCDCDC" height="29" align="center">
        <input type="text" name="emailto" value="<?echo $emailto;?>" size="35" style="font-size: 11px; background: #F5F5F5; border-width : 1 1 1 1; border-color : #95B3D0 #95B3D0 #95B3D0 #95B3D0;"></td>
      </tr>
      <tr>
        <td width="33%" bgcolor="#BCCFE2" valign="top" height="19">
        <font face="verdana" color="#333333" size="2">&nbsp;Lead
        Format:</font></td>
        <td width="67%" bgcolor="#BCCFE2" height="19" align="center">
        <table border="0" cellpadding="0" cellspacing="0" width="220">
          <tr>
            <td width="19">
            <input type="radio" value="csv" name="format" checked></td>
            <td width="201"><font face="verdana" size="2" color="#333333">&nbsp;Send
            As Attachment (.csv)</font></td>
          </tr>
          <tr>
            <td width="19"><input type="radio" value="separate" name="format"></td>
            <td width="201">&nbsp;<font face="verdana" size="2" color="#333333">Send
            Separately</font></td>
          </tr>
          <tr>
            <td width="19"><input type="radio" value="together" name="format"></td>
            <td width="201">&nbsp;<font face="verdana" size="2" color="#333333">Send
            Together</font></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="33%" bgcolor="#EAEDF4" valign="top" height="31">&nbsp;</td>
        <td width="67%" bgcolor="#EAEDF4" height="31" align="center">
        <input type="submit" value="Send Email(s) Now" name="submit" style="font-size: 11px; background: #F5F5F5; border-width : 1 1 1 1; border-color : #95B3D0 #95B3D0 #95B3D0 #95B3D0;"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<input type="hidden" name="send" value="1">
<input type="hidden" name="checkedleads" value="<?echo $checkedleads;?>">
</form>
</body>

</html>
<?
}
?>
