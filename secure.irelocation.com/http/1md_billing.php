<?
$today=date("Y-m-d");
$sql="select r.next,c.comp_name,r.description,r.amount,r.status from company as c, billing_rules as r where c.comp_id=r.comp_id and r.end>='$today' and r.active='1' order by next";

//$sql2="select b.date as next,c.comp_name,b.description,b.amount,b.status from company as c, billing as b where c.comp_id=b.comp_id and date>='$today' order by c.comp_name,b.date,b.amount desc;";

$sql2="select b.date as next,c.comp_name,b.description,b.amount,b.status from company as c, billing as b where c.comp_id=b.comp_id and date>='$today' order by b.date desc;";

//$sql3="select c.comp_name,b.date,b.description,b.status,b.comp_id,b.amount from company as c, billing as b where c.comp_id=b.comp_id and date<='$today' order by c.comp_name,b.date,b.amount desc;";

$sql3="select c.comp_name,b.date,b.description,b.status,b.comp_id,b.amount from company as c, billing as b where c.comp_id=b.comp_id and date<='$today' and status='received' order by b.date desc;";

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Billing Summary</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<p><font face="verdana" color="#2360A5" size="3"><b>Billing Summary</b></font></p>
<table border="0" cellspacing="0" width="800" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="14">
          <font face="verdana" size="3" color="#FFFFFF"><b>&nbsp;Current Billing</b></font>
        </td>
      </tr>
      <tr>
        <td width="10%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">date</font></b></td>
        <td width="30%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">company</font></b></td>
        <td width="35%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">description</font></b></td>
        <td width="10%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">amount</font></b></td>
        <td width="20%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">status</font></b></td>
      </tr>
      <?
      $rs=new mysql_recordset($sql);
      while($rs->fetch_array())
      {
        $billing[]=$rs->myarray;
      }

      $rs=new mysql_recordset($sql2);
      while($rs->fetch_array())
      {
        $billing[]=$rs->myarray;
      }

      sort($billing);
      reset($billing);

      foreach($billing as $mybilling)
      {
      ?>
      <tr>
        <td width="10%" class="quote_col_o">&nbsp;<?echo substr($mybilling["next"],-5)."-".substr($mybilling["next"],0,4);?></td>
        <td width="30%" class="quote_col_s">&nbsp;<?echo $mybilling["comp_name"];?></td>
        <td width="35%" class="quote_col_s">&nbsp;<?echo $mybilling["description"];?></td>
        <td width="10%" class="quote_col_x">&nbsp;$<?echo $mybilling["amount"];?></td>
        <td width="20%" class="quote_col_d">&nbsp;<?if($mybilling["status"]=='new') echo "Auto-Billing"; else echo "Collect Payment!";?></td>
      </tr>
      <?
      }
      ?>
    </table>
   </td>
 </tr>
</table>
<br>
<table border="0" cellspacing="0" width="800" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="14">
          <font face="verdana" size="3" color="#FFFFFF"><b>&nbsp;Past Billing</b></font>
        </td>
      </tr>
      <tr>
        <td width="10%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">date</font></b></td>
        <td width="30%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">company</font></b></td>
        <td width="35%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">description</font></b></td>
        <td width="10%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">amount</font></b></td>
        <td width="20%" bgcolor="#DCDCDC" align="center"><b><font color="#000000">status</font></b></td>
      </tr>
      <?
      $rs=new mysql_recordset($sql3);

      while($rs->fetch_array())
      {
      ?>
      <tr>
        <td width="10%" class="quote_col_o">&nbsp;<?echo substr($rs->myarray["date"],-5)."-".substr($rs->myarray["date"],0,4);?></td>
        <td width="30%" class="quote_col_s">&nbsp;<?echo $rs->myarray["comp_name"];?></td>
        <td width="35%" class="quote_col_s">&nbsp;<?echo $rs->myarray["description"];?></td>
        <td width="10%" class="quote_col_x">&nbsp;$<?echo $rs->myarray["amount"];?></td>
        <td width="20%" class="quote_col_d">&nbsp;<?if($rs->myarray["status"]=='new') echo "<b>Processing</b>"; else echo ucwords($rs->myarray["status"]);?></td>
      </tr>
      <?
      }
      ?>
    </table>
   </td>
 </tr>
</table>
</body>

</html>