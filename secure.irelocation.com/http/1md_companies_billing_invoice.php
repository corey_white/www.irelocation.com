<?
#
#
#
if(!$comp_id) exit;
#
#
#

include_once("mysql.php");
include_once("inc_pdf.php");


// clean up pdf directory
$dh = opendir("pdf/");
while (false !== ($file = readdir($dh)))
{
 if ($file != "." && $file != "..")
 {
   if(substr($file,5,6)!=strrev(date("dmy")))
   {
     //echo $file;
     unlink("pdf/".$file);
   }
 }
}
closedir($dh);



$mylist=explode(",",$list);
foreach($mylist as $bill)
{
  if($bill_id[$bill]=='yes')
  {
    $sql.="id='$bill' or ";
  }
}
$sql=substr($sql,0,-4);
if(strlen($sql)!=0)
{
  $rs=new mysql_recordset("select * from billing where comp_id='$comp_id' and ($sql) order by date");
  while($rs->fetch_array())
  {
    $transaction[]=$rs->myarray;
  }
}

$rs=new mysql_recordset("select * from company where comp_id='$comp_id'");
$rs->fetch_array();
$company=$rs->myarray;

$invoice_number=strrev(date("Hisdmy"));
$invoice_date=date("m/d/y");

//set up document
$p=new pdf("8.5","11","PDF Generator","Invoice $invoice_number","Invoice $invoice_date","pdf/$comp_id-$invoice_number.pdf");
$p->openpdf("blank_invoice.pdf","11");

//add address information
$p->setfont("10","Helvetica","000000");
$p->addtext($billing_name,50,185);
$p->addtext($company["comp_name"],50,197);
$p->addtext($company["address1"],50,209);
$p->addtext($company["city"].", ".$company["state"]." ".$company["zip"],50,221);

$p->addtext($invoice_number,475,93);
$p->addtext($invoice_date,475,106);
$p->addtext($imessage,402,146);

$p->setfont("9","Helvetica","FF0000");
$p->addtext($payments,515,330); //payments & credits
$p->setfont("9","Helvetica","000000");
$p->addtext($previous,515,318); //previous balance
$p->addtext($charges,515,342); //charges
$p->addtext($balance,515,355); //current balance

//transactions... we will loop this!  Increment 14 to each height
$c=0;
if(is_array($transaction))
{
  foreach($transaction as $tran)
  {
    $p->addtext($tran["date"]."   ".$tran["description"],90,430+(14*$c)); // date / desc
    if($tran["amount"]<0) $tran["amount"]="-\$".abs($tran["amount"]);
    $p->addtext($tran["amount"],515,430+(14*$c));  // total
    $c++;
  }
}
else
{
  $p->addtext($tran["date"]."   ".$tran["description"],90,430); // date / desc
  if($tran["amount"]<0) $tran["amount"]="-\$".abs($tran["amount"]);
  $p->addtext($tran["amount"],515,430);  // total
}

$p->addtext($charges,515,703);           // sub total

$p->setfont("12","Helvetica","000000");
$p->addtext($balance,515,730);  // grand total  (sub total plus previous balance)
$p->addtext($balance,475,134);  // grand total

$p->save_pdf();
header("location: pdf/$comp_id-$invoice_number.pdf");
?>