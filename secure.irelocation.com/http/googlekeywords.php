<?

function get_state_name($state_abrv)
{
  switch($state_abrv)
  {
    case "AK": $mystate="Alaska"; break;
    case "AL": $mystate="Alabama"; break;
    case "AR": $mystate="Arkansas"; break;
    case "AZ": $mystate="Arizona"; break;
    case "CA": $mystate="California"; break;
    case "CO": $mystate="Colorado"; break;
    case "CT": $mystate="Connecticut"; break;
    case "DC": $mystate="Washington D.C."; break;
    case "DE": $mystate="Delaware"; break;
    case "FL": $mystate="Florida"; break;
    case "GA": $mystate="Georgia"; break;
    case "HI": $mystate="Hawaii"; break;
    case "IA": $mystate="Iowa"; break;
    case "ID": $mystate="Idaho"; break;
    case "IL": $mystate="Illinois"; break;
    case "IN": $mystate="Indiana"; break;
    case "KS": $mystate="Kansas"; break;
    case "KY": $mystate="Kentucky"; break;
    case "LA": $mystate="Louisiana"; break;
    case "MA": $mystate="Massachusetts"; break;
    case "MD": $mystate="Maryland"; break;
    case "ME": $mystate="Maine"; break;
    case "MI": $mystate="Michigan"; break;
    case "MN": $mystate="Minnesota"; break;
    case "MO": $mystate="Missouri"; break;
    case "MS": $mystate="Mississippi"; break;
    case "MT": $mystate="Montana"; break;
    case "NC": $mystate="North Carolina"; break;
    case "ND": $mystate="North Dakota"; break;
    case "NE": $mystate="Nebraska"; break;
    case "NH": $mystate="New Hampshire"; break;
    case "NJ": $mystate="New Jersey"; break;
    case "NM": $mystate="New Mexico"; break;
    case "NV": $mystate="Nevada"; break;
    case "NY": $mystate="New York"; break;
    case "OH": $mystate="Ohio"; break;
    case "OK": $mystate="Oklahoma"; break;
    case "OR": $mystate="Oregon"; break;
    case "PA": $mystate="Pennsylvania"; break;
    case "RI": $mystate="Rhode Island"; break;
    case "SC": $mystate="South Carolina"; break;
    case "SD": $mystate="South Dakota"; break;
    case "TN": $mystate="Tennessee"; break;
    case "TX": $mystate="Texas"; break;
    case "UT": $mystate="Utah"; break;
    case "VA": $mystate="Virginia"; break;
    case "VT": $mystate="Vermont"; break;
    case "WA": $mystate="Washington"; break;
    case "WI": $mystate="Wisconsin"; break;
    case "WV": $mystate="West Virginia"; break;
    case "WY": $mystate="Wyoming"; break;
  }
  return $mystate;
}

	include 'mysql.php';
	extract($_POST);
	if (strlen($limit) > 0 && $limit != "all")
		$limit_string = " limit $limit";
	else
		$limit_string = "";
	
	if (($_POST["locationtype"] == "city") || ($_POST["locationtype"] == "cs") || ($_POST["locationtype"] == "csa"))
		$sql = "select distinct(city) as location, state as state_abrv from zip_codes where city > 'a' and CHAR_LENGTH(city) < 20 order by city $limit_string;";		
	else
		$sql = "select distinct(zip) as location, state as state_abrv from zip_codes where CHAR_LENGTH(zip) = 5 order by zip $limit_string;";		
	
	$header = "Campaign\tCampaign Daily Budget(USD)\tAd Group\tReport Type\tMax Content CPC(USD)\tMax CPC(USD)\tMinimum Bid(USD)\tKeyword Type\tKeyword\tHeadline\tDescription Line 1\tDescription Line 2\tDisplay URL\tDestination URL\tCampaign Status\tAdGroup Status\tCreative Status\tKeyword Status\n";
	
	function addNegative($keywords,$campaign="Top Security Companies",$budget=75,$status="Paused")
	{
		$line = "$campaign\t$budget\tKeyword\t\t\t";
		
		if (strpos($keywords," ") > 0)
			$line .= "Campaign Negative Phrase\t";
		else
			$line .= "Campaign Negative\t";
			
		$line .= $keywords."\t\t\t\t\tPaused\t\t\t\n";
		
		return $line;
	}
	
	function addAdGroup($adGroupName,$maxcpc,$headline,$desc1,$desc2,$displayUrl="www.TopSecurityCompanies.com",$destUrl="http://www.TopSecurityCompanies.com/?source=google",$campaign="Top Security Companies", $budget=75)
	{
		return "$campaign\t$budget\t$adGroupName\tAd Text\t0\t$maxcpc\t\t\t\t$headline\t$desc1\t$desc2\thttp://$displayUrl\thttp://$destUrl\tPaused\tActive\tActive\t\t\n";
	}

	function addKeyword($adGroupName,$keyword,$campaign="Top Security Companies", $budget=75)
	{
		return "$campaign\t$budget\t$adGroupName\tKeyword\t\t\t0.05\tBroad\t$keyword\t\t\t\t\t\tPaused\tActive\t\tActive\n";	
	}

	header("Content-type: application/octet-stream");

	# replace excelfile.xls with whatever you want the filename to default to
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	echo $header;
	
	echo addAdGroup($adgroupname,$maxcpc,$headline,$description1,$description2,$displayurl,$destinationurl,$account,$budget);
	
	$rs = new mysql_recordset($sql);
	while ($rs->fetch_array())
	{
		
		$location = $rs->myarray["location"];
		$state_abrv = $rs->myarray["state_abrv"];
				
		if ($_POST["locationtype"] == "cs") { //city and state
		
		$mystate = get_state_name($state_abrv);
		
			if ($city_pos == "before")
				$keywords = "$mystate $location $keyword ";
			else 
				$keywords = "$keyword $location $mystate";
			
		} else if ($_POST["locationtype"] == "csa") { //city and state abrv
		
			if ($city_pos == "before")
				$keywords = "$state_abrv $location $keyword ";
			else 
				$keywords = "$keyword $location $state_abrv";
			
		} else {
		
			if ($city_pos == "before")
				$keywords = "$location $keyword ";
			else 
				$keywords = "$keyword $location";			
		}
		
		echo addKeyword($adgroupname,$keywords,$account,$budget);
	}
?>