<?
include("mysql.php");

//echo "<pre>";

//Get current day billings
$rs=new mysql_recordset("select distinct(comp_id),sum(amount) as mysum from billing where date='".date("Y-m-d")."' group by comp_id");
while($rs->fetch_array())
{
  if($rs->myarray["mysum"]>0)
  {
    $sql="select 
       c.comp_id,
	b.amount,
	b.description,
	b.status,
	c.comp_name,
	c.contact_name,
	c.phone_num,
	c.phone_ext
	
from 
       billing as b, 
       company as c 
where 
	c.comp_id=b.comp_id 
	and c.comp_id='".$rs->myarray["comp_id"]."'
	and b.date='".date("Y-m-d")."'";
    $rs2=new mysql_recordset($sql);
    while($rs2->fetch_array())
    {
      if($rs2->myarray["status"]=='processed')
      {
        $today_invoice[]=$rs2->myarray;
      }
      else
      {
        $today_failed[]=$rs2->myarray;
      }
    }
  }
}

//Get the list of outstanding companies with billing info
$sql="select 
	distinct(c.comp_name),
	sum(b.amount) as mysum,
	c.comp_id,
	c.contact_name,
	c.phone_num,
	c.phone_ext,
	c.city,
	c.state,
	c.active,
	c.url,
	c.contact_email
from 
	company as c,
	billing as b 
where 
	c.comp_id=b.comp_id and 
	b.status!='new' and 
	b.date<='".date("Y-m-d")."'
group by 
	c.comp_id 
order by 
	c.comp_name;";

$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{
  if($rs->myarray["mysum"]>0)
  {
    $outstanding[]=$rs->myarray;
  }
}


$i=1;
if(is_array($today_failed))
{
  $text=date("m-d-Y")."\n\nFAILED TRANSACTIONS TODAY:\n";
  foreach($today_failed as $failed)
  {
    $text.="".$i++.".  \$".$failed["amount"]." for ".$failed["description"]."
    ".$failed["comp_id"].": ".$failed["comp_name"]." - ".$failed["contact_name"]." ".$failed["phone_num"]." ".$failed["phone_ext"]."\n\n";
  }
}

$i=1;
if(is_array($today_invoice))
{
  $text.="PAYMENTS DUE TODAY:\n";
  foreach($today_invoice as $invoice)
  {
    $text.="".$i++.".  \$".$invoice["amount"]." for ".$invoice["description"]."
    ".$invoice["comp_id"].": ".$invoice["comp_name"]." - ".$invoice["contact_name"]." ".$invoice["phone_num"]." ".$invoice["phone_ext"]."\n\n";
  }
}

$text.="OUTSTANDING:\n";
foreach($outstanding as $due)
{
  $rs=new mysql_recordset("select * from contact where comp_id='".$due["comp_id"]."'");
  $rs->fetch_array();
  $notes=$rs->myarray["contact"];
  
  $text.="___________________________________________________________
".$due["comp_name"]." - ".$due["contact_name"]." ".$due["phone_num"]." ".$due["phone_ext"]."
".$due["comp_id"].": \$".$due["mysum"]." Outstanding!

Email: ".$due["contact_email"]."
Notes:  
$notes\n\n";
}

mail("adrian@irelocation.com","Daily billing reminders",$text,"From: billing@irelocation.com");
mail("katrina@irelocation.com","Daily billing reminders",$text,"From: billing@irelocation.com");
mail("vsmith@irelocation.com","Daily billing reminders",$text,"From: billing@irelocation.com");

//echo "</pre>";
?>