<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Tools</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>
<body>
<img src="https://secure.overture.com/images/logo/ysm.gif" title="Overture" /><br/>
Fill out these fields to create Overture Keywords <Br/>
based on the Cities or Zip codes in our database.
<hr width="50%" align="left"/>
<form action="overturekeywords2.php" method="post" >
<table>
 	<tr><th align='left'>File Name</th> <td align='left'><input name="filename" class='quote_box' value="<?php echo $filename; ?>" type="text" size="20" />.xls</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
	  <th align='left'>Campaign </th> 
	  <td align='left'><input name="campaign" class='quote_box' value="<?php echo $campaign; ?>" type="text" size="20" maxlength="20" /></td>
	</tr>
	<tr>
	  <th align='left'>Campaign ID</th> 
	  <td align='left'><input name="campaignid" class='quote_box' value="<?php echo $campaignid; ?>" type="text" size="20" maxlength="20" /></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
	  <th align='left'>Ad Group </th> 
	  <td align='left'><input name="adgroup" class='quote_box' value="<?php echo $adgroup; ?>" type="text" size="20" maxlength="20" /></td>
	</tr>
	<tr><td colspan="2">&nbsp;</td></tr>
	<tr>
	  <th align='left'>Ad </th> 
	  <td align='left'><input name="ad" class='quote_box' value="<?php echo $ad; ?>" type="text" size="20" maxlength="20" /></td>
	</tr>
	<tr><th align='left'>Title</th><td align='left'><input name="title" class='quote_box' value="<?php echo $title; ?>" type="text" />* leave blank to copy search term.</td></tr>
    <tr><th align='left'>Short Description</th> <td align='left'><input name="shortdescription" class='quote_box' value="<?php echo $shortdescription; ?>"  size="40" type="text" /></td></tr>
	<tr><th align='left'>Long Description</th> <td align='left'><input name="longdescription" class='quote_box' value="<?php echo $longdescription; ?>"  size="40" type="text" /></td></tr>
	
	<tr><th align='left'>Display URL</th><td align='left'><input name="displayurl" class='quote_box' value="<?php echo $displayurl; ?>" type="text" /></td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>
    <tr>
		<th align='left'>City Position </th> 
		<td align='left'>
			<input type="radio" name="city_pos" class='quote_box' value="before" <?php if ($city_pos == "before") echo "checked"; ?> >Before <input type="radio" name="city_pos" class='quote_box' value="after" <?php if ($city_pos == "after") echo "checked"; ?> >After
		</td>
	</tr>
	<tr>
		<th align='left'>Search Term </th> 
		<td align='left'>
			<input name="search_term" class='quote_box' value="<?php echo $search_term; ?>" type="text" />
			<select name="locationtype" >
				<option value="city">in Cities</option>
				<option value="zip">in ZipCodes</option>
				<option value="cs">City and State</option>
				<option value="csa">City and State Abrv</option>				
			</select>
		</td>
	</tr>	
    <tr><th align='left'>URL</th><td align='left'>http://<input name="url" class='quote_box' value="<?php echo $url; ?>" type="text" size="40" /></td></tr>    
	<tr><td colspan="2">&nbsp;</td></tr>    
    <tr><th align='left'>Standard Bid ($)</th><td align='left'><input name="standardbid" class='quote_box' value="<?php echo $standardbid; ?>" type="text" size="5" maxlength="5" /></td></tr>	
	<tr><th align='left'>Standard Bid Limit</th><td align='left'><input name="standardbidlimit" class='quote_box' value="<?php echo $standardbidlimit; ?>" type="text" size="5" maxlength="5" /></td></tr>	
	<tr><th align='left'>Standard Bid Status</th><td align='left'><input name="standardbidstatus" class='quote_box' type="checkbox" value="on" /> on</td></tr>	
	<tr><td colspan="2">&nbsp;</td></tr>    
    <tr><th align='left'>Content Bid ($)</th><td align='left'><input name="contentbid" class='quote_box' value="<?php echo $contentbid; ?>" type="text" size="5" maxlength="5" /></td></tr>	
	<tr><th align='left'>Content Bid Limit</th><td align='left'><input name="contentbidlimit" class='quote_box' value="<?php echo $contentbidlimit; ?>" type="text" size="5" maxlength="5" /></td></tr>	
	<tr><th align='left'>Content Bid Status</th><td align='left'><input name="contentbidstatus" class='quote_box' type="checkbox" value="on" /> on</td></tr>
	<tr><td colspan="2">&nbsp;</td></tr>    
	<tr>
		<th align='left'>Watch List</th> 
		<td align='left'>
			<input type="radio" name="watchlist" class='quote_box' value="on" <?php if ($watchlist == "on") echo "checked"; ?> >on <input type="radio" name="watchlist" class='quote_box' value="off" <?php if ($city_pos == "off" || $city_pos == "") echo "checked"; ?> >off
		</td>
	</tr>
	<tr><th align='left'>Keyword Limit </th><td align='left'><input name="limit" size="5" class='quote_box' value="<?php echo $limit; ?>" type="text" />* enter "all" to get all keywords.</td></tr>

 	<Tr><td colspan="2"><input type="submit" name='go' class='quote_box' value="Submit"/></td></Tr>
	
</table>
</form>

</body></html>