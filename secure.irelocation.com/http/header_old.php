<?
include("session.php");

#
# Get current daily lead counts
#
$rs=new mysql_recordset("select cat_id,count(*) as mycount from quotes where left(received,8)='".date("Ymd")."' group by cat_id order by cat_id");
while($rs->fetch_array())
{
  $lead_count[$rs->myarray["cat_id"]]=$rs->myarray["mycount"];
}

#
# Get time until next update
#
$update_quotemailer_m=9-substr(date("i"),-1);
$update_quotemailer_s=60-date("s");
$update_billing_m=74-date("i");
$update_billing_s=60-date("s");
if($update_billing_m>=60){$update_billing_m=$update_billing_m-60;}

?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta http-equiv="Refresh" content="300;URL=header.php">
<title>iReloControl</title>
<link rel="stylesheet" href="style.css" type="text/css">
<base target="content">
<script language="JavaScript">
<!--
var timerID = null;
var qs;var qm;var cs;var cm;
qm=<?echo $update_quotemailer_m;?>;
qs=<?echo $update_quotemailer_s;?>;
cm=<?echo $update_billing_m;?>;
cs=<?echo $update_billing_s;?>;
function showtime(num1,num2,num3,num4){
  qm=num1;  qs=num2;  cm=num3;  cs=num4;
  if(qm==0 && qs==0){qm=10;}
  if(cm==0 && cs==0){cm=60;}
  if(qs==0){qs=60;qm--;}
  if(cs==0){cs=60;cm--;}
  qs--;cs--;
  if(qs<10){qLocalTimer.innerHTML = qm + ":0" + qs + " Minutes";}
  else{qLocalTimer.innerHTML = qm + ":" + qs + " Minutes";}
  if(cs<10){cLocalTimer.innerHTML = cm + ":0" + cs + " Minutes";}
  else{cLocalTimer.innerHTML = cm + ":" + cs + " Minutes";}
  setTimeout("showtime(qm,qs,cm,cs)",1000);}
//-->
</script>
</head>

<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" onload="return showtime(qm,qs,cm,cs)">
<table border="0" cellspacing="0" width="100%" cellpadding="0" background="images/topbg.gif" height="136">
  <tr>
    <td width="590">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="136">
      <tr>
        <td width="100%" valign="top" height="65">
        <img border="0" src="images/logo.gif" width="176" height="44"></td>
      </tr>
      <tr>
        <td width="100%" valign="top" height="15" class="text1" valign="bottom">
        www.1stmovingdirectory.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        www.carshipping.com&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        server tools</td>
      </tr>
      <tr>
        <td width="100%" height="56"><a href="control.php?action=1md_companies"><img border="0" src="images/icon_companies.gif" width="57" height="56"></a><a href="control.php?action=1md_quotes"><img border="0" src="images/icon_quotes.gif" width="56" height="56"></a><a href="control.php?action=1md_referrer"><img border="0" src="images/icon_affiliates.gif" width="56" height="56"></a><a href="control.php?action=1md_billing"><img border="0" src="images/icon_billing.gif" width="57" height="56"></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="control.php?action=cs_companies"><img border="0" src="images/icon_companies.gif" width="57" height="56"></a><a href="control.php?action=cs_log"><img border="0" src="images/icon_log.gif" width="56" height="56"></a><a href="control.php?action=cs_billing"><img border="0" src="images/icon_billing.gif" width="57" height="56"></a>&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="http://email.spiderwt.com/index.php" target="_blank"><img border="0" src="images/icon_webmail.gif" width="57" height="56"></a><a href="control.php?action=tracker"><img border="0" src="images/icon_tracker.gif" width="56" height="56"></a><a href="https://secure.irelocation.com:8443" target="_blank"><img border="0" src="images/icon_server.gif" width="56" height="56"></a><a href="control.php?action=database"><img border="0" src="images/icon_database.gif" width="56" height="56"></a></td>
      </tr>
    </table>
    </td>
    <td>&nbsp;</td>
    <td width="200">
    <table border="0" cellspacing="0" width="100%" cellpadding="0">
      <tr>
        <td width="82%" class="text1">&nbsp;</td>
        <td width="15%" class="text2">&nbsp;</td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
      <tr>
        <td width="82%" class="text1">
        <p class="heading">Daily Lead Count</td>
        <td width="15%" class="text2">&nbsp;</td>
        <td width="3%" class="text2"><b>&nbsp;</b></td>
      </tr>
      <tr>
        <td width="82%" class="text1">Auto Transport Quotes:</td>
        <td width="15%" class="text2"><?echo intval($lead_count[1]);?></td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
      <tr>
        <td width="82%" class="text1">Full Service Moving
        Quotes:</td>
        <td width="15%" class="text2"><?echo intval($lead_count[2]);?></td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
      <tr>
        <td width="82%" class="text1">&nbsp;</td>
        <td width="15%" class="text2">&nbsp;</td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
    </table>
    <table border="0" cellspacing="0" width="100%" cellpadding="0">
      <tr>
        <td width="52%">
        <p class="heading">Website
        Updates</td>
        <td width="45%" class="text2">&nbsp;</td>
        <td width="3%" class="text2"><b>&nbsp;</b></td>
      </tr>
      <tr>
        <td width="52%" class="text1">Quote Mailer:</td>
        <td width="45%" align="right" class="text2">
        <span ID="qLocalTimer">LOADING</span></td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
      <tr>
        <td width="52%" class="text1">Auto Billing:</td>
        <td width="45%" align="right" class="text2">
        <span ID="cLocalTimer">LOADING</span></td>
        <td width="3%" class="text2">&nbsp;</td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td colspan="3"><img src="images/line.gif"></td>
  </tr>
</table>
</body>

</html>