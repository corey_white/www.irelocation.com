<?
include("mysql.php");

$errmsg='';
//Error check: validate that zip code is 5 digits
if(strlen($_POST['zip']<5)) {
	$errmsg="Please Enter a valid 5 digit zip code";
}

if($_POST['zip']!='' && $_POST['miles']!='' && $errmsg=='') {
	//Setting the current year and month in preparation of the monthly report later on
	$temptime=date('Ym',time());
	$myyear=substr($temptime,0,4);
	$mymonth=substr($temptime,4,2);
	
	//Select Coordinate information for posted zip code
	$sql="select z.latitude,z.longitude from vacationhomes.zip_codes z where zip = '".$_POST['zip']."'";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array()) {
		//Math in preparation for the next query of finding zip codes in a given radius
		$distance = "(69.172 * SQRT(((".$rs->myarray['latitude']."-z.latitude)*(".$rs->myarray['latitude']."-z.latitude))+
		((".$rs->myarray['longitude']."-z.longitude)*(".$rs->myarray['longitude']."-z.longitude))))";
	}
	
	//This selects any zip codes that fall within the radius of the posted zip code, using the posted mileage
	$sql2="select z.zip,z.city,s.fullstate from vacationhomes.zip_codes as z, vacationhomes.states as s 
	where s.state_id=z.state and ($distance <= ".$_POST['miles'].") order by z.city asc, z.zip asc";
	$rs2=new mysql_recordset($sql2);
}
?>
<html>
<head>
<title>TopMoving Lead Report (By Zip Code Radius)</title>
</head>
<body>
<form name="zipform" method="post" action="radius.php">
<table border="0" cellspacing="1" width="100%">
  <tr>
    <td align="center">
    <h3>TopMoving Lead Report (By Zip Code Radius)</h3>
    <?
    //Display error message, if an error exists
	if($errmsg!='') {
		echo "<font color='red'><b>$errmsg</b></font><br><br>";
	}
	?>
    Zip Code<input size="5" name="zip" class="formfield" maxlength="5" value="<?=$_POST['zip']?>">
    Miles<input size="4" name="miles" class="formfield" maxlength="4" value="<?=$_POST['miles']?>">
    <input type="submit" value="Submit" name="submit"><hr></td>
  </tr>
  <tr>
    <td>
    <?
    if($_POST['zip']!='' && $_POST['miles']!='' && $errmsg=='') {
    	?>
    	<table border="0" cellspacing="1" width="100%">
    	  <tr>
    	    <td align="left" valign="top" width="400">
	    	<?
	    	//List the unique zip codes that fall within the radius search
	    	$statecount=$rs2->rowcount();
	    	echo "<b>$statecount Zip Codes Found</b><br><br>";
			while($rs2->fetch_array()) {
				echo "".$rs2->myarray['zip']." - ".$rs2->myarray['city'].", ".$rs2->myarray['fullstate']."<br>";
			}
			?>
			</td>
			<td align="left" valign="top">
			<b>Lead Counts for Specified Area</b><br><br>
			<table border="1" cellspacing="1" width="125">
			<?php
			//$i is used for the iteration loop, $tempavg is used in preparation of averaging out the lead counts over a period of time
			$i=0;
			$tempavg=0;
			
			//The following grabs information from the past year, including the current month
			while($i<=12) {
				//Adjusting the 'month' depending on where the loop is at.  The 'year' automatically adjusts when appropriate.
				$year = $myyear;
				$month = $mymonth - $i;
				
				//$sqldate is the date used for the DB query, $dispdate is used for display purposes on the page
				$sqldate=date('Ym',mktime(0, 0, 0, $month, 1, $year));
				$dispdate=date('M Y',mktime(0, 0, 0, $month, 1, $year));
				
				//Grab the lead counts for the specified date from the lead archives
				$sql3="select q.email from movingdirectory.quotes as q, vacationhomes.zip_codes as z where z.zip=q.origin_zip and ($distance <= ".$_POST['miles'].") 
				and received like '$sqldate%' and q.cat_id='2' and q.source like 'tm%' and q.lead_ids like '%1387%'";
				$rs3=new mysql_recordset($sql3);
				$leadcount=$rs3->rowcount();
				
				//$tempavg will continue to be added onto through the loops. It will be averaged out later.
				$tempavg=$tempavg + $leadcount;
				
				?>
				<tr><td align="left" width="75"><?=$dispdate?></td>
				<td align="center" width="50"><b><?=$leadcount?></b></td></tr>
				<?
				$i++;
			}
			?>
			</table>
			<br>
			<?
			//$tempavg is averaged out by $i. The number will be averaged out with the 'round' function
			$leadavg=$tempavg/$i;
			?>
			Average Lead Count Per Month = <b><?=round($leadavg)?> Leads</b>
			</td>
		  </tr>
		</table>
	<?
    }
    else {
    ?>
    Please Submit Information
    <?
    }
    ?>
    </td>
  </tr>
</table>
</form>
</body>
</html>