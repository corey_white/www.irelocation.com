<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Tools</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<img src='https://adwords.google.com/select/images/adwords_home/new_logo.gif' title='Google' /><br/>
Fill out these fields to create Google Keywords <Br/>
based on the Cities or Zip codes in our database.
<hr width="50%" align="left"/>
<?php 

	extract($_POST); 
	
	if (strlen($city_pos) < 1)
		$city_pos = "before";
	
	if (strlen($contentmatchbid) < 1)//default.
	{
		$contentmatchbid = .1;
		if(strlen($content_match_status) < 1)
			$content_match_status = "on";
	}
		
	if (strlen($standardbid) < 1)//default.
	{
		$standardbid = .1;
		if(strlen($advanced_match_status) < 1)	
			$advanced_match_status = "on";
	}

?>
<form action="googlekeywords.php" method="post" >
<table >
 	<tr><th align='left'>File Name</th> <td align='left'><input name="filename" class="quote_box" value="<?php echo $filename; ?>" type="text" maxlength="150" />.xls</td></tr>
	<tr><th align='left'>Account</th> <td align='left'><input name="account" class="quote_box" value="<?php echo $account; ?>" type="text" maxlength="150" /></td></tr>
    <tr><th align='left'>Daily Budget</th><td align='left'><input name="budget"  class="quote_box" value="<?php echo $budget; ?>" type="text" size="5" maxlength="5" /></td></tr>	
	<tr><th align='left'>Ad Group Name</th> <td align='left'><input name="adgroupname" class="quote_box" value="<?php echo $adgroupname; ?>" type="text" maxlength="100" /></td></tr>
	<tr><th align='left'>Max Ad Group CPC</th><td align='left'><input name="maxcpc"  class="quote_box" value="<?php echo $maxcpc; ?>" type="text" size="5" maxlength="5" /></td></tr>	
    <tr>
		<th align='left'>City Position </th> 
		<td align='left'>
			<input type="radio" name="city_pos" value="before" <?php if ($city_pos == "before") echo "checked"; ?> >Before <input type="radio" name="city_pos" value="after" <?php if ($city_pos == "after") echo "checked"; ?> >After
		</td>
	</tr>
	<tr>
		<th align='left'>Keyword </th> 
		<td align='left'>
			<input name="keyword" value="<?php echo $keyword; ?>" type="text"  class="quote_box" />
			<select name="locationtype"  class="quote_box" >
				<option value="city">Cities</option>
				<option value="zip">ZipCodes</option>
				<option value="cs">City and State</option>
				<option value="csa">City and State Abrv</option>
			</select>
			
			
		</td>
	</tr>
	<tr><th align='left'>Title</th><td align='left'><input name="headline"  class="quote_box" value="<?php echo $headline; ?>" type="text" size="25" /></td></tr>
	<tr><th align='left'>Description</th> <td align='left'><input name="description1" class="quote_box" value="<?php echo $description1; ?>"  size="35" type="text" /></td></tr>
	<tr><th align='left'>Description</th> <td align='left'><input name="description2"  class="quote_box" value="<?php echo $description2; ?>"  size="35" type="text" /></td></tr>
    <tr><th align='left'>Display Url</th><td align='left'>http://<input name="displayurl" class="quote_box" value="<?php echo $displayurl; ?>" type="text" size="40" /></td></tr>
	<tr><th align='left'>Destination Url</th><td align='left'>http://<input name="destinationurl" class="quote_box" value="<?php echo $destinationurl; ?>" type="text" size="40" /></td></tr> 
    <tr><th align='left'>Excluded Words </th><td align='left'><input name="excludedwords" class="quote_box" value="<?php echo $excludedwords; ?>" type="text" />*comma-delimited list.</td></tr>
	<tr><th align='left'>Keyword Limit </th><td align='left'><input name="limit" size="5" class="quote_box" value="<?php echo $limit; ?>" type="text" />* enter "all" to get all keywords.</td></tr>

 	<Tr><td colspan="2"><input type="submit" name='go' class="quote_box" value="Submit"/></td></Tr>
	
</table>
</form>

</body></html>