<?php
include("config.php");
include("mysql.php");
	mail("dave@irelocation.com","1md Auto Billing","Cron Ran",'From: "iRelocation Merchant" <billing@irelocation.com>');
/*
	1) De-Activate rules that have ended.
	
	2) Select all billing rules whose:
			a. next billing date is today
			b. have not ended
			c. and are active
	
		2.1) For each of those results:
			a.  insert record into billing table
			b.  update rules record to reflect next billing date
	3) Select all billing records whose:
			a. billing date < today
			b. billing status = new
			
		3.1) For each of those results:
			a. billing type
				i. if credit card run it.
				ii. if direct deposit run it.
			b. add transaction to log.
			c. check status
				i. success - add a 'received' billing record (-ammount)
				ii. failure - set status.
			d. update billing record with status and log id.
			e. send out followup email to client if successful.
*/

#
# Select the transactions from the billing_rules table and enter any automatic transactions into the billing table
#

//get todays date
$today=date("Y-m-d");

//deactivate all rules that have ended!
$rs=new mysql_recordset("update billing_rules set active='0' where end<'$today'");

//get new transactions for the day
$rs=new mysql_recordset("select * from billing_rules where next='$today' and end>='$today' and active='1'");
while($rs->fetch_array())
{
  //enter the transaction in the billing table
  $rs2=new mysql_recordset("insert into billing (comp_id,cc_id,amount,description,date,status) values ('".$rs->myarray["comp_id"]."','".$rs->myarray["cc_id"]."','".$rs->myarray["amount"]."','".$rs->myarray["description"]."','".$rs->myarray["next"]."','".$rs->myarray["status"]."')");

  //set the next date to run
  list($year,$month,$day)=explode("-",$rs->myarray["next"]);
  switch($rs->myarray["terms"])
      {
        case "weekly":
          $next=date("Y-m-d",mktime(0,0,0,$month,$day+7,$year));
          break;
        case "bi-weekly":
          $next=date("Y-m-d",mktime(0,0,0,$month,$day+14,$year));
          break;
        case "monthly":
          $next=date("Y-m-d",mktime(0,0,0,$month+1,$day,$year));
          break;
        case "bi-monthly":
          $next=date("Y-m-d",mktime(0,0,0,$month+2,$day,$year));
          break;
        case "quarterly":
          $next=date("Y-m-d",mktime(0,0,0,$month+3,$day,$year));
          break;
        case "semi-annually":
          $next=date("Y-m-d",mktime(0,0,0,$month+6,$day,$year));
          break;
        case "annually":
          $next=date("Y-m-d",mktime(0,0,0,$month,$day,$year+1));
          break;
      }
  $rs2=new mysql_recordset("update billing_rules set next='$next' where id='".$rs->myarray["id"]."'");
}

/*
# Select the new transactions from the billing table
# where the date is less than or equal to  today.
*/
$rs=new mysql_recordset("select company.comp_name,billing.*,decode(billing_cc.ccinfo,'$xxxkey') as myccinfo from company,billing,billing_cc where company.comp_id=billing.comp_id and billing.cc_id=billing_cc.id and billing.date<='$today' and billing.status='new'");

#
# Process transactions
#

while($rs->fetch_array())
{
  //pull credit card information
  $cc=nothing;
  $cc=unserialize($rs->myarray["myccinfo"]);
  $id=$rs->myarray["id"];
  $comp_id=$rs->myarray["comp_id"];
  $amount=$rs->myarray["amount"];
  $comp_name=$rs->myarray["comp_name"];
  $description=$rs->myarray["description"];

  //set up post variables for transaction
  $data="x_Version=3.1&x_Delim_Data=True&x_Method=CC&x_Login=relocate480&x_Password=ALMGCz5k&x_Amount=$amount&x_Description=$description&x_Type=AUTH_CAPTURE&x_Cust_ID=$comp_id";
  //$data.="&x_Test_Request=TRUE";

  if($cc["method"]=='cc')
  {
    $data.="&x_Method=CC&x_Card_Num=".$cc["card_num"]."&x_Exp_Date=".$cc["exp_date"]."&x_First_Name=".$cc["first_name"]."&x_Last_Name=".$cc["last_name"]."";
  }
  else
  {
    $data.="&x_Method=ECHECK&x_Bank_ABA_Code=".$cc["bank_aba_code"]."&x_Bank_Acct_Num=".$cc["bank_acct_num"]."&x_Bank_Acct_Type=".$cc["bank_acct_type"]."&x_Bank_Name=".$cc["bank_name"]."&x_Bank_Account_Name=".$cc["first_name"]." ".$cc["last_name"]."";
  }

  //initialize curl and process transaction
  $url="https://secure.authorize.net/gateway/transact.dll";
  $ch=curl_init();
  curl_setopt($ch, CURLOPT_URL,$url);
  curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0); 
  curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0); 
  curl_setopt($ch, CURLOPT_FAILONERROR, 1); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  curl_setopt($ch, CURLOPT_HEADER,0);
  curl_setopt($ch, CURLOPT_POST,1);
  curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
  $buf = curl_exec($ch);
  $trans=explode(",",$buf);
  curl_close($ch);

  //add transaction to log
  $rs2=new mysql_recordset("insert into authorizenetlog (comp_id,x_response_code,x_response_subcode,x_response_reason_code,x_auth_code,x_avs_code,x_trans_id,x_amount,x_method,x_type,x_first_name,x_last_name,x_email) values ('$comp_id','$trans[0]','$trans[1]','$trans[2]','$trans[4]','$trans[5]','$trans[6]','$trans[9]','$trans[10]','$trans[11]','$trans[13]','$trans[14]','$trans[23]')");
  $rs2=new mysql_recordset("select max(log_id) as myid from authorizenetlog");
  $rs2->fetch_array();
  $log_id=$rs2->myarray["myid"];

  //update original billing entry (processed, failed)
  if($trans[0]=='1')
  {
    $status="processed";
    //enter the new transaction
    $rs2=new mysql_recordset("insert into billing (comp_id,cc_id,log_id,amount,description,date,status) values ('$comp_id','','$log_id','-$amount','Authorize.net $trans[10] #$trans[6]','$today','received')");
  }
  else
  {
    $status="failed";
  }

  $rs2=new mysql_recordset("update billing set log_id='$log_id',status='$status' where id='$id'");

  //send out emails
  if($trans[0]=='1')
  {
    //email customer
    $email_body="Dear iRelocation Client:\n\nPer your prior authorization, we processed a transaction on ".date("F j, Y")." and deposited \$$amount to your account. The transaction was successful.  The details of your transaction are as follows:\n\nCompany: $comp_name\nCompany ID: $comp_id\nTransaction: $description\n\n";
    if($cc["method"]=='cc')
    {
      $email_body.="Card Holder Name: ".$cc["first_name"]." ".$cc["last_name"]."\nCredit Card: xxxxxxxxxxxx".substr($cc["card_num"],-4)."\nExpiration Date: ".$cc["exp_date"]."\n\n";
    }
    else
    {
      $email_body.="Bank Name: ".$cc["bank_name"]."\nBank Account Type: ".$cc["bank_acct_type"]."\nBank Account Number: ".substr($cc["bank_acct_num"],-4)."\n\n";
    }
    $email_body.="Amount: \$$amount\nDate: ".date("F j, Y")."\n\nPlease retain/print a copy of this e-mail for your records.  If you have any billing questions, please contact us at billing@irelocation.com.\n\nWe appreciate your business!\n\nSincerely,\n\nYour partners at The iRelocation Network\n\nwww.iRelocation.com\nwww.1stMovingDirectory.com\nwww.TopMoving.com\nwww.ProMoving.com\nwww.CarShipping.com\nwww.MoveMyCar.com\n";
    if($cc["email_address"]!='')
    {
      mail($cc["email_address"],"Email Receipt",$email_body,'From: "iRelocation Auto-Receipt" <billing@irelocation.com>');
      mail('mark@irelocation.com',"[COPY] Email Receipt",$email_body,'From: "iRelocation Auto-Receipt" <billing@irelocation.com>');
      mail('katrina@irelocation.com',"[COPY] Email Receipt",$email_body,'From: "iRelocation Auto-Receipt" <billing@irelocation.com>');
    }
  }
  else
  {
    //email admins
    mail("mark@irelocation.com","Transaction Failed for $comp_name","Failed Transaction:\n$today - $description - $amount\nReason: $trans[3]",'From: "iRelocation Merchant" <billing@irelocation.com>');
    mail("katrina@irelocation.com","Transaction Failed for $comp_name","Failed Transaction:\n$today - $description - $amount\nReason: $trans[3]",'From: "iRelocation Merchant" <billing@irelocation.com>');
  }

}
?>