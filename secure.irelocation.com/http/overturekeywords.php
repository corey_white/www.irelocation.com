<?php

function get_state_name($state_abrv)
{
  switch($state_abrv)
  {
    case "AK": $mystate="Alaska"; break;
    case "AL": $mystate="Alabama"; break;
    case "AR": $mystate="Arkansas"; break;
    case "AZ": $mystate="Arizona"; break;
    case "CA": $mystate="California"; break;
    case "CO": $mystate="Colorado"; break;
    case "CT": $mystate="Connecticut"; break;
    case "DC": $mystate="Washington D.C."; break;
    case "DE": $mystate="Delaware"; break;
    case "FL": $mystate="Florida"; break;
    case "GA": $mystate="Georgia"; break;
    case "HI": $mystate="Hawaii"; break;
    case "IA": $mystate="Iowa"; break;
    case "ID": $mystate="Idaho"; break;
    case "IL": $mystate="Illinois"; break;
    case "IN": $mystate="Indiana"; break;
    case "KS": $mystate="Kansas"; break;
    case "KY": $mystate="Kentucky"; break;
    case "LA": $mystate="Louisiana"; break;
    case "MA": $mystate="Massachusetts"; break;
    case "MD": $mystate="Maryland"; break;
    case "ME": $mystate="Maine"; break;
    case "MI": $mystate="Michigan"; break;
    case "MN": $mystate="Minnesota"; break;
    case "MO": $mystate="Missouri"; break;
    case "MS": $mystate="Mississippi"; break;
    case "MT": $mystate="Montana"; break;
    case "NC": $mystate="North Carolina"; break;
    case "ND": $mystate="North Dakota"; break;
    case "NE": $mystate="Nebraska"; break;
    case "NH": $mystate="New Hampshire"; break;
    case "NJ": $mystate="New Jersey"; break;
    case "NM": $mystate="New Mexico"; break;
    case "NV": $mystate="Nevada"; break;
    case "NY": $mystate="New York"; break;
    case "OH": $mystate="Ohio"; break;
    case "OK": $mystate="Oklahoma"; break;
    case "OR": $mystate="Oregon"; break;
    case "PA": $mystate="Pennsylvania"; break;
    case "RI": $mystate="Rhode Island"; break;
    case "SC": $mystate="South Carolina"; break;
    case "SD": $mystate="South Dakota"; break;
    case "TN": $mystate="Tennessee"; break;
    case "TX": $mystate="Texas"; break;
    case "UT": $mystate="Utah"; break;
    case "VA": $mystate="Virginia"; break;
    case "VT": $mystate="Vermont"; break;
    case "WA": $mystate="Washington"; break;
    case "WI": $mystate="Wisconsin"; break;
    case "WV": $mystate="West Virginia"; break;
    case "WY": $mystate="Wyoming"; break;
  }
  return $mystate;
}

	include 'mysql.php';
	extract($_POST); 
	header("Content-type: application/octet-stream");

	# replace excelfile.xls with whatever you want the filename to default to
	header("Content-Disposition: attachment; filename=$filename.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	
	
	if (strlen($city_pos) < 1)
		$city_pos = "before";
	
	if (strlen($contentmatchbid) < 1)//default.
	{
		$contentmatchbid = .1;
		if(strlen($content_match_status) < 1)
			$content_match_status = "on";
	}
		
	if (strlen($standardbid) < 1)//default.
	{
		$standardbid = .1;
		if(strlen($advanced_match_status) < 1)	
			$advanced_match_status = "on";
	}


	if (strlen($go) > 0)
	{
		if (strlen($limit) > 0 && $limit != "all")
			$limit_string = " limit $limit";
		else
			$limit_string = "";
			//locationtype
		
		if (($_POST["locationtype"] == "city") || ($_POST["locationtype"] == "cs") || ($_POST["locationtype"] == "csa"))
			$sql = "select distinct(city) as location, state as state_abrv from zip_codes where city > 'a' and CHAR_LENGTH(city) < 20 order by city $limit_string;";		
		else
			$sql = "select distinct(zip) as location, state as state_abrv from zip_codes where CHAR_LENGTH(zip) = 5 order by zip $limit_string;";		
	
		if (strlen($advanced_match_status) < 1) 
			$advanced_match_status = "off";
		if (strlen($content_match_status) < 1)
			$content_match_status = "off";
		if (strlen($title) == 0)
			$title = $search_term;
		$url = "http://".$url;
		$rs = new mysql_recordset($sql);
		echo "Account ID\tSearch Term\tURL\tTitle\tDescription\tAdvanced Match Status\tStandard Bid ($)\tContent Match Status\tContent Match Bid ($)\tExcluded Words\tCategory\n";
		while ($rs->fetch_array())
		{
			$location = $rs->myarray["location"];
			$state_abrv = $rs->myarray["state_abrv"];
			
			if ($_POST["locationtype"] == "cs") { //city and state
		
		$mystate = get_state_name($state_abrv);
		
			if ($city_pos == "before")
				$search_string = "$mystate $location $search_term ";
			else 
				$search_string = "$search_term $location $mystate";
			
		} else if ($_POST["locationtype"] == "csa") { //city and state abrv
		
			if ($city_pos == "before")
				$search_string = "$state_abrv $location $search_term ";
			else 
				$search_string = "$search_term $location $state_abrv";
			
		} else {
		
			if ($city_pos == "before")
				$search_string = "$location $search_term ";
			else 
				$search_string = "$search_term $location";			
		}
			
			
			
			
			$sql = " $account\t$search_string\t$url\t$title\t$description\t$advanced_match_status\t$standardbid\t$content_match_status\t$contentmatchbid\t$excludedwords\t$category ";
			//saveKeyword($sql);
			echo "$sql\n";
		}
		echo "</div>";
		
	}
	
	function saveKeyword($sql)
	{
		$query = START_OF_INSERT.$sql.");";
		$rs = new mysql_recordset($query);
	}
	
?>