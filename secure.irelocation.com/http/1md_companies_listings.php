<?

#
# Add New Listing
#
if($action2=='add_new_listing')
{
  if($list_type=='state')
  {
    foreach($statelist as $mystate)
    {
      $sql="select * from directlistings where cat_id='$cat_id' and comp_id='$id' and list_type='$list_type' and state='$mystate'";

      $rs=new mysql_recordset($sql);
      if($rs->rowcount()>0)
      {
        $msg.="$mystate listing already exists!<br>";
      }
      else
      {
        if($never==1)
        {
          $expiration="9999-12-31";
        }
        else
        {
          $expiration="$exp_year-$exp_month-$exp_day";
        }
        $rs=new mysql_recordset("insert into directlistings (comp_id,cat_id,expiration,list_type,name,email,phone_num,url,description,active,state) values ('$id','$cat_id','$expiration','$list_type','$name','$email','$phone_num','$url','$description','1','$mystate')");
      }
    }
  }
  elseif($list_type=='national')
  {
    $sql="select * from directlistings where cat_id='$cat_id' and comp_id='$id' and list_type='$list_type'";

    $rs=new mysql_recordset($sql);
    if($rs->rowcount()>0)
    {
        $msg.="That national listing already exists!<br>";
    }
    else
    {
      if($never==1)
      {
        $expiration="9999-12-31";
      }
      else
      {
        $expiration="$exp_year-$exp_month-$exp_day";
      }
      $rs=new mysql_recordset("insert into directlistings (comp_id,cat_id,expiration,list_type,name,email,phone_num,url,description,active) values ('$id','$cat_id','$expiration','$list_type','$name','$email','$phone_num','$url','$description','1')");
    }
  }
  elseif($list_type=='area')
  {
    $sql="select * from directlistings where cat_id='$cat_id' and comp_id='$id' and list_type='$list_type' and area_code='$area_code'";

    $rs=new mysql_recordset($sql);
    if($rs->rowcount()>0)
    {
        $msg.="That area code listing already exists!<br>";
    }
    else
    {
      if($never==1)
      {
        $expiration="9999-12-31";
      }
      else
      {
        $expiration="$exp_year-$exp_month-$exp_day";
      }
      $rs=new mysql_recordset("insert into directlistings (comp_id,cat_id,expiration,list_type,name,email,phone_num,url,description,active,area_code) values ('$id','$cat_id','$expiration','$list_type','$name','$email','$phone_num','$url','$description','1','$area_code')");
    }
  }

  $rs=new mysql_recordset("update directlistingscategory set doupdate='1' where cat_id='$cat_id'");
  $msg="New $list_type listing has been added!";
}
#
# Delete Listing
#
elseif($action2=='delete_listing')
{
  $rs=new mysql_recordset("delete from directlistings where list_id='$list_id'");
  $rs=new mysql_recordset("update directlistingscategory set doupdate='1' where cat_id='$cat_id'");
  $msg="Listing #$list_id has been deactivated!";
}
#
# Deactivate Listing
#
elseif($action2=='deactivate')
{
  $rs=new mysql_recordset("update directlistings set active='0' where list_id='$list_id'");
  $rs=new mysql_recordset("update directlistingscategory set doupdate='1' where cat_id='$cat_id'");
  $msg="Listing #$list_id has been deactivated!";
}
#
# Reactivate Listing
#
elseif($action2=='reactivate')
{
  $rs=new mysql_recordset("update directlistings set active='1' where list_id='$list_id'");
  $rs=new mysql_recordset("update directlistingscategory set doupdate='1' where cat_id='$cat_id'");
  $msg="Listing #$list_id has been reactivated!";
}


#
# Get the company info
#
$rs=new mysql_recordset("select * from company where comp_id='$id'");
$rs->fetch_array();
$comp_info=$rs->myarray;

#
# Get all the categories
#
$rs=new mysql_recordset("select cat_id,name from directlistingscategory where parent_id!='0' and link_url='' order by name");
while($rs->fetch_array())
{
  $cats[]=$rs->myarray;
}

#
# Get all the listings for the company
#
$rs=new mysql_recordset("select directlistings.*,directlistingscategory.name as catname from directlistings,directlistingscategory where directlistings.cat_id=directlistingscategory.cat_id and directlistings.comp_id='$id' order by directlistingscategory.name,directlistings.list_type,directlistings.state");
if($rs->rowcount()>0)
{
  while($rs->fetch_array())
  {
    $listings[]=$rs->myarray;
  }
  $haslistings=TRUE;
}
else
{
  $haslistings=FALSE;
}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Listings Information for <?echo $comp_info["comp_name"];?></title>
<script language=JavaScript>
var checkboxName = 'checkbox'
var isCheck = true;

function checkall(form) {
  for (var i = 1; true; i++){
    if(form.elements[checkboxName+i] == null)
      break;
    form.elements[checkboxName+i].checked = isCheck;
  }
  isCheck = !isCheck;
}

function open_action_win(url) {
  window.open(url,"listings",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=550,height=400');
}

function popup(form, page) {
  var url = page
  var somethingChecked = false
  var nothingMsg = 'No listings selected.'
  for (var i = 1; true; i++) {
    if(form.elements[checkboxName+i] == null){
      break;
    }
    if (form.elements[checkboxName+i].checked == true) {
      if (somethingChecked) {
        url += "," + form.elements[checkboxName+i].value
      }
      else {
        url += "?list_ids=" + form.elements[checkboxName+i].value
        somethingChecked = true
      }
    }
  }
  if (somethingChecked) {
    //alert(url)
    url += "&action=1md_companies_listings_modify&id=<?echo $id;?>"
    open_action_win(url)
  }
  else {
    alert(nothingMsg)
  }
}
</script>
</head>

<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Add a New Listing</b></font><?if($haslistings==FALSE){?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="heading2">No Listings Found!</span><?}?></p>
<form name="listings" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_listings">
<input type="hidden" name="action2" value="add_new_listing">
<input type="hidden" name="id" value="<?echo $id;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="200"><span class="text_2">Category:</span></td>
          <td width="300"><select name="cat_id" class="quote_box">
          <?
          foreach($cats as $mycat)
          {
            echo "<option value=\"".$mycat["cat_id"]."\">".$mycat["name"]."</option>";
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Expiration:</span></td>
          <td width="300"><select name="exp_month" class="quote_box">
          <?
          for($i=1;$i<=12;$i++)
          {
            ?><option value="<?echo $i;?>"<?if($i==date("n")){echo " selected";}?>><?echo date("M",mktime(0,0,0,$i,1,2000));?></option>"<?
          }
          ?></select><select name="exp_day" class="quote_box">
          <?
          for($i=1;$i<=31;$i++)
          {
            ?><option value="<?echo $i;?>"<?if($i==date("j")){echo " selected";}?>><?echo $i;?></option>"<?
          }
          ?></select><select name="exp_year" class="quote_box">
          <?
          for($i=date("Y");$i<=date("Y")+5;$i++)
          {
            ?><option value="<?echo $i;?>"><?echo $i;?></option>"<?
          }
          ?></select> &nbsp;<input type="checkbox" name="never" value="1"><span class="text_2"> Never</span></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Listing Type:</span></td>
          <td width="300"><select name="list_type" class="quote_box">
          <option value="national">National Listing</option>
          <option value="state">State Listing</option>
          <option value="area">Area Code Listing</option></select></td>
        </tr>
        <tr>
          <td width="500" colspan="2"><table border="0" cellspacing="0" cellpadding="0" width="500">
          <?$i=0;?>
  <tr>
    <td width="5"><input type="checkbox" value="AL" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Alabama</span></td>
    <td width="5"><input type="checkbox" value="HI" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Hawaii</span></td>
    <td width="5"><input type="checkbox" value="MA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Massachusetts</span></td>
    <td width="5"><input type="checkbox" value="NM" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">New Mexico</span></td>
    <td width="5"><input type="checkbox" value="SD" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">South Dakota</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="AK" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Alaska</span></td>
    <td width="5"><input type="checkbox" value="ID" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Idaho</span></td>
    <td width="5"><input type="checkbox" value="MI" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Michigan</span></td>
    <td width="5"><input type="checkbox" value="NY" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">New York</span></td>
    <td width="5"><input type="checkbox" value="TN" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Tennessee</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="AZ" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Arizona</span></td>
    <td width="5"><input type="checkbox" value="IL" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Illinois</span></td>
    <td width="5"><input type="checkbox" value="MN" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Minnesota</span></td>
    <td width="5"><input type="checkbox" value="NC" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">North Carolina</span></td>
    <td width="5"><input type="checkbox" value="TX" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Texas</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="AR" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Arkansas</span></td>
    <td width="5"><input type="checkbox" value="IN" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Indiana</span></td>
    <td width="5"><input type="checkbox" value="MS" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Mississippi</span></td>
    <td width="5"><input type="checkbox" value="ND" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">North Dakota</span></td>
    <td width="5"><input type="checkbox" value="UT" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Utah</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="CA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">California</span></td>
    <td width="5"><input type="checkbox" value="IA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Iowa</span></td>
    <td width="5"><input type="checkbox" value="MO" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Missouri</span></td>
    <td width="5"><input type="checkbox" value="OH" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Ohio</span></td>
    <td width="5"><input type="checkbox" value="VT" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Vermont</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="CO" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Colorado</span></td>
    <td width="5"><input type="checkbox" value="KS" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Kansas</span></td>
    <td width="5"><input type="checkbox" value="MT" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Montana</span></td>
    <td width="5"><input type="checkbox" value="OK" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Oklahoma</span></td>
    <td width="5"><input type="checkbox" value="VA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Virginia</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="CT" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Connecticut</span></td>
    <td width="5"><input type="checkbox" value="KY" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Kentucky</span></td>
    <td width="5"><input type="checkbox" value="NE" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Nebraska</span></td>
    <td width="5"><input type="checkbox" value="OR" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Oregon</span></td>
    <td width="5"><input type="checkbox" value="WA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Washington</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="DE" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Delaware</span></td>
    <td width="5"><input type="checkbox" value="LA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Louisiana</span></td>
    <td width="5"><input type="checkbox" value="NV" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Nevada</span></td>
    <td width="5"><input type="checkbox" value="PA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Pennsylvania</span></td>
    <td width="5"><input type="checkbox" value="DC" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Washington, DC</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="FL" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Florida</span></td>
    <td width="5"><input type="checkbox" value="ME" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Maine</span></td>
    <td width="5"><input type="checkbox" value="NH" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">New Hampshire</span></td>
    <td width="5"><input type="checkbox" value="RI" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Rhode Island</span></td>
    <td width="5"><input type="checkbox" value="WV" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">West Virginia</span></td>
  </tr>
  <tr>
    <td width="5"><input type="checkbox" value="GA" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Georgia</span></td>
    <td width="5"><input type="checkbox" value="MD" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Maryland</span></td>
    <td width="5"><input type="checkbox" value="NJ" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">New Jersey</span></td>
    <td width="5"><input type="checkbox" value="SC" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">South Carolina</span></td>
    <td width="5"><input type="checkbox" value="WI" name="statelist[<?echo ++$i;?>]"></td>
    <td><span class="realsmall">Wisconsin</span></td>
  </tr>
  <tr>
    <td width="5">&nbsp;</td>
    <td width="90">&nbsp;</td>
    <td width="5">&nbsp;</td>
    <td width="90">&nbsp;</td>
    <td width="5">&nbsp;</td>
    <td width="90">&nbsp;</td>
    <td width="5">&nbsp;</td>
    <td width="90">&nbsp;</td>
    <td width="5"><input type="checkbox" value="WY" name="statelist[<?echo ++$i;?>]"></td>
    <td width="115"><span class="realsmall">Wyoming</span></td>
  </tr>
</table>
</td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Area Code:</span></td>
          <td width="300"><input type="text" name="area_code" class="quote_box" size="5" maxlength="3"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Name:</span></td>
          <td width="300"><input type="text" name="name" class="quote_box" size="45" value="<?echo $comp_info["comp_name"];?>"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Email:</span></td>
          <td width="300"><input type="text" name="email" class="quote_box" size="45" value="<?echo $comp_info["contact_email"];?>"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Phone Number:</span></td>
          <td width="300"><input type="text" name="phone_num" class="quote_box" size="45" value="<?echo $comp_info["phone_num"];?>"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Web Address:</span></td>
          <td width="300"><input type="text" name="url" class="quote_box" size="45" value="<?echo $comp_info["url"];?>"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Description:</span></td>
          <td width="300"><input type="text" name="description" class="quote_box" size="45"></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Add New Listing" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<?
if($haslistings==TRUE)
{
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Current Listings</b></font></p>
<form name="mylistings" method="POST" action="control.php">
    <span class="notice">
      <table cellSpacing="0" cellPadding="2" width="500" border="0">
      <tr>
        <td width="500" bgcolor="#A9CAEB" colspan="5">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
            <img src="images/arrow_top.gif" width="25" height="16"><a href="control.php" onClick="popup(document.mylistings,this.href); return false;"><img border="0" src="images/bmodify.gif"></a></td>
          </tr>
        </table>
        </td>
      </tr>
        <tr>
          <td width="10" bgcolor="#A9CAEB" valign="bottom"><p align="center"><a onclick="checkall(document.mylistings); return false;" href="#"><img alt src="images/checkall.gif" border="0" width="16" height="24"></a></td>
          <td width="140"><span class="heading">category</span></td>
          <td width="100"><span class="heading">type</span></td>
          <td width="100"><span class="heading">expiration</span></td>
          <td width="150"><span class="heading">options</span></td>
        </tr>
        <?
        $i=0;
        foreach($listings as $mylisting)
        {
        ?>
        <tr<?if($mylisting["active"]!='1'){echo " class='quote_col_x'";}?>>
          <td width="10" bgcolor="#A9CAEB"><input type="checkbox" name="checkbox<?echo ++$i;?>" value="<?echo $mylisting["list_id"];?>"></td>
          <td width="140"><span class="realsmall"><?echo $mylisting["catname"];?></span></td>
          <td width="100"><span class="text_2"><?echo $mylisting["list_type"];?><?if($mylisting["list_type"]=='state'){echo "-"; echo $mylisting["state"];}elseif($mylisting["list_type"]=='area'){echo "-"; echo $mylisting["area_code"];}?></span></td>
          <td width="100"><span class="realsmall"><?if($mylisting["expiration"]=='9999-12-31'){echo "N/A";}else{echo $mylisting["expiration"];}?></span></td>
          <td width="150"><a href="control.php?action=1md_companies_listings&action2=<?if($mylisting["active"]=='1'){echo "d";}else{echo "r";}?>eactivate&id=<?echo $id;?>&list_id=<?echo $mylisting["list_id"];?>&cat_id=<?echo $mylisting["cat_id"];?>"><img src="images/b<?if($mylisting["active"]=='1'){echo "d";}else{echo "r";}?>eactivate2.gif" border="0"></a><a href="control.php?action=1md_companies_listings&action2=delete_listing&id=<?echo $id;?>&list_id=<?echo $mylisting["list_id"];?>&cat_id=<?echo $mylisting["cat_id"];?>"><img src="images/bdelete.gif" border="0"></a></td>
        </tr>
        <?
        }
        ?>
      </table>
    </span>
  </form>
<p></p>
<?
}
?>
<script language="javascript">
focus();
</script>
</body>
</html>