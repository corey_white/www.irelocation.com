<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Go Dispatch Billing</title>
<script language=javascript>
<!--
  function validate(form)
  {
    if(form.ck_name.value=='')
    {
      alert("Please enter the name that appears on your account!");
      form.ck_name.focus()
      return false;
    }
    if(form.ck_bank.value=='')
    {
      alert("Please enter your bank name!");
      form.ck_bank.focus()
      return false;
    }
    if(form.ck_aba.value=='')
    {
      alert("Please enter your rounting number!");
      form.ck_aba.focus()
      return false;
    }
    if(form.ck_account.value=='')
    {
      alert("Please enter your account number!");
      form.ck_account.focus()
      return false;
    }
  }
// -->
</script>
</head>

<body>
<form method="post" action="billing.php" onSubmit="return validate(this)">
<div align="center">
        <table border="0" width="640" cellspacing="0" cellpadding="0">
                <tr>
                        <td>
                        <img border="0" src="images/logo.gif" width="435" height="63"><p class="pagetitle">
                        <?if($msg){echo "<font color=\"#000000\" size=\"4\">$msg</font><br><br>";}?>
                        Account Information:</p>
                        <table border="0" width="200" cellspacing="1">
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="ck_term" value="monthly"<?if(($info["term"]=='') || ($info["ck_term"]=='monthly')){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Monthly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate;?></td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="ck_term" value="quarterly"<?if($info["term"]=='quarterly'){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Quarterly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate*3;?></td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="ck_term" value="yearly"<?if($info["term"]=='yearly'){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Yearly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate*12*0.90;?> Save 10%</td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">&nbsp;</td>
                                        <td class="pagetext">&nbsp;</td>
                                        <td width="95" class="pagetext">&nbsp;</td>
                                </tr>
                        </table>
                        <table border="0" width="380" cellspacing="0" cellpadding="4" bgcolor="#EDEDF0">
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Account Type:</td>
                                        <td>
                                        <p class="pagetext">
                                        <input type="radio" value="checking" name="ck_type"<?if($info["ck_type"]!='savings'){echo " checked";}?>>checking&nbsp;
                                        <input type="radio" value="savings" name="ck_type"<?if($info["ck_type"]=='savings'){echo " checked";}?>>savings</td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Name on Account:</td>
                                        <td>
                                        <input type="text" name="ck_name" size="20" class="signup" value="<?echo $info["ck_name"];?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Bank Name:</td>
                                        <td>
                                        <input type="text" name="ck_bank" size="20" class="signup" value="<?echo $info["ck_bank"];?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Routing (ABA)#:</td>
                                        <td>
                                        <input type="text" name="ck_aba" size="20" class="signup" value="<?if($info["ck_aba"]!=''){echo "*****".substr($info["ck_aba"],-4);}?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Account #:</td>
                                        <td>
                                        <input type="text" name="ck_account" size="20" class="signup" value="<?if($info["ck_account"]!=''){echo "******".substr($info["ck_account"],-4);}?>"></td>
                                </tr>
                                </table>
                        <p class="pagetext">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input border="0" src="images/continue.gif" name="submit" width="77" height="23" type="image"><br>
&nbsp;</p>
                        <p>
                        <img border="0" src="images/sample_check.jpg" width="421" height="220"></td>
                </tr>
        </table>
</div>
<input type="hidden" name="payment_type" value="ck">
<input type="hidden" name="comp_id" value="<?echo $comp_id;?>">
<input type="hidden" name="sid" value="<?echo $sid;?>">
<input type="hidden" name="action" value="billing_update">
</form>
</body>

</html>