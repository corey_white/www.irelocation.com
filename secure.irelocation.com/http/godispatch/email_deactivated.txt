Dear Auto Transporter,

This is an automatically generated email to inform you that your account is more than 15 days past due, and as a result have been deactivated.  No further payment is due!  If you wish to continue your service in the future please contact us so that we may reactivate your account.  If there is anything we can do you help you or to earn your business in the future please let us know.

Sincerely,


The Go Dispatch Team!
http://www.usautotransport.com

"An Association of Auto Transporters!"