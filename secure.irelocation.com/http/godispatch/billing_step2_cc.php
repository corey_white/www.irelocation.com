<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Go Dispatch Billing</title>
<script language=javascript>
<!--
  function validate(form)
  {
    if(form.cc_fname.value=='')
    {
      alert("Please enter your first name!");
      form.cc_fname.focus()
      return false;
    }
    if(form.cc_lname.value=='')
    {
      alert("Please enter your last name!");
      form.cc_lname.focus()
      return false;
    }
    if(form.cc_num.value=='')
    {
      alert("Please enter your card number!");
      form.cc_num.focus()
      return false;
    }
  }
// -->
</script>
</head>

<body>
<form method="post" action="billing.php" onSubmit="return validate(this)">
<div align="center">
        <table border="0" width="640" cellspacing="0" cellpadding="0">
                <tr>
                        <td>
                        <img border="0" src="images/logo.gif" width="435" height="63"><p class="pagetitle">
                        <?if($msg){echo "<font color=\"#000000\" size=\"4\">$msg</font><br><br>";}?>
                        Account Information:</p>
                        <table border="0" width="200" cellspacing="1">
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="cc_term" value="monthly"<?if(($info["term"]=='') || ($info["cc_term"]=='monthly')){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Monthly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate;?></td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="cc_term" value="quarterly"<?if($info["term"]=='quarterly'){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Quarterly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate*3;?></td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">
                                        <input type="radio" name="cc_term" value="yearly"<?if($info["term"]=='yearly'){echo " checked";}?>></td>
                                        <td class="pagetext">Pay Yearly</td>
                                        <td width="95" class="pagetext">$<?echo $monthly_rate*12*0.90;?> Save 10%</td>
                                </tr>
                                <tr>
                                        <td width="20" class="pagetext">&nbsp;</td>
                                        <td class="pagetext">&nbsp;</td>
                                        <td width="95" class="pagetext">&nbsp;</td>
                                </tr>
                        </table>
                        <table border="0" width="380" cellspacing="0" cellpadding="4" bgcolor="#EDEDF0">
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;First Name:</td>
                                        <td>
                                        <input type="text" name="cc_fname" size="20" class="signup" value="<?echo $info["cc_fname"];?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Last Name:</td>
                                        <td>
                                        <input type="text" name="cc_lname" size="20" class="signup" value="<?echo $info["cc_lname"];?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Visa/Mastercard #:</td>
                                        <td>
                                        <input type="text" name="cc_num" size="25" class="signup" value="<?if($info["cc_num"]!=''){echo "************".substr($info["cc_num"],-4);}?>"></td>
                                </tr>
                                <tr>
                                        <td width="153" class="pagebold">&nbsp;Expiration Date:</td>
                                        <td>
                                        <select name="cc_exp_month">
                                          <?for($i=1;$i<=12;$i++){?>
                                          <option value="<?if($i<10){?>0<?} echo $i;?>"<?if($i==intval($info["cc_exp_month"])){echo " selected";}?>><?if($i<10){?>0<?} echo $i;?></option>
                                          <?}?>
                                        </select>
                                        <select name="cc_exp_year">
                                          <?for($i=0;$i<=8;$i++)
                                          {
                                            $year=intval(date("y"))+$i;
                                            if($year<10)
                                            {
                                              $year="0".strval($year);
                                            }
                                          ?>
                                          <option value="<?echo $year;?>"<?if($year==$info["cc_exp_year"]){echo " selected";}?>><?echo date("Y")+$i;?></option>
                                          <?}?>
                                        </select></td>
                                </tr>
                                </table>
                        <p class="pagetext">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input border="0" src="images/continue.gif" name="submit" width="77" height="23" type="image"><br>
&nbsp;</p></td>
                </tr>
        </table>
</div>
<input type="hidden" name="payment_type" value="cc">
<input type="hidden" name="comp_id" value="<?echo $comp_id;?>">
<input type="hidden" name="sid" value="<?echo $sid;?>">
<input type="hidden" name="action" value="billing_update">
</form>
</body>

</html>