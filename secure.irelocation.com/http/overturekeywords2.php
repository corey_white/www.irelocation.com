<?php

function get_state_name($state_abrv)
{
  switch($state_abrv)
  {
    case "AK": $mystate="Alaska"; break;
    case "AL": $mystate="Alabama"; break;
    case "AR": $mystate="Arkansas"; break;
    case "AZ": $mystate="Arizona"; break;
    case "CA": $mystate="California"; break;
    case "CO": $mystate="Colorado"; break;
    case "CT": $mystate="Connecticut"; break;
    case "DC": $mystate="Washington D.C."; break;
    case "DE": $mystate="Delaware"; break;
    case "FL": $mystate="Florida"; break;
    case "GA": $mystate="Georgia"; break;
    case "HI": $mystate="Hawaii"; break;
    case "IA": $mystate="Iowa"; break;
    case "ID": $mystate="Idaho"; break;
    case "IL": $mystate="Illinois"; break;
    case "IN": $mystate="Indiana"; break;
    case "KS": $mystate="Kansas"; break;
    case "KY": $mystate="Kentucky"; break;
    case "LA": $mystate="Louisiana"; break;
    case "MA": $mystate="Massachusetts"; break;
    case "MD": $mystate="Maryland"; break;
    case "ME": $mystate="Maine"; break;
    case "MI": $mystate="Michigan"; break;
    case "MN": $mystate="Minnesota"; break;
    case "MO": $mystate="Missouri"; break;
    case "MS": $mystate="Mississippi"; break;
    case "MT": $mystate="Montana"; break;
    case "NC": $mystate="North Carolina"; break;
    case "ND": $mystate="North Dakota"; break;
    case "NE": $mystate="Nebraska"; break;
    case "NH": $mystate="New Hampshire"; break;
    case "NJ": $mystate="New Jersey"; break;
    case "NM": $mystate="New Mexico"; break;
    case "NV": $mystate="Nevada"; break;
    case "NY": $mystate="New York"; break;
    case "OH": $mystate="Ohio"; break;
    case "OK": $mystate="Oklahoma"; break;
    case "OR": $mystate="Oregon"; break;
    case "PA": $mystate="Pennsylvania"; break;
    case "RI": $mystate="Rhode Island"; break;
    case "SC": $mystate="South Carolina"; break;
    case "SD": $mystate="South Dakota"; break;
    case "TN": $mystate="Tennessee"; break;
    case "TX": $mystate="Texas"; break;
    case "UT": $mystate="Utah"; break;
    case "VA": $mystate="Virginia"; break;
    case "VT": $mystate="Vermont"; break;
    case "WA": $mystate="Washington"; break;
    case "WI": $mystate="Wisconsin"; break;
    case "WV": $mystate="West Virginia"; break;
    case "WY": $mystate="Wyoming"; break;
  }
  return $mystate;
}

	define(DELIMETER,"\t");

	include 'mysql.php';
	extract($_POST); 
	//header("Content-type: application/octet-stream");

	# replace excelfile.xls with whatever you want the filename to default to
	
	header("Content-Disposition: attachment; filename=$filename.txt");
	header("Pragma: no-cache");
	header("Expires: 0");
	
	
	
	if (strlen($city_pos) < 1)
	{
		$city_pos = "before";
		$_POST['city_pos'] = "before";
	}	
	
	if (strlen($contentmatchbid) < 1)//default.
	{
		$contentmatchbid = .1;
		if(strlen($content_match_status) < 1)
			$content_match_status = "on";
	}
		
	if (strlen($standardbid) < 1)//default.
	{
		$standardbid = .1;
		if(strlen($advanced_match_status) < 1)	
			$advanced_match_status = "on";
	}


	if (strlen($go) > 0)
	{
		if (strlen($limit) > 0 && $limit != "all")
			$limit_string = " limit $limit";
		else
			$limit_string = "";
			//locationtype
		
		if (($_POST["locationtype"] == "city") || ($_POST["locationtype"] == "cs") || ($_POST["locationtype"] == "csa"))
			$sql = "select distinct(city) as location, state as state_abrv from zip_codes where city > 'a' and CHAR_LENGTH(city) < 20 order by city $limit_string;";		
		else
			$sql = "select distinct(zip) as location, state as state_abrv from zip_codes where CHAR_LENGTH(zip) = 5 order by zip $limit_string;";		
	
		if (strlen($advanced_match_status) < 1) 
			$advanced_match_status = "off";
		if (strlen($content_match_status) < 1)
			$content_match_status = "off";
		if (strlen($title) == 0)
			$title = $search_term;
		
		$url = "http://".$url;
		$rs = new mysql_recordset($sql);
		$type = "keyword";
		echo "Campaign Name".DELIMETER."Ad Group Name".DELIMETER."Component Type".DELIMETER."Component Status".DELIMETER."Keyword".DELIMETER."Keyword Alt Text".DELIMETER."Keyword Custom URL".DELIMETER."Sponsored Search Bid (USD)".DELIMETER."Sponsored Search Bid Limit (USD)".DELIMETER."Sponsored Search Status".DELIMETER."Match Type".DELIMETER."Content Match Bid (USD)".DELIMETER."Content Match Bid Limit (USD)".DELIMETER."Content Match Status".DELIMETER."Ad Name".DELIMETER."Ad Title".DELIMETER."Ad Short Description".DELIMETER."Ad Long Description".DELIMETER."Display URL".DELIMETER."Destination URL".DELIMETER."Watch List".DELIMETER."Campaign ID".DELIMETER."Campaign Description".DELIMETER."Campaign Start Date".DELIMETER."Campaign End Date".DELIMETER."Ad Group ID".DELIMETER."Ad Group: Optimize Ad Display".DELIMETER."Ad ID".DELIMETER."Keyword ID".DELIMETER."Checksum".DELIMETER."Error Message\n";
		$groupcount = 0;
		printCampaignRow();
		printAdGroupRow($_POST['adgroup']);
		printAdRow($_POST['adgroup']);
		$counter = 0;
		while ($rs->fetch_array())
		{
			printKeywordRow($rs->myarray);
			$counter++;
			if ($counter % 1000 == 0 && $counter != 0)
			{
				$groupcount++;
				$adgroupname = $_POST['adgroup']."_".$groupcount;
				printAdGroupRow($adgroupname);
				printAdRow($adgroupname);
			}
		}
	}
	
	function printKeywordRow($rsarray)
	{
		extract($_POST);

		$location = $rsarray["location"];
		$state_abrv = $rsarray["state_abrv"];
		
		if ($locationtype == "cs") { //city and state
	
			$mystate = get_state_name($state_abrv);
	
			if ($city_pos == "before")
				$search_string = "$mystate $location $search_term ";
			else 
				$search_string = "$search_term $location $mystate";
		
		} else if ($locationtype == "csa") { //city and state abrv
	
			if ($city_pos == "before")
				$search_string = "$state_abrv $location $search_term ";
			else 
				$search_string = "$search_term $location $state_abrv";
		
		} else {
	
			if ($city_pos == "before")
				$search_string = "$location $search_term ";
			else 
				$search_string = "$search_term $location";			
		}
			
		echo "$campaign".DELIMETER."$adgroup".DELIMETER.""; 
		echo "Keyword".DELIMETER."On".DELIMETER."";
		echo "$search_string".DELIMETER."$search_string".DELIMETER."$url".DELIMETER."$standardbid".DELIMETER."$standardbidlimit".DELIMETER."$standardbidstatus".DELIMETER."Advanced".DELIMETER."$contentbid".DELIMETER."$contentbidlimit".DELIMETER."$contentbidstatus".DELIMETER."";					
		echo skip(6).$watchlist.DELIMETER.$campaignid.DELIMETER."\n";
	}

	function skip($int)
	{
		$output = "";
		for ($i =0; $i < $int; $i++) $output .= DELIMETER;
		return $output;
	}

	function printAdRow($adgroupname)//FIX
	{
		extract($_POST);
		echo "$campaign".DELIMETER."$adgroupname".DELIMETER.""; 
		echo "Ad".DELIMETER."On".DELIMETER."";
		echo skip(3).$standardbid.skip(2).$standardbidstatus.DELIMETER."Advanced".DELIMETER.$contentbid.skip(2).$contentbidstatus.DELIMETER."";
		echo "$ad".DELIMETER."$title".DELIMETER."$shortdescription".DELIMETER."$longdescription".DELIMETER."displayurl".skip(3).$campaignid.DELIMETER."\n";					
	}

	function printAdGroupRow($adgroupname)
	{
		extract($_POST);
		echo "$campaign".DELIMETER."$adgroupname".DELIMETER.""; 
		echo "Ad Group".DELIMETER."On".DELIMETER."";
		echo skip(3).$standardbid.skip(2).$standardbidstatus.DELIMETER."Advanced".DELIMETER.$contentbid.skip(2).$contentbidstatus.skip(7).$watchlist.DELIMETER.$campaignid.DELIMETER."\n";					
	}
	function printCampaignRow()
	{
		extract($_POST);
		echo "$campaign".skip(2)."";
		echo "Campaign".DELIMETER."On".DELIMETER."";
		echo skip(5).$standardbidstatus.DELIMETER."Advanced".skip(3).$contentbidstatus.skip(7).$watchlist.DELIMETER.$campaignid.DELIMETER."\n";
	}	
?>