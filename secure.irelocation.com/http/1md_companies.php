<?
# default to show companies with overdue balances

$keywords=strtolower($keywords);

if(isset($display))
{
  $sql_select="distinct(company.comp_name),company.comp_id,company.contact_name,company.phone_num,company.phone_ext,company.city,company.state,company.active,company.url,company.contact_email";
  $sql_from="company";
  $sql_where="company.comp_id!=''";
  $sql_order="company.comp_name";

  //generate sql
  if($lead_campaigns=='yes')
  {
    $sql_select.=",directleads.lead_id";
    $sql_where.=" and directleads.comp_id=company.comp_id";
    $sql_from.=",directleads";
    if($active_leads=='auto')
    {
      $sql_where.=" and directleads.cat_id='1' and directleads.advantage='1' and directleads.active='1'";
    }
    elseif($active_leads=='moving')
    {
      $sql_where.=" and directleads.cat_id='2' and directleads.advantage='1' and directleads.active='1'";
    }
  }
  elseif($active_leads=='auto')
  {
    $sql_select.=",directleads.lead_id";
    $sql_where.=" and directleads.comp_id=company.comp_id and directleads.cat_id='1' and directleads.advantage='1' and directleads.active='1'";
    $sql_from.=",directleads";
  }
  elseif($active_leads=='moving')
  {
    $sql_select.=",directleads.lead_id";
    $sql_where.=" and directleads.comp_id=company.comp_id and directleads.cat_id='2' and directleads.advantage='1' and directleads.active='1'";
    $sql_from.=",directleads";
  }

  if($category!='')
  {
    $sql_where.=" and directlistings.comp_id=company.comp_id and directlistings.cat_id='$category'";
    $sql_from.=",directlistings";
  }

  if($category_state!='')
  {
    if($category!='')
    {
      $sql_where.=" and directlistings.state='$category_state'";
    }
    else
    {
      $sql_from.=",directlistings";
      $sql_where.=" and directlistings.comp_id=company.comp_id and directlistings.state='$category_state'";
    }
  }

  if(trim($keywords)!='')
  {
    $sql_where.=" and ((company.comp_id='$keywords') or (lower(company.comp_name) like '%$keywords%') or (lower(company.contact_name) like '%$keywords%') or (lower(company.contact_email) like '%$keywords%') or (lower(company.url) like '%$keywords%'))";
  }

  if($company_state!='')
  {
    $sql_where.=" and company.state='$company_state'";
  }
}
else
{
  $sql_select="distinct(company.comp_name),sum(billing.amount) as mysum,company.comp_id,company.contact_name,company.phone_num,company.phone_ext,company.city,company.state,company.active,company.url,company.contact_email";
  $sql_from="company,billing";
  $sql_where="company.comp_id=billing.comp_id and billing.status!='new' and billing.date<='".date("Y-m-d")."' group by company.comp_id";
  $sql_order="company.comp_name";

  $flagged_only=1;
}

//generate recordset of companies flagged if billing is overdue
$rs=new mysql_recordset("select $sql_select from $sql_from where $sql_where order by $sql_order limit 400");
//echo "<b>select $sql_select from $sql_from where $sql_where order by $sql_order</b><br>";

$therowcount=$rs->rowcount();

if($rs->rowcount()==400)
{
  $msg="To many records!  Results limited to 400!";
}

//get the list of categories
$rs_cat=new mysql_recordset("SELECT a.cat_id,a.name as cat_name,b.name as par_name FROM directlistingscategory as a, directlistingscategory as b WHERE a.parent_id=b.cat_id and a.link_url='' and a.parent_id!='0' order by b.name,a.name");
while($rs_cat->fetch_array())
{
  $mycategories[$rs_cat->myarray["cat_id"]]["id"]=$rs_cat->myarray["cat_id"];
  $mycategories[$rs_cat->myarray["cat_id"]]["name"]=$rs_cat->myarray["par_name"] . " > " . $rs_cat->myarray["cat_name"];
}

# generate a list of states
$mystates[]='AL';
$mystates[]='AK';
$mystates[]='AZ';
$mystates[]='AR';
$mystates[]='CA';
$mystates[]='CO';
$mystates[]='CT';
$mystates[]='DC';
$mystates[]='DE';
$mystates[]='FL';
$mystates[]='GA';
$mystates[]='HI';
$mystates[]='ID';
$mystates[]='IL';
$mystates[]='IN';
$mystates[]='IA';
$mystates[]='KS';
$mystates[]='KY';
$mystates[]='LA';
$mystates[]='ME';
$mystates[]='MD';
$mystates[]='MA';
$mystates[]='MI';
$mystates[]='MN';
$mystates[]='MS';
$mystates[]='MO';
$mystates[]='MT';
$mystates[]='NE';
$mystates[]='NV';
$mystates[]='NH';
$mystates[]='NJ';
$mystates[]='NM';
$mystates[]='NY';
$mystates[]='NC';
$mystates[]='ND';
$mystates[]='OH';
$mystates[]='OK';
$mystates[]='OR';
$mystates[]='PA';
$mystates[]='RI';
$mystates[]='SC';
$mystates[]='SD';
$mystates[]='TN';
$mystates[]='TX';
$mystates[]='UT';
$mystates[]='VT';
$mystates[]='VA';
$mystates[]='WA';
$mystates[]='WV';
$mystates[]='WI';
$mystates[]='WY';
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Manage Companies</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script language=JavaScript>
var checkboxName = 'checkbox'
var emailName = 'emailto'
var isCheck = true;

function checkall(form) {
  for (var i = 1; true; i++){
    if(form.elements[checkboxName+i] == null)
      break;
    form.elements[checkboxName+i].checked = isCheck;
  }
  isCheck = !isCheck;
}

function open_action_win(url) {
  window.open(url,"new_window",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=680,height=500');
}

function orderby(form,myorder)
{
  var hiddenName = 'order'
  form.elements[hiddenName].value = myorder
  form.submit()
}

function popup(form, page) {
  var url = page
  var somethingChecked = false
  var nothingMsg = 'No listings selected.'
  for (var i = 1; true; i++) {
    if(form.elements[checkboxName+i] == null){
      break;
    }
    if (form.elements[checkboxName+i].checked == true) {
      if (somethingChecked) {
        url += "," + form.elements[checkboxName+i].value
      }
      else {
        url += "&comp_ids=" + form.elements[checkboxName+i].value
        somethingChecked = true
      }
    }
  }
  if (somethingChecked) {
    //alert(url)
    open_action_win(url)
  }
  else {
    alert(nothingMsg)
  }
}
</script>
</head>

<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"></script>
<p><font face="verdana" color="#2360A5" size="3"><b>
    Manage Companies</b></font></p>
<form name="display" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies">
<input type="hidden" name="display" value="1">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="91"><span class="text_2">Keywords:</span></td>
          <td width="409"><input type="text" name="keywords" value="<?echo $keywords;?>" class="quote_box"> <span class="text_2">(parital name, email, url...)</span></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Category:</span></td>
          <td width="409"><select name="category" class="quote_box"><option value="">All</option>
          <?
          foreach($mycategories as $mycategory)
          {
          ?>
          <option value="<?echo $mycategory["id"];?>"<?if($category==$mycategory["id"]){echo " selected";}?>><?echo $mycategory["name"];?></option>
          <?
          }
          ?>
          </select><span class="text_2">in</span> <select name="category_state" class="quote_box"><option value="">All</option>
          <?
          foreach($mystates as $mystate)
          {
          ?>
          <option value="<?echo $mystate;?>"<?if($category_state==$mystate){echo " selected";}?>><?echo $mystate;?></option>
          <?
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Company Location:</span></td>
          <td width="409"><select name="company_state" class="quote_box"><option value="">All</option>
          <?
          foreach($mystates as $mystate)
          {
          ?>
          <option value="<?echo $mystate;?>"<?if($company_state==$mystate){echo " selected";}?>><?echo $mystate;?></option>
          <?
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Lead Campaigns:</span></td>
          <td width="409"><input type="checkbox" name="lead_campaigns" value="yes"<?if($lead_campaigns=='yes'){echo " checked";}?>><span class="text_2">yes</span>&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="radio" name="active_leads" value="any"<?if($active_leads!='auto' && $active_leads!='moving'){echo " checked";}?>><span class="text_2">any</span>&nbsp;
          <input type="radio" name="active_leads" value="auto"<?if($active_leads=='auto'){echo " checked";}?>><span class="text_2">active auto</span>&nbsp;
          <input type="radio" name="active_leads" value="moving"<?if($active_leads=='moving'){echo " checked";}?>><span class="text_2">active moving</span></td>
        </tr>
        <tr>
          <td width="91">&nbsp;</td>
          <td width="409" align="right"><input type="submit" name="submit" value="Display Companies" class="quote_box"></td>
        </tr>
      </table>
    </span>
  <p></p>
<?
if($rs->rowcount()>0)
{
?>
<table border="0" cellspacing="0" width="585" cellpadding="0">
  <tr>
    <td width="585" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="585">
      <tr>
        <td width="585" bgcolor="#A9CAEB" colspan="3">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
            <img src="images/arrow_top.gif" width="25" height="16"><a href="control.php?action=1md_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=1md_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=1md_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="5" bgcolor="#A9CAEB" valign="bottom">
        <p align="center">
        <a onClick="checkall(document.display); return false;" href="#">
        <img alt src="images/checkall.gif" border="0" width="16" height="24">
        </a></td>
        <td width="240" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b>company</b></font></td>
        <td width="340" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b>options</b></font></td>
      </tr>
      <?
      $i=0;
      while($rs->fetch_array())
      {
           if($active_leads!='')
           {
             $reveived=date("Ym");
             $rs2=new mysql_recordset("select count(*) as mycount from quotes where received like '$reveived%' and lead_ids like '%".$rs->myarray["lead_id"]."%'");
             $rs2->fetch_array();
             $leadcount=$rs2->myarray["mycount"]; 
             $rs2=new mysql_recordset("select count(*) as mycount from quotes_old where received like '$reveived%' and lead_ids like '%".$rs->myarray["lead_id"]."%'");
             $rs2->fetch_array();
             $leadcount=$leadcount+$rs2->myarray["mycount"];         
           }
       if($flagged_only==1)
       {
         if($rs->myarray["mysum"]>0)
         {
         $i++;           
      ?>
      <tr>
        <td width="5" bgcolor="#A9CAEB"><font size="1" face="Arial">
        <input type="checkbox" value="<?echo $rs->myarray["comp_id"];?>" name="checkbox<?echo $i;?>"></font></td>
        <td width="240" class="quote_col_x" style="cursor:help;" onClick="return overlib('<?echo str_replace("'","",str_replace('"','',$rs->myarray["contact_name"]));?> <br><?echo $rs->myarray["city"];?>, <?echo $rs->myarray["state"];?><br><?echo $rs->myarray["phone_num"];?> <?echo $rs->myarray["phone_ext"];?>', STICKY, RIGHT);" onMouseOut="nd();"><?if($leadcount!=''){echo $leadcount." -";}?><?echo $rs->myarray["comp_name"];?></td>
        <td width="340" class="quote_col_s" align="left"><a href="control.php?action=1md_companies_billing&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bbilling.gif" border="0"></a><a href="control.php?action=1md_companies_listings&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/blistings.gif" border="0"></a><a href="control.php?action=1md_companies_leads&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bleads.gif" border="0"></a><a href="control.php?action=1md_companies_contact&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bcontact.gif" border="0"></a><a href="http://www.1stmovingdirectory.com/user/index.php?useremail=<?echo urlencode($rs->myarray["contact_email"]);?>&masterpass=1" target="_blank"><img src="images/blogin.gif" border="0"></a><a href="http://<?echo $rs->myarray["url"];?>" target="_blank"><img src="images/bwebsite.gif" border="0"></a></td>
      </tr>
      <?
          if(($i%15==0) && ($i!=$therowcount))
          {
          ?>
          <tr>
            <td width="585" bgcolor="#A9CAEB" colspan="3">
                <img src="images/arrow_middle.gif" width="25" height="17"><a href="control.php?action=1md_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=1md_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=1md_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
          </tr>
          <?
          }
         }
       }
       else
       {
         $i++;
      ?>
      <tr>
        <td width="5" bgcolor="#A9CAEB"><font size="1" face="Arial">
        <input type="checkbox" value="<?echo $rs->myarray["comp_id"];?>" name="checkbox<?echo $i;?>"></font></td>
        <td width="240" class="<?if($rs->myarray["active"]=='1'){echo "quote_col_o";}else{echo "quote_col_s";}?>" style="cursor:help;" onClick="return overlib('<?echo str_replace("'","",str_replace('"','',$rs->myarray["contact_name"]));?> <br><?echo $rs->myarray["city"];?>, <?echo $rs->myarray["state"];?><br><?echo $rs->myarray["phone_num"];?> <?echo $rs->myarray["phone_ext"];?>', STICKY, RIGHT);" onMouseOut="nd();"><?if($leadcount!=''){echo $leadcount." -";}?><?echo $rs->myarray["comp_name"];?></td>
        <td width="340" class="quote_col_s" align="left"><a href="control.php?action=1md_companies_billing&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bbilling.gif" border="0"></a><a href="control.php?action=1md_companies_listings&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/blistings.gif" border="0"></a><a href="control.php?action=1md_companies_leads&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bleads.gif" border="0"></a><a href="control.php?action=1md_companies_contact&id=<?echo $rs->myarray["comp_id"];?>" target="right"><img src="images/bcontact.gif" border="0"></a><a href="http://www.1stmovingdirectory.com/user/index.php?useremail=<?echo urlencode($rs->myarray["contact_email"]);?>&masterpass=1" target="_blank"><img src="images/blogin.gif" border="0"></a><a href="http://<?echo $rs->myarray["url"];?>" target="_blank"><img src="images/bwebsite.gif" border="0"></a></td>
      </tr>
      <?
        if(($i%15==0) && ($i!=$therowcount))
        {
        ?>
        <tr>
          <td width="585" bgcolor="#A9CAEB" colspan="3">
              <img src="images/arrow_middle.gif" width="25" height="17"><a href="control.php?action=1md_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=1md_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=1md_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
        </tr>
        <?
        }
       }
      }
      ?>
      <tr>
        <td width="585" colspan="3" bgcolor="#A9CAEB">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%">
            <img src="images/arrow_bottom.gif" width="25" height="16"><a href="control.php?action=1md_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=1md_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=1md_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</form>
</table>
<?
}
?>
</body>

</html>