<?
# default to show all companies

// IMPORTANT: Note that we set a 2 after the sql statement... this tells us to connect to the CS database and no the default 1MD!

$rs=new mysql_recordset("select id,login_email,name,url,contact,active from companies order by name",2);
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Manage Companies</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script language=JavaScript>
var checkboxName = 'checkbox'
var emailName = 'emailto'
var isCheck = true;

function checkall(form) {
  for (var i = 1; true; i++){
    if(form.elements[checkboxName+i] == null)
      break;
    form.elements[checkboxName+i].checked = isCheck;
  }
  isCheck = !isCheck;
}

function open_action_win(url) {
  window.open(url,"new_window",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=680,height=500');
}

function orderby(form,myorder)
{
  var hiddenName = 'order'
  form.elements[hiddenName].value = myorder
  form.submit()
}

function popup(form, page) {
  var url = page
  var somethingChecked = false
  var nothingMsg = 'No listings selected.'
  for (var i = 1; true; i++) {
    if(form.elements[checkboxName+i] == null){
      break;
    }
    if (form.elements[checkboxName+i].checked == true) {
      if (somethingChecked) {
        url += "," + form.elements[checkboxName+i].value
      }
      else {
        url += "&comp_ids=" + form.elements[checkboxName+i].value
        somethingChecked = true
      }
    }
  }
  if (somethingChecked) {
    //alert(url)
    open_action_win(url)
  }
  else {
    alert(nothingMsg)
  }
}
</script>
</head>

<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<div id="overDiv" style="position:absolute; visibility:hidden; z-index:1000;"></div>
<script language="JavaScript" src="overlib.js"></script>
<p><font face="verdana" color="#2360A5" size="3"><b>
    Manage Companies</b></font></p>
<form name="display" method="POST" action="control.php">
<?
if($rs->rowcount()>0)
{
?>
<table border="0" cellspacing="0" width="715" cellpadding="0">
  <tr>
    <td width="715" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="715">
      <tr>
        <td width="715" bgcolor="#A9CAEB" colspan="4">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" valign="top">
            <img src="images/arrow_top.gif" width="25" height="16"><a href="control.php?action=cs_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=cs_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=cs_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="5" bgcolor="#A9CAEB" valign="bottom">
        <p align="center">
        <a onclick="checkall(document.display); return false;" href="#">
        <img alt src="images/checkall.gif" border="0" width="16" height="24">
        </a></td>
        <td width="280" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b>company</b></font></td>
        <td width="215" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b>info</b></font></td>
        <td width="215" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b>options</b></font></td>
      </tr>
      <?
      $i=0;
      while($rs->fetch_array())
      {
      $i++;
      ?>
      <tr>
        <td width="5" bgcolor="#A9CAEB"><font size="1" face="Arial">
        <input type="checkbox" value="<?echo $rs->myarray["id"];?>" name="checkbox<?echo $i;?>"></font></td>
        <td width="280" class="<?if($rs->myarray["active"]=='1'){echo "quote_col_o";}else{echo "quote_col_s";}?>" style="cursor:help;" onClick="return overlib('<?echo $rs->myarray["login_email"];?>', STICKY, RIGHT);" onmouseout="nd();"><?echo $rs->myarray["name"];?></td>
        <td width="215" class="quote_col_s" align="left"><?echo strtoupper($rs->myarray["contact"]);?></td>
        <td width="215" class="quote_col_s" align="left"><a href="https://secure.irelocation.com/carshipping/account.php?username=<?echo urlencode($rs->myarray["login_email"]);?>&masterpass=1" onClick="open_action_win(this.href); return false;"><img src="images/blogin.gif" border="0"></a><a href="<?echo $rs->myarray["url"];?>" onClick="open_action_win(this.href); return false;"><img src="images/bwebsite.gif" border="0"></a><?if($rs->myarray["active"]=='1'){?><a href="control.php?action=cs_deactivate&id=<?echo $rs->myarray["id"];?>"><img src="images/bdeactivate.gif" border="0"></a><?}else{?><a href="control.php?action=cs_reactivate&id=<?echo $rs->myarray["id"];?>"><img src="images/breactivate.gif" border="0"></a><?}?></td>
      </tr>
      <?

       if((($i%15)==0) && $i!=$row_count2)
       {
      ?>
      <tr>
        <td width="715" bgcolor="#A9CAEB" colspan="4><img src="images/arrow_middle.gif" width="25" height="17"><a href="control.php?action=cs_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=cs_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=cs_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
      </tr>
      <?
       }
      }
      ?>
      <tr>
        <td width="715" colspan="4 bgcolor="#A9CAEB">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%">
            <img src="images/arrow_bottom.gif" width="25" height="16"><a href="control.php?action=cs_companies_view" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bview.gif"></a><a href="control.php?action=cs_companies_email" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bemail3.gif"></a><a href="control.php?action=cs_companies_delete" onClick="popup(document.display,this.href); return false;"><img border="0" src="images/bdelete.gif"></a></td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
</form>
</table>
<?
}
?>
</body>

</html>