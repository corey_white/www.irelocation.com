<?
$rs=new mysql_recordset("select * from billing_log where comp_id='".$user["id"]."' order by date");
while($rs->fetch_array())
{
  $results[]=$rs->myarray;
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Billing History</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="40%">
    <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></td>
    <td width="60%">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
      <tr>
        <td width="100%" height="10"></td>
      </tr>
      <tr>
        <td width="100%" bgcolor="#2360A5" height="25">
        <p class="title2" align="center"><font color="#FFFFFF">
        <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
      </tr>
      <tr>
        <td width="100%" height="18"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<br><br>
<?
if(gettype($results)=="array")
{
?>
<table border="0" cellspacing="0" width="550" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="3">
          <font face="verdana" size="2" color="#FFFFFF"><b>&nbsp;Billing history for <?echo $user["login_email"];?> as of <?echo date("m-d-Y");?></b></font>
        </td>
      </tr>
      <tr>
        <td width="20%" bgcolor="#DCDCDC" align="center"><font face="verdana" color="#333333" size="1"><b><font color="#000000">date</font></a></b></font></td>
        <td width="60%" bgcolor="#DCDCDC" align="center"><font face="verdana" color="#333333" size="1"><b><font color="#000000">description</font></a></b></font></td>
        <td width="20%" bgcolor="#DCDCDC" align="center"><font face="verdana" color="#333333" size="1"><b><font color="#000000">amount</font></a></b></font></td>
      </tr>
      <?
      foreach($results as $theresult)
      {
        $total_amount+=$theresult["amount"];
      ?>
      <tr>
        <td width="20%" class="col_s">&nbsp;<?echo $theresult["date"];?></td>
        <td width="60%" class="col_o">&nbsp;<?echo $theresult["description"];?></td>
        <td width="20%" class="col_o">&nbsp;<?printf("%01.2f", $theresult["amount"]);?></td>
      </tr>
      <?
      }
      ?>
      <tr>
        <td width="20%" class="col_d">TOTAL:</td>
        <td width="60%" class="col_d">&nbsp;</td>
        <td width="20%" class="col_d">$<?printf("%01.2f",$total_amount);?></td>
      </tr>
    </table>
   </td>
 </tr>
</table>
<br><br>
<p align="center"><form><input type="button" value="Print this Page!" onclick="window.print();"></form></p>
<?
}
else
{
?>
<p align="center">No records were found!</p>
<?
}
?>
</body>

</html>