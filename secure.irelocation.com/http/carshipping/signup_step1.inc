<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Advertise With Us</title>
</head>

<body>
<form method="post" action="signup.php">
<input type="hidden" name="action" value="step1">
<div align="center">
  <center>
  <table border="0" cellspacing="1" width="705">
    <tr>
      <td width="701">
      <table border="0" cellspacing="0" width="100%" cellpadding="0">
        <tr>
          <td width="40%">
          <a href="http://www.carshipping.com">
          <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></a></td>
          <td width="60%">
          <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
            <tr>
              <td width="100%" height="10"></td>
            </tr>
            <tr>
              <td width="100%" bgcolor="#2360A5" height="25">
              <p class="title2" align="center"><font color="#FFFFFF">
              <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
            </tr>
            <tr>
              <td width="100%" height="18"></td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="specialfield" align="center">Add your site to the
      CarShipping.com Directory!</td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="title2"><font size="3">Step 1 of 3 - Create Your Listing</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">Clearly and accurately describe your site, and emphasize
      any unique benefits of your company.&nbsp; Your title and description
      should have normal sentence capitalization using proper grammar and
      correct spelling.</td>
    </tr>
    <tr>
      <td width="701">
      <p align="center" class="title"><?if($msg!=''){echo $msg;}?></td>
    </tr>
    <tr>
      <td width="701" align="center">
      <table cellspacing="1" width="70%" style="border: 2px dashed; border-color: darkgrey; padding: 4px;" cellpadding="0">
        <tr>
          <td width="100%" bgcolor="#F4F4F4">
          <table border="0" width="100%" cellpadding="2">
            <tr>
              <td width="35%" valign="middle">
              <p class="regtxt">Title / Name of Company</td>
              <td width="65%" valign="top">
              <input type="text" size="32" class="loginfield" name="name" value="<?echo $mylisting["name"];?>"></td>
            </tr>
            <tr>
              <td width="35%" valign="top">
              <p class="regtxt">Description</td>
              <td width="65%" valign="top">
              <p class="regtxt">max 255 characters<br>
              <textarea rows="5" cols="32" class="loginfield" name="description"><?echo $mylisting["description"];?></textarea></td>
            </tr>
            <tr>
              <td width="35%" valign="middle">
              <p class="regtxt">Website Address</td>
              <td width="65%" valign="top"><input type="text" size="32" class="loginfield" name="url" value="<?if($mylisting["url"]!=''){echo $mylisting["url"];}else{echo "http://";}?>"></td>
            </tr>
            <tr>
              <td width="35%" valign="middle">
              <p class="regtxt">Link to Your Quote Page</td>
              <td width="65%" valign="top"><input type="text" size="32" class="loginfield" name="quote_url" value="<?if($mylisting["quote_url"]!=''){echo $mylisting["quote_url"];}else{echo "http://";}?>"></td>
            </tr>
            <tr>
              <td width="35%" valign="middle">
              <p class="regtxt">1-800 Number</td>
              <td width="65%" valign="top"><input type="text" size="20" class="loginfield" name="phone" value="<?if($mylisting["phone"]!=''){echo $mylisting["phone"];}?>"></td>
            </tr>
            <tr>
              <td width="35%" valign="top">
              <p class="regtxt">&nbsp;</td>
              <td width="65%" valign="top">&nbsp;</td>
            </tr>
            <tr>
              <td width="35%" valign="top">
              <p class="regtxt">&nbsp;</td>
              <td width="65%" valign="top">
              <input type="submit" value="Save &amp; Continue" name="Submit" class="loginfield"></td>
            </tr>
            </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="qlinks"><font size="2">Step 2 of 3 - Activate Your Account</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">To activate your account you will need to enter your
      email address and choose a password.&nbsp; </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="qlinks"><font size="2">Step 3 of 3 - Login to Your Account</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">Login to your account manager to choose your categories and
      enter billing information.&nbsp; You may choose to be placed in any category
      on the website.&nbsp; </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701"><p align="center"><span class="regtxt">�2003 iRelocation.com, Inc. - All Rights Reserved - <a href="http://www.carshipping.com/privacy_policy.htm" target="_blank"><span class="redtxt"><u>Privacy Policy</u></span></a></span></p></td>
    </tr>
  </table>
  </center>
</div>

</body>

</html>