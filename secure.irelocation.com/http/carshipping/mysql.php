<?
/*///////////////////////////////////////////////
//(c)2002 Spider Web Technologies, LLC
//Jon Olawski
//MySQL CONNECTION CLASS
//mysql.inc.php
///////////////////////////////////////////////*/

class mysql_recordset{
    var $host;
    var $usr;
    var $pass;
    var $dbname;

    var $db;
    var $rowcount;
    var $result;

    var $myarray;
    var $myrow;
    var $myobject;
    var $myfield;
    var $myfieldinfo;

    var $sql;

    function mysql_recordset($thesql='-1',$pconnect='-1')
    {
      $this->sql = $thesql;

      //include("config.php");
      $this->host = "localhost";
      $this->usr = "admin2";
      $this->pass = "triad8920";
      $this->dbname = "carshipping2";
      $this->myarray = array();
      $this->myrow = array();
      $this->myobject = array();
      $this->myfield = array();
      $this->myfieldinfo = array();

      if($pconnect != '-1')
      {
      $this->pconnect();
      }
      else
      {
      $this->connect();
      }

      if($this->sql != '-1')
      {
      $this->query();
      }

    }

    function connect()
    {
       $this->db = mysql_connect($this->host, $this->usr, $this->pass)
                   or die("Cannot connect to database!");
       mysql_select_db($this->dbname,$this->db);
    }

    function pconnect()
    {
       $this->db = mysql_pconnect($this->host, $this->usr, $this->pass)
                   or die("Cannot connect to database!");
       mysql_select_db($this->dbname,$this->db);
    }

    function close()
    {
       mysql_close($this->db);
    }

    function rowcount()
    {
       //$this->result = mysql_query($this->sql,$this->db);
       $this->rowcount = mysql_num_rows($this->result);
       return $this->rowcount;
    }

    function query()
    {
       $this->result = mysql_query($this->sql,$this->db)
                      or die("Cannot query database. Please contact the administrator.");
    }

    function fetch_array()
    {
        if($this->myarray = mysql_fetch_array($this->result))
        {
          return TRUE;
        }
        else
        {
          return FALSE;
        }
    }

    function fetch_row()
    {
        if($this->myrow = mysql_fetch_row($this->result))
        {
          return TRUE;
        }
        else
        {
          return FALSE;
        }
    }

    function fetch_object()
    {
        if($this->myobject = mysql_fetch_object($this->result))
        {
          return TRUE;
        }
        else
        {
          return FALSE;
        }
    }

    function fetch_field_names()
    {
        $fields = mysql_num_fields($this->result);

        for($i=0;$i<$fields;$i++)
        {
          $this->myfield[$i]=mysql_field_name($this->result,$i);
        }

    }

    function fetch_field_info()
    {
        $fields = mysql_num_fields($this->result);

        for($i=0;$i<$fields;$i++)
        {
          $this->myfieldinfo[$i]=mysql_fetch_field($this->result,$i);
        }

    }

    function movefirst()
    {
        return mysql_data_seek($this->result,'0');
    }


}
?>