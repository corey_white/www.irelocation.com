<?

if($action2=='update_billing')
{
  $rs=new mysql_recordset("select decode(ccinfo,'$xxxkey') as myccinfo,ac,ac_amount from billing where comp_id='".$user["id"]."'");
  if($rs->fetch_array())
  {
    $cc=unserialize($rs->myarray["myccinfo"]);
  }
  else
  {
    $rs=new mysql_recordset("insert into billing (comp_id) values ('".$user["id"]."')");
    $new_billing=1;
  }
  $rs=nothing;

  $cc["cctype"]=$cctype;
  if(substr($ccnum,0,4)!='xxxx'){ $cc["ccnum"]=trim(str_replace(' ','',str_replace('-','',$ccnum))); $showcc=1;}
  $cc["ccexp_month"]=$ccexp_month;
  $cc["ccexp_year"]=$ccexp_year;
  $cc["fname"]=ucwords(strtolower($fname));
  $cc["lname"]=ucwords(strtolower($lname));
  $cc["company"]=ucwords(strtolower($company));
  $cc["address1"]=$address1;
  $cc["address2"]=$address2;
  $cc["city"]=ucwords(strtolower($city));
  $cc["state"]=strtoupper($state);
  $cc["zip"]=intval(trim($zip));
  $cc["phone"]=intval(trim(str_replace(' ','',str_replace('(','',str_replace(')','',str_replace('-','',$phone))))));

  $msg="";
  if($cc["ccnum"]=='') $msg.="-Credit card number missing!<br>";
  if($cc["fname"]=='') $msg.="-First Name missing!<br>";
  if($cc["lname"]=='') $msg.="-Last Name missing!<br>";
  if($cc["address1"]=='') $msg.="-Address missing!<br>";
  if($cc["city"]=='') $msg.="-City missing!<br>";
  if($cc["state"]=='') $msg.="-State missing!<br>";
  elseif(strlen($cc["state"])!=2) $msg.="-State abbr invalid!<br>";
  if($cc["zip"]=='') $msg.="-Zip code missing!<br>";
  elseif(strlen($cc["zip"])!=5) $msg.="-Zip code invalid!<br>";
  if($adminx=='1')
  {
    if($xamount<0) $msg.="-ADMIN: you must enter a billing amount!<br>";
    if($xmonth=='') $msg.="-ADMIN: you must enter a billing month!<br>";
    if($xday=='') $msg.="-ADMIN: you must enter a billing day!<br>";
    if($xyear<date("Y")) $msg.="-ADMIN: you must enter a billing year!<br>";
  }

  if($accept_terms!='1') $msg.="-You must agree to the terms & conditions!<br>";

  if($msg=='')
  {
    $ccinfo=serialize($cc);
    if($adminx=='1')
    {
      $ac_next=mktime(0,0,0,$xmonth,$xday,$xyear);
      $rs=new mysql_recordset("update billing set ccinfo=encode('$ccinfo','$xxxkey'),ac='Y',ac_amount='$xamount',ac_next='$ac_next' where comp_id='".$user["id"]."'");
    }
    else
    {
      $rs=new mysql_recordset("update billing set ccinfo=encode('$ccinfo','$xxxkey'),ac='Y' where comp_id='".$user["id"]."'");
    }

    $showcc=0;
    $msg="Your billing information has been updated!";

    //include("cron_billing.php");
    include("close_and_refresh.htm");
    exit;
  }
  else
  {
    $msg="Your billing information has NOT been updated:<br>".$msg."";
  }

  $ccinfo=nothing;
  $rs=nothing;
}
else
{
  $rs=new mysql_recordset("select decode(ccinfo,'$xxxkey') as myccinfo,ac_amount,ac_next from billing where comp_id='".$user["id"]."'");
  if($rs->fetch_array())
  {
    $cc=unserialize($rs->myarray["myccinfo"]);
    if($cc["ccnum"]=='')
    {
      $msg="No billing information on file!";
    }
    $xamount=$rs->myarray["ac_amount"];
    list($xyear,$xmonth,$xday)=explode("-",date("Y-m-d",$rs->myarray["ac_next"]));
  }
  else
  {
    $msg="No billing information on file!";
  }
  $rs=nothing;
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Account Billing Information</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="40%">
    <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></td>
    <td width="60%">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
      <tr>
        <td width="100%" height="10"></td>
      </tr>
      <tr>
        <td width="100%" bgcolor="#2360A5" height="25">
        <p class="title2" align="center"><font color="#FFFFFF">
        <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
      </tr>
      <tr>
        <td width="100%" height="18"><p align="right"><img src="images/authorizenet.gif" border="0"> <img src="images/geotrust.gif" border="0"></p></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?
if($msg!='')
{
  echo "<p class=\"notice\">$msg</p><br>";
}
?>
<form name="summary" method="POST" action="account.php">
<input type="hidden" name="action" value="account_billing">
<input type="hidden" name="action2" value="update_billing">
<?if($new_billing=='1'){?><input type="hidden" name="new_billing" value="1"><?}?>
<table width="100%">
  <tr>
    <td>
    <table>
      <tr>
        <td vAlign="top">
          <table width="300">
            <tr>
              <td class="regtxt" colspan="2" width="300">
              <p class="title"><b>Billing information:<br>
&nbsp;</b></td>
            </tr>
            <tr>
              <td class="regtxt" width="150">Type of credit card:</td>
              <td width="150">
                <select name="cctype"<?if($agent_level!='low'){?>class="loginfield"<?}?>>
                <option value="visa"<?if($cc["cctype"]=='visa'){echo " selected";}?>>Visa</option>
                <option value="mc"<?if($cc["cctype"]=='mc'){echo " selected";}?>>MasterCard</option>
                </select>
              </td>
            </tr>
            <tr>
              <td class="regtxt" width="150">Credit card number:</td>
              <td width="150"><input name="ccnum" value="<?if($cc["ccnum"]!=''){if($showcc=='1'){echo $cc["ccnum"];}else{echo "xxxxxxxxxxxx".substr($cc["ccnum"],-4)."";}}?>" size="22"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
            </tr>
            <tr>
              <td class="regtxt" width="150">Expiration date:</td>
              <td width="150">
                <select name="ccexp_month"<?if($agent_level!='low'){?>class="loginfield"<?}?>>
                <option value="01"<?if($cc["ccexp_month"]=='01'){echo " selected";}?>>Jan</option>
                <option value="02"<?if($cc["ccexp_month"]=='02'){echo " selected";}?>>Feb</option>
                <option value="03"<?if($cc["ccexp_month"]=='03'){echo " selected";}?>>Mar</option>
                <option value="04"<?if($cc["ccexp_month"]=='04'){echo " selected";}?>>Apr</option>
                <option value="05"<?if($cc["ccexp_month"]=='05'){echo " selected";}?>>May</option>
                <option value="06"<?if($cc["ccexp_month"]=='06'){echo " selected";}?>>Jun</option>
                <option value="07"<?if($cc["ccexp_month"]=='07'){echo " selected";}?>>Jul</option>
                <option value="08"<?if($cc["ccexp_month"]=='08'){echo " selected";}?>>Aug</option>
                <option value="09"<?if($cc["ccexp_month"]=='09'){echo " selected";}?>>Sep</option>
                <option value="10"<?if($cc["ccexp_month"]=='10'){echo " selected";}?>>Oct</option>
                <option value="11"<?if($cc["ccexp_month"]=='11'){echo " selected";}?>>Nov</option>
                <option value="12"<?if($cc["ccexp_month"]=='12'){echo " selected";}?>>Dec</option>
                </select> <select name="ccexp_year"<?if($agent_level!='low'){?>class="loginfield"<?}?>>
                <?
                for($i=date("Y");$i<date("Y")+10;$i++)
                {
                ?><option value="<?echo $i;?>"<?if($cc["ccexp_year"]==$i){echo " selected";}?>><?echo $i;?></option>
                <?
                }
                ?>
                </select>
              </td>
            </tr>
            <?if($adminx=='1'){?>
            <tr>
              <td class="redtxt" width="150">Amount to charge:</td>
              <td width="150">$<input name="xamount" value="<?if($xamount!=''){echo $xamount;}?>" size="10" class="loginfield"></td>
            </tr>
            <tr>
              <td class="redtxt" width="150">Start auto-billing:</td>
              <td width="150"><input name="xmonth" value="<?if($xmonth!=''){echo $xmonth;}?>" size="3" maxlength="2" class="loginfield">-<input name="xday" value="<?if($xday!=''){echo $xday;}?>" size="3" maxlength="2" class="loginfield">-<input name="xyear" value="<?if($xyear!=''){echo $xyear;}?>" size="5" maxlength="4" class="loginfield"> (mm-dd-yyyy)</td>
            </tr>
            <?}?>
          </table>
        </td>
      </tr>
    </table>
    </td>
  </tr>
  <tr>
    <td>
      <table width="361">
        <tr>
          <td colSpan="2" width="319">&nbsp;<p class="title"><b>Billing address:</b><br>&nbsp;</td>
        </tr>
        <tr>
          <td class="regtxt" width="112">First Name:</td>
          <td width="239"><input size="30" value="<?echo $cc["fname"];?>" name="fname"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Last Name:</td>
          <td width="239"><input size="30" value="<?echo $cc["lname"];?>" name="lname"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Company name: </td>
          <td width="239"><input size="30" value="<?echo $cc["company"];?>" name="company"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Address line 1: </td>
          <td width="239"><input size="30" value="<?echo $cc["address1"];?>" name="address1"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Address line 2: </td>
          <td width="239"><input size="30" value="<?echo $cc["address2"];?>" name="address2"<?if($agent_level!='low'){?>class="loginfield"<?}?>></div>
          </td>
        </tr>
        <tr>
          <td class="regtxt" width="112">City: </td>
          <td width="239">
          <input size="19" value="<?echo $cc["city"];?>" name="city"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">State: </td>
          <td width="239">
          <input size="6" value="<?echo $cc["state"];?>" name="state"<?if($agent_level!='low'){?>class="loginfield"<?}?> maxlength="2"></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Zip code: </td>
          <td width="239"><input size="10" value="<?echo $cc["zip"];?>" name="zip"<?if($agent_level!='low'){?>class="loginfield"<?}?> maxlength="5"></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">Phone number: </td>
          <td width="239"><input size="30" value="<?echo $cc["phone"];?>" name="phone"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">&nbsp;</td>
          <td width="239">&nbsp;</td>
        </tr>
        <tr>
          <td class="regtxt" colspan="2" width="319">Please review the following
          terms &amp; conditions:<br>
          <textarea rows="5" name="terms" cols="50"<?if($agent_level!='low'){?>class="loginfield"<?}?>><?readfile("terms.txt");?></textarea></td>
        </tr>
        <tr>
          <td class="regtxt" width="112">
          <input type="checkbox" name="accept_terms" value="1" style="float: right"></td>
          <td width="239">
          <p class="regtxt">I accept the terms &amp; conditions</td>
        </tr>
        <tr>
          <td class="regtxt" width="112">&nbsp;</td>
          <td width="239">&nbsp;</td>
        </tr>
        <tr>
          <td class="regtxt" width="112">&nbsp;</td>
          <td width="239"><input type="submit" value="Update Billing Information" name="submit"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>

</html>