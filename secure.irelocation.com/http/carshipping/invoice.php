<?

class success_invoice{

  function success_invoice($id,$to,$for,$amount,$xkey)
  {
    include_once("mysql.php");
    $rs=new mysql_recordset("select decode(ccinfo,'$xkey') as myccinfo from billing where comp_id='$id'");
    $rs->fetch_array();
    $ccinfo=unserialize($rs->myarray["myccinfo"]);

    $body="Dear CarShipping.com Client:\n\nYour transaction was successful!  We processed a transaction on ".date("F j, Y")." and deposited \$$amount to your account. The details of your transaction are as follows:\n\nAccount Login: $to\n\nCard Holder Name: ".$ccinfo["fname"]." ".$ccinfo["lname"]."\nCredit Card Type: ".$ccinfo["cctype"]."\nCredit Card: XXXX-XXXX-XXXX-".substr($ccinfo["ccnum"],-4)."\nExpiration Date: ".$ccinfo["ccexp_month"]."/".$ccinfo["ccexp_year"]."\nAmount: \$$amount\n\nPlease retain/print a copy of this e-mail for your records.  If you have any billing questions, please contact us at billing@carshipping.com.\n\nWe appreciate your business!\n\nSincerely,\n\nYour partners at The iRelocation Network\n\nwww.CarShipping.com\n\n";
    mail($to, $for, $body,'From: "CarShipping.com" <admin@carshipping.com>');
  }

}

class failed_invoice{

  function failed_invoice($id,$to,$for,$amount,$xkey)
  {
    $body="Dear CarShipping.com Client:\n\nWe attempted to process your card on ".date("F j, Y")." for \$$amount. Unfortunately, we were not able to complete the transaction because the credit card you provided was invalid.  This typically occurs when there is a problem with the credit card we have on file (i.e. the credit card has expired, or it has reached its credit limit).\n\nPlease login to your account and update your billing information so that we can reactivate your account, at http://www.carshipping.com\n\nIf you have any billing questions, please contact us at billing@carshipping.com.\n\nWe appreciate your business!\n\nSincerely,\n\nYour partners at The iRelocation Network\n\nwww.CarShipping.com\n\n";
    mail($to, $for, $body,'From: "CarShipping.com" <admin@carshipping.com>');
  }

}

?>