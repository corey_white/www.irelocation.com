<?
#
# If no category is specified, exit
#
if(!isset($c)){exit;}

#
# Get current category name
#
$rs=new mysql_recordset("select category from categories where id='$c'");
$rs->fetch_array();
$category=$rs->myarray["category"];

#
# Get listings in current category
#
$sql="
select
  companies.name,
  companies.url,
  companies.quote_url,
  companies.description,
  bids.id,
  bids.bid,
  bids.timestamp
from
  companies,
  categories,
  bids
where
  companies.id = bids.comp_id and
  bids.cat_id = categories.id and
  categories.id = '$c' and
  companies.balance >= bids.bid
order by
  bids.bid desc,
  bids.timestamp asc
";
$rs=new mysql_recordset($sql);

$i=0;
while($rs->fetch_array())
{
  $mylisting[$i]=$rs->myarray;
  $i++;
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>More Bids for <?echo $category;?></title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="40%">
    <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></td>
    <td width="60%">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
      <tr>
        <td width="100%" height="10"></td>
      </tr>
      <tr>
        <td width="100%" bgcolor="#2360A5" height="25">
        <p class="title2" align="center"><font color="#FFFFFF">
        <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
      </tr>
      <tr>
        <td width="100%" height="18"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?
if($msg!='')
{
  echo "<p class=\"notice\">$msg</p><br>";
}
?>
<p class="title"><b><?echo $category;?> Bids:<br>&nbsp;</b></p>
<blockquote>
<p class="regtxt">
<?
$i=1;
if(gettype($mylisting)=='array')
{
foreach($mylisting as $thelisting)
{
?>
<?echo $i;?>. ($<?echo $thelisting["bid"]?>) <?echo $thelisting["name"]?><br>
<?
$i++;
}
}
else
{
  echo "No results found!";
}
?>
</p>
</blockquote>
</body>

</html>
