<?
if(!isset($date_type))
{
  $date_type=1;
  $date_range="month";
}

if($date_type=='1')
{
  switch($date_range)
  {
    case "today":
      $date_start_month=date("m");
      $date_start_day=date("d");
      $date_start_year=date("Y");
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "yesterday":
      $start=time()-86400;
      $end=time()-86400;
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m",$end);
      $date_end_day=date("d",$end);
      $date_end_year=date("Y",$end);
      break;
    case "week":
      $start=time()-518400;
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "month":
      $start=mktime(0,0,0,date("n"),1,date("Y"));
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "lastmonth":
      $start=mktime(0,0,0,date("n")-1,1,date("Y"));
      $end=mktime(0,0,0,date("n"),0,date("Y"));
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m",$end);
      $date_end_day=date("d",$end);
      $date_end_year=date("Y",$end);
      break;
    case "all":
      $date_start_month=date("m",0);
      $date_start_day=date("d",0);
      $date_start_year=date("Y",0);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
  }
}

# get category names
$category=array();
$sql="select * from categories";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{
  $category[$rs->myarray["id"]]=$rs->myarray["category"];
}

$sql="select cat_id,sum(clicks) as totalclicks from log where comp_id='".$user["id"]."' and date>='$date_start_year-$date_start_month-$date_start_day' and date<='$date_end_year-$date_end_month-$date_end_day' group by cat_id order by totalclicks desc";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{
  $results[]=$rs->myarray;
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Daily Account Summary</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script language=JavaScript>
function enabled(set)
{
  var f = document.forms.summary;
  f["date_range"].disabled = !set;
  f["date_start_month"].disabled = set;
  f["date_start_day"].disabled = set;
  f["date_start_year"].disabled = set;
  f["date_end_month"].disabled = set;
  f["date_end_day"].disabled = set;
  f["date_end_year"].disabled = set;
}
</script>
</head>

<body>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="40%">
    <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></td>
    <td width="60%">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
      <tr>
        <td width="100%" height="10"></td>
      </tr>
      <tr>
        <td width="100%" bgcolor="#2360A5" height="25">
        <p class="title2" align="center"><font color="#FFFFFF">
        <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
      </tr>
      <tr>
        <td width="100%" height="18"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<form name="summary" method="POST" action="account.php">
<input type="hidden" name="action" value="account_summary">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2" align="center">
        <tr>
          <td width="91"><span class="regtxt">Date Range:</span></td>
          <td width="409"><input type="radio" name="date_type" value="1"<? if($date_type=='1'){echo " checked";}?> onclick="enabled(true)"><select size="1" name="date_range" class="loginfield">
          <option value="today"<? if($date_range=='today'){echo " selected";}?>>Today</option>
          <option value="yesterday"<? if($date_range=='yesterday'){echo " selected";}?>>Yesterday</option>
          <option value="week"<? if($date_range=='week'){echo " selected";}?>>Last 7 Days</option>
          <option value="month"<? if($date_range=='month'){echo " selected";}?>>This Month</option>
          <option value="lastmonth"<? if($date_range=='lastmonth'){echo " selected";}?>>Last Month</option>
          <option value="all"<? if($date_range=='all'){echo " selected";}?>>All Time</option>
          </select></td>
        </tr>
        <tr>
          <td width="91">&nbsp;</td>
          <td width="409"><input type="radio" name="date_type" value="2"<? if($date_type=='2'){echo " checked";}?> onclick="enabled(false)"><select size="1" name="date_start_month" class="loginfield">
          <? for($i=1;$i<=12;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_start_month==$i){echo " selected";}?>><? echo date("M",mktime(0,0,0,$i,1,2002));?></option>
          <?}?></select>
          <select size="1" name="date_start_day" class="loginfield">
          <? for($i=1;$i<=31;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_start_day==$i){echo " selected";}?>><? echo $i;?></option>
          <?}?></select>
          <select size="1" name="date_start_year" class="loginfield">
          <? for($i=(date("Y")-2);$i<=date("Y");$i++){?>
          <option value="<? echo $i;?>"<? if($date_start_year==$i){echo " selected";}?>><? echo $i;?></option>
          <?}?></select> -
          <select size="1" name="date_end_month" class="loginfield">
          <? for($i=1;$i<=12;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_end_month==$i){echo " selected";}?>><? echo date("M",mktime(0,0,0,$i,1,2002));?></option>
          <?}?></select>
          <select size="1" name="date_end_day" class="loginfield">
          <? for($i=1;$i<=31;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_end_day==$i){echo " selected";}?>><? echo $i;?></option>
          <? }?></select>
          <select size="1" name="date_end_year" class="loginfield">
          <? for($i=(date("Y")-2);$i<=date("Y");$i++){?>
          <option value="<? echo $i;?>"<? if($date_end_year==$i){echo " selected";}?>><? echo $i;?></option>
          <? }?></select></td>
        </tr>
        <tr>
          <td width="91">&nbsp;</td>
          <td width="409" align="right"><input type="submit" name="display" value="Display Report" class="loginfield"></td>
        </tr>
      </table>
    </span>
<script language=JavaScript>
<? if($date_type=='1'){?>
enabled(true);
<? }else{?>
enabled(false);
<? }?>
</script>
<br>
<br>
<?
if(gettype($results)=="array")
{
?>
<table border="0" cellspacing="0" width="400" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="2">
          <font face="verdana" size="2" color="#FFFFFF"><b>&nbsp;Summary for <? echo date("M",mktime(0,0,0,$date_start_month,1,2002));?> <?echo $date_start_day;?>, <?echo $date_start_year;?> to <?echo date("M",mktime(0,0,0,$date_end_month,1,2002));?> <?echo $date_end_day;?>, <?echo $date_end_year;?></b></font>
        </td>
      </tr>
      <tr>
        <td width="75%" bgcolor="#DCDCDC" align="center"><font face="verdana" color="#333333" size="1"><b><font color="#000000">category</font></a></b></font></td>
        <td width="25%" bgcolor="#DCDCDC" align="center"><font face="verdana" color="#333333" size="1"><b><font color="#000000">number of clicks</font></a></b></font></td>
      </tr>
      <?
      foreach($results as $theresult)
      {
      $total_clicks+=$theresult["totalclicks"];
      $total_cost+=$theresult["totalcost"];

      ?>
      <tr>
        <td width="75%" class="col_s">&nbsp;<?echo $category[$theresult["cat_id"]];?></td>
        <td width="25%" class="col_o">&nbsp;<?echo $theresult["totalclicks"];?></td>
      </tr>
      <?
      }
      ?>
      <tr>
        <td width="75%" class="col_d">&nbsp;TOTAL:</td>
        <td width="25%" class="col_d">&nbsp;<?echo $total_clicks;?></td>
      </tr>
    </table>
   </td>
 </tr>
</table>
<?
}
else
{
?>
<p align="center">No records were found for the specified dates!</p>
<?
}
?>
</form>
</body>

</html>