<?
include("mysql.php");
session_register("listing");

if($listing!='') $mylisting=unserialize($listing);

function cleantxt($txt)
{
  $txt=strip_tags($txt);
  $replace=array("'","\"","*","!","#","%","^","`","~","<",">","\n","\r","&quot","&#34","&amp","&#38","&lt","&#60","&gt","&#62","&nbsp","&#160","&iexcl",chr(161),"&cent",chr(162),"&pound",chr(163),"&copy",chr(169),"&#(\d+);'e","&");
  foreach($replace as $key)
  {
    $txt=str_replace($key,"",$txt);
  }
  return $txt;
}

if($action=='step1')
{
  #
  # Verify / Clean entries
  #
  if($name=='')
  {
    $msg.="Title is Missing!<br>";
  }
  else
  {
    $name=cleantxt($name);
    $name=substr($name,0,50);
  }

  if($phone=='')
  {
    $msg.="Phone is Missing!<br>";
  }
  else
  {
    $phone=cleantxt($phone);
    $phone=str_replace("-","",$phone);
    $phone=str_replace("(","",$phone);
    $phone=str_replace(")","",$phone);
    $phone=str_replace("{","",$phone);
    $phone=str_replace("}","",$phone);
    $phone=str_replace("[","",$phone);
    $phone=str_replace("]","",$phone);
    $phone=str_replace(" ","",$phone);
    if(substr($phone,0,1)=='1')
    {
      $phone=substr($phone,1);
    }
  }

  if($description=='')
  {
    $msg.="Description is Missing!<br>";
  }
  else
  {
    $description=cleantxt($description);
    $description=substr($description,0,255);
  }

  if($url=='')
  {
    $msg.="Website Address is Missing!<br>";
  }
  else
  {
    $url=cleantxt($url);
    $url=str_replace(" ","",trim($url));
    $url=substr($url,0,255);
    if((substr_count($url,"http")==0) || (strlen($url)<11))
    {
      $msg.="Invalid Website Address!<br>";
    }
  }

  if($quote_url=='')
  {
    $msg.="Quote Page Link is Missing!<br>";
  }
  else
  {
    $quote_url=cleantxt($quote_url);
    $quote_url=str_replace(" ","",trim($quote_url));
    $quote_url=substr($quote_url,0,255);
    if((substr_count($quote_url,"http")==0) || (strlen($quote_url)<11))
    {
      $msg.="Invalid Quote Page Link!<br>";
    }
  }

  #
  # Save listing to session
  #
  $mylisting["name"]=$name;
  $mylisting["description"]=$description;
  $mylisting["url"]=$url;
  $mylisting["quote_url"]=$quote_url;
  $mylisting["phone"]=$phone;
  $listing=serialize($mylisting);

  #
  # Show Step 2
  #
  if($msg=='')
  {
    $step=2;
  }
  else
  {
    $step=1;
  }
}
elseif($action=='step2')
{
  #
  # Verify / Clean entries
  #
  $login_email=strtolower($login_email);
  $login_email2=strtolower($login_email2);

  if($login_email=='')
  {
    $msg.="Email Address Missing!<br>";
  }
  else
  {
    $login_email=cleantxt($login_email);
    $login_email2=cleantxt($login_email2);
    $login_email=str_replace(" ","",trim($login_email));
    $login_email2=str_replace(" ","",trim($login_email2));
    if($login_email=='')
    {
      $msg.="Email Address Missing!<br>";
    }
    elseif((substr_count($login_email,"@")==0) || (strlen($login_email)<9))
    {
      $msg.="Invalid Email Address!<br>";
    }
  }

  if($login_email!=$login_email2)
  {
    $msg.="Email Addresses Do Not Match!<br>";
  }

  $rs=new mysql_recordset("select * from companies where login_email='$login_email'");
  if($rs->rowcount()>0)
  {
    $msg.="That Email Already Exists In Our Database!<br>";
  }

  $temp_password=$password;
  $password=cleantxt($password);
  if($temp_password!=$password)
  {
    $msg.="Password Contains Illegal Characters. Please Try Again!<br>";
  }

  if((strlen($password)<4) || (strlen($password)>10))
  {
    $msg.="Password Must Be 4 to 10 Characters!<br>";
  }

  if($contact_name=='')
  {
    $msg.="Please Enter Your Name!<br>";
  }
  else
  {
    $contact_name=cleantxt($contact_name);
  }
  if($contact_phone=='')
  {
    $msg.="Please Enter Your Phone Number!<br>";
  }
  else
  {
    $contact_phone=cleantxt($contact_phone);
  }

  #
  # Save listing to session
  #
  $mylisting["login_email"]=$login_email;
  $mylisting["login_email2"]=$login_email2;
  $mylisting["contact_name"]=$contact_name;
  $mylisting["contact_phone"]=$contact_phone;
  $mylisting["pass_length"]=strlen($password);

  $listing=serialize($mylisting);

  if($msg=='')
  {
    #
    # Insert Company into database
    #
    $rs=new mysql_recordset("insert into companies (login_email,password,name,phone,url,quote_url,description,contact) values ('".$mylisting["login_email"]."',PASSWORD('$password'),'".$mylisting["name"]."','".$mylisting["phone"]."','".$mylisting["url"]."','".$mylisting["quote_url"]."','".$mylisting["description"]."','".$mylisting["contact_name"]."-".$mylisting["contact_phone"]."')");

    #
    # Email us a copy for validation
    #
    mail("brian@irelocation.com,jon@irelocation.com,chris@irelocation.com","".$mylisting["name"]." has added their site to CarShipping.com!","Please verify the following information:\n\n".$mylisting["contact_name"]."\n".$mylisting["contact_phone"]."\n\n".$mylisting["name"]."\n".$mylisting["description"]."\nWebsite Address: ".$mylisting["url"]."\nQuote Address: ".$mylisting["quote_url"]."\n1-800 Number: ".$mylisting["phone"]."\n\n","From: CarShipping.com <no_reply@carshipping.com>");

    #
    # Email the company
    #
    mail($mylisting["login_email"],"Welcome to CarShipping.com!","Dear ".$mylisting["contact_name"].",\n\nThank you for adding your company to CarShipping.com.  If you havn't already done so, please login to your account to add your billing information and view the available categories.  We will be in contact with you to help set up your categories.  If you need faster help, we are available during normal business hours at 480-785-7400.\n\nFor your records, here is your login information:\nEmail Login: ".$mylisting["login_email"]."\nPassword: ".$password."\n\nThanks again,\n\nThe CarShipping.com Staff\nPart of the iRelocation Network!\n\nwww.CarShipping.com\n","From: CarShipping.com <admin@carshipping.com>");

    #
    # Show Step 3
    #
    $step=3;
  }
  else
  {
    $step=2;
  }

}

#
# Show Step
#

if($step==2)
{
  include("signup_step2.inc");
}
elseif($step==3)
{
  include("signup_step3.inc");
}
else
{
  include("signup_step1.inc");
}

?>