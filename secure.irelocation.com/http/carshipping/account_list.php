<?
if(isset($add_remove_go))
{
  foreach($add_remove as $myadd_remove)
  {
    $rs=new mysql_recordset("select * from listings where comp_id='".$user["id"]."' and cat_id='$myadd_remove'");
    if($rs->rowcount()>0)
    {
      $rs=new mysql_recordset("delete from listings where comp_id='".$user["id"]."' and cat_id='$myadd_remove'");
    }
    else
    {
      $rs=new mysql_recordset("insert into listings (comp_id,cat_id) values ('".$user["id"]."','$myadd_remove')");
    }
  }
}

#
# Get the list of categories
#
$rs=new mysql_recordset("select * from categories order by state,position,category");
while($rs->fetch_array())
{
  $categories[]=$rs->myarray;
}

#
# Get the listings for the current user
#
$rs=new mysql_recordset("select * from listings where comp_id='".$user["id"]."'");
while($rs->fetch_array())
{
  $listings[$rs->myarray["cat_id"]]=$rs->myarray;
}

#
# Get the average daily clicks (last 7 days)
#
$rs=new mysql_recordset("select sum(clicks) as mysum from log where comp_id='".$user["id"]."' and date between '".date("Y-m-d",time()-604800)."' and '".date("Y-m-d",time()-86400)."'");
$rs->fetch_array();
$avg_clicks=$rs->myarray["mysum"] / 7;

#
# Check for billing information!
#
$rs=new mysql_recordset("select * from billing where comp_id='".$user["id"]."'");
if($rs->fetch_array())
{
  $has_billing=true;
}



?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Manage Your Account</title>
<script language=JavaScript>
function open_action_win(url)
{
  var winl = (screen.width - 600) / 2;
  var wint = (screen.height - 500) / 2;
  window.open(url,"new_window",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=0,width=600,height=500,top='+wint+',left='+winl+'');
}
</script>
</head>

<body>
<form name="display" method="post" action="account.php">
<input type="hidden" name="action" value="update_listings">
<div align="center">
  <center>
  <table border="0" cellspacing="1" width="705">
    <tr>
      <td width="701">
      <table border="0" cellspacing="0" width="100%" cellpadding="0">
        <tr>
          <td width="40%">
          <a href="http://www.carshipping.com/">
          <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></a></td>
          <td width="60%">
          <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
            <tr>
              <td width="100%" height="10"></td>
            </tr>
            <tr>
              <td width="100%" bgcolor="#2360A5" height="25">
              <p class="title2" align="center"><font color="#FFFFFF">
              <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
            </tr>
            <tr>
              <td width="100%" height="18"></td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <table border="0" cellspacing="0" width="100%" cellpadding="5">
        <tr>
          <td width="60%" valign="top">
          <p class="title2"><font size="3">Account Manager</font></p>
          <p class="regtxt" size="2">Below you will see the list of current categories on CarShipping.com along with any listings you have purchased.
          If you would like to sign up for more categories/states, please contact support using the link to the right,
          or call us during normal business hours at 480-785-7400.
          <p class="redtxt"><?if($has_billing!=true){echo "No billing information on file!<br><br>Before your listings can go live you will need to enter your billing information.";}?>&nbsp;</td>
          <td width="40%">
          <table border="0" width="100%" cellpadding="4">
            <tr>
              <td width="100%" align="center"><span class="redtxt"><?echo $user["login_email"];?></span> &nbsp;<input type="button" name="logout" value="Logout" class="formfield" onclick="window.location.href='account.php?action=logout'"></td>
            </tr>
            <tr>
              <td width="100%" background="images/back2.gif">
              <table border="0" cellspacing="0" width="100%" cellpadding="0">
                <tr>
                  <td width="100%" height="25"><b><font size="2">Account
                  Activity:</font></b></td>
                </tr>
                <tr>
                  <td width="100%" height="15"><font size="2">&nbsp; Account
                  Status: <b><?if($user["active"]="1"){echo "<font color='green'>ACTIVE</font>";}else{echo "<font color='red'>PAUSED</font>";}?></b></font></td>
                </tr>
                <tr>
                  <td width="100%" height="15"><font size="2">&nbsp; Average
                  Daily Clicks: <b><?echo $avg_clicks;?></b></font></td>
                </tr>
              </table>
              </td>
            </tr>
            <tr>
              <td width="100%">
              <table border="0" cellspacing="1" width="100%" cellpadding="0" style="border: 2px dashed; border-color: darkgrey; padding: 4px;">
                <tr>
                  <td width="100%">
                  <ul imagesrc="images/arrow.gif">
                    <li><span class="regtxt"><a href="account.php?action=account_billing" onclick="open_action_win(this.href); return false;"><span class="redtxt">Edit Your Billing Information</span></a></li>
                    <li><span class="regtxt"><a href="account.php?action=account_summary" onclick="open_action_win(this.href); return false;">Daily Account Summary</a></li>
                    <li><span class="regtxt"><a href="account.php?action=billing_history" onclick="open_action_win(this.href); return false;">Review Billing History</a></li>
                    <li><span class="regtxt"><a href="account.php?action=edit_listing" onclick="open_action_win(this.href); return false;">Edit Your Listing</a></li>
                    <li><span class="regtxt"><a href="mailto: admin@carshipping.com">Contact Support</a></li>
                  </ul>
                  </td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="450">
<table border="0" cellspacing="0" width="450" cellpadding="0">
  <tr>
    <td width="450" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="450">
      <tr>
        <td width="180" align="center" class="col_d"><b>Category</b></td>
        <td width="90" align="center" class="col_d"><b>Your Position</b></td>
        <td width="180" align="center" class="col_d"><b>Cost</b></td>
      </tr>
      <?
      $i=0;
      foreach($categories as $cat)
      {
        if($listings[$cat["id"]]["id"]!="")
        {
        ?>
        <tr>
          <td width="180" class="col_o">&nbsp;<?echo $cat["category"];?></td>
          <td width="90" class="col_o">&nbsp;<?if($listings[$cat["id"]]["featured"]=='1'){echo "Positional: ".$listings[$cat["id"]]["position"];}else{echo "Rotational";}?></td>
          <td width="180" class="col_x">&nbsp;-<?if($adminx=='1'){?><input type="checkbox" name="add_remove[<?echo $i;?>]" value="<?echo $cat["id"];?>"><?}?> Purchased</td>
        </tr>
        <?
        }
        else
        {
        ?>
        <tr>
          <td width="180" class="col_o">&nbsp;<?echo $cat["category"];?></td>
          <td width="90" class="col_o">&nbsp;Rotational</td>
          <td width="180" class="col_o">&nbsp;+<?if($adminx=='1'){?><input type="checkbox" name="add_remove[<?echo $i;?>]" value="<?echo $cat["id"];?>"><?}?> $<?printf("%01.2f",$cat["cost"]);?> ($<?printf("%01.2f",$cat["bulk"]);?> bulk)</td>
        </tr>
        <?
        }
        $i++;
      }
      ?>
      </table>
      <?if($adminx=='1'){?><input type="submit" name="submit" value="Submit Changes"><input type="hidden" name="add_remove_go" value="1"><?}?>
    </td>
  </tr>
</table>
      </td>
    </tr>
    <tr>
      <td width="701"><p align="center"><img src="images/authorizenet.gif" border="0"> <img src="images/geotrust.gif" border="0"></p></td>
    </tr>
    <tr>
      <td width="701"><p align="center"><span class="regtxt">�2003 iRelocation.com, Inc. - All Rights Reserved - <a href="http://www.carshipping.com/privacy_policy.htm" target="_blank"><span class="redtxt"><u>Privacy Policy</u></span></a></span></p></td>
    </tr>
  </table>
  </center>
</div>

</form>

</body>

</html>