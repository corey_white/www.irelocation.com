<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Advertise With Us</title>
</head>

<body>
<form method="post" action="account.php">
<div align="center">
  <center>
  <table border="0" cellspacing="1" width="705">
    <tr>
      <td width="701">
      <table border="0" cellspacing="0" width="100%" cellpadding="0">
        <tr>
          <td width="40%">
          <a href="http://www.carshipping.com">
          <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></a></td>
          <td width="60%">
          <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
            <tr>
              <td width="100%" height="10"></td>
            </tr>
            <tr>
              <td width="100%" bgcolor="#2360A5" height="25">
              <p class="title2" align="center"><font color="#FFFFFF">
              <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
            </tr>
            <tr>
              <td width="100%" height="18"></td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="specialfield" align="center">Add your site to the
      CarShipping.com Directory!</td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="qlinks"><font size="2">Step 1 of 3 - Create Your Listing</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">Clearly and accurately describe your site, and emphasize
      any unique benefits of your company.&nbsp; Your title and description
      should have normal sentence capitalization using proper grammar and
      correct spelling.</td>
    </tr>
    <tr>
      <td width="701">
      <p align="center" class="title">&nbsp;</td>
    </tr>
    <tr>
      <td width="701" align="center">
      <table cellspacing="1" width="80%" style="border: 2px dashed; border-color: darkgrey; padding: 4px;" cellpadding="0">
        <tr>
          <td width="100%">
          <span class="title2">
          <a href="<?echo $mylisting["url"];?>" target="_blank">
          <font color="#2360A5"><?echo $mylisting["name"];?></font></a></span>  <span class="phone"><?echo substr($mylisting["phone"],0,3)."-".substr($mylisting["phone"],3,3)."-".substr($mylisting["phone"],6,4);?></span><br>
          <span class="regtxt"><?echo $mylisting["description"];?></span><br>
          <a href="<?echo $mylisting["quote_url"];?>" target="_blank">
          <span class="qlinks">GET A QUOTE</span></a> -
          <a href="<?echo $mylisting["url"];?>" target="_blank">
          <span class="qlinks">VISIT WEBSITE</span></a></td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="qlinks"><font size="2">Step 2 of 3 - Activate Your Account</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">To activate your account you will need to enter your
      email address and choose a password.</td>
    </tr>
    <tr>
      <td width="701">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="701" align="center">
      <table border="0" cellspacing="1" width="50%" cellpadding="0" style="border: 2px dashed; border-color: darkgrey; padding: 4px;">
        <tr>
          <td width="100%">
          <table border="0" width="100%" cellpadding="2">
            <tr>
              <td width="35%" class="regtxt">Your Name:</td>
              <td width="65%" class="regtxt"><?echo $mylisting["contact_name"];?></td>
            </tr>
            <tr>
              <td width="35%" class="regtxt">Phone Number:</td>
              <td width="65%" class="regtxt"><?echo $mylisting["contact_phone"];?></td>
            </tr>
            <tr>
              <td width="35%" class="regtxt">Email Address:</td>
              <td width="65%" class="regtxt"><?echo $mylisting["login_email"];?></td>
            </tr>
            <tr>
              <td width="35%" class="regtxt">Password:</td>
              <td width="65%" class="regtxt">
              <p class="regtxt"><?for($i=0;$i<$mylisting["pass_length"];$i++){?>*<?}?></td>
            </tr>
          </table>
          </td>
        </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701">
      <p class="title2"><font size="3">Step 3 of 3 - Login to Your Account</font></td>
    </tr>
    <tr>
      <td width="701">
      <p class="regtxt">Login to your account manager to choose your categories and
      enter billing information.&nbsp; You may choose to be placed in any category
      on the website.&nbsp; </td>
    </tr>
    <tr>
      <td width="701">
      <p align="center" class="title"><?echo $msg;?></td>
    </tr>
    <tr>
      <td width="701" align="center">
      <table border="0" cellspacing="1" width="30%" style="border: 2px dashed; border-color: darkgrey; padding: 4px;">
            <tr>
              <td width="100%" bgcolor="#F4F4F4">
              <table border="0" cellspacing="1" width="100%">
                <tr>
                  <td width="100%" colspan="2">
                  <p align="center"><b>Account Login</b></td>
                </tr>
                <tr>
                  <td width="34%" align="right"><font size="1">Email:</font></td>
                  <td width="66%" align="right">
                  <input size="15" class="loginfield" name="username" value="<?echo $mylisting["login_email"];?>"></td>
                </tr>
                <tr>
                  <td width="34%" align="right"><font size="1">Password:</font></td>
                  <td width="66%" align="right">
                  <input type="password" size="15" class="loginfield" name="password"></td>
                </tr>
                <tr>
                  <td width="34%">&nbsp;</td>
                  <td width="66%">
                  <p align="right">
                  <input type="submit" value="Login" name="submit" class="formfield"></td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
      </td>
    </tr>
    <tr>
      <td width="701">&nbsp;</td>
    </tr>
    <tr>
      <td width="701"><p align="center"><span class="regtxt">�2003 iRelocation.com, Inc. - All Rights Reserved - <a href="http://www.carshipping.com/privacy_policy.htm" target="_blank"><span class="redtxt"><u>Privacy Policy</u></span></a></span></p></td>
    </tr>
  </table>
  </center>
</div>

</form>

</body>

</html>