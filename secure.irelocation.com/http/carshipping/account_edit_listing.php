<?

function cleantxt($txt)
{
  $txt=strip_tags($txt);
  $replace=array("'","\"","*","!","#","%","^","`","~","<",">","\n","\r","&quot","&#34","&amp","&#38","&lt","&#60","&gt","&#62","&nbsp","&#160","&iexcl",chr(161),"&cent",chr(162),"&pound",chr(163),"&copy",chr(169),"&#(\d+);'e","&");
  foreach($replace as $key)
  {
    $txt=str_replace($key,"",$txt);
  }
  return $txt;
}

if($action2=='update_listing')
{
  if($name=='')
  {
    $msg.="Title is Missing!<br>";
  }
  else
  {
    $name=cleantxt($name);
    $name=substr($name,0,50);
  }

  if($phone=='')
  {
    $msg.="Phone is Missing!<br>";
  }
  else
  {
    $phone=cleantxt($phone);
    $phone=str_replace("-","",$phone);
    $phone=str_replace("(","",$phone);
    $phone=str_replace(")","",$phone);
    $phone=str_replace("{","",$phone);
    $phone=str_replace("}","",$phone);
    $phone=str_replace("[","",$phone);
    $phone=str_replace("]","",$phone);
    $phone=str_replace(" ","",$phone);
    if(substr($phone,0,1)=='1')
    {
      $phone=substr($phone,1);
    }
  }

  if($description=='')
  {
    $msg.="Description is Missing!<br>";
  }
  else
  {
    $description=cleantxt($description);
    $description=substr($description,0,255);
  }

  if($url=='')
  {
    $msg.="Website Address is Missing!<br>";
  }
  else
  {
    $url=cleantxt($url);
    $url=str_replace(" ","",trim($url));
    $url=substr($url,0,255);
    if((substr_count($url,"http")==0) || (strlen($url)<15))
    {
      $msg.="Invalid Website Address!<br>";
    }
  }

  if($quote_url=='')
  {
    $msg.="Quote Page Link is Missing!<br>";
  }
  else
  {
    $quote_url=cleantxt($quote_url);
    $quote_url=str_replace(" ","",trim($quote_url));
    $quote_url=substr($quote_url,0,255);
    if((substr_count($quote_url,"http")==0) || (strlen($quote_url)<14))
    {
      $msg.="Invalid Quote Page Link!<br>";
    }
  }

  if($msg=='')
  {
    $rs=new mysql_recordset("update companies set name='$name',phone='$phone',description='$description',url='$url',quote_url='$quote_url' where id='".$user["id"]."'");
    $user["name"]=$name;
    $user["phone"]=$phone;
    $user["description"]=$description;
    $user["url"]=$url;
    $user["quote_url"]=$quote_url;
    $user_login=serialize($user);
    $msg="Your listing has bee updated!";
  }
}
else
{
  $name=$user["name"];
  $phone=$user["phone"];
  $description=$user["description"];
  $url=$user["url"];
  $quote_url=$user["quote_url"];
}
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Edit Your Site Listing</title>
<link rel="stylesheet" href="style.css" type="text/css">
</head>

<body>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="40%">
    <img border="0" src="images/carshipping_logo.gif" width="274" height="62"></td>
    <td width="60%">
    <table border="0" cellspacing="0" width="100%" cellpadding="0" height="53">
      <tr>
        <td width="100%" height="10"></td>
      </tr>
      <tr>
        <td width="100%" bgcolor="#2360A5" height="25">
        <p class="title2" align="center"><font color="#FFFFFF">
        <span style="font-weight: 400">Targeted traffic for Car Shippers!</span></font></td>
      </tr>
      <tr>
        <td width="100%" height="18"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<?
if($msg!='')
{
  echo "<p class=\"notice\">$msg</p>";
}
?>
<form name="summary" method="POST" action="account.php">
<input type="hidden" name="action" value="edit_listing">
<input type="hidden" name="action2" value="update_listing">
<table width="100%">
  <tr>
    <td>
      <table width="450">
        <tr>
          <td colSpan="2" width="450">&nbsp;<p class="title"><b>Edit Your Listing:</b><br>&nbsp;</td>
        </tr>
        <tr>
          <td class="regtxt" width="200">Title / Name of Company:</td>
          <td width="250"><input size="35" value="<?echo $name;?>" name="name"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="200">1-800 Number:</td>
          <td width="250"><input size="35" value="<?echo $phone;?>" name="phone"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="200">Description:</td>
          <td width="250"><p class="regtxt">max 255 characters<br><textarea rows="6" cols="40" <?if($agent_level!='low'){?>class="loginfield"<?}?>name="description"><?echo $description;?></textarea></td>
        </tr>
        <tr>
          <td class="regtxt" width="200">Website Address: </td>
          <td width="250"><input size="35" value="<?echo $url;?>" name="url"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="200">Link to Your Quote Page: </td>
          <td width="250"><input size="35" value="<?echo $quote_url;?>" name="quote_url"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
        <tr>
          <td class="regtxt" width="200">&nbsp;</td>
          <td width="250">&nbsp;</td>
        </tr>
        <tr>
          <td class="regtxt" width="200">&nbsp;</td>
          <td width="250"><input type="submit" value="Save Listing" name="submit"<?if($agent_level!='low'){?>class="loginfield"<?}?>></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>

</html>