<?

#
# Setup terms
#
$myterms[]="Once";
$myterms[]="Weekly";
$myterms[]="Bi-Weekly";
$myterms[]="Monthly";
$myterms[]="Bi-Monthly";
$myterms[]="Quarterly";
$myterms[]="Semi-Annually";
$myterms[]="Annually";

#
# Update Billing Information
#
if($action2=='update_billing_info')
{
  $cc["first_name"]=str_replace("'","",$first_name);
  $cc["last_name"]=str_replace("'","",$last_name);
  $cc["email_address"]=str_replace("'","",$email_address);
  $cc["method"]=$method;
  $cc["card_num"]=str_replace("-","",$card_num);
  $cc["exp_date"]=$exp_date;
  $cc["bank_name"]=$bank_name;
  $cc["bank_acct_type"]=$bank_acct_type;
  $cc["bank_aba_code"]=$bank_aba_code;
  $cc["bank_acct_num"]=$bank_acct_num;

  $msg="";

  if($cc["first_name"]=="")
  {
    $msg.="Update Failed! Missing First Name!<br>";
  }
  if($cc["last_name"]=="")
  {
    $msg.="Update Failed! Missing Last Name!<br>";
  }
  if($method=='cc')
  {
    if($cc["card_num"]=="")
    {
      $msg.="Update Failed! Missing Credit Card Number!<br>";
    }
    if(strlen($cc["card_num"])<15)
    {
      $msg.="Update Failed! Invalid Credit Card Number!<br>";
    }
    if(strlen($cc["exp_date"])!=5)
    {
      $msg.="Update Failed! Invalid Expiration Date!<br>";
    }
  }
  if($method=='ck')
  {
    if($cc["bank_name"]=="")
    {
      $msg.="Update Failed! Missing Bank Name!<br>";
    }
    if($cc["bank_aba_code"]=="")
    {
      $msg.="Update Failed! Missing Bank ABA Code!<br>";
    }
    if($cc["bank_acct_num"]=="")
    {
      $msg.="Update Failed! Missing Bank Acct Number!<br>";
    }
  }

  if($msg=="")
  {
    if($new_billing=='1')
    {
      $ccinfo=serialize($cc);
      $rs=new mysql_recordset("insert into billing_cc (comp_id,ccinfo) values ('$id',encode('$ccinfo','$xxxkey'))");
    }
    else
    {
      if(substr($cc["card_num"],0,4)=='xxxx')
      {
        $rs=new mysql_recordset("select decode(ccinfo,'$xxxkey') as myccinfo from billing_cc where id='$cc_id'");
        $rs->fetch_array();
        $temp=unserialize($rs->myarray["myccinfo"]);
        $cc["card_num"]=$temp["card_num"];
      }
      $ccinfo=serialize($cc);
      $rs=new mysql_recordset("update billing_cc set ccinfo=encode('$ccinfo','$xxxkey') where id='$cc_id'");
    }
  }

}
#
# Update All Billing History
#
elseif($action2=='update_all_history')
{
  $mylist=explode(",",$list);
  foreach($mylist as $bill_id)
  {
    $rs=new mysql_recordset("update billing set date='".$date[$bill_id]."',description='".$description[$bill_id]."',amount='".$amount[$bill_id]."' where id='$bill_id' and comp_id='$id'");
    //echo "<pre>update billing set date='".$date[$bill_id]."',description='".$description[$bill_id]."',amount='".$amount[$bill_id]."' where id='$bill_id' and comp_id='$id'<pre><br>\n";
  }
}
#
# Add a New Transaction
#
elseif($action2=='add_transaction')
{
  if($description=="")
  {
    $msg.="Add Transaction Failed! Missing Description!<br>";
  }
  if($amount<1)
  {
    $msg.="Add Transaction Failed! Invalid Amount!<br>";
  }

  $description=str_replace("'","",$description);
  switch($options)
  {
     case "1":
       $status="new";
       break;
     case "2":
       $status="processed";
       break;
     case "3":
       $status="received";
       $amount=$amount*-1;
       break;
  }

  if($msg=="")
  {
    $rs=new mysql_recordset("insert into billing (comp_id,cc_id,amount,description,date,status) values ('$id','$cc_id','$amount','$description','$start_year-$start_month-$start_day','$status')");

    $terms=strtolower($terms);
    if(($terms!="once") && ($options!='3'))
    {
      switch($terms)
      {
        case "weekly":
          $next=date("Y-m-d",mktime(0,0,0,$start_month,$start_day+7,$start_year));
          break;
        case "bi-weekly":
          $next=date("Y-m-d",mktime(0,0,0,$start_month,$start_day+14,$start_year));
          break;
        case "monthly":
          $next=date("Y-m-d",mktime(0,0,0,$start_month+1,$start_day,$start_year));
          break;
        case "bi-monthly":
          $next=date("Y-m-d",mktime(0,0,0,$start_month+2,$start_day,$start_year));
          break;
        case "quarterly":
          $next=date("Y-m-d",mktime(0,0,0,$start_month+3,$start_day,$start_year));
          break;
        case "semi-annually":
          $next=date("Y-m-d",mktime(0,0,0,$start_month+6,$start_day,$start_year));
          break;
        case "annually":
          $next=date("Y-m-d",mktime(0,0,0,$start_month,$start_day,$start_year+1));
          break;
      }

    $rs=new mysql_recordset("insert into billing_rules (comp_id,cc_id,amount,description,start,end,next,terms,status) values ('$id','$cc_id','$amount','$description','$start_year-$start_month-$start_day','$end_year-$end_month-$end_day','$next','$terms','$status')");
    }
  }
}
elseif($action2=='reoccurring_reactivate')
{
  $rs=new mysql_recordset("update billing_rules set active='1' where id='$rule_id'");
  $msg="Rule #$rule_id has be reactivated!";
}
elseif($action2=='reoccurring_deactivate')
{
  $rs=new mysql_recordset("update billing_rules set active='0' where id='$rule_id'");
  $msg="Rule #$rule_id has be deactivated!";
}
elseif($action2=='reoccurring_delete')
{
  $rs=new mysql_recordset("delete from billing_rules where id='$rule_id'");
  $msg="Rule #$rule_id has be deleted!";
}
elseif($action2=='trans_delete')
{
  $rs=new mysql_recordset("delete from billing where id='$bill_id'");
  $msg="Billing entry #$bill_id has be deleted!";
}
elseif($action2=='reset_to_new')
{
  $rs=new mysql_recordset("update billing set status='new' where id='$bill_id'");
  $msg="Billing entry #$bill_id has reset to new!";
}

#
# Get the company name
#
$rs=new mysql_recordset("select comp_name from company where comp_id='$id'");
$rs->fetch_array();
$comp_name=$rs->myarray["comp_name"];

#
# Check if there is billing information on file.  If so, decode it and unserialize into $cc array
#
$rs=new mysql_recordset("select id,decode(ccinfo,'$xxxkey') as myccinfo from billing_cc where comp_id='$id'");
if($rs->rowcount()>0)
{
  unset($cc);
  $rs->fetch_array();
  $cc=unserialize($rs->myarray["myccinfo"]);
  $cc_id=$rs->myarray["id"];
  $hasbilling=TRUE;
}
else
{
  $hasbilling=FALSE;
}

  #
  # Now check to see if the user has reoccurring billing rules already set up
  #
  $rs=new mysql_recordset("select * from billing_rules where comp_id='$id'");
  if($rs->rowcount()>0)
  {
    while($rs->fetch_array())
    {
      $reoccurring[]=$rs->myarray;
    }
    $hasreoccurring=TRUE;
  }

#
# Get all the billing history for the company
#
$rs=new mysql_recordset("select * from billing where comp_id='$id' order by date,status");
if($rs->rowcount()>0)
{
  while($rs->fetch_array())
  {
    $history[]=$rs->myarray;
  }
  $hashistory=TRUE;
}
else
{
  $hashistory=FALSE;
}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Billing Information for <?echo $comp_name;?></title>
<script language="javascript">
function yesorno(link,msg)
{
  response = confirm(msg);
  if(response!=0) window.location = link;
}
</script>
</head>

<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Billing Information</b></font><?if($hasbilling==FALSE){?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="heading2">No Billing Information On File!</span><?}?></p>
<p><font face="verdana" size="3"><?echo $comp_name;?></font></p>
<form name="billing_info" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_billing">
<input type="hidden" name="action2" value="update_billing_info">
<?
if($hasbilling==FALSE)
{
?>
<input type="hidden" name="new_billing" value="1">
<?
}
else
{
?>
<input type="hidden" name="cc_id" value="<?echo $cc_id;?>">
<?
}
?>
<input type="hidden" name="id" value="<?echo $id;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="200"><span class="text_2">First Name:</span></td>
          <td width="300"><input type="text" name="first_name" value="<?echo $cc["first_name"];?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Last Name:</span></td>
          <td width="300"><input type="text" name="last_name" value="<?echo $cc["last_name"];?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Email Address:</span></td>
          <td width="300"><input type="text" name="email_address" value="<?echo $cc["email_address"];?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="200" class="quote_col_s"><input type="radio" name="method" value="cc"<?if($cc["method"]!='ck'){echo " checked";}?>><span class="text_2"> Credit Card</span></td>
          <td width="300" class="quote_col_s">&nbsp;</td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Credit Card Number:</span></td>
          <td width="300"><input type="text" name="card_num" class="quote_box" value="<?if($hasbilling==TRUE){if($user["level"]!='admin'){?>xxxxxxxxxxxx<?echo substr($cc["card_num"],-4);}else{echo $cc["card_num"];}}?>" size="40"> <img width='20' src="images/<?if(substr($cc["card_num"],0,1)==3){echo "amex";}elseif(substr($cc["card_num"],0,1)==4){echo "visa";}elseif(substr($cc["card_num"],0,1)==5){echo "mc";}elseif(substr($cc["card_num"],0,1)==6){echo "dis";}else{echo "none";}?>.gif"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Expiration:</span></td>
          <td width="300"><input type="text" name="exp_date" value="<?echo $cc["exp_date"];?>" class="quote_box" size="6" maxlength="5"> <span class="text_2">mm/yy</span></td>
        </tr>
        <tr>
          <td width="200" class="quote_col_s"><input type="radio" name="method" value="ck"<?if($cc["method"]=='ck'){echo " checked";}?>><span class="text_2"> e-Check</span></td>
          <td width="300" class="quote_col_s">&nbsp;</td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Bank Name:</span></td>
          <td width="300"><input type="text" name="bank_name" value="<?echo $cc["bank_name"];?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Account Type:</span></td>
          <td width="300"><span class="text_2"><input type="radio" name="bank_acct_type" value="checking"<?if($cc["bank_acct_type"]!='savings'){echo " checked";}?>> Checking &nbsp;<input type="radio" name="bank_acct_type" value="savings"<?if($cc["bank_acct_type"]=='savings'){echo " checked";}?>> Savings</span></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Bank ABA Code:</span></td>
          <td width="300"><input type="text" name="bank_aba_code" value="<?echo $cc["bank_aba_code"];?>" class="quote_box" maxlength="9"> <span class="text_2">9 digit routing number</span></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Bank Account Number:</span></td>
          <td width="300"><input type="text" name="bank_acct_num" value="<?echo $cc["bank_acct_num"];?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Update Billing Information" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<p><font face="verdana" color="#2360A5" size="3"><b> Add a Transaction</b></font></p>
<form name="add_a_transaction" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_billing">
<input type="hidden" name="action2" value="add_transaction">
<input type="hidden" name="cc_id" value="<?echo $cc_id;?>">
<input type="hidden" name="id" value="<?echo $id;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="120"><span class="text_2">Date to Process:</span></td>
          <td width="380"><select name="start_month" class="quote_box">
          <?
          for($i=1;$i<=12;$i++)
          {
            if($i==date('n'))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select>
          <select name="start_day" class="quote_box">
          <?
          for($i=1;$i<=31;$i++)
          {
            if($i==date('j'))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select>
          <select name="start_year" class="quote_box">
          <?
          for($i=date("Y");$i<=date("Y")+3;$i++)
          {
            if($i==date("Y"))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select>
          </td>
        </tr>
        <tr>
          <td width="120"><span class="text_2">Description:</span></td>
          <td width="380"><input type="text" name="description" class="quote_box" size="50"></td>
        </tr>
        <tr>
          <td width="120"><span class="text_2">Amount:</span></td>
          <td width="380"><span class="text_2">$</span><input type="text" name="amount" class="quote_box" size="8"></td>
        </tr>
        <tr>
          <td width="120"><span class="text_2">Terms:</span></td>
          <td width="380"><select name="terms" class="quote_box">
          <?
          foreach($myterms as $term)
          {
            echo "<option value='$term'>$term</option>\n";
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="120"><span class="text_2">Options:</span></td>
          <td width="380"><select name="options" class="quote_box">
          <?
          if($hasbilling==TRUE)
          {
            ?><option value='1'>Automatically Charge Customer</option>
            <?
          }
          ?>
          <option value='2'>Do Not Charge Customer</option>
          <option value='3'>This is a Received Payment</option>
          </select></td>
        </tr>
        <tr>
          <td width="120"><span class="text_2">Ending Date:</span></td>
          <td width="380"><select name="end_month" class="quote_box">
          <?
          for($i=1;$i<=12;$i++)
          {
            if($i==date('n'))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select>
          <select name="end_day" class="quote_box">
          <?
          for($i=1;$i<=31;$i++)
          {
            if($i==date('j'))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select>
          <select name="end_year" class="quote_box">
          <?
          for($i=date("Y");$i<=date("Y")+3;$i++)
          {
            if($i==(date("Y")+1))
            {
              echo "<option value='$i' selected>$i</option>\n";
            }
            else
            {
              echo "<option value='$i'>$i</option>\n";
            }
          }
          ?>
          </select> <span class="text_2">(Reoccurring Only)</span>
          </td>
        </tr>
        <tr>
          <td width="120">&nbsp;</td>
          <td width="380" align="right"><input type="submit" name="submit" value="Add New Transaction" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<?
  if($hasreoccurring==TRUE)
  {
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Reoccurring Transactions</b></font></p>
    <span class="notice">
      <table cellSpacing="2" cellPadding="2" width="600" border="0">
        <tr>
          <td width="70"><span class="heading">start/<br>end</span></td>
          <td width="70"><span class="heading">charge</span></td>
          <td width="70"><span class="heading">terms</span></td>
          <td width="70"><span class="heading">next</span></td>
          <td width="70"><span class="heading">amount</span></td>
          <td width="100"><span class="heading">description</span></td>
          <td width="150"><span class="heading">options</span></td>
        </tr>
        <?
        foreach($reoccurring as $myreoccurring)
        {
        ?>
        <tr<?if($myreoccurring["active"]!='1'){echo " class='quote_col_x'";}?>>
          <td width="60"><span class="realsmall"><?echo $myreoccurring["start"];?><br><?echo $myreoccurring["end"];?></span></td>
          <td width="60"><span class="realsmall"><?if($myreoccurring["status"]=='new'){echo "Auto-bill";}else{echo "Manual";}?></span></td>
          <td width="60"><span class="realsmall"><?echo $myreoccurring["terms"];?></span></td>
          <td width="60"><span class="realsmall"><?echo $myreoccurring["next"];?></span></td>
          <td width="50"><span class="realsmall">$<?echo $myreoccurring["amount"];?></span></td>
          <td width="160"><span class="realsmall"><?echo $myreoccurring["description"];?></span></td>
          <td width="150"><a href="control.php?action=1md_companies_billing&action2=reoccurring_<?if($myreoccurring["active"]=='1'){echo "d";}else{echo "r";}?>eactivate&id=<?echo $id;?>&rule_id=<?echo $myreoccurring["id"];?>"><img src="images/b<?if($myreoccurring["active"]=='1'){echo "d";}else{echo "r";}?>eactivate2.gif" border="0"></a><a href="control.php?action=1md_companies_billing&action2=reoccurring_delete&id=<?echo $id;?>&rule_id=<?echo $myreoccurring["id"];?>"><img src="images/bdelete.gif" border="0"></a></td>
        </tr>
        <?
        }
        ?>
      </table>
    </span>
<p></p>
<?
  }
if($hashistory==TRUE)
{
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Billing History</b> <?if($user["level"]=='admin'){?><a href="control.php?action=1md_companies_billing_history&id=<?echo $id;?>"><font color="#FF0000" size="2">edit all</font></a><?}?></font></p>
  <form name="invoice" action="control.php" method="post" target="_blank">
    <span class="notice">
      <table cellSpacing="2" cellPadding="2" width="500" border="0">
        <tr>
          <td width="10"><span class="heading">&nbsp;</span></td>
          <td width="60"><span class="heading">date</span></td>
          <td width="70"><span class="heading">status</span></td>
          <td width="260"><span class="heading">description</span></td>
          <td width="50"><span class="heading">amount</span></td>
          <?if($user["level"]=='admin'){?><td width="50"><span class="heading">options</span></td><?}?>
        </tr>
        <?
        $total=0;
        foreach($history as $myhistory)
        {
          $total+=$myhistory["amount"];
          $list.=$myhistory["id"].",";
        ?>
        <tr>
          <td width="10"><span class="realsmall"><input type="checkbox" name="bill_id[<?echo $myhistory["id"];?>]" value="yes"></span></td>
          <td width="60"><span class="realsmall"><?echo $myhistory["date"];?></span></td>
          <td width="70"><span class="realsmall"><?if($myhistory["status"]=='failed'){echo "<a href=\"control.php?action=1md_companies_billing&action2=reset_to_new&id=$id&bill_id=".$myhistory["id"]."\" onclick=\"yesorno(this.href,'Reset to New?'); return false;\"><span class=\"realsmall\">failed</span></a>";}elseif($myhistory["status"]=='processed'){echo "invoice";}else{echo $myhistory["status"];}?></span></td>
          <td width="260"><span class="realsmall"><?echo $myhistory["description"];?></span></td>
          <td width="50"><span class="realsmall"><?echo $myhistory["amount"];?></span></td>
          <?if($user["level"]=='admin'){?><td width="50"><span class="realsmall"><a href="control.php?action=1md_companies_billing&action2=trans_delete&id=<?echo $id;?>&bill_id=<?echo $myhistory["id"];?>" onClick="yesorno(this.href,'Delete billing entry?'); return false;"><img src="images/bdelete.gif" border="0"></a></span></td><?}?>
        </tr>
        <?
        }
        ?>
        <tr>
          <td width="450" colspan="4" class="quote_col_s"><span class="heading">Total Due:</span></td>
          <td width="50" class="quote_col_s"><span class="heading">$<?printf("%01.2f",$total);?></span></td>
          <?if($user["level"]=='admin'){?><td width="50" class="quote_col_s">&nbsp;</td><?}?>
        </tr>
      </table>
    </span>
    <p></p>
    <?
    $rs=new mysql_recordset("select * from billing where comp_id='$id' and status='received' order by date desc limit 1");
    $rs->fetch_array();
    ?>
    <table width="279" border="0" cellspacing='0' cellpadding='0'>
      <tr>
        <td width="29" valign="top"><img src="images/arrow_large.gif"></td>
        <td width="250"><span class="notice2">
          <table cellSpacing="1" width="250" border="0">
            <tr>
              <td><span class="realsmall">Previous Balance</span></td>
              <td><input type="text" name="previous" size="15" class="quote_box" value="$<?printf("%01.2f",$rs->myarray["amount"]*-1);?>"></td>
            </tr>
            <tr>
              <td><span class="realsmall">Payments &amp; Credits</span></td>
              <td><input type="text" name="payments" size="15" class="quote_box" value="-$<?printf("%01.2f",$rs->myarray["amount"]*-1);?>"></td>
            </tr>
            <tr>
              <td><span class="realsmall">Charges &amp; Invoices</span></td>
              <td><input type="text" name="charges" size="15" class="quote_box" value="$<?printf("%01.2f",$total);?>"></td>
            </tr>
            <tr>
              <td class="quote_col_s"><span class="realsmall">Total Due</span></td>
              <td class="quote_col_s"><input type="text" name="balance" size="15" class="quote_box" value="$<?printf("%01.2f",$total);?>"></td>
            </tr>
            <tr>
              <td><span class="realsmall">Customer Email</span></td>
              <td><input type="text" name="cemail" size="15" class="quote_box" value="<?echo $cc["email_address"];?>"></td>
            </tr>
			<tr>
              <td><span class="realsmall">Message</span></td>
              
            <td><select name="imessage" class="quote_box">
                <option selected>Due Upon Reciept</option>
                <option>Net 10</option>
                <option>Net 30</option>
              </select></td>
            </tr>
            <tr>
              <td colspan="2">
              <p align="center"><input type="submit" value="Create Invoice" name="create" class="quote_box"></td>
            </tr>
          </table>
        </span></td>
      </tr>
    </table>
    <input type="hidden" name="action" value="1md_companies_billing_invoice">
    <input type="hidden" name="billing_name" value="<?echo $cc["first_name"];?> <?echo $cc["last_name"];?>">
    <input type="hidden" name="comp_id" value="<?echo $id;?>">
    <input type="hidden" name="list" value="<?echo substr($list,0,-1);?>">
  </form>
<p></p>
<?
}
?>
<script language="javascript">
focus();
</script>
</body>
</html>