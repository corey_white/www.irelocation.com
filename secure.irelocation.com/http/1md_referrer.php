<?

if(!isset($order))
{
  $order="source asc";
}

if(!isset($date_type))
{
  $date_type=1;
  $date_range="month";
}

if($date_type=='1')
{
  switch($date_range)
  {
    case "today":
      $date_start_month=date("m");
      $date_start_day=date("d");
      $date_start_year=date("Y");
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "yesterday":
      $start=time()-86400;
      $end=time()-86400;
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m",$end);
      $date_end_day=date("d",$end);
      $date_end_year=date("Y",$end);
      break;
    case "week":
      $start=time()-518400;
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "month":
      $start=mktime(0,0,0,date("n"),1,date("Y"));
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
    case "lastmonth":
      $start=mktime(0,0,0,date("n")-1,1,date("Y"));
      $end=mktime(0,0,0,date("n"),0,date("Y"));
      $date_start_month=date("m",$start);
      $date_start_day=date("d",$start);
      $date_start_year=date("Y",$start);
      $date_end_month=date("m",$end);
      $date_end_day=date("d",$end);
      $date_end_year=date("Y",$end);
      break;
    case "all":
      $date_start_month=date("m",0);
      $date_start_day=date("d",0);
      $date_start_year=date("Y",0);
      $date_end_month=date("m");
      $date_end_day=date("d");
      $date_end_year=date("Y");
      break;
  }
}

if(!isset($lead_type))
{
  $lead_type="all";
}

# get all categories
$rs_cats=new mysql_recordset("select * from directleadscategory");
while($rs_cats->fetch_array())
{
  $category_name[]=$rs_cats->myarray["name"];
  $category_id[]=$rs_cats->myarray["cat_id"];
}

$category_name[]="BIG Moving Quotes";
$category_id[]="22";

$rs_cats=nothing;

$sql_x="";
if($referrer!=''){$sql_x.=" and lower(source) like '%".strtolower($referrer)."%'";}
if($keyword!=''){$sql_x.=" and lower(keyword) like '%".strtolower($keyword)."%'";}
if($lead_type!='all')
{
  if($lead_type=='22')
  {
    $sql_x.=" and cat_id='2' and (source='tm' or source='tm_overture_big' or source='tm_google_big' or source='tm_superpages') and origin_state!=destination_state";
  }
  elseif($lead_type=='2')
  {
    $sql_x.=" and cat_id='2' and ((source!='tm' and source!='tm_overture_big' and source!='tm_google_big' and source!='tm_superpages') or origin_state=destination_state)";
  } 
  else
  {
    $sql_x.=" and cat_id='$lead_type'";
  } 
}

$sql_quotes="";
$sql_quotes="select count(distinct(name)) as mycount,source from quotes where left(received,8)>='$date_start_year$date_start_month$date_start_day' and left(received,8)<='$date_end_year$date_end_month$date_end_day' $sql_x group by source order by $order";

$sql_quotes_old="";
$sql_quotes_old="select count(distinct(name)) as mycount,source from quotes_old where left(received,8)>='$date_start_year$date_start_month$date_start_day' and left(received,8)<='$date_end_year$date_end_month$date_end_day' $sql_x group by source order by $order";

  $rs=new mysql_recordset($sql_quotes);
  while($rs->fetch_array())
  {
    if(trim($rs->myarray["source"])=='')
    {
      $results["other"][0]=$rs->myarray["mycount"];

      $results["other"][1]="other";
    }
    else
    {
      $results[strtolower($rs->myarray["source"])][0]=$rs->myarray["mycount"];
      $results[strtolower($rs->myarray["source"])][1]=strtolower($rs->myarray["source"]);
    }
  }

  $rs=new mysql_recordset($sql_quotes_old);
  while($rs->fetch_array())
  {
    if(trim($rs->myarray["source"])=='')
    {
      $results["other"][0]+=$rs->myarray["mycount"];
      $results["other"][1]="other";
    }
    else
    {
      $results[strtolower($rs->myarray["source"])][0]+=$rs->myarray["mycount"];
      $results[strtolower($rs->myarray["source"])][1]=strtolower($rs->myarray["source"]);
    }
  }

if(gettype($results)=="array")
{
  switch($order)
  {
  case "source asc":
          ksort($results);
        reset($results);
        break;
  case "source desc":
    krsort($results);
        reset($results);
          break;
  case "mycount asc":
    asort($results);
    reset($results);
    break;
  case "mycount desc":
    arsort($results);
    reset($results);
          break;
  }
}

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Referrer Summary</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script language=JavaScript>
function orderby(myorder)
{
  var f = document.forms.summary;
  var hiddenName = 'order'
  f.elements[hiddenName].value = myorder
  f.submit()
}
function enabled(set)
{
  var f = document.forms.summary;
  f["date_range"].disabled = !set;
  f["date_start_month"].disabled = set;
  f["date_start_day"].disabled = set;
  f["date_start_year"].disabled = set;
  f["date_end_month"].disabled = set;
  f["date_end_day"].disabled = set;
  f["date_end_year"].disabled = set;
}
</script>
</head>

<body>
<p><font face="verdana" color="#2360A5" size="3"><b>
    Affiliates - Referrer Summary</b></font></p>
<form name="summary" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_referrer">
<input type="hidden" name="order" value="<? echo $order;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="91"><span class="text_2">Date Range:</span></td>
          <td width="409"><input type="radio" name="date_type" value="1"<? if($date_type=='1'){echo " checked";}?> onclick="enabled(true)"><select size="1" name="date_range" class="quote_box">
          <option value="today"<? if($date_range=='today'){echo " selected";}?>>Today</option>
          <option value="yesterday"<? if($date_range=='yesterday'){echo " selected";}?>>Yesterday</option>
          <option value="week"<? if($date_range=='week'){echo " selected";}?>>Last 7 Days</option>
          <option value="month"<? if($date_range=='month'){echo " selected";}?>>This Month</option>
          <option value="lastmonth"<? if($date_range=='lastmonth'){echo " selected";}?>>Last Month</option>
          <option value="all"<? if($date_range=='all'){echo " selected";}?>>All Time</option>
          </select></td>
        </tr>
        <tr>
          <td width="91">&nbsp;</td>
          <td width="409"><input type="radio" name="date_type" value="2"<? if($date_type=='2'){echo " checked";}?> onclick="enabled(false)"><select size="1" name="date_start_month" class="quote_box">
          <? for($i=1;$i<=12;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_start_month==$i){echo " selected";}?>><? echo date("M",mktime(0,0,0,$i,1,2002));?></option>
          <?}?></select>
          <select size="1" name="date_start_day" class="quote_box">
          <? for($i=1;$i<=31;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_start_day==$i){echo " selected";}?>><? echo $i;?></option>
          <?}?></select>
          <select size="1" name="date_start_year" class="quote_box">
          <? for($i=(date("Y")-2);$i<=date("Y");$i++){?>
          <option value="<? echo $i;?>"<? if($date_start_year==$i){echo " selected";}?>><? echo $i;?></option>
          <?}?></select> -
          <select size="1" name="date_end_month" class="quote_box">
          <? for($i=1;$i<=12;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_end_month==$i){echo " selected";}?>><? echo date("M",mktime(0,0,0,$i,1,2002));?></option>
          <?}?></select>
          <select size="1" name="date_end_day" class="quote_box">
          <? for($i=1;$i<=31;$i++){?>
          <option value="<? if($i<10){echo "0";} echo $i;?>"<? if($date_end_day==$i){echo " selected";}?>><? echo $i;?></option>
          <? }?></select>
          <select size="1" name="date_end_year" class="quote_box">
          <? for($i=(date("Y")-2);$i<=date("Y");$i++){?>
          <option value="<? echo $i;?>"<? if($date_end_year==$i){echo " selected";}?>><? echo $i;?></option>
          <? }?></select></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Lead Type:</span></td>
          <td width="409"><select size="1" name="lead_type" class="quote_box">
          <option selected value="all"<? if($lead_type=='all'){echo " selected";}?>>All</option>
          <?
          $i=0;
          while($category_name[$i])
          {
            ?><option value="<? echo $category_id[$i];?>"<? if($category_id[$i]==$lead_type){echo " selected";}?>><? echo $category_name[$i];?></option>
            <?
            $i++;
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Referrer:</span></td>
          <td width="409"><input type="text" size="20" name="referrer" value="<? echo $referrer;?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Keyword:</span></td>
          <td width="409"><input type="text" size="20" name="keyword" value="<? echo $keyword;?>" class="quote_box"></td>
        </tr>
        <tr>
          <td width="91"><span class="text_2">Total Cost:</span></td>
          <td width="409">$<input type="text" size="6" name="cost" value="<? echo $cost;?>" class="quote_box"> <span class="text_2">(cost to attain results)</span></td>
        </tr>
        <tr>
          <td width="91">&nbsp;</td>
          <td width="409" align="right"><input type="submit" name="display" value="Display Report" class="quote_box"></td>
        </tr>
      </table>
    </span>
<script language=JavaScript>
<? if($date_type=='1'){?>
enabled(true);
<? }else{?>
enabled(false);
<? }?>
</script>
<br>
<table border="0" cellspacing="0" width="500" cellpadding="0">
  <tr>
    <td width="100%" bgcolor="#95B3D0">
    <table border="0" cellspacing="1" width="100%">
      <tr>
        <td width="100%" bgcolor="#A9CAEB" colspan="14">
          <font face="verdana" size="3" color="#FFFFFF"><b>&nbsp;Summary for <? echo date("M",mktime(0,0,0,$date_start_month,1,2002));?> <?echo $date_start_day;?>, <?echo $date_start_year;?> to <?echo date("M",mktime(0,0,0,$date_end_month,1,2002));?> <?echo $date_end_day;?>, <?echo $date_end_year;?></b></font>
        </td>
      </tr>
      <tr>
        <td width="70%" bgcolor="#DCDCDC" align="center">
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby("<?if($order=='source asc'){echo "source desc";}else{echo "source asc";}?>"); return false;'>
        <font color="#000000">referrer</font></a></b></font></td>
        <td width="30%" bgcolor="#DCDCDC" align="center"><b>
        <font face="verdana" color="#333333" size="1"><b><a href="#" onClick='orderby("<?if($order=='mycount asc'){echo "mycount desc";}else{echo "mycount asc";}?>"); return false;'>
        <font color="#000000">number of leads</font></a></b></font></td>
      </tr>
      <?
          if(gettype($results)=="array")
      {
      foreach($results as $thesource)
      {
      $total_count+=$thesource[0];
      ?>
      <tr>
        <td width="70%" class="quote_col_s">&nbsp;<a href="control.php?action=1md_referrer_stats&source=<? echo $thesource[1];?>&cat=<?if($lead_type!='all'){echo $lead_type;}?>" target="right"><font color="#000000"><? echo $thesource[1];?></font></a></td>
        <td width="30%" class="quote_col_o">&nbsp;<? echo $thesource[0];?></td>
      </tr>
      <?
      }
          }
      ?>
      <tr>
        <td width="70%" class="quote_col_d">TOTAL:</td>
        <td width="30%" class="quote_col_d"><? echo $total_count;?></td>
      </tr>
      <?
      if($cost>0)
      {
      ?>
      <tr>
        <td width="70%" class="quote_col_x">AVERAGE COST PER LEAD (CPL):</td>
        <td width="30%" class="quote_col_x">$<? printf("%01.2f",$cost/$total_count);?></td>
      </tr>
      <?
      }
      ?>
    </table>
   </td>
 </tr>
</table>
</form>
</body>

</html>