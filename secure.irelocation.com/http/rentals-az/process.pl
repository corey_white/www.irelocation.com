#!/usr/bin/perl
print "content-type: text/html\n\n";

#        Called by application page to get fingerprint for 'Authorize.net'.
#        Creates form with fingerprint & all necessary form data to go to
#        payment form, complete transaction on 'authorize.net' & finish
#        necessary email's save order data etc. on Focalp's server.
#        Designed to return to calling page

# -----------------------------------------------------------------------------------------
#        This script uses 'SimLib.pm' & 'SimHMAC.pm' supplied by 'Authorize.net'

use SimLib;

# -----------------------------------------------------------------------------------------
#        Following string used for offline test purposes only

#$buffer = "x_Amount=125.00&x_last_name=Bob Simpson&x_address=PO Box 1639&x_city=Vashon, WA 98070&x_email=bob\@bob.com&Churchill_vol.ii_UK-29.30=3&x_description=Book Orders (UK)&x_Receipt_Link_URL=http://focal.org/asim/focal_mail.cgi";

# -----------------------------------------------------------------------------------------
#        Read posted data & create name & data arrays

read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
@pairs = split(/&/, $buffer);
$i = 0;
foreach $pair (@pairs)
{
        ($name, $value) = split(/=/, $pair);
        $value =~ tr/+/ /;
        $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
        $Form{$name} = $value;
        $nameArr[$i] = lc($name);
        $valueArr[$i++] = $value;
}

$x_amount=$Form{'x_Amount'};

# -----------------------------------------------------------------------------------------
#        Croak if no amount

if($x_amount == 0)
{
        print "Amount is missing! \n";
        exit;
}

$loginid = "rentalsaz1";
$txnkey = "FqvwKnupjt98IJ9z";

# -----------------------------------------------------------------------------------------
#        Now lets create form page & go to 'Authorize.net'

print <<HTML;

<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Process Payment</title>
<style type="text/css">
<!--
input, select, textarea
{
  font-size: 11px;
  background: #FAFAFA;
  border: 1px solid;
  border-color : #7297B5;
}
//-->
</style>
</head>

<body>
<form method="POST" action="https://secure.authorize.net/gateway/transact.dll">
<p align="center">&nbsp;</p>
<p align="center">&nbsp;</p>
<p align="center">
<img border="0" src="https://secure.irelocation.com/rentals-az/images/logo.gif" width="129" height="94"></p>
<p align="center"><font face="Verdana" color="#FF0000" size="2">Please confirm
your billing information:</font></p>
<div align="center">
  <center>
  <table border="0" width="287" cellpadding="2">
    <tr>
      <td width="113"><font face="Verdana" size="2">Payment for:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_Description'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">First Name:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_First_Name'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">Last Name:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_Last_Name'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">Email:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_Email'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">Amount:</font></td>
      <td width="160"><font face="Verdana" size="2">\$$Form{'x_Amount'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">CC Number:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_Card_Num'}</font></td>
    </tr>
    <tr>
      <td width="113"><font face="Verdana" size="2">Expiration:</font></td>
      <td width="160"><font face="Verdana" size="2">$Form{'x_Exp_Date'}</font></td>
    </tr>
  </table>
  </center>
</div>
<p align="center"><input type="submit" value="Process Payment" name="process_payment"></p>
<input type="hidden" name="x_adc_relay_response" value="True">
<input type="hidden" name="x_adc_url" value="https://secure.irelocation.com/rentals-az/complete_payment.php">
<input type="hidden" name="x_method" value="CC">

HTML

# -----------------------------------------------------------------------------------------
#        Compute fingerprint & insert HTML

&SimLib::InsertFP($loginid, $txnkey, $x_amount);

print "<input type='hidden' name='x_login' value='$loginid'>\n";

# -----------------------------------------------------------------------------------------
#        Loop thru passed form elements & insert HTML

for($i = 0; $i < scalar(@nameArr); $i++)
{
        print "<input type='hidden' name='$nameArr[$i]' value='$valueArr[$i]'>\n";
}

# -----------------------------------------------------------------------------------------
#        Finish form & page

print <<HTML;

</form>
</body>

</html>

HTML

# -----------------------------------------------------------------------------------------
#        The dienice subroutine, for handling errors.


1;