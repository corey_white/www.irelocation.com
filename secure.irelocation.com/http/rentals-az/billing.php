<?
include_once("mysql.php");
session_register("user_login");

if(isset($user))
{
  $user_login=$user;
}

if($user_login=='bjensen~@')
{
  $code=md5(date("Y-m-d"));
  header("location: abilling.php?c=$code");
  exit;
}

$rs_cust=new mysql_recordset("select * from customers where id='$user_login'");
if($rs_cust->rowcount()>0)
{
 $user_login=$user;
 $rs_cust->fetch_array();
 $cust=$rs_cust->myarray;
}
else
{
 die("Invalid customer id!");
}

$total=0;
$rs_trans=new mysql_recordset("select * from transactions where cust_id='$user_login'");
while($rs_trans->fetch_array())
{
  $trans[]=$rs_trans->myarray;
  $total+=$rs_trans->myarray["total"];
}

list($fname,$lname)=explode(" ",$cust["name"]);
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rentals Arizona - Billing Information / Invoice</title>
<style type="text/css">
<!--
a:link
{
  text-decoration: none;
}
a:visited
{
  text-decoration: none;
}
a:hover
{
  text-decoration: underline;
}
input, select, textarea
{
  font-size: 11px;
  background: #FAFAFA;
  border: 1px solid;
  border-color : #7297B5;
}
//-->
</style>

<script language="javascript">
var slideShowSpeed = 5000
var crossFadeDuration = 3
var Pic = new Array()

        Pic[0] = 'images/home_front.jpg'
        Pic[1] = 'images/home_lake.jpg'
        Pic[2] = 'images/home_back.jpg'
        Pic[3] = 'images/home_belt.jpg'

var t
var j = 0
var p = Pic.length

var preLoad = new Array()
for (i = 0; i < p; i++){
   preLoad[i] = new Image()
   preLoad[i].src = Pic[i]
}

function runSlideShow(){
   if (document.all){
      document.images.SlideShow.style.filter="blendTrans(duration=2)"
      document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)"
      document.images.SlideShow.filters.blendTrans.Apply()
   }
   document.images.SlideShow.src = preLoad[j].src
   if (document.all){
      document.images.SlideShow.filters.blendTrans.Play()
   }
   j = j + 1
   if (j > (p-1)) j=0
   t = setTimeout('runSlideShow()', slideShowSpeed)
}
</script>

<script language="JavaScript">
<!--
function validate(form)
{
  if (form.x_Description.value == "")
  {
    alert("Please enter a description of what this payment is for!");
    form.x_Description.focus()
    return false;
  }
  if (form.x_First_Name.value == "")
  {
    alert("Please enter your first name!");
    form.x_First_Name.focus()
    return false;
  }
  if (form.x_Last_Name.value == "")
  {
    alert("Please enter your last name!");
    form.x_Last_Name.focus()
    return false;
  }
  if (form.x_Email.value == "")
  {
    alert("Please enter your email!");
    form.x_Email.focus()
    return false;
  }
  if (form.x_Amount.value<=0)
  {
    alert("Please enter a payment amount!");
    form.x_Amount.focus()
    return false;
  }
  if (form.x_Card_Num.value == "")
  {
    alert("Please enter your credit card number!");
    form.x_Card_Num.focus()
    return false;
  }
  if (form.x_Exp_Date.value == "")
  {
    alert("Please enter your expiration date!");
    form.x_Exp_Date.focus()
    return false;
  }
}
// -->
</script>
</head>
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">


<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td align="left" valign="bottom" width="571">
    <img border="0" src="images/top_bar.gif" width="406" height="48"></td>
    <td width="129">
    <p align="right">
    <a href="index.htm">
    <img border="0" src="images/logo.gif" width="129" height="94"></a></td>
  </tr>
</table>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="700">
    <img border="0" src="images/mountain_bar.jpg" width="700" height="115"></td>
    <td>&nbsp;</td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="466" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="10">
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="5">
          <tr>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center"><b><font face="Arial">B</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">N</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">G</font></b></td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="10%" align="center">&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <?if($msg){?>
      <tr>
        <td width="100%">
          <font color="#FF0000" size="2" face="verdana"><?echo $msg;?></font>
        </td>
      </tr>
      <?}?>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="25%"><font face="Arial"><b><i><font size="2">Customer ID:</font></i></b><font size="2">
            <?echo $cust["id"];?></font></font></td>
            <td width="25%"><b><i><font size="2" face="Arial">Bill To:</font></i></b></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font size="2" face="Arial"><?echo $cust["name"];?></font></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font face="Arial" size="2"><?echo $cust["address"];?></font></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font face="Arial" size="2"><?echo $cust["city"];?>, <?echo $cust["state"];?> <?echo $cust["zip"];?></font></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" bgcolor="#000000">
            <table border="0" cellspacing="1" width="100%" cellpadding="0" height="126">
              <tr>
                <td width="68%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Description</font></td>
                <td width="18%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Rate</font></td>
                <td width="14%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Total</font></td>
              </tr>
              <?
              if($trans[0]["total"]!='')
              {
              foreach($trans as $mytrans)
              {
              ?>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["description"];?></font></td>
                <td width="18%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["rate"];?></font></td>
                <td width="14%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["total"];?></font></td>
              </tr>
              <?
              }
              }
              ?>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="14">
                <font size="1" face="Arial">&nbsp;</font></td>
                <td width="18%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
                <td width="14%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
              </tr>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
                <td width="18%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
                <td width="14%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
              </tr>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="19">&nbsp;</td>
                <td width="18%" bgcolor="#C0C0C0" height="19">
                <font face="Arial" size="1">&nbsp;</font><font face="Arial" size="2">Balance
                Due</font></td>
                <td width="14%" bgcolor="#C0C0C0" height="19"><b>
                <font face="Arial" size="1">&nbsp;</font><font face="Arial" size="2">$<?printf("%01.2f",$total);?></font></b></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">Rentals-AZ</font></td>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">Tel: (480) 785-7400</font></td>
          </tr>
          <tr>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">8920 South Hardy Drive</font></td>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">Fax: (480) 763-8108</font></td>
          </tr>
          <tr>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">Tempe, Arizona&nbsp; 85284</font></td>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">http://www.rentals-az.com</font></td>
          </tr>
          <tr>
            <td width="50%" bgcolor="#EEEEEE" align="center">&nbsp;</td>
            <td width="50%" bgcolor="#EEEEEE" align="center">
            <font face="Arial" size="2">brian@rentals-az.com</font></td>
          </tr>
        </table>
        </td>
      </tr>
      <form method="POST" action="../cgi-bin/process.pl" onSubmit="return validate(this)">
      <tr>
        <td width="100%">
        <div align="center">
          <center>
          <table border="0" cellspacing="0" width="400" cellpadding="0">
            <tr>
              <td width="100%" bgcolor="#FF0000">
              <table border="0" cellspacing="1" width="100%" cellpadding="0">
                <tr>
                  <td width="100%" bgcolor="#FFFFFF">
                  <p align="center"><u><font face="Verdana" size="2">Make a
                  Payment Online</font></u></p>
                  <table border="0" cellspacing="0" width="100%" cellpadding="4">
                    <tr>
                      <td width="100%"><font face="Verdana" size="1">We accept
                      Master Card &amp; Visa. All credit card transactions are
                      processed on a secure server through Authorize.net. This
                      is the quickest and most convenient of all payment
                      methods. This is a one time payment; your credit card
                      information will NOT be stored in our database.</font></td>
                    </tr>
                    <tr>
                      <td width="100%">&nbsp;</td>
                    </tr>
                  </table>
                  <table border="0" cellspacing="0" width="100%" cellpadding="0">
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Payment
                      for:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="x_Description"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;First
                      Name:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="x_First_Name" value="<?echo $fname;?>"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Last
                      Name:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="x_Last_Name" value="<?echo $lname;?>"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Email:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="x_Email" value="<?echo $cust["email"];?>"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Amount:</font></td>
                      <td width="55%"><font face="Verdana">$</font><b><font face="Arial" size="2"><input type="text" size="7" name="x_Amount"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left">&nbsp;</td>
                      <td width="55%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;CC
                      Number:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="17" name="x_Card_Num"> </font></b>
                      <img src="images/visa_mc.gif" border="0" width="63" height="20"></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Expiration:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="7" name="x_Exp_Date"> </font>
                      <font face="Verdana" size="1">mm/yy</font></b></td>
                    </tr>
                    <tr>
                      <td width="15%">&nbsp;</td>
                      <td width="29%">&nbsp;</td>
                      <td width="55%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15%">&nbsp;</td>
                      <td width="29%">&nbsp;</td>
                      <td width="55%">
                      <input type="submit" value="Submit Payment" name="submit"></td>
                    </tr>
                    <tr>
                      <td width="15%">&nbsp;</td>
                      <td width="29%">&nbsp;</td>
                      <td width="55%">&nbsp;</td>
                    </tr>
                  </table>
                  </td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
          </center>
        </div>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <p align="center">
        <img src="images/geotrust.gif" border="0" width="100" height="19">&nbsp;&nbsp;&nbsp;
        <img src="images/authorizenet.gif" border="0" width="100" height="21"></td>
      </tr>
     <input type="hidden" name="x_Cust_ID" value="<?echo $cust["id"];?>">
     </form>
    </table>
    </td>
    <td width="234" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="0">
      <tr>
        <td width="100%">
        <img border="0" src="images/arizona.gif" width="120" height="51"></td>
      </tr>
      <tr>
        <td width="100%">
        <img border="0" src="images/home_front.jpg" name="SlideShow" id="SlideShow" width="207" height="191"></td>
      </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="100%">&nbsp;</td>
  </tr>
</table>
<script language="javascript">
runSlideShow();
</script>
</body>

</html>