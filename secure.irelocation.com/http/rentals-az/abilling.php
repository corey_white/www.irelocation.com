<?

include_once("mysql.php");

if($c!=md5(date("Y-m-d")))
{
 die("Invalid customer id!");
}

$rs=new mysql_recordset("select * from customers where arrival>='".date("Y-m-d")."' order by arrival");
while($rs->fetch_array())
{
  $cust[]=$rs->myarray;
  $rs2=new mysql_recordset("select * from transactions where cust_id='".$rs->myarray["id"]."'");
  $tempbalance=0;
  while($rs2->fetch_array())
  {
    $tempbalance+=$rs2->myarray["total"];
  }
  $balance[$rs->myarray["id"]]=$tempbalance;
}

$rs=new mysql_recordset("select * from customers where arrival<'".date("Y-m-d")."' order by arrival");
while($rs->fetch_array())
{
  $cust2[]=$rs->myarray;
  $rs2=new mysql_recordset("select * from transactions where cust_id='".$rs->myarray["id"]."'");
  $tempbalance=0;
  while($rs2->fetch_array())
  {
    $tempbalance+=$rs2->myarray["total"];
  }
  $balance[$rs->myarray["id"]]=$tempbalance;
}




?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rentals Arizona - Billing Information / Invoice</title>
<style type="text/css">
<!--
a:link
{
  text-decoration: none;
}
a:visited
{
  text-decoration: none;
}
a:hover
{
  text-decoration: underline;
}
input, select, textarea
{
  font-size: 11px;
  background: #FAFAFA;
  border: 1px solid;
  border-color : #7297B5;
}
//-->
</style>

<script language="javascript">
var slideShowSpeed = 5000
var crossFadeDuration = 3
var Pic = new Array()

        Pic[0] = 'images/home_front.jpg'
        Pic[1] = 'images/home_lake.jpg'
        Pic[2] = 'images/home_back.jpg'
        Pic[3] = 'images/home_belt.jpg'

var t
var j = 0
var p = Pic.length

var preLoad = new Array()
for (i = 0; i < p; i++){
   preLoad[i] = new Image()
   preLoad[i].src = Pic[i]
}

function runSlideShow(){
   if (document.all){
      document.images.SlideShow.style.filter="blendTrans(duration=2)"
      document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)"
      document.images.SlideShow.filters.blendTrans.Apply()
   }
   document.images.SlideShow.src = preLoad[j].src
   if (document.all){
      document.images.SlideShow.filters.blendTrans.Play()
   }
   j = j + 1
   if (j > (p-1)) j=0
   t = setTimeout('runSlideShow()', slideShowSpeed)
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">


<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td align="left" valign="bottom" width="571">
    <img border="0" src="images/top_bar.gif" width="406" height="48"></td>
    <td width="129">
    <p align="right">
    <a href="index.htm">
    <img border="0" src="images/logo.gif" width="129" height="94"></a></td>
  </tr>
</table>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="700">
    <img border="0" src="images/mountain_bar.jpg" width="700" height="115"></td>
    <td>&nbsp;</td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="466" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="10">
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="5">
          <tr>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center"><b><font face="Arial">B</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">N</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">G</font></b></td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="10%" align="center">&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" bgcolor="#000000">
            <table border="0" cellspacing="1" width="100%" cellpadding="2">
              <tr>
                <td width="15%" bgcolor="#DDB7B7" height="16">
                <font face="Arial" size="2">&nbsp;Arrival</font></td>
                <td width="60%" bgcolor="#DDB7B7" height="16">
                <font face="Arial" size="2">&nbsp;Name</font></td>
                <td width="15%" bgcolor="#DDB7B7" height="16">
                <font face="Arial" size="2">&nbsp;Balance</font></td>
              </tr>
              <?
              if($cust2[0]["arrival"]!='')
              {
              foreach($cust2 as $mycust)
              {
              if($balance[$mycust["id"]]>0)
              {
              ?>
              <tr>
                <td width="15%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mycust["arrival"];?></font></td>
                <td width="60%" bgcolor="#FFFFFF" height="13">
                <a href="billing.php?user=<?echo $mycust["id"];?>"><font face="Arial" size="1" color="#000000">&nbsp;<?echo $mycust["name"];?></font></a></td>
                <td width="15%" bgcolor="#FFFFFF" height="13" align="right">
                <font face="Arial" size="1">&nbsp;$<?printf("%01.2f",$balance[$mycust["id"]]);?> &nbsp;<a href="addtransaction.php?id=<?echo $mycust["id"];?>"><img src="images/bill.gif" border="0" alt="Add a New Transaction"></a></font></td>
              </tr>
              <?
              }
              }
              }
              ?>
              <tr>
                <td width="15%" bgcolor="#FFFFFF" height="14">
                <font size="1" face="Arial">&nbsp;</font></td>
                <td width="60%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
                <td width="15%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" bgcolor="#000000">
            <table border="0" cellspacing="1" width="100%" cellpadding="2">
              <tr>
                <td width="15%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Arrival</font></td>
                <td width="60%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Name</font></td>
                <td width="15%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Balance</font></td>
              </tr>
              <?
              if($cust[0]["arrival"]!='')
              {
              foreach($cust as $mycust)
              {
              ?>
              <tr>
                <td width="15%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mycust["arrival"];?></font></td>
                <td width="60%" bgcolor="#FFFFFF" height="13">
                <a href="billing.php?user=<?echo $mycust["id"];?>"><font face="Arial" size="1" color="#000000">&nbsp;<?echo $mycust["name"];?></font></a></td>
                <td width="15%" bgcolor="#FFFFFF" height="13" align="right">
                <font face="Arial" size="1">&nbsp;$<?printf("%01.2f",$balance[$mycust["id"]]);?> &nbsp;<a href="addtransaction.php?id=<?echo $mycust["id"];?>"><img src="images/bill.gif" border="0" alt="Add a New Transaction"></a></font></td>
              </tr>
              <?
              }
              }
              ?>
              <tr>
                <td width="15%" bgcolor="#FFFFFF" height="14">
                <font size="1" face="Arial">&nbsp;</font></td>
                <td width="60%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
                <td width="15%" bgcolor="#FFFFFF" height="14">
                <font face="Arial" size="1">&nbsp;</font></td>
              </tr>
            </table>
            </td>
          </tr>
        </table>
        </td>
      </tr>
      <form method="POST" action="addcustomer.php">
      <tr>
        <td width="100%">
        <div align="center">
          <center>
          <table border="0" cellspacing="0" width="400" cellpadding="0">
            <tr>
              <td width="100%" bgcolor="#FF0000">
              <table border="0" cellspacing="1" width="100%" cellpadding="0">
                <tr>
                  <td width="100%" bgcolor="#FFFFFF">
                  <p align="center"><u><font face="Verdana" size="2">Add A New Customer</font></u></p>
                  <table border="0" cellspacing="0" width="100%" cellpadding="0">
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Customer ID:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="id"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Name:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="name"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Email:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="email"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Address:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="address"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;City:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="20" name="city"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;State:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="4" name="state" maxlength="2"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Zip:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="4" name="zip" maxlength="10"></font></b></td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left">&nbsp;</td>
                      <td width="55%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="15%" align="left">&nbsp;</td>
                      <td width="29%" align="left"><font face="Verdana" size="2">&nbsp;Arrival:</font></td>
                      <td width="55%"><b><font face="Arial" size="2">
                      <input type="text" size="9" name="arrival" maxlength="10"> </font>
                      <font face="Verdana" size="1">YYYY-MM-DD</font></b></td>
                    </tr>
                    <tr>
                      <td width="15%">&nbsp;</td>
                      <td width="29%">&nbsp;</td>
                      <td width="55%">
                      <input type="submit" value="Add Customer" name="submit"></td>
                    </tr>
                    <tr>
                      <td width="15%">&nbsp;</td>
                      <td width="29%">&nbsp;</td>
                      <td width="55%">&nbsp;</td>
                    </tr>
                  </table>
                  </td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
          </center>
        </div>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <p align="center">
        <img src="images/geotrust.gif" border="0" width="100" height="19">&nbsp;&nbsp;&nbsp;
        <img src="images/authorizenet.gif" border="0" width="100" height="21"></td>
      </tr>
     </form>
    </table>
    </td>
    <td width="234" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="0">
      <tr>
        <td width="100%">
        <img border="0" src="images/arizona.gif" width="120" height="51"></td>
      </tr>
      <tr>
        <td width="100%">
        <img border="0" src="images/home_front.jpg" name="SlideShow" id="SlideShow" width="207" height="191"></td>
      </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="100%">&nbsp;</td>
  </tr>
</table>
<script language="javascript">
runSlideShow();
</script>
</body>

</html>