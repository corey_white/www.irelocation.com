<?
include_once("mysql.php");
if(!isset($id) && !isset($cust_id))
{
  die("Go Away!");
}

if(isset($cust_id))
{
  $rs=new mysql_recordset("insert into transactions values ('$cust_id','".time()."','$description','$rate','$total')");
  $code=md5(date("Y-m-d"));
  header("location: abilling.php?c=$code");
  exit;
}
else
{

$rs_cust=new mysql_recordset("select * from customers where id='$id'");
if($rs_cust->rowcount()>0)
{
 $rs_cust->fetch_array();
 $cust=$rs_cust->myarray;
}
else
{
 die("Invalid customer id!");
}

$total=0;
$rs_trans=new mysql_recordset("select * from transactions where cust_id='$id'");
while($rs_trans->fetch_array())
{
  $trans[]=$rs_trans->myarray;
  $total+=$rs_trans->myarray["total"];
}

}
?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rentals Arizona - Billing Information / Invoice</title>
<style type="text/css">
<!--
a:link
{
  text-decoration: none;
}
a:visited
{
  text-decoration: none;
}
a:hover
{
  text-decoration: underline;
}
input, select, textarea
{
  font-size: 13px;
  background: #FFFFFF;
  border: 2px solid;
  border-color : #FFFFFF;
}
//-->
</style>

<script language="javascript">
var slideShowSpeed = 5000
var crossFadeDuration = 3
var Pic = new Array()

        Pic[0] = 'images/home_front.jpg'
        Pic[1] = 'images/home_lake.jpg'
        Pic[2] = 'images/home_back.jpg'
        Pic[3] = 'images/home_belt.jpg'

var t
var j = 0
var p = Pic.length

var preLoad = new Array()
for (i = 0; i < p; i++){
   preLoad[i] = new Image()
   preLoad[i].src = Pic[i]
}

function runSlideShow(){
   if (document.all){
      document.images.SlideShow.style.filter="blendTrans(duration=2)"
      document.images.SlideShow.style.filter="blendTrans(duration=crossFadeDuration)"
      document.images.SlideShow.filters.blendTrans.Apply()
   }
   document.images.SlideShow.src = preLoad[j].src
   if (document.all){
      document.images.SlideShow.filters.blendTrans.Play()
   }
   j = j + 1
   if (j > (p-1)) j=0
   t = setTimeout('runSlideShow()', slideShowSpeed)
}
</script>
</head>
<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0" text="#000000" link="#000000" vlink="#000000" alink="#000000">


<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td align="left" valign="bottom" width="571">
    <img border="0" src="images/top_bar.gif" width="406" height="48"></td>
    <td width="129">
    <p align="right">
    <a href="index.htm">
    <img border="0" src="images/logo.gif" width="129" height="94"></a></td>
  </tr>
</table>
<table border="0" cellspacing="0" width="100%" cellpadding="0">
  <tr>
    <td width="700">
    <img border="0" src="images/mountain_bar.jpg" width="700" height="115"></td>
    <td>&nbsp;</td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="466" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="10">
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="5">
          <tr>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="9%" align="center"><b><font face="Arial">B</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">L</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">I</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">N</font></b></td>
            <td width="9%" align="center"><b><font face="Arial">G</font></b></td>
            <td width="9%" align="center">&nbsp;</td>
            <td width="10%" align="center">&nbsp;</td>
          </tr>
        </table>
        </td>
      </tr>
      <?if($msg){?>
      <tr>
        <td width="100%">
          <font color="#FF0000" size="2" face="verdana"><?echo $msg;?></font>
        </td>
      </tr>
      <?}?>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="25%"><font face="Arial"><b><i><font size="2">Customer ID:</font></i></b><font size="2">
            <?echo $cust["id"];?></font></font></td>
            <td width="25%"><b><i><font size="2" face="Arial">Bill To:</font></i></b></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font size="2" face="Arial"><?echo $cust["name"];?></font></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font face="Arial" size="2"><?echo $cust["address"];?></font></td>
          </tr>
          <tr>
            <td width="25%">&nbsp;</td>
            <td width="25%"><font face="Arial" size="2"><?echo $cust["city"];?>, <?echo $cust["state"];?> <?echo $cust["zip"];?></font></td>
          </tr>
        </table>
        </td>
      </tr>
      <tr>
        <td width="100%">
        <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr>
            <td width="100%" bgcolor="#000000">
            <form method="post" action="addtransaction.php">
            <table border="0" cellspacing="1" width="100%" cellpadding="0" height="126">
              <tr>
                <td width="68%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Description</font></td>
                <td width="18%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Rate</font></td>
                <td width="14%" bgcolor="#C0C0C0" height="16">
                <font face="Arial" size="2">&nbsp;Total</font></td>
              </tr>
              <?
              if($trans[0]["total"]!='')
              {
              foreach($trans as $mytrans)
              {
              ?>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["description"];?></font></td>
                <td width="18%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["rate"];?></font></td>
                <td width="14%" bgcolor="#FFFFFF" height="13">
                <font face="Arial" size="1">&nbsp;<?echo $mytrans["total"];?></font></td>
              </tr>
              <?
              }
              }
              ?>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="14"><input type="text" size="40" name="description" value="RENTAL - Arriving [ ] - Departing [ ]"></td>
                <td width="18%" bgcolor="#FFFFFF" height="14"><input type="text" size="10" name="rate" value="[ ]/week"></td>
                <td width="14%" bgcolor="#FFFFFF" height="14"><input type="text" size="10" name="total" value="0.00"></td>
              </tr>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
                <td width="18%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
                <td width="14%" bgcolor="#FFFFFF" height="14"><b>
                <font face="Arial" size="1">&nbsp;</font></b></td>
              </tr>
              <tr>
                <td width="68%" bgcolor="#FFFFFF" height="19">&nbsp;</td>
                <td width="18%" bgcolor="#C0C0C0" height="19">
                <font face="Arial" size="1">&nbsp;</font><font face="Arial" size="2">Balance
                Due</font></td>
                <td width="14%" bgcolor="#C0C0C0" height="19"><b>
                <font face="Arial" size="1">&nbsp;</font><font face="Arial" size="2">$<?printf("%01.2f",$total);?></font></b></td>
              </tr>
            </table>
            <input type="hidden" name="cust_id" value="<?echo $cust["id"];?>">
            <center><input type="submit" name="submit" value="Enter New Transaction"></center>
            </form>
            </td>
          </tr>
        </table>
        </td>
      </tr>
    </table>
    </td>
    <td width="234" valign="top">
    <table border="0" cellspacing="0" width="100%" cellpadding="0">
      <tr>
        <td width="100%">
        <img border="0" src="images/arizona.gif" width="120" height="51"></td>
      </tr>
      <tr>
        <td width="100%">
        <img border="0" src="images/home_front.jpg" name="SlideShow" id="SlideShow" width="207" height="191"></td>
      </tr>
      </table>
    </td>
  </tr>
</table>
<table border="0" cellspacing="0" width="700" cellpadding="0">
  <tr>
    <td width="100%">&nbsp;</td>
  </tr>
</table>
<script language="javascript">
runSlideShow();
</script>
</body>

</html>