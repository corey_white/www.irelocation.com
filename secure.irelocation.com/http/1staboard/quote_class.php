<?
#
# Quote Process Script
#

class moving_quote
{
  var $cat_id;
  var $name;
  var $email;
  var $phone_home;
  var $contact;
  var $est_move_date;
  var $origin_city;
  var $origin_state;
  var $origin_zip;
  var $destination_city;
  var $destination_state;

  var $move_type;
  var $type_home;
  var $bedrooms;
  var $attic;
  var $living;
  var $basement;
  var $office;
  var $den;
  var $patio;
  var $dining;
  var $play;
  var $kitchen;
  var $shed;

  var $elevator;
  var $stairs;
  var $comments;

  var $source;
  var $keyword;

  var $msg;

  function moving_quote()
  {
    $this->cat_id='2';
  }

  function getcitystate()
  {
    $zip=$this->origin_zip;

    include_once("mysql.php");
    $sql="select * from zip_codes where zip='$zip'";
    $rs=new mysql_recordset($sql);
    $rs->fetch_array();
    $this->origin_city=str_replace("'","",$rs->myarray["city"]);
    $this->origin_state=$rs->myarray["state"];

    /*
    //get city and state from zip at usps
    $ch=curl_init("http://56.0.134.24/zip4/zip_response.jsp");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "Selection=3&zipcode=$zip");  // set the fields to post
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    // make sure we get the response back
    $buffer = curl_exec($ch);                       // execute the post
    curl_close($ch);                                // close our session

    $content=explode('<font size="-1" face="Arial, Helvetica, sans-serif">',$buffer);
    $content2=explode('</font>',$content[5]);
    $this->origin_city=ucwords(strtolower(trim($content2[0])));
    $content3=explode('</font>',$content[6]);
    $this->origin_state=trim($content3[0]);
    */
  }

  function save_quote()
  {
    //set up fields
    $this->name=ucwords(strtolower($this->name));
    $this->email=strtolower($this->email);
    if(substr_count($this->email,"@")==0)
    {
      $this->msg.="Email address invalid<br>";
    }
    $this->phone_home=str_replace("-","",str_replace(".","",str_replace("(","",str_replace(")"," ",str_replace("-","",$this->phone_home)))));

    if(strlen($this->origin_zip)<5)
    {
      $this->msg.="Zip code invalid";
    }
    else
    {
      $this->getcitystate();
    }
    $this->destination_city=ucwords(strtolower($this->destination_city));
    $this->destination_state=strtoupper($this->destination_state);

    if($this->attic=='1'){$rooms.="attic,";}
    if($this->basement=='1'){$rooms.="basement,";}
    if($this->den=='1'){$rooms.="den,";}
    if($this->dining=='1'){$rooms.="dining room,";}
    if($this->kitchen=='1'){$rooms.="kitchen,";}
    if($this->living=='1'){$rooms.="living room,";}
    if($this->office=='1'){$rooms.="office,";}
    if($this->patio=='1'){$rooms.="patio,";}
    if($this->play=='1'){$rooms.="play room,";}
    if($this->shed=='1'){$rooms.="shed,";}
    if($rooms!='')
    {
      $rooms=" with ".substr($rooms,0,-1);
    }

    $this->comments="Type of Move: ".$this->move_type."\nType of Home: ".$this->type_home."\nFurnished Rooms: ".$this->bedrooms."$rooms\nElevator: ".$this->elevator."\nStairs: ".$this->stairs."\n\nComments/Unique Items: ".$this->comments."";

    if($this->msg!='')
    {
      return false;
      break;
    }

    //connect to database
    include_once("mysql.php");
    $sql="insert into quotes (cat_id,ready_to_send,name,email,phone_home,contact,est_move_date,origin_city,origin_state,origin_zip,destination_city,destination_state,comments,source,keyword) values ('".$this->cat_id."','1','".$this->name."','".$this->email."','".$this->phone_home."','".$this->contact."','".$this->est_move_date."','".$this->origin_city."','".$this->origin_state."','".$this->origin_zip."','".$this->destination_city."','".$this->destination_state."','".$this->comments."','".$this->source."','".$this->keyword."')";
    $rs=new mysql_recordset($sql);
    return true;
  }
}

class auto_quote
{
  var $cat_id;
  var $name;
  var $email;
  var $phone_home;
  var $contact;
  var $est_move_date;
  var $origin_city;
  var $origin_state;
  var $origin_zip;
  var $destination_city;
  var $destination_state;

  var $type;
  var $make;
  var $model;
  var $year;
  var $comments;

  var $source;
  var $keyword;

  var $msg;

  function auto_quote()
  {
    $this->cat_id='1';
  }

  function getcitystate()
  {
    $zip=$this->origin_zip;

    include_once("mysql.php");
    $sql="select * from zip_codes where zip='$zip'";
    $rs=new mysql_recordset($sql);
    $rs->fetch_array();
    $this->origin_city=str_replace("'","",$rs->myarray["city"]);
    $this->origin_state=$rs->myarray["state"];

    /*
    //get city and state from zip at usps
    $ch=curl_init("http://56.0.134.24/zip4/zip_response.jsp");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "Selection=3&zipcode=$zip");  // set the fields to post
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    // make sure we get the response back
    $buffer = curl_exec($ch);                       // execute the post
    curl_close($ch);                                // close our session

    $content=explode('<font size="-1" face="Arial, Helvetica, sans-serif">',$buffer);
    $content2=explode('</font>',$content[5]);
    $this->origin_city=ucwords(strtolower(trim($content2[0])));
    $content3=explode('</font>',$content[6]);
    $this->origin_state=trim($content3[0]);
    */
  }

  function save_quote()
  {
    //set up fields
    $this->name=ucwords(strtolower($this->name));
    $this->email=strtolower($this->email);
    if(substr_count($this->email,"@")==0)
    {
      $this->msg.="Email address invalid<br>";
    }
    $this->phone_home=str_replace("-","",str_replace(".","",str_replace("(","",str_replace(")"," ",str_replace("-","",$this->phone_home)))));

    if(strlen($this->origin_zip)<5)
    {
      $this->msg.="Zip code invalid";
    }
    else
    {
      $this->getcitystate();
    }
    $this->destination_city=ucwords(strtolower($this->destination_city));
    $this->destination_state=strtoupper($this->destination_state);

    $this->comments="Vehicle Type: ".$this->type."\nVehicle Year: ".$this->year."\nVehicle Make: ".$this->make."\nVehicle Model: ".$this->model."\n\nComments: ".$this->comments."";

    if($this->msg!='')
    {
      return false;
      break;
    }

    //connect to database
    include_once("mysql.php");
    $sql="insert into quotes (cat_id,ready_to_send,name,email,phone_home,contact,est_move_date,origin_city,origin_state,origin_zip,destination_city,destination_state,comments,source,keyword) values ('".$this->cat_id."','1','".$this->name."','".$this->email."','".$this->phone_home."','".$this->contact."','".$this->est_move_date."','".$this->origin_city."','".$this->origin_state."','".$this->origin_zip."','".$this->destination_city."','".$this->destination_state."','".$this->comments."','".$this->source."','".$this->keyword."')";
    $rs=new mysql_recordset($sql);
    return true;
  }
}
?>