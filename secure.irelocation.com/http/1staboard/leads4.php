<?
include_once("quote_class.php");

if(!$contents){exit;}

$temp=explode("contactname:",$contents);
$temp2=explode("\n",$temp[1]);
$name=str_replace("\\","",str_replace("'","",ucwords(trim(str_replace("\r","",$temp2[0])))));

$temp=explode("contact2:",$contents);
$temp2=explode("\n",$temp[1]);
$email=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("phone:",$contents);
$temp2=explode("\n",$temp[1]);
$phone=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("OriginCity:",$contents);
$temp2=explode("\n",$temp[1]);
$origin_zip=str_replace("\\","",str_replace("'","",ucwords(trim(str_replace("\r","",$temp2[0])))));

$temp=explode("DestinationCity:",$contents);
$temp2=explode("\n",$temp[1]);
$destination_city=str_replace("\\","",str_replace("'","",ucwords(trim(str_replace("\r","",$temp2[0])))));

$temp=explode("DestinationState:",$contents);
$temp2=explode("\n",$temp[1]);
$destination_state=str_replace("\\","",str_replace("'","",strtoupper(trim(str_replace("\r","",$temp2[0])))));

$temp=explode("D1:",$contents);
$temp2=explode("\n",$temp[1]);
$temp3=str_replace("\\","",str_replace("'","",strtolower(trim(str_replace("\r","",$temp2[0])))));
switch($temp3)
{
  case "january": $month=1; break;
  case "february": $month=2; break;
  case "march": $month=3; break;
  case "april": $month=4; break;
  case "may": $month=5; break;
  case "june": $month=6; break;
  case "july": $month=7; break;
  case "august": $month=8; break;
  case "september": $month=9; break;
  case "october": $month=10; break;
  case "november": $month=11; break;
  case "december": $month=12; break;
}

$temp=explode("D2:",$contents);
$temp2=explode("\n",$temp[1]);
$day=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("ShipYear:",$contents);
$temp2=explode("\n",$temp[1]);
$year=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("move_type:",$contents);
$temp2=explode("\n",$temp[1]);
$move_type=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("type_home:",$contents);
$temp2=explode("\n",$temp[1]);
$type_home=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("Bedrooms:",$contents);
$temp2=explode("\n",$temp[1]);
$bedrooms=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("Elevator:",$contents);
$temp2=explode("\n",$temp[1]);
$elevator=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("Stairs:",$contents);
$temp2=explode("\n",$temp[1]);
$stairs=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("play:",$contents);
$temp2=explode("\n",$temp[1]);
$play=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("shed:",$contents);
$temp2=explode("\n",$temp[1]);
$shed=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("attic:",$contents);
$temp2=explode("\n",$temp[1]);
$attic=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("basement:",$contents);
$temp2=explode("\n",$temp[1]);
$basement=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("den:",$contents);
$temp2=explode("\n",$temp[1]);
$den=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("dining:",$contents);
$temp2=explode("\n",$temp[1]);
$dining=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("kitchen:",$contents);
$temp2=explode("\n",$temp[1]);
$kitchen=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("living:",$contents);
$temp2=explode("\n",$temp[1]);
$living=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("office:",$contents);
$temp2=explode("\n",$temp[1]);
$office=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("patio:",$contents);
$temp2=explode("\n",$temp[1]);
$patio=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$temp=explode("S1:",$contents);
$temp2=explode("\n",$temp[1]);
$comments=str_replace("\\","",str_replace("'","",trim(str_replace("\r","",$temp2[0]))));

$origin_zip=intval($origin_zip);

if(strlen($origin_zip)==5)
{

$q=new moving_quote();

$q->name=$name;
$q->email=$email;
$q->phone_home=$phone;
$q->contact="email";
$q->est_move_date="$year-$month-$day";
$q->origin_zip=$origin_zip;
$q->destination_city=$destination_city;
$q->destination_state=$destination_state;
$q->move_type=$move_type;
$q->type_home=$type_home;
$q->bedrooms=$bedrooms;
$q->attic=$attic;
$q->living=$living;
$q->basement=$basement;
$q->office=$office;
$q->den=$den;
$q->patio=$patio;
$q->dining=$dining;
$q->play=$play;
$q->kitchen=$kitchen;
$q->shed=$shed;
$q->elevator=$elevator;
$q->stairs=$stairs;
$q->comments=$comments;
$q->source="1staboard.com";

$result=$q->save_quote();

}
?>