<?

#
# Add new campaign
#
if($action2=='add_new_campaign')
{
  //check to see if that campaign already exists
  $rs=new mysql_recordset("select * from directleads where cat_id='$cat_id' and comp_id='$id'");
  if($rs->rowcount()>0)
  {
    $msg="Add New Campaign Failed! There is already a campaign for this category!";
  }
  else
  {
    //check all values
    if(($cost_per_lead<0)||($cost_per_lead==''))
    {
      $msg="Add New Campaign Failed! Missing cost per lead!";
    }
    if(($cost_per_lead<0)||($cost_per_lead==''))
    {
      $msg="Add New Campaign Failed! Missing credit!";
    }

    //add campaign to database
    if($msg=='')
    {
      $rs=new mysql_recordset("insert into directleads (comp_id,cat_id,credit,cost_per_lead,active,advantage) values ('$id','$cat_id','$credit','$cost_per_lead','1','$advantage')");
      $msg="New campaign has been added!";
    }
  }
}
#
# Add new rule
#
elseif($action2=='add_new_rule')
{
  list($lead_id,$name)=explode("|",$thelead);

  //check to see if that rule already exists
  $rs=new mysql_recordset("select * from rules where lead_id='$lead_id' and origin='$origin' and destination='$destination'");
  if($rs->rowcount()>0)
  {
    $msg="Add New Rule Failed! That rule already exists!";
  }
  else
  {
    //check all values
    if($email=='')
    {
      $msg="Add New Rule Failed! The email address is missing!";
    }

    //add rule to database
    if($msg=='')
    {
      $rs=new mysql_recordset("insert into rules (lead_id,user_id,origin,destination,email,name) values ('$lead_id','$id','$origin','$destination','$email','$name')");
      $msg="New rule has been added!";
    }
  }
}
#
# Update campaign
#
elseif($action2=='update_campaign')
{
  //NOTHING TO UPDATE AS OF NOW
}
#
# Delete campaign
#
elseif($action2=='delete_campaign')
{
  //DO NOT GIVE THE ABILITY TO DELETE THE CAMPAIGNS!!!  WE WANT TO ARCHIVE ALL INFO!!!
}
#
# Delete rule
#
elseif($action2=='delete_rule')
{
  $rs=new mysql_recordset("delete from rules where id='$rule_id' and user_id='$id'");
  $msg="Rule #$rule_id has been deleted!";
}
#
# Reactivate Campaign
#
elseif($action2=='reactivate')
{
  $rs=new mysql_recordset("update directleads set active='1' where lead_id='$lead_id' and comp_id='$id'");
  $msg="Campaign #$lead_id has been reactivated!";
}
#
# Deactivate Campaign
#
elseif($action2=='deactivate')
{
  $rs=new mysql_recordset("update directleads set active='0' where lead_id='$lead_id' and comp_id='$id'");
  $msg="Campaign #$lead_id has been deactivated!";
}

#
# Get the company info
#
$rs=new mysql_recordset("select * from company where comp_id='$id'");
$rs->fetch_array();
$comp_info=$rs->myarray;

#
# Get all current lead campaigns
#
$rs=new mysql_recordset("select directleads.*,directleadscategory.name from directleads,directleadscategory where directleads.cat_id=directleadscategory.cat_id and comp_id='$id'");
if($rs->rowcount()>0)
{
  $hasleads=TRUE;
  while($rs->fetch_array())
  {
    $campaigns[]=$rs->myarray;
  }

  #
  # Get the current rules
  #
  $rs=new mysql_recordset("select * from rules where user_id='$id'");
  if($rs->rowcount()>0)
  {
    $hasrules=TRUE;
    while($rs->fetch_array())
    {
      $rules[]=$rs->myarray;
    }
  }
  else
  {
    $hasrules=FALSE;
  }
}
else
{
  $hasleads=FALSE;
}

#
# Get current lead categories with pricing
#
$rs=new mysql_recordset("select * from directleadscategory order by name");
while($rs->fetch_array())
{
  $cats[]=$rs->myarray;
}


?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Lead Information for <?echo $comp_info["comp_name"];?></title>
<script language=JavaScript>

function open_action_win(url) {
  window.open(url,"add_money",'toolbar=0,location=0,directories=0,status=1,menubar=0,scrollbars=1,resizable=1,width=350,height=300');
}
</script>
</head>

<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Add a Lead Campaign</b></font><?if($hasleads==FALSE){?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="heading2">No Lead Campaigns Found!</span><?}?></p>
<form name="lead_info" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_leads">
<input type="hidden" name="action2" value="add_new_campaign">
<input type="hidden" name="id" value="<?echo $id;?>">
<input type="hidden" name="cost_per_lead" value="0">
<input type="hidden" name="credit" value="100">
<input type="hidden" name="advantage" value="1">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="200"><span class="text_2">Category:</span></td>
          <td width="300"><select name="cat_id" class="quote_box">
          <?
          foreach($cats as $mycat)
          {
            echo "<option value=\"".$mycat["cat_id"]."\">".$mycat["name"]."</option>";
          }
          ?></select></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300">&nbsp;</td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Add New Campaign" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<?
if($hasleads==TRUE)
{
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Current Lead Campaigns</b></font></p>
    <span class="notice">
      <table cellSpacing="2" cellPadding="2" width="500" border="0">
        <tr>
          <td width="200"><span class="heading">category</span></td>
          <td width="120"><span class="heading">leads sent</span></td>
          <td width="180"><span class="heading">options</span></td>
        </tr>
        <?
        foreach($campaigns as $mycampaign)
        {
        ?>
        <tr<?if($mycampaign["active"]!='1'){echo " class='quote_col_x'";}?>>
          <td width="200"><span class="realsmall"><?echo $mycampaign["name"];?></span></td>
          <td width="120"><span class="realsmall"><?echo $mycampaign["total_leads_sent"];?></span></td>
          <td width="180"><a href="control.php?action=1md_companies_leads&action2=<?if($mycampaign["active"]=='1'){echo "d";}else{echo "r";}?>eactivate&id=<?echo $id;?>&lead_id=<?echo $mycampaign["lead_id"];?>"><img src="images/b<?if($mycampaign["active"]=='1'){echo "d";}else{echo "r";}?>eactivate2.gif" border="0"></a></td>
        </tr>
        <?
        }
        ?>
      </table>
    </span>
<p></p>
<p><font face="verdana" color="#2360A5" size="3"><b> Add a New Rule</b></font></p>
<form name="rule_info" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_leads">
<input type="hidden" name="action2" value="add_new_rule">
<input type="hidden" name="id" value="<?echo $id;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="200"><span class="text_2">Campaign:</span></td>
          <td width="300"><select name="thelead" class="quote_box">
          <?
          foreach($campaigns as $mycampaign)
          {
            echo "<option value=\"".$mycampaign["lead_id"]."|".$mycampaign["name"]."\">".$mycampaign["name"]."</option>";
          }
          ?></select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Origin:</span></td>
          <td width="300"><select name="origin" class="quote_box">
          <option value="ANY" selected>ANY STATE</option>
    <option value="AL">AL</option>
    <option value="AK">AK</option>
    <option value="AZ">AZ</option>
    <option value="AR">AR</option>
    <option value="CA">CA</option>
    <option value="CO">CO</option>
    <option value="CT">CT</option>
    <option value="DC">DC</option>
    <option value="DE">DE</option>
    <option value="FL">FL</option>
    <option value="GA">GA</option>
    <option value="HI">HI</option>
    <option value="ID">ID</option>
    <option value="IL">IL</option>
    <option value="IN">IN</option>
    <option value="IA">IA</option>
    <option value="KS">KS</option>
    <option value="KY">KY</option>
    <option value="LA">LA</option>
    <option value="ME">ME</option>
    <option value="MD">MD</option>
    <option value="MA">MA</option>
    <option value="MI">MI</option>
    <option value="MN">MN</option>
    <option value="MS">MS</option>
    <option value="MO">MO</option>
    <option value="MT">MT</option>
    <option value="NE">NE</option>
    <option value="NV">NV</option>
    <option value="NH">NH</option>
    <option value="NJ">NJ</option>
    <option value="NM">NM</option>
    <option value="NY">NY</option>
    <option value="NC">NC</option>
    <option value="ND">ND</option>
    <option value="OH">OH</option>
    <option value="OK">OK</option>
    <option value="OR">OR</option>
    <option value="PA">PA</option>
    <option value="RI">RI</option>
    <option value="SC">SC</option>
    <option value="SD">SD</option>
    <option value="TN">TN</option>
    <option value="TX">TX</option>
    <option value="VI">VI</option>
    <option value="UT">UT</option>
    <option value="VT">VT</option>
    <option value="VA">VA</option>
    <option value="WA">WA</option>
    <option value="WV">WV</option>
    <option value="WI">WI</option>
    <option value="WY">WY</option>
    </select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Destination:</span></td>
          <td width="300"><select name="destination" class="quote_box">
          <option value="ANY" selected>ANY STATE</option>
    <option value="AL">AL</option>
    <option value="AK">AK</option>
    <option value="AZ">AZ</option>
    <option value="AR">AR</option>
    <option value="CA">CA</option>
    <option value="CO">CO</option>
    <option value="CT">CT</option>
    <option value="DC">DC</option>
    <option value="DE">DE</option>
    <option value="FL">FL</option>
    <option value="GA">GA</option>
    <option value="HI">HI</option>
    <option value="ID">ID</option>
    <option value="IL">IL</option>
    <option value="IN">IN</option>
    <option value="IA">IA</option>
    <option value="KS">KS</option>
    <option value="KY">KY</option>
    <option value="LA">LA</option>
    <option value="ME">ME</option>
    <option value="MD">MD</option>
    <option value="MA">MA</option>
    <option value="MI">MI</option>
    <option value="MN">MN</option>
    <option value="MS">MS</option>
    <option value="MO">MO</option>
    <option value="MT">MT</option>
    <option value="NE">NE</option>
    <option value="NV">NV</option>
    <option value="NH">NH</option>
    <option value="NJ">NJ</option>
    <option value="NM">NM</option>
    <option value="NY">NY</option>
    <option value="NC">NC</option>
    <option value="ND">ND</option>
    <option value="OH">OH</option>
    <option value="OK">OK</option>
    <option value="OR">OR</option>
    <option value="PA">PA</option>
    <option value="RI">RI</option>
    <option value="SC">SC</option>
    <option value="SD">SD</option>
    <option value="TN">TN</option>
    <option value="TX">TX</option>
    <option value="VI">VI</option>
    <option value="UT">UT</option>
    <option value="VT">VT</option>
    <option value="VA">VA</option>
    <option value="WA">WA</option>
    <option value="WV">WV</option>
    <option value="WI">WI</option>
    <option value="WY">WY</option>
    </select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Email:</span></td>
          <td width="300"><input type="text" name="email" class="quote_box" value="<?echo $comp_info["contact_email"];?>" size="40"></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Add New Rule" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<?
  if($hasrules==TRUE)
  {
  ?>
<p><font face="verdana" color="#2360A5" size="3"><b> Current Rules</b></font></p>
    <span class="notice">
      <table cellSpacing="2" cellPadding="2" width="500" border="0">
        <tr>
          <td width="200"><span class="heading">campaign</span></td>
          <td width="50"><span class="heading">origin</span></td>
          <td width="50"><span class="heading">destination</span></td>
          <td width="150"><span class="heading">email</span></td>
          <td width="50"><span class="heading">options</span></td>
        </tr>
        <?
        foreach($rules as $myrule)
        {
        ?>
        <tr>
          <td width="200"><span class="realsmall"><?echo $myrule["name"];?></span></td>
          <td width="50"><span class="text_2"><?echo $myrule["origin"];?></span></td>
          <td width="50"><span class="text_2"><?echo $myrule["destination"];?></span></td>
          <td width="150"><span class="realsmall"><?echo $myrule["email"];?></span></td>
          <td width="50"><a href="control.php?action=1md_companies_leads&action2=delete_rule&id=<?echo $id;?>&rule_id=<?echo $myrule["id"];?>"><img src="images/bdelete.gif" border="0"></a></td>
        </tr>
        <?
        }
        ?>
      </table>
    </span>
<p></p>
  <?
  }
}
?>
<script language="javascript">
focus();
</script>
</body>
</html>