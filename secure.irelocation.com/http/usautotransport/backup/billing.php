<?
include_once("inc_config.php");
include_once("inc_mysql.php");
include_once("inc_phpmailer.php");
include_once("inc_pdf.php");


if((!$comp_id) || (!$sid))
{
  header("location: http://www.usautotransport.com/account.php");
  exit;
}

if($sid!=md5($comp_id.date("Ymd").$comp_id))
{
  die("Invalid sid - your information has been sent to the administrator.");
}


//******************************************************
//  Start the mailing class
//******************************************************
$mail=new phpmailer();
$mail->From = "admin@usautotransport.com";
$mail->FromName = "US Auto Transport";
$mail->Host = "localhost";
$mail->Mailer = "sendmail";


//get current balance
$rs=new mysql_recordset("select b.amount,b.billing_date from billing_inv as b,companies as c where c.id=b.comp_id and c.active!='false' and comp_id='$comp_id' and status!='received'");
if($rs->rowcount()>0)
{
  $rs->fetch_array();
  $balance=$rs->myarray["amount"];
  list($year,$month,$day)=explode("-",$rs->myarray["billing_date"]);
  $due_date=intval((mktime(0,0,0,$month,$day+15,$year)-time())/86400) . " days";
}
else
{
  $rs=new mysql_recordset("select * from companies where id='$comp_id'");
  $rs->fetch_array();
  if($rs->myarray["active"]=='false')
  {  
    $next_invoice_date=date("m/d/Y"); 
    $next_due_date=date("m/d/Y"); 
    $balance=$current_monthly_rate;
    
    $rs=new mysql_recordset("delete from billing_inv where comp_id='$comp_id'");
    $rs=new mysql_recordset("insert into billing_inv (comp_id,billing_date,type,amount,status,term) values ('$comp_id','".date("Y-m-d")."','invoice','$balance','new','monthly')");
    $rs=new mysql_recordset("update billing set next_invoice_date='".date("Y-m-d",mktime(0,0,0,date("m")+1,date("d"),date("Y")))."' where comp_id='".$rs->myarray["comp_id"]."'");
  }
  else
  {
    //get next billing date
    $rs=new mysql_recordset("select next_invoice_date from billing where comp_id='$comp_id'");
    $rs->fetch_array();  
    list($year,$month,$day)=explode("-",$rs->myarray["next_invoice_date"]);  
    $next_invoice_date=date("m/d/Y",mktime(0,0,0,$month,$day,$year));
    $next_due_date=date("m/d/Y",mktime(0,0,0,$month,$day+15,$year));    
    $balance="0";		
  }
}

//get current due date


if($action=='billing_update')
{
  $rs=new mysql_recordset("select decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='$comp_id'");
  if($rs->rowcount()>0)
  {
    $rs->fetch_array();
    $info=unserialize($rs->myarray["myinfo"]);
  } 
  
  $info["type"]=$payment_type;
  
  if($payment_type=='cc')
  {    
    $term=$cc_term;
    $info["term"]=$cc_term;
    $info["cc_fname"]=$cc_fname;
    $info["cc_lname"]=$cc_lname;
    if(substr($cc_num,0,4)!='****') $info["cc_num"]=$cc_num;
    $info["cc_exp_month"]=$cc_exp_month;
    $info["cc_exp_year"]=$cc_exp_year;  	
  }
  else
  {
    $term=$ck_term;
    $info["term"]=$ck_term;
    $info["ck_type"]=$ck_type;
    $info["ck_name"]=$ck_name;
    $info["ck_bank"]=$ck_bank;
    if(substr($ck_aba,0,4)!='****') $info["ck_aba"]=$ck_aba;
    if(substr($ck_account,0,4)!='****') $info["ck_account"]=$ck_account;     
  }
  
  $myinfo=serialize($info);
  
  if($rs->rowcount()>0)
  {
    $rs=new mysql_recordset("update billing_cc set info=encode('$myinfo','$xxxkey') where comp_id='$comp_id'");
  }
  else
  {
    $rs=new mysql_recordset("insert into billing_cc (comp_id,info) values ('$comp_id',encode('$myinfo','$xxxkey'))");	
  }
  
  $rs=new mysql_recordset("select monthly_rate from billing where comp_id='$comp_id'");
  $rs->fetch_array();
  
  switch($term)
  {
    case "monthly":
      $myamount=$rs->myarray["monthly_rate"];
      break;
    case "quarterly":
      $myamount=$rs->myarray["monthly_rate"]*3;
      break;
    case "yearly":
      $myamount=$rs->myarray["monthly_rate"]*12*0.9;
      break;
  }
  
  $rs=new mysql_recordset("update billing_inv set term='$term',amount='$myamount' where comp_id='$comp_id' and status!='received'");


  //if balance>0 then charge them now
  if($balance>0)
  {
    $fp = fopen("email_receipt.htm", "r");
    $body_receipt=fread($fp,filesize("email_receipt.htm"));
    fclose($fp);

    $fp = fopen("email_receipt.txt", "r");
    $text_body_receipt=fread($fp,filesize("email_receipt.txt"));
    fclose($fp);
    
    $rs=new mysql_recordset("select b.*,c.comp_name from billing_inv as b,companies as c where c.id=b.comp_id and comp_id='$comp_id' and status!='received'");
    $rs->fetch_array();
    
    $data="x_Version=3.1&x_Delim_Data=True&x_Method=CC&x_Login=relocate480&x_tran_key=4NKH9P7em3yz6p4N";
    $data.="&x_Amount=".$rs->myarray["amount"]."&x_Description=".$rs->myarray["term"]." US Auto Transport Membership&x_Type=AUTH_CAPTURE&x_Cust_ID=$comp_id";
		
    if($info["type"]=='cc')
    {
      $data.="&x_Method=CC&x_Card_Num=".$info["cc_num"]."&x_Exp_Date=".$info["cc_exp_month"]."/".$info["cc_exp_year"]."&x_First_Name=".$info["cc_fname"]."&x_Last_Name=".$info["cc_lname"]."";
      $payment_info="Card Holder Name: ".$info["cc_fname"]." ".$info["cc_lname"]."<br>\nCredit Card Number: xxxxxxxxxxxx".substr($info["cc_num"],-4)."<br>\nExpiration Date: ".$info["cc_exp_month"]."/".$info["cc_exp_year"]."\n";
      $payment_info_txt="Card Holder Name: ".$info["cc_fname"]." ".$info["cc_lname"]."\nCredit Card Number: xxxxxxxxxxxx".substr($info["cc_num"],-4)."\nExpiration Date: ".$info["cc_exp_month"]."/".$info["cc_exp_year"]."\n";
    }
    else
    {
      $data.="&x_Method=ECHECK&x_Bank_ABA_Code=".$info["ck_aba"]."&x_Bank_Acct_Num=".$info["ck_account"]."&x_Bank_Acct_Type=".$info["ck_type"]."&x_Bank_Name=".$info["ck_bank"]."&x_Bank_Account_Name=".$info["ck_name"]."";
      $payment_info="Name on Account: ".$info["ck_name"]."<br>\nBank Name: ".$info["ck_bank"]."<br>\nAccount Type: ".$info["ck_type"]."<br>\nRouting (ABA)#: xxxxx".substr($info["ck_aba"],-4)."<br>\nAccount #: xxxxx".substr($info["ck_account"],-4)."\n";
      $payment_info_txt="Name on Account: ".$info["ck_name"]."\nBank Name: ".$info["ck_bank"]."\nAccount Type: ".$info["ck_type"]."\nRouting (ABA)#: xxxxx".substr($info["ck_aba"],-4)."\nAccount #: xxxxx".substr($info["ck_account"],-4)."\n";    			
    } 	
    
    // TEST
    //$data.="&x_Test_Request=TRUE";
    
    //initialize curl and process transaction
    $url="https://secure.authorize.net/gateway/transact.dll";
    $ch=curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
    curl_setopt($ch, CURLOPT_FAILONERROR, 1); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
    curl_setopt($ch, CURLOPT_HEADER,0);
    curl_setopt($ch, CURLOPT_POST,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
    $buf = curl_exec($ch);
    $trans=explode(",",$buf);
    curl_close($ch);	

    if($trans[0]=='1')
    {
      //enter transaction in table
      $today=date("Y-m-d");
      $rs2=new mysql_recordset("update billing set last_payment_amount='".$rs->myarray["amount"]."',last_payment_date='$today' where comp_id='".$rs->myarray["comp_id"]."'");
      $rs2=new mysql_recordset("update billing_inv set type='auto-charge',payment='".$rs->myarray["amount"]."',trans_id='".$trans[6]."',status='received' where inv_id='".$rs->myarray["inv_id"]."'");  			
      $rs2=new mysql_recordset("update companies set active='true' where id='$comp_id'");  			
  			       
      //send out receipts  			
      $myreceipt=str_replace("[payment_info]",$payment_info,str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),str_replace("[trans_id]",$trans[6],str_replace("[comp_id]",$rs->myarray["comp_id"],str_replace("[company_name]",$rs->myarray["comp_name"],$body_receipt)))))));
      $myreceipt_text=str_replace("[payment_info]",$payment_info_txt,str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),str_replace("[trans_id]",$trans[6],str_replace("[comp_id]",$rs->myarray["comp_id"],str_replace("[company_name]",$rs->myarray["comp_name"],$text_body_receipt)))))));
  			
      $mail->Body = $myreceipt;
      $mail->AltBody = $myreceipt_text;      
      //$mail->AddAddress("jon@irelocation.com");
      $mail->AddAddress($rs->myarray["email"]);
      $mail->Subject = "US Auto Transport Receipt";
      $mail->Send();
	
      //reset mailer
      $mail->Priority = 3;
      $mail->ClearAddresses();
      
      //transaction successful
      $success=true;
      $msg="Transaction successful!";
      include("billing_step3.php");
      exit;
    }
    else
    {
      //transaction declined
      $success=false;
      $msg="Transaction failed!";
   
      $rs=new mysql_recordset("select monthly_rate from billing where comp_id='$comp_id'");
      $rs->fetch_array();
      $monthly_rate=intval($rs->myarray["monthly_rate"]);
 
      $rs=new mysql_recordset("select decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='$comp_id'");
      if($rs->rowcount()>0)
      {
        $rs->fetch_array();
        $info=unserialize($rs->myarray["myinfo"]);
      }
          include("billing_step2_$payment_type.php");
          exit;
    }  
  }
  else
  {
    include("billing_step3.php");
    exit;	
  }
}
elseif($action=='billing_info')
{
  $rs=new mysql_recordset("select monthly_rate from billing where comp_id='$comp_id'");
  $rs->fetch_array();
  $monthly_rate=intval($rs->myarray["monthly_rate"]);

  $rs=new mysql_recordset("select decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='$comp_id'");
  if($rs->rowcount()>0)
  {
    $rs->fetch_array();
    $info=unserialize($rs->myarray["myinfo"]);
  }

  if($payment_type=='cc')
  {
    include("billing_step2_cc.php");

  }
  elseif($payment_type=='ck')
  {
    include("billing_step2_ck.php");

  }
  else
  {
    die("Invalid payment type - your information has been sent to the administrator.");
  }
}
else
{
  $rs=new mysql_recordset("select decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='$comp_id'");
  if($rs->rowcount()>0)
  {
    $rs->fetch_array();
    $info=unserialize($rs->myarray["myinfo"]);
  }
  include("billing_step1.php");
}
?>