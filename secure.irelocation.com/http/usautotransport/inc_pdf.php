<?
/******************************************************************
#  PHP PDF Class for libpdf+pdi from www.libpdf.com
#
#  Author: Jon Olawski
#  Phoenix68 Technology
#
#  12/03/03 - Added standard fuctions for PDI and text entry.
#           - Added support for inches or pixels, but you have
#             to use inches for anything less than 50 pixels.
******************************************************************/


class pdf
{
  var $p;
  var $form;
  var $page;
  var $font;
  var $pdf_open;
  var $pdi_open;
  var $fname;
  var $spath;
  var $save;

  function pdf($width,$height,$author='',$title='',$subject='',$filename='')
  {
    dl('libpdf_php_68.so');

    if(substr(strtolower(trim($filename)),-4)=='.pdf')
    {
      $this->fname=$filename;
      $this->save=true;
    }

    $this->p=PDF_new();
    if(PDF_open_file($this->p, $this->fname) == 0)
    {
      die("Error: " . PDF_get_errmsg($this->p));
    }

    PDF_set_info($this->p, "Author", $author);
    PDF_set_info($this->p, "Title", $title);
    PDF_set_info($this->p, "Subject", $subject);
    PDF_set_parameter($this->p, "topdown", "true");
    if($width<50) $width=$width*72;
    if($height<50) $height=$height*72;
    PDF_begin_page($this->p, $width, $height);
    $this->pdf_open=true;
  }

  function openpdf($filename,$height,$searchpath='./',$page_num='1')
  {
    PDF_set_parameter($this->p, "SearchPath", $searchpath);
    // open the actual pdf document
    $this->form = PDF_open_pdi($this->p, $filename, "", 0);
    if($this->form == 0)
    {
      die("Error: " . PDF_get_errmsg($this->p));
    }
    // open the page in the document
    $this->page = PDF_open_pdi_page($this->p, $this->form, $page_num, "");
    if($this->page == 0)
    {
      die("Error: " . PDF_get_errmsg($this->p));
    }
    if($height<50) $height=$height*72;
    PDF_fit_pdi_page($this->p, $this->page, 0, $height, "");
    PDF_close_pdi_page($this->p, $this->page);
    $this->pdi_open=true;
  }

  function addtext($text,$x,$y)
  {
    if($x<50) $x=$x*72;
    if($y<50) $y=$y*72;
    PDF_show_xy($this->p,$text,$x,$y);
  }

  function setfont($size='12',$face='Helvetica',$hexcolor='000000')
  {
    $this->font=PDF_findfont($this->p,$face,"winansi",0);
    PDF_setfont($this->p,$this->font,$size) or die("Missing Font!");
    $r = hexdec(substr($hexcolor,0,2))/255;
    $g = hexdec(substr($hexcolor,2,2))/255;
    $b = hexdec(substr($hexcolor,4,2))/255;
    PDF_setcolor($this->p,'both','rgb',$r,$g,$b);
  }

  function display_pdf()
  {
    if($this->pdi_open==true)
    {
      PDF_close_pdi($this->p, $this->form);
      $this->pdi_open==false;
    }
    PDF_end_page($this->p);
    PDF_close($this->p);

    if($this->save==true)
    {
      $filesize=filesize($this->fname);
      header("Content-type: application/pdf");
      header("Content-Length: $filesize");
      header("Content-Disposition: inline; filename=".$this->fname);
      readfile($this->fname);
    }
    else
    {
      $buffer=PDF_get_buffer($this->p);
      $len = strlen($buffer);
      header("Content-type: application/pdf");
      header("Content-Length: $len");
      header("Content-Disposition: inline; filename=".$this->fname);
      print $buffer;
    }

    PDF_delete($this->p);
    $this->pdf_open=false;
  }

  function save_pdf()
  {
    if($this->pdi_open==true)
    {
      PDF_close_pdi($this->p, $this->form);
      $this->pdi_open==false;
    }
    PDF_end_page($this->p);
    PDF_close($this->p);
    PDF_delete($this->p);
    $this->pdf_open=false;
  }
}
?>