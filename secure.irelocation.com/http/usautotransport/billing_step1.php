<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>US Auto Transport Billing</title>
</head>

<body>
<form action="billing.php">
<div align="center">
        <table border="0" width="640" cellspacing="0" cellpadding="0">
                <tr>
                        <td>
                        <img border="0" src="images/logo.gif" width="435" height="63"><p class="pagetitle">
                        Current Balance: $<?echo $balance;?><br>
                        <?
                        if($due_date)
                        {
                          echo "Payment Due: $due_date";	
                        }
                        else
                        {
                          echo "Next Invoice: $next_invoice_date<br>";
                          echo "Next Payment Due: $next_due_date";
                        }?></p>
                        <p class="pagetext">
                        US Auto Transport is proud to offer our convenient online billing
                        payment service.&nbsp; To set up your online payments, please review
                        the following authorization agreement.</p>
                        <p class="pagetext">
                        <i>I/We authorize US Auto Transport to charge my credit card or to
                        draw by electronic funds transfer from the checking or savings
                        account named on the following enrollment form, at the financial
                        institution named on the following enrollment form.</i></p>
                        <p class="pagetext">
                        Choose your payment method and click &quot;Continue&quot; to accept the terms.</p>
                        <p class="pagetext">
                        <input type="radio" value="cc" checked name="payment_type"><?if($info["type"]=='cc'){?>Update<?}else{?>Pay with<?}?> your Visa/Mastercard<br>
                        <input type="radio" value="ck" name="payment_type"><?if($info["type"]=='ck'){?>Update your<?}else{?>Pay with<?}?> Electronic Check</p>
                        <p class="pagetext">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <input border="0" src="images/continue.gif" name="submit" width="77" height="23" type="image"></p>
                        <p>&nbsp;</td>
                </tr>
        </table>
</div>
<input type="hidden" name="comp_id" value="<?echo $comp_id;?>">
<input type="hidden" name="sid" value="<?echo $sid;?>">
<input type="hidden" name="action" value="billing_info">
</form>
</body>

</html>