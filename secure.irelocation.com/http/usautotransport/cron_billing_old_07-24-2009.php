<?
//****************************************************************
//  Script must be on SSL server because of payment gateway.
//****************************************************************


exit;


//$test=true;

include_once("inc_config.php");
include_once("inc_mysql.php");
include_once("inc_phpmailer.php");
include_once("inc_pdf.php");

$today=date("Y-m-d");
$next_invoice_date=date("Y-m-d",mktime(0,0,0,date("m")+1,date("d"),date("Y")));
$past_invoice_date=date("Y-m-d",mktime(0,0,0,date("m"),date("d")-15,date("Y")));

//******************************************************
//  Start the mailing class
//******************************************************
$mail=new phpmailer();
$mail->From = "admin@usautotransport.com";
$mail->FromName = "US Auto Transport";
$mail->Host = "localhost";
$mail->Mailer = "sendmail";


//******************************************************
//  Deactivate people who have not paid in 15 days
//******************************************************
$fp = fopen("email_deactivated.htm", "r");
$body=fread($fp,filesize("email_deactivated.htm"));
$mail->Body = $body;
fclose($fp);

$fp = fopen("email_deactivated.txt", "r");
$text_body=fread($fp,filesize("email_deactivated.txt"));
$mail->AltBody = $text_body;
fclose($fp);

$rs=new mysql_recordset("select b.*,u.email from billing_inv as b,users as u, companies as c where c.id=b.comp_id and b.comp_id=u.comp_id and c.active='true' and b.billing_date<'$past_invoice_date' and u.level='admin' and b.payment='0.00'");
while($rs->fetch_array())
{
        //deactive account and delete the current billing invoice
        if(isset($test)==false)
        {
          $rs2=new mysql_recordset("update companies set active='false' where id='".$rs->myarray["comp_id"]."'");
          $rs2=new mysql_recordset("delete from billing_inv where inv_id='".$rs->myarray["inv_id"]."'");
        }
        else
        {
          $test_str.="update companies set active='false' where id='".$rs->myarray["comp_id"]."'\n";	
          $test_str.="delete from billing_inv where inv_id='".$rs->myarray["inv_id"]."'\n";	
        }

        //email out a letter explaining that the service has been terminated.
        if(isset($test)==false)
        {
          $mail->AddAddress($rs->myarray["email"]);
        }
        else
        {
          $mail->AddAddress("jon@irelocation.com");
        }
        echo "Account Deactivated Email sent to ".$rs->myarray["email"]."<br>\n";
        $mail->Subject = "Account Deactivated";
        $mail->Send();
        $mail->ClearAddresses();
}

//******************************************************
//  Check billing and find companies who owe us money!
//******************************************************
$rs=new mysql_recordset("select b.* from billing as b,companies as c where c.id=b.comp_id and b.next_invoice_date<='$today' and c.active!='false' and b.monthly_rate>0 and c.type!='broker' limit 40");
while($rs->fetch_array())
{
        $rs2=new mysql_recordset("select comp_id,decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='".$rs->myarray["comp_id"]."'");
        if($rs2->rowcount()>0)
        {
                $rs2->fetch_array();
                $info=unserialize($rs2->myarray["myinfo"]);
                switch(strtolower(trim($info[term])))
                {
                        case "monthly":
                                $therate=intval($rs->myarray["monthly_rate"])*1;
                                $ndate=date("Y-m-d",mktime(0,0,0,date("m")+1,date("d"),date("Y")));
                                break;
                        case "quarterly":
                                $therate=intval($rs->myarray["monthly_rate"])*3;
                                $ndate=date("Y-m-d",mktime(0,0,0,date("m")+3,date("d"),date("Y")));
                                break;
                        case "yearly":
                                $therate=intval($rs->myarray["monthly_rate"])*12*0.9;
                                $ndate=date("Y-m-d",mktime(0,0,0,date("m"),date("d"),date("Y")+1));                                
                                break;
                }
                if(isset($test)==false)
                {
                  $rs3=new mysql_recordset("insert into billing_inv (comp_id,billing_date,type,amount,status) values ('".$rs->myarray["comp_id"]."','$today','auto-charge','".$therate."','new')");
                }
                else
                {
                  $test_str.="insert into billing_inv (comp_id,billing_date,type,amount,status) values ('".$rs->myarray["comp_id"]."','$today','auto-charge','".$therate."','new')\n";	
                }

        }
        else
        {
        	$ndate=$next_invoice_date;
        	if(isset($test)==false)
                {
                  $rs3=new mysql_recordset("insert into billing_inv (comp_id,billing_date,type,amount,status) values ('".$rs->myarray["comp_id"]."','$today','invoice','".$rs->myarray["monthly_rate"]."','new')");
                }
                else
                {
                  $test_str.="insert into billing_inv (comp_id,billing_date,type,amount,status) values ('".$rs->myarray["comp_id"]."','$today','invoice','".$rs->myarray["monthly_rate"]."','new')\n";	
                }                
        }
        
        if(isset($test)==false)
        {
          $rs3=new mysql_recordset("update billing set last_invoice_date='$today',next_invoice_date='$ndate' where comp_id='".$rs->myarray["comp_id"]."'");
        }
        else
        {
          $test_str.="update billing set last_invoice_date='$today',next_invoice_date='$ndate' where comp_id='".$rs->myarray["comp_id"]."'\n";	
        }
}


//******************************************************
//  Charge customers or send out invoices.
//******************************************************
$fp = fopen($DOCUMENT_ROOT."/email_invoice.htm", "r");
$body_invoice=fread($fp,filesize("email_invoice.htm"));
fclose($fp);

$fp = fopen($DOCUMENT_ROOT."/email_receipt.htm", "r");
$body_receipt=fread($fp,filesize("email_receipt.htm"));
fclose($fp);

$fp = fopen($DOCUMENT_ROOT."/email_failed.htm", "r");
$body_failed=fread($fp,filesize("email_failed.htm"));
fclose($fp);

$fp = fopen($DOCUMENT_ROOT."/email_invoice.txt", "r");
$text_body_invoice=fread($fp,filesize("email_invoice.txt"));
fclose($fp);

$fp = fopen($DOCUMENT_ROOT."/email_receipt.txt", "r");
$text_body_receipt=fread($fp,filesize("email_receipt.txt"));
fclose($fp);

$fp = fopen($DOCUMENT_ROOT."/email_failed.txt", "r");
$text_body_failed=fread($fp,filesize("email_failed.txt"));
fclose($fp);


$rs=new mysql_recordset("select b.*,b.type as mytype,u.*,c.* from billing_inv as b,users as u, companies as c where b.comp_id=u.comp_id and b.comp_id=c.id and b.status='new' and u.level='admin'");
while($rs->fetch_array())
{
        if($rs->myarray["mytype"]=='auto-charge')
        {
        	$rs2=new mysql_recordset("select decode(info,'$xxxkey') as myinfo from billing_cc where comp_id='".$rs->myarray["comp_id"]."'");
		$rs2->fetch_array();
		$info=unserialize($rs2->myarray["myinfo"]);
		
		$data="x_Version=3.1&x_Delim_Data=True&x_Method=CC&x_Login=relocate480&x_Password=ALMGCz5k";
		$data.="&x_Amount=".$rs->myarray["amount"]."&x_Description=".$rs->myarray["term"]."US Auto Transport Membership&x_Type=AUTH_CAPTURE&x_Cust_ID=".$rs->myarray["comp_id"]."";
		
		if($info["type"]=='cc')
  		{
    			$data.="&x_Method=CC&x_Card_Num=".$info["cc_num"]."&x_Exp_Date=".$info["cc_exp_month"]."/".$info["cc_exp_year"]."&x_First_Name=".$info["cc_fname"]."&x_Last_Name=".$info["cc_lname"]."";
    			$payment_info="Card Holder Name: ".$info["cc_fname"]." ".$info["cc_lname"]."<br>\nCredit Card Number: xxxxxxxxxxxx".substr($info["cc_num"],-4)."<br>\nExpiration Date: ".$info["cc_exp_month"]."/".$info["cc_exp_year"]."\n";
    			$payment_info_txt="Card Holder Name: ".$info["cc_fname"]." ".$info["cc_lname"]."\nCredit Card Number: xxxxxxxxxxxx".substr($info["cc_num"],-4)."\nExpiration Date: ".$info["cc_exp_month"]."/".$info["cc_exp_year"]."\n";
  		}
  		else
  		{
	    		$data.="&x_Method=ECHECK&x_Bank_ABA_Code=".$info["ck_aba"]."&x_Bank_Acct_Num=".$info["ck_account"]."&x_Bank_Acct_Type=".$info["ck_type"]."&x_Bank_Name=".$info["ck_bank"]."&x_Bank_Account_Name=".$info["ck_name"]."";
	    		$payment_info="Name on Account: ".$info["ck_name"]."<br>\nBank Name: ".$info["ck_bank"]."<br>\nAccount Type: ".$info["ck_type"]."<br>\nRouting (ABA)#: xxxxx".substr($info["ck_aba"],-4)."<br>\nAccount #: xxxxx".substr($info["ck_account"],-4)."\n";
	    		$payment_info_txt="Name on Account: ".$info["ck_name"]."\nBank Name: ".$info["ck_bank"]."\nAccount Type: ".$info["ck_type"]."\nRouting (ABA)#: xxxxx".substr($info["ck_aba"],-4)."\nAccount #: xxxxx".substr($info["ck_account"],-4)."\n";    			
  		}
  		
  		//initialize curl and process transaction
  		$url="https://secure.authorize.net/gateway/transact.dll";
  		$ch=curl_init();
  		curl_setopt($ch, CURLOPT_URL,$url);
  		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
  		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
  		curl_setopt($ch, CURLOPT_FAILONERROR, 1); 
  		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
  		curl_setopt($ch, CURLOPT_HEADER,0);
  		curl_setopt($ch, CURLOPT_POST,1);
  		curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
  		if(isset($test)==false)
                {
  		  $buf = curl_exec($ch);  	        
  		  $trans=explode(",",$buf);
  		  curl_close($ch);
  		}
  		else
  		{
  		  $test_str.="https://secure.authorize.net/gateway/transact.dll?$data\n";	
  		  $trans[0]=1;
  		}

  		
  		if($trans[0]=='1')
  		{
                        $rs_temp=new mysql_recordset("select last_invoice_date from billing where comp_id='".$rs->myarray["b.comp_id"]."'");
                        $rs_temp->fetch_array();                        
                        list($inv_year,$inv_month,$inv_day)=explode('-',$rs_temp->myarray["last_invoice_date"]);
                        switch($rs->myarray["term"])
                        {
                          case "yearly":
                            $next_invoice_date=date("Y-m-d",mktime(0,0,0,$inv_month,$inv_day,$inv_year+1));
                            break;
                          case "quarterly":
                            $next_invoice_date=date("Y-m-d",mktime(0,0,0,$inv_month+3,$inv_day,$inv_year));
                            break;
                          default:
                            $next_invoice_date=date("Y-m-d",mktime(0,0,0,$inv_month+1,$inv_day,$inv_year+1));
                            break;
                        }
  			//enter transaction in table
  			if(isset($test)==false)
                        {
  			  $rs2=new mysql_recordset("update billing set last_payment_amount='".$rs->myarray["amount"]."',last_payment_date='".date("Y-m-d")."' where comp_id='".$rs->myarray["comp_id"]."'");
  			  $rs2=new mysql_recordset("update billing_inv set payment='".$rs->myarray["amount"]."',trans_id='".$trans[6]."',status='received' where inv_id='".$rs->myarray["inv_id"]."'");  			
  			}
  			else
  			{
  			  $test_str.="update billing set last_payment_amount='".$rs->myarray["amount"]."',last_payment_date='".date("Y-m-d")."' where comp_id='".$rs->myarray["comp_id"]."'\n";	
  			  $test_str.="update billing_inv set payment='".$rs->myarray["amount"]."',trans_id='".$trans[6]."',status='received' where inv_id='".$rs->myarray["inv_id"]."'";	
  			}
  			
  			//send out receipts  			
  			$myreceipt=str_replace("[payment_info]",$payment_info,str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),str_replace("[trans_id]",$trans[6],str_replace("[comp_id]",$rs->myarray["id"],str_replace("[company_name]",$rs->myarray["comp_name"],$body_receipt)))))));
  			$myreceipt_text=str_replace("[payment_info]",$payment_info_txt,str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),str_replace("[trans_id]",$trans[6],str_replace("[comp_id]",$rs->myarray["id"],str_replace("[company_name]",$rs->myarray["comp_name"],$text_body_receipt)))))));
  			
  			$mail->Body = $myreceipt;
               		$mail->AltBody = $myreceipt_text;
               		if(isset($test)==false)
                        {
               		  $mail->AddAddress($rs->myarray["email"]);
               		}
               		else
               		{
               		  $mail->AddAddress("jon@irelocation.com");	
               		}
               		
               		echo "Receipt sent to ".$rs->myarray["email"]."<br>\n";
               		$mail->Subject = "US Auto Transport Receipt";               		
               		$mail->Send();
	
               		//reset mailer
               		$mail->Priority = 3;
               		$mail->ClearAddresses();
               		
  		}
  		else
  		{
  			$rs3=new mysql_recordset("select monthly_rate from billing where comp_id='".$rs->myarray["id"]."'");
                        $rs3->fetch_array();
                        $monthly_rate=intval($rs3->myarray["monthly_rate"]);
  			
  			//set inv to monthly and reset rate
  			if(isset($test)==false)
                        {
  			  $rs2=new mysql_recordset("update billing_inv set type='invoice',term='monthly',amount='$monthly_rate' where inv_id='".$rs->myarray["inv_id"]."'");  
  			}
  			else
  			{
  			  $test_str.="update billing_inv set type='invoice',term='monthly',amount='$monthly_rate' where inv_id='".$rs->myarray["inv_id"]."'\n";
  			}
  			
  			//send out failed notice
  			$myfailed=str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),$body_failed)));
  			$myfailed_text=str_replace("[term]",$rs->myarray["term"],str_replace("[amount]",$rs->myarray["amount"],str_replace("[date]",date("F j, Y"),$text_body_failed)));
  			
  			$mail->Body = $myfailed;
               		$mail->AltBody = $myfailed_text;
               		if(isset($test)==false)
                        {
               		  $mail->AddAddress($rs->myarray["email"]);
               		}
               		else
               		{
               		  $mail->AddAddress("jon@irelocation.com");	
               		}
               		echo "Transaction Failed sent to ".$rs->myarray["email"]."<br>\n";
               		$mail->Subject = "Transaction Failed!";               		
               		$mail->Send();
	
               		//reset mailer
               		$mail->Priority = 3;
               		$mail->ClearAddresses();		
  		}		

        }
        elseif($rs->myarray["mytype"]=='invoice')
        {               
                //create the pdf invoice
                $filename="/home/httpd/vhosts/secure.irelocation.com/httpsdocs/usautotransport/pdf/".$rs->myarray["comp_id"]."-".$rs->myarray["inv_id"].".pdf";

                $p=new pdf("8.5","11","PDF Generator","Invoice ".$rs->myarray["inv_id"]."","Invoice ".date("m/d/Y")."",$filename);
                $p->openpdf("/home/httpd/vhosts/secure.irelocation.com/httpsdocs/usautotransport/pdf/blank_invoice.pdf","11");

                $p->setfont("10","Helvetica","000000");
                $p->addtext("attn: ".ucwords(strtolower($rs->myarray["name"])),50,185);
                $p->addtext($rs->myarray["comp_name"],50,197);
                $p->addtext($rs->myarray["address"],50,209);
                $p->addtext($rs->myarray["city"].", ".$rs->myarray["state"]." ".$rs->myarray["zip"],50,221);
                $p->addtext($rs->myarray["inv_id"],475,93);
                $p->addtext(date("m/d/Y"),475,106);
                $p->setfont("9","Helvetica","FF0000");

                $rs2=new mysql_recordset("select * from billing_inv where comp_id='".$rs->myarray["comp_id"]."' and status='received' order by billing_date desc");
                if($rs2->rowcount()>0)
                {
                        $p->addtext($rs2->myarray["payment"],515,330);
                        $p->setfont("9","Helvetica","000000");
                        $p->addtext($rs2->myarray["amount"],515,330);
                }
                else
                {
                        $p->addtext("0.00",515,330);
                        $p->setfont("9","Helvetica","000000");
                        $p->addtext("0.00",515,330);
                }
                $p->addtext($rs->myarray["amount"],515,342);
                $p->addtext($rs->myarray["amount"],515,355);
                $p->addtext(date("m/d/Y")."   ".$rs->myarray["term"]." US Auto Transport Membership",90,430);
                $p->addtext($rs->myarray["amount"],515,430);  // total
                $p->addtext($rs->myarray["amount"],515,703);  // sub total
                $p->setfont("12","Helvetica","000000");
                $p->addtext($rs->myarray["amount"],515,730);  // grand total  (sub total plus previous balance)
                $p->addtext($rs->myarray["amount"],475,134);  // grand total
                $p->save_pdf();

                //email the invoice
                $mail->Body = $body_invoice;
                $mail->AltBody = $text_body_invoice;
                if(isset($test)==false)
                {
               	  $mail->AddAddress($rs->myarray["email"]);
               	}
               	else
               	{
               	  $mail->AddAddress("jon@irelocation.com");	
               	}                
                echo "Invoice sent to ".$rs->myarray["email"]."<br>\n";
                $mail->AddAttachment($filename);
                $mail->Subject = "US Auto Transport Invoice";
                $mail->Priority = 1;
                $mail->Send();

                //reset mailer
                $mail->Priority = 3;
                $mail->ClearAddresses();
                $mail->ClearAttachments();
                
                if(isset($test)==false)
                {
                  $rs2=new mysql_recordset("update billing_inv set status='sent' where inv_id='".$rs->myarray["inv_id"]."'");  
                }
                else
                {
                  $test_str.="update billing_inv set status='sent' where inv_id='".$rs->myarray["inv_id"]."'\n";  	
                }
        }
        
}
if(isset($test)==true)
{
  mail("jon@irelocation.com","CRON BILLING TEST RESULTS","$test_str");	
}
?>