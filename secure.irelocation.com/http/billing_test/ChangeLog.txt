Changelog V1.4.4 - 1.4.5
 

_______________________________________________________________________

	New Features and Enhancements in this release:
_______________________________________________________________________
 
* Zip code and address verification less restrictive by default

* Streamline the admin theme so less space is wasted at the top of the screen

* Separate admin/user .php files that force the correct template and allow 
  simaltanious access without the wrong template loading

* Custom fields now support collecting a date with the date selector popup

* Zip code and address verification should be lest restrictive 

* Jpgraph class supporting php5 now available


_______________________________________________________________________

	Fixed Bugs in this release:
_______________________________________________________________________

* Currency Variables not parsed in the admin paid email template.

* Upon changing default currency, change the currency of all existing sessions.

* Cannot remove all items selected in a multi-selectable array list.

* Plesk PSA 7.5 Reloaded plugin bugs, XML error when debug mode turned on in IE

* Worldpay and Futurepay plugin bugs

* Trustcommerce plugin configuration bugs

* Eway plugin configuration bugs

* Account Cleanup task should not delete accounts with an expiration date defined.
 
* New jpgraph class available for php5 (email if you need it)

* Failing to enter a trailing slash (/) at the end of the URL & SSL URL can break the cart.

* Cookies not set on both www.domain.com and domain.com

* Unparsed alert on delete: The %%module_name%% record(s) you specified have been deleted.

* When exiting the admin area, go to the site default theme, not the actual theme named "default"

* When using 'enter' key to submit a form, calendar pops up

* The popup window for editing the language does not include a scrollbar

* The popup window for account selection is not large enough to show all the accounts

* On postback admin payment received email template: This e-mail is to alert you that %acct_first_name% %acct_last_name% made a payment...

 
_______________________________________________________________________

	Changed files:
_______________________________________________________________________
 
M ChangeLog.txt
M htaccess_index.php
M modules/account/account_construct.xml
M modules/account_admin/account_admin_construct.xml
M modules/checkout/checkout.inc.php
M modules/core/core.inc.php
M modules/core/database_mass_delete.inc.php
M modules/core/database_update.inc.php
M modules/core/list_calendar.inc.php
M modules/core/list_popup.inc.php
M modules/core/session.inc.php
M modules/core/static_var.inc.php
M modules/core/theme.inc.php
M modules/core/xml.inc.php
M modules/email_template/email_template.inc.php
M modules/email_template/email_template_construct.xml
M modules/host_registrar_plugin/host_registrar_plugin_install_data.xml
M modules/host_tld/host_tld_install_data.xml
M modules/invoice/invoice.inc.php
M modules/module/module.inc.php
M modules/product_translate/product_translate_construct.xml
M modules/setup/setup.inc.php
M plugins/checkout/AUTHORIZE_NET.php
M plugins/checkout/BLUEPAY.php
M plugins/checkout/DPILINK.php
M plugins/checkout/ECHO.php
M plugins/checkout/ECX_QUICKCOMMERCE.php
M plugins/checkout/EPROCESSING_NETWORK.php
M plugins/checkout/EWAY.php
M plugins/checkout/FREE.php
M plugins/checkout/GOEMERCHANT.php
M plugins/checkout/IBILL.php
M plugins/checkout/LINKPOINT.php
M plugins/checkout/MANUAL.php
M plugins/checkout/NETBILLING.php
M plugins/checkout/PLANETPAYMENT.php
M plugins/checkout/PLUGNPAY.php
M plugins/checkout/PROTX.php
M plugins/checkout/PSIGATE.php
M plugins/checkout/RTWARE.php
M plugins/checkout/SECPAY.php
M plugins/checkout/SKIPJACK.php
M plugins/checkout/SUREPAY.php
M plugins/checkout/SWREG_ADVANCED.php
M plugins/checkout/TRUSTCOMMERCE.php
M plugins/checkout/VERISIGN_PFPRO.php
M plugins/checkout/WORLDPAY.php
M plugins/checkout/WORLDPAY_FUTUREPAY.php
M plugins/provision/PLESK_RELOADED_7_5.php
M themes/default/left.tpl
M themes/default/blocks/account/add.tpl
M themes/default/blocks/account/view.tpl
M themes/default/blocks/account_admin/add_iframe.tpl
M themes/default/blocks/account_admin/iframe_search_form.tpl
M themes/default/blocks/account_admin/main.tpl
M themes/default/blocks/account_admin/view.tpl
M themes/default/blocks/account_admin/view_iframe.tpl
M themes/default/blocks/affiliate/main.tpl
M themes/default/blocks/affiliate/view.tpl 
M themes/default/blocks/affiliate_commission/view.tpl
M themes/default/blocks/affiliate_template/view.tpl
M themes/default/blocks/charge/view.tpl
M themes/default/blocks/checkout/admin_checkout.tpl
M themes/default/blocks/checkout/checkout.tpl
M themes/default/blocks/checkout_plugin/credit_card.tpl
M themes/default/blocks/checkout_plugin/credit_card_avs.tpl
M themes/default/blocks/checkout_plugin/plugin_cfg_WORLDPAY.tpl
M themes/default/blocks/checkout_plugin/plugin_cfg_WORLDPAY_FUTUREPAY.tpl
M themes/default/blocks/checkout_plugin/plugin_ord_REMIT_CHECK_ALERT.tpl
M themes/default/blocks/checkout_plugin/plugin_ord_SWREG_ADVANCED.tpl
M themes/default/blocks/checkout_plugin/third_party.tpl
M themes/default/blocks/core/admin.tpl
M themes/default/blocks/core/export_search.tpl 
M themes/default/blocks/discount/view.tpl
M themes/default/blocks/email_template/add.tpl 
M themes/default/blocks/email_template/view.tpl
M themes/default/blocks/email_template_translate/add.tpl 
M themes/default/blocks/email_template_translate/view.tpl
M themes/default/blocks/group/add.tpl 
M themes/default/blocks/group/search_show.tpl
M themes/default/blocks/group/view.tpl 
M themes/default/blocks/host_server/search_show.tpl
M themes/default/blocks/host_server/view.tpl
M themes/default/blocks/host_tld/view.tpl
M themes/default/blocks/htaccess/htaccess.tpl
M themes/default/blocks/invoice/add.tpl
M themes/default/blocks/module/dev_add.tpl
M themes/default/blocks/newsletter/view.tpl 
M themes/default/blocks/newsletter_subscriber/view.tpl
M themes/default/blocks/product/add.tpl
M themes/default/blocks/product/admin_details.tpl
M themes/default/blocks/product/iframe_associations.tpl
M themes/default/blocks/product/iframe_discounts.tpl
M themes/default/blocks/product/iframe_hosting.tpl
M themes/default/blocks/product/iframe_plugins.tpl
M themes/default/blocks/product/iframe_price_onetime.tpl
M themes/default/blocks/product/iframe_price_recurring.tpl
M themes/default/blocks/product/view.tpl
M themes/default/blocks/product_translate/view.tpl 
M themes/default/blocks/static_var/view.tpl
M themes/default_admin/style.css
M themes/default_admin/template.tpl
M themes/default_admin/blocks/core/leftFrameBlue.tpl 
 

_______________________________________________________________________

	New Files:
_______________________________________________________________________
 
A Version.txt
A admin.php 
A themes/default_admin/images/logo.gif  
A upgrades/upgrade_1_4_3.php
A upgrades/upgrade_1_4_4.php 