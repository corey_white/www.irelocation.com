<HTML>
<HEAD>
<meta http-equiv="no-cache">
<TITLE>{$smarty.const.SITE_NAME}</TITLE>
 

{literal}
<SCRIPT LANGUAGE="JavaScript">
<!-- START 
	function openUrl(u) {
		var random1=Math.round(Math.random()+4*123);
		var random2=Math.round(Math.random()+ random1 * 321);
 		window.open('admin.php'+u+'&random='+random1+'='+random2,'mainFrame','');
	}
	 
	function exitAdmin() {
		var random1=Math.round(Math.random()+4*123);
		var random2=Math.round(Math.random()+ random1 * 321);
		//window.open('?_page=account:account&tid=default&'+random1+'='+random2,'_parent','');
		document.location='index.php?tid={/literal}{$smarty.const.DEF_THEME_N}{literal}';
	}  
//  END --> 
</SCRIPT>
 
 
<!-- Apycom Tree Menu --> 
<script type="text/javascript" language="JavaScript1.2" src="includes/aptree/apytmenu.js"></script>

</HEAD>
<body bgcolor="#7AA1E6" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center><img src='themes/{/literal}{$THEME_NAME}{literal}/images/logo.gif' align="middle"></center>

<script language="JavaScript1.2">
var tblankImage      = "includes/aptree/img/blank.gif";
var tmenuWidth       = '220';
var tmenuHeight      = '99%';

var tabsolute        = 1;
var tleft            = 15;
var ttop             = 50;
var tsaveState 		 = 0; 

var tfloatable       = 0;
var tfloatIterations = 1;
var tmoveable        = 0;
var tmoveImage       = "includes/aptree/img/movepic.gif";
var tmoveImageHeight = 12;

var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#215DC6","#428EFF"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#D6DFF7","#D6DFF7"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "hand";
var titemHeight      = 22;

var titemTarget      = "_blank";
var ticonWidth       = 16;
var ticonHeight      = 16;
var ticonAlign       = "left";

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;

var texpandBtn       =["includes/aptree/img/expandbtn2.gif","includes/aptree/img/expandbtn2.gif","includes/aptree/img/collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"
var texpandItemClick = 1;

var texpanded 		 = 0;

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 1;                  // expand/collapse speed
var tXPTitleTopBackColor = "";
var tXPTitleBackColor    = "#265BCC";
var tXPTitleLeft    = "includes/aptree/img/xptitleleft.gif";
var tXPExpandBtn    = ["includes/aptree/img/xpexpand1.gif","includes/aptree/img/xpexpand2.gif","includes/aptree/img/xpcollapse1.gif","includes/aptree/img/xpcollapse2.gif"];
var tXPBtnHeight    = 25;
var tXPTitleBackImg = "includes/aptree/img/xptitle.gif";

var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#FFFFFF,#428EFF", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#215DC6,#428EFF", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#D0DAF8", "tXPExpandBtn=includes/aptree/img/xpexpand3.gif,includes/aptree/img/xpexpand4.gif,includes/aptree/img/xpcollapse3.gif,includes/aptree/img/xpcollapse4.gif", "tXPTitleBackImg=includes/aptree/img/xptitle2.gif"]
];

var tmenuItems =
[
    ["+General", "", "includes/aptree/img/xpicon1.gif","","", "Main Page",,"0"],
    ["|Overview", "javascript:openUrl('?_page=core:admin');", "includes/aptree/img/icons/info.gif", "",  "", "","0"],
    ["|Exit Administration", "javascript:exitAdmin();", "includes/aptree/img/icons/info.gif", "",  "", "","0"],
   
    ["+|Agile Links",    "",             "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help2.gif", "Help",,"2"],
    	["||Tasks",  	"javascript:openUrl('?_page=core:search&module=task');", "themes/default_admin/images/icons/apps_16.gif", "",                    "",                   "Glossary"],
    	["||<nobr><input id='account_admin' value='all' size=15 style='font-size:9' onKeyPress='if(checkEnter(event)) { openQuickSearch_account(); }' onfocus='javascript:this.select()'>&nbsp;<input type='button' value='Go' style='font-size:9' onclick='javascript:openQuickSearch_account()'></nobr>","", "themes/default_admin/images/icons/user_16.gif", "", "", "Search Accounts",,"2"],
    	["||<nobr><input id='invoice' 		value='all' size=15 style='font-size:9' onKeyPress='if(checkEnter(event)) { openQuickSearch_invoice(); }' onfocus='javascript:this.select()'>&nbsp;<input type='button' value='Go' style='font-size:9' onclick='javascript:openQuickSearch_invoice()'></nobr>","", "themes/default_admin/images/icons/calc_16.gif", "", "", "Search Invoices",,"2"],
    	["||<nobr><input id='service' 		value='all' size=15 style='font-size:9' onKeyPress='if(checkEnter(event)) { openQuickSearch_service(); }' onfocus='javascript:this.select()'>&nbsp;<input type='button' value='Go' style='font-size:9' onclick='javascript:openQuickSearch_service()'></nobr>","", "themes/default_admin/images/icons/tools_16.gif", "", "", "Search Services",,"2"],
		    
    ["|Misc.",       "",             "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help2.gif", "Help",,"2"],
    	["||Documentation ",  "javascript:window.open('http://agilebill.com/documentation','mainFrame','');", "includes/aptree/img/icons/paper.gif", "", "", ""],
 		["||Agileco News ",  "javascript:window.open('http://agileco.com/?_page=static_page:list&id=2&_escape=1','mainFrame','');", "includes/aptree/img/icons/paper.gif", "", "", ""],
		["||Version Check ",  "javascript:window.open('http://agileco.com/checkversion.php?version={/literal}{$list->version()}{literal}','mainFrame','');", "includes/aptree/img/icons/paper.gif", "", "", ""],
 

{/literal}

{ $list->generate_admin_menu() }

{literal}

 ];

apy_tmenuInit();

	// check for enter key pressed
	function checkEnter(e){  
		var characterCode; 
		if(e && e.which){  
			e = e;
			characterCode = e.which;
		} else {
			e = event;
			characterCode = e.keyCode;
		} 
		if(characterCode == 13){ 
			return true;
		} else {
			return false;
		} 
	}
	
	// function for calling the account quicksearch
	function openQuickSearch_account() {
		var m = 'account_admin';
		var st= "";
		var input = document.getElementById(m).value;		
		if (input.indexOf(" ")== -1) {
			var s = input;
			if(s == '[i]' || s == 'inactive') {
				st += '&account_admin_status=0';				
			} else if(s == '' || s == 'all') {
				st += '';
			} else if(s == '[t]' || s == 'today') {
				st += '&account_admin_date_orig[0]={/literal}{$today_start}{literal}&field_option[account_admin_date_orig][0]=>';
			} else if(s == '[w]' || s == 'week') {
				st += '&account_admin_date_orig[0]={/literal}{$week_start}{literal}&field_option[account_admin_date_orig][0]=>';
			} else if(s == '[m]' || s == 'month') {
				st += '&account_admin_date_orig[0]={/literal}{$month_start}{literal}&field_option[account_admin_date_orig][0]=>';
			} else if(s.match(/fn:/)) {
				var str = s.replace(/fn:/, "");
				st += '&account_admin_first_name='+str;
			} else if(s.match(/ln:/)) {
				var str = s.replace(/ln:/, "");
				st += '&account_admin_last_name='+str;
			} else if(s.match(/co:/)) {
				var str = s.replace(/co:/, "");
				st += '&account_admin_company='+str;
			} else if(s.match(/em:/)) {
				var str = s.replace(/em:/, "");
				st += '&account_admin_email='+str;
			} else {
				st += '&account_admin_username=' +s;
			}	
		} else {
			var array=input.split(" ");
			var num=0;
			while(num < array.length) 
			{
				var s = array[num];
				if(s == '[i]' || s == 'inactive') {
					st += '&account_admin_status=0';				
				} else if(s == '[t]' || s == 'today') {
					st += '&account_admin_date_orig[0]={/literal}{$today_start}{literal}&field_option[account_admin_date_orig][0]=>';
				} else if(s == '[w]' || s == 'week') {
					st += '&account_admin_date_orig[0]={/literal}{$week_start}{literal}&field_option[account_admin_date_orig][0]=>';
				} else if(s == '[m]' || s == 'month') {
					st += '&account_admin_date_orig[0]={/literal}{$month_start}{literal}&field_option[account_admin_date_orig][0]=>';
				} else if(s.match(/fn:/)) {
					var str = s.replace(/fn:/, "");
					st += '&account_admin_first_name='+str;
				} else if(s.match(/ln:/)) {
					var str = s.replace(/ln:/, "");
					st += '&account_admin_last_name='+str;
				} else if(s.match(/co:/)) {
					var str = s.replace(/co:/, "");
					st += '&account_admin_company='+str;
				} else if(s.match(/em:/)) {
					var str = s.replace(/em:/, "");
					st += '&account_admin_email='+str;
				} else {
					st += '&account_admin_username=' +s;
				}	
				num+=1;
			}
		} 	
		var u = '?_page=core:search&module='+m+'&_next_page_one=view&_escape=1'+st;
		openUrl(u);  
	}
	
	// function for calling the invoice quicksearch
	function openQuickSearch_invoice() {
		var m = 'invoice';
		var st= "";
		var input = document.getElementById(m).value;		
		if (input.indexOf(" ")== -1) {
			var s = input;
			if(s == '[d]' || s == 'due') {
				st += '&invoice_billing_status=0';
			} else if(s == '[p]' || s == 'pending') {
				st += '&invoice_billing_status=1&invoice_process_status=0';			
			} else if(s == '' || s == 'all') {
				st += '';
			} else if(s == '[t]' || s == 'today') {
				st += '&invoice_date_orig[0]={/literal}{$today_start}{literal}&field_option[invoice_date_orig][0]=>';
			} else if(s == '[w]' || s == 'week') {
				st += '&invoice_date_orig[0]={/literal}{$week_start}{literal}&field_option[invoice_date_orig][0]=>';
			} else if(s == '[m]' || s == 'month') {
				st += '&invoice_date_orig[0]={/literal}{$month_start}{literal}&field_option[invoice_date_orig][0]=>';
			} else {
				st += '&invoice_id=' +s;
			}	
		} else {
			var array=input.split(" ");
			var num=0;
			while(num < array.length) 
			{
				var s = array[num];
				if(s == '[d]' || s == 'due') {
					st += '&invoice_billing_status=0';
				} else if(s == '[p]' || s == 'pending') {
					st += '&invoice_billing_status=1&invoice_process_status=0';			
				} else if(s == '' || s == 'all') {
					st += '';
				} else if(s == '[t]' || s == 'today') {
					st += '&invoice_date_orig[0]={/literal}{$today_start}{literal}&field_option[invoice_date_orig][0]=>';
				} else if(s == '[w]' || s == 'week') {
					st += '&invoice_date_orig[0]={/literal}{$week_start}{literal}&field_option[invoice_date_orig][0]=>';
				} else if(s == '[m]' || s == 'month') {
					st += '&invoice_date_orig[0]={/literal}{$month_start}{literal}&field_option[invoice_date_orig][0]=>';
				} else {
					st += '&invoice_id=' +s;
				}	
				num+=1;
			}
		} 	
		var u = '?_page=core:search&module='+m+'&_next_page_one=view&_escape=1'+st;
		openUrl(u);  
	}
	
	
	// function for calling the invoice quicksearch
	function openQuickSearch_service() {
		var m = 'service';
		var st= "";
		var input = document.getElementById(m).value;		
		if (input.indexOf(" ")== -1) {
			var s = input;
			if(s == '[i]' || s == 'inactive') {
				st += '&service_active=0'; 			
			} else if(s == '' || s == 'all') {
				st += '';
			} else if(s == '[t]' || s == 'today') {
				st += '&service_date_orig[0]={/literal}{$today_start}{literal}&field_option[service_date_orig][0]=>';
			} else if(s == '[w]' || s == 'week') {
				st += '&service_date_orig[0]={/literal}{$week_start}{literal}&field_option[service_date_orig][0]=>';
			} else if(s == '[m]' || s == 'month') {
				st += '&service_date_orig[0]={/literal}{$month_start}{literal}&field_option[service_date_orig][0]=>';
			} else {
				st += '&service_id=' +s;
			}	
		} else {
			var array=input.split(" ");
			var num=0;
			while(num < array.length) 
			{
				var s = array[num];
				if(s == '[i]' || s == 'inactive') {
					st += '&service_active=0'; 			
				} else if(s == '' || s == 'all') {
					st += '';
				} else if(s == '[t]' || s == 'today') {
					st += '&service_date_orig[0]={/literal}{$today_start}{literal}&field_option[service_date_orig][0]=>';
				} else if(s == '[w]' || s == 'week') {
					st += '&service_date_orig[0]={/literal}{$week_start}{literal}&field_option[service_date_orig][0]=>';
				} else if(s == '[m]' || s == 'month') {
					st += '&service_date_orig[0]={/literal}{$month_start}{literal}&field_option[service_date_orig][0]=>';
				} else {
					st += '&service_id=' +s;
				}	
				num+=1;
			}
		} 	
		var u = '?_page=core:search&module='+m+'&_next_page_one=view&_escape=1'+st;
		openUrl(u);  
	}			
	
</script> {/literal} 
</BODY>
</HTML>