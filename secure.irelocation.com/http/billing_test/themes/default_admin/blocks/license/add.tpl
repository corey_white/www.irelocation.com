

<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
<form id="license_add" name="license_add" method="post" action="">
{$COOKIE_FORM}
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=license}title_add{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_account_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->popup("license_add", "license_account_id", $VAR.license_account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_status 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->bool("license_status", $VAR.license_status, "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_type 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <select name="license_type" class="form_menu">
                      <option value="trial" {if $VAR.license_type == 'trial'}selected{/if}>Trial</option>
                      <option value="temp"  {if $VAR.license_type == 'temp'}selected{/if}>Temporary</option>
                      <option value="perm"  {if $VAR.license_type == 'perm'}selected{/if}>Permanent</option>
                    </select>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_term 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="license_term" value="{$VAR.license_term}" {if $license_term == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_domain 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="license_domain" value="{$VAR.license_domain}" {if $license_domain == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=license}
                    field_ip 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="license_ip" value="{$VAR.license_ip}" {if $license_ip == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                    <input type="hidden" name="_page" value="license:view">
                    <input type="hidden" name="_page_current" value="license:add">
                    <input type="hidden" name="do[]" value="license:add">
                    <input type="hidden" name="license_date_last" value="{$smarty.now}">
                    <input type="hidden" name="license_date_orig" value="{$list->date($smarty.now)}">
                    <input type="hidden" name="license_invoice_id" value="{$VAR.license_invoice_id}">
                  </td>
                </tr>
              </table>
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
