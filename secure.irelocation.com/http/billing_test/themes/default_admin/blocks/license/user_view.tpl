{ $method->exe("license","user_view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'license';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$license item=license} <a name="{$license.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form name="license_view" method="post" action="">
{$COOKIE_FORM}
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=license}
                title_view 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="33%"> <b> 
                    {translate module=license}
                    field_date_orig 
                    {/translate}
                    </b> </td>
                  <td width="33%"> <b> 
                    {translate module=license}
                    field_date_last 
                    {/translate}
                    </b> </td>
                  <td width="25%"> <b> 
                    {translate module=license}
                    field_date_perm 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->date($license.date_orig)}
                  </td>
                  <td width="33%"> 
                    {$list->date($license.date_last)}
                  </td>
                  <td width="25%"> 
                    {$list->date($license.date_perm)}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> <b> 
                    {translate module=license}
                    field_type 
                    {/translate}
                    </b> </td>
                  <td width="33%"> <b> 
                    {translate module=license}
                    field_domain 
                    {/translate}
                    </b> </td>
                  <td width="25%"> <b> 
                    {translate module=license}
                    field_ip 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {if $license.type == 'perm'}
                    Permanent License 
                    {elseif $license.type == 'temp'}
                    Temporary License 
                    {elseif $license.type == 'trial'}
                    Trial License 
                    {/if}
                  </td>
                  <td width="33%"> <u>
                    {$license.domain}
                    </u> </td>
                  <td width="25%"> <u>
                    {$license.ip}
                    </u> </td>
                </tr>
              </table>
              {if $license.license_code == ''}
              <br>
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="20%"> 
                    {translate module=license}
                    field_domain 
                    {/translate}
                  </td>
                  <td width="20%"> 
                    <input type="text" name="license_domain" value="" class="form_field" size="20" maxlength="128">
                  </td>
                  <td width="60%">Enter the domain name you was to use the software 
                    on, eg: agileco.com (common values such as &quot;localhost&quot; 
                    will not be accepted.)</td>
                </tr>
                <tr valign="top"> 
                  <td width="20%"> 
                    {translate module=license}
                    field_ip 
                    {/translate}
                  </td>
                  <td width="20%"> 
                    <input type="text" name="license_ip" value="" class="form_field" size="20" maxlength="20">
                  </td>
                  <td width="60%">If applicable, enter the IP address you was 
                    to use the software on, eg: 225.10.2.55. (common values such 
                    as 127.0.0.1 will not be accepted)</td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="20%">&nbsp; </td>
                  <td width="20%">&nbsp; </td>
                  <td width="60%">&nbsp;</td>
                </tr>
              </table>
              <input type="submit" name="Submit2" value="Generate License Code" class="form_button">
              <br>
              <br>
              {elseif $license.change_domain == '' && $license.change_ip == ''}
              <br>
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="20%"> 
                    {translate module=license}
                    field_change_domain 
                    {/translate}
                  </td>
                  <td width="20%"> 
                    <input type="text" name="license_change_domain" value="{$license.change_domain}" class="form_field" size="20" maxlength="128">
                  </td>
                  <td width="60%">Enter the newdomain name you was to use the 
                    software on, eg: agileco.com (common values such as &quot;localhost&quot; 
                    will not be accepted.)</td>
                </tr>
                <tr valign="top"> 
                  <td width="20%"> 
                    {translate module=license}
                    field_change_ip 
                    {/translate}
                  </td>
                  <td width="20%"> 
                    <input type="text" name="license_change_ip" value="{$license.change_ip}" class="form_field" size="20" maxlength="20">
                  </td>
                  <td width="60%">If applicable, enter the newIP address you was 
                    to use the software on, eg: 225.10.2.55. (common values such 
                    as 127.0.0.1 will not be accepted)</td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="20%">&nbsp; </td>
                  <td width="20%">&nbsp; </td>
                  <td width="60%"><b>All license relocation request are subject 
                    to approval.</b> </td>
                </tr>
              </table>
              <p> 
                <input type="submit" name="Submit22" value="Request License Relocation" class="form_button">
                <br>
                <br>
                {else}
                <b>Your license relocation request is being proccessed.</b> 
                {/if}
              </p>
            </td>
          </tr>
          {if $license.license_code != ""}
          <tr valign="top"> 
            <td width="65%" class="row1">
              <div align="center"><b> 
                {translate module=license}
                field_license_key 
                {/translate}
                </b> </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <div align="center"> <b> 
                <input type="text" name="na" value="{$license.license_key}" class="form_field" size="50">
                <br>
                </b> </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <div align="center"><b>
                {translate module=license}
                field_license_code 
                {/translate}
                </b> </div>
            </td>
          </tr>
          <tr valign="top">
            <td width="65%" class="row1"> 
              <div align="center">
                <textarea name="textarea" cols="80" rows="20" class="form_field" wrap="OFF">{$license.license_code}</textarea>
              </div>
            </td>
          </tr>
          {/if}
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="license:user_view">
    <input type="hidden" name="license_id" value="{$license.id}">
    <input type="hidden" name="do[]" value="license:user_update">
    <input type="hidden" name="id" value="{$VAR.id}">
  </form>
  {/foreach}
{/if}
