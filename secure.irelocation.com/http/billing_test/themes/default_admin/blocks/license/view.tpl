
{ $method->exe("license","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'license';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$license item=license} <a name="{$license.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form name="license_view" method="post" action="">
{$COOKIE_FORM}
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=license}title_view{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
            <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_orig
                        {/translate}</td>
                    <td width="65%">
                        {$list->date($license.date_orig)}
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_last
                        {/translate}</td>
                    <td width="65%">
                        {$list->date($license.date_last)}  <input type="hidden" name="license_date_last" value="{$smarty.now}">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_expire
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_view("license_date_expire", $license.date_expire, "form_field", $license.id) }
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_perm
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_view("license_date_perm", $license.date_perm, "form_field", $license.id) }
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_account_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->popup("license_view", "license_account_id", $license.account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_invoice_id
                        {/translate}</td>
                    
                  <td width="65%">{$license.invoice_id} </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_license_key
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_license_key" value="{$license.license_key}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_license_code
                        {/translate}</td>
                    <td width="65%">
                        
                    <textarea name="license_license_code" cols="65" rows="20" class="form_field">{$license.license_code}</textarea>
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_status
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("license_status", $license.status, "form_menu") }
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_type" value="{$license.type}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_term
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_term" value="{$license.term}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_domain
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_domain" value="{$license.domain}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_ip
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_ip" value="{$license.ip}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_change_domain
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_change_domain" value="{$license.change_domain}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_change_ip
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_change_ip" value="{$license.change_ip}" class="form_field" size="32">
                    </td>
                  </tr>
                  <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_change_status
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("license_change_status", $license.change_status, "form_menu") }
                    </td>
                  </tr>
          <tr class="row1" valign="middle" align="left">
                    <td width="35%"></td>
                    <td width="65%">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td>
                            <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                          </td>
                          <td align="right">
                            <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$license.id}','{$VAR.id}');">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="license:view">
    <input type="hidden" name="license_id" value="{$license.id}">
    <input type="hidden" name="do[]" value="license:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  </form>
  {/foreach}
{/if}
