
{ $method->exe("license","search_form") }
{ if ($method->result == FALSE) }
    { $block->display("core:method_error") }
{else}

<form name="license_search" method="post" action="">
  {$COOKIE_FORM}
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=license}title_search{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
            <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_orig
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_search("license_date_orig", $VAR.license_date_orig, "form_field", "") }
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_expire
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_search("license_date_expire", $VAR.license_date_expire, "form_field", "") }
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_date_perm
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_search("license_date_perm", $VAR.license_date_perm, "form_field", "") }
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_account_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->popup("license_search", "license_account_id", $VAR.license_account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_license_key
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_license_key" value="{$VAR.license_license_key}" {if $license_license_key == true}class="form_field_error"{else}class="form_field"{/if}> &nbsp;&nbsp;{translate}search_partial{/translate}
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_status
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("license_status", "all", "form_menu") }
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_type" value="{$VAR.license_type}" {if $license_type == true}class="form_field_error"{else}class="form_field"{/if}> &nbsp;&nbsp;{translate}search_partial{/translate}
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_term
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_term" value="{$VAR.license_term}" {if $license_term == true}class="form_field_error"{else}class="form_field"{/if}> &nbsp;&nbsp;{translate}search_partial{/translate}
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_domain
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_domain" value="{$VAR.license_domain}" {if $license_domain == true}class="form_field_error"{else}class="form_field"{/if}> &nbsp;&nbsp;{translate}search_partial{/translate}
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_ip
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="license_ip" value="{$VAR.license_ip}" {if $license_ip == true}class="form_field_error"{else}class="form_field"{/if}> &nbsp;&nbsp;{translate}search_partial{/translate}
                    </td>
                  </tr>
                   <tr valign="top">
                    <td width="35%">
                        {translate module=license}
                            field_change_status
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("license_change_status", "all", "form_menu") }
                    </td>
                  </tr>
                           <!-- Define the results per page -->
                  <tr class="row1" valign="top">
                    <td width="35%">{translate}search_results_per{/translate}</td>
                    <td width="65%">
                      <input type="text" class="form_field" name="limit" size="5" value="{$license_limit}">
                    </td>
                  </tr>

                  <!-- Define the order by field per page -->
                  <tr class="row1" valign="top">
                    <td width="35%">{translate}search_order_by{/translate}</td>
                    <td width="65%">
                      <select class="form_menu" name="order_by">
        		          {foreach from=$license item=record}
                            <option value="{$record.field}">{$record.translate}</option>
        		          {/foreach}
                      </select>
                    </td>
                  </tr>

                  <tr class="row1" valign="top">
                    <td width="35%"></td>
                    <td width="65%">
                      <input type="submit" name="Submit" value="{translate}search{/translate}" class="form_button">
                      <input type="hidden" name="_page" value="core:search">
                      <input type="hidden" name="_escape" value="Y">
                      <input type="hidden" name="module" value="license">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
{ $block->display("core:saved_searches") }
{ $block->display("core:recent_searches") }
{/if}
