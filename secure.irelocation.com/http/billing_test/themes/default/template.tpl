<HTML>
<HEAD>
<TITLE>{$smarty.const.SITE_NAME} - Powered by AgileBill</TITLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- START
	var escape			= "";
	var sess_expires 	= "{$smarty.const.SESSION_EXPIRE}";
	var cookie_name		= "{$smarty.const.COOKIE_NAME}";
	var SESS	 		= "{$SESS}";
	var URL				= "{$URL}";
	var SSL_URL			= "{$SSL_URL}";
	var THEME_NAME  	= "{$THEME_NAME}";
	var REDIRECT_PAGE 	= "{$smarty.const.REDIRECT_PAGE}";
{literal}
  if (top.location != location) {
    top.location.href = document.location.href ;
  }
{/literal}

//  END -->
</SCRIPT>

<!-- Load the main stylesheet -->
<link rel="stylesheet" href="themes/{$THEME_NAME}/style.css" type="text/css">

<!-- Load the main javascript code -->
<SCRIPT SRC="themes/{$THEME_NAME}/top.js"></SCRIPT>
 
</HEAD> 
<body bgcolor="#CCCCCC" text="#000000" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" class="body">
<img src="themes/{$THEME_NAME}/images/invisible.gif" width="160" height="15" align="middle"> 
<table width="700" border="0" align="center" bgcolor="#006600" cellpadding="1" cellspacing="0">
  <tr> 
    <td> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class=body>
        <tr> 
          <td> 
            <div align="center"><br>
              <img src="themes/{$THEME_NAME}/images/logo.gif"><br>
              <br>
            </div>
          </td>
        </tr>
        <tr> 
          <td bgcolor="#CCCCCC" height="1"></td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="200">
              <tr> 
           
                <td width="160" valign="top" height="200" bgcolor="F5F5F5"> <img src="themes/{$THEME_NAME}/images/invisible.gif" width="160" height="10"> 
                  { $block->display("TEMPLATE:left")   }
                </td>
              <td width="1" bgcolor="#CCCCCC"> <img src="themes/{$THEME_NAME}/images/invisible.gif" width="1" height="1"><br>
              </td>
              <td valign="top" width="575" bgcolor="#FFFFFF"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="14">
                  <tr> 
                    <td valign="top" class="body"> 
                      <!-- display the alert block -->
                      {if $alert}
                      { $block->display("core:alert") }
                      {/if}
                      <!-- display the main page -->
                      {if $VAR._page != ''}
                      { $block->display($smarty.const.THEME_PAGE) }
                      {elseif $SESS_LOGGED == "1"}
                      { $block->display("account:account") }
                      {else}
                      { $block->display("account:login") }
                      {/if}
                    </td>
                  </tr>
                </table>
              </td>
              <td width="1" bgcolor="#FFFFFF"> </td>
              </tr>
              <tr> 
                <td width="160" valign="top" bgcolor="#F5F5F5"></td>
                <td width="1" bgcolor="#CCCCCC"></td>
                <td height="1" width="200" bgcolor="#CCCCCC"><img src="themes/{$THEME_NAME}/images/invisible.gif" width="1" height="1"></td>
                <td width="1" bgcolor="#FFFFFF"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"><br>
          </td>
        </tr>
        <tr> 
          <td bgcolor="#999999" height="1"></td>
        </tr>
        <tr> 
          <td bgcolor="F1F1F1"> 
            <div align="center"><img src="themes/{$THEME_NAME}/images/invisible.gif" width="160" height="5"><br>
              <a href="http://agileco.com/"><font color="#999999">Billing Software</font></a><font color="#999999"> 
              powered by</font> <a href="http://agilebill.com/"><font color="#000000">Agile</font><font color="#006600">Bill</font></a><font color="#999999">&#153;</font><br>
              <img src="themes/{$THEME_NAME}/images/invisible.gif" width="160" height="5"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</BODY>
</HTML>