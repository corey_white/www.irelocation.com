{if $smarty.const.SESS_LOGGED}

{ $method->exe("ticket","user_list")} 		{ if ($method->result == FALSE) } { $block->display("core:method_error") } {/if}

{if $ticket_results != false}
<table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top"> 
          <td width="65%" class="table_heading"> 
            <div align="center"> 
              {translate module=ticket}
              menu_view 
              {/translate}
            </div>
          </td>
        </tr>
        <tr valign="top"> 
          <td width="65%" class="row1">
            <table width="100%" border="0" cellpadding="3" class=row1>
              <tr> 
                <td width="60%"> 
                  {translate module=ticket}
                  user_ticket_list 
                  {/translate}
                </td>
              </tr>
              <tr> 
                <td width="60%"> 
                  <p> 
                  <table width="100%" border="0" cellpadding="1" class="row2">
                    <tr> 
                      <td width="61%"> <u> <b> 
                        {translate}
                        field_subject 
                        {/translate}
                        </b></u></td>
                      <td width="29%"> <u> <b> 
                        {translate}
                        field_date_last 
                        {/translate}
                        </b></u></td>
                      <td width="10%"> <u> </u><u><b> 
                        {translate}
                        field_status 
                        {/translate}
                        </b></u></td>
                    </tr>
                    {foreach from=$ticket_results item=record}
                  </table>
                  <table width="100%" border="0" cellpadding="1" class="row2">
                    <tr> 
                      <td width="61%"> <a href="?_page=ticket:user_view&id={$record.id}"><font color="#000000"> 
                        <b> 
                        {$record.subject|truncate:40}
                        </b></font></a> </td>
                      <td width="29%"> 
                        { $list->date_time($record.date_orig)}
                      </td>
                      <td width="10%"> 
                        { if $record.status == '0' }
                        {translate module=ticket}
                        status_open 
                        {/translate}
                        { elseif $record.status == '1' }
                        {translate module=ticket}
                        status_hold 
                        {/translate}
                        { elseif $record.status == '2' }
                        {translate module=ticket}
                        status_close 
                        {/translate}
                        { /if }
                      </td>
                    </tr>
                    {/foreach}
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p> 
  {else}
  {translate module=ticket}
  user_no_existing_ticket 
  {/translate}
  {/if}
  {else}
  {translate}
  login_required 
  {/translate}
  { $block->display("account:login")}
</p>
{/if}
