
{ $method->exe("ticket","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'ticket';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		

		
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}
			
		function delete_message(id,ids)
		{
			temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
			if(temp == false) return; 
    		var url = '?_page=ticket:view&id=' + ids + '&do[]=ticket_message:delete&delete_id=' + id;
    		document.location = url;
    		return; 	
		}				
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$ticket item=ticket} <a name="{$ticket.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form name="ticket_view" method="post" action="">

  <table width=100% border="0" cellspacing="0" cellpadding="1" class="table_background">
    <tr>
      <td class="table_heading">
        <center>
          {translate module=ticket}title_view{/translate}
        </center>
      </td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="3" cellpadding="2" class="row1">
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_date_orig 
              {/translate}
            </td>
            <td width="80%"> 
              {$list->date_time($ticket.date_orig)}
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_date_last 
              {/translate}
            </td>
            <td width="80%"> 
              {$list->date_time($ticket.date_last)}
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_status 
              {/translate}
            </td>
            <td width="80%"> 
              <select name="ticket_status" class="form_menu">
                <option value="0" {if $ticket.status == "0"}selected{/if}> 
                {translate module=ticket}
                status_open 
                {/translate}
                </option>
                <option value="1" {if $ticket.status == "1"}selected{/if}> 
                {translate module=ticket}
                status_hold 
                {/translate}
                </option>
                <option value="2" {if $ticket.status == "2"}selected{/if}> 
                {translate module=ticket}
                status_close 
                {/translate}
                </option>
              </select>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_priority 
              {/translate}
            </td>
            <td width="80%"> 
              <select name="ticket_priority" class="form_menu">
                <option value="0" {if $ticket.priority == "0"}selected{/if}> 
                {translate module=ticket}
                priority_standard 
                {/translate}
                </option>
                <option value="1" {if $ticket.priority == "1"}selected{/if}> 
                {translate module=ticket}
                priority_medium 
                {/translate}
                </option>
                <option value="2" {if $ticket.priority == "2"}selected{/if}> 
                {translate module=ticket}
                priority_high 
                {/translate}
                </option>
                <option value="3" {if $ticket.priority == "3"}selected{/if}> 
                {translate module=ticket}
                priority_emergency 
                {/translate}
                </option>
              </select>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_department_id 
              {/translate}
            </td>
            <td width="80%"> 
              { $list->menu("", "ticket_department_id", "ticket_department", "name", $ticket.department_id, "form_menu") }
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_staff_id 
              {/translate}
            </td>
            <td width="80%"> 
              { $list->menu("", "ticket_staff_id", "staff", "nickname", $ticket.staff_id, "form_menu") }
            </td>
          </tr>
          <tr valign="top"> 
            <td width="20%"> 
              {translate module=ticket}
              field_account_id 
              {/translate}
            </td>
            <td width="80%"> 
              { $list->popup("ticket_view", "ticket_account_id", $ticket.account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", false) }
            </td>
          </tr>
		  
          {if $ticket.static_var != false}
          {foreach from=$ticket.static_var item=record}
          <tr valign="top" class="row2"> 
            <td width="20%"> 
              {$record.name}
            </td>
            <td width="80%"> 
              {$record.html}
            </td>
          </tr>
          {/foreach}
          {/if}
		  
		  		  
          <tr class="row1" valign="middle" align="left"> 
            <td width="20%"></td>
            <td width="80%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                  <td align="right"> 
                    <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$ticket.id}','{$VAR.id}');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="ticket:view">
    <input type="hidden" name="ticket_id" value="{$ticket.id}">
    <input type="hidden" name="do[]" value="ticket:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  <input type="hidden" name="ticket_subject" value="{$ticket.subject}">
  <input type="hidden" name="ticket_body" value="{$ticket.body}">
  <input type="hidden" name="ticket_date_last" value="">
</form>
  
<table width=100% border="0" cellspacing="0" cellpadding="1" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="4" cellpadding="1" class="row1">
        <tr valign="top"> 
          <td width="20%">&nbsp; </td>
          <td width="80%" valign="top" align="right"> 
            {$list->date_time($ticket.date_orig)}
          </td>
        </tr>
        <tr valign="top" class="row2"> 
          <td width="20%"> 
            {translate module=ticket}
            field_subject 
            {/translate}
          </td>
          <td width="80%"> 
            {$ticket.subject}
          </td>
        </tr>
        <tr valign="top"  class="row2"> 
          <td width="20%"> 
            {translate module=ticket}
            field_body 
            {/translate}
          </td>
          <td width="80%"> 
            {$ticket.body|replace:"
":"<br>"}
          </td>
        </tr>
        {if $show_static_var != false}
        {foreach from=$static_var item=record}
        {/foreach}
        {/if}
      </table>
    </td>
  </tr>
</table>
<br>
{if $ticket.reply != false}
{foreach from=$ticket.reply item=reply}
<table width=100% border="0" cellspacing="0" cellpadding="1" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="4" cellpadding="1" class="row2">
        <tr valign="top"> 
          <td width="80%" valign="top" align="right"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="row1">
              <tr> 
                <td width="9%">
                  <input type="button" name="delete2" value="{translate}delete{/translate}" class="form_button" onClick="delete_message('{$reply.id}','{$VAR.id}');">
                </td>
                <td width="70%"> 
                  {if $reply.staff_id < 1 }
                  <b> 
                  {translate module=ticket}
                  user_wrote_staff 
                  {/translate}
                  </b> 
                  {else}
                  <a href="?_page=staff:view&id={$reply.staff_id}">
                  {translate module=ticket}
                  staff_wrote 
                  {/translate}
                  </a> 
                  {/if}
                </td>
                <td width="21%" align="right"> 
                  {if $reply.staff_id > 1 }
                  <b> 
                  {$list->date_time($reply.date_orig)}
                  </b> 
                  {else}
                  {$list->date_time($reply.date_orig)}
                  {/if}
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr valign="top"  class="row2"> 
          <td width="80%"> 
            {if $reply.staff_id > 1 }
            <b> 
            {$reply.message|replace:"
":"<br>"}
            </b> 
            {else}
            {$reply.message|replace:"
":"<br>"}
            {/if}
          </td>
        </tr>
        {if $show_static_var != false}
        {foreach from=$static_var item=record}
        {/foreach}
        {/if}
      </table>
    </td>
  </tr>
</table>
<br>
{/foreach}
{/if}
<form name="ticket_view" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="1" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="4" cellpadding="1" class="row1">
          <tr valign="top"  class="row2"> 
            <td width="80%"> 
              <div align="center"> 
                {translate module=ticket}
                user_add_response 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"  class="row2"> 
            <td width="80%"> 
              <div align="center"> 
                <textarea name="ticket_reply" cols="85" rows="10" class="form_field"></textarea>
              </div>
            </td>
          </tr>
          <tr valign="top"  class="row2"> 
            <td width="80%"> 
              <div align="center"> 
                <input type="submit" name="Submit2" value="{translate}submit{/translate}" class="form_button">
                <input type="hidden" name="_page" value="ticket:view">
                <input type="hidden" name="ticket_id" value="{$ticket.id}">
                <input type="hidden" name="do[]" value="ticket:reply">
                <input type="hidden" name="id" value="{$VAR.id}">
              </div>
            </td>
          </tr>
          {if $show_static_var != false}
          {foreach from=$static_var item=record}
          {/foreach}
          {/if}
        </table>
      </td>
    </tr>
  </table>
  <br>
</form>
  
 

{/foreach}
{/if}
