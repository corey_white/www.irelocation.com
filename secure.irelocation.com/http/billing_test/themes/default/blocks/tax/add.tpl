

<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
<form id="tax_add" name="tax_add" method="post" action="">

<table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=tax}title_add{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
            <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top">
                    <td width="35%">
                        {translate module=tax}
                            field_country_id
                        {/translate}</td>
                    <td width="65%">
					{if $VAR.tax_country_id != ""}
                        { $list->menu("", "tax_country_id", "country", "name", $VAR.tax_country_id, "form_menu") }
					{else}
						{ $list->menu("", "tax_country_id", "country", "name", $smarty.const.DEFAULT_COUNTRY, "form_menu") }
					{/if}
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=tax}
                            field_zone
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="tax_zone" value="{$VAR.tax_zone}" {if $tax_zone == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=tax}
                            field_description
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="tax_description" value="{$VAR.tax_description}" {if $tax_description == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=tax}
                            field_rate
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="tax_rate" value="{$VAR.tax_rate}" {if $tax_rate == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
           <tr valign="top">
                    <td width="35%"></td>
                    <td width="65%">
                      <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                      <input type="hidden" name="_page" value="tax:view">
                      <input type="hidden" name="_page_current" value="tax:add">
                      <input type="hidden" name="do[]" value="tax:add">
                    </td>
                  </tr>
                </table>
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
