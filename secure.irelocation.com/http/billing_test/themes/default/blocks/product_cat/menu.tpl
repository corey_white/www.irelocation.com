{ if $list->smarty_array("product_cat", "position,template,name", " AND status='1'", "product_cat_arr") } 
<table width="140" border="0" cellspacing="0" cellpadding="0" class="menu_background" align="center">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center">
        <tr> 
          <td class="menu_1"> 
            <center>
              {translate}
              products 
              {/translate}
            </center>
          </td>
        </tr>
		
		{foreach from=$product_cat_arr item=record key=key}
        <tr> 
          <td class="menu_2"> &nbsp;&nbsp; 
            <a href="{$URL}?_page=product_cat:t_{$record.template}&id={$record.id}">
			{if $list->translate("product_cat_translate","name,description","product_cat_id", $record.id, "cat_translate") }
            {$cat_translate.name}
            {else}
            {$record.name}
            {/if}
            </a>
 			</td>
        </tr>
        {/foreach}
		
		{ if $smarty.const.SHOW_DOMAIN_LINK }
        <tr> 
          <td class="menu_2">  &nbsp;&nbsp; 
		    <a href="{$URL}?_page=host_tld:search">
            {translate}
            domain_search 
            {/translate}
            </a>
		  </td>
        </tr>
        {/if}
      </table>
    </td>
  </tr>
</table>
<br>
{/if}
