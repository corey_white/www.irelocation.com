{if $VAR.id == ""}	{$block->display("product:cat")} {else}
	{assign var=sql1 value=" AND id='"} 	{assign var=sql2 value=$VAR.id} 	{assign var=sql3 value="' "} 	{assign var=sql value=$sql1$sql2$sql3}
{ if $list->smarty_array("product_cat", "position,template,name,thumbnail", $sql, "product_cat_arr") } 
{foreach from=$product_cat_arr item=record}
<table width=100% border="0" cellspacing="1" cellpadding="0" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr valign="top"> 
          <td width="65%" class="row1"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_heading_cart" height="25">
              <tr valign="top"> 
                <td> <a href="{$URL}?_page=product:cat"> Categories</a> &nbsp;&nbsp;:: 
                  &nbsp; <a href="{$URL}?_page=product_cat:t_{$record.template}&id={$record.id}"> 
                  {if $list->translate("product_cat_translate","name,description","product_cat_id", $record.id, "cat_translate") }
                  {$cat_translate.name}
                  {else}
                  {$record.name}
                  </a> <b> 
                  {/if}
                  </b> </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
{/foreach}
{/if}
{/if}
<br>
<br>
{ if $list->smarty_array("product", "sku,thumbnail,avail_category_id,price_base,price_setup", " AND active='1' ", "product_arr") }
{foreach from=$product_arr item=product}
{$list->unserial($product.avail_category_id, "avail_cat")}
{assign var=show value=false}
{foreach from=$avail_cat item=avail_category}
{if $avail_category == $VAR.id}
{assign var=show value=true}
{/if}
{/foreach}
{if $show}
<table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top"> 
          <td width="65%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_heading_cart">
              <tr valign="top"> 
                <td width="78%" > 
                  <p><a href="{$URL}?_page=product:details&id={$product.id}"> 
                    </a><a href="{$URL}?_page=product:details&id={$product.id}"> 
                    {if $list->translate("product_translate","name,description_short,description_full","product_id", $product.id, "prod_translate") }
                    {$prod_translate.name}
                    {else}
                    {$product.sku}
                    {/if}
                    </a> </p>
                </td>
                <td width="22%" valign="middle" align="right"> <b> 
                  {$list->format_currency($product.price_base,$smarty.const.SESS_CURRENCY)}
                  </b> </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr valign="top"> 
          <td width="65%" class="row1"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="5" class="body" bgcolor="#FFFFFF">
              <tr valign="top"> 
                
				{if $product.thumbnail != ""}
				<td width="8%" valign="top"> 
                  <p> 
				  <a href="{$URL}?_page=product:details&id={$product.id}"><img  src="{$URL}{$smarty.const.URL_IMAGES}{$product.thumbnail}" hspace="5" border="0"></a>
				  </p>
                </td>
				{/if}
				
                <td width="92%" align="left"> 
                  <p> 
                    {if $product.thumbnail != "" }
                    <a href="{$URL}?_page=product:details&id={$product.id}"> </a> 
                    {/if}
                    {if $prod_translate.description_short != "" }
                    {$prod_translate.description_short}
                    {else}
                    No Description Available 
                    {/if}
                  </p>
                  </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
{/if}
{/foreach}
{/if}
