	{$method->exe("cart","view")}
	{if ($method->result == FALSE)}
		{$block->display("core:method_error")}
	{else}
		{if $results == 0}
			No items in your cart at this time....
		{else}
			{if $SESS_LOGGED != "1"}
				{ $block->display("account:login")  }
			{else}
				
				
				
<!-- LOOP THROUGH EACH RECORD -->
{foreach from=$cart item=cart}
<DIV id="{$cart.id}"> 
  {if $cart.cart_type == "2"}
  <!-- Show domain -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> <u> 
                          {$cart.domain_name|upper}.{$cart.domain_tld|upper}
                          </u> </b></td>
                        <td width="37%">&nbsp;</td>
                        <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a><a href="?_page=product:details&id={$cart.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> 
                    {if $cart.host_type == "register"}
                    {translate module=cart}
                    register 
                    {/translate}
                    {elseif $cart.host_type == "transfer"}
                    {translate module=cart}
                    transfer 
                    {/translate}
                    {elseif $cart.host_type == "park"}
                    {translate module=cart}
                    park 
                    {/translate}
                    {/if}
                    </b></td>
                </tr>
                {if $cart.cart_type == "1"}
                {if $cart.host_type == "ns_transfer"}
                {/if}
                {if $cart.host_type == "ip"}
                {/if}
                {/if}
              </table>
            </td>
            <td width="30%" class="row1" valign="top" align="right"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <div id="def_base_price_{$cart_assoc.id}"> 
                    <DIV id="def_base_price_{$cart.id}"> 
                      {$list->format_currency($cart.price, $smarty.const.SESS_CURRENCY)}
                    </DIV>
                    <DIV id="base_price_{$cart.id}"></DIV>
                    </div>
                    <div id="base_price_{$cart_assoc.id}"></div>
                  </td>
                </tr>
              </table>
			  {if $cart.host_type == 'register'}
              <select id="quantity_{$cart.id}" class="form_menu" disabled>
                {foreach from=$cart.tld_arr item=tld_price key=tld_term}
                <option value="{$tld_term}" {if $tld_term == $cart.domain_term}selected{/if}> 
                {$tld_term}
                Year 
                {$list->format_currency($tld_price, $smarty.const.SESS_CURRENCY)}
                </option>
                {/foreach}
              </select>
			  {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
{else}
<!-- Show product -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> 
                          {if $list->translate("product_translate","name", "product_id", $cart.product_id, "translate_product")}
                          {/if}
                          <U> 
                          {$translate_product.name}
                          </U> </b></td>
                        <td width="35%"><b>&nbsp;</b>( <a href="?_page=product:details&id={$cart.product_id}"> 
                          {$cart.product.sku}
                          </a> ) &nbsp; </td>
                        <td width="14%" align="right"><a href="?_page=product:details&id={$cart.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a><a href="?_page=product:details&id={$cart.product_id}"> 
                          </a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> 
                    {translate module=cart}
                    price_type 
                    {/translate}
                    : </b> 
                    {if $cart.product.price_type == "0"}
                    {translate module=cart}
                    price_type_one 
                    {/translate}
                    {/if}
                    {if $cart.product.price_type == "1"}
                    {translate module=cart}
                    price_type_recurr 
                    {/translate}
                    {/if}
                    {if $cart.product.price_type == "2"}
                    {translate module=cart}
                    price_type_trial 
                    {/translate}
                    {/if}
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> 
                    {if $cart.product.price_type == "1"}
                    &nbsp;&nbsp; 
                    <select id="recurr_schedule_{$cart.id}" name="recurr_schedule_{$cart.id}" class="form_menu" disabled>
                      {foreach from=$cart.price item=price_recurr key=key}
                      <option value="{$key}" {if $cart.recurr_schedule == $key} selected{/if}> 
                      {$list->format_currency($price_recurr.base,$smarty.const.SESS_CURRENCY)}
                      &nbsp;&nbsp; 
                      {if $key == "0" }
                      {translate module=cart}
                      recurr_week 
                      {/translate}
                      {/if}
                      {if $key == "1" }
                      {translate module=cart}
                      recurr_month 
                      {/translate}
                      {/if}
                      {if $key == "2" }
                      {translate module=cart}
                      recurr_quarter 
                      {/translate}
                      {/if}
                      {if $key == "3" }
                      {translate module=cart}
                      recurr_semianual 
                      {/translate}
                      {/if}
                      {if $key == "4" }
                      {translate module=cart}
                      recurr_anual 
                      {/translate}
                      {/if}
                      {if $key == "5" }
                      {translate module=cart}
                      recurr_twoyear 
                      {/translate}
                      {/if}
                      &nbsp;&nbsp; + &nbsp; 
                      {$list->format_currency($price_recurr.setup,$smarty.const.SESS_CURRENCY)}
                      {translate module=cart}
                      setup
                      {/translate}
                      </option>
                      {/foreach}
                    </select>      
					{/if}                   
                  </td>
                </tr>
				
				 
                {if $cart.cart_type == "1"}
				{if $cart.host_type == "ns_transfer"}
                <tr>
                  <td width="67%">&nbsp;&nbsp; 
                    {translate module=cart}
                    host_type_domain 
                    {/translate}
                    - <u>
                    {$cart.domain_name}.{$cart.domain_tld}
                    </u></td>
                </tr>
				{/if}
				
				{if $cart.host_type == "ip"}
                <tr>
                  <td width="67%">&nbsp;&nbsp; 
                    {translate module=cart}
                    host_type_ip 
                    {/translate}
                  </td>
                </tr>
				{/if}
				{/if}				
              </table>
            </td>
            <td width="30%" class="row1" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <DIV id="def_base_price_{$cart.id}"> 
                      {$list->format_currency($cart.price_base, $smarty.const.SESS_CURRENCY)}
                    </DIV>
                    <DIV id="base_price_{$cart.id}"></DIV>
                  </td>
                </tr>
                <tr> 
                  <td width="43%">Setup Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <DIV id="def_setup_price_{$cart.id}"> 
                      {$list->format_currency($cart.price_setup, $smarty.const.SESS_CURRENCY)}
                    </DIV>
                    <DIV id="setup_price_{$cart.id}"></DIV>
                  </td>
                </tr>
                <tr> 
                  <td width="43%"> 
                    {translate module=cart}
                    quantity 
                    {/translate}
                  </td>
                  <td width="57%" valign="middle" align="right"> 
                    {$cart.quantity}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
 </table>
{/if}
<br> 

{foreach from=$cart.assoc item=cart_assoc} 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> <u> 
                          {$cart_assoc.domain_name|upper}
                          . 
                          {$cart_assoc.domain_tld|upper}
                          </u> </b></td>
                        <td width="37%">&nbsp;</td>
                        <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> </b> 
                    {if $cart_assoc.host_type == "register"}
						{translate module=cart} host_type_register {/translate}
					{elseif $cart_assoc.host_type == "transfer"}
						{translate module=cart} host_type_transfer {/translate}
					{elseif $cart_assoc.host_type == "park"}
						{translate module=cart} host_type_park {/translate}
					{/if}
                    {$cart.product.sku}
                  </td>
                </tr>
              </table>
            </td>
            <td width="30%" class="row1" valign="top" align="right"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <div id="def_base_price_{$cart_assoc.id}"> 
                      {$list->format_currency($cart_assoc.price, $smarty.const.SESS_CURRENCY)}
                    </div>
                    <div id="base_price_{$cart_assoc.id}"></div> 
                  </td>
                </tr>
              </table>
			  {if $cart_assoc.host_type == 'register'}
              <select id="quantity_{$cart_assoc.id}" class="form_menu" disabled>
                {foreach from=$cart_assoc.tld_arr item=tld_price key=tld_term}
                <option value="{$tld_term}" {if $tld_term == $cart_assoc.domain_term}selected{/if}> 
                {$tld_term}
                Year  
                {$list->format_currency($tld_price, $smarty.const.SESS_CURRENCY)}
                </option>
                {/foreach}
              </select>
			  {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
 </table>
<br> 
{/foreach}
</DIV>
{/foreach}
<!-- END OF RESULT LOOP -->


<!-- CURRENCY OPTIONS & DISCOUNT CODE ENTRY -->
<table width="100%" border="0" cellspacing="0" cellpadding="3" class="body">
  <tr> 
    <td width="33%" align="left" valign="top"> 
      <table width="150" border="0" cellspacing="3" cellpadding="1" class="row1">
        <tr valign="top" class="row2"> 
          <td width="50%"><b> 
            {translate}
            currency 
            {/translate}
            </b></td>
        </tr>
        <tr valign="top" class="row1"> 
          <td width="50%" valign="middle"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="78%"> 
                  {literal}
                  <script language="JavaScript"> function CurrencyChange(obj) { document.location='{/literal}{$SSL_URL}{literal}?_page=checkout:checkout&cyid='+obj.value; } </script>
                  {/literal}
                  {$list->currency_list("cyid_arr")}
                  <select name="currency" class="form_menu" onChange="CurrencyChange(this);">
                    {foreach key=key item=item from=$cyid_arr}
                    <option value="{$key}" {if $key == $smarty.const.SESS_CURRENCY}{assign var=currency_thumbnail value=$item.iso}selected{/if}> 
                    {$item.iso}
                    </option>
                    {/foreach}
                  </select>
                </td>
                <td width="22%"> <img src="themes/{$THEME_NAME}/images/currency/{$currency_thumbnail}.gif" border="0"> 
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="33%" valign="top" align="center"> 
      { if $smarty.const.SHOW_DISCOUNT_CODE == 1 }
      <form name="form1" method="post" action="javascript:addDiscount()">
        <table width="150" border="0" cellspacing="3" cellpadding="1" class="row1">
          <tr valign="top" class="row2"> 
            <td width="50%"><b> 
              {translate module=checkout}
              discounts 
              {/translate}
              </b></td>
          </tr>
          <tr valign="top" class="row1"> 
            <td width="50%" valign="middle"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="92%"> 
                    {literal}
                    <script language="JavaScript"> 
				  function addDiscount() { 
				  	var discount = document.getElementById("discount").value;
				  	document.location='{/literal}{$SSL_URL}{literal}?_page=checkout:checkout&discount='+discount+'&do[]=checkout:adddiscount'; 
				  } 
					</script>
                    {/literal}
                    <input type="text" id="discount" name="discount" size="12" class="form_field" onBlur="addDiscount()">
                    </td>
                  <td width="8%"><a href="javascript:addDiscount();"><img src="themes/{$THEME_NAME}/images/icons/calc_16.gif" border="0"></a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </form>
	  {/if}
	  
    </td>
    <td width="33%" valign="top" align="right"> 
	{ if $list->is_installed('affiliate') == 1 && $smarty.const.SHOW_AFFILIATE_CODE == 1 }
      <form name="aid" method="post" action="{$SSL_URL}">
        
        <input type="hidden" name="_page" value="checkout:checkout"> 
        <table width="150" border="0" cellspacing="3" cellpadding="1" class="row1">
          <tr valign="top" class="row2"> 
            <td width="50%"><b> 
              {translate module=checkout}
              affiliate 
              {/translate}
              </b></td>
          </tr>
          <tr valign="top" class="row1"> 
            <td width="50%" valign="middle"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td width="92%"> 
                    <input type="text" id="discount" name="aid" size="12" class="form_field" value="{$smarty.const.SESS_AFFILIATE}" onBlur="document.aid.submit()">
                  </td>
                  <td width="8%">
				  <a href="javascript:document.aid.submit();">
				  <img src="themes/{$THEME_NAME}/images/icons/exp_16.gif" border="0"></a></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </form>
	  {/if}
	  
    </td>
  </tr>
</table>
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="body">
  <tr> 
    <td valign="top" width="67%">To modify / remove products from this purchase, 
      <a href="?_page=cart:cart">click here</a>...</td>
    <td width="33%"> 
      <table width="225" border="0" cellspacing="3" cellpadding="1" class="row1" align="right">
        <tr valign="top" class="row2"> 
          <td width="50%"><b> 
            {translate module=checkout}
            totals 
            {/translate}
            </b></td>
        </tr>
        <tr valign="top" class="row1"> 
          <td width="50%" valign="middle"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
              <tr> 
                <td width="63%"> Sub-total</td>
                <td width="37%" align="right"> 
                  {$list->format_currency($sub_total, $smarty.const.SESS_CURRENCY)}
                </td>
              </tr>
              {foreach from=$discount item=discount}
              {if $discount.total > 0}
              <tr> 
                <td width="63%">Discount ( 
                  {$discount.name}
                  ) </td>
                <td width="37%" align="right"> 
                  {$list->format_currency($discount.total, $smarty.const.SESS_CURRENCY)}
                </td>
              </tr>
              {/if}
              {/foreach}
              {if $tax != false}
              <tr> 
                <td width="63%"> 
                  {$tax.name}
                </td>
                <td width="37%" align="right"> 
                  {$list->format_currency($tax.rate, $smarty.const.SESS_CURRENCY)}
                </td>
              </tr>
              {/if}
              <tr> 
                <td width="63%"><b>Total</b></td>
                <td width="37%" align="right"><b> 
                  {$list->format_currency($total, $smarty.const.SESS_CURRENCY)}
                  </b></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<center>
<iframe id="iframeCheckout" style="border:0px; width:0px; height:0px;" scrolling="auto" ALLOWTRANSPARENCY="true" frameborder="0" SRC="themes/{$THEME_NAME}/IEFrameWarningBypass.htm"></iframe> 
</center>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="body">
  <tr> 
    <td valign="top" width="67%"> 
      {if $checkout}
      {foreach from=$checkout item=checkout key=key}
      <b><u>
      {literal}
      <script language="JavaScript"> 
	  	function changeCheckoutOption(option)  {  
				var url ='{/literal}{$SSL_URL}{literal}?_page=checkout:checkoutoption&option='+option+'&_escape=1'; 
				showIFrame('iframeCheckout',0,0,url);
				document.getElementById("checkout_option_"+option).checked = "1";
	    }
		
		function getCheckoutOption() {
			return getElementById("checkout_option").value;
		} 
	  </script>
      {/literal}
      </u></b> 
      <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
        <tr valign="top" class="row2"> 
          <td id="checkout_{$checkout.fields.id}"
			onMouseOver="class_change('checkout_{$checkout.fields.id}', 'row1');"
			onMouseOut ="class_change('checkout_{$checkout.fields.id}',  'row2');"		  
		    onClick="changeCheckoutOption({$checkout.fields.id})">
		  <b><u>  
            <input type="radio" id="checkout_option_{$checkout.fields.id}" name="checkout_option" value="{$checkout.fields.id}">
            {$checkout.fields.name}
            </u> </b></td>
        </tr>
		  {if $checkout_c == 1}<script language=javascript>changeCheckoutOption({$checkout.fields.id});</script>{/if}
		
        <tr valign="top" class="row1"> 
          <td width="50%" valign="middle"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
              <tr> 
                <td width="63%">
                  {$checkout.fields.description}
                </td>
              </tr>
              {foreach from=$discount item=discount}
              {if $discount.total > 0}
              {/if}
              {/foreach}
              {if $tax != false}
              {/if}
            </table>
          </td>
        </tr>
      </table>
      <br>
      {/foreach}
      {else}
      Sorry, no online payment option has been defined for this order type.<br>
      Please contact customer service for ordering details. 
      {/if}
    </td>
  </tr>
</table>

{/if}
{/if}
{/if}
