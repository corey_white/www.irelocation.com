{ $method->exe("account_admin","search_form") }
{ if ($method->result == FALSE) }
    { $block->display("core:method_error") }
{else}

<form name="search_form" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center">
                {translate module=account_admin}
                title_search 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_username 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_username" value="{$VAR.account_admin_username}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_email 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_email" value="{$VAR.account_admin_email}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_first_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_first_name" value="{$VAR.account_admin_first_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_middle_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_middle_name" value="{$VAR.account_admin_middle_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_last_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_last_name" value="{$VAR.account_admin_last_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_company 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_company" value="{$VAR.account_admin_company}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_address1 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_address1" value="{$VAR.account_admin_address1}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_address2 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_address2" value="{$VAR.account_admin_address2}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_city 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_city" value="{$VAR.account_admin_city}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_state 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_state" value="{$VAR.account_admin_state}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_zip 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_zip" value="{$VAR.account_admin_zip}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_misc 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_misc" value="{$VAR.account_admin_misc}" {if $account_admin_misc == true}class="form_field_error"{else}class="form_field"{/if}>
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_date_orig 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->calender_search("account_admin_date_orig", 
                    $VAR.account_admin_date_orig, "form_field", "") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_date_expire 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->calender_search("account_admin_date_expire", 
                    $VAR.account_admin_date_expire, "form_field", "") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_affiliate_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_affiliate_id" value="{$VAR.account_admin_affiliate_id}" {if $account_admin_affiliate_id == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_country_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "account_admin_country_id", 
                    "country", "name", "all", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_language_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu_files("", "account_admin_language_id", 
                    "all", "language", "", "_core.xml", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_currency_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "account_admin_currency_id", 
                    "currency", "name", "all", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_theme_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu_files("", "account_admin_theme_id", 
                    "all", "theme", "", ".user_theme", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_status 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->bool("account_admin_status", "all", 
                    "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    account_group
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "account_group", "group", "name", "all", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_title 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <select name="account_admin_title" class="form_menu">
                      <option value=""></option>
                      <option value="Mr"{if $VAR.account_admin_title == "Mr"} selected{/if}> 
                      {translate module=account_admin}
                      mr 
                      {/translate}
                      </option>
                      <option value="Mrs"{if $VAR.account_admin_title == "Mrs"} selected{/if}> 
                      {translate module=account_admin}
                      mrs 
                      {/translate}
                      </option>
                      <option value="Miss"{if $VAR.account_admin_title == "Miss"} selected{/if}> 
                      {translate module=account_admin}
                      miss 
                      {/translate}
                      </option>					  
                      <option value="Dr"{if $VAR.account_admin_title == "Dr"} selected{/if}> 
                      {translate module=account_admin}
                      dr 
                      {/translate}
                      </option>
                      <option value="Prof"{if $VAR.account_admin_title == "Prof"} selected{/if}> 
                      {translate module=account_admin}
                      prof 
                      {/translate}
                      </option>
                    </select>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate}
                    search_results_per 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" class="form_field" name="limit" size="5" value="{$account_admin_limit}">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate}
                    search_order_by 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <select class="form_menu" name="order_by">
                      {foreach from=$account_admin item=record}
                      <option value="{$record.field}"> 
                      {$record.translate}
                      </option>
                      {/foreach}
                    </select>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp;</td>
                  <td width="65%">&nbsp;</td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp;</td>
                  <td width="65%"> 
                    <input type="submit" name="Submit" value="{translate}search{/translate}" class="form_button">
                    <input type="hidden" name="_page" value="core:search">
                    <input type="hidden" name="_next_page_one" value="view">
                    <input type="hidden" name="_escape" value="Y">
                    <input type="hidden" name="module" value="account_admin">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp;</td>
                  <td width="65%">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
{/if}
