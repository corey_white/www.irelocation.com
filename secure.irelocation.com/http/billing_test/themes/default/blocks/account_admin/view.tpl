{ $method->exe("account_admin","view") } { if ($method->result == FALSE) }  { $block->display("core:method_error") } {else} 

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'account_admin';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$account item=account_admin} <a name="{$account_admin.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form id="update_form" name="update_form" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                {translate module=account_admin}
                title_view 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> </b>
                    <table width="100%" border="0" cellspacing="2" cellpadding="0" align="left"  class="body">
                      <tr class="row1"> 
                        <td width="18" height="18" valign="middle" align="center"><a href="?_page=invoice:add&invoice_account_id={$account_admin.id}"><img title=Add src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="18" height="18" valign="middle" align="center"><a href="?_page=core:search&module=invoice&invoice_account_id={$account_admin.id}&_next_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="30%" height="18">Invoices </td>
                        <td width="18" height="18">&nbsp;</td>
                        <td width="18" height="18"><a href="?_page=core:search&module=session&session_account_id={$account_admin.id}&_next_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="70%" height="18">Sessions </td>
                      </tr>
                      <tr class="row2"> 
                        <td width="18" valign="middle" align="center"><a href="?_page=login_log:add&iservice_account_id={$account_admin.id}"> 
                          </a></td>
                        <td width="18" valign="middle" align="center"><a href="?_page=core:search&module=service&service_account_id={$account_admin.id}&_next_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="30%">Services </td>
                        <td width="18">&nbsp;</td>
                        <td width="18"><a href="?_page=core:search&module=login_log&login_log_account_id={$account_admin.id}"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="70%">Login Logs </td>
                      </tr>
                      <tr class="row1"> 
                        <td width="18" valign="middle" align="center" height="18"><a href="?_page=account_billing:add&account_billing_account_id={$account_admin.id}"><img title=Add src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="18" valign="middle" align="center" height="18"><a href="?_page=core:search&module=account_billing&account_billing_account_id={$account_admin.id}&_next_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="30%" height="18">Billing Details</td>
						
                        { if $list->is_installed('ticket') }
						<td width="18" height="18"><a href="?_page=ticket:add&ticket_account_id={$account_admin.id}"><img title=Add src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="18" height="18"><a href="?_page=core:search&module=ticket&ticket_account_id={$account_admin.id}&_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="70%" height="18">Tickets </td>
						{/if}
						
                      </tr>
                      <tr class="row2"> 
                        <td width="18" valign="middle" align="center"><a href="?_page=discount:add&discount_avail_account_id={$account_admin.id}"><img title=Add src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="18" valign="middle" align="center"><a href="?_page=core:search&module=discount&discount_avail_account_id={$account_admin.id}&_next_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="30%">Discounts </td>
						
						{ if $list->is_installed('affiliate') }
                        <td width="18"><a href="?_page=affiliate:add&affiliate_account_id={$account_admin.id}"><img title=Add src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="18"><a href="?_page=core:search&module=affiliate&affiliate_account_id={$account_admin.id}&_page_one=view"><img title=View src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a></td>
                        <td width="70%">Affiliate </td>
						{/if}
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
			  
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
						  
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_date_orig 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_date_last 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_status 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->date_time($account_admin.date_orig)}
                  </td>
                  <td width="33%"> 
                    {$list->date_time($account_admin.date_last)}
                    <input type="hidden" name="account_admin_date_last" value="{$smarty.now}">
                  </td>
                  <td width="33%"> 
                    { $list->bool("account_admin_status", $account_admin.status, "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                    {if $account_admin.status != "1" }
                    &nbsp; <a href="javascript:NewWindow('blank','toolbar=no,status=yes,width=250,height=200','?_page=account:blank&_escape=1&do[]=account_admin:send_verify_email&id={$account_admin.id}');"><img src="themes/{$THEME_NAME}/images/icons/mail_16.gif" alt="{translate module=account_admin}resend_verify_email{/translate}" border="0" width="16" height="16"> 
                    </a> 
                    {/if}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_username 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_password 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_company 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <input type="text" name="account_admin_username" class="form_field" value="{$account_admin.username}">
                  </td>
                  <td width="33%"> 
                    <input type="text" name="_password" class="form_field">
                    &nbsp; <a href="javascript:NewWindow('blank','toolbar=no,status=yes,width=250,height=200','?_page=core:blank&_escape=1&do[]=account_admin:send_password_email&id={$account_admin.id}');"> 
                    <img title="E-mail Password Reminder" src="themes/{$THEME_NAME}/images/icons/mail_16.gif" alt="{translate module=account_admin}send_password_reset{/translate}" border="0" width="16" height="16"></a> 
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_company" class="form_field" value="{$account_admin.company}">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_first_name 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_middle_name 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_last_name 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <input type="text" name="account_admin_first_name" class="form_field" value="{$account_admin.first_name}">
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_middle_name" class="form_field" value="{$account_admin.middle_name}">
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_last_name" class="form_field" value="{$account_admin.last_name}">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_title
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_address1 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_address2 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%">
                    <select name="account_admin_title" class="form_menu" onChange="document.getElementById('update_form').submit()">
                      <option value="Mr"{if $account_admin.title == "Mr"} selected{/if}> 
                      {translate module=account_admin}
                      mr 
                      {/translate}
                      </option>
                      <option value="Mrs"{if $account_admin.title == "Mrs"} selected{/if}> 
                      {translate module=account_admin}
                      mrs 
                      {/translate}
                      </option>
                      <option value="Miss"{if $account_admin.title == "Miss"} selected{/if}> 
                      {translate module=account_admin}
                      miss 
                      {/translate}
                      </option>					  
                      <option value="Dr"{if $account_admin.title == "Dr"} selected{/if}> 
                      {translate module=account_admin}
                      dr 
                      {/translate}
                      </option>
                      <option value="Prof"{if $account_admin.title == "Prof"} selected{/if}> 
                      {translate module=account_admin}
                      prof 
                      {/translate}
                      </option>
                    </select>
                  </td>
                  <td width="33%">
                    <input type="text" name="account_admin_address1" class="form_field" value="{$account_admin.address1}">
                  </td>
                  <td width="33%">
                    <input type="text" name="account_admin_address2" class="form_field" value="{$account_admin.address2}">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_city 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_state 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_zip 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%">
                    <input type="text" name="account_admin_city" class="form_field" value="{$account_admin.city}">
                  </td>
                  <td width="33%">
				 	<input type="text" name="account_admin_state" class="form_field" value="{$account_admin.state}">
				  </td>
                  <td width="33%">
				  	<input type="text" name="account_admin_zip" class="form_field" value="{$account_admin.zip}">
				  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_email 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_email_html 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_theme_id 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <input type="text" name="account_admin_email" class="form_field" value="{$account_admin.email}">
                    <a href="?_page=account_admin:mail_one&mail_account_id={$account_admin.id}"><img title="E-mail user" src="themes/{$THEME_NAME}/images/icons/mail_16.gif" border="0" width="16" height="16"></a> 
                  </td>
                  <td width="33%">
                    { $list->bool("account_admin_email_type", $account_admin.email_type, "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                  </td>
                  <td width="33%">
                    { $list->menu_files("", "account_admin_theme_id", $account_admin.theme_id, "theme", "", ".user_theme", "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="66%"> <b> 
                    {translate module=account_admin}
                    field_country_id 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=account_admin}
                    field_currency_id 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    { $list->menu("no", "account_admin_country_id", "country", "name", $account_admin.country_id, "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                  </td>
                  <td width="33%"> 
                    { $list->menu("no", "account_admin_currency_id", "currency", "name", $account_admin.currency_id, "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b>
                    {translate module=account_admin}
                    field_language_id 
                    {/translate}
                    </b></td>
                  <td width="34%"> <b>
                    {translate module=account_admin}
                    field_date_expire 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
				    {if $list->is_installed('affiliate') }
                    {translate module=account_admin}
                    field_affiliate_id 
                    {/translate}
					{/if}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    { $list->menu_files("", "account_admin_language_id", $account_admin.language_id, "language", "", "_core.xml", "form_menu\" onChange=\"document.getElementById('update_form').submit()") }
                  </td>
                  <td width="34%">
                    { $list->calender_view("account_admin_date_expire", $account_admin.date_expire, "form_field", $account_admin.id) }
                  </td>
                  <td width="33%"> 
                    {if $list->is_installed('affiliate') }
                    { $list->menu("", "account_admin_affiliate_id", "affiliate", "id", $account_admin.affiliate_id, "form_menu\" onChange=\"document.getElementById('update_form').submit()", all) }
                    {/if}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate}
                    authorized_groups 
                    {/translate}
                    </b></td>
                  <td width="67%"> <b> 
                    {translate module=account_admin}
                    field_misc 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    { $list->select_groups($account_admin.groups,"groups","form_field","10", $account_admin.own_account) }
                  </td>
                  <td width="67%"> 
                    <textarea name="account_admin_misc" cols="60" rows="3" {if $account_admin_misc == true}class="form_field_error"{else}class="form_field"{/if}>{$account_admin.misc}</textarea>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          {if $account_admin.static_var}
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                {foreach from=$account_admin.static_var item=record}
                <tr valign="top"> 
                  <td width="33%" valign="top"> 
                    {$record.name}
                  </td>
                  <td width="67%"> 
                    {$record.html}
                  </td>
                </tr>
                {/foreach}
              </table>
            </td>
          </tr>
          {/if}
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row1" valign="middle" align="left"> 
                  <td width="33%" valign="top">
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                  <td width="67%" valign="top" align="right"> 
                    <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$account_admin.id}','{$VAR.id}');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <div align="center">
    <input type="hidden" name="_page" value="account_admin:view"> 
    <input type="hidden" name="_page_current" value="account_admin:view">
    <input type="hidden" name="account_admin_id" value="{$account_admin.id}">
    <input type="hidden" name="do[]" value="account_admin:update">
    <input type="hidden" name="id" value="{$VAR.id}">
    {if $smarty.const.SESS_ACCOUNT != $account_admin.id }
    <a href="{$URL}?page=account:account&id={$account_admin.id}&account_id={$account_admin.id}&do[]=account_admin:login&tid={$account_admin.theme_id}" target="_parent"> 
    Become This User</a> 
    {/if}
  </div>
</form>
  
<br>
{/foreach}
{/if}
