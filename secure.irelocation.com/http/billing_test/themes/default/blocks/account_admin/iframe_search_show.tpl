{ $block->display("core:top_clean") }

<div align="center"><br>
{$method->exe("account_admin","search_show")}
{if ($method->result == FALSE)}
    {$block->display("core:method_error")}
{else} 

  {literal}
  <script language="JavaScript">
    <!-- START
    	var module 		= 'account_admin'; 
    	var p 			= '&_page=account_admin:iframe_search_show&_escape=1&_escape_next=1&field={/literal}{$VAR.field}{literal}'; 
    	var IMAGE 		= '{/literal}{$NONSSL_IMAGE}{literal}';
    	var order 		= '{/literal}{$order}{literal}';
    	var sort1  		= '{/literal}{$sort}{literal}';
    	var search_id 	= '{/literal}{$search_id}{literal}';
    	var page 		= '{/literal}{$page}{literal}';
    	var pages		= '{/literal}{$pages}{literal}';
    	var results		= '{/literal}{$results}{literal}';
    	var limit 		= '{/literal}{$limit}{literal}';
    	record_arr = new Array ('{/literal}{$limit}{literal}');
    	var i = 0;	
    //  END -->
    </script>
  <SCRIPT SRC="themes/{/literal}{$THEME_NAME}{literal}/search.js"></SCRIPT>
  {/literal}
  <!-- SHOW THE SEARCH NAVIGATION MENU -->
</div>
<center><script language="JavaScript">document.write(search_nav_top());</script></center>

  
<!-- BEGIN THE RESULTS CONTENT AREA -->
<div id="search_results" onKeyPress="key_handler(event);"> 
  <table id="main1" width="97%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
    <form id="form1" name="form1" method="post" action="">
      <tr> 
        <td> 
          <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="3">
            <!-- DISPLAY THE SEARCH HEADING -->
            <tr valign="middle" align="center" class="table_heading"> 
              <td width="337" class="table_heading"> 
                {translate module=account_admin}field_last_name{/translate} 
              </td>
              <td width="352" class="table_heading">  
                {translate module=account_admin}field_first_name{/translate}  
              </td>
              <td width="299" class="table_heading"> 
			    {translate module=account_admin}field_username{/translate} 
              </td>
            </tr>
            <!-- LOOP THROUGH EACH RECORD -->
            {foreach from=$account item=record}
            <tr id="row{$record.id}" {if $VAR.field == ''}onClick="window.opener.popup_fill('{$record.id}|{$record.first_name} {$record.last_name}');window.close();"{else}onClick="window.opener.popup_fill_{$VAR.field}('{$record.id}|{$record.first_name} {$record.last_name}');window.close();"{/if}> 
              <td width="337"> &nbsp; 
                <input type="hidden" name="record{$record.id}" value="{$record.id}">
                {$record.last_name}
              </td>
              <td width="352"> &nbsp; 
                {$record.first_name}
              </td>
              <td width="299"> &nbsp; 
                {$record.username}
              </td>
            </tr>
            {literal}
            <script language="JavaScript">row_sel('{/literal}{$record.id}{literal}', 0, '{/literal}{$record._C}{literal}'); record_arr[i] = '{/literal}{$record.id}{literal}'; i++; </script>
            {/literal}
            {/foreach}
            <!-- END OF RESULT LOOP -->
          </table>
        </td>
      </tr>
    </form>
  </table>
  
<center>
    {/if}
    <br>
    <input title=Close type="image" src="themes/{$THEME_NAME}/images/icons/cancl_24.gif" onClick="window.close();" name="image">
  </center>
</div>
