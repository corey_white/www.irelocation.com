{ $block->display("core:top_clean") }

<!-- display the alert block -->
{if $alert} { $block->display("core:alert") } {/if}
					  
{ $method->exe("account_admin","search_form") }
{ if ($method->result == FALSE) }
    { $block->display("core:method_error") }
{else}

<form name="search_form" method="post" action="">
  
  <br>
  <table width=97% border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0" align="center">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center">
                {translate module=account_admin}
                title_search 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1" align="center">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_username 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_username" value="{$VAR.account_admin_username}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_email 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_email" value="{$VAR.account_admin_email}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_first_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_first_name" value="{$VAR.account_admin_first_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_middle_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_middle_name" value="{$VAR.account_admin_middle_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_last_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_last_name" value="{$VAR.account_admin_last_name}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_company 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_company" value="{$VAR.account_admin_company}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_address1 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_address1" value="{$VAR.account_admin_address1}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_address2 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_address2" value="{$VAR.account_admin_address2}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_city 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_city" value="{$VAR.account_admin_city}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_state 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_state" value="{$VAR.account_admin_state}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_zip 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_zip" value="{$VAR.account_admin_zip}" class="form_field">
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_misc 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_misc" value="{$VAR.account_admin_misc}" {if $account_admin_misc == true}class="form_field_error"{else}class="form_field"{/if}>
                    {translate}
                    search_partial 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_affiliate_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="account_admin_affiliate_id" value="{$VAR.account_admin_affiliate_id}" {if $account_admin_affiliate_id == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_admin}
                    field_country_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "account_admin_country_id", 
                    "country", "name", "all", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp;</td>
                  <td width="65%">&nbsp;</td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp;</td>
                  <td width="65%"> 
                    <input type="submit" name="Submit"  value="{translate}search{/translate}" class="form_button">
                    <input type="hidden" name="_page"   		value="core:search_iframe">
                    <input type="hidden" name="_next_page"   	value="iframe_search_show">
                    <input type="hidden" name="_escape" 		value="1">
                    <input type="hidden" name="_escape_next" 	value="1">
                    <input type="hidden" name="module"  		value="account_admin">
                    <input type="hidden" name="name_id1"  		value="field">
                    <input type="hidden" name="val_id1"   		value="{$VAR.field}">
                    <input type="hidden"  name="limit" size="5" value="20">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
{/if} 
