 
{if $smarty.const.GD == true}
<form name="form1" method="post" action="">
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width=100% border="0" cellspacing="1" cellpadding="0" align="center">
          <tr> 
            <td class="row1"> 
              <table width="100%" border="0" cellpadding="0" class="row1">
                <tr> 
                  <td><img src="{$URL}?_page=core:graph&_escape&graph_module=log_error&graph_range={$VAR.graph_range}&graph_start={$VAR.graph_start}&graph_extra={$VAR.graph_extra}&s={$SESS}&_log=1" name="GRAPH"> 
                    {literal}
                    <script language="JavaScript"><!--
		function reloadImage() {
			var now = new Date();
			if (document.images) 
			{{/literal}
				document.images.GRAPH.src = '{$URL}?_page=core:graph&_escape&graph_module=log_error&graph_range={$VAR.graph_range}&graph_start={$VAR.graph_start}&graph_extra={$VAR.graph_extra}&s={$SESS}&_log=1&__regenerate=' + now.getTime();
			}
			setTimeout('reloadImage()',60000);
		}
		setTimeout('reloadImage()',1);
		//--></script>
                  </td>
                </tr>
                <tr> 
                  <td>
                    <table width="415" border="0" cellpadding="0" class="row1" height="55" align="center">
                      <tr class="row1"> 
                        <td width="31%"> 
                          {translate}
                          stats_range 
                          {/translate}
                        </td>
                        <td width="28%"> 
                          { if $VAR.graph_range != "" && $VAR.graph_range != "year" || $VAR.graph_range == "" }
                          {translate}
                          stats_start 
                          {/translate}
                          {/if}
                        </td>
                        <td width="41%"> </td>
                      </tr>
                      <tr> 
                        <td width="31%"> 
                          <select name="graph_range" class="form_menu">
                            <option value="day"{if $VAR.graph_range == "day"} selected{/if}> 
                            {translate}
                            day 
                            {/translate}
                            </option>
                            <option value="week"{if $VAR.graph_range == "week"} selected{/if}> 
                            {translate}
                            week 
                            {/translate}
                            </option>
                            <option value="month"{if $VAR.graph_range == "month"} selected{/if}> 
                            {translate}
                            month 
                            {/translate}
                            </option>
                            <option value="year"{if $VAR.graph_range == "year"} selected{/if}> 
                            {translate}
                            year 
                            {/translate}
                            </option>
                          </select>
                        </td>
                        <td width="28%"> 
                          {if $VAR.graph_start == "" }
                          { assign var="_selected" value=$smarty.now|date_format:"%m" }
                          {else}
                          { assign var="_selected" value=$VAR.graph_start }
                          {/if}
                          { if $VAR.graph_range == "day" || $VAR.graph_range == ""}
                          <select name="graph_start" class="form_menu">
                            <option value="1"{if $VAR.graph_start == "1"}selected{/if}> 
                            {translate}
                            jan 
                            {/translate}
                            </option>
                            <option value="2"{if $_selected == "2"}selected{/if}> 
                            {translate}
                            feb 
                            {/translate}
                            </option>
                            <option value="3"{if $_selected == "3"}selected{/if}> 
                            {translate}
                            mar 
                            {/translate}
                            </option>
                            <option value="4"{if $_selected == "4"}selected{/if}> 
                            {translate}
                            apr 
                            {/translate}
                            </option>
                            <option value="5"{if $_selected == "5"}selected{/if}> 
                            {translate}
                            may 
                            {/translate}
                            </option>
                            <option value="6"{if $_selected == "6"}selected{/if}> 
                            {translate}
                            jun 
                            {/translate}
                            </option>
                            <option value="7"{if $_selected == "7"}selected{/if}> 
                            {translate}
                            jul 
                            {/translate}
                            </option>
                            <option value="8"{if $_selected == "8"}selected{/if}> 
                            {translate}
                            aug 
                            {/translate}
                            </option>
                            <option value="9"{if $_selected == "9"}selected{/if}> 
                            {translate}
                            oct 
                            {/translate}
                            </option>
                            <option value="10"{if $_selected == "10"}selected{/if}> 
                            {translate}
                            sep 
                            {/translate}
                            </option>
                            <option value="11"{if $_selected == "11"}selected{/if}> 
                            {translate}
                            nov 
                            {/translate}
                            </option>
                            <option value="12"{if $_selected == "12"}selected{/if}> 
                            {translate}
                            dec 
                            {/translate}
                            </option>
                          </select>
                          {/if}
                          { if $VAR.graph_range == "week" || $VAR.graph_range == "month" }
                          <select name="graph_start" class="form_menu">
                            <option value="2003"{if $VAR.graph_start == "2003"} selected{/if}>2003</option>
                            <option value="2004"{if $VAR.graph_start == "2004"} selected{/if}>2004</option>
                            <option value="2005"{if $VAR.graph_start == "2005"} selected{/if}>2005</option>
                            <option value="2006"{if $VAR.graph_start == "2006"} selected{/if}>2006</option>
                            <option value="2007"{if $VAR.graph_start == "2007"} selected{/if}>2007</option>
                            <option value="2007"{if $VAR.graph_start == "2008"} selected{/if}>2008</option>
                            <option value="2009"{if $VAR.graph_start == "2009"} selected{/if}>2009</option>
                            <option value="2010"{if $VAR.graph_start == "2010"} selected{/if}>2010</option>
                          </select>
                          {/if}
                        </td>
                        <td width="41%"> 
                          <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="_page" value="log_error:main">
  <input type="hidden" name="graph_module" value="log_error">
</form>
<br>
<br>
{/if}
<table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr> 
    <td> 
      <table width=100% border="0" cellspacing="1" cellpadding="0" align="center">
        <tr> 
          <td class="table_heading"> 
            <center>
              {translate module=log_error}
              menu 
              {/translate}
            </center>
          </td>
        </tr>
        <tr> 
          <td class="row1">
            <table width="100%" border="0" cellpadding="5" class="row1">
              <tr>
                <td>{translate module=log_error}help_file{/translate}</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
