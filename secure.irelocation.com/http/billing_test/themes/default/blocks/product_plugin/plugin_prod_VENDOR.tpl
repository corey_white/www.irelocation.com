{$list->unserial($product.prod_plugin_data, "plugin_data")} 
<table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
  <tr valign="top" class="row2"> 
    <td width="49%">&nbsp; </td>
    <td width="17%"> 
      {translate module=host_provision_plugin}
      enabled 
      {/translate}
    </td>
    <td width="34%"> 
      {translate module=host_provision_plugin}
      options 
      {/translate}
    </td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Categories Allowed</td>
    <td width="17%">--- </td>
    <td width="34%"> 
      <input type="text" name="product_prod_plugin_data[categories_allowed]" value="{$plugin_data.categories_allowed}" class="form_field" size="10">
      0-Unlimited </td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Zip Codes Allowed</td>
    <td width="17%"> ---</td>
    <td width="34%"> 
      <input type="text" name="product_prod_plugin_data[zip_allowed]" value="{$plugin_data.zip_allowed}" class="form_field" size="10">
      0-Unlimited</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> States Allowed</td>
    <td width="17%"> ---</td>
    <td width="34%"> 
      <input type="text" name="product_prod_plugin_data[states_allowed]" value="{$plugin_data.states_allowed}" class="form_field" size="10">
      0-Unlimited</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Maximum portfolio space</td>
    <td width="17%">--- </td>
    <td width="34%"> 
      <input type="text" name="product_prod_plugin_data[max_kb]" value="{$plugin_data.max_kb}" class="form_field" size="10">
      KB</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Allow e-mail?</td>
    <td width="17%"> 
      { $list->bool("product_prod_plugin_data[email_enabled]", $plugin_data.email_enabled, "form_menu") }
    </td>
    <td width="34%">---</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Allow website URL?</td>
    <td width="17%"> 
      { $list->bool("product_prod_plugin_data[url_enabled]", $plugin_data.url_enabled, "form_menu") }
    </td>
    <td width="34%">---</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Allow customer ratings?</td>
    <td width="17%"> 
      { $list->bool("product_prod_plugin_data[ratings_enabled]", $plugin_data.ratings_enabled, "form_menu") }
    </td>
    <td width="34%">---</td>
  </tr>
  <tr valign="top"> 
    <td width="49%"> Allow customer reviews?</td>
    <td width="17%"> 
      { $list->bool("product_prod_plugin_data[review_enabled]", $plugin_data.review_enabled, "form_menu") }
    </td>
    <td width="34%">---</td>
  </tr>
  <tr valign="top"> 
    <td width="49%">Allow national exposure?</td>
    <td width="17%">
      { $list->bool("product_prod_plugin_data[national_exposure]", $plugin_data.national_exposure, "form_menu") }
    </td>
    <td width="34%">--- </td>
  </tr>
</table>
