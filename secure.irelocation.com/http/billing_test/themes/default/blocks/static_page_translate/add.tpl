{literal}
<script type="text/javascript"> 
   var _editor_url = "includes/htmlarea/";
   var _editor_lang = "{/literal}{$smarty.const.SESS_LANGUAGE}{literal}";
</script>	
<script type="text/javascript" src="includes/htmlarea/htmlarea.js"></script>
{/literal}

<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
<form id="static_page_translate_add" name="static_page_translate_add" method="post" action="">
{$COOKIE_FORM}
  <table width=550 border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=static_page_translate}
                title_add 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="23%"> 
                    {translate module=static_page_translate}
                    field_static_page_id 
                    {/translate}
                  </td>
                  <td width="77%"> 
                    {if $VAR.id == ""}
                    { $list->menu("", "static_page_translate_static_page_id", "static_page", "name", $VAR.static_page_translate_static_page_id, "form_menu") }
                    {else}
                    { $list->menu("", "static_page_translate_static_page_id", "static_page", "name", $VAR.id, "form_menu") }
                    {/if}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="23%"> 
                    {translate module=static_page_translate}
                    field_language_id 
                    {/translate}
                  </td>
                  <td width="77%"> 
                    {if $VAR.static_page_translate_language_id == ""}
                    { $list->menu_files("", "static_page_translate_language_id", $smarty.const.DEFAULT_LANGUAGE, "language", "", "_core.xml", "form_menu") }
                    {else}
                    { $list->menu_files("", "static_page_translate_language_id", $VAR.static_page_translate_language_id, "language", "", "_core.xml", "form_menu") }
                    {/if}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="23%"> 
                    {translate module=static_page_translate}
                    field_title 
                    {/translate}
                  </td>
                  <td width="77%"> 
                    <input type="text" name="static_page_translate_title" value="{$VAR.static_page_translate_title}" {if $static_page_translate_title == true}class="form_field_error"{else}class="form_field"{/if} size="40">
                  </td>
                </tr>
              </table>
              <br>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="body">
                <tr> 
                  <td> <b> 
                    {translate module=static_page_translate}
                    field_body_intro 
                    {/translate}
                    </b> </td>
                </tr>
                <tr> 
                  <td> 
                    {htmlarea field=static_page_translate_body_intro height=250 width=550}
                    {$VAR.static_page_translate_body_intro}
                    {/htmlarea}
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td> <b> 
                    {translate module=static_page_translate}
                    field_body_full 
                    {/translate}
                    </b> </td>
                </tr>
                <tr> 
                  <td> 
                    {htmlarea field=static_page_translate_body_full height=350 width=550}
                    {$VAR.static_page_translate_body_full}
                    {/htmlarea}
                  </td>
                </tr>
              </table>
              <p><br>
                <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                <input type="hidden" name="_page" value="static_page_translate:view">
                <input type="hidden" name="_page_current" value="static_page_translate:add">
                <input type="hidden" name="do[]" value="static_page_translate:add">
                <input type="hidden" name="static_page_translate_date_last" value="{$smarty.now}">
                <input type="hidden" name="static_page_translate_date_orig" value="{$smarty.now}">
              </p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
