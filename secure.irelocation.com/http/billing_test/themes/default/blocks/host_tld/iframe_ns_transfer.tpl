{ $block->display("core:top_clean") }

<form name="domain" method="post" action="{$SSL_URL}" enctype="multipart/form-data">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="3" class="row1">
    <tr> 
      <td width="95%" class="body"> <b> <br>
        {translate module=host_tld}
        ns_transfer_instructions 
        {/translate}
        </b></td>
      <td width="5%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="95%"> 
        <p>
          <input type="text" id="domain" name="domain" class="form_field" maxlength="128" size="22" onchange="parent.document.getElementById('domain_name').value = this.value;">
          . 
          <input type="text" id="tld"    name="tld"	 class="form_field" size="5" maxlength="7"    onchange="parent.document.getElementById('domain_tld').value = this.value;">
        </p>
        <p>&nbsp;</p>
        </td>
      <td width="5%">&nbsp; </td>
    </tr>
  </table>
  </form> 
  <script language="JavaScript">  
   document.domain.domain.focus(); 
  </script>  
</body>
</html>
