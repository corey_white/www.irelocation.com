
<table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
  <tr valign="middle" class="row2"> 
    <td width="96%"> <b> </b> 
      <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
        <tr> 
          <td width="70%" valign="middle"><b> 
            {translate module=host_tld}
            domain_transfer 
            {/translate}
            </b></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr valign="top"> 
    <td width="96%">  
      <form name="domain" method="post" action="javascript:search();" enctype="multipart/form-data" onSubmit="search();">
        {literal}
        <script language="JavaScript">
    <!-- START
 	
	function domainUpdate(domain,tld,type)
	{
		document.getElementById("domain_name").value   = domain;
		document.getElementById("domain_tld").value    = tld; 
	}
 	
	function cartAdd(addtype)
	{ 
		var domain =  document.getElementById("domain_name").value;
		var tld    =  document.getElementById("domain_tld").value;
		document.location = '?_page=cart:cart&do[]=cart:add&host_type=transfer&domain_name='+domain+'&domain_tld='+tld;
	} 
	
    //  END -->
    </script>
        {/literal}
        
        <input type="hidden"  id="domain_name"    name="domain_name"    value="0">
        <input type="hidden"  id="domain_tld"     name="domain_tld"     value="0">
        <input type="hidden"  id="domain_option"  name="domain_option"  value="0">
        <input type="hidden"  name="_page"        value="cart:cart">
        <table width="100%" border="0" cellspacing="0" cellpadding="3" class="row1">
          <tr> 
            <td width="95%"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr> 
                  <td width="13%"> 
                    <input type="text" id="domain" name="domain" class="form_field" maxlength="128" size="22" onChange="parent.document.getElementById('domain_name').value = this.value;">
                  </td>
                  <td width="5%"> 
                    <select name="tld" onChange="parent.document.getElementById('domain_tld').value = this.value;" class="form_menu">
                      { if $list->smarty_array("host_tld", "name", "", "tld") }
                      {foreach from=$tld item=tld}
                      <option value="{$tld.name}"> 
                      {$tld.name}
                      </option>
                      {/foreach}
                      {/if}
                    </select>
                  </td>
                  <td width="5%"><a href="javascript:search();"><img src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a><b></b></td>
                  <td width="77%"><b> 
                    {literal}
                    <script language="javascript"> 
			  
			  function search()
			  {
				document.getElementById("search").style.display='block';
				
				// hide other elements:
				document.getElementById("instructions").style.display='none';
				document.getElementById("available").style.display='none';
				document.getElementById("unavailable").style.display='none';
				
				// do the search:
				var domain = document.domain.domain.value;
				var tld = document.domain.tld.value;
				showIFrame('iframeDomainSearch',0,0,'?_page=host_tld:whois_transfer&_escape=1&tld='+tld+'&domain='+domain);
				
				// set the hidden values in the parent page...
				var domain = '0';
				var tld    = '0'; 
				var type   = 'transfer';
				window.parent.domainUpdate(domain,tld,type);							
			  }

			  function available()
			  {
				document.getElementById("available").style.display='block';
				
				// hide other elements
				document.getElementById("search").style.display='none';
				document.getElementById("unavailable").style.display='none'; 
				
				// set the hidden values in the parent page...
				var domain = document.domain.domain.value;
				var tld    = document.domain.tld.value;		
				var type   = 'transfer';
				window.parent.domainUpdate(domain,tld,type);
			  }
			  
			  function unavailable()
			  {
				document.getElementById("unavailable").style.display='block';
				
				// hide other elements
				document.getElementById("search").style.display='none';
				document.getElementById("available").style.display='none';
				
				// set the hidden values in the parent page...
				var domain = '0';
				var tld    = '0'; 						
				var type   = 'transfer';
				window.parent.domainUpdate(domain,tld,type);				
			  }
			  			  		 
			</script>
                    {/literal}
                    </b></td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="3">
                <tr> 
                  <td class="row1"> 
                    <div id="search" style="display:none"><b> 
                      {translate module=host_tld}
                      transfer_searching 
                      {/translate}
                      </b></div>
                    <div id="unavailable" style="display:none"><b> 
                      {translate module=host_tld}
                      transfer_unavail 
                      {/translate}
                      </b></div>
                    <div id="available" style="display:none"> <b> 
                      {translate module=host_tld}
                      transfer_avail_s 
                      {/translate}</b><br><br>
                      <a href="javascript:cartAdd()"> 
					  <img src="themes/{$THEME_NAME}/images/icons/cart_16.gif" border="0">
                      {translate module=host_tld}
                      transfer_cart 
                      {/translate}
                      </a> </div> 
                    <div id="instructions"> <b> 
                      {translate module=host_tld}
                      register_instructions_s 
                      {/translate}
                      </b> </div> 					  
                  </td>
                </tr>
                <tr> 
                  <td width="13%"> </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <iframe id="iframeDomainSearch" style="border:0px; width:0px; height:0px;" scrolling="no"   allowtransparency="true" frameborder="0"></iframe> 
      </form>
      
    </td>
  </tr>
</table>
