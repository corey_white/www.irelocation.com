{ $block->display("core:top_clean") }

<form name="domain" method="post" action="javascript:search();" enctype="multipart/form-data" onsubmit="search();">
  
  <input type="hidden" name="_page" value="host_template:if">
  <input type="hidden" name="_page2" value="currency:view">
  <table width="100%" border="0" cellspacing="0" cellpadding="3" class="row1">
    <tr> 
      <td width="95%" class="body"> <b> <br>
        {translate module=host_tld}
        register_instructions 
        {/translate}
        </b></td>
    </tr>
    <tr> 
      <td width="95%">
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr> 
            <td width="13%"> 
              <input type="text" id="domain" name="domain" class="form_field" maxlength="128" size="22" onChange="parent.document.getElementById('domain_name').value = this.value;">
            </td>
            <td width="5%"> 
              <select name="tld" onChange="parent.document.getElementById('domain_tld').value = this.value;" class="form_menu">
                { if $list->smarty_array("host_tld", "name", "", "tld") }
                {foreach from=$tld item=tld}
                <option value="{$tld.name}"> 
                {$tld.name}
                </option>
                {/foreach}
                {/if}
              </select>
            </td>
            <td width="5%"><a href="javascript:search();"><img src="themes/{$THEME_NAME}/images/icons/srch_16.gif" border="0" width="16" height="16"></a><b></b></td>
            <td width="77%"><b>
              {literal}
              <script language="javascript"> 
			  
			  function search()
			  {
				document.getElementById("search").style.display='block';
				
				// hide other elements:
				document.getElementById("available").style.display='none';
				document.getElementById("unavailable").style.display='none';
				
				// do the search:
				var domain = document.domain.domain.value;
				var tld = document.domain.tld.value;
				showIFrame('iframeDomainSearch',0,0,'?_page=host_tld:whois&_escape=1&tld='+tld+'&domain='+domain);
				
				// set the hidden values in the parent page...
				var domain = '0';
				var tld    = '0'; 
				var type   = 'register';
				window.parent.domainUpdate(domain,tld,type);							
			  }

			  function available()
			  {
				document.getElementById("available").style.display='block';
				
				// hide other elements
				document.getElementById("search").style.display='none';
				document.getElementById("unavailable").style.display='none'; 
				
				// set the hidden values in the parent page...
				var domain = document.domain.domain.value;
				var tld    = document.domain.tld.value; 
				var type   = 'register';
				window.parent.domainUpdate(domain,tld,type);
			  }
			  
			  function unavailable()
			  {
				document.getElementById("unavailable").style.display='block';
				
				// hide other elements
				document.getElementById("search").style.display='none';
				document.getElementById("available").style.display='none';
				
				// set the hidden values in the parent page...
				var domain = '0';
				var tld    = '0'; 
				var type   = 'register';
				window.parent.domainUpdate(domain,tld,type);				
			  }
			  			  		 
			</script>
              {/literal}
              </b></td>
          </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="3">
          <tr> 
            <td class="body"> 
			  
              <DIV id="search" style="display:none"><b> 
                {translate module=host_tld}
                searching 
                {/translate}
                </b></DIV>
              <DIV id="unavailable" style="display:none"><b> 
                {translate module=host_tld}
                domain_unavail 
                {/translate}
                </b></DIV>
              <DIV id="available" style="display:none"> <b> 
                {translate module=host_tld}
                domain_avail 
                {/translate}
                </b> </DIV>
			  <DIV id="none" style="display:block">&nbsp;</DIV>
            </td>
          </tr>
          <tr> 
            <td width="13%"> 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <iframe id="iframeDomainSearch" style="border:0px; width:0px; height:0px;" scrolling="no"   ALLOWTRANSPARENCY="true" frameborder="0"></iframe> 
  </form> 
  </body>
</html>
