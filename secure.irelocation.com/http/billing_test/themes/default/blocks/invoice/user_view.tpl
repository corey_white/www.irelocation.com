{if $SESS_LOGGED == "1" }
{ $method->exe("invoice","user_view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}
{popup_init src="$SSL_URL/includes/overlib/overlib.js"}

{literal}
    <!-- Define the update delete function -->
    <script language="JavaScript">
    <!-- START
        var module = 'invoice';
    	var locations = '{/literal}{$VAR.module_id}{literal}';
    	if (locations != "")
    	{
    		refresh(0,'#'+locations)
    	}
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$invoice item=invoice}
<a name="{$invoice.id}"></a> 
<!-- Display the field validation -->
{if $form_validation}
{ $block->display("core:alert_fields") }
{/if}
<!-- Display each record -->
{ if $invoice.billing_status == "0" }
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr> 
    <td> 
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top"> 
          <td width="65%" class="table_heading"> 
            <center>
              {translate module=invoice}
              pay_invoice 
              {/translate}
            </center>
          </td>
        </tr>
		{* show checkout/payment plugin details *}
        <tr valign="top"> 
          <td width="65%" class="row1">  
            <table width="100%" border="0" cellspacing="0" cellpadding="2" class="body">
              <tr> 
                <td valign="top" width="67%"> 
                  {if $checkoutoptions}
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="row1">
                    <tr> 
                      <td><b>This invoice is currently due, please select from 
                        the payment options below to pay the balance due.</b></td>
                    </tr>
                  </table>
				  <center>
				  	<iframe id="iframeCheckout" style="border:0px; width:0px; height:0px;" scrolling="auto" allowtransparency="true" frameborder="0" SRC="themes/{$THEME_NAME}/IEFrameWarningBypass.htm"></iframe> 
				  </center>				  
                  {foreach from=$checkoutoptions item=checkout key=key}
                  <b><u> 
                  {literal}
                  <script language="JavaScript"> 
					function changeCheckoutOption(option)  {  
					var url ='{/literal}{$SSL_URL}{literal}?_page=invoice:checkoutoption&option='+option+'&_escape=1&invoice_id={/literal}{$invoice.id}{literal}'; 
					showIFrame('iframeCheckout',0,0,url);
					document.getElementById("checkout_option_"+option).checked = "1";
					}
					
					function getCheckoutOption() {
						return getElementById("checkout_option").value;
					} 
	  			  </script>
                  {/literal}
                  </u></b> 
                  <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                    <tr valign="top" class="row2"> 
                      <td id="checkout_{$checkout.fields.id}"
						onMouseOver="class_change('checkout_{$checkout.fields.id}', 'row1');"
						onMouseOut ="class_change('checkout_{$checkout.fields.id}',  'row2');"		  
		    			onClick="changeCheckoutOption({$checkout.fields.id})"> <b><u> 
                        <input type="radio" id="checkout_option_{$checkout.fields.id}" name="checkout_option" value="{$checkout.fields.id}">
                        {$checkout.fields.name}
                        </u> </b></td>
                    </tr>
                    <tr valign="top" class="row1"> 
                      <td width="50%" valign="middle"> 
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
                          <tr> 
                            <td width="63%"> 
                              {$checkout.fields.description}
                            </td>
                          </tr>
                          {foreach from=$discount item=discount}
                          {if $discount.total > 0}
                          {/if}
                          {/foreach}
                          {if $tax != false}
                          {/if}
                        </table>
                      </td>
                    </tr>
                  </table>
                  <b>
                  {/foreach}
                  {else}
                  </b> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="5" class="row1">
                    <tr>
                      <td><b>Sorry, no online payment option has been defined 
                        for this order type.<br>
                        Please <a href="?_page=staff:staff">contact</a> customer 
                        service for payment instructions.</b></td>
                    </tr>
                  </table>
                  <b>
                  {/if}
                  </b> </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<br>
{/if} 

		   
<form name="invoice_view" method="post" action="">
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=invoice}
                title_view 
                {/translate}
                { $invoice.id }
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_date_orig 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_date_last 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_due_date 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->date_time($invoice.date_orig)}
                  </td>
                  <td width="33%"> 
                    {$list->date_time($invoice.date_last)}
                    <input type="hidden" name="invoice_date_last" value="{$smarty.now}">
                  </td>
                  <td width="33%"> 
                    {$list->date($invoice.due_date)}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_process_status 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> <font color="#990000">
                    {translate module=invoice}
                    field_billing_status 
                    {/translate}
                    </font></b></td>
                  <td width="33%"> <b> <a href="?_page=invoice:pdf&id={$invoice.id}&_escape=true" target="_blank"> 
                    {translate module=invoice}
                    print_invoice 
                    {/translate}
                    </a> </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
				  {if $invoice.process_status == 1}
				  	{translate}true{/translate}
				  {else}
				  	{translate}false{/translate}
				  {/if}  
                  </td>
                  <td width="33%"> <b>
                    {if $invoice.billing_status == 1}
                    {translate}
                    true
                    {/translate}
                    {else}
                    {translate}
                    false
                    {/translate}
                    {/if}
                    </b> </td>
                  <td width="33%"> <a href="?_page=invoice:pdf&id={$invoice.id}&_escape=true" target="_blank"><img src="themes/{$THEME_NAME}/images/icons/print_16.gif" border="0" alt="E-mail User"></a></td>
                </tr>
              </table>
            </td>
          </tr>
          {* show discount details *}
          {if $invoice.discount_arr != '' && $invoice.discount_amt > 0}
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="25%"> <b> </b></td>
                  <td width="8%">&nbsp; </td>
                  <td width="41%"><b> 
                    {translate module=invoice}
                    field_discount_amt 
                    {/translate}
                    </b></td>
                  <td width="26%"> 
                    {assign var=discount_details value=$invoice.discount_popup}
                    <a href="#" {popup fgcolor=FCFCFC sticky=true caption="Discount Details" text="$discount_details" snapx=1 snapy=1}> 
                    {$list->format_currency($invoice.discount_amt, $invoice.billed_currency_id)}
                    </a> </td>
                </tr>
              </table>
            </td>
          </tr> 
          {/if}
		  
		  <!-- billing details -->
		  {assign var=cc_user value=true}
		  {* show checkout/payment plugin details *}
          {if $invoice.checkout_plugin_id != '0'}
          {assign var=sql1 value=" AND id='"}
          {assign var=sql2 value="' "}
          {assign var=sql3 value=$invoice.checkout_plugin_id}
          {assign var=sql  value=$sql1$sql3$sql2}
          {if $list->smarty_array("checkout", "checkout_plugin", $sql, "checkout") }
          {assign var=checkout_plugin value=$checkout[0].checkout_plugin}           
              {assign var="ablock" 	value="checkout_plugin:plugin_inv_"}
              {assign var="blockfile" value="$ablock$checkout_plugin"}
              {$block->display($blockfile)} 
          {/if}
          {/if}          
		  <!-- end billing details -->
		  
          <tr valign="top"> 
            <td width="65%" class="row1" height="49"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_tax_amt 
                    {/translate}
                    </b></td>
                  <td width="66%"><b> 
                    {translate module=invoice}
                    field_tax_id 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="33%"> 
                    {$list->format_currency($invoice.tax_amt, $invoice.billed_currency_id)}
                  </td>
                  <td width="66%"> 
                    { $list->menu("no", "invoice_tax_id", "tax", "description", $invoice.tax_id, "form_menu\" disabled") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_total_amt 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_billed_amt 
                    {/translate}
                    </b></td>
                  <td width="33%"> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->format_currency($invoice.total_amt, $invoice.billed_currency_id)}
                  </td>
                  <td width="33%"> 
                    {$list->format_currency($invoice.billed_amt, $invoice.billed_currency_id)}
                  </td>
                  <td width="33%">&nbsp; </td>
                </tr>
              </table>
            </td>
          </tr>
           
          <tr valign="top" class="table_background"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=invoice}
                products_ordered 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <!-- Loop through each invoice item record -->
              {foreach from=$cart item=cart}
              <br>
              {if $cart.item_type == "2"}
              <!-- Show domain -->
              <table width="97%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
                <tr> 
                  <td> 
                    <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr> 
                        <td width="70%" class="row2" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                            <tr> 
                              <td width="67%" class="row2"><b> </b> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                                  <tr> 
                                    <td width="51%"><b> <u> 
                                      {$cart.domain_name|upper}
                                      . 
                                      {$cart.domain_tld|upper}
                                      </u> </b></td>
                                    <td width="37%">&nbsp;</td>
                                    <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a><a href="?_page=product:details&id={$cart.product_id}"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp;<b> </b> 
                                {if $cart.sku == "DOMAIN-REGISTER"}
                                {translate module=cart}
                                register 
                                {/translate}
                                {elseif $cart.sku == "DOMAIN-TRANSFER"}
                                {translate module=cart}
                                transfer 
                                {/translate}
                                {elseif $cart.sku == "DOMAIN-PARK"}
                                {translate module=cart}
                                park 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            {if $cart.cart_type == "1"}
                            {if $cart.host_type == "ns_transfer"}
                            {/if}
                            {if $cart.host_type == "ip"}
                            {/if}
                            {/if}
                          </table>
                        </td>
                        <td width="30%" class="row1" valign="top" align="right"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                            <tr> 
                              <td width="43%">Base Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_base_price_{$cart_assoc.id}"> 
                                  <div id="def_base_price_{$cart.id}"> 
                                    {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                  </div>
                                  <div id="base_price_{$cart.id}"></div>
                                </div>
                                <div id="base_price_{$cart_assoc.id}"></div>
                              </td>
                            </tr>
                          </table>
                          {if $cart.sku != 'DOMAIN-PARK'}
                          <select id="quantity_{$cart.id}" class="form_menu" disabled name="select">
                            <option value="">
                            {$cart.domain_term}
                            Year</option>
                          </select>
                          {/if}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              {else}
              <!-- Show product -->
              <table width="97%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
                <tr> 
                  <td> 
                    <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr> 
                        <td width="70%" class="row2" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                            <tr> 
                              <td width="67%" class="row2"><b> </b> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                                  <tr> 
                                    <td width="51%"><b> 
                                      {if $list->translate("product_translate","name", "product_id", $cart.product_id, "translate_product")}
                                      {/if}
                                      <u> 
                                      {$translate_product.name}
                                      </u> </b></td>
                                    <td width="35%"><b>&nbsp;</b>( <a href="?_page=product:details&id={$cart.product_id}"> 
                                      {$cart.sku}
                                      </a> ) &nbsp; </td>
                                    <td width="14%" align="right"> 
                                      {if $cart.attribute_popup != ""}
                                      <a href="#" {popup fgcolor=FCFCFC sticky=true width="300" caption="Item Attributes" text=$cart.attribute_popup snapx=1 snapy=1}> 
                                      <img src="themes/{$THEME_NAME}/images/icons/edit_16.gif" border="0" width="16" height="16"> 
                                      </a> 
                                      {/if}
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp; 
                                {if $cart.price_type == "0"}
                                {translate module=cart}
                                price_type_one 
                                {/translate}
                                {/if}
                                {if $cart.price_type == "1"}
                                {translate module=cart}
                                price_type_recurr 
                                {/translate}
                                {/if}
                                {if $cart.price_type == "2"}
                                {translate module=cart}
                                price_type_trial 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp; 
                                {if $cart.price_type == "1"}
                                {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                {if $cart.recurring_schedule == "0" }
                                {translate module=cart}
                                recurr_week 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "1" }
                                {translate module=cart}
                                recurr_month 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "2" }
                                {translate module=cart}
                                recurr_quarter 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "3" }
                                {translate module=cart}
                                recurr_semianual 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "4" }
                                {translate module=cart}
                                recurr_anual 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "5" }
                                {translate module=cart}
                                recurr_twoyear 
                                {/translate}
                                {/if}
                                &nbsp;&nbsp; + &nbsp; 
                                {$list->format_currency($cart.price_setup, $invoice.billed_currency_id)}
                                {translate module=cart}
                                setup 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            {if $cart.item_type == "1"}
                            {if $cart.domain_type == "ns_transfer"}
                            <tr> 
                              <td width="67%">&nbsp;&nbsp; 
                                {translate module=cart}
                                host_type_domain 
                                {/translate}
                                - <u> 
                                {$cart.domain_name}
                                .
                                {$cart.domain_tld}
                                </u> </td>
                            </tr>
                            {/if}
                            {if $cart.domain_type == "ip"}
                            <tr> 
                              <td width="67%">&nbsp;&nbsp; 
                                {translate module=cart}
                                host_type_ip 
                                {/translate}
                              </td>
                            </tr>
                            {/if}
                            {/if}
                          </table>
                        </td>
                        <td width="30%" class="row1" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                            <tr> 
                              <td width="43%">Base Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_base_price_{$cart.id}"> 
                                  {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                </div>
                                <div id="base_price_{$cart.id}"></div>
                              </td>
                            </tr>
                            <tr> 
                              <td width="43%">Setup Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_setup_price_{$cart.id}"> 
                                  {$list->format_currency($cart.price_setup, $invoice.billed_currency_id)}
                                </div>
                                <div id="setup_price_{$cart.id}"></div>
                              </td>
                            </tr>
                            <tr> 
                              <td width="43%"> 
                                {translate module=cart}
                                quantity 
                                {/translate}
                              </td>
                              <td width="57%" valign="middle" align="right"> 
                                {$cart.quantity}
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              {/if}
              {/foreach}
              <br>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <center>
  </center>
</form>
 
{/foreach}
{/if}
{else}
{ $block->display("account:login") }
{/if}
