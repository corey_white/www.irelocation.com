{ $method->exe("invoice","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

{popup_init src="$URL/includes/overlib/overlib.js"}
 
{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'invoice';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
		
		function approveInvoice(id,status) 
		{
			if(status.value == '1') {
				document.location = '?_page=invoice:view&id='+id+'&do[]=invoice:approveInvoice';
			} else {
				document.location = '?_page=invoice:view&id='+id+'&do[]=invoice:voidInvoice';
			}
		} 
    </script>
{/literal}
 

<!-- Loop through each record -->
{foreach from=$invoice item=invoice} <a name="{$invoice.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form id="invoice_view" name="invoice_view" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=invoice}
                title_view 
                {/translate}
                { $invoice.id } 
                 
</center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_date_orig 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_date_last 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_due_date 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->date_time($invoice.date_orig)}
                  </td>
                  <td width="33%"> 
                    {$list->date_time($invoice.date_last)}
                    <input type="hidden" name="invoice_date_last" value="{$smarty.now}">
                  </td>
                  <td width="33%"> 
                    {if $invoice.billing_status != 1}
                    { $list->calender_view("invoice_due_date", $invoice.due_date, "form_field", $invoice.id) }
                    {else}
                    {$list->date($invoice.due_date)}
                    {/if}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_process_status 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_billing_status 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_print_status 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
					{literal}<script language="JavaScript"> 
							function approveInvoice(status)  {
								var id = {/literal}{$invoice.id}{literal}
								if(status.value == '1') {
									document.location = '?_page=invoice:view&id='+id+'&do[]=invoice:approveInvoice';
								} else {
									document.location = '?_page=invoice:view&id='+id+'&do[]=invoice:voidInvoice';
								}
							} 
					</script>{/literal}				  
                    { $list->bool("invoice_process_status", $invoice.process_status, "form_menu\" onChange=\"javascript:approveInvoice(this)")  }
                  </td>
                  <td width="33%"> 
                    { $list->bool("invoice_billing_status", $invoice.billing_status, "form_menu\" onChange=\"document.getElementById('invoice_view').submit()")  }
                  </td>
                  <td width="33%"> 
                    { $list->bool("invoice_print_status", $invoice.print_status, "form_menu\" onChange=\"document.getElementById('invoice_view').submit()") }
                    <a href="?_page=invoice:pdf&id={$invoice.id}&_escape=true" target="_blank"> 
                    </a> <a href="?_page=account_admin:mail_one&mail_account_id={$invoice.account_id}"> 
                    </a> </td>
                </tr>
              </table>
            </td>
          </tr>
          {* show affiliate details *}
          {if $list->is_installed("affiliate") }
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="17%"> <b> 
                    {translate module=invoice}
                    field_affiliate_id 
                    {/translate}:</b></td>
                  <td width="16%"> 
				    {$list->menu("no", "invoice_affiliate_id", "affiliate", "id", $invoice.affiliate_id, "form_menu\" onChange=\"document.getElementById('invoice_view').submit()", all) }
                    {if $invoice.affiliate_id != ""}
					{assign var=affiliate_details value=$invoice.affiliate_popup}
                    <a href="?_page=affiliate:view&id={$invoice.affiliate_id}" {popup fgcolor=FCFCFC sticky=false width="300" caption="Affiliate Details" text="$affiliate_details" snapx=1 snapy=1}> 
                    [?]
                    </a> 
					{/if}
					</td>
                  <td width="34%"><b> 
                    {translate module=invoice}
                    affiliate_commissions 
                    {/translate}
                    </b></td>
                  <td width="33%"> 
                    {if $invoice.affiliate_commissions > 0}
                    {$list->format_currency($invoice.affiliate_commissions, '')}
                    {else}
                    ---- 
                    {/if}
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          {/if}
          {* show discount details *}
          {if $invoice.discount_arr != '' && $invoice.discount_amt > 0}
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"><b> 
                    {translate module=invoice}
                    field_discount_amt 
                    {/translate}
                    </b></td>
                  <td width="66%"> 
                    {assign var=discount_details value=$invoice.discount_popup}
                    <a href="#" {popup fgcolor=FCFCFC sticky=true caption="Discount Details" text="$discount_details" snapx=1 snapy=1}> 
                    {$list->format_currency($invoice.discount_amt, $invoice.billed_currency_id)}
                    </a> </td>
                </tr>
              </table>
            </td>
          </tr>
          {/if}

		  
		  {if $invoice.tax_amt > 0}
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_tax_amt 
                    {/translate}
                    </b></td>
                  <td width="66%"><b> 
                    {translate module=invoice}
                    field_tax_id 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="33%"> 
                    {$list->format_currency($invoice.tax_amt, $invoice.billed_currency_id)}
                  </td>
                  <td width="66%"> 
                    { $list->menu("no", "invoice_tax_id", "tax", "description", $invoice.tax_id, "form_menu\" disabled") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
		  {/if}
		  
		  
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_total_amt 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b> 
                    {translate module=invoice}
                    field_billed_amt 
                    {/translate}
                    </b></td>
                  <td width="33%"> <b>
				  {if $invoice.billed_currency_id != $invoice.actual_billed_currency_id} 
                    {translate module=invoice}
                    field_actual_billed_amt 
                    {/translate}
                    </b>
				  {/if}
				  </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    {$list->format_currency($invoice.total_amt, $invoice.billed_currency_id)}
                  </td>
                  <td width="33%"> 
				  	{$list->format_currency($invoice.billed_amt,$invoice.billed_currency_id)}
                  </td>
                  <td width="33%">
				   {if $invoice.billed_currency_id != $invoice.actual_billed_currency_id}
				   {$list->format_currency($invoice.actual_billed_amt,$invoice.actual_billed_currency_id)}  
				   {/if}
                  </td>
                </tr>
              </table>
            </td>
          </tr> 
		  
          { if $invoice.billing_status == "0" }
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="141"> <b> 
                    {translate module=invoice}
                    field_notice_count 
                    {/translate}
                    </b> </td>
                  <td width="103" valign="middle"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
                      <tr> 
                        <td> 
                          { $invoice.notice_count }
                        </td>
                        <td><a href="?_page=invoice:view&account_id={$invoice.account_id}&id={$invoice.id}&ids={$VAR.ids}&do[]=invoice:resend"><img src="themes/{$THEME_NAME}/images/icons/mail_16.gif" border="0" width="16" height="16" alt="Resend Invoice"></a></td>
                      </tr>
                    </table>
                  </td>
                  <td width="287"> <b> 
                    {translate module=invoice}
                    field_notice_next_date 
                    {/translate}
                    </b> </td>
                  <td width="212"> <b> 
                    { $list->calender_view("invoice_notice_next_date", $invoice.notice_next_date, "form_field", $invoice.id) }
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="141"> <b> 
                    {translate module=invoice}
                    field_notice_max 
                    {/translate}
                    </b> </td>
                  <td width="103"> 
                    <input type="text" name="invoice_notice_max" value="{$invoice.notice_max}" class="form_field" size="2">
                  </td>
                  <td width="287"> <b> 
                    {translate module=invoice}
                    field_grace_period 
                    {/translate}
                    </b> </td>
                  <td width="212"> 
                    <input type="text" name="invoice_grace_period" value="{$invoice.grace_period}" class="form_field" size="2">
                  </td>
                </tr>
                <tr valign="top">
                  <td width="141"><b> </b></td>
                  <td width="103">&nbsp; </td>
                  <td width="287"><b>
                    {translate module=invoice}
                    field_suspend_billing 
                    {/translate}
                    </b></td>
                  <td width="212">
                    { $list->bool("invoice_suspend_billing", $invoice.suspend_billing, "form_menu\" onChange=\"javascript:document.getElementById('invoice_view').submit()")  }
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>
          {/if}
		  
		  <!-- billing details -->
          {* show checkout/payment plugin details *}
          {if $invoice.checkout_plugin_id != '0'}
          {assign var=sql1 value=" AND id='"}
          {assign var=sql2 value="' "}
          {assign var=sql3 value=$invoice.checkout_plugin_id}
          {assign var=sql  value=$sql1$sql3$sql2}
          {if $list->smarty_array("checkout", "checkout_plugin", $sql, "checkout") }
          {assign var=checkout_plugin value=$checkout[0].checkout_plugin}  
              {assign var="ablock" 	value="checkout_plugin:plugin_inv_"}
              {assign var="blockfile" value="$ablock$checkout_plugin"}
              {$block->display($blockfile)} 
          {/if}
          {/if} 
		  <!-- end billing details -->
		  		  
          <tr valign="top" class="table_background"> 
            <td width="65%" class="table_heading">
              <center>
                Items Purchased 
              </center>
            </td>
          </tr>         
		  <tr valign="top"> 
            <td width="65%" class="row1"> 
			
			
          	<!-- Loop through each invoice item record -->
          	{foreach from=$cart item=cart}
			
					
			<br>
              {if $cart.item_type == "2"}
              <!-- Show domain -->
              <table width="97%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
                <tr> 
                  <td> 
                    <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr> 
                        <td width="70%" class="row2" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                            <tr> 
                              <td width="67%" class="row2"><b> </b> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                                  <tr> 
                                    <td width="51%"><b> <u> 
                                      {$cart.domain_name|upper}
                                      .
                                      {$cart.domain_tld|upper}
                                      </u> </b></td>
                                    <td width="37%">&nbsp;</td>
                                    <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a><a href="?_page=product:details&id={$cart.product_id}"> 
                                      </a><a href="javascript:deleteCart('{$cart.id}');"> 
                                      </a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp;<b> </b>
                                {if $cart.sku == "DOMAIN-REGISTER"}
                                {translate module=cart}
                                register 
                                {/translate}
                                {elseif $cart.sku == "DOMAIN-TRANSFER"}
                                {translate module=cart}
                                transfer 
                                {/translate}
                                {elseif $cart.sku == "DOMAIN-PARK"}
                                {translate module=cart}
                                park 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            {if $cart.cart_type == "1"}
                            {if $cart.host_type == "ns_transfer"}
                            {/if}
                            {if $cart.host_type == "ip"}
                            {/if}
                            {/if}
                          </table>
                        </td>
                        <td width="30%" class="row1" valign="top" align="right"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                            <tr> 
                              <td width="43%">Base Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_base_price_{$cart_assoc.id}"> 
                                  <div id="def_base_price_{$cart.id}"> 
                                    {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                  </div>
                                  <div id="base_price_{$cart.id}"></div>
                                </div>
                                <div id="base_price_{$cart_assoc.id}"></div>
                              </td>
                            </tr>
                          </table>
                          {if $cart.sku != 'DOMAIN-PARK'}
                          <select id="quantity_{$cart.id}" class="form_menu" disabled name="select">
                             
                            <option value="">{$cart.domain_term} Year</option>
                          
                          </select>
                          {/if}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              {else}
              <!-- Show product -->
              <table width="97%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
                <tr> 
                  <td> 
                    <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr> 
                        <td width="70%" class="row2" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                            <tr> 
                              <td width="67%" class="row2"><b> </b> 
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                                  <tr> 
                                    <td width="51%"><b> 
                                      {if $list->translate("product_translate","name", "product_id", $cart.product_id, "translate_product")}
                                      {/if}
                                      <u> 
                                      {$translate_product.name}
                                      </u> </b></td>
                                    <td width="35%"><b>&nbsp; 
                                      {if $invoice.type != 1}
                                      </b>( <a href="?_page=product:view&id={$cart.product_id}"> 
                                      {$cart.sku}
                                      </a> ) 
                                      {else}
                                      ( <a href="?_page=service:view&id={$cart.service_id}"> 
                                      Service 
                                      {$cart.service_id}
                                      </a> ) 
                                      {/if}
                                    </td>
                                    <td width="14%" align="right"> 
                                      {if $cart.attribute_popup != ""}
                                      <a href="#" {popup fgcolor=FCFCFC sticky=true width="300" caption="Item Attributes" text=$cart.attribute_popup snapx=1 snapy=1}> 
                                      <img src="themes/{$THEME_NAME}/images/icons/edit_16.gif" border="0" width="16" height="16">
									  </a>   
                                      {/if}
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp; 
                                {if $cart.price_type == "0"}
                                {translate module=cart}
                                price_type_one 
                                {/translate}
                                {/if}
                                {if $cart.price_type == "1"}
                                {translate module=cart}
                                price_type_recurr 
                                {/translate}
                                {/if}
                                {if $cart.price_type == "2"}
                                {translate module=cart}
                                price_type_trial 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            <tr> 
                              <td width="67%"> &nbsp;&nbsp;
                                {if $cart.price_type == "1"}
                                {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                {if $cart.recurring_schedule == "0" }
                                {translate module=cart}
                                recurr_week 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "1" }
                                {translate module=cart}
                                recurr_month 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "2" }
                                {translate module=cart}
                                recurr_quarter 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "3" }
                                {translate module=cart}
                                recurr_semianual 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "4" }
                                {translate module=cart}
                                recurr_anual 
                                {/translate}
                                {/if}
                                {if $cart.recurring_schedule == "5" }
                                {translate module=cart}
                                recurr_twoyear 
                                {/translate}
                                {/if}
                                &nbsp;&nbsp; + &nbsp; 
                                {$list->format_currency($cart.price_setup, $invoice.billed_currency_id)}
                                {translate module=cart}
                                setup 
                                {/translate}
                                {/if}
                              </td>
                            </tr>
                            {if $cart.item_type == "1"}
                            {if $cart.domain_type == "ns_transfer"}
                            <tr> 
                              <td width="67%">&nbsp;&nbsp; 
                                {translate module=cart}
                                host_type_domain 
                                {/translate}
                                - <u> {$cart.domain_name}.{$cart.domain_tld}</u>
								</td>
                            </tr>
                            {/if}
                            {if $cart.domain_type == "ip"}
                            <tr> 
                              <td width="67%">&nbsp;&nbsp; 
                                {translate module=cart}
                                host_type_ip 
                                {/translate}
                              </td>
                            </tr>
                            {/if}
                            {/if}
                          </table>
                        </td>
                        <td width="30%" class="row1" valign="top"> 
                          <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                            <tr> 
                              <td width="43%">Base Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_base_price_{$cart.id}"> 
                                  {$list->format_currency($cart.price_base, $invoice.billed_currency_id)}
                                </div>
                                <div id="base_price_{$cart.id}"></div>
                              </td>
                            </tr>
                            <tr> 
                              <td width="43%">Setup Price</td>
                              <td width="57%" valign="middle" align="right"> 
                                <div id="def_setup_price_{$cart.id}"> 
                                  {$list->format_currency($cart.price_setup, $invoice.billed_currency_id)}
                                </div>
                                <div id="setup_price_{$cart.id}"></div>
                              </td>
                            </tr>
                            <tr> 
                              <td width="43%"> 
                                {translate module=cart}
                                quantity 
                                {/translate}
                              </td>
                              <td width="57%" valign="middle" align="right"> 
                                {$cart.quantity}
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              {/if}
              {/foreach}
              <br>
            </td>
          </tr> 
          <tr valign="top">
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr> 
                  <td width="15%"> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                  <td align="center" width="70%" valign="middle"><a href="javascript:showMemos()"><img title=Memos src="themes/{$THEME_NAME}/images/icons/notep_16.gif" border="0" alt="Memos"></a> 
                    {if $invoice.type != 1}&nbsp;&nbsp<a href="javascript:showServices()"><img title=Services src="themes/{$THEME_NAME}/images/icons/tools_16.gif" border="0" alt="Services"></a>{/if} 
                    &nbsp;&nbsp&nbsp;<a href="javascript:showEmail()"><img title=E-mail src="themes/{$THEME_NAME}/images/icons/mail_16.gif" border="0" alt="E-mail User"></a> 
                    &nbsp;&nbsp&nbsp;<a href="?_page=invoice:pdf&id={$invoice.id}&_escape=true" target="_blank"><img title=Print src="themes/{$THEME_NAME}/images/icons/print_16.gif" border="0" alt="Printable Invoice"></a> 
                    &nbsp;&nbsp 
                    {assign var=account_details value=$invoice.account_popup}
                    <a href="?_page=account_admin:view&id={$invoice.account_id}" {popup fgcolor=FCFCFC sticky=false width="150" caption="Account Details" text="$account_details" snapx=1 snapy=1}><img src="themes/{$THEME_NAME}/images/icons/user_16.gif" border="0" alt="View User Account"></a> 
                    {if $invoice.total_amt > $invoice.billed_amt}
                    &nbsp;&nbsp;<a href="javascript:showReconcile({$invoice.total_amt})"><img title=Reconcile src="themes/{$THEME_NAME}/images/icons/add_16.gif" border="0" alt="Services"></a> 
                    {/if}
                    {if $invoice.billed_amt > 0}
                    &nbsp;&nbsp<a href="javascript:showRefund({$invoice.billed_amt})"><img title=Refund src="themes/{$THEME_NAME}/images/icons/remov_16.gif" border="0" alt="Services"></a> 
                    {/if}
                  </td>
                  <td align="right" width="15%"> 
                    <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$invoice.id}','{$VAR.id}');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table> 
	<input type="hidden" name="_page" value="invoice:view">
    <input type="hidden" name="invoice_id" value="{$invoice.id}">
    <input type="hidden" name="do[]" value="invoice:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  <center> 
    <BR>
	<iframe id="iframeInvoice" style="border:0px; width:0px; height:0px;" scrolling="auto" ALLOWTRANSPARENCY="true" frameborder="0"></iframe> 
  </center>
</form>
   
{/foreach}
{/if}

{literal}
<script language="JavaScript">
<!-- START
 
var invoice_id 	= {/literal}{$invoice.id}{literal};
var account_id 	= '{/literal}{$invoice.account_id}{literal}';  
 
function showEmail() {
	showIFrame('iframeInvoice',600,350,'?_page=account_admin:iframe_mail_one&_escape=1&mail_account_id='+account_id);
}

function showMemos() {
	showIFrame('iframeInvoice',600,350,'?_page=core:search_iframe&module=invoice_memo&_escape=1&invoice_memo_invoice_id='+invoice_id+
			   '&_escape_next=1&_next_page_one=view&_next_page_none=add&name_id1=invoice_memo_invoice_id&val_id1='+invoice_id);
}

function showServices() { 
	showIFrame('iframeInvoice',600,350,'?_page=core:search_iframe&module=service&_escape=1&service_invoice_id='+invoice_id+
			   '&_escape_next=1&_next_page=iframe_search_show&_next_page_one=iframe_view&_next_page_none=none&name_id1=service_invoice_id&val_id1='+invoice_id);
}

function showReconcile(amt) { 
	showIFrame('iframeInvoice',600,200,'?_page=invoice:reconcile&id='+invoice_id+'&_escape=1&amount='+amt);
}

function showRefund(amt) { 
	showIFrame('iframeInvoice',600,200,'?_page=invoice:refund&id='+invoice_id+'&_escape=1&amount='+amt);
}

//  END -->
</script>
{/literal}
