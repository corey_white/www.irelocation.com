
<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
{if $VAR.invoice_account_id == "" }
<form id="invoice_add" name="invoice_add" method="post" action=""> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=invoice}title_add{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=invoice}
                    field_account_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->popup("invoice_add", "invoice_account_id", $VAR.invoice_account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                  </td>
                </tr>
				{ if $list->is_installed('affiliate') == 1  }
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=invoice}
                    field_affiliate_id 
                    {/translate}
                  </td>
                  <td width="65%">
                    <input type="text" id="discount" name="aid" size="12" class="form_field" value="---">
                  </td>
				  {/if}
                </tr>
                <tr valign="top">
                  <td width="35%"></td>
                  <td width="65%">
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                    <input type="hidden" name="_page" value="invoice:add">
                    <input type="hidden" name="_page_current" value="invoice:add">
                  </td>
                </tr>
              </table>
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
 
{else}
<form id="invoice_add" name="invoice_add" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=invoice}
                title_add
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=invoice}
                    field_account_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->popup("invoice_add", "invoice_account_id", $VAR.invoice_account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">Add Item to Invoice</td>
                  <td width="65%">
                    { $list->menu("no", "invoice_product_id", "product", "sku", "all", "form_menu\" onchange=\"showProduct(this.value)\"") }
                  </td>
                </tr>
                <tr valign="top">
                  <td width="35%">&nbsp;</td>
                  <td width="65%"> <a href="?_page=discount:add&discount_avail_account_id={$VAR.invoice_account_id}" target="_blank"> 
                    {translate module=invoice}
                    add_discount {/translate} </a> 
					
					| <a href="javascript:showCart()"> {translate module=invoice}
                    add_view_items {/translate}</a> | 
					
					<a href="javascript:showCheckout()">{translate module=invoice}
                    add_finalize {/translate}</a> 
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
{/if}
{literal}
  <center> 
    <BR>
	<iframe id="iframeInvoice" style="border:0px; width:0px; height:0px;" scrolling="auto" ALLOWTRANSPARENCY="true" frameborder="0"></iframe> 
  </center>
<script language="JavaScript">
<!-- START
  
var account_id 	= '{/literal}{$VAR.invoice_account_id}{literal}';  
 
function showCart() {
	showIFrame('iframeInvoice',575,500,'?_page=cart:admin_view&_escape=1&account_id='+account_id);
}

function showCheckout() {
	showIFrame('iframeInvoice',575,500,'{/literal}{$SSL_URL}{literal}admin.php?_page=checkout:admin_checkout&_escape=1&account_id='+account_id);
}
 
function showProduct(product_id) {
	showIFrame('iframeInvoice',575,500,'?_page=product:admin_details&_escape=1&id='+product_id+'&account_id='+account_id);
}
 
//  END -->
</script>
{/literal}
