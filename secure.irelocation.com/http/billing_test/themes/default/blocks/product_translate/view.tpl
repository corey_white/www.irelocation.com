{ $block->display("core:top_clean") }

{literal}
	<script type="text/javascript"> 
	   var _editor_url  = "includes/htmlarea/";
	   var _editor_lang = "{/literal}{$smarty.const.SESS_LANGUAGE}{literal}";
	   function getThisField(id) {
	   	return document.getElementById(id).value;
	   }
	</script>	
	<script type="text/javascript" src="includes/htmlarea/htmlarea.js"></script>	
{/literal}

{ $method->exe("product_translate","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

 <!-- Loop through each record -->
{foreach from=$product_translate item=product_translate}
<a name="{$product_translate.id}"></a> 
<!-- Display the field validation -->
{if $form_validation}
{ $block->display("core:alert_fields") }
{/if}
<!-- Display each record -->
<form name="product_translate_view" method="post" action="">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> <b>
                    {translate module=product_translate}
                    field_name 
                    {/translate}
                    </b> </td>
                  <td width="65%"> 
                    <input type="text" name="product_translate_name" value="{$product_translate.name}" class="form_field" size="32">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> <b> 
                    {translate module=product_translate}
                    field_email_template 
                    {/translate}
                    <br>
                    <br>
                    <input type="submit" name="Submit2" value="{translate}submit{/translate}" class="form_button">
                    </b> </td>
                  <td width="65%"> 
                    <textarea  name="product_translate_email_template" rows="3" cols="55" class="form_field">{$product_translate.email_template}</textarea>
                  </td>
                </tr>
              </table>
              <br>
              <table width="100%" border="0" cellspacing="0" cellpadding="3" class="body">
                <tr> 
                  <td> <b>
                    {translate module=product_translate}
                    field_description_short 
                    {/translate}
                    </b> </td>
                </tr>
                <tr> 
                  <td>
                    {htmlarea field=product_translate_description_short width=500 height=150}
                    {$product_translate.description_short}
                    {/htmlarea}
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td> <b>
                    {translate module=product_translate}
                    field_description_full 
                    {/translate}
                    </b> </td>
                </tr>
                <tr>
                  <td>
                    {htmlarea field=product_translate_description_full width=500 height=270}
                    {$product_translate.description_full}
                    {/htmlarea}
                  </td>
                </tr>
              </table>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr> 
                  <td>&nbsp; </td>
                  <td align="right"> 
                    <div align="left">
                      <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td align="right">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="product_translate:view">
    <input type="hidden" name="product_translate_id" value="{$product_translate.id}">
    <input type="hidden" name="do[]" value="product_translate:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  <input type="hidden" name="product_translate_product_id" value="{$product_translate.product_id}">
  <input type="hidden" name="product_translate_language_id" value="{$product_translate.language_id}">
</form>
  {/foreach}
{/if}
