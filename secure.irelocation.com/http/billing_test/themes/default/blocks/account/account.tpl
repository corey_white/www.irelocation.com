{if $smarty.const.SESS_LOGGED != true }
	{ $block->display("account:login") }
{else}
<table width="460" border="0" cellpadding="0" class="body" cellspacing="0" align="center">
  <tr> 
    <td>
      <div align="center">
        <table width="200" border="0" cellspacing="0" cellpadding="0" class="body" align="left">
          <tr> 
            <td valign="top" align="center" width="35%"><a href="{$SSL_URL}?_page=account:view">{translate module=account}account_link{/translate}</a><br>
              <a href="{$SSL_URL}?_page=core:user_search&module=account_billing&_next_page=user_search_show&_next_page_one=user_view">{translate module=account}billing_link{/translate}</a><br>
              <a href="{$SSL_URL}?_page=core:user_search&module=discount&_next_page=user_search_show">{translate module=account}discount_link{/translate}</a></td>
          </tr>
        </table>
      </div>
    </td>
    <td>
      <div align="center">
<table width="200" border="0" cellspacing="0" cellpadding="0" class="body" align="left">
          <tr> 
            <td valign="top" align="center" width="35%"><a href="?_page=staff:staff">{translate}contact{/translate}</a>
			
			<br>
              <a href="?_page=newsletter:newsletter">{translate module=account}newsletter_link{/translate}</a> <br>
              {if $list->is_installed('affiliate') }
              <a href="{$SSL_URL}?_page=affiliate:affiliate">{translate module=account}affiliate_link{/translate}</a> 
              {/if}
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr> 
    <td>&nbsp; </td>
    <td>&nbsp; </td>
  </tr>
  <tr> 
    <td> 
      <table width="200" border="0" cellspacing="0" cellpadding="0" class="body">
        <tr> 
          <td valign="top" align="center" width="35%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
                    <form name="form1" method="post" action="">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr valign="top"> 
                              <td width="65%" class="table_heading"> 
                                <div align="center"> 
                                  {translate module=invoice}
                                  menu 
                                  {/translate}
                                </div>
                              </td>
                            </tr>
                            <tr valign="top"> 
                              <td width="65%" class="row1"> 
                                <table width="100%" border="0" cellspacing="5" cellpadding="1" class="row1">
                                  <tr> 
                                    <td width="74%"> 
                                      {translate module=invoice}
                                      {/translate}
                                      Due Invoices</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=invoice&_next_page=user_search_show&invoice_billing_status=0&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a> </td>
                                  </tr>
                                  <tr> 
                                    <td width="74%"> 
                                      {translate module=invoice}
                                      {/translate}
                                      Paid Invoices</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=invoice&_next_page=user_search_show&invoice_billing_status=1&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a></td>
                                  </tr>
                                  <tr> 
                                    <td width="74%"> 
                                      {translate module=invoice}
                                      {/translate}
                                      All Invoices</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=invoice&_next_page=user_search_show&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </form>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td> 
      <table width="200" border="0" cellspacing="0" cellpadding="0" class="body">
        <tr> 
          <td valign="top" align="center" width="35%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
                    <form name="form1" method="post" action="">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr valign="top"> 
                              <td width="65%" class="table_heading"> 
                                <div align="center"> 
                                  {translate module=service}
                                  menu 
                                  {/translate}
                                </div>
                              </td>
                            </tr>
                            <tr valign="top"> 
                              <td width="65%" class="row1"> 
                                <table width="100%" border="0" cellspacing="5" cellpadding="1" class="row1">
                                  <tr> 
                                    <td width="74%"> Active Services</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=service&_next_page=user_search_show&service_active=1&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a> </td>
                                  </tr>
                                  <tr> 
                                    <td width="74%"> Inactive Services</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=service&_next_page=user_search_show&service_active=0&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a></td>
                                  </tr>
                                  <tr> 
                                    <td width="74%"> All Services</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=core:user_search&module=service&_next_page=user_search_show&_next_page_one=user_view"> 
                                      {translate module=invoice}
                                      menu_view 
                                      {/translate}
                                      </a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </form>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr> 
    <td><br>
      <br>
    </td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top"> 
      {if $list->is_installed('ticket') }
      <table width="200" border="0" cellspacing="0" cellpadding="0" class="body">
        <tr> 
          <td valign="top" align="center" width="35%"> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
                    <form name="form1" method="post" action="">
                      <tr> 
                        <td> 
                          <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr valign="top"> 
                              <td width="65%" class="table_heading"> 
                                <div align="center"> 
                                  {translate module=ticket}
                                  menu 
                                  {/translate}
                                </div>
                              </td>
                            </tr>
                            <tr valign="top"> 
                              <td width="65%" class="row1"> 
                                <table width="100%" border="0" cellspacing="5" cellpadding="1" class="row1">
                                  <tr> 
                                    <td width="74%"> Create New Ticket</td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=ticket:ticket"> 
                                      {translate module=ticket}
                                      menu_add 
                                      {/translate}
                                      </a> </td>
                                  </tr>
                                  <tr> 
                                    <td width="74%"> Manage Tickets </td>
                                    <td width="26%" align="right"> <a href="{$SSL_URL}?_page=ticket:ticket"> 
                                      {translate module=ticket}
                                      menu_view 
                                      {/translate}
                                      </a></td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </form>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      {/if}
    </td>
    <td valign="top"> 
      {if $list->is_installed('htaccess') }
      {$block->display('htaccess:htaccess')}
      {/if}
    </td>
  </tr>
</table>
{/if}