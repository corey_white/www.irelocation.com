 <form name="form1" method="post" action="">
  <table width="140" border="0" cellspacing="3" cellpadding="0" align="center" class="body">
    <tr> 
      <td width="48%"> <b> 
        {translate}
        username 
        {/translate}
        </b><br>
        <input type="text" name="_username" class="form_field" value="{$VAR._username}" size="10">
      </td>
    </tr>
    <tr> 
      <td width="48%"> <b> 
        {translate}
        password 
        {/translate}
        </b><br>
        <input type="password" name="_password" class="form_field" size="10">
      </td>
    </tr>
    <tr> 
      <td width="48%"> 
        <input type="hidden" name="_login" value="Y">
        {if $VAR._page == ""}
        <input type="hidden" name="_page" value="account:account">
        {else}
        <input type="hidden" name="_page" value="{$VAR._page}">
        {/if}
		{if $VAR._htaccess_id != "" || $VAR._htaccess_dir_id != ""}
        <input type="hidden" name="_htaccess_id" value="{$VAR._htaccess_id}"> 
        <input type="hidden" name="_htaccess_dir_id" value="{$VAR._htaccess_dir_id}">
		{/if}
        <input type="submit" name="_login" value="{translate}login{/translate}" class="form_button">
      </td>
    </tr>
    <tr> 
      <td width="48%"> <br>
        {translate}
        login_user_add 
        {/translate}
        <a href="?_page=account:add"> <br>
        {translate}
        register 
        {/translate}
        </a><br>
        <br>
      </td>
    </tr>
    <tr> 
      <td width="48%"> 
        {translate}
        reset_password_txt 
        {/translate}
        <a href="?_page=account:password"> <br>
        {translate}
        reset_password 
        {/translate}
        </a><br>
      </td>
    </tr>
    {if $smarty.const.DEFAULT_ACCOUNT_STATUS == "1"}
    <tr> 
      <td width="48%" class="row1"> 
        {translate}
        verify_txt 
        {/translate}
        <a href="?_page=account:verify"> <br>
        {translate}
        verify 
        {/translate}
        </a> </td>
    </tr>
    {/if}
  </table>
  </form>
 
