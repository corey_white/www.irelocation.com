<!-- Load the JSCalender code -->
<link rel="stylesheet" type="text/css" media="all" href="includes/jscalendar/calendar-blue.css" title="win2k-1" />
<script type="text/javascript" src="includes/jscalendar/calendar_stripped.js"></script>
<script type="text/javascript" src="includes/jscalendar/lang/calendar-{$smarty.const.LANG}.js"></script>
<script type="text/javascript" src="includes/jscalendar/calendar-setup_stripped.js"></script>


{if $smarty.const.SESS_LOGGED != true }
	{ $block->display("account:login") }
{else}

{ $method->exe("account","view") } { if ($method->result == FALSE) } 
{ $block->display("core:method_error") } {else} 

<!-- Loop through each record -->
{foreach from=$account item=account}  

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form id="update_form" name="update_form" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                {translate module=account}
                title_view
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row4" valign="top"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_date_last 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    {$list->date_time($account.date_last)}
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_username 
                    {/translate}
                  </td>
                  <td width="69%"> <b> 
                    {$account.username}
                    </b> 
                    <input type="hidden" name="account_username" value="{$account.username}">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_update_password 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_password" class="form_field" value="">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_confirm_password 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="confirm_password" class="form_field" value="">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%" height="20"> 
                    {translate module=account}
                    field_email 
                    {/translate}
                  </td>
                  <td width="69%" height="20"> 
                    <input type="text" name="account_email" class="form_field" value="{$account.email}">
                  </td>
                </tr>				
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_company 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_company" class="form_field" value="{$account.company}">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_first_name 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_first_name" class="form_field" value="{$account.first_name}">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_middle_name 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_middle_name" class="form_field" value="{$account.middle_name}">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_last_name 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_last_name" class="form_field" value="{$account.last_name}">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_title 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    <select name="account_title" class="form_menu">
                      <option value="Mr"{if $account.title == "Mr"} selected{/if}> 
                      {translate module=account}
                      mr 
                      {/translate}
                      </option>
                      <option value="Mrs"{if $account.title == "Mrs"} selected{/if}> 
                      {translate module=account}
                      mrs 
                      {/translate}
                      </option>
                      <option value="Miss"{if $account.title == "Miss"} selected{/if}> 
                      {translate module=account}
                      miss 
                      {/translate}
                      </option>					  
                      <option value="Dr"{if $account.title == "Dr"} selected{/if}> 
                      {translate module=account}
                      dr 
                      {/translate}
                      </option>
                      <option value="Prof"{if $account.title == "Prof"} selected{/if}> 
                      {translate module=account}
                      prof 
                      {/translate}
                      </option>
                    </select>
                  </td>
                </tr>			
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    {translate module=account}
                    field_address1 
                    {/translate}
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address1" value="{$account.address1}" {if $account_address1 == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    {translate module=account}
                    field_address2 
                    {/translate}
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address2" value="{$account.address2}" {if $account_address2 == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    {translate module=account}
                    field_city 
                    {/translate}
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_city" value="{$account.city}" {if $account_city == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    {translate module=account}
                    field_state 
                    {/translate}
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_state" value="{$account.state}" {if $account_state == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    {translate module=account}
                    field_zip 
                    {/translate}
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_zip" value="{$account.zip}" {if $account_zip == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr> 
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_country_id
                    {/translate}
                  </td>
                  <td width="69%">
                    { $list->menu("no", "cid", "country", "name", $account.country_id, "form_menu") }
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_language_id
                    {/translate}
                  </td>
                  <td width="69%"> 
                    { $list->menu_files("", "lid", $account.language_id, "language", "", "_core.xml", "form_menu") }
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_currency_id 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    { $list->menu("no", "cyid", "currency", "name", $account.currency_id, "form_menu") }
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_theme_id 
                    {/translate}
                  </td>
                  <td width="69%">   
                    { $list->menu_files("", "tid", $account.theme_id, "theme", "", ".user_theme", "form_menu") }
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    {translate module=account}
                    field_email_html 
                    {/translate}
                  </td>
                  <td width="69%"> 
                    { $list->bool("account_email_type", $account.email_type, "form_menu") }
                  </td>
                </tr>
                {foreach from=$static_var item=record}
                <tr valign="top"> 
                  <td width="29%"> 
                    {$record.name}
                  </td>
                  <td width="71%"> 
                    {$record.html}
                  </td>
                </tr>
                {/foreach}
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"></td>
                  <td width="69%"> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="_page" value="account:view">
	<input type="hidden" name="_page_current" value="account:view">
    <input type="hidden" name="do[]" value="account:update">
	
  </form>
  {/foreach}    
{/if}
{/if}
