

<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
<form id="newsletter_add" name="newsletter_add" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center">
                {translate module=newsletter}
                title_add
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="newsletter_name" value="{$VAR.newsletter_name}" {if $newsletter_name == true}class="form_field_error"{else}class="form_field"{/if}>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_group_avail 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu_multi($VAR.newsletter_group_avail, 'newsletter_group_avail', 'group', 'name', '', '10', 'form_menu') }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_description 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <textarea name="newsletter_description" cols="40" rows="10" {if $newsletter_description == true}class="form_field_error"{else}class="form_field"{/if}>{$VAR.newsletter_description}</textarea> 
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_notes 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <textarea name="newsletter_notes" cols="40" rows="5" {if $newsletter_notes == true}class="form_field_error"{else}class="form_field"{/if}>{$VAR.newsletter_notes}</textarea>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_active 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->bool("newsletter_active", $VAR.newsletter_active, "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    field_display_signup 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->bool("newsletter_display_signup", $VAR.newsletter_display_signup, "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                    <input type="hidden" name="_page" value="newsletter:view">
                    <input type="hidden" name="_page_current" value="newsletter:add">
                    <input type="hidden" name="do[]" value="newsletter:add">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
