
<form name="newsletter_send" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                {translate module=newsletter}
                menu_send 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_newsletters 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->check("", "newsletter_id", "newsletter", "name", "", "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_from_email 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "setup_email_id", "setup_email", "name", $smarty.const.DEFAULT_SETUP_EMAIL, "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_subject 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="newsletter_subject" size="60" class="form_field" value="" maxlength="255">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_body_text 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <textarea name="newsletter_body_text" cols="65" rows="10" class="form_field"></textarea>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_body_html 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <textarea name="newsletter_body_html" cols="40" rows="10" class="form_field"></textarea> 
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_high_priority 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="checkbox" name="newsletter_priority" value="1">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"> 
                    {translate module=newsletter}
                    send_test 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="checkbox" name="newsletter_test" value="1">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                        </td>
                        <td align="right">&nbsp; </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="_page" value="newsletter:main">
  <input type="hidden" name="do[]" value="newsletter:send">
</form>
