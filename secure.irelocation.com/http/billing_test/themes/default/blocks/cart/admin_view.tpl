{ $block->display("core:top_clean") }

{$method->exe("cart","admin_view")}
{if ($method->result == FALSE)}
    {$block->display("core:method_error")}
{else}
	{if $results <= 0}
        No items in your cart at this time....
    {else}

	{literal}
		<script language="JavaScript">
		<!-- START

		var acct = '{/literal}{$VAR.account_id}{literal}';
		
		function changeDomainTerm(id,term)
		{
			showIFrame('iframeCart',0,0,'?_page=cart:changeqty&type=3&_escape=1&id='+id+'&term='+term); 			
		}
				
		function changeRecurring(id,schedule)
		{
			showIFrame('iframeCart',0,0,'?_page=cart:admin_changeqty&account_id='+acct+'&type=2&_escape=1&id='+id+'&schedule='+schedule); 			
		}
		
		function changeQuantity(id,qty)
		{ 
			if(qty == "0") qty = 1;					
			showIFrame('iframeCart',0,0,'?_page=cart:admin_changeqty&account_id='+acct+'&type=1&_escape=1&id='+id+'&qty='+qty); 			
		}
		
		function deleteCart(id)
		{
			document.getElementById(id).style.display = 'none';
			showIFrame('iframeCart',0,0,'?_page=cart:admin_changeqty&account_id='+acct+'&_escape=1&id='+id+'&qty=0'); 
		}
		
		function updatePrice(id,base,setup,qty)
		{ 
			document.getElementById("quantity_"+id).value = qty;
			document.getElementById("def_base_price_"+id).style.display='none';
			document.getElementById("base_price_"+id).innerHTML = base;		
			
			if(document.getElementById("def_setup_price_"+id))
			document.getElementById("def_setup_price_"+id).style.display='none';
			
			if(document.getElementById("setup_price_"+id))
			document.getElementById("setup_price_"+id).innerHTML = setup;
		}		
		
		//  END -->
		</script>
{/literal}
<!-- LOOP THROUGH EACH RECORD -->
{foreach from=$cart item=cart}
<DIV id="{$cart.id}"> 
  {if $cart.cart_type == "2"}
  <!-- Show domain -->
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> <u> 
                          {$cart.domain_name|upper}.{$cart.domain_tld|upper}
                          </u> </b></td>
                        <td width="37%">&nbsp;</td>
                        <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a><a href="?_page=product:details&id={$cart.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"><img src="themes/{$THEME_NAME}/images/icons/trash_16.gif" border="0" alt="Remove from Cart"></a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> 
                    {if $cart.host_type == "register"}
                    {translate module=cart}
                    register 
                    {/translate}
                    {elseif $cart.host_type == "transfer"}
                    {translate module=cart}
                    transfer 
                    {/translate}
                    {elseif $cart.host_type == "park"}
                    {translate module=cart}
                    park 
                    {/translate}
                    {/if}
                    </b></td>
                </tr>
                {if $cart.cart_type == "1"}
                {if $cart.host_type == "ns_transfer"}
                {/if}
                {if $cart.host_type == "ip"}
                {/if}
                {/if}
              </table>
            </td>
            <td width="30%" class="row1" valign="top" align="right"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <div id="def_base_price_{$cart_assoc.id}"> 
                      <DIV id="def_base_price_{$cart.id}">
                        {$list->format_currency($cart.price, $smarty.const.SESS_CURRENCY)}
                      </DIV>
                    <DIV id="base_price_{$cart.id}"></DIV>
                    </div>
                    <div id="base_price_{$cart_assoc.id}"></div>
                  </td>
                </tr>
              </table>
			  {if $cart.host_type == 'register'}
              <select id="quantity_{$cart.id}" class="form_menu" onChange="changeDomainTerm('{$cart.id}',this.value);">
                {foreach from=$cart.tld_arr item=tld_price key=tld_term}
                <option value="{$tld_term}" {if $tld_term == $cart.domain_term}selected{/if}> 
                {$tld_term}
                Year 
                {$list->format_currency($tld_price, $smarty.const.SESS_CURRENCY)}
                </option>
                {/foreach}
              </select>
			  {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
{else}
<!-- Show product -->
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> 
                          {if $list->translate("product_translate","name", "product_id", $cart.product_id, "translate_product")}
                          {/if}
                          <U> 
                          {$translate_product.name}
                          </U> </b></td>
                        <td width="35%"><b>&nbsp;</b>( {$cart.product.sku}  ) &nbsp; </td>
                        <td width="14%" align="right"> <a href="?_page=product:admin_details&id={$cart.product_id}&_escape=1"> 
                          </a> <a href="javascript:deleteCart('{$cart.id}');"> 
                          <img title=Delete src="themes/{$THEME_NAME}/images/icons/trash_16.gif" border="0"></a> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> 
                    {translate module=cart}
                    price_type 
                    {/translate}
                    : </b> 
                    {if $cart.product.price_type == "0"}
                    {translate module=cart}
                    price_type_one 
                    {/translate}
                    {/if}
                    {if $cart.product.price_type == "1"}
                    {translate module=cart}
                    price_type_recurr 
                    {/translate}
                    {/if}
                    {if $cart.product.price_type == "2"}
                    {translate module=cart}
                    price_type_trial 
                    {/translate}
                    {/if}
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> 
                    {if $cart.product.price_type == "1"}
                    &nbsp;&nbsp; 
                    <select id="recurr_schedule_{$cart.id}" name="recurr_schedule_{$cart.id}" onChange="changeRecurring('{$cart.id}',this.value);" class="form_menu">
                      {foreach from=$cart.price item=price_recurr key=key}
                      <option value="{$key}" {if $cart.recurr_schedule == $key} selected{/if}> 
                      {$list->format_currency($price_recurr.base,$smarty.const.SESS_CURRENCY)}
                      &nbsp;&nbsp; 
                      {if $key == "0" }
                      {translate module=cart}
                      recurr_week 
                      {/translate}
                      {/if}
                      {if $key == "1" }
                      {translate module=cart}
                      recurr_month 
                      {/translate}
                      {/if}
                      {if $key == "2" }
                      {translate module=cart}
                      recurr_quarter 
                      {/translate}
                      {/if}
                      {if $key == "3" }
                      {translate module=cart}
                      recurr_semianual 
                      {/translate}
                      {/if}
                      {if $key == "4" }
                      {translate module=cart}
                      recurr_anual 
                      {/translate}
                      {/if}
                      {if $key == "5" }
                      {translate module=cart}
                      recurr_twoyear 
                      {/translate}
                      {/if}
                      &nbsp;&nbsp; + &nbsp; 
                      {$list->format_currency($price_recurr.setup,$smarty.const.SESS_CURRENCY)}
                      {translate module=cart}
                      setup
                      {/translate}
                      </option>
                      {/foreach}
                    </select>      
					{/if}                   
                  </td>
                </tr>
				
				 
                {if $cart.cart_type == "1"}
				{if $cart.host_type == "ns_transfer"}
                <tr>
                  <td width="67%">&nbsp;&nbsp; 
                    {translate module=cart}
                    host_type_domain 
                    {/translate}
                    - <u>
                    {$cart.domain_name}.{$cart.domain_tld}
                    </u></td>
                </tr>
				{/if}
				
				{if $cart.host_type == "ip"}
                <tr>
                  <td width="67%">&nbsp;&nbsp; 
                    {translate module=cart}
                    host_type_ip 
                    {/translate}
                  </td>
                </tr>
				{/if}
				{/if}				
              </table>
            </td>
            <td width="30%" class="row1" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <DIV id="def_base_price_{$cart.id}"> 
                      {$list->format_currency($cart.price_base, $smarty.const.SESS_CURRENCY)}
                    </DIV>
                    <DIV id="base_price_{$cart.id}"></DIV>
                  </td>
                </tr>
                <tr> 
                  <td width="43%">Setup Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <DIV id="def_setup_price_{$cart.id}"> 
                      {$list->format_currency($cart.price_setup, $smarty.const.SESS_CURRENCY)}
                    </DIV>
                    <DIV id="setup_price_{$cart.id}"></DIV>
                  </td>
                </tr>
                <tr> 
                  <td width="43%"> 
                    {translate module=cart}
                    quantity 
                    {/translate}
                  </td>
                  <td width="57%" valign="middle" align="right"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="76%" align="right"> 
                          <input type="text" id="quantity_{$cart.id}" name="quantity_{$cart.id}" size="2" class="form_field" value="{$cart.quantity}" onChange="changeQuantity('{$cart.id}',this.value);" {if $cart.product.price_type != "0"}disabled{/if}>
                        </td>
                        <td width="24%" valign="middle" align="right"><img src="themes/{$THEME_NAME}/images/icons/calc_16.gif" border="0"></td>
                      </tr>
                    </table>                    
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
 </table>
{/if}
<br> 

{foreach from=$cart.assoc item=cart_assoc} 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
      <td> 
        <table id="main2" width="100%" border="0" cellspacing="1" cellpadding="2">
          <tr> 
            <td width="70%" class="row2" valign="top"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row2">
                <tr> 
                  <td width="67%" class="row2"><b> </b> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="51%"><b> <u> 
                          {$cart_assoc.domain_name|upper}
                          . 
                          {$cart_assoc.domain_tld|upper}
                          </u> </b></td>
                        <td width="37%">&nbsp;</td>
                        <td width="12%" align="right"><a href="?_page=product:details&id={$cart_assoc.product_id}"> 
                          </a><a href="javascript:deleteCart('{$cart.id}');"> 
                          </a></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr> 
                  <td width="67%"> &nbsp;&nbsp;<b> </b> 
                    {if $cart_assoc.host_type == "register"}
						{translate module=cart} host_type_register {/translate}
					{elseif $cart_assoc.host_type == "transfer"}
						{translate module=cart} host_type_transfer {/translate}
					{elseif $cart_assoc.host_type == "park"}
						{translate module=cart} host_type_park {/translate}
					{/if}
                    {$cart.product.sku}
                  </td>
                </tr>
              </table>
            </td>
            <td width="30%" class="row1" valign="top" align="right"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="0" class="row1">
                <tr> 
                  <td width="43%">Base Price</td>
                  <td width="57%" valign="middle" align="right"> 
                    <div id="def_base_price_{$cart_assoc.id}"> 
                      {$list->format_currency($cart_assoc.price, $smarty.const.SESS_CURRENCY)}
                    </div>
                    <div id="base_price_{$cart_assoc.id}"></div> 
                  </td>
                </tr>
              </table>
			  {if $cart_assoc.host_type == 'register'}
              <select id="quantity_{$cart_assoc.id}" class="form_menu" onChange="changeDomainTerm('{$cart_assoc.id}',this.value);">
                {foreach from=$cart_assoc.tld_arr item=tld_price key=tld_term}
                <option value="{$tld_term}" {if $tld_term == $cart_assoc.domain_term}selected{/if}> 
                {$tld_term}
                Year  
                {$list->format_currency($tld_price, $smarty.const.SESS_CURRENCY)}
                </option>
                {/foreach}
              </select>
			  {/if}
            </td>
          </tr>
        </table>
      </td>
    </tr>
 </table>
<br> 
{/foreach}

</DIV>
{/foreach}
<iframe id="iframeCart" style="border:0px; width:0px; height:0px;" scrolling="no" ALLOWTRANSPARENCY="true" frameborder="0"></iframe> 
 
{/if}
{/if}
