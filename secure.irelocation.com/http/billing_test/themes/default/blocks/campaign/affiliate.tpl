{ $method->exe("campaign","affiliate") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

<!-- Loop through each record -->
{foreach from=$campaign item=campaign} <a name="{$campaign.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form name="campaign_view" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=campaign}
                title_view 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                {if $campaign.file1 != ""}
                <tr> 
                  <td> 
                    <div align="center"> 
                      <p><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=1&_log=no" target="_blank" border="0"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=1&_log=no&_escape=1&aid={$VAR.curr_aid}" border="0"></a></p>
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td> 
                    <div align="center"> 
                      <textarea name="code" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=1&aid={$VAR.curr_aid}"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=1&_escape=1"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                {if $campaign.file2 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=2&_log=no&aid={$VAR.curr_aid}" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=2&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=2&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add"> 
                      </a> 
                      <textarea name="textarea" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=2">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=2&_escape=1&aid={$VAR.curr_aid}"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                {if $campaign.file3 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=3&_log=no&aid={$VAR.curr_aid}" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=3&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=3&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add"> 
                      </a> 
                      <textarea name="textarea2" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=3">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=3&_escape=1&aid={$VAR.curr_aid}"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                {if $campaign.file4 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=4&_log=no&aid={$VAR.curr_aid}" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=4&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=4&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add"> 
                      </a> 
                      <textarea name="textarea3" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=4">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=4&_escape=1&aid={$VAR.curr_aid}"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <p>&nbsp;</p>
              <p align="center"><b><br>
                </b> 
                {translate module=campaign}
                random_code 
                {/translate}
              </p>
              <p align="center">&nbsp; </p>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="6" class="row2">
                <tr> 
                  <td> 
                    <div align="center"> 
                      <textarea name="textarea4" class="form_field" cols="75" rows="5"><script language="Javascript">{literal}<!--
var currentdate = 0;
var core = 0;
function StringArray (n) {
  this.length = n;
  for (var i  = 1; i <= n; i++) {
    this[i]   = " ";
  }
}{/literal}
{counter start=0 skip=1 assign="counter"}{if $campaign.file2 != ""}{counter}{/if}{if $campaign.file3 != ""}{counter}{/if}{if $campaign.file4 != ""}{counter}{/if}{counter}
image = new StringArray({$counter});
{counter start=0 skip=1 assign="counter"} 
image[0] = '1';  
{if $campaign.file2 != ""}{counter}image[{$counter}] = '2';  
{/if}
{if $campaign.file3 != ""}{counter}image[{$counter}] = '3'; 
{/if}
{if $campaign.file4 != ""}{counter}image[{$counter}] = '4'; 
{/if}

var ran = 60/image.length
{literal}function ranimage() {
  currentdate = new Date()
  core = currentdate.getSeconds()
  core = Math.floor(core/ran)
  return(image[core])
}{/literal}
var fileId = ranimage(); 
var write1 = '<a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file='+fileId+ '&aid={$VAR.curr_aid}">';
var write2 = '<img src="{$URL}?_page=campaign:display&id={$campaign.id}&file='+fileId+'&_escape=1" border="0"></a>';
document.write(write1 + "" + write2);
//--></script></textarea>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <p>
    <input type="hidden" name="_page" value="campaign:view">
    <input type="hidden" name="campaign_id" value="{$campaign.id}">
    <input type="hidden" name="do[]" value="campaign:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  </p>
  <p>&nbsp; </p>
</form>
  {/foreach}
{/if}
