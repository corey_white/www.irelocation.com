
{ $method->exe("campaign","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

<!-- Image Preview -->{literal}
<script language="JavaScript" type="text/JavaScript">
function previewImage(fileInfo) {
var filename = "";
//create the path to your local file
if (fileInfo == null) {
if (document.form1.file != "") {
filename = "file:///" + document.form1.file.value;
}
} else {
filename = fileInfo;
}
//check if there is a value
if (filename == "") {
alert ("Please select a image.");
document.form1.file.focus();
} else {
//create the popup 
popup = window.open('', 'imagePreview', 'width=640,height=100,left=100,top=75, screenX=100,screenY=75,scrollbars,location,menubar,status=0,toolbar=0,resizable=1');
//start writing in the html code
popup.document.writeln("<html><body bgcolor='#FFFFFF'>");
//get the extension of the file to see if it has one of the image extensions
var fileExtension = filename.substring(filename.lastIndexOf(".")+1);
if (fileExtension == "jpg" || fileExtension == "jpeg" || fileExtension == "gif" 
|| fileExtension == "png")
popup.document.writeln("<img src='" + filename + "'>");
else
//if not extension fron list above write URL to file 
popup.document.writeln("<a href='" + filename + "'>" + filename + "</a>");
popup.document.writeln("</body></html>");
popup.document.close();
popup.focus();
}
}
</script>{/literal}

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'campaign';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$campaign item=campaign} <a name="{$campaign.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form name="campaign_view" method="post" action="" enctype="multipart/form-data">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=campaign}
                title_view 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_date_orig 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    {$list->date_time($campaign.date_orig)}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_date_last 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    {$list->date_time($campaign.date_last)}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_date_start 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->calender_view("campaign_date_start", $campaign.date_start, "form_field", $campaign.id) }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_date_expire 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->calender_view("campaign_date_expire", $campaign.date_expire, "form_field", $campaign.id) }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_status 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->bool("campaign_status", $campaign.status, "form_menu") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_name 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="campaign_name" value="{$campaign.name}" class="form_field" size="32">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_description 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <textarea name="campaign_description" cols="40" rows="5" class="form_field">{$campaign.description}</textarea>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_budget 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="campaign_budget" value="{$campaign.budget}" class="form_field" size="5">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_target_conversion 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="campaign_target_conversion" value="{$campaign.target_conversion}" class="form_field" size="5">
                    {translate module=campaign}
                    conversion_tip 
                    {/translate}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=campaign}
                    field_url 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="campaign_url" value="{$campaign.url}" class="form_field" size="32">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">&nbsp; </td>
                  <td width="65%">&nbsp; </td>
                </tr>
              </table>
              <br>
              <table width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr> 
                  <td class="row1"> 
                    <table width="100%" border="0" cellpadding="3" class="row1">
                      <tr> 
                        <td width="31%" height="24">&nbsp;</td>
                        <td width="21%" height="24" align="right"><b> 
                          {translate module=campaign}
                          impressions 
                          {/translate}
                          </b></td>
                        <td width="20%" height="24" align="right"><b> 
                          {translate module=campaign}
                          clicks 
                          {/translate}
                          </b></td>
                        <td width="28%" height="24" align="right"><b> 
                          {translate module=campaign}
                          conversion 
                          {/translate}
                          </b></td>
                      </tr>
                      <tr class="row2"> 
                        <td width="31%"> <b> 
                          {translate module=campaign}
                          add_one 
                          {/translate}
                          </b></td>
                        <td width="21%" align="right"> 
                          {assign var="b1" value=$campaign.served1}
                          {$campaign.served1|number_format}
                        </td>
                        <td width="20%" align="right"> 
                          {assign var="a1" value=$campaign.clicked1}
                          {$campaign.clicked1|number_format}
                        </td>
                        <td width="28%" align="right"> 
                          {math equation="x / y * 100" x=$a1|default:"1" y=$b1|default:"1" assign="c1" format="%.2f"}
                          {$c1}
                          %</td>
                      </tr>
                      <tr> 
                        <td width="31%"> <b> 
                          {translate module=campaign}
                          add_two 
                          {/translate}
                          </b></td>
                        <td width="21%" align="right"> 
                          {assign var="b2" value=$campaign.served2}
                          {$campaign.served2|number_format}
                        </td>
                        <td width="20%" align="right"> 
                          {assign var="a2" value=$campaign.clicked2}
                          {$campaign.clicked2|number_format}
                        </td>
                        <td width="28%" align="right"> 
                          {math equation="x / y * 100" x=$a2|default:"1" y=$b2|default:"1" assign="c2" format="%.2f"}
                          {$c2}
                          %</td>
                      </tr>
                      <tr class="row2"> 
                        <td width="31%"> <b> 
                          {translate module=campaign}
                          add_three 
                          {/translate}
                          </b></td>
                        <td width="21%" align="right"> 
                          {assign var="b3" value=$campaign.served3}
                          {$campaign.served3|number_format}
                        </td>
                        <td width="20%" align="right"> 
                          {assign var="a3" value=$campaign.clicked3}
                          {$campaign.clicked3|number_format}
                        </td>
                        <td width="28%" align="right"> 
                          {math equation="x / y * 100" x=$a3|default:"1" y=$b3|default:"1" assign="c3" format="%.2f"}
                          {$c3}
                          %</td>
                      </tr>
                      <tr> 
                        <td width="31%"> <b> 
                          {translate module=campaign}
                          add_four 
                          {/translate}
                          </b></td>
                        <td width="21%" align="right"> 
                          {assign var="b4" value=$campaign.served4}
                          {$campaign.served4|number_format}
                        </td>
                        <td width="20%" align="right"> 
                          {assign var="a4" value=$campaign.clicked4}
                          {$campaign.clicked4|number_format}
                        </td>
                        <td width="28%" align="right"> 
                          {math equation="x / y * 100" x=$a4|default:"1" y=$b4|default:"1" assign="c4" format="%.2f"}
                          {$c4}
                          %</td>
                      </tr>
                      <tr class="row2"> 
                        <td width="31%">&nbsp;</td>
                        <td width="21%" align="right"> <b> 
                          {math equation="a + b + c + d" a=$b1|default:"0" b=$b2|default:"0" c=$b3|default:"0" d=$b4|default:"0" assign="total_served"}
                          {$total_served|number_format}
                          </b></td>
                        <td width="20%" align="right"><b> 
                          {math equation="a + b + c + d" a=$a1|default:"0" b=$a2|default:"0" c=$a3|default:"0" d=$a4|default:"0" assign="total_clicked"}
                          {$total_clicked|number_format}
                          </b></td>
                        <td width="28%" align="right"><b> 
                          {if $total_clicked > 0 && $total_served > 0}
                          {math equation="x / y * 100" x=$total_clicked|default:"1" y=$total_served|default:"1" assign="total_conversion" format="%.2f"}
                          {$total_conversion}
                          % 
                          {else}
                          ------------- 
                          {/if}
                          </b></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <p>&nbsp;</p><p>&nbsp;</p>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                <tr class="row2"> 
                  <td class="row2"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="34%"> 
                          {translate module=campaign}
                          field_file1 
                          {/translate}
                        </td>
                        <td align="right" width="66%"> 
                          <input type="hidden" name="campaign_file1" value="{$campaign.file1}">
                          <input type="file" name="upload_file1" class="form_field" size="38" {if $campaign_file1 == true}class="form_field_error"{else}class="form_field"{/if}>
                          <img src="themes/{$THEME_NAME}/images/icons/picts_16.gif" onClick="previewImage(document.campaign_view.upload_file1.value);"> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {if $campaign.file1 != ""}
                <tr> 
                  <td> 
                    <div align="center"> 
                      <p><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=1&_log=no" target="_blank" border="0"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=1&_log=no&_escape=1" border="0"></a></p>
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td> 
                    <div align="center"> 
                      <textarea name="code" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=1"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=1&_escape=1"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                <tr class="row2"> 
                  <td class="row2"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="34%"> 
                          {translate module=campaign}
                          field_file2 
                          {/translate}
                        </td>
                        <td align="right" width="66%"> 
                          <input type="file" name="upload_file2" class="form_field" size="38">
                          <img src="themes/{$THEME_NAME}/images/icons/picts_16.gif" onClick="previewImage(document.campaign_view.upload_file2.value);"> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {if $campaign.file2 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=2&_log=no" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=2&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=2&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add">
					{translate module=campaign}delete_file{/translate}</a> 
                      <textarea name="textarea" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=2">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=2&_escape=1"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                <tr class="row2"> 
                  <td class="row2">  
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="34%"> 
                          {translate module=campaign}
                          field_file3 
                          {/translate}
                        </td>
                        <td align="right" width="66%"> 
                          <input type="file" name="upload_file3" class="form_field" size="38">
                          <img src="themes/{$THEME_NAME}/images/icons/picts_16.gif" onClick="previewImage(document.campaign_view.upload_file3.value);"> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {if $campaign.file3 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=3&_log=no" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=3&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=3&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add">{translate module=campaign}delete_file{/translate}</a> 
                      <textarea name="textarea2" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=3">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=3&_escape=1"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellpadding="5">
                <tr class="row2"> 
                  <td class="row2"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row2">
                      <tr> 
                        <td width="34%"> 
                          {translate module=campaign}
                          field_file4 
                          {/translate}
                        </td>
                        <td align="right" width="66%"> 
                          <input type="file" name="upload_file4" class="form_field" size="38" >
                          <img src="themes/{$THEME_NAME}/images/icons/picts_16.gif" onClick="previewImage(document.campaign_view.upload_file4.value);"> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                {if $campaign.file4 != ""}
                <tr> 
                  <td> 
                    <div align="center"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=4&_log=no" target="_blank"><img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=4&_log=no&_escape=1" border="0"></a> 
                    </div>
                  </td>
                </tr>
                <tr> 
                  <td class="row1"> 
                    <div align="center"><a href="{$URL}?_page=campaign:view&file=4&id={$VAR.id}&campaign_id={$campaign.id}&do%5B%5D=campaign:delete_add">{translate module=campaign}delete_file{/translate}</a> 
                      <textarea name="textarea3" class="form_field" cols="75" rows="2"><a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file=4">
					  <img src="{$URL}?_page=campaign:display&id={$campaign.id}&file=4&_escape=1"></a></textarea>
                      <br>
                      <br>
                      <br>
                    </div>
                  </td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <p>&nbsp;</p>
              <p align="center"><b><br>
                </b> 
                {translate module=campaign}
                random_code 
                {/translate}
              </p>
              <p align="center">&nbsp; </p>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="6" class="row2">
                <tr> 
                  <td>
                    <div align="center">
                      <textarea name="textarea4" class="form_field" cols="75" rows="5"><script language="Javascript">{literal}<!--
var currentdate = 0;
var core = 0;
function StringArray (n) {
  this.length = n;
  for (var i  = 1; i <= n; i++) {
    this[i]   = " ";
  }
}{/literal}
{counter start=0 skip=1 assign="counter"}{if $campaign.file2 != ""}{counter}{/if}{if $campaign.file3 != ""}{counter}{/if}{if $campaign.file4 != ""}{counter}{/if}{counter}
image = new StringArray({$counter});
{counter start=0 skip=1 assign="counter"} 
image[0] = '1';  
{if $campaign.file2 != ""}{counter}image[{$counter}] = '2';  
{/if}
{if $campaign.file3 != ""}{counter}image[{$counter}] = '3'; 
{/if}
{if $campaign.file4 != ""}{counter}image[{$counter}] = '4'; 
{/if}

var ran = 60/image.length
{literal}function ranimage() {
  currentdate = new Date()
  core = currentdate.getSeconds()
  core = Math.floor(core/ran)
  return(image[core])
}{/literal}
var fileId = ranimage(); 
var write1 = '<a href="{$URL}?_page=campaign:click&caid={$campaign.id}&file='+fileId+ '">';
var write2 = '<img src="{$URL}?_page=campaign:display&id={$campaign.id}&file='+fileId+'&_escape=1" border="0"></a>';
document.write(write1 + "" + write2);
//--></script></textarea>
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top">
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="0" cellpadding="6">
                <tr> 
                  <td> 
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                  <td align="right"> 
                    <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$campaign.id}','{$VAR.id}');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  <p>
    <input type="hidden" name="_page" value="campaign:view">
    <input type="hidden" name="campaign_id" value="{$campaign.id}">
    <input type="hidden" name="do[]" value="campaign:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  </p>
  <p>&nbsp; </p>
</form>
  {/foreach}
{/if}
