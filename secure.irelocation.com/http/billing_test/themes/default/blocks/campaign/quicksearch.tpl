{ $method->exe("campaign","search_form") }
{ if ($method->result == FALSE) }
    { $block->display("core:method_error") }
{else}

<HTML>
<HEAD>
<TITLE>{$smarty.const.SITE_NAME}</TITLE>

<link rel="stylesheet" href="themes/{$THEME_NAME}/quicksearch.css" type="text/css">

{literal}<SCRIPT LANGUAGE="JavaScript">
<!-- START
function doSearch()
{ 
	var vars;
	var module = 'campaign';
	vars = '&campaign_name='+document.search_form.campaign_name.value; 
	top.location = '?_page=core:search&module='+module+'&_next_page_one=view&_escape=1'+vars;
}
//  END -->
</SCRIPT>{/literal}
</head>
<body style="background-color: transparent" ALLOWTRANSPARENCY="true" onLoad="document.search_form.campaign_name.focus();">



<form name="search_form" method="post" onsubmit="doSearch();">
  
  <table width="185" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                {translate module=campaign}
                title_search 
                {/translate}
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="180" border="0" cellspacing="1" cellpadding="1" class="row1" align="center">
                <tr valign="top"> 
                  <td width="70%"> 
                    {translate module=campaign}
                    field_name 
                    {/translate}
                  </td>
                  <td width="30%"> 
                    <input type="text" name="campaign_name" class="form_field">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="70%"> <img src="themes/{$THEME_NAME}/images/icons/cancl_16.gif" border="0" onclick="window.parent.hideIFrame('iframeQuickSearch');"> 
                  </td>
                  <td width="30%"> 
                    <div align="right">  
                      <input type="image" border="0" name="imageField" src="themes/{$THEME_NAME}/images/icons/srch_16.gif">
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
 </body>
 </html>
{/if}
 
