

<!-- Display the form validation -->
{if $form_validation}
	{ $block->display("core:alert_fields") }
{/if}

<!-- Display the form to collect the input values -->
<form id="service_add" name="service_add" method="post" action="">

<table width="500" border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=service}title_add{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
            <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_date_orig
                        {/translate}</td>
                    <td width="65%">
                        {$list->date_time("")}  <input type="hidden" name="service_date_orig" value="{$smarty.now}">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_date_last
                        {/translate}</td>
                    <td width="65%">
                        {$list->date_time("")}  <input type="hidden" name="service_date_last" value="{$smarty.now}">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_parent_id
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_parent_id" value="{$VAR.service_parent_id}" {if $service_parent_id == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_invoice_id
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_invoice_id" value="{$VAR.service_invoice_id}" {if $service_invoice_id == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_account_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->popup("service_add", "service_account_id", $VAR.service_account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field", "") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_account_billing_id
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_account_billing_id" value="{$VAR.service_account_billing_id}" {if $service_account_billing_id == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_product_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->menu("", "service_product_id", "product", "name", $VAR.service_product_id, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_sku
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_sku" value="{$VAR.service_sku}" {if $service_sku == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_active
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("service_active", $VAR.service_active, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_bind
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("service_bind", $VAR.service_bind, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_type" value="{$VAR.service_type}" {if $service_type == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_price
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_price" value="{$VAR.service_price}" {if $service_price == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_price_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_price_type" value="{$VAR.service_price_type}" {if $service_price_type == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_taxable
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("service_taxable", $VAR.service_taxable, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_queue
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_queue" value="{$VAR.service_queue}" {if $service_queue == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_date_last_invoice
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_add("service_date_last_invoice", $VAR.service_date_last_invoice, "form_field") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_date_next_invoice
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_add("service_date_next_invoice", $VAR.service_date_next_invoice, "form_field") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_recur_type" value="{$VAR.service_recur_type}" {if $service_recur_type == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_schedule
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_recur_schedule" value="{$VAR.service_recur_schedule}" {if $service_recur_schedule == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_weekday
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_recur_weekday" value="{$VAR.service_recur_weekday}" {if $service_recur_weekday == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_week
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_recur_week" value="{$VAR.service_recur_week}" {if $service_recur_week == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_schedule_change
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("service_recur_schedule_change", $VAR.service_recur_schedule_change, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_recur_cancel
                        {/translate}</td>
                    <td width="65%">
                        { $list->bool("service_recur_cancel", $VAR.service_recur_cancel, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_group_grant
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_group_grant" value="{$VAR.service_group_grant}" {if $service_group_grant == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_group_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_group_type" value="{$VAR.service_group_type}" {if $service_group_type == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_group_days
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_group_days" value="{$VAR.service_group_days}" {if $service_group_days == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_host_server_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->menu("", "service_host_server_id", "host_server", "name", $VAR.service_host_server_id, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_host_provision_plugin_data
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_host_provision_plugin_data" value="{$VAR.service_host_provision_plugin_data}" {if $service_host_provision_plugin_data == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_host_ip
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_host_ip" value="{$VAR.service_host_ip}" {if $service_host_ip == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_host_username
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_host_username" value="{$VAR.service_host_username}" {if $service_host_username == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_host_password
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_host_password" value="{$VAR.service_host_password}" {if $service_host_password == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_name
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_domain_name" value="{$VAR.service_domain_name}" {if $service_domain_name == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_tld
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_domain_tld" value="{$VAR.service_domain_tld}" {if $service_domain_tld == true}class="form_field_error"{else}class="form_field"{/if} size="5">
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_term
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_domain_term" value="{$VAR.service_domain_term}" {if $service_domain_term == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_type
                        {/translate}</td>
                    <td width="65%">
                        <input type="text" name="service_domain_type" value="{$VAR.service_domain_type}" {if $service_domain_type == true}class="form_field_error"{else}class="form_field"{/if}>
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_date_expire
                        {/translate}</td>
                    <td width="65%">
                        { $list->calender_add("service_domain_date_expire", $VAR.service_domain_date_expire, "form_field") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_host_tld_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->menu("", "service_domain_host_tld_id", "host_tld", "name", $VAR.service_domain_host_tld_id, "form_menu") }
                    </td>
                  </tr>
                <tr valign="top">
                    <td width="35%">
                        {translate module=service}
                            field_domain_host_registrar_id
                        {/translate}</td>
                    <td width="65%">
                        { $list->menu("", "service_domain_host_registrar_id", "host_registrar_plugin", "name", $VAR.service_domain_host_registrar_id, "form_menu") }
                    </td>
                  </tr>
           <tr valign="top">
                    <td width="35%"></td>
                    <td width="65%">
                      <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                      <input type="hidden" name="_page" value="service:view">
                      <input type="hidden" name="_page_current" value="service:add">
                      <input type="hidden" name="do[]" value="service:add">
                    </td>
                  </tr>
                </table>
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
