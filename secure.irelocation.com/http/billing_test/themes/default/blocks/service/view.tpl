{ $method->exe("service","view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}

{literal}
	<script src="themes/{/literal}{$THEME_NAME}{literal}/view.js"></script>
    <script language="JavaScript"> 
        var module 		= 'service';
    	var locations 	= '{/literal}{$VAR.module_id}{literal}';		
		var id 			= '{/literal}{$VAR.id}{literal}';
		var ids 		= '{/literal}{$VAR.ids}{literal}';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = '?_page='+module+':view&id='+array_id[0]+'&ids='+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
		
		// service status updater + run queue
		function statusUpdate(obj) {
			var status = obj.value;
			var id = '{/literal}{$service.id}{literal}';
			document.location = '?_page=service:view&id='+id+'&service_status='+status+'&do[]=service:update&do[]=service:queue';
		}
		
    //  END -->
    </script>
{/literal}

<!-- Loop through each record -->
{foreach from=$service item=service} <a name="{$service.id}"></a>

<!-- Display the field validation -->
{if $form_validation}
   { $block->display("core:alert_fields") }
{/if}

<!-- Display each record -->
<form id="service_view" name="service_view" method="post" action="">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              {translate module=service}title_view{/translate}
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_date_orig 
                    {/translate}
                    </b> </td>
                  <td width="40%"> <b> 
                    {translate module=service}
                    field_date_last 
                    {/translate}
                    </b> </td>
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_invoice_id 
                    {/translate}
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="30%"> 
                    {$list->date_time($service.date_orig)}
                  </td>
                  <td width="40%"> 
                    {$list->date_time($service.date_last)}
                  </td>
                  <td width="30%"> <a href="?_page=invoice:view&id={$service.invoice_id}"> 
                    {$service.invoice_id}
                    </a></td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_active 
                    {/translate}
                    </b> </td>
                  <td width="40%"> <b> 
                    {translate module=service}
                    field_bind 
                    {/translate}
                    </b> </td>
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_type 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="30%"> 

					{literal} <script language="JavaScript"> 
							function statusUpdate(obj) {
								var status = obj.value;
								var id = '{/literal}{$service.id}{literal}';
								document.location = '?_page=service:view&id='+id+'&service_id='+id+'&service_active='+status+'&do[0]=service:update';
							} 
					</script> {/literal}
				  
                    { $list->bool("service_active", $service.active, "form_menu\" onChange=\"document.getElementById('service_view').submit()\"") }
                  </td>
                  <td width="40%"> 
                    { $list->bool("service_bind", $service.bind, "form_menu\" onChange=\"document.getElementById('service_view').submit()\"") }
                  </td>
                  <td width="30%"> 
                    {translate module=service}{$service.type}{/translate}
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_queue 
                    {/translate}
                    </b> </td>
                  <td width="40%"> <b> 
                    {translate module=service}
                    field_account_id 
                    {/translate}
                    </b></td>
                  <td width="30%"><b> 
                    {translate module=service}
                    field_sku 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="30%"> 
				  	{if $service.queue != "" && $service.queue!= 'none' }
                    {translate module=service}
                    	{$service.queue}
                    {/translate}
					{else}
                    {translate module=service}
                    	queue_none
                    {/translate}					
					{/if}
                  </td>
                  <td width="40%"> 
                    { $list->popup("service_view", "service_account_id", $service.account_id, "account_admin", "account", "first_name,middle_name,last_name", "form_field\" disabled", false) }
                  </td>
                  <td width="30%"> <a href="?_page=core:search&module=product&product_sku={$service.sku}&_next_page_one=view"> 
                    {$service.sku}
                    </a></td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_price 
                    {/translate}
                    </b></td>
                  <td width="40%"> <b> 
                    {translate module=service}
                    field_price_type 
                    {/translate}
                    </b></td>
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_taxable 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="30%"> 
                    <input type="text" name="service_price" value="{$service.price}" class="form_field" size="5">
                    {$list->currency_iso("")}
                  </td>
                  <td width="40%"> 
                    {if $service.price_type == "0"}
                    {translate module=product}
                    price_type_one 
                    {/translate}
                    {/if}
                    {if $service.price_type == "1"}
                    {translate module=product}
                    price_type_recurr 
                    {/translate}
                    {/if}
                  </td>
                  <td width="30%"> 
                    { $list->bool("service_taxable", $service.taxable, "form_menu\" onChange=\"document.getElementById('service_view').submit()\"") }
                  </td>
                </tr>
				{if $service.account_billing_id > 0}
                <tr valign="top"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_account_billing_id 
                    {/translate}
                    </b> </td>
                  <td width="40%"> 
                    { $list->menu_cc_admin("service_account_billing_id", $service.account_id, $service.account_billing_id, "form_menu") }
                  </td>
                  <td width="30%">&nbsp;</td>
                </tr>
				{/if}
                <tr class="row1" valign="middle" align="left"> 
                  <td width="30%">
                    <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                  </td>
                  <td width="40%"><a href="?_page=service:view&id={$service.id}&do[]=service:queue"> 
                    </a></td>
                  <td width="30%"> 
                    <div align="right">
                      <input type="button" name="delete" value="{translate}delete{/translate}" class="form_button" onClick="delete_record('{$service.id}','{$VAR.id}');">
                    </div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    
  {if $service.date_next_invoice > 0}
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=service}
                title_recurring 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_date_last_invoice 
                    {/translate}
                    </b></td>
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_date_next_invoice 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    {$list->date($service.date_last_invoice)}
                  </td>
                  <td width="50%"> 
                    { $list->calender_view("service_date_next_invoice", $service.date_next_invoice, "form_field", $service.id) }
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_recur_schedule 
                    {/translate}
                    </b></td>
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_recur_type 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <select name="service_recur_schedule" class="form_menu">
                      <option value="0" {if $service.recur_schedule == "0"} selected{/if}> 
                      {translate module=product}
                      recurr_week 
                      {/translate}
                      </option>
                      <option value="1" {if $service.recur_schedule == "1"} selected{/if}> 
                      {translate module=product}
                      recurr_month 
                      {/translate}
                      </option>
                      <option value="2" {if $service.recur_schedule == "2"} selected{/if}> 
                      {translate module=product}
                      recurr_quarter 
                      {/translate}
                      </option>
                      <option value="3" {if $service.recur_schedule == "3"} selected{/if}> 
                      {translate module=product}
                      recurr_semianual 
                      {/translate}
                      </option>
                      <option value="4" {if $service.recur_schedule == "4"} selected{/if}> 
                      {translate module=product}
                      recurr_anual 
                      {/translate}
                      </option>
                      <option value="5" {if $service.recur_schedule == "5"} selected{/if}> 
                      {translate module=product}
                      recurr_twoyear 
                      {/translate}
                      </option>
                    </select>
                  </td>
                  <td width="50%"> 
                    {if $service.recur_type == 0 }
                    {translate module=product}
                    recurr_type_aniv 
                    {/translate}
                    {else}
                    {translate module=product}
                    recurr_type_fixed 
                    {/translate}
                    {/if}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_recur_weekday 
                    {/translate}
                    </b></td>
                  <td width="50%"> 
                    <input type="text" name="service_recur_weekday" value="{$service.recur_weekday}" class="form_field" size="2" maxlength="2">
                    (1-28) </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_recur_schedule_change 
                    {/translate}
                    </b></td>
                  <td width="50%"> 
                    { $list->bool("service_recur_schedule_change", $service.recur_schedule_change, "form_menu\" onChange=\"document.getElementById('service_view').submit()\"") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> <b> 
                    {translate module=service}
                    field_recur_cancel 
                    {/translate}
                    </b></td>
                  <td width="50%"> 
                    { $list->bool("service_recur_cancel", $service.recur_cancel, "form_menu\" onChange=\"document.getElementById('service_view').submit()\"") }
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"><b> 
                    {translate module=service}
                    field_suspend_billing 
                    {/translate}
                    </b></td>
                  <td width="50%"> 
                    { $list->bool("service_suspend_billing", $service.suspend_billing, "form_menu\" onChange=\"javascript:document.getElementById('service_view').submit()")  }
                  </td>
                </tr>
				
				{if $service.suspend_billing != 1 || $service.active == 1}
                <tr valign="top">
                  <td width="50%">&nbsp;</td>
                  <td width="50%">
                    <div align="right"><a href="?_page=service:view&id={$service.id}&do%5B%5D=service:cancelservice"> 
                      </a> 
                      <input type="button" name="cancelservice" value="{translate}cancel{/translate}" class="form_button" onClick="if (confirm('{translate module=service}confirm_cancel{/translate}')) {literal}{ document.location='?_page=service:view&id={/literal}{$service.id}{literal}&do[]=service:cancelservice'; }{/literal}">
                    </div>
                  </td>
                </tr>
				{/if}
				
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  {/if}
  
  {if $service.type == 'group' || $service.type == 'host_group' || $service.type == 'product_group' }
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=service}
                title_group
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="2" cellpadding="3" class="row1">
                <tr> 
                  <td width="98%" valign="top"> <b> </b> 
                    <table width="100%" border="0" cellspacing="2" cellpadding="1" class="row1">
                      <tr> 
                        <td> 
                          <p> 
                            <input type="radio" name="service_group_type" value="0" {if $service.group_type == "0"}checked{/if}>
                            {translate module=product}
                            assoc_group_limited 
                            {/translate}
                            <input type="text" name="service_group_days" value="{$service.group_days}" class="form_field" size="3">
                            <br>
                            <input type="radio" name="service_group_type" value="1" {if $service.group_type == "1"}checked{/if}>
                            {translate module=product}
                            assoc_group_subscription 
                            {/translate}
                            <br>
                            <input type="radio" name="service_group_type" value="2" {if $service.group_type == "2"}checked{/if}>
                            {translate module=product}
                            assoc_group_forever 
                            {/translate}
                          </p>
                        </td>
                      </tr>
                    </table>
                    </td>
                  <td width="2%" align="left" valign="top"> 
                    { $list->menu_multi($service.group_grant, "service_group_grant", "group", "name", "10", "", "form_menu") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  {/if}
  
  {if $service.type == 'domain' }
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=service}
                title_domain 
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_domain_name 
                    {/translate}
                    </b></td>
                  <td width="40%"> <b> 
                    {translate module=service}
                    field_domain_term 
                    {/translate}
                    </b></td>
                  <td width="30%"> <b> 
                    {translate module=service}
                    field_domain_date_expire 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="30%"> 
                    { $service.domain_name|upper }.{ $service.domain_tld|upper }
                    <b>&nbsp; <a href="?_page=core:search&module=service&service_domain_name={$service.domain_name}&service_domain_tld={$service.domain_tld}"><img src="themes/{$THEME_NAME}/images/icons/zoomi_16.gif" border="0" width="16" height="16" alt="Resend Invoice"></a> 
                    </b> </td>
                  <td width="40%"> 
                    { $service.domain_term }
                    Year(s)</td>
                  <td width="30%"> 
                    { $list->calender_view("service_domain_date_expire", $service.domain_date_expire, "form_field", $service.id) }
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="30%" height="22"> <b> 
                    {translate module=service}
                    field_domain_type 
                    {/translate}
                    </b></td>
                  <td width="40%" height="22"> <b> 
                    {translate module=service}
                    field_domain_host_registrar_id 
                    {/translate}
                    </b></td>
                  <td width="30%" height="22"> <b> 
                    {translate module=service}
                    field_domain_host_tld_id 
                    {/translate}
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="30%" height="27"> 
                   {translate module=cart}{$service.domain_type}{/translate} 
                  </td>
                  <td width="40%" height="27"> 
                    { $list->menu("", "service_domain_host_registrar_id", "host_registrar_plugin", "name", $service.domain_host_registrar_id, "form_menu") }
                  </td>
                  <td width="30%" height="27"> 
                    { $list->menu("", "service_domain_host_tld_id", "host_tld", "name", $service.domain_host_tld_id, "form_menu") }
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  {/if}
  
  {if $service.type == 'host' || $service.type == 'host_group' }
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                {translate module=service}
                title_hosting
                {/translate}
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=service}
                    field_domain_name 
                    {/translate}
                  </td>
                  <td width="65%"> <b> 
                    { $service.domain_name|upper }
                    .
                    { $service.domain_tld|upper }
                    &nbsp; <a href="?_page=core:search&module=service&service_domain_name={$service.domain_name}&service_domain_tld={$service.domain_tld}"><img src="themes/{$THEME_NAME}/images/icons/zoomi_16.gif" border="0" width="16" height="16" alt="Resend Invoice"></a> 
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=service}
                    field_host_server_id 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $list->menu("", "disabled", "host_server", "name", $service.host_server_id, "form_menu\" disabled") }
                  </td>
                </tr>
                { if $service.host_ip != "" }
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=service}
                    field_host_ip 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    { $service.host_ip }
                  </td>
                </tr>
                {/if}
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=service}
                    field_host_username 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="service_host_username" value="{$service.host_username}" class="form_field" size="24">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=service}
                    field_host_password 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    <input type="text" name="service_host_password" value="{$service.host_password}" class="form_field" size="24">
                  </td>
                </tr>
                <tr valign="top">
                  <td width="35%"> 
                    {translate module=service}
                    resend_email
                    {/translate}
                  </td>
                  <td width="65%"><a href="?_page=service:view&do[]=service:resend_hosting_email&id={$service.id}&account_id={$service.account_id}"><img title="E-mail user" src="themes/{$THEME_NAME}/images/icons/mail_16.gif" border="0" width="16" height="16"></a></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top">
            <td width="65%" class="row1">
			   {if ($list->smarty_array("host_server","provision_plugin", "", "plugin")) } 
					{foreach from=$plugin item=arr} 
						{if $service.host_server_id == $arr.id}
			 			  {assign var="product" value=$service}
						  {assign var="afile" 	value=$arr.provision_plugin}
						  {assign var="ablock" 	value="host_provision_plugin:plugin_prod_"}
						  {assign var="blockfile" value="$ablock$afile"}
						  { $block->display($blockfile) } 
						{/if}
					 {/foreach}        						
				 {/if} 			
			</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

 
  
  {/if}
  {if $service.type == 'product' || $service.type == 'product_group' }
  <br>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                Product Plugin Details 
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="50%"> Plugin</td>
                  <td width="65%"> <b> 
                    { $service.prod_plugin_name }
                    </b> </td>
                </tr>
                { if $service.host_ip != "" }
                {/if}
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">  
              {assign var="product" value=$service}
              {assign var="afile" 	value=$service.prod_plugin_name}
              {assign var="ablock" 	value="product_plugin:plugin_prod_"}
              {assign var="blockfile" value="$ablock$afile"}
              { $block->display($blockfile) } 
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  {/if}
  <input type="hidden" name="_page" value="service:view">
    <input type="hidden" name="service_id" value="{$service.id}">
    <input type="hidden" name="do[]" value="service:update">
    <input type="hidden" name="id" value="{$VAR.id}">
  
  </form>
  {/foreach}
{/if}
