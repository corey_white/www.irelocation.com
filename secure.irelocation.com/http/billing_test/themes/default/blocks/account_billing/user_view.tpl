{ $method->exe("account_billing","user_view") } { if ($method->result == FALSE) } { $block->display("core:method_error") } {else}
 
{literal}
    <!-- Define the update delete function -->
    <script language="JavaScript">
    <!-- START
        var module = 'account_billing';
    	var locations = '{/literal}{$VAR.module_id}{literal}';
    	if (locations != "")
    	{
    		refresh(0,'#'+locations)
    	}
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("{/literal}{translate}alert_delete{/translate}{literal}");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, '');		
    		if(ids == '') {
    			var url = '?_page=core:search&module=' + module + '&do[]=' + module + ':delete&delete_id=' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = 'view&id=' +ids;
    		}		
    		
    		var doit = 'delete';
    		var url = '?_page='+ module +':'+ page +'&do[]=' + module + ':' + doit + '&delete_id=' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
{/literal} 

<!-- Loop through each record -->
{foreach from=$account_billing item=account_billing}
 
<!-- Display the field validation -->
{if $form_validation}
{ $block->display("core:alert_fields") }
{/if}
<!-- Display each record -->
<form name="account_billing_view" method="post" action="">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
                {translate module=account_billing}
                title_view 
                {/translate}
              </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                {if $account_billing.card_type != ""}
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_billing}
                    field_card_type 
                    {/translate}
                  </td>
                  <td width="65%">
				  {if $VAR.account_billing_card_type != ""} 
                    <select name="account_billing_card_type" class="form_menu"> 
                      <option value="visa" {if $VAR.account_billing_card_type == "visa"}selected{/if}> 
                      {translate module=checkout}
                      card_type_visa 
                      {/translate}
                      </option>
                      <option value="mc" {if $VAR.account_billing_card_type == "mc"}selected{/if}> 
                      {translate module=checkout}
                      card_type_mc 
                      {/translate}
                      </option>
                      <option value="amex" {if $VAR.account_billing_card_type == "amex"}selected{/if}> 
                      {translate module=checkout}
                      card_type_amex 
                      {/translate}
                      </option>
                      <option value="discover" {if $VAR.account_billing_card_type == "discover"}selected{/if}> 
                      {translate module=checkout}
                      card_type_discover 
                      {/translate}
                      </option>
                      <option value="delta" {if $VAR.account_billing_card_type == "delta"}selected{/if}> 
                      {translate module=checkout}
                      card_type_delta 
                      {/translate}
                      </option>
                      <option value="solo" {if $VAR.account_billing_card_type == "solo"}selected{/if}> 
                      {translate module=checkout}
                      card_type_solo 
                      {/translate}
                      </option>
                      <option value="switch" {if $VAR.account_billing_card_type == "switch"}selected{/if}> 
                      {translate module=checkout}
                      card_type_switch 
                      {/translate}
                      </option>
                      <option value="jcb" {if $VAR.account_billing_card_type == "jcb"}selected{/if}> 
                      {translate module=checkout}
                      card_type_jcb 
                      {/translate}
                      </option>
                      <option value="diners" {if $VAR.account_billing_card_type == "diners"}selected{/if}> 
                      {translate module=checkout}
                      card_type_diners 
                      {/translate}
                      </option>
                      <option value="carteblanche" {if $VAR.account_billing_card_type == "carteblanche"}selected{/if}> 
                      {translate module=checkout}
                      card_type_carteblanche 
                      {/translate}
                      </option>
                      <option value="enroute" {if $VAR.account_billing_card_type == "enroute"}selected{/if}> 
                      {translate module=checkout}
                      card_type_enroute 
                      {/translate}
                      </option>
                    </select>
				 {else}
                    <select name="account_billing_card_type" class="form_menu"> 
                      <option value="visa" {if $account_billing.card_type == "visa"}selected{/if}> 
                      {translate module=checkout}
                      card_type_visa 
                      {/translate}
                      </option>
                      <option value="mc" {if $account_billing.card_type == "mc"}selected{/if}> 
                      {translate module=checkout}
                      card_type_mc 
                      {/translate}
                      </option>
                      <option value="amex" {if $account_billing.card_type == "amex"}selected{/if}> 
                      {translate module=checkout}
                      card_type_amex 
                      {/translate}
                      </option>
                      <option value="discover" {if $account_billing.card_type == "discover"}selected{/if}> 
                      {translate module=checkout}
                      card_type_discover 
                      {/translate}
                      </option>
                      <option value="delta" {if $account_billing.card_type == "delta"}selected{/if}> 
                      {translate module=checkout}
                      card_type_delta 
                      {/translate}
                      </option>
                      <option value="solo" {if $account_billing.card_type == "solo"}selected{/if}> 
                      {translate module=checkout}
                      card_type_solo 
                      {/translate}
                      </option>
                      <option value="switch" {if $account_billing.card_type == "switch"}selected{/if}> 
                      {translate module=checkout}
                      card_type_switch 
                      {/translate}
                      </option>
                      <option value="jcb" {if $account_billing.card_type == "jcb"}selected{/if}> 
                      {translate module=checkout}
                      card_type_jcb 
                      {/translate}
                      </option>
                      <option value="diners" {if $account_billing.card_type == "diners"}selected{/if}> 
                      {translate module=checkout}
                      card_type_diners 
                      {/translate}
                      </option>
                      <option value="carteblanche" {if $account_billing.card_type == "carteblanche"}selected{/if}> 
                      {translate module=checkout}
                      card_type_carteblanche 
                      {/translate}
                      </option>
                      <option value="enroute" {if $account_billing.card_type == "enroute"}selected{/if}> 
                      {translate module=checkout}
                      card_type_enroute 
                      {/translate}
                      </option>
                    </select>					
					{/if}
                  </td>
                </tr>
                {/if}
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_billing}
                    card_num_current 
                    {/translate}
                  </td>
                  <td width="65%">   
                    ....{$account_billing.card_num4}
                    </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_billing}
                    card_num_new 
                    {/translate}
                  </td>
                  <td width="65%"><b>
                    <input type="text" name="account_billing_card_num" value="" class="form_field" size="18" maxlength="16">
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    {translate module=account_billing}
                    field_card_exp_month 
                    {/translate}
                  </td>
                  <td width="65%">&nbsp; </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <select name="account_billing_card_exp_month" class="form_menu">
                      <option value="01" {if $account_billing.card_exp_month == "1"}selected{/if}>1 
                      (Jan)</option>
                      <option value="02" {if $account_billing.card_exp_month == "2"}selected{/if}>2 
                      (Feb)</option>
                      <option value="03" {if $account_billing.card_exp_month == "3"}selected{/if}>3 
                      (Mar)</option>
                      <option value="04" {if $account_billing.card_exp_month == "4"}selected{/if}>4 
                      (Apr)</option>
                      <option value="05" {if $account_billing.card_exp_month == "5"}selected{/if}>5 
                      (May)</option>
                      <option value="06" {if $account_billing.card_exp_month == "6"}selected{/if}>6 
                      (Jun)</option>
                      <option value="07" {if $account_billing.card_exp_month == "7"}selected{/if}>7 
                      (Jul)</option>
                      <option value="08" {if $account_billing.card_exp_month == "8"}selected{/if}>8 
                      (Aug)</option>
                      <option value="09" {if $account_billing.card_exp_month == "9"}selected{/if}>9 
                      (Oct)</option>
                      <option value="10" {if $account_billing.card_exp_month == "10"}selected{/if}>10 
                      (Sep)</option>
                      <option value="11" {if $account_billing.card_exp_month == "11"}selected{/if}>11 
                      (Nov)</option>
                      <option value="12" {if $account_billing.card_exp_month == "12"}selected{/if}>12 
                      (Dec)</option>
                    </select>
                    {translate module=account_billing}
                    field_card_exp_year 
                    {/translate}
                  </td>
                  <td width="65%">20 
                    <input type="text" name="account_billing_card_exp_year" value="{$account_billing.card_exp_year}" class="form_field" size="2" maxlength="2">
                    (example: 08) </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"><b></b></td>
                  <td width="65%"><b> </b></td>
                </tr>
                {if $account_billing.card_type == "amex"}
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"> 
                    {translate module=account_billing}
                    field_card_start_month 
                    {/translate}
                  </td>
                  <td width="65%"> 
                    {translate module=account_billing}
                    field_card_start_year 
                    {/translate}
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"> 
                    <select name="account_billing_card_start_month" class="form_menu">
                      <option value="01" {if $account_billing.card_start_month == "1"}selected{/if}>1 
                      (Jan)</option>
                      <option value="02" {if $account_billing.card_start_month == "2"}selected{/if}>2 
                      (Feb)</option>
                      <option value="03" {if $account_billing.card_start_month == "3"}selected{/if}>3 
                      (Mar)</option>
                      <option value="04" {if $account_billing.card_start_month == "4"}selected{/if}>4 
                      (Apr)</option>
                      <option value="05" {if $account_billing.card_start_month == "5"}selected{/if}>5 
                      (May)</option>
                      <option value="06" {if $account_billing.card_start_month == "6"}selected{/if}>6 
                      (Jun)</option>
                      <option value="07" {if $account_billing.card_start_month == "7"}selected{/if}>7 
                      (Jul)</option>
                      <option value="08" {if $account_billing.card_start_month == "8"}selected{/if}>8 
                      (Aug)</option>
                      <option value="09" {if $account_billing.card_start_month == "9"}selected{/if}>9 
                      (Oct)</option>
                      <option value="10" {if $account_billing.card_start_month == "10"}selected{/if}>10 
                      (Sep)</option>
                      <option value="11" {if $account_billing.card_start_month == "11"}selected{/if}>11 
                      (Nov)</option>
                      <option value="12" {if $account_billing.card_start_month == "12"}selected{/if}>12 
                      (Dec)</option>
                    </select>
                  </td>
                  <td width="65%">20 
                    <input type="text" name="account_billing_card_start_year" value="{$account_billing.card_start_year}" class="form_field" size="2" maxlength="2">
                    (example: 08) </td>
                </tr>
                {/if}
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td>&nbsp; </td>
                        <td align="right"> 
                          <input type="submit" name="Submit" value="{translate}submit{/translate}" class="form_button">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="account_billing:user_view">
    <input type="hidden" name="account_billing_id" value="{$account_billing.id}">
    <input type="hidden" name="do[]" value="account_billing:user_update">
    <input type="hidden" name="id" value="{$VAR.id}">
  </form>
  {/foreach}
{/if}
