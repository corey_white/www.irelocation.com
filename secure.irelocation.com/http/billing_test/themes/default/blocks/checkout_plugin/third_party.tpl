{literal}
<script language="JavaScript"> 
 
	function verifyCheckout() 
	{
		if (confirm('{/literal}{translate module=checkout}redirect{/translate}{literal}')) {  
			checkoutNow(); 
		} else {
			document.write('{/literal}{translate module=checkout}redirect_cancel{/translate}{literal}');
		}
	} 
	{/literal}
	  
	{if $VAR.invoice_id == ""} 
		{if $VAR.admin == '1' && $VAR.account_id != '' } {literal}
			function checkoutNow() { document.location = '?_page=core:blank&_escape=1&do[]=checkout:admin_checkoutnow&option={/literal}{$VAR.option}{literal}&account_id={/literal}{$VAR.account_id}{literal}'; }
		{/literal}{else}{literal} 
			function checkoutNow() { document.location = '?_page=core:blank&_escape=1&do[]=checkout:checkoutnow&option={/literal}{$VAR.option}{literal}'; }
		{/literal}{/if}{literal}
	{/literal}{else} 
		{if $VAR.admin == '1' && $VAR.account_id != '' } {literal}
			function checkoutNow() { document.location = '?_page=core:blank&_escape=1&do[]=invoice:admin_checkoutnow&option={/literal}{$VAR.option}{literal}&invoice_id={/literal}{$VAR.invoice_id}{literal}&account_id={/literal}{$VAR.account_id}{literal}'; }		
		{/literal}{else}{literal}
			function checkoutNow() { document.location = '?_page=core:blank&_escape=1&do[]=invoice:checkoutnow&option={/literal}{$VAR.option}{literal}&invoice_id={/literal}{$VAR.invoice_id}{literal}'; }
		{/literal}{/if}{literal}
	{/literal} {/if} {literal}	
	verifyCheckout(); 
</script>
{/literal}