{literal}
<script language="JavaScript"> 
	function checkoutNow() {  
		document.getElementById('submitform').disabled=true;
		document.getElementById('form1').submit(); 
	}  
	parent.showIFrame("iframeCheckout",550,80,false); 
</script>
{/literal}
<form id="form1" name="form1" method="post" action="">

  {if $VAR.invoice_id == ""}
  	{if $VAR.admin != '' && $VAR.account_id != '' }
	<input type="hidden" name="admin" value="1">
	<input type="hidden" name="do[]" value="checkout:admin_checkoutnow">
	<input type="hidden" name="account_id" value="{$VAR.account_id}">
	{else}
  	<input type="hidden" name="do[]" value="checkout:checkoutnow">
	{/if}
  {else}
  	{if $VAR.admin != '' && $VAR.account_id != '' }
	<input type="hidden" name="admin" value="1">
	<input type="hidden" name="do[]" value="checkout:admin_checkoutnow">
	<input type="hidden" name="account_id" value="{$VAR.account_id}">	
	{else}
  	<input type="hidden" name="do[]" value="invoice:checkoutnow"> 
	{/if}
	<input type="hidden" name="invoice_id" value="{$VAR.invoice_id}">
  {/if}
  
  <input type="hidden" name="_page" value="core:blank">
  <input type="hidden" name="option" value="{$VAR.option}">
  <input type="hidden" name="_escape" value="1">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background" align="center">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="row2"> 
              <table width="100%" border="0" cellspacing="0" cellpadding="2" class="row1">
                <tr> 
                  <td valign="top" align="center"> <font color="#990000"> 
                    {if $VAR.msg != ""}
                    {$VAR.msg|upper}
                    {else}
                    {translate module=checkout}
                    enter_card 
                    {/translate}
                    {/if}
                    </font></td>
                  </tr>
                </table> 
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="0" cellpadding="1" class="body">
                <tr> 
                  <td width="20%"> 
                    {translate module=checkout}
                    card_type 
                    {/translate}
                  </td>
                  <td width="25%"> 
                    {translate module=checkout}
                    card_no 
                    {/translate}
                  </td>
                  <td width="15%"><a href="javascript:NewWindow('ccv_help','','{$SSL_URL}?_page=checkout:ccv_help&_escape=1');"> 
                    {translate module=checkout}
                    ccv 
                    {/translate}
                    </a> </td>
                  <td width="30%"> 
                    {translate module=checkout}
                    exp_date 
                    {/translate}
                  </td>
                  <td width="10%">&nbsp;</td>
                </tr>
                <tr> 
                  <td width="20%"> 
                    { $list->card_type_menu($VAR.card_type, $VAR.option,'checkout_plugin_data[card_type]','form_menu') }
  
                  </td>
                  <td width="25%"> 
                    <input type="text" name="checkout_plugin_data[cc_no]" class="form_field" value="{$VAR.cc_no}" size="17" maxlength="16">
                  </td>
                  <td width="15%"> 
                    <input type="text" name="checkout_plugin_data[ccv]" value="{$VAR.ccv}" class="form_field" size="4" maxlength="4">
                  </td>
                  <td width="30%"> 
                    <select name="checkout_plugin_data[exp_month]" class="form_menu">
                      <option value="01" {if $VAR.exp_month == "1"}selected{/if}>1 
                      (Jan)</option>
                      <option value="02" {if $VAR.exp_month == "2"}selected{/if}>2 
                      (Feb)</option>
                      <option value="03" {if $VAR.exp_month == "3"}selected{/if}>3 
                      (Mar)</option>
                      <option value="04" {if $VAR.exp_month == "4"}selected{/if}>4 
                      (Apr)</option>
                      <option value="05" {if $VAR.exp_month == "5"}selected{/if}>5 
                      (May)</option>
                      <option value="06" {if $VAR.exp_month == "6"}selected{/if}>6 
                      (Jun)</option>
                      <option value="07" {if $VAR.exp_month == "7"}selected{/if}>7 
                      (Jul)</option>
                      <option value="08" {if $VAR.exp_month == "8"}selected{/if}>8 
                      (Aug)</option>
                      <option value="09" {if $VAR.exp_month == "9"}selected{/if}>9 
                      (Sep)</option>
                      <option value="10" {if $VAR.exp_month == "10"}selected{/if}>10 
                      (Oct)</option>
                      <option value="11" {if $VAR.exp_month == "11"}selected{/if}>11 
                      (Nov)</option>
                      <option value="12" {if $VAR.exp_month == "12"}selected{/if}>12 
                      (Dec)</option>
                    </select>
                    &nbsp;&nbsp;20 
                    <input type="text" name="checkout_plugin_data[exp_year]" value="{$VAR.exp_year}" class="form_field" size="2" maxlength="2">
                  </td>
                  <td width="10%"> 
                    <input type="submit" id="submitform" name="submitform" value="{translate module=checkout}process_ord{/translate}" class="form_menu" onClick="javascript:checkoutNow()">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table> 
</form> 