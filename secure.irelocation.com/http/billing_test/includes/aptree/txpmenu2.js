
//******************************************************************************
// ------ Apycom.com Tree-menu Data --------------------------------------------
//******************************************************************************
var tblankImage      = "includes/aptree/img/blank.gif";
var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#3F3D3D","#7E7C7C"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#F0F1F5","#F0F1F5"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "hand";
var titemHeight      = 20;

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;
var tmenuWidth       = 230;
var tmenuHeight      = 600;

var titemTarget      = "_blank";
var ticonWidth       = 16;
var ticonHeight      = 16;
var ticonAlign       = "left";

var texpandBtn       =["includes/aptree/img/expandbtn2.gif","includes/aptree/img/expandbtn2.gif","includes/aptree/img/collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"
var texpandItemClick = 1;

var texpanded = 0;

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

var tmoveable        = 1;
var tmoveImage       = "includes/aptree/img/movepic.gif";
var tmoveImageHeight = 12;
var tabsolute        = 1;
var tleft            = 10;
var ttop             = 10;

var tfloatable       = 1;
var tfloatIterations = 6;

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 5;                  // expand/collapse speed
var tXPTitleTopBackColor = "";
var tXPTitleBackColor    = "#AFB1C3";
var tXPTitleLeft    = "includes/aptree/img/xptitleleft_s.gif";
var tXPExpandBtn    = ["includes/aptree/img/xpexpand1_s.gif","includes/aptree/img/xpexpand1_s.gif","includes/aptree/img/xpcollapse1_s.gif","includes/aptree/img/xpcollapse1_s.gif"];
var tXPBtnHeight    = 23;
var tXPTitleBackImg = "includes/aptree/img/xptitle_s.gif";

var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#FFFFFF,#E6E6E6", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#3F3D3D,#7E7C7C", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#D9DAE2",
     "tXPExpandBtn=includes/aptree/img/xpexpand2_s.gif,includes/aptree/img/xpexpand3_s.gif,includes/aptree/img/xpcollapse2_s.gif,includes/aptree/img/xpcollapse3_s.gif", "tXPTitleBackImg=includes/aptree/img/xptitle2_s.gif"]
];

var tmenuItems =
[
    ["+General", "", "includes/aptree/img/xpicon1_s.gif","","", "Main Page",,"0"],
    ["|Overview", "javascript:openUrl('?_page=core:admin');", "includes/aptree/img/icons/info.gif", "",  "", "Information","_blank"],
    
	["|Support",     "",                         "includes/aptree/img/icons/support.gif",   "", "", "Support",, "2"],
    	["||Contacts",   "mailto:support@apycom.com", "includes/aptree/img/icons/contacts.gif", "", "", "Contacts"],
    	["||E-mail",     "mailto:support@apycom.com", "includes/aptree/img/icons/email.gif", "", "", "E-mail"],

    ["|Help",       "",             "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help1.gif", "includes/aptree/img/icons/help2.gif", "Help",,"2"],
    	["||Glossary",  "testlink.htm", "includes/aptree/img/icons/paper.gif", "",                    "",                   "Glossary"],
    	["||Index",     "testlink.htm", "includes/aptree/img/icons/paper.gif", "",                    "",                   "Index"],
    	["||<nobr><input value='search' size=10 style='font-size:10'>&nbsp;<input type='button' value='Search' style='font-size:10'></nobr>","", "includes/aptree/img/icons/search.gif", "", "", "Search",,"2"],

    ["||<nobr>Contents:&nbsp;<select  style='font-size:10'><option>Item 1</option><option>Item 2</option><option>Item 3</option></select></nobr>","", "includes/aptree/img/icons/list.gif", "", "", "Contents",,"2"],



    ["Accounts",   		"", "", "","","",,"","0"],
    ["|Add",        	"javascript:openUrl(' ')", "includes/aptree/img/icons/paper.gif",,, ""],
	["|List",       	"javascript:openUrl(' ')", "includes/aptree/img/icons/list.gif",,, ""],
	["|Search",     	"javascript:openUrl(' ')", "includes/aptree/img/icons/search.gif",,, ""],
	["|Statistics", 	"javascript:openUrl(' ')", "includes/aptree/img/icons/info.gif",,, ""],
    ["|Sub-accounts", 	"",,,, "",,"3"],
    ["||SubItem 1",    "javascript:openUrl(' ')", "includes/aptree/img/icons/help1.gif", "","", ""],
    ["||SubItem 2",    "javascript:openUrl(' ')", "includes/aptree/img/icons/help1.gif", "","", ""] 

 ];
apy_tmenuInit();
