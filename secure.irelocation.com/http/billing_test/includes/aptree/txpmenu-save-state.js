//******************************************************************************
// ------ Apycom.com Tree-menu Data --------------------------------------------
//******************************************************************************

// Saving state parameters -----
var tsaveState = 1;              // 0/1 - disable/exnable state saving;
var tsaveMenuN = 0;              // menu number on a page;
var tsavePrefix = "myMenu";      // individual prefix for each menu. Must be not empty.
//------------------------------

var tblankImage      = "img/blank.gif";
var tmenuWidth       = 230;
var tmenuHeight      = 400;      // If equal to 0 or "" - menu height setted automatically (without vertical scrollbar).

var tabsolute        = 1;
var tleft            = 20;
var ttop             = 120;

var tfloatable       = 1;
var tfloatIterations = 6;
var tmoveable        = 0;
var tmoveImage       = "img/movepic.gif";
var tmoveImageHeight = 12;

var tfontStyle       = "normal 8pt Tahoma";
var tfontColor       = ["#854E15","#BF772C"];
var tfontDecoration  = ["none","underline"];

var titemBackColor   = ["#F9E8D7","#F9E8D7"];
var titemAlign       = "left";
var titemBackImage   = ["",""];
var titemCursor      = "hand";
var titemHeight      = 22;

var titemTarget      = "_blank";
var ticonWidth       = 16;
var ticonHeight      = 16;
var ticonAlign       = "left";

var tmenuBackImage   = "";
var tmenuBackColor   = "";
var tmenuBorderColor = "#FFFFFF";
var tmenuBorderStyle = "solid";
var tmenuBorderWidth = 0;

var texpandBtn       =["img/expandbtn2.gif","img/expandbtn2.gif","img/collapsebtn2.gif"];
var texpandBtnW      = 9;
var texpandBtnH      = 9;
var texpandBtnAlign  = "left"

var texpanded = 0;

var tpoints       = 0;
var tpointsImage  = "";
var tpointsVImage = "";
var tpointsCImage = "";

// XP-Style Parameters
var tXPStyle = 1;
var tXPIterations = 5;                  // expand/collapse speed
var tXPTitleTopBackColor = "";
var tXPTitleBackColor    = "#E18A2C";
var tXPTitleLeft    = "img/xptitleleft_orange.gif";
var tXPExpandBtn    = ["img/xpexpand1_orange.gif","img/xpexpand1_orange.gif","img/xpcollapse1_orange.gif","img/xpcollapse1_orange.gif"];
var tXPBtnHeight    = 25;
var tXPTitleBackImg = "img/xptitle_orange.gif";

var tstyles =
[
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#FFFFFF,#FFDFBD", "tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","titemBackColor=#265BCC,#265BCC","tfontColor=#854E15,#AD7235", "tfontDecoration=none,none"],
    ["tfontDecoration=none,none"],
    ["tfontStyle=bold 8pt Tahoma","tfontColor=#444444,#5555FF"],
];

var tXPStyles =
[
    ["tXPTitleBackColor=#F9E7D5", "tXPExpandBtn=img/xpexpand2_orange.gif,img/xpexpand2_orange.gif,img/xpcollapse2_orange.gif,img/xpcollapse2_orange.gif", "tXPTitleBackImg=img/xptitle2_orange.gif"]
];

var tmenuItems =
[
    ["+XP-style Title with Icon", "", "img/xpicon_orange.gif","","", "Main Page",,"0"],
    ["|Information", "testlink.htm", "img/icons/info.gif", "",  "", "Information","_blank"],
    ["|Support",     "",             "img/icons/support.gif",   "", "", "Support",, "2"],
    ["||Contacts",   "mailto:support@apycom.com", "img/icons/contacts.gif", "", "", "Contacts"],
    ["||E-mail",     "mailto:support@apycom.com", "img/icons/email.gif", "", "", "E-mail"],

    ["|Help",       "",              "img/icons/help1.gif", "img/icons/help1.gif", "img/icons/help2.gif", "Help",,"2"],
    ["||Glossary",  "testlink.htm", "img/icons/paper.gif", "",                     "",                   "Glossary"],
    ["||Index",     "testlink.htm", "img/icons/paper.gif", "",                     "",                   "Index"],
    ["||<nobr><input value='search' size=10 style='font-size:10'>&nbsp;<input type='button' value='Search' style='font-size:10'></nobr>",
     "", "img/icons/search.gif", "", "", "Search",,"2"],
     
    ["||<nobr>Contents:&nbsp;<select  style='font-size:10'><option>Item 1</option><option>Item 2</option><option>Item 3</option></select></nobr>",
     "", "img/icons/list.gif", "", "", "Contents",,"2"],

    ["XP-style Title without Icon",   "", "", "","","Download software",,"1","0"],
    ["|Item without icon",          "testlink.htm", ,,, "Item 1 Hint"],
    ["|Item with individual style", "",             ,,, "Item 2 Hint",,"3"],
    ["||SubItem 1",    "testlink.htm", "img/icons/help1.gif", "","", "SubItem 1 Hint"],
    ["||SubItem 2",    "testlink.htm", "img/icons/help1.gif", "","", "SubItem 1 Hint"],
    ["|||SubItem 2_1", "testlink.htm", ,,, "SubItem 1_2 Hint"],

];
apy_tmenuInit();
