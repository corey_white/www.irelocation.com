<?php /* Smarty version 2.5.0, created on 2005-02-02 18:53:09
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/invoice/add.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/invoice/add.tpl', 17, false),)); ?>
<!-- Display the form validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
	<?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display the form to collect the input values -->
<?php if ($this->_tpl_vars['VAR']['invoice_account_id'] == ""): ?>
<form id="invoice_add" name="invoice_add" method="post" action=""> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>title_add<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_account_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->popup('invoice_add','invoice_account_id',$this->_tpl_vars['VAR']['invoice_account_id'],'account_admin','account',"first_name,middle_name,last_name",'form_field',""); ?>

                  </td>
                </tr>
				<?php if ($this->_tpl_vars['list']->is_installed('affiliate') == 1): ?>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_affiliate_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%">
                    <input type="text" id="discount" name="aid" size="12" class="form_field" value="---">
                  </td>
				  <?php endif; ?>
                </tr>
                <tr valign="top">
                  <td width="35%"></td>
                  <td width="65%">
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    <input type="hidden" name="_page" value="invoice:add">
                    <input type="hidden" name="_page_current" value="invoice:add">
                  </td>
                </tr>
              </table>
              </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
 
<?php else: ?>
<form id="invoice_add" name="invoice_add" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                title_add
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_account_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->popup('invoice_add','invoice_account_id',$this->_tpl_vars['VAR']['invoice_account_id'],'account_admin','account',"first_name,middle_name,last_name",'form_field',""); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%">Add Item to Invoice</td>
                  <td width="65%">
                    <?php echo $this->_tpl_vars['list']->menu('no','invoice_product_id','product','sku','all',"form_menu\" onchange=\"showProduct(this.value)\""); ?>

                  </td>
                </tr>
                <tr valign="top">
                  <td width="35%">&nbsp;</td>
                  <td width="65%"> <a href="?_page=discount:add&discount_avail_account_id=<?php echo $this->_tpl_vars['VAR']['invoice_account_id']; ?>
" target="_blank"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    add_discount <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?> </a> 
					
					| <a href="javascript:showCart()"> <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    add_view_items <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> | 
					
					<a href="javascript:showCheckout()"><?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    add_finalize <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
<?php endif; ?>
<?php echo '
  <center> 
    <BR>
	<iframe id="iframeInvoice" style="border:0px; width:0px; height:0px;" scrolling="auto" ALLOWTRANSPARENCY="true" frameborder="0"></iframe> 
  </center>
<script language="JavaScript">
<!-- START
  
var account_id 	= \''; ?>
<?php echo $this->_tpl_vars['VAR']['invoice_account_id']; ?>
<?php echo '\';  
 
function showCart() {
	showIFrame(\'iframeInvoice\',575,500,\'?_page=cart:admin_view&_escape=1&account_id=\'+account_id);
}

function showCheckout() {
	showIFrame(\'iframeInvoice\',575,500,\''; ?>
<?php echo $this->_tpl_vars['SSL_URL']; ?>
<?php echo 'admin.php?_page=checkout:admin_checkout&_escape=1&account_id=\'+account_id);
}
 
function showProduct(product_id) {
	showIFrame(\'iframeInvoice\',575,500,\'?_page=product:admin_details&_escape=1&id=\'+product_id+\'&account_id=\'+account_id);
}
 
//  END -->
</script>
'; ?>
