<?php /* Smarty version 2.5.0, created on 2005-02-02 18:59:35
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/invoice/search_form.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/invoice/search_form.tpl', 16, false),)); ?>
<?php echo $this->_tpl_vars['method']->exe('invoice','search_form'); ?>

<?php if (( $this->_tpl_vars['method']->result == FALSE )): ?>
    <?php echo $this->_tpl_vars['block']->display("core:method_error"); ?>

<?php else: ?>

<form name="invoice_search" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>title_search<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <input type="text" name="invoice_id" value="<?php echo $this->_tpl_vars['VAR']['invoice_id']; ?>
" <?php if ($this->_tpl_vars['invoice_id'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                    &nbsp;&nbsp; 
                    <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                    search_partial 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_affiliate_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <input type="text" name="invoice_affiliate_id" value="<?php echo $this->_tpl_vars['VAR']['invoice_affiliate_id']; ?>
" <?php if ($this->_tpl_vars['invoice_affiliate_id'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                    &nbsp;&nbsp; 
                    <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                    search_partial 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>

                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_process_status 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->bool('invoice_process_status','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_billing_status 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->bool('invoice_billing_status','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_print_status 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->bool('invoice_print_status','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_account_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->popup('invoice_search','invoice_account_id',$this->_tpl_vars['VAR']['invoice_account_id'],'account_admin','account',"first_name,middle_name,last_name",'form_field',""); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_checkout_plugin_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'invoice_checkout_plugin_id','checkout','name','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_tax_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'invoice_tax_id','tax','description','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_billed_currency_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'invoice_billed_currency_id','currency','name','all','form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_date_orig 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->calender_search('invoice_date_orig',$this->_tpl_vars['VAR']['invoice_date_orig'],'form_field',""); ?>

                  </td>
                </tr>				
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_due_date
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <?php echo $this->_tpl_vars['list']->calender_search('invoice_due_date',$this->_tpl_vars['VAR']['invoice_due_date'],'form_field',""); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'invoice')); $this->_plugins['block']['translate'][0](array('module' => 'invoice'), null, $this); ob_start(); ?>
                    field_notice_next_date 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%">
                    <?php echo $this->_tpl_vars['list']->calender_search('invoice_notice_next_date',$this->_tpl_vars['VAR']['invoice_notice_next_date'],'form_field',""); ?>

                  </td>
                </tr>
                <!-- Define the results per page -->
                <tr class="row1" valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_sku 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%">
                    <?php echo $this->_tpl_vars['list']->menu("",'join_product_id','product','sku','all',"form_menu\" onchange=\"showAttributes(this)"); ?>

					<?php echo '<script language="javascript">
						function showAttributes(obj)
						{
							if(obj.value == \'\')
							{
								document.getElementById("attributes1").style.display=\'none\';
								document.getElementById("attributes2").style.display=\'none\';
							}
							else
							{
								document.getElementById("attributes1").style.display=\'block\';
								document.getElementById("attributes2").style.display=\'block\';
							}
						}
					</script>'; ?>

                  </td>
                </tr>
				 
                <tr class="row1" valign="top"> 
                  <td width="35%"> 
				  <DIV id="attributes1" style="display:none">
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    attributes 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
					</DIV>
                  </td>
                  <td width="65%">
				  <DIV id="attributes2" style="display:none">
                    <input type="text" name="item_attributes[0][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[0][1]" class=form_field size="16"> <br>
                    <input type="text" name="item_attributes[1][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[1][1]" class=form_field size="16"> <br>
                    <input type="text" name="item_attributes[2][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[2][1]" class=form_field size="16"> <br>
                    <input type="text" name="item_attributes[3][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[3][1]" class=form_field size="16"> <br>
                    <input type="text" name="item_attributes[4][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[4][1]" class=form_field size="16"> <br>
                    <input type="text" name="item_attributes[5][0]" class=form_field size="16"> = 
                    <input type="text" name="item_attributes[5][1]" class=form_field size="16">  																									
					</DIV>
                  </td>
                </tr> 
				
                <tr class="row1" valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                    search_results_per 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <input type="text" class="form_field" name="limit" size="5" value="<?php echo $this->_tpl_vars['invoice_limit']; ?>
">
                  </td>
                </tr>								
                <!-- Define the order by field per page -->
                <tr class="row1" valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                    search_order_by 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <select class="form_menu" name="order_by">
                      <?php if (count((array)$this->_tpl_vars['invoice'])):
    foreach ((array)$this->_tpl_vars['invoice'] as $this->_tpl_vars['record']):
?>
                      <option value="<?php echo $this->_tpl_vars['record']['field']; ?>
"> 
                      <?php echo $this->_tpl_vars['record']['translate']; ?>

                      </option>
                      <?php endforeach; endif; ?>
                    </select>
                  </td>
                </tr>
                <tr class="row1" valign="top"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>search<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    <input type="hidden" name="_page" value="core:search">
                    <input type="hidden" name="_escape" value="Y">
                    <input type="hidden" name="module" value="invoice">
                    <input type="hidden" name="_next_page_one" value="view">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</form>
<?php echo $this->_tpl_vars['block']->display("core:saved_searches"); ?>

<?php echo $this->_tpl_vars['block']->display("core:recent_searches"); ?>

<?php endif; ?>