<?php /* Smarty version 2.5.0, created on 2005-02-02 18:45:28
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/setup/view.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/setup/view.tpl', 16, false),)); ?><?php echo $this->_tpl_vars['method']->exe('setup','view'); ?>
 <?php if (( $this->_tpl_vars['method']->result == FALSE )): ?> <?php echo $this->_tpl_vars['block']->display("core:method_error"); ?>
 <?php else: ?>

<?php echo '
    <!-- Define the update delete function -->
    <script language="JavaScript">
    <!-- START
        var module = \'setup\';
    	var locations = \''; ?>
<?php echo $this->_tpl_vars['VAR']['module_id']; ?>
<?php echo '\';
    	if (locations != "")
    	{
    		refresh(0,\'#\'+locations)
    	}
    	// Mass update, view, and delete controller
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("'; ?>
<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>alert_delete<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?><?php echo '");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, \'\');		
    		if(ids == \'\') {
    			var url = \'?_page=core:search&module=\' + module + \'&do[]=\' + module + \':delete&delete_id=\' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = \'view&id=\' +ids;
    		}		
    		
    		var doit = \'delete\';
    		var url = \'?_page=\'+ module +\':\'+ page +\'&do[]=\' + module + \':\' + doit + \'&delete_id=\' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
'; ?>


<!-- Loop through each record -->
<?php if (count((array)$this->_tpl_vars['setup'])):
    foreach ((array)$this->_tpl_vars['setup'] as $this->_tpl_vars['setup']):
?> <a name="<?php echo $this->_tpl_vars['setup']['id']; ?>
"></a>

<!-- Display the field validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
   <?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display each record -->
<form name="setup_update" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                title_view
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_name" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_name']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_address 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_address" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_address']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_city 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_city" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_city']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_state 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_state" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_state']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_zip 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_zip" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_zip']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row0" valign="top"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_phone 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_phone" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_phone']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_fax 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_fax" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_fax']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_site_email 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_site_email" class="form_field" value="<?php echo $this->_tpl_vars['setup']['site_email']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_country_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu('no','setup_country_id','country','name',$this->_tpl_vars['setup']['country_id'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_currency_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'setup_currency_id','currency','name',$this->_tpl_vars['setup']['currency_id'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_language_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu_files("",'setup_language_id',$this->_tpl_vars['setup']['language_id'],'language',"","_core.xml",'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_theme_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <p> 
                      <?php echo $this->_tpl_vars['list']->menu_files("",'setup_theme_id',$this->_tpl_vars['setup']['theme_id'],'theme',"",".user_theme",'form_menu'); ?>

                    </p>
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_admin_theme_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu_files("",'setup_admin_theme_id',$this->_tpl_vars['setup']['admin_theme_id'],'theme',"",".admin_theme",'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_group_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'setup_group_id','group','name',$this->_tpl_vars['setup']['group_id'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_setup_email_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'setup_setup_email_id','setup_email','name',$this->_tpl_vars['setup']['setup_email_id'],'form_menu'); ?>

                  </td>
                </tr>
                <?php if ($this->_tpl_vars['list']->is_installed('affiliate')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_affiliate_template_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu("",'setup_affiliate_template_id','affiliate_template','name',$this->_tpl_vars['setup']['setup_affiliate_template_id'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_default_account_status 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_default_account_status',$this->_tpl_vars['setup']['default_account_status'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_db_cache 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_db_cache',$this->_tpl_vars['setup']['db_cache'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_cache_sessions 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_cache_sessions',$this->_tpl_vars['setup']['cache_sessions'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_newsletter_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_newsletter_link',$this->_tpl_vars['setup']['show_newsletter_link'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_newsletter_registration 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_newsletter_registration',$this->_tpl_vars['setup']['newsletter_registration'],'form_menu'); ?>

                  </td>
                </tr>				
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_contact_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_contact_link',$this->_tpl_vars['setup']['show_contact_link'],'form_menu'); ?>

                  </td>
                </tr>
                <?php if ($this->_tpl_vars['list']->is_installed('host_tld')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_domain_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_domain_link',$this->_tpl_vars['setup']['show_domain_link'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_cart_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_cart_link',$this->_tpl_vars['setup']['show_cart_link'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_checkout_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_checkout_link',$this->_tpl_vars['setup']['show_checkout_link'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_product_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_product_link',$this->_tpl_vars['setup']['show_product_link'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_cat_block 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_cat_block',$this->_tpl_vars['setup']['show_cat_block'],'form_menu'); ?>

                  </td>
                </tr>
				
				<?php if ($this->_tpl_vars['list']->is_installed('file')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_file_block 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_file_block',$this->_tpl_vars['setup']['show_file_block'],'form_menu'); ?>

                  </td>
                </tr>
				<?php endif; ?>
				
                <?php if ($this->_tpl_vars['list']->is_installed('static_page')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_static_block 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_static_block',$this->_tpl_vars['setup']['show_static_block'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
				
                <?php if ($this->_tpl_vars['list']->is_installed('affiliate')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_affiliate_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_affiliate_link',$this->_tpl_vars['setup']['show_affiliate_link'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_affiliate_code 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_affiliate_code',$this->_tpl_vars['setup']['show_affiliate_code'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['list']->is_installed('ticket')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_ticket_link 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_ticket_link',$this->_tpl_vars['setup']['show_ticket_link'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_show_discount_code 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_show_discount_code',$this->_tpl_vars['setup']['show_discount_code'],'form_menu'); ?>

                  </td>
                </tr>
                <?php if ($this->_tpl_vars['list']->is_installed('weblog')): ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_weblog 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_weblog',$this->_tpl_vars['setup']['weblog'],'form_menu'); ?>

                  </td>
                </tr>
                <?php endif; ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_os 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <select name="setup_os" class="form_menu">
                      <option value="0" <?php if ($this->_tpl_vars['setup']['setup_os'] == '0'): ?>selected<?php endif; ?>> 
                      Linux </option>
                      <option value="1" <?php if ($this->_tpl_vars['setup']['setup_os'] == '1'): ?>selected<?php endif; ?>> 
                      Windows </option>
                    </select>
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_path_curl 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_path_curl" class="form_field" value="<?php echo $this->_tpl_vars['setup']['path_curl']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_nonssl_url 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_nonssl_url" class="form_field" value="<?php echo $this->_tpl_vars['setup']['nonssl_url']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_ssl_url 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_ssl_url" class="form_field" value="<?php echo $this->_tpl_vars['setup']['ssl_url']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_login_expire 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_login_expire" class="form_field" value="<?php echo $this->_tpl_vars['setup']['login_expire']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_cookie_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_cookie_name" class="form_field" value="<?php echo $this->_tpl_vars['setup']['cookie_name']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_cookie_expire 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_cookie_expire" class="form_field" value="<?php echo $this->_tpl_vars['setup']['cookie_expire']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_error_reporting 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <select name="setup_error_reporting" class="form_menu">
                      <option value="0"> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_none 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="1" <?php if ($this->_tpl_vars['setup']['error_reporting'] == '1'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_error 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="2"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '2'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_warning 
                      <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                      </option>
                      <option value="4"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '4'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_parse 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="8"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '8'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_notice 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="16"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '16'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_core_error 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="32"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '32'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_core_warning 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="64"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '64'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_compile_error 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="128"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '128'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_compile_warning 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="256"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '256'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_user_error 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="512"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '512'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_user_warning 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="1024"<?php if ($this->_tpl_vars['setup']['error_reporting'] == '1024'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_user_notice 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="E_ALL"<?php if ($this->_tpl_vars['setup']['error_reporting'] == 'E_ALL'): ?>selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                      e_all 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                    </select>
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_debug 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('setup_debug',$this->_tpl_vars['setup']['debug'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_search_expire 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_search_expire" class="form_field" value="<?php echo $this->_tpl_vars['setup']['search_expire']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_decimal_place 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_decimal_place" class="form_field" value="<?php echo $this->_tpl_vars['setup']['decimal_place']; ?>
" size="32">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%" valign="top"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_time_format 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <p> 
                      <input type="text" name="setup_time_format" class="form_field" value="<?php echo $this->_tpl_vars['setup']['time_format']; ?>
" size="32">
                    </p>
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%" valign="top"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_date_format 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->setup_default_date($this->_tpl_vars['setup']['date_format'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_login_attempt_try 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_login_attempt_try" class="form_field" value="<?php echo $this->_tpl_vars['setup']['login_attempt_try']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_login_attempt_time 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_login_attempt_time" class="form_field" value="<?php echo $this->_tpl_vars['setup']['login_attempt_time']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_login_attempt_lock 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_login_attempt_lock" class="form_field" value="<?php echo $this->_tpl_vars['setup']['login_attempt_lock']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_billing_weekday 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_billing_weekday" class="form_field" value="<?php echo $this->_tpl_vars['setup']['billing_weekday']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_grace_period 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_grace_period" class="form_field" value="<?php echo $this->_tpl_vars['setup']['grace_period']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_max_billing_notice 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_max_billing_notice" class="form_field" value="<?php echo $this->_tpl_vars['setup']['max_billing_notice']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_max_inv_gen_period 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="setup_max_inv_gen_period" class="form_field" value="<?php echo $this->_tpl_vars['setup']['max_inv_gen_period']; ?>
" size="4">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_license_key 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%">
                    <input type="text" name="setup_license_key" class="form_field" value="<?php echo $this->_tpl_vars['setup']['license_key']; ?>
" size="40">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%" valign="top"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'setup')); $this->_plugins['block']['translate'][0](array('module' => 'setup'), null, $this); ob_start(); ?>
                    field_license_code 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%">
                    <textarea name="setup_license_code"  cols="65" rows="10" wrap="Yes" class="form_field"><?php echo $this->_tpl_vars['setup']['license_code']; ?>
</textarea>
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left">
                  <td width="50%"></td>
                  <td width="50%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr> 
                        <td> 
                          <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                        </td>
                        <td align="right"> 
                          <input type="button" name="delete" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>delete<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button" onClick="delete_record('<?php echo $this->_tpl_vars['setup']['id']; ?>
','<?php echo $this->_tpl_vars['VAR']['id']; ?>
');">
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="_page" value="setup:view">
    <input type="hidden" name="setup_id" value="<?php echo $this->_tpl_vars['setup']['id']; ?>
">
    <input type="hidden" name="do[]" value="setup:update">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['VAR']['id']; ?>
">
  </form>
  <?php endforeach; endif; ?>    
<?php endif; ?>