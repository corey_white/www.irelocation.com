<?php /* Smarty version 2.5.0, created on 2005-02-02 18:51:53
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account_admin/add.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account_admin/add.tpl', 17, false),)); ?>
<!-- Display the form validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
	<?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display the form to collect the input values -->
<form id="account_admin_add" name="account_admin_add" method="post" action="">
  
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                title_add 
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_username 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <input type="text" name="account_admin_username" class="form_field" value="<?php echo $this->_tpl_vars['VAR']['account_admin_username']; ?>
">
                    </b></td>
                  <td width="33%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    blank_to_autogen 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"><b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_password 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_password" class="form_field" value="<?php echo $this->_tpl_vars['VAR']['account_admin_password']; ?>
">
                  </td>
                  <td width="33%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    blank_to_autogen 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>
                <tr valign="top">
                  <td width="33%"><b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    welcome_email 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%">
                    <input type="checkbox" name="welcome_email" value="1" checked>
                  </td>
                  <td width="33%">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_status 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_email 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_company 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_admin_status'] == ""): ?>
                    <?php if (@constant('DEFAULT_ACCOUNT_STATUS') != '1'): ?>
                    <?php echo $this->_tpl_vars['list']->bool('account_admin_status','1','form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->bool('account_admin_status','0','form_menu'); ?>

                    <?php endif; ?>
                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->bool('account_admin_status',$this->_tpl_vars['VAR']['account_admin_status'],'form_menu'); ?>

                    <?php endif; ?>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_email" value="<?php echo $this->_tpl_vars['VAR']['account_admin_email']; ?>
" <?php if ($this->_tpl_vars['account_admin_email'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_company" value="<?php echo $this->_tpl_vars['VAR']['account_admin_company']; ?>
" <?php if ($this->_tpl_vars['account_admin_company'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
						
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_first_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_middle_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_last_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <input type="text" name="account_admin_first_name" value="<?php echo $this->_tpl_vars['VAR']['account_admin_first_name']; ?>
" <?php if ($this->_tpl_vars['account_admin_first_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_middle_name" value="<?php echo $this->_tpl_vars['VAR']['account_admin_middle_name']; ?>
" <?php if ($this->_tpl_vars['account_admin_middle_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_last_name" value="<?php echo $this->_tpl_vars['VAR']['account_admin_last_name']; ?>
" <?php if ($this->_tpl_vars['account_admin_last_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_title 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_address1 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_address2 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <select name="account_admin_title" class="form_field">
                      <option value="Mr"<?php if ($this->_tpl_vars['VAR']['account_admin_title'] == 'Mr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                      mr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Mrs"<?php if ($this->_tpl_vars['VAR']['account_admin_title'] == 'Mrs'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                      mrs 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Miss"<?php if ($this->_tpl_vars['VAR']['account_admin_title'] == 'Miss'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                      miss 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>					  
                      <option value="Dr"<?php if ($this->_tpl_vars['VAR']['account_admin_title'] == 'Dr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                      dr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Prof"<?php if ($this->_tpl_vars['VAR']['account_admin_title'] == 'Prof'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                      prof 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                    </select>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_address1"  value="<?php echo $this->_tpl_vars['VAR']['account_admin_address1']; ?>
" <?php if ($this->_tpl_vars['account_admin_address1'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_address2"  value="<?php echo $this->_tpl_vars['VAR']['account_admin_address2']; ?>
" <?php if ($this->_tpl_vars['account_admin_address2'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_city 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_state 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_zip 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <input type="text" name="account_admin_city"  value="<?php echo $this->_tpl_vars['VAR']['account_admin_city']; ?>
" <?php if ($this->_tpl_vars['account_admin_city'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_state" value="<?php echo $this->_tpl_vars['VAR']['account_admin_state']; ?>
" <?php if ($this->_tpl_vars['account_admin_state'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="33%"> 
                    <input type="text" name="account_admin_zip"  value="<?php echo $this->_tpl_vars['VAR']['account_admin_zip']; ?>
" <?php if ($this->_tpl_vars['account_admin_zip'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_country_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_date_expire 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_email_html 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_admin_country_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_admin_country_id','country','name',$this->_tpl_vars['VAR']['account_admin_country_id'],'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_admin_country_id','country','name',@constant('DEFAULT_COUNTRY'),'form_menu'); ?>

                    <?php endif; ?>
                  </td>
                  <td width="33%"> 
                    <?php echo $this->_tpl_vars['list']->calender_add('account_admin_date_expire',$this->_tpl_vars['VAR']['account_admin_date_expire'],'form_field'); ?>

                  </td>
                  <td width="33%"> 
                    <?php echo $this->_tpl_vars['list']->bool('account_admin_email_html',$this->_tpl_vars['VAR']['account_email_type'],'form_menu'); ?>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_language_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_theme_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_currency_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_admin_language_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu_files("",'account_admin_language_id',$this->_tpl_vars['VAR']['account_admin_language_id'],'language',"","_core.xml",'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu_files("",'account_admin_language_id',@constant('DEFAULT_LANGUAGE'),'language',"","_core.xml",'form_menu'); ?>

                    <?php endif; ?>
                  </td>
                  <td width="33%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_admin_theme_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu_files("",'account_admin_theme_id',$this->_tpl_vars['VAR']['account_admin_theme_id'],'theme',"",".user_theme",'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu_files("",'account_admin_theme_id',@constant('DEFAULT_THEME'),'theme',"",".user_theme",'form_menu'); ?>

                    <?php endif; ?>
                  </td>
                  <td width="33%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_admin_currency_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_admin_currency_id','currency','name',$this->_tpl_vars['VAR']['account_admin_currency_id'],'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_admin_currency_id','currency','name',@constant('DEFAULT_CURRENCY'),'form_menu'); ?>

                    <?php endif; ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>
                    authorized_groups 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php if ($this->_tpl_vars['list']->is_installed('affiliate')): ?>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_affiliate_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <?php endif; ?>
                    </b></td>
                  <td width="33%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account_admin')); $this->_plugins['block']['translate'][0](array('module' => 'account_admin'), null, $this); ob_start(); ?>
                    field_misc 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="33%"> 
                    <?php echo $this->_tpl_vars['list']->select_groups($this->_tpl_vars['VAR']['groups'],'groups','form_field','10',""); ?>

                  </td>
                  <td width="33%"> <b> 
                    <?php if ($this->_tpl_vars['list']->is_installed('affiliate')): ?>
                    <?php if ($this->_tpl_vars['VAR']['account_admin_affiliate_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu("",'account_admin_affiliate_id','affiliate','id',$this->_tpl_vars['VAR']['account_admin_affiliate_id'],'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu("",'account_admin_affiliate_id','affiliate','id','all','form_menu'); ?>

                    <?php endif; ?>
                    <?php endif; ?>
                    </b> </td>
                  <td width="33%"> 
                    <textarea name="account_admin_misc" cols="25" rows="2" <?php if ($this->_tpl_vars['account_admin_misc'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>><?php echo $this->_tpl_vars['VAR']['account_admin_misc']; ?>
</textarea>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <?php echo $this->_tpl_vars['method']->exe('account','static_var'); ?>
 
                <?php if (count((array)$this->_tpl_vars['static_var'])):
    foreach ((array)$this->_tpl_vars['static_var'] as $this->_tpl_vars['record']):
?>
                <tr valign="top"> 
                  <td width="33%"> 
                    <?php echo $this->_tpl_vars['record']['name']; ?>

                  </td>
                  <td width="67%"> 
                    <?php echo $this->_tpl_vars['record']['html']; ?>

                  </td>
                </tr>
                <?php endforeach; endif; ?>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <?php echo $this->_tpl_vars['method']->exe('account','static_var'); ?>

                <?php echo $this->_tpl_vars['block']->display("core:method_error"); ?>

                <?php if (count((array)$this->_tpl_vars['static_var'])):
    foreach ((array)$this->_tpl_vars['static_var'] as $this->_tpl_vars['record']):
?>
                <?php endforeach; endif; ?>
                <tr valign="top"> 
                  <td width="33%">&nbsp;</td>
                  <td width="67%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    <input type="hidden" name="_page" value="account_admin:view">
                    <input type="hidden" name="_page_current" value="account_admin:add">
                    <input type="hidden" name="do[]" value="account_admin:add">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>