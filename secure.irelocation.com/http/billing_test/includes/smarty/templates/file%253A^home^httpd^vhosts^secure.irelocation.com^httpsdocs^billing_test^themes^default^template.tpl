<?php /* Smarty version 2.5.0, created on 2005-02-02 18:45:27
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/template.tpl */ ?>
<HTML>
<HEAD>
<TITLE><?php echo @constant('SITE_NAME'); ?>
 - Powered by AgileBill</TITLE>

<SCRIPT LANGUAGE="JavaScript">
<!-- START
	var escape			= "";
	var sess_expires 	= "<?php echo @constant('SESSION_EXPIRE'); ?>
";
	var cookie_name		= "<?php echo @constant('COOKIE_NAME'); ?>
";
	var SESS	 		= "<?php echo $this->_tpl_vars['SESS']; ?>
";
	var URL				= "<?php echo $this->_tpl_vars['URL']; ?>
";
	var SSL_URL			= "<?php echo $this->_tpl_vars['SSL_URL']; ?>
";
	var THEME_NAME  	= "<?php echo $this->_tpl_vars['THEME_NAME']; ?>
";
	var REDIRECT_PAGE 	= "<?php echo @constant('REDIRECT_PAGE'); ?>
";
<?php echo '
  if (top.location != location) {
    top.location.href = document.location.href ;
  }
'; ?>


//  END -->
</SCRIPT>

<!-- Load the main stylesheet -->
<link rel="stylesheet" href="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/style.css" type="text/css">

<!-- Load the main javascript code -->
<SCRIPT SRC="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/top.js"></SCRIPT>
 
</HEAD> 
<body bgcolor="#CCCCCC" text="#000000" leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" class="body">
<img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="160" height="15" align="middle"> 
<table width="700" border="0" align="center" bgcolor="#006600" cellpadding="1" cellspacing="0">
  <tr> 
    <td> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class=body>
        <tr> 
          <td> 
            <div align="center"><br>
              <img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/logo.gif"><br>
              <br>
            </div>
          </td>
        </tr>
        <tr> 
          <td bgcolor="#CCCCCC" height="1"></td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0" height="200">
              <tr> 
           
                <td width="160" valign="top" height="200" bgcolor="F5F5F5"> <img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="160" height="10"> 
                  <?php echo $this->_tpl_vars['block']->display("TEMPLATE:left"); ?>

                </td>
              <td width="1" bgcolor="#CCCCCC"> <img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="1" height="1"><br>
              </td>
              <td valign="top" width="575" bgcolor="#FFFFFF"> 
                <table width="100%" border="0" cellspacing="0" cellpadding="14">
                  <tr> 
                    <td valign="top" class="body"> 
                      <!-- display the alert block -->
                      <?php if ($this->_tpl_vars['alert']): ?>
                      <?php echo $this->_tpl_vars['block']->display("core:alert"); ?>

                      <?php endif; ?>
                      <!-- display the main page -->
                      <?php if ($this->_tpl_vars['VAR']['_page'] != ''): ?>
                      <?php echo $this->_tpl_vars['block']->display(@constant('THEME_PAGE')); ?>

                      <?php elseif ($this->_tpl_vars['SESS_LOGGED'] == '1'): ?>
                      <?php echo $this->_tpl_vars['block']->display("account:account"); ?>

                      <?php else: ?>
                      <?php echo $this->_tpl_vars['block']->display("account:login"); ?>

                      <?php endif; ?>
                    </td>
                  </tr>
                </table>
              </td>
              <td width="1" bgcolor="#FFFFFF"> </td>
              </tr>
              <tr> 
                <td width="160" valign="top" bgcolor="#F5F5F5"></td>
                <td width="1" bgcolor="#CCCCCC"></td>
                <td height="1" width="200" bgcolor="#CCCCCC"><img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="1" height="1"></td>
                <td width="1" bgcolor="#FFFFFF"></td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td bgcolor="F5F5F5"><br>
          </td>
        </tr>
        <tr> 
          <td bgcolor="#999999" height="1"></td>
        </tr>
        <tr> 
          <td bgcolor="F1F1F1"> 
            <div align="center"><img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="160" height="5"><br>
              <a href="http://agileco.com/"><font color="#999999">Billing Software</font></a><font color="#999999"> 
              powered by</font> <a href="http://agilebill.com/"><font color="#000000">Agile</font><font color="#006600">Bill</font></a><font color="#999999">&#153;</font><br>
              <img src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/invisible.gif" width="160" height="5"></div>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</BODY>
</HTML>