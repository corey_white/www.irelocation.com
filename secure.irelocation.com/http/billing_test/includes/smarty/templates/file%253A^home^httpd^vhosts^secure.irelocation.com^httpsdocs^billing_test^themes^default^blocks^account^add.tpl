<?php /* Smarty version 2.5.0, created on 2005-02-02 18:50:52
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account/add.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account/add.tpl', 27, false),)); ?><!-- Make sure user is logged out -->
<?php if (@constant('SESS_LOGGED')): ?>
Please logout before attempting to register for an account 
<?php else: ?>

<!-- Load the JSCalender code -->
<link rel="stylesheet" type="text/css" media="all" href="includes/jscalendar/calendar-blue.css" title="win2k-1" />
<script type="text/javascript" src="includes/jscalendar/calendar_stripped.js"></script>
<script type="text/javascript" src="includes/jscalendar/lang/calendar-<?php echo @constant('LANG'); ?>
.js"></script>
<script type="text/javascript" src="includes/jscalendar/calendar-setup_stripped.js"></script>


<!-- Display the form validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
<?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>
<!-- Display the form to collect the input values -->
<form id="account_add" name="account_add" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center">
                <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                title_add
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_username 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_username" value="<?php echo $this->_tpl_vars['VAR']['account_username']; ?>
" <?php if ($this->_tpl_vars['account_username'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_password 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="password" name="account_password" <?php if ($this->_tpl_vars['account_password'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?> value="<?php echo $this->_tpl_vars['confirm_account_password']; ?>
">
                  </td>
                </tr>
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_confirm_password 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="password" name="confirm_password" <?php if ($this->_tpl_vars['account_password'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?> value="<?php echo $this->_tpl_vars['confirm_account_password']; ?>
">
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_email 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_email" value="<?php echo $this->_tpl_vars['VAR']['account_email']; ?>
" <?php if ($this->_tpl_vars['account_email'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_company 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_company" value="<?php echo $this->_tpl_vars['VAR']['account_company']; ?>
" <?php if ($this->_tpl_vars['account_company'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_first_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_first_name" value="<?php echo $this->_tpl_vars['VAR']['account_first_name']; ?>
" <?php if ($this->_tpl_vars['account_first_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_middle_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_middle_name" value="<?php echo $this->_tpl_vars['VAR']['account_middle_name']; ?>
" <?php if ($this->_tpl_vars['account_middle_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_last_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_last_name" value="<?php echo $this->_tpl_vars['VAR']['account_last_name']; ?>
" <?php if ($this->_tpl_vars['account_last_name'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_title 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <select name="account_title" class="form_field">
                      <option value="Mr"<?php if ($this->_tpl_vars['VAR']['account_title'] == 'Mr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      mr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Mrs"<?php if ($this->_tpl_vars['VAR']['account_title'] == 'Mrs'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      mrs 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Miss"<?php if ($this->_tpl_vars['VAR']['account_title'] == 'Miss'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      miss 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>					  
                      <option value="Dr"<?php if ($this->_tpl_vars['VAR']['account_title'] == 'Dr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      dr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Prof"<?php if ($this->_tpl_vars['VAR']['account_title'] == 'Prof'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      prof 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                    </select>
                  </td>
                </tr>
				
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_address1 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address1" value="<?php echo $this->_tpl_vars['VAR']['account_address1']; ?>
" <?php if ($this->_tpl_vars['account_address1'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
				
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_address2 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address2" value="<?php echo $this->_tpl_vars['VAR']['account_address2']; ?>
" <?php if ($this->_tpl_vars['account_address2'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
				
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_city 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_city" value="<?php echo $this->_tpl_vars['VAR']['account_city']; ?>
" <?php if ($this->_tpl_vars['account_city'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
				
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_state 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_state" value="<?php echo $this->_tpl_vars['VAR']['account_state']; ?>
" <?php if ($this->_tpl_vars['account_state'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
				
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_zip 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_zip" value="<?php echo $this->_tpl_vars['VAR']['account_zip']; ?>
" <?php if ($this->_tpl_vars['account_zip'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
																								
                <tr valign="top" class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_country_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <?php if ($this->_tpl_vars['VAR']['account_country_id'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_country_id','country','name',$this->_tpl_vars['VAR']['account_country_id'],'form_field'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->menu('no','account_country_id','country','name',@constant('DEFAULT_COUNTRY'),'form_field'); ?>

                    <?php endif; ?>
                  </td>
                </tr>
               
			    <?php echo $this->_tpl_vars['method']->exe('account','static_var'); ?>
 
                <?php if (count((array)$this->_tpl_vars['static_var'])):
    foreach ((array)$this->_tpl_vars['static_var'] as $this->_tpl_vars['record']):
?>
                <tr valign="top"> 
                  <td width="29%"> 
                    <?php echo $this->_tpl_vars['record']['name']; ?>

                  </td>
                  <td width="71%"> 
                    <?php echo $this->_tpl_vars['record']['html']; ?>

                  </td>
                </tr>
                <?php endforeach; endif; ?>
				
                <?php if (@constant('NEWSLETTER_REGISTRATION') == '1'): ?>
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    subscribe_newsletters 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <?php echo $this->_tpl_vars['method']->exe('newsletter','check_list_registration'); ?>
 
                  </td>
                  <?php endif; ?>
                </tr>
                <tr valign="top"  class="row2"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_email_html 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <?php echo $this->_tpl_vars['list']->bool('account_email_type',$this->_tpl_vars['VAR']['account_email_type'],'form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="43%">&nbsp;</td>
                  <td width="57%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    
					<input type="hidden" name="_page" value="<?php echo $this->_tpl_vars['VAR']['_page']; ?>
">
					
					<?php if ($this->_tpl_vars['VAR']['_page'] == ""): ?>
                    <input type="hidden" name="_page_current" value="account:add">
					<?php else: ?>
					<input type="hidden" name="_page_current" value="<?php echo $this->_tpl_vars['VAR']['_page']; ?>
">
					<?php endif; ?>
					
                    <input type="hidden" name="do[]" value="account:add">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>
<?php endif; ?>