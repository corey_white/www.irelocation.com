<?php /* Smarty version 2.5.0, created on 2005-02-02 18:52:45
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/blocked_ip/add.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/blocked_ip/add.tpl', 18, false),)); ?>

<!-- Display the form validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
	<?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display the form to collect the input values -->
<form id="blocked_ip_add" name="blocked_ip_add" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                <?php $this->_tag_stack[] = array('translate', array('module' => 'blocked_ip')); $this->_plugins['block']['translate'][0](array('module' => 'blocked_ip'), null, $this); ob_start(); ?>
                title_add
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'blocked_ip')); $this->_plugins['block']['translate'][0](array('module' => 'blocked_ip'), null, $this); ob_start(); ?>
                    field_ip 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <input type="text" name="blocked_ip_ip" value="<?php echo $this->_tpl_vars['VAR']['blocked_ip_ip']; ?>
" <?php if ($this->_tpl_vars['blocked_ip_ip'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'blocked_ip')); $this->_plugins['block']['translate'][0](array('module' => 'blocked_ip'), null, $this); ob_start(); ?>
                    field_notes 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="65%"> 
                    <textarea name="blocked_ip_notes" cols="40" rows="5" <?php if ($this->_tpl_vars['blocked_ip_notes'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>><?php echo $this->_tpl_vars['VAR']['blocked_ip_notes']; ?>
</textarea>
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="35%"></td>
                  <td width="65%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    <input type="hidden" name="_page" value="blocked_ip:view">
                    <input type="hidden" name="_page_current" value="blocked_ip:add">
                    <input type="hidden" name="do[]" value="blocked_ip:add">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>