<?php /* Smarty version 2.5.0, created on 2005-02-02 18:52:10
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/product/add.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/product/add.tpl', 24, false),
array('block', 'htmlarea', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/product/add.tpl', 142, false),)); ?><?php echo '
	<script type="text/javascript"> 
	   var _editor_url  = "includes/htmlarea/";
	   var _editor_lang = "'; ?>
<?php echo @constant('SESS_LANGUAGE'); ?>
<?php echo '";
	</script>	
	<script type="text/javascript" src="includes/htmlarea/htmlarea.js"></script>	
'; ?>


<!-- Display the form validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
	<?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display the form to collect the input values -->
<form id="product_add" name="product_add" method="post" action="">

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr>
    <td>
      <table width="100%" border="0" cellspacing="1" cellpadding="0">
        <tr valign="top">
          <td width="65%" class="table_heading">
            <center>
              <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>title_add<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
            </center>
          </td>
        </tr>
        <tr valign="top">
          <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_sku 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product_translate')); $this->_plugins['block']['translate'][0](array('module' => 'product_translate'), null, $this); ob_start(); ?>
                    field_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <input type="text" name="product_sku" value="<?php echo $this->_tpl_vars['VAR']['product_sku']; ?>
" <?php if ($this->_tpl_vars['product_sku'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                  <td width="50%"> 
                    <input type="text" name="translate_name" value="<?php echo $this->_tpl_vars['VAR']['translate_name']; ?>
" class="form_field">
                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_type 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_avail_category_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <input type="radio" name="product_price_type" value="0" <?php if ($this->_tpl_vars['VAR']['product_price_type'] == '0' || $this->_tpl_vars['VAR']['product_price_type'] == ""): ?>checked<?php endif; ?>>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_one 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <br>
                    <input type="radio" name="product_price_type" value="1" <?php if ($this->_tpl_vars['VAR']['product_price_type'] == '1'): ?>checked<?php endif; ?>>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_recurr 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <br>
                    <input type="radio" name="product_price_type" value="2" <?php if ($this->_tpl_vars['VAR']['product_price_type'] == '2'): ?>checked<?php endif; ?>>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_trial 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu_multi($this->_tpl_vars['VAR']['product_avail_category_id'],'product_avail_category_id','product_cat','name','5',"",'form_menu'); ?>

                  </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
                      <tr> 
                        <td width="50%"><b> 
                          <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                          field_price_base 
                          <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                          </b></td>
                        <td width="50%"><b> 
                          <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                          field_price_setup 
                          <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                          </b> </td>
                      </tr>
                    </table>
                  </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_taxable 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="row1">
                      <tr> 
                        <td width="50%"> 
                          <input type="text" name="product_price_base" value="<?php echo $this->_tpl_vars['VAR']['product_price_base']; ?>
" <?php if ($this->_tpl_vars['product_price_base'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?> size="5">
                          <?php echo $this->_tpl_vars['list']->currency_iso(""); ?>

                        </td>
                        <td width="50%"> 
                          <input type="text" name="product_price_setup" value="<?php echo $this->_tpl_vars['VAR']['product_price_setup']; ?>
" <?php if ($this->_tpl_vars['product_price_setup'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?> size="5">
                          <?php echo $this->_tpl_vars['list']->currency_iso(""); ?>

                        </td>
                      </tr>
                    </table>
                    <b> </b></td>
                  <td width="50%"> 
                    <?php if ($this->_tpl_vars['VAR']['product_taxable'] != ""): ?>
                    <?php echo $this->_tpl_vars['list']->bool('product_taxable',$this->_tpl_vars['VAR']['product_taxable'],'form_menu'); ?>

                    <?php else: ?>
                    <?php echo $this->_tpl_vars['list']->bool('product_taxable','1','form_menu'); ?>

                    <?php endif; ?>
                  </td>
                </tr>
              </table>
              <br>
              <table width="100%" border="0" cellspacing="0" cellpadding="5" class="body">
                <tr> 
                  <td> <b>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product_translate')); $this->_plugins['block']['translate'][0](array('module' => 'product_translate'), null, $this); ob_start(); ?>
                    field_description_short 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr> 
                  <td> 
                    <?php $this->_tag_stack[] = array('htmlarea', array('field' => 'translate_description_short','width' => 550,'height' => 150)); $this->_plugins['block']['htmlarea'][0](array('field' => 'translate_description_short','width' => 550,'height' => 150), null, $this); ob_start(); ?>
                    <?php echo $this->_tpl_vars['VAR']['translate_description_short']; ?>

                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['htmlarea'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>
                <tr> 
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td> <b>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product_translate')); $this->_plugins['block']['translate'][0](array('module' => 'product_translate'), null, $this); ob_start(); ?>
                    field_description_full 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr> 
                  <td> 
				  <?php $this->_tag_stack[] = array('htmlarea', array('field' => 'translate_description_full','width' => 550,'height' => 200)); $this->_plugins['block']['htmlarea'][0](array('field' => 'translate_description_full','width' => 550,'height' => 200), null, $this); ob_start(); ?><?php echo $this->_tpl_vars['VAR']['translate_description_full']; ?>
 <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['htmlarea'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?> 
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                    <input type="hidden" name="_page" value="product:view">
                    <input type="hidden" name="_page_current" value="product:add">
                    <input type="hidden" name="do[]" value="product:add">
                    <input type="hidden" name="product_active" value="1">
                    <input type="hidden" name="product_plugin" value="0">
                    <input type="hidden" name="product_discount" value="0">
                    <input type="hidden" name="product_date_orig" value="<?php echo $this->_tpl_vars['list']->date(time()); ?>
">
                    <input type="hidden" name="product_date_last" value="<?php echo time(); ?>
">
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  </form>