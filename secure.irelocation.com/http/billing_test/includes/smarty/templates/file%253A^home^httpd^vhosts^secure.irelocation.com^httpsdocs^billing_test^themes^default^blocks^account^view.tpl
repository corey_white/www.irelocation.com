<?php /* Smarty version 2.5.0, created on 2005-02-02 18:49:11
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account/view.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/account/view.tpl', 33, false),)); ?><!-- Load the JSCalender code -->
<link rel="stylesheet" type="text/css" media="all" href="includes/jscalendar/calendar-blue.css" title="win2k-1" />
<script type="text/javascript" src="includes/jscalendar/calendar_stripped.js"></script>
<script type="text/javascript" src="includes/jscalendar/lang/calendar-<?php echo @constant('LANG'); ?>
.js"></script>
<script type="text/javascript" src="includes/jscalendar/calendar-setup_stripped.js"></script>


<?php if (@constant('SESS_LOGGED') != true): ?>
	<?php echo $this->_tpl_vars['block']->display("account:login"); ?>

<?php else: ?>

<?php echo $this->_tpl_vars['method']->exe('account','view'); ?>
 <?php if (( $this->_tpl_vars['method']->result == FALSE )): ?> 
<?php echo $this->_tpl_vars['block']->display("core:method_error"); ?>
 <?php else: ?> 

<!-- Loop through each record -->
<?php if (count((array)$this->_tpl_vars['account'])):
    foreach ((array)$this->_tpl_vars['account'] as $this->_tpl_vars['account']):
?>  

<!-- Display the field validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
   <?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display each record -->
<form id="update_form" name="update_form" method="post" action="">
  
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
    <tr> 
      <td> 
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <div align="center"> 
                <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                title_view
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </div>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row4" valign="top"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_date_last 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <?php echo $this->_tpl_vars['list']->date_time($this->_tpl_vars['account']['date_last']); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_username 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> <b> 
                    <?php echo $this->_tpl_vars['account']['username']; ?>

                    </b> 
                    <input type="hidden" name="account_username" value="<?php echo $this->_tpl_vars['account']['username']; ?>
">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_update_password 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_password" class="form_field" value="">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_confirm_password 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="confirm_password" class="form_field" value="">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%" height="20"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_email 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%" height="20"> 
                    <input type="text" name="account_email" class="form_field" value="<?php echo $this->_tpl_vars['account']['email']; ?>
">
                  </td>
                </tr>				
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_company 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_company" class="form_field" value="<?php echo $this->_tpl_vars['account']['company']; ?>
">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_first_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_first_name" class="form_field" value="<?php echo $this->_tpl_vars['account']['first_name']; ?>
">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_middle_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_middle_name" class="form_field" value="<?php echo $this->_tpl_vars['account']['middle_name']; ?>
">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_last_name 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <input type="text" name="account_last_name" class="form_field" value="<?php echo $this->_tpl_vars['account']['last_name']; ?>
">
                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_title 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <select name="account_title" class="form_menu">
                      <option value="Mr"<?php if ($this->_tpl_vars['account']['title'] == 'Mr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      mr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Mrs"<?php if ($this->_tpl_vars['account']['title'] == 'Mrs'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      mrs 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Miss"<?php if ($this->_tpl_vars['account']['title'] == 'Miss'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      miss 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>					  
                      <option value="Dr"<?php if ($this->_tpl_vars['account']['title'] == 'Dr'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      dr 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="Prof"<?php if ($this->_tpl_vars['account']['title'] == 'Prof'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                      prof 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                    </select>
                  </td>
                </tr>			
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_address1 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address1" value="<?php echo $this->_tpl_vars['account']['address1']; ?>
" <?php if ($this->_tpl_vars['account_address1'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_address2 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_address2" value="<?php echo $this->_tpl_vars['account']['address2']; ?>
" <?php if ($this->_tpl_vars['account_address2'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_city 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_city" value="<?php echo $this->_tpl_vars['account']['city']; ?>
" <?php if ($this->_tpl_vars['account_city'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_state 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_state" value="<?php echo $this->_tpl_vars['account']['state']; ?>
" <?php if ($this->_tpl_vars['account_state'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr> 
                <tr valign="top" class="row1"> 
                  <td width="43%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_zip 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="57%"> 
                    <input type="text" name="account_zip" value="<?php echo $this->_tpl_vars['account']['zip']; ?>
" <?php if ($this->_tpl_vars['account_zip'] == true): ?>class="form_field_error"<?php else: ?>class="form_field"<?php endif; ?>>
                  </td>
                </tr> 
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_country_id
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%">
                    <?php echo $this->_tpl_vars['list']->menu('no','cid','country','name',$this->_tpl_vars['account']['country_id'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_language_id
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <?php echo $this->_tpl_vars['list']->menu_files("",'lid',$this->_tpl_vars['account']['language_id'],'language',"","_core.xml",'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_currency_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <?php echo $this->_tpl_vars['list']->menu('no','cyid','currency','name',$this->_tpl_vars['account']['currency_id'],'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_theme_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%">   
                    <?php echo $this->_tpl_vars['list']->menu_files("",'tid',$this->_tpl_vars['account']['theme_id'],'theme',"",".user_theme",'form_menu'); ?>

                  </td>
                </tr>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'account')); $this->_plugins['block']['translate'][0](array('module' => 'account'), null, $this); ob_start(); ?>
                    field_email_html 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                  <td width="69%"> 
                    <?php echo $this->_tpl_vars['list']->bool('account_email_type',$this->_tpl_vars['account']['email_type'],'form_menu'); ?>

                  </td>
                </tr>
                <?php if (count((array)$this->_tpl_vars['static_var'])):
    foreach ((array)$this->_tpl_vars['static_var'] as $this->_tpl_vars['record']):
?>
                <tr valign="top"> 
                  <td width="29%"> 
                    <?php echo $this->_tpl_vars['record']['name']; ?>

                  </td>
                  <td width="71%"> 
                    <?php echo $this->_tpl_vars['record']['html']; ?>

                  </td>
                </tr>
                <?php endforeach; endif; ?>
                <tr class="row1" valign="middle" align="left"> 
                  <td width="31%"></td>
                  <td width="69%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <input type="hidden" name="_page" value="account:view">
	<input type="hidden" name="_page_current" value="account:view">
    <input type="hidden" name="do[]" value="account:update">
	
  </form>
  <?php endforeach; endif; ?>    
<?php endif; ?>
<?php endif; ?>