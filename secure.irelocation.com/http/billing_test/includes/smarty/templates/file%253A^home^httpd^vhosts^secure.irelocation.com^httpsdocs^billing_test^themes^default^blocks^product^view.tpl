<?php /* Smarty version 2.5.0, created on 2005-02-02 18:57:21
         compiled from file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/product/view.tpl */ ?>
<?php $this->_load_plugins(array(
array('block', 'translate', 'file:/home/httpd/vhosts/secure.irelocation.com/httpsdocs/billing_test/themes/default/blocks/product/view.tpl', 22, false),)); ?>
<?php echo $this->_tpl_vars['method']->exe('product','view'); ?>
 <?php if (( $this->_tpl_vars['method']->result == FALSE )): ?> <?php echo $this->_tpl_vars['block']->display("core:method_error"); ?>
 <?php else: ?>

<?php echo '
	<script src="themes/'; ?>
<?php echo $this->_tpl_vars['THEME_NAME']; ?>
<?php echo '/view.js"></script>
    <script language="JavaScript"> 
        var module 		= \'product\';
    	var locations 	= \''; ?>
<?php echo $this->_tpl_vars['VAR']['module_id']; ?>
<?php echo '\';		
		var id 			= \''; ?>
<?php echo $this->_tpl_vars['VAR']['id']; ?>
<?php echo '\';
		var ids 		= \''; ?>
<?php echo $this->_tpl_vars['VAR']['ids']; ?>
<?php echo '\';    	 
		var array_id    = id.split(",");
		var array_ids   = ids.split(",");		
		var num=0;
		if(array_id.length > 2) {				 
			document.location = \'?_page=\'+module+\':view&id=\'+array_id[0]+\'&ids=\'+id;				 		
		}else if (array_ids.length > 2) {
			document.write(view_nav_top(array_ids,id,ids));
		}
		
    	function delete_record(id,ids)
    	{				
    		temp = window.confirm("'; ?>
<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>alert_delete<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?><?php echo '");
    		if(temp == false) return;
    		
    		var replace_id = id + ",";
    		ids = ids.replace(replace_id, \'\');		
    		if(ids == \'\') {
    			var url = \'?_page=core:search&module=\' + module + \'&do[]=\' + module + \':delete&delete_id=\' + id + COOKIE_URL;
    			window.location = url;
    			return;
    		} else {
    			var page = \'view&id=\' +ids;
    		}		
    		
    		var doit = \'delete\';
    		var url = \'?_page=\'+ module +\':\'+ page +\'&do[]=\' + module + \':\' + doit + \'&delete_id=\' + id + COOKIE_URL;
    		window.location = url;	
    	}
    //  END -->
    </script>
'; ?>


<!-- Loop through each record -->
<?php if (count((array)$this->_tpl_vars['product'])):
    foreach ((array)$this->_tpl_vars['product'] as $this->_tpl_vars['product']):
?> <a name="<?php echo $this->_tpl_vars['product']['id']; ?>
"></a>

<!-- Display the field validation -->
<?php if ($this->_tpl_vars['form_validation']): ?>
   <?php echo $this->_tpl_vars['block']->display("core:alert_fields"); ?>

<?php endif; ?>

<!-- Display each record -->
<form id="product_view" name="product_view" method="post" action="" enctype="multipart/form-data">
  <table width=100% border="0" cellspacing="0" cellpadding="0" class="table_background">
  <tr>
    <td>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr valign="top"> 
            <td width="65%" class="table_heading"> 
              <center>
                <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                title_view 
                <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
              </center>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_date_orig 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_date_last 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->date($this->_tpl_vars['product']['date_orig']); ?>

                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->date($this->_tpl_vars['product']['date_last']); ?>

                    <input type="hidden" name="product_date_last" value="<?php echo time(); ?>
">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_sku 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="50%"><b>Translations</b> (for product name &amp; 
                    description)</td>
                </tr>
                <tr valign="top" class="row1"> 
                  <td width="50%"> 
                    <input type="text" name="product_sku" value="<?php echo $this->_tpl_vars['product']['sku']; ?>
" class="form_field" size="32">
                  </td>
                  <td width="50%"> 
                       <?php echo $this->_tpl_vars['list']->menu_files("",'language_id','all','language',"","_core.xml","form_menu\" onChange=\"showTranslations();"); ?>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_active 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_taxable 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('product_active',$this->_tpl_vars['product']['active'],'form_menu'); ?>

                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('product_taxable',$this->_tpl_vars['product']['taxable'],'form_menu'); ?>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_base 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_setup 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                    <input type="text" name="product_price_base" value="<?php echo $this->_tpl_vars['product']['price_base']; ?>
" class="form_field" size="5">
                    <?php echo $this->_tpl_vars['list']->currency_iso(""); ?>

                  </td>
                  <td width="50%"> 
                    <input type="text" name="product_price_setup" value="<?php echo $this->_tpl_vars['product']['price_setup']; ?>
" class="form_field" size="5">
                    <?php echo $this->_tpl_vars['list']->currency_iso(""); ?>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_type 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b><br>
                  </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_avail_category_id 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%"> 
                     <?php if ($this->_tpl_vars['product']['price_type'] == '0'): ?>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_one 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
					
					<?php endif; ?>
                    <?php if ($this->_tpl_vars['product']['price_type'] == '1'): ?>
					 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_recurr 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
					
					<?php endif; ?>
                    <?php if ($this->_tpl_vars['product']['price_type'] == '2'): ?> 
					
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    price_type_trial 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
					
					<?php endif; ?>
                  </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->menu_multi($this->_tpl_vars['product']['avail_category_id'],'product_avail_category_id','product_cat','name','5',"",'form_menu'); ?>

                  </td>
                </tr>
              </table>


			  <?php if ($this->_tpl_vars['product']['price_type'] == '1'): ?>
			  
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 			  
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"><b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_recurr_default 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="50%"> 
                    <select name="product_price_recurr_default" class="form_menu">
                      <option value="0" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '0'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_week 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="1" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '1'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_month 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="2" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '2'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_quarter 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="3" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '3'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_semianual 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="4" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '4'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_anual 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                      <option value="5" <?php if ($this->_tpl_vars['product']['price_recurr_default'] == '5'): ?> selected<?php endif; ?>> 
                      <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                      recurr_twoyear 
                      <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                      </option>
                    </select>
                  </td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="50%"><b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_recurr_type 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                  <td width="50%"><b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    user_options 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b></td>
                </tr>
                <tr valign="top" class="row2"> 
                  <td width="50%"> 
                    <input type="radio" name="product_price_recurr_type" value="0" <?php if ($this->_tpl_vars['product']['price_recurr_type'] == '0'): ?>checked<?php endif; ?>>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    recurr_type_aniv 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <br>
                    <input type="radio" name="product_price_recurr_type" value="1" <?php if ($this->_tpl_vars['product']['price_recurr_type'] == '1'): ?>checked<?php endif; ?>>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    recurr_type_fixed 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <br>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_recurr_weekday 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <input type="text" name="product_price_recurr_weekday" value="<?php echo $this->_tpl_vars['product']['price_recurr_weekday']; ?>
" class="form_field" size="2" maxlength="2">
                    (1-28) </td>
                  <td width="50%"> 
                    <?php echo $this->_tpl_vars['list']->bool('product_price_recurr_schedule',$this->_tpl_vars['product']['price_recurr_schedule'],'form_menu'); ?>

                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_recurr_schedule 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    <br>
                    <?php echo $this->_tpl_vars['list']->bool('product_price_recurr_cancel',$this->_tpl_vars['product']['price_recurr_cancel'],'form_menu'); ?>

                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_recurr_cancel 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                  </td>
                </tr>
              </table>
			  <?php endif; ?>
			  

			  <?php if ($this->_tpl_vars['product']['price_type'] == '2'): ?>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 			  
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr valign="top" class="row2"> 
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_trial_prod 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b><br>
                  </td>
                  <td width="50%"> <b> 
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_price_trial_length 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    </b> </td>
                </tr>
                <tr valign="top"> 
                  <td width="50%">
                    <?php echo $this->_tpl_vars['list']->menu("",'product_price_trial_prod','product','sku',$this->_tpl_vars['product']['price_trial_prod'],'form_menu'); ?>

                  </td>
                  <td width="50%">
                    <input type="text" name="product_price_trial_length" value="<?php echo $this->_tpl_vars['product']['price_trial_length']; ?>
" class="form_field" size="4">
                    <select name="product_price_trial_length_type" class="form_menu">
                      <option value="0" <?php if ($this->_tpl_vars['product']['price_trial_length_type'] == '0'): ?> selected<?php endif; ?>><?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>day<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></option>
                      <option value="1" <?php if ($this->_tpl_vars['product']['price_trial_length_type'] == '1'): ?> selected<?php endif; ?>><?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>week<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></option>
                      <option value="2" <?php if ($this->_tpl_vars['product']['price_trial_length_type'] == '2'): ?> selected<?php endif; ?>><?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>month<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></option>
                    </select>
                  </td>
                </tr>
              </table>
			  <?php endif; ?>
			  
			  			  
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1"> 
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row1" valign="middle" align="left"> 
                  <td width="50%" valign="top"><b> 
                    <?php if ($this->_tpl_vars['product']['thumbnail'] != ""): ?>
                    <img src="<?php echo $this->_tpl_vars['URL']; ?>
<?php echo @constant('URL_IMAGES'); ?>
<?php echo $this->_tpl_vars['product']['thumbnail']; ?>
"> 
                    &nbsp;&nbsp;&nbsp; 
                    <?php endif; ?>
                    <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>
                    field_thumbnail 
                    <?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>
                    &nbsp;&nbsp;&nbsp; </b> </td>
                  <td width="50%" align="left"><b> 
                    <input type="file" name="upload_file1" size="38" class="form_field">
                    <?php if ($this->_tpl_vars['product']['thumbnail'] != ""): ?>
                    <img title="Delete Thumbnail Image" src="themes/<?php echo $this->_tpl_vars['THEME_NAME']; ?>
/images/icons/del_16.gif" onClick="document.getElementById('delimg').value = '1'; document.getElementById('product_view').submit();">
                    <?php endif; ?>
                    </b></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top"> 
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1">
                <tr class="row2"> 
                  <td width="50%"> 
                    <input type="submit" name="Submit" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>submit<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button">                    
                  </td>
                  <td align="right" width="50%"> 
                    <input type="button" name="delete" value="<?php $this->_tag_stack[] = array('translate', array()); $this->_plugins['block']['translate'][0](array(), null, $this); ob_start(); ?>delete<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?>" class="form_button" onClick="delete_record('<?php echo $this->_tpl_vars['product']['id']; ?>
','<?php echo $this->_tpl_vars['VAR']['id']; ?>
');">
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr valign="top">
            <td width="65%" class="row1">
              <table width="100%" border="0" cellspacing="3" cellpadding="1" class="row1">
                <tr class="row1" valign="middle" align="left"> 
                  <td width="35%" align="center"> 
				  <a href="javascript:showImages();">
				  <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>manage_images<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> | 
				  
				  <a href="javascript:showAttributes();">
				  <?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>attributes<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a>
				   
                    | <a href="javascript:showBilling();">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>advanced_billing<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
					
                    | <a href="javascript:showAssociations();">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>associations<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a>
					 
                    | <a href="javascript:showDiscounts();">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>discounts<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
					
                    <?php if ($this->_tpl_vars['list']->is_installed('host_server')): ?>
                    | <a href="javascript:showHosting();">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>hosting<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
                    <?php endif; ?>
					
                    | <a href="javascript:showPlugins();">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>product_plugins<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
					
                    | <a href="javascript:showLink('<?php echo $this->_tpl_vars['product']['id']; ?>
', '<?php echo $this->_tpl_vars['product']['price_recurr_default']; ?>
')">
					<?php $this->_tag_stack[] = array('translate', array('module' => 'product')); $this->_plugins['block']['translate'][0](array('module' => 'product'), null, $this); ob_start(); ?>link<?php $this->_block_content = ob_get_contents(); ob_end_clean(); echo $this->_plugins['block']['translate'][0]($this->_tag_stack[count($this->_tag_stack)-1][1], $this->_block_content, $this); array_pop($this->_tag_stack); ?></a> 
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
    <input type="hidden" name="_page" value="product:view">
    <input type="hidden" name="product_id" value="<?php echo $this->_tpl_vars['product']['id']; ?>
">
    <input type="hidden" name="do[]" value="product:update">
    <input type="hidden" name="id" value="<?php echo $this->_tpl_vars['VAR']['id']; ?>
">
    <input type="hidden" id="delimg" name="delimg" value="0">
	<input type="hidden" name="product_assoc_req_prod" value="IGNORE-ARRAY-VALUE">
	<input type="hidden" name="product_assoc_grant_prod" value="IGNORE-ARRAY-VALUE">
	<input type="hidden" name="product_assoc_grant_group" value="IGNORE-ARRAY-VALUE">
	<input type="hidden" name="product_price_group" value="IGNORE-ARRAY-VALUE"> 
	<input type="hidden" name="product_host_discount_tld" value="IGNORE-ARRAY-VALUE">
	<input type="hidden" name="product_host_provision_plugin_data" value="IGNORE-ARRAY-VALUE">
	<input type="hidden" name="product_prod_plugin_data" value="IGNORE-ARRAY-VALUE">
</form>

<CENTER><iframe id="iframeProduct" style="border:0px; width:0px; height:0px;" scrolling="auto" ALLOWTRANSPARENCY="true" frameborder="0"></iframe> </CENTER>
<div id="code"></div>
  
<?php echo '
<SCRIPT LANGUAGE="JavaScript">
<!-- START
 
var product_id 	= '; ?>
<?php echo $this->_tpl_vars['product']['id']; ?>
<?php echo ';
  
function  showTranslations()
{ 
	document.getElementById("code").innerHTML = "";
	var language_id = document.product_view.language_id.value;
	if (language_id == "") return;  
	var url = \'?_page=core:search_iframe&module=product_translate&product_translate_language_id=\'+
			   language_id+\'&product_translate_product_id=\'+
			   product_id+\'&_escape=1&_escape_next=1&_next_page_one=view&_next_page_none=add&name_id1=product_translate_product_id&val_id1=\'
			   +product_id+\'&name_id2=product_translate_language_id&val_id2=\'+language_id;		  	 			   
	window.open(url,\'ProductLanguage\',\'scrollbars=yes,toolbar=no,status=no,width=540,height=640\'); 
} 

function  showLink(product_id, recurr_schedule)
{ 
	showIFrame(\'iframeProduct\',0,0,\'?_page=core:blank\');
	var code;
	code = "<br>Add to cart and checkout:<textarea cols=100 rows=2 class=form_field>"+
			"'; ?>
<?php echo $this->_tpl_vars['URL']; ?>
<?php echo '?_page=checkout:checkout&_next_page=checkout:checkout&do[]=cart:add&product_id="+product_id+"&recurr_schedule="+recurr_schedule+ 
			"</textarea>";
	code += "<br><br>Add to cart and view cart:<textarea cols=100 rows=2 class=form_field>"+
		 	"'; ?>
<?php echo $this->_tpl_vars['URL']; ?>
<?php echo '?_page=cart:cart&do[]=cart:add&product_id="+product_id+"&recurr_schedule="+recurr_schedule+
			"</textarea>";			
	document.getElementById("code").innerHTML = code; 
} 

function  showImages()
{ 
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=core:search_iframe&module=product_img&product_img_product_id=\'+
			   product_id+\'&_escape=1&_escape_next=1&_next_page_one=view&_next_page_none=add&name_id1=product_img_product_id&val_id1=\'
			+product_id);		 
} 

function  showAttributes()
{ 
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=core:search_iframe&module=product_attr&product_attr_product_id=\'+
			   product_id+\'&_escape=1&_escape_next=1&_next_page_one=view&_next_page_none=add&name_id1=product_attr_product_id&val_id1=\'
			+product_id+\'&name_id2=product_attribute_product_price_type&val_id2='; ?>
<?php echo $this->_tpl_vars['product']['price_type']; ?>
<?php echo '\');		 
}

function showBilling()
{
	document.getElementById("code").innerHTML = "";
	var billingtype = '; ?>
<?php echo $this->_tpl_vars['product']['price_type']; ?>
<?php echo '; 	
	if(billingtype == "0" || billingtype == "2") 
		showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_price_onetime&_escape=1&id=\'+product_id); 
	else if(billingtype == "1") 
		showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_price_recurring&_escape=1&id=\'+product_id); 
}

function showAssociations()
{
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_associations&_escape=1&id=\'+product_id);
}

function showDiscounts()
{
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_discounts&_escape=1&id=\'+product_id);
}

function showHosting()
{
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_hosting&_escape=1&id=\'+product_id);
}

function showPlugins()
{
	document.getElementById("code").innerHTML = "";
	showIFrame(\'iframeProduct\',600,300,\'?_page=product:iframe_plugins&_escape=1&id=\'+product_id);
}
//  END -->
</SCRIPT>'; ?>


  <?php endforeach; endif; ?>
<?php endif; ?>