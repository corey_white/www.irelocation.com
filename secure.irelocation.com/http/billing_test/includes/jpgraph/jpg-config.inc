<?php
DEFINE("CSIMCACHE_DIR","csimcache/"); 
DEFINE("CSIMCACHE_HTTP_DIR","csimcache/");
DEFINE('CHINESE_TTF_FONT','bkai00mp.ttf');
DEFINE("LANGUAGE_CYRILLIC",false);
DEFINE("CYRILLIC_FROM_WINDOWS",false);
DEFINE("DEFAULT_GFORMAT","auto");
DEFINE('USE_TRUECOLOR',false);
DEFINE("USE_LIBRARY_GD2", false);
DEFINE("USE_CACHE",false);
DEFINE("READ_CACHE",false);
DEFINE("USE_IMAGE_ERROR_HANDLER",true);
DEFINE("INSTALL_PHP_ERR_HANDLER",true);
DEFINE("USE_APPROX_COLORS",true);
DEFINE("ERR_DEPRECATED",true);
DEFINE("BRAND_TIMING",false);
DEFINE("BRAND_TIME_FORMAT","(%01.3fs)");
DEFINE("CACHE_FILE_GROUP","wwwadmin");
DEFINE("CACHE_FILE_MOD",0664);
DEFINE("USE_BRESENHAM",false);
DEFINE("_CSIM_SPECIALFILE","_csim_special_");
DEFINE("_CSIM_DISPLAY","_jpg_csimd");
DEFINE("_IMG_HANDLER","__handle");        
?>
