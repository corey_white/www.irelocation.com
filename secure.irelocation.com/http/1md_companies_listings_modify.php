<?
$list_id=explode(",",$list_ids);
if(count($list_id)==1)
{
  $onlyone=true;
}

#
# Get Categories
#
$rs=new mysql_recordset("select cat_id,name from directlistingscategory where parent_id!='0' and link_url='' order by name");
while($rs->fetch_array())
{
  $cat[]=$rs->myarray;
}


#
# Update listings
#
if($action2=='update')
{
  if($change_cat=='1')
  {
    $set.="cat_id='$cat_id',";
  }

  if($expiration!='')
  {
    $set.="expiration='$expiration',";
  }

  if($change_list=='1')
  {
    $set.="list_type='$list_type',";
  }

  if($area_code!='')
  {
    $set.="area_code='$area_code',";
  }

  if($name!='')
  {
    $set.="name='$name',";
  }

  if($email!='')
  {
    $set.="email='$email',";
  }

  if($state!='')
  {
    $set.="state='$state',";
  }

  if($phone_num!='')
  {
    $set.="phone_num='$phone_num',";
  }

  if($url!='')
  {
    $url=str_replace("http://","",$url);
    $set.="url='$url',";
  }

  if($description!='')
  {
    $set.="description='$description',";
  }

  $set=substr($set,0,-1);

  foreach($list_id as $mylist_id)
  {
    $where.="list_id='$mylist_id' or ";
  }
  $where=substr($where,0,-4);

  if($set!='')
  {
    $rs=new mysql_recordset("update directlistings set $set where $where");
    $msg="Listings has been updated!";
  }
}

if($onlyone==true)
{
  $rs=new mysql_recordset("select * from directlistings where list_id='$list_ids'");
  $rs->fetch_array();
  $mylisting=$rs->myarray;
}

$list_types[]="national";
$list_types[]="state";
$list_types[]="area";

?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<link rel="stylesheet" href="style.css" type="text/css">
<title>Modify Listings</title>
</head>
<body>
<?
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Modify Listing<?if($onlyone!=true){?>s<?}?></b></font></p>
<form name="emails" method="POST" action="control.php">
<input type="hidden" name="action" value="1md_companies_listings_modify">
<input type="hidden" name="action2" value="update">
<input type="hidden" name="list_ids" value="<?echo $list_ids;?>">
    <span class="notice">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="200"><span class="text_2">Category:</span></td>
          <td width="300"><input type="checkbox" name="change_cat" value="1"> <select name="cat_id" class="quote_box">
          <?
          foreach($cat as $mycat)
          {
            ?><option value="<?echo $mycat["cat_id"];?>"<?if($onlyone==true){if($mycat["cat_id"]==$mylisting["cat_id"]){echo " selected";}}?>><?echo $mycat["name"];?></option>
            <?
          }
          ?></select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Expiration:</span></td>
          <td width="300"><input type="text" name="expiration" class="quote_box" value="<?if($onlyone==true){echo $mylisting["expiration"];}?>" size="20"> <span class="text_2">YYYY-MM-DD</span></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">List Type:</span></td>
          <td width="300"><input type="checkbox" name="change_list" value="1"> <select name="cat_id" class="quote_box">
          <?
          foreach($list_types as $mylist)
          {
            ?><option value="<?echo $mylist;?>"<?if($onlyone==true){if($mylist==$mylisting["list_type"]){echo " selected";}}?>><?echo $mylist;?></option>
            <?
          }
          ?>
          </select></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Area Code:</span></td>
          <td width="300"><input type="text" name="area_code" class="quote_box" value="<?if($onlyone==true){echo $mylisting["area_code"];}?>" size="5" maxlength="3"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Name:</span></td>
          <td width="300"><input type="text" name="name" class="quote_box" value="<?if($onlyone==true){echo $mylisting["name"];}?>" size="40"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Email:</span></td>
          <td width="300"><input type="text" name="email" class="quote_box" value="<?if($onlyone==true){echo $mylisting["email"];}?>" size="40"></td>
        </tr>
        <?if($onlyone==true){?>
        <tr>
          <td width="200"><span class="text_2">State:</span></td>
          <td width="300"><input type="text" name="state" class="quote_box" value="<?if($onlyone==true){echo $mylisting["state"];}?>" size="3" maxlength="2"></td>
        </tr>
        <?}?>
        <tr>
          <td width="200"><span class="text_2">Phone Number:</span></td>
          <td width="300"><input type="text" name="phone_num" class="quote_box" value="<?if($onlyone==true){echo $mylisting["phone_num"];}?>" size="20"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Url:</span></td>
          <td width="300"><input type="text" name="url" class="quote_box" value="<?if($onlyone==true){echo $mylisting["url"];}?>" size="40"></td>
        </tr>
        <tr>
          <td width="200"><span class="text_2">Description:</span></td>
          <td width="300"><textarea name="description" class="quote_box" cols="40" rows="3"><?if($onlyone==true){echo $mylisting["description"];}?></textarea></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Update Listing<?if($onlyone!=true){?>s<?}?>!" class="quote_box"></td>
        </tr>
      </table>
    </span>
  </form>
<p></p>
<script language="javascript">
focus();
</script>
</body>
</html>