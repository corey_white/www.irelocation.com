<?
session_start();

// Setup Error messages

$error_msg = $_REQUEST["msg"];
$msgs["name"] = "Please enter your Full Name.";
$msgs["phone"] = "Please enter your Phone Number.";
$msgs["company"] = "Please enter your Company Name.";
$msgs["email"] = "Please enter your Email Address.";
$msgs["lead_type"] = "Please select your type of lead.";
$msgs["thankyou"] = "Thank you. We will contact you shortly.";
$msgs["verify"] = "Verification code is invalid.";
			
$msg = $msgs[$error_msg];

extract($_POST);


// is the form ready to check and send            
if ($send == "yes") { 
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$output = strtolower(print_r($_POST,true));
		
		if ($type == "javascript")
		{
			if (substr_count($output,"<script") > 0)
				$content = substr($output,strpos($output,"<script"));						
			else
				$content = substr($output,strpos($output,"onload="));
		}
		else if ($type == "link")
			$content = substr($output,strpos($output,"href="));	
		else if  ($type == "serverside")
		{
			if (substr_count($output,"<?") > 0)
				$content = substr($output,strpos($output,"<?"));		
			else
				$content = substr($output,strpos($output,"<%"));		
		}
		else if ($type == "dirty")
		{
			$content = "Dirty words.";
			header("Location: contact.php?msg=thankyou");
			exit;
		}	
		$content = str_replace("'","&prime;",$content);
		$content = str_replace("\"","&quot;",$content);
		
		$sql = "insert into marble.injectors (site,ip,received,content) values ".
				"('tm','$ip','".date("YmdHis")."','$content'); ";
				
		$rs = new mysql_recordset($sql);
		
		header("Location: contact.php?msg=thankyou");
		exit;
	}

	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");		
	}		
	testInjection();//if it gets past this the input 'should' be ok.
 
	// error checking
	if ($name == "") {
		header("Location: lead_form.php?msg=name&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($email == "") {
		header("Location: lead_form.php?msg=email&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($company == "") {
		header("Location: lead_form.php?msg=company&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($phone == "") {
		header("Location: lead_form.php?msg=phone&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($lead_type == "") {
		header("Location: lead_form.php?msg=lead_type&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($_SESSION['answer'] != $verify) {
        header("Location: lead_form.php?msg=verify&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
        
    } else {
		//passed error checking
 		// to email address
		$to = "katrina@irelocation.com, travis@irelocation.com, vsmith@irelocation.com, markb@irelocation.com";
		// The subject
		$subject = "iRelocation PPC Lead Form";
		// The message
		$message = "Name: ".$name."\n\n";
		$message .= "Company: ".$company."\n\n";
		$message .= "Email: ".$email."\n\n";
		$message .= "Phone: ".$phone."\n\n";
		$message .= "Lead Type: ".$lead_type."\n\n";
		$message .= "Comment: ".$comment;
		mail($to, $subject, $message, "From: $email");
		header("Location: thank_you.php");
		exit;
		
	}
	
} else {

//create numbers for human verification
$q1 = rand(0,10);    
$q2 = rand(0,10);
$_SESSION['answer'] = $q1 + $q2;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
<link href="http://www.irelocation.com/irelocation/ppc-styles.css" rel="stylesheet" type="text/css" />
<title><?php echo $pageTitle; ?></title>
<meta name="description" content="<?php echo $metaDescription; ?>" />
<meta name="keywords" content="<?php echo $metaTags; ?>" />

<!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35525013-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>



<body>
 <!--wrapper-->
 <div class="wrapper-index">
 
 <!--header_index-->
 <div id="header">
 <div id="logo">
  <a href="#"><img src="irelocation/images/logo.png" height="40px" /></a>
 </div>
<ul id="nav_index">
        <li><a title="" href="#">Leads</a></li>
        <li><a title="" href="#">Process</a></li>
        <li><a title="" href="#">About</a></li>
        <li><a title="" href="#">Contact</a></li>
        <li><a title="" href="#">Partners</a></li>
    </ul>
</div>

 <!--content_index-->
 <div id="content_index">
 
  <div id="learn_button">
 <a href="#"><img src="irelocation/images/learn-more-button.png" onmouseover="this.src='irelocation/images/learn-more-hover.png'" onmouseout="this.src='irelocation/images/learn-more-button.png'" /></a>
 </div>
 <div id="leads_button">
  <a href="#"><img src="irelocation/images/get-leads-button.png" onmouseover="this.src='irelocation/images/get-leads-hover.png'" onmouseout="this.src='irelocation/images/get-leads-button.png'" /></a>
 </div>
 
 
 <div class="icons">
<a href="#" class="icon-auto"><img src="irelocation/images/icons/car-transport-icon.png" onmouseover="this.src='irelocation/images/icons/car-transport-hover.png'" onmouseout="this.src='irelocation/images/icons/car-transport-icon.png'" /></a>
 
<a href="#" class="icon-moving"><img src="irelocation/images/icons/moving-icon.png" onmouseover="this.src='irelocation/images/icons/moving-hover.png'" onmouseout="this.src='irelocation/images/icons/moving-icon.png'" /></a>

<a href="#" class="icon-medical"><img src="irelocation/images/icons/medical-alert-icon.png" onmouseover="this.src='irelocation/images/icons/medical-alert-hover.png'" onmouseout="this.src='irelocation/images/icons/medical-alert-icon.png'" /></a>


<a href="#" class="icon-senior"><img src="irelocation/images/icons/senior-care-icon.png" onmouseover="this.src='irelocation/images/icons/senior-care-hover.png'" onmouseout="this.src='irelocation/images/icons/senior-care-icon.png'" /></a>

<a href="#" class="icon-security"><img src="irelocation/images/icons/security-icon.png" onmouseover="this.src='irelocation/images/icons/security-hover.png'" onmouseout="this.src='irelocation/images/icons/security-icon.png'" /></a>
</div>
 </div>
 <div style="clear:both;"></div>

<div id="index_footer">
<p>Copyright &copy; 2013 <a href="http://www.irelocation.com/">The iRelocation Network</a> | <a href="http://www.irelocation.com/privacy.php">Privacy Policy</a> | <a href="mailto:marketing@irelocation.com">Contact Us</a></p>
  </div> 
<div style="clear:both;"></div>
   
  </div>
</body>
</html>
  <?php }  ?>



















