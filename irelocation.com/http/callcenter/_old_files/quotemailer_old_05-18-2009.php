<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for CALLCENTER campaign
**********************************************************************
* Creation Date:  1/30/09 12:32 PM
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOTES:  This set of scripts in this folder are cloned from the moving folder and modified as needed.

*/

//include library functions
include_once "../inc_mysql.php";
include_once "../cronlib/.functions.php";
include "CompanyClasses.php";

updateCronStatus("callcenter","started");

define(LIVE,true);//signal this IS a live quotemailer.


#
# Delete Test Leads.  //-- this funciton has not been altered
#
$sql="delete from movingdirectory.leads_callcenter where ( (lower(first_name) like '%test%')  && (lower(last_name) like '%test%') ) or email = 'test@test.com' ";
$output_body .=  "Deleting Test Leads: $sql \n\n";
$rs=new mysql_recordset($sql);


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");

$sql="select * from movingdirectory.leads_callcenter where (received='' or received='0') and email not like '%tessting@gmail.com%' and ready_to_send = 1 ";
$output_body .=  "SELECT FOR CLEANING UP ENTRIES: $sql \n\n";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{

	
	$newfirstname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["first_name"])));
	$newlastname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["last_name"])));
	$newcompany=mysql_real_escape_string($rs->myarray["company"]);
	$newzip=mysql_real_escape_string($rs->myarray["zip"]);
	$newphone=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone"]);
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newcontact=mysql_real_escape_string($rs->myarray["contact"]);
	$newtimezone=mysql_real_escape_string($rs->myarray["timezone"]);

	$newtype_cust_service=mysql_real_escape_string($rs->myarray["type_cust_service"]);
	$newtype_order_process=mysql_real_escape_string($rs->myarray["type_order_process"]);
	$newtype_help_desk=mysql_real_escape_string($rs->myarray["type_help_desk"]);
	$newtype_answer_service=mysql_real_escape_string($rs->myarray["type_answer_service"]);
	$newtype_telemarketing=mysql_real_escape_string($rs->myarray["type_telemarketing"]);
	$newtype_other_services=mysql_real_escape_string($rs->myarray["type_other_services"]);

	$newcall_num=mysql_real_escape_string($rs->myarray["call_num"]);
	$newservice_hours=mysql_real_escape_string($rs->myarray["service_hours"]);
	$newused_before=mysql_real_escape_string($rs->myarray["used_before"]);
	$newtimeline=mysql_real_escape_string($rs->myarray["timeline"]);
	$newcustomer_type=mysql_real_escape_string($rs->myarray["customer_type"]);
	$newservice_type=mysql_real_escape_string($rs->myarray["service_type"]);

	$newcomment=mysql_real_escape_string($rs->myarray["comment"]);

	$newsource= mysql_real_escape_string($rs->myarray["source"]);
	
	$update_sql = "update movingdirectory.leads_callcenter set first_name='$newfirstname', last_name='$newlastname', company='$newcompany', zip='$newzip', phone='$newphone', email='$newemail', contact='$newcontact', timezone='$newtimezone', type_cust_service='$newtype_cust_service', type_order_process='$newtype_order_process', type_help_desk='$newtype_help_desk', type_answer_service='$newtype_answer_service', type_telemarketing='$newtype_telemarketing', type_other_services='$newtype_other_services', call_num='$newcall_num', service_hours='$newservice_hours', used_before='$newused_before', timeline='$newtimeline', customer_type='$newcustomer_type', service_type='$newservice_type', comment='$newcomment', received = '$mytime', source='$newsource'  	where quote_id='".$rs->myarray["quote_id"]."'";
	$output_body .=  "UPDATE SQL $update_sql\n\n";
	$rs2=new mysql_recordset($update_sql);
}	


/*  begin processing */


$mytime = substr($mytime,0,10); //YYYYMMDDHH

//select all leads that are ready to go, and moving or internationals.
$sql = "select * from movingdirectory.leads_callcenter where ready_to_send = 1 AND received like '$mytime%' ";

// TEMP SQL PULL that replaces the above for testing
#$sql = "select * from movingdirectory.leads_callcenter where quote_id = XYZZY ";

$output_body .=  "SELECTING ALL QUOTES: $sql \n";

$rs = new mysql_recordset($sql);
$count = 0;

$output_body .=  "BEGIN LOOP OF QUOTES\n";

while ($rs->fetch_array()) { //-- BEGIN LOOP OF QUOTES

	$output_body .=  "\n";
	$output_body .=  "first_name: " . $rs->myarray["first_name"] . "\n";
	$output_body .=  "last_name: " . $rs->myarray["last_name"] . "\n";
	$output_body .=  "company: " . $rs->myarray["company"] . "\n";
	$output_body .=  "zip: " . $rs->myarray["zip"] . "\n";
	$output_body .=  "phone: " . $rs->myarray["phone"] . "\n";
	$output_body .=  "email: " . $rs->myarray["email"] . "\n";

	$output_body .=  "contact: " . $rs->myarray["contact"] . "\n";
	$output_body .=  "timezone: " . $rs->myarray["timezone"] . "\n";
	$output_body .=  "type_cust_service: " . $rs->myarray["type_cust_service"] . "\n";
	$output_body .=  "type_order_process: " . $rs->myarray["type_order_process"] . "\n";
	$output_body .=  "type_help_desk: " . $rs->myarray["XXX"] . "\n";
	$output_body .=  "type_answer_service: " . $rs->myarray["type_answer_service"] . "\n";
	$output_body .=  "type_telemarketing: " . $rs->myarray["type_telemarketing"] . "\n";
	$output_body .=  "type_other_services: " . $rs->myarray["type_other_services"] . "\n";
	$output_body .=  "call_num: " . $rs->myarray["call_num"] . "\n";
	$output_body .=  "service_hours: " . $rs->myarray["service_hours"] . "\n";
	$output_body .=  "used_before: " . $rs->myarray["used_before"] . "\n";
	$output_body .=  "timeline: " . $rs->myarray["timeline"] . "\n";
	$output_body .=  "customer_type: " . $rs->myarray["customer_type"] . "\n";
	$output_body .=  "service_type: " . $rs->myarray["service_type"] . "\n";
	$output_body .=  "comment: " . $rs->myarray["comment"] . "\n";

	
	$output_body .=  "\n";
	

	$count++;
		
		$data = new CallCenterQuoteData($rs->myarray);  //-- insantiate the class
		$output_body .=  "\n\n\n\n";
		$output_body .=  "=====================================================\n";
		$output_body .=  "Processing Call Center Quote\n\n";
		$output_body .=  "=====================================================\n";
		
		/*-- process:
				THERE ARE 2 TYPES OF CLIENTS
						1. those that get all leads (PULL_ORDER 1)
						2. those that only get some leads, based on a daily quota (PULL_ORDER 2)
						
				EACH LEAD WILL GO TO 4 CLIENTS
					3 slots for those that get all leads
					The 4th slot will be one of the daily quota clients, if any are below their daily limit

				So,
				1. DETERMINE if there are any Type 2 clients that need to get leads, if not, then need to pull 4 Type 1 leads.
				2. Determine how many Type 1 clients will be getting leads.
				
	-	Pull all pull_order=2
	-	Loop thru them, checking if they have received all their leads for the day
	-	IF they still need leads
		-	then add to array
		-	update movingdirectory.leads_callcenter set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	-	IF array is empty
		-	then all_count = 6
	-	ELSE
		-	all_count = 5
		-	shuffle array
		-	send lead to first lead_id in array
		
	-	Pull either 5 or 6 leads for pull_order=1 
	-	loop thru and send them each a lead
	-	update movingdirectory.leads_callcenter set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	
	
	NEW CHANGES
	Need to make the following happen:
	
	1. When either lead_id 1672 or 1687 comes up, the other lead_id has to also be one of the lead_ids getting the lead.  IOW, 1672 & 1687 always get leads together if either of them are selected.
	
	2. Certain companies don't want certain types of leads.  Need to filter those out.
	
	

		*/
		
		//-- $output_body .=  "\nXXXXX\n";
		//-- $output_body .=  "\n";
		
		//-- define some vars based on the current quote_id
		$quote_id = $rs->myarray["quote_id"];
		$sent_to_leads = array();
		$campaign_month = date("Ym");
		$current_date = date("Ymd");
		$current_month = $campaign_month;

		$lead_count = 0;
		
		//-- output a little info
		$output_body .=  "\nquote_id = $quote_id\n";
		$output_body .=  "\ncampaign_month = $campaign_month\n";
		$output_body .=  "\nlead_count = $lead_count\n";
		$output_body .=  "\ncampaign_month = $campaign_month\n";
		$output_body .=  "\ncurrent_date = $current_date\n";
		$output_body .=  "\n";

//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS


		
		//-- process daily quota clients
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		
		$site_id = "callctr";
		$output_body .=  "\nsite_id = $site_id\n\n";
		
		//-- get the quota companies
		$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, ca.monthly_goal, 
			(select email from movingdirectory.rules 
				where lead_id = lf.lead_id) as 'email',
			lf.lead_id,
			format_type,
			lf.cat_id, 
			if(lead_body < 0,
				(select lead_body from movingdirectory.lead_format 
					where lead_id = lf.lead_body),
				lf.lead_body) as 'lead_body' 
		from
			movingdirectory.lead_format as lf 
			join 
			movingdirectory.directleads as d
			on d.lead_id = lf.lead_id 
			join 
			movingdirectory.campaign as ca
			on d.lead_id = ca.lead_id	

		where	
			ca.active
			and lf.cat_id = 8 
			and ca.month = '$campaign_month'
			and ca.site_id = 'callctr'
			and ca.pull_order = 2
		group by	
			ca.lead_id
		order by rand()";
		
		$output_body .=  "SQL FOR TYPE 2 (QUOTA) CLIENTS:\n$comp_sql\n\n";
		
		$comp_rs = new mysql_recordset($comp_sql);
		
		$quota_arr = array();
		
		//-- Loop through those that have been pulled and if any still need leads for today, stick them in an array.
		$output_body .=  "LOOPING THRU QUOTA CLIENTS\n";
		while ($comp_rs->fetch_array()) {
			# $XXXX = $comp_rs->myarray["XXXX"];
			$lead_id = $comp_rs->myarray["lead_id"];
			$leads_per_day = $comp_rs->myarray["leads_per_day"];
			$leads_per_month = $comp_rs->myarray["monthly_goal"];
			$output_body .=  "$lead_id can get $leads_per_day per day\n";
			$output_body .=  "$lead_id can get $leads_per_month per month\n";
			
			//-- get daily count
			$w_sql = "select * from movingdirectory.leads_callcenter where lead_ids like '%$lead_id%' and left(received,8) = '$current_date' ";
			$output_body .= "$w_sql\n";
			$rs_w = new mysql_recordset($w_sql);
			$w_count = $rs_w->rowcount();
			
			$output_body .=  "Daily lead count for $lead_id = $w_count\n";
			
			//- get monthly count
			$m_sql = "select * from movingdirectory.leads_callcenter where lead_ids like '%$lead_id%' and left(received,6) = '$current_month' ";
			$output_body .= "$m_sql\n";
			$rs_m = new mysql_recordset($m_sql);
			$m_count = $rs_m->rowcount();
			
			$output_body .=  "monthly lead count for $lead_id = $m_count\n";
			
			//-- if still need leads, add them to array
			if ( $w_count < $leads_per_day && $m_count < $leads_per_month ) {
				$output_body .=  "------- Added $lead_id to array\n";
				$quota_arr[] = $lead_id;
			} 


		}
		
			$output_body .=  "QUOTA ARRAY AFTER CHECKING DAILY LEADS TOTALS:\n<pre>";
			$output_body .= print_r($quota_arr, true);
			$output_body .=  "</pre>\n";



			//-- HERE IS WHERE WE CAN REMOVE LEAD_IDS FOR THOSE THAT ONLY WANT CERTAIN CRITERIA FOR LEADS
			
			//-- OnBrand24 (1670) does not receive Answering Service leads
			$output_body .=  "rs->myarray[type_answer_service] = " . $rs->myarray["type_answer_service"] . "\n";
			
			if ( $rs->myarray["type_answer_service"] == "Yes" ) {
				$output_body .=  "OnBrand24 (1670) does not want Answering Service leads, checking for 1670 in the Quota Array\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1670" ) {
						unset($quota_arr[$key]);
						$output_body .=  "REMOVING 1670 FROM THE ARRAY\n";
					}
				}
			} 



			$output_body .=  "QUOTA ARRAY AFTER CHECKING FOR \"INBOUND\" LEADS:\n<pre>";
			$output_body .= print_r($quota_arr, true);
			$output_body .=  "</pre>\n";

		
			//-- if there are no lead_ids in the array, set all_count 
			if ( sizeof($quota_arr) == 0 ) {
				$all_count = 6;
				
			} else { //-- else set all_count = 5, shuffle array, send lead to first lead_id
				$all_count = 5;
				shuffle($quota_arr);
				$output_body .=  "QUOTA ARRAY AFTER SHUFFLING:\n<pre>";
				$output_body .= print_r($quota_arr,true);
				$output_body .=  "</pre>\n";

				$Qlead_id = $quota_arr[0];
				
				$output_body .=  "SELECTING $Qlead_id for the quota lead\n\n";
				
				//-- get lead data for just the one lead_id
				$lead_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
					(select email from movingdirectory.rules 
						where lead_id = lf.lead_id) as 'email',
					lf.lead_id,
					format_type,
					lf.cat_id, 
					if(lead_body < 0,
						(select lead_body from movingdirectory.lead_format 
							where lead_id = lf.lead_body),
						lf.lead_body) as 'lead_body' 
				from
					movingdirectory.lead_format as lf 
					join 
					movingdirectory.directleads as d
					on d.lead_id = lf.lead_id 
					join 
					movingdirectory.campaign as ca
					on d.lead_id = ca.lead_id	
		
				where	
					ca.active
					and lf.cat_id = 8
					and ca.month = '$campaign_month'
					and ca.site_id = 'callctr'
					and ca.pull_order = 2
					and ca.lead_id = $Qlead_id
				group by	
					ca.lead_id";
					
				$output_body .=  "GETTING SINGLE LEAD_ID FOR QUOTA SLOT: $lead_sql \n\n";
				
				$leads_rs = new mysql_recordset($lead_sql);
				$leads_rs->fetch_array();
		
$output_body .=  "LEADS ARRAY<pre>";
$output_body .= print_r($leads_rs->myarray,true);
$output_body .=  "</pre>\n";

				$lead_count++;
				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$output_body .=  "\nINTERNAL TEST - NOT BEING SENT\n";
				} else {
					$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
					$output_body .=  "\nSent to Processing (\$data->process)\n";
				}
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				$output_body .=  "\nSent to Finishing (\$data->finishLead)\n";
*/
				
				$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
				$output_body .=  "sent to " . $leads_rs->myarray['lead_id'] . "\n";
				$output_body .=  "\n\n";

			}
			
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS

//-- ============== PROCESS ANSWER CENTER AMERICA (1672 & 1687)
//-- ============== PROCESS ANSWER CENTER AMERICA (1672 & 1687)
//-- ============== PROCESS ANSWER CENTER AMERICA (1672 & 1687)

			//-- Now, send the lead to the Type 1 pull order leads
			$output_body .=  "\nPROCESS ANSWER CENTER AMERICA (1672 & 1687)\n";
			$output_body .=  "PROCESS ANSWER CENTER AMERICA (1672 & 1687)\n";
			$output_body .=  "PROCESS ANSWER CENTER AMERICA (1672 & 1687)\n";
			
			$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
				(select email from movingdirectory.rules 
					where lead_id = lf.lead_id) as 'email',
				lf.lead_id,
				format_type,
				lf.cat_id, 
				if(lead_body < 0,
					(select lead_body from movingdirectory.lead_format 
						where lead_id = lf.lead_body),
					lf.lead_body) as 'lead_body' 
			from
				movingdirectory.lead_format as lf 
				join 
				movingdirectory.directleads as d
				on d.lead_id = lf.lead_id 
				join 
				movingdirectory.campaign as ca
				on d.lead_id = ca.lead_id	
	
			where	
				ca.active
				and lf.cat_id = 8 
				and ca.month = '$campaign_month'
				and ca.site_id = 'callctr'
				and ca.pull_order = 1
				and ( ca.lead_id = 1672 || ca.lead_id = 1687 )
			group by	
				ca.lead_id  ";
			
			$output_body .=  "ANSWER CENTER AMERICA SQL:\n$comp_sql\n";
			
			$lead_array = new mysql_recordset($comp_sql);
			
			//-- loop through and send out lead
			while ($lead_array->fetch_array()) {

				$lead_id = $lead_array->myarray["lead_id"];
				
				$output_body .=  "<b>PROCESSING COMPANY $lead_count: $lead_id</b>\n";

				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$output_body .=  "INTERNAL TEST - NOT BEING SENT\n\n";
				
				} else {
					$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
					$output_body .=  "\nSent to Processing (\$data->process)\n";

					$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
					$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
					$output_body .=  "\n\n";

					$lead_count++;
					
					$output_body .= "\n";
				}
				
				
				
				
			}
			
			$output_body .=  "So Far, sent to $lead_count leads\n";
	


//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS
//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS
//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS

//-- NOTE, we are now randomly pulling ALL companies that are remining and counting up to 6 

			//-- Now, send the lead to the Type 1 pull order leads
			$output_body .=  "\nPROCESSING CLIENTS THAT GET ALL LEADS\n";
			$output_body .=  "PROCESSING CLIENTS THAT GET ALL LEADS\n";
			$output_body .=  "PROCESSING CLIENTS THAT GET ALL LEADS\n";
			
			$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
				(select email from movingdirectory.rules 
					where lead_id = lf.lead_id) as 'email',
				lf.lead_id,
				format_type,
				lf.cat_id, 
				if(lead_body < 0,
					(select lead_body from movingdirectory.lead_format 
						where lead_id = lf.lead_body),
					lf.lead_body) as 'lead_body' 
			from
				movingdirectory.lead_format as lf 
				join 
				movingdirectory.directleads as d
				on d.lead_id = lf.lead_id 
				join 
				movingdirectory.campaign as ca
				on d.lead_id = ca.lead_id	
	
			where	
				ca.active
				and lf.cat_id = 8 
				and ca.month = '$campaign_month'
				and ca.site_id = 'callctr'
				and ca.pull_order = 1
				and ca.lead_id != 1672 
				and ca.lead_id != 1687 
			group by	
				ca.lead_id
			order by rand() ";
			
			$output_body .=  "ALL LEADS COMPANIES SQL:\n$comp_sql\n";
			
			$lead_array = new mysql_recordset($comp_sql);
			
			//-- test for FL zip
			$fl_sql = "select * from movingdirectory.zip_codes where state = 'fl' and zip = " . $rs->myarray["zip"] ;
			$output_body .= "<br />Checking if zip is in Florida with: $fl_sql <br />";
			$rs_fl = new mysql_recordset($fl_sql);
			if ( $rs_fl->rowcount() > 0 ) {
				$fl_zip = "yes";
			} else {
				$fl_zip = "no";
			}
			$output_body .= "FLORIDA ZIP? :: $fl_zip <br /><br />";
			
			//-- loop through and send out lead
			while ($lead_array->fetch_array()) {

				$lead_id = $lead_array->myarray["lead_id"];
				
				$output_body .=  "<b>PROCESSING COMPANY $lead_count: $lead_id</b>\n";
				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$output_body .=  "INTERNAL TEST - NOT BEING SENT\n\n";
				
				} elseif ( $lead_array->myarray['lead_id'] == 1684 ) { //-- Support Seven (1684) only wants Inbound CS leads 
					if ( strtolower($rs->myarray["type_cust_service"]) == "yes" && strtolower($rs->myarray["service_type"]) == "inbound calls"  ) {
						$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
						$output_body .=  "\nSent to Processing (\$data->process)\n";
	
						$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
						$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
						$output_body .=  "\n\n";
	
						$lead_count++;
						
						$output_body .= "\n";
					} else {
						$output_body .=  "Support Seven (1684) only wants Inbound CS leads - NOT BEING SENT\n\n";
					}

				} elseif ( $rs->myarray["type_telemarketing"] == "Yes" && $lead_array->myarray['lead_id'] == 1676 ) { //-- 24x7 (1676) does not receive Tele Marketing leads 
					$output_body .=  "24x7 (1676) does not want Tele Marketing leads - NOT BEING SENT\n\n";
				
				} elseif ( ($rs->myarray["service_type"] == "Inbound Calls" || $rs->myarray["service_type"] == "Inbound & Outbound Calls") && ($rs->myarray["type_cust_service"] != "Yes" || $rs->myarray["type_order_process"] != "Yes" || $rs->myarray["type_answer_service"] != "Yes" || $rs->myarray["type_telemarketing"] != "Yes" ) && $lead_array->myarray['lead_id'] == 1669 ) { //-- Ansafone Communications (1669) Inbound leads: Customer Service, Answering Service, Order Processing and Telemarketing
					$output_body .=  "Ansafone Communications (1669) Inbound leads: Customer Service, Answering Service, Order Processing and Telemarketing only - NOT BEING SENT\n\n";
				
				} elseif ( ($rs->myarray["service_type"] == "Outbound Calls" || $rs->myarray["service_type"] == "Inbound & Outbound Calls") && $rs->myarray["type_cust_service"] != "Yes" && $lead_array->myarray['lead_id'] == 1669 ) { //-- Ansafone Communications (1669) Outbound Leads: Customer Service only
					$output_body .=  "Ansafone Communications (1669) Outbound Leads: Customer Service only - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["service_type"] != "Inbound Calls" && $lead_array->myarray['lead_id'] == 1671 ) { //-- 24-7 Intouch (1671) wants inbound leads only 
					$output_body .=  "24-7 Intouch (1671) wants inbound leads only - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["call_num"] == "0-500" && $lead_array->myarray['lead_id'] == 1703 ) { //-- New Image Techology (1703) only wants call volume of >500 
					$output_body .=  "New Image Techology (1703) only wants call volume of >500 - NOT BEING SENT\n\n";
				
				} elseif (  $lead_array->myarray['lead_id'] == 1682 ) { //-- Sunshine Communications (1682) wants Inbound leads from Florida only 
					if ( $fl_zip == "yes" && $rs->myarray["service_type"] == "Inbound Calls" ) {
						$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
						$output_body .=  "\nSent to Processing (\$data->process)\n";
	
						$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
						$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
						$output_body .=  "\n\n";
	
						$lead_count++;
						
						$output_body .= "\n";
					} else {
						$output_body .=  "Sunshine Communications (1682) wants Inbound leads from Florida only - NOT BEING SENT\n\n";
					}

				} elseif (  $lead_array->myarray['lead_id'] == 1701 ) { //-- Qualified Marketing (1701), only wants Service Type: Outbound,  Telemarketing:Yes
#					if ( $rs->myarray["customer_type"] == "Business" && $rs->myarray["service_type"] == "Outbound Calls" && $rs->myarray["type_telemarketing"] == "Yes" ) { //-- with Customer Type: Business only
					if ( $rs->myarray["service_type"] == "Outbound Calls" && $rs->myarray["type_telemarketing"] == "Yes" ) {
						$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
						$output_body .=  "\nSent to Processing (\$data->process)\n";
	
						$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
						$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
						$output_body .=  "\n\n";
	
						$lead_count++;
						
						$output_body .= "\n";
					} else {
						$output_body .=  "Qualified Marketing (1701), only wants Customer Type: Business,  Service Type: Outbound,  Telemarketing:Yes - NOT BEING SENT\n\n";
					}

				} elseif ( $rs->myarray["service_type"] != "Outbound Calls" && $lead_array->myarray['lead_id'] == 1678 ) { //-- Focus One Sales (1678) wants Outbound leads only 
					$output_body .=  "Focus One Sales (1678) wants Outbound leads only - NOT BEING SENT\n\n";
				
				} else {
					$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
					$output_body .=  "\nSent to Processing (\$data->process)\n";

					$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
					$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
					$output_body .=  "\n\n";

					$lead_count++;
					
					$output_body .= "\n";
					
					
				}
				
				if ( $lead_count == 6 ) { //-- If we have sent to 6 leads, break the While Loop
					break;
				} 

			}
	

		
		
		$output_body .=  "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.\n";
		foreach($sent_to_leads as $key => $value){
			$output_body .=  "Lead: $key sent to: $value\n";
		}

		//-- Hopefully this is where it needs to be
		$data->finishLead(); //-- update ready_to_send = 2
		$output_body .=  "\nSent to Finishing (\$data->finishLead)\n";
	
	
} //-- end the while loop (needs to stay)


/* ===== we can leave this ===== */
if ($count > 0) {
	$output_body .=  $count." leads processed.";
} else {
	$output_body .=  "no leads processed.";
}
	
	
#mail("rob@irelocation.com","Call Center Quotemailer Output","$output_body");
$output_body = nl2br($output_body);
echo $output_body;

#mail("rob@irelocation.com","Call Center Quotemailer Ran","Call Center Quotemailer Ran");

/* turn on for trouble shooting
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r";
$mail_body = ereg_replace("\n","",$body);
mail("rob@irelocation.com","**NEW** QUOTEMAILER OUTPUT","$mail_body","$headers");
*/

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>