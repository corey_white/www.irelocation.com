<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for CALLCENTER campaign
**********************************************************************
* Creation Date:  1/30/09 12:32 PM
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOTES:  This set of scripts in this folder are cloned from the moving folder and modified as needed.

*/

//include library functions
include_once "../inc_mysql.php";
include_once "../cronlib/.functions.php";
include "CompanyClasses.php";

updateCronStatus("callcenter","started");

define(LIVE,true);//signal this IS a live quotemailer.


#
# Delete Test Leads.  //-- this funciton has not been altered
#
$sql="delete from movingdirectory.leads_callcenter where ( (lower(first_name) like '%test%')  && (lower(last_name) like '%test%') ) or email = 'test@test.com' ";
echo  "Deleting Test Leads: $sql <br />\n<br />\n";
$rs=new mysql_recordset($sql);


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");

$sql="select * from movingdirectory.leads_callcenter where (received='' or received='0') and email not like '%tessting@gmail.com%'";
echo  "SELECT FOR CLEANING UP ENTRIES: $sql <br />\n<br />\n";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{

	
	$newfirstname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["first_name"])));
	$newlastname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["last_name"])));
	$newcompany=mysql_real_escape_string($rs->myarray["company"]);
	$newzip=mysql_real_escape_string($rs->myarray["zip"]);
	$newphone=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone"]);
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newcontact=mysql_real_escape_string($rs->myarray["contact"]);
	$newtimezone=mysql_real_escape_string($rs->myarray["timezone"]);

	$newtype_cust_service=mysql_real_escape_string($rs->myarray["type_cust_service"]);
	$newtype_order_process=mysql_real_escape_string($rs->myarray["type_order_process"]);
	$newtype_help_desk=mysql_real_escape_string($rs->myarray["type_help_desk"]);
	$newtype_answer_service=mysql_real_escape_string($rs->myarray["type_answer_service"]);
	$newtype_telemarketing=mysql_real_escape_string($rs->myarray["type_telemarketing"]);
	$newtype_other_services=mysql_real_escape_string($rs->myarray["type_other_services"]);

	$newcall_num=mysql_real_escape_string($rs->myarray["call_num"]);
	$newservice_hours=mysql_real_escape_string($rs->myarray["service_hours"]);
	$newused_before=mysql_real_escape_string($rs->myarray["used_before"]);
	$newtimeline=mysql_real_escape_string($rs->myarray["timeline"]);
	$newcustomer_type=mysql_real_escape_string($rs->myarray["customer_type"]);
	$newservice_type=mysql_real_escape_string($rs->myarray["service_type"]);

	$newcomment=mysql_real_escape_string($rs->myarray["comment"]);

	$newsource= mysql_real_escape_string($rs->myarray["source"]);
	
	$update_sql = "update movingdirectory.leads_callcenter set first_name='$newfirstname', last_name='$newlastname', company='$newcompany', zip='$newzip', phone='$newphone', email='$newemail', contact='$newcontact', timezone='$newtimezone', type_cust_service='$newtype_cust_service', type_order_process='$newtype_order_process', type_help_desk='$newtype_help_desk', type_answer_service='$newtype_answer_service', type_telemarketing='$newtype_telemarketing', type_other_services='$newtype_other_services', call_num='$newcall_num', service_hours='$newservice_hours', used_before='$newused_before', timeline='$newtimeline', customer_type='$newcustomer_type', service_type='$newservice_type', comment='$newcomment', received = '$mytime', source='$newsource'  	where quote_id='".$rs->myarray["quote_id"]."'";
	echo  "UPDATE SQL $update_sql<br />\n<br />\n";
	$rs2=new mysql_recordset($update_sql);
}	


/*  begin processing */


$mytime = substr($mytime,0,10); //YYYYMMDDHH

//select all leads that are ready to go, and moving or internationals.
$sql = "select * from movingdirectory.leads_callcenter where ready_to_send = 1 AND received like '$mytime%' ";

// TEMP SQL PULL that replaces the above for testing
#$sql = "select * from movingdirectory.leads_callcenter where quote_id = XYZZY ";

echo  "SELECTING ALL QUOTES: $sql <br />\n";

$rs = new mysql_recordset($sql);
$count = 0;

echo  "BEGIN LOOP OF QUOTES<br />\n";

while ($rs->fetch_array()) { //-- BEGIN LOOP OF QUOTES

	echo  "<br />\n";
	echo  "first_name: " . $rs->myarray["first_name"] . "<br />\n";
	echo  "last_name: " . $rs->myarray["last_name"] . "<br />\n";
	echo  "company: " . $rs->myarray["company"] . "<br />\n";
	echo  "zip: " . $rs->myarray["zip"] . "<br />\n";
	echo  "phone: " . $rs->myarray["phone"] . "<br />\n";
	echo  "email: " . $rs->myarray["email"] . "<br />\n";

	echo  "contact: " . $rs->myarray["contact"] . "<br />\n";
	echo  "timezone: " . $rs->myarray["timezone"] . "<br />\n";
	echo  "type_cust_service: " . $rs->myarray["type_cust_service"] . "<br />\n";
	echo  "type_order_process: " . $rs->myarray["type_order_process"] . "<br />\n";
	echo  "type_help_desk: " . $rs->myarray["XXX"] . "<br />\n";
	echo  "type_answer_service: " . $rs->myarray["type_answer_service"] . "<br />\n";
	echo  "type_telemarketing: " . $rs->myarray["type_telemarketing"] . "<br />\n";
	echo  "type_other_services: " . $rs->myarray["type_other_services"] . "<br />\n";
	echo  "call_num: " . $rs->myarray["call_num"] . "<br />\n";
	echo  "service_hours: " . $rs->myarray["service_hours"] . "<br />\n";
	echo  "used_before: " . $rs->myarray["used_before"] . "<br />\n";
	echo  "timeline: " . $rs->myarray["timeline"] . "<br />\n";
	echo  "customer_type: " . $rs->myarray["customer_type"] . "<br />\n";
	echo  "service_type: " . $rs->myarray["service_type"] . "<br />\n";
	echo  "comment: " . $rs->myarray["comment"] . "<br />\n";

	
	echo  "<br />\n";
	

	$count++;
		
		$data = new CallCenterQuoteData($rs->myarray);  //-- insantiate the class
		echo  "<br />\n<br />\n<br />\n<br />\n";
		echo  "=====================================================<br />\n";
		echo  "Processing Call Center Quote<br />\n<br />\n";
		echo  "=====================================================<br />\n";
		
		/*-- process:
				THERE ARE 2 TYPES OF CLIENTS
						1. those that get all leads (PULL_ORDER 1)
						2. those that only get some leads, based on a daily quota (PULL_ORDER 2)
						
				EACH LEAD WILL GO TO 4 CLIENTS
					3 slots for those that get all leads
					The 4th slot will be one of the daily quota clients, if any are below their daily limit

				So,
				1. DETERMINE if there are any Type 2 clients that need to get leads, if not, then need to pull 4 Type 1 leads.
				2. Determine how many Type 1 clients will be getting leads.
				
	-	Pull all pull_order=2
	-	Loop thru them, checking if they have received all their leads for the day
	-	IF they still need leads
		-	then add to array
		-	update movingdirectory.leads_callcenter set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	-	IF array is empty
		-	then all_count = 6
	-	ELSE
		-	all_count = 5
		-	shuffle array
		-	send lead to first lead_id in array
		
	-	Pull either 5 or 6 leads for pull_order=1 
	-	loop thru and send them each a lead
	-	update movingdirectory.leads_callcenter set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??

		*/
		
		//-- echo  "<br />\nXXXXX<br />\n";
		//-- echo  "<br />\n";
		
		//-- define some vars based on the current quote_id
		$quote_id = $rs->myarray["quote_id"];
		$sent_to_leads = array();
		$campaign_month = date("Ym");
		$current_date = date("Ymd");

		$lead_count = 0;
		
		//-- output a little info
		echo  "<br />\nquote_id = $quote_id<br />\n";
		echo  "<br />\ncampaign_month = $campaign_month<br />\n";
		echo  "<br />\nlead_count = $lead_count<br />\n";
		echo  "<br />\ncampaign_month = $campaign_month<br />\n";
		echo  "<br />\ncurrent_date = $current_date<br />\n";
		echo  "<br />\n";

//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS


		
		//-- process daily quota clients
		echo  "<br />\nPROCESSING DAILY QUOTA CLIENTS<br />\n";
		echo  "<br />\nPROCESSING DAILY QUOTA CLIENTS<br />\n";
		echo  "<br />\nPROCESSING DAILY QUOTA CLIENTS<br />\n";
		
		$site_id = "callctr";
		echo  "<br />\nsite_id = $site_id<br />\n<br />\n";
		
		//-- get the quota companies
		$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
			(select email from movingdirectory.rules 
				where lead_id = lf.lead_id) as 'email',
			lf.lead_id,
			format_type,
			lf.cat_id, 
			if(lead_body < 0,
				(select lead_body from movingdirectory.lead_format 
					where lead_id = lf.lead_body),
				lf.lead_body) as 'lead_body' 
		from
			movingdirectory.lead_format as lf 
			join 
			movingdirectory.directleads as d
			on d.lead_id = lf.lead_id 
			join 
			movingdirectory.campaign as ca
			on d.lead_id = ca.lead_id	

		where	
			ca.active
			and lf.cat_id = 8 
			and ca.month = '$campaign_month'
			and ca.site_id = 'callctr'
			and ca.pull_order = 2
		group by	
			ca.lead_id
		order by rand()";
		
		echo  "SQL FOR TYPE 2 (QUOTA) CLIENTS:<br />\n$comp_sql<br />\n<br />\n";
		
		$comp_rs = new mysql_recordset($comp_sql);
		
		$quota_arr = array();
		
		//-- Loop through those that have been pulled and if any still need leads for today, stick them in an array.
		echo  "LOOPING THRU QUOTA CLIENTS<br />\n";
		while ($comp_rs->fetch_array()) {
			# $XXXX = $comp_rs->myarray["XXXX"];
			$lead_id = $comp_rs->myarray["lead_id"];
			$leads_per_day = $comp_rs->myarray["leads_per_day"];
			echo  "$lead_id can get $leads_per_day per day<br />\n";
			
			$w_sql = "select * from movingdirectory.leads_callcenter where lead_ids like '%$lead_id%' and left(received,8) = '$current_date' ";
			$rs_w = new mysql_recordset($w_sql);
			$w_count = $rs_w->rowcount();
			
			echo  "Daily lead count for $lead_id = $w_count<br />\n";
			
			//-- if still need leads, add them to array
			if ( $w_count < $leads_per_day ) {
				echo  "------- Added $lead_id to array<br />\n";
				$quota_arr[] = $lead_id;
			} 


		}
		
			echo  "QUOTA ARRAY AFTER CHECKING DAILY LEADS TOTALS:<br />\n<pre>";
			echo print_r($quota_arr, true);
			echo  "</pre><br />\n";



/*  THIS IS WHERE WE NEED TO FILTER OUT THOSE THAT ONLY WANT INBOUND LEADS, WHEN WE GET THAT SET UP
			//-- HERE IS WHERE WE CAN REMOVE LEAD_IDS FOR THOSE THAT ONLY WANT CERTAIN CRITERIA FOR LEADS
			echo  "rs->myarray[receive_cc_orders] = " . $rs->myarray["receive_cc_orders"] . "<br />\n";
			
			if ( $rs->myarray["receive_cc_orders"] != "In Person" ) {
				echo  "NOT an In Person lead, checking for 1664 in the Quota Array<br />\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1664" ) {
						unset($quota_arr[$key]);
						echo  "REMOVING 1664 FROM THE ARRAY<br />\n";
					}
				}
			} 


*/

			echo  "QUOTA ARRAY AFTER CHECKING FOR \"INBOUND\" LEADS:<br />\n<pre>";
			echo print_r($quota_arr, true);
			echo  "</pre><br />\n";

		
			//-- if there are no lead_ids in the array, set all_count 
			if ( sizeof($quota_arr) == 0 ) {
				$all_count = 6;
				
			} else { //-- else set all_count = 5, shuffle array, send lead to first lead_id
				$all_count = 5;
				shuffle($quota_arr);
				echo  "QUOTA ARRAY AFTER SHUFFLING:<br />\n<pre>";
				echo print_r($quota_arr,true);
				echo  "</pre><br />\n";

				$Qlead_id = $quota_arr[0];
				
				echo  "SELECTING $Qlead_id for the quota lead<br />\n<br />\n";
				
				//-- get lead data for just the one lead_id
				$lead_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
					(select email from movingdirectory.rules 
						where lead_id = lf.lead_id) as 'email',
					lf.lead_id,
					format_type,
					lf.cat_id, 
					if(lead_body < 0,
						(select lead_body from movingdirectory.lead_format 
							where lead_id = lf.lead_body),
						lf.lead_body) as 'lead_body' 
				from
					movingdirectory.lead_format as lf 
					join 
					movingdirectory.directleads as d
					on d.lead_id = lf.lead_id 
					join 
					movingdirectory.campaign as ca
					on d.lead_id = ca.lead_id	
		
				where	
					ca.active
					and lf.cat_id = 8
					and ca.month = '$campaign_month'
					and ca.site_id = 'callctr'
					and ca.pull_order = 2
					and ca.lead_id = $Qlead_id
				group by	
					ca.lead_id";
					
				echo  "GETTING SINGLE LEAD_ID FOR QUOTA SLOT: $lead_sql <br />\n<br />\n";
				
				$leads_rs = new mysql_recordset($lead_sql);
				$leads_rs->fetch_array();
		
echo  "LEADS ARRAY<pre>";
echo print_r($leads_rs->myarray,true);
echo  "</pre><br />\n";

				$lead_count++;
				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					echo  "<br />\nINTERNAL TEST - NOT BEING SENT<br />\n";
				} else {
					$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
					echo  "<br />\nSent to Processing (\$data->process)<br />\n";
				}
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				echo  "<br />\nSent to Finishing (\$data->finishLead)<br />\n";
*/
				
				$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
				echo  "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
				echo  "<br />\n<br />\n";

			}
			
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS

//-- ============== BEGIN CLEINTS THAT GET ALL LEADS
//-- ============== BEGIN CLEINTS THAT GET ALL LEADS
//-- ============== BEGIN CLEINTS THAT GET ALL LEADS

			//-- Now, send the lead to the Type 1 pull order leads
			echo  "<br />\nPROCESSING CLIENTS THAT GET ALL LEADS<br />\n";
			echo  "<br />\nPROCESSING CLIENTS THAT GET ALL LEADS<br />\n";
			echo  "<br />\nPROCESSING CLIENTS THAT GET ALL LEADS<br />\n";
			
			$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
				(select email from movingdirectory.rules 
					where lead_id = lf.lead_id) as 'email',
				lf.lead_id,
				format_type,
				lf.cat_id, 
				if(lead_body < 0,
					(select lead_body from movingdirectory.lead_format 
						where lead_id = lf.lead_body),
					lf.lead_body) as 'lead_body' 
			from
				movingdirectory.lead_format as lf 
				join 
				movingdirectory.directleads as d
				on d.lead_id = lf.lead_id 
				join 
				movingdirectory.campaign as ca
				on d.lead_id = ca.lead_id	
	
			where	
				ca.active
				and lf.cat_id = 8 
				and ca.month = '$campaign_month'
				and ca.site_id = 'callctr'
				and ca.pull_order = 1
			group by	
				ca.lead_id
			order by rand()
			limit  $all_count ";
			
			echo  "ALL LEADS COMPANIES SQL:<br />\n$comp_sql<br />\n";
			
			$lead_array = new mysql_recordset($comp_sql);
			
			//-- loop through and send out lead
			while ($lead_array->fetch_array()) {

				$lead_id = $lead_array->myarray["lead_id"];
				
				$lead_count++;

				echo  "<b>PROCESSING COMPANY $lead_count: $lead_id</b><br />\n";

				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					echo  "<br />\nINTERNAL TEST - NOT BEING SENT<br />\n";
				} else {
					$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
					echo  "<br />\nSent to Processing (\$data->process)<br />\n";
				}
				
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				echo  "<br />\nSent to Finishing (\$data->finishLead)<br />\n";
*/
				
				$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
				echo  "sent to " . $lead_array->myarray['lead_id'] . "<br />\n";
				echo  "<br />\n<br />\n";
				
			}
	

		
		
		echo  "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br />\n";
		foreach($sent_to_leads as $key => $value){
			echo  "Lead: $key sent to: $value<br />\n";
		}

		//-- Hopefully this is where it needs to be
		$data->finishLead(); //-- update ready_to_send = 2
		echo  "<br />\nSent to Finishing (\$data->finishLead)<br />\n";
	
	
} //-- end the while loop (needs to stay)


/* ===== we can leave this ===== */
if ($count > 0) {
	echo  $count." leads processed.";
} else {
	echo  "no leads processed.";
}
	
	

echo  $body;
#mail("rob@irelocation.com","Call Center Quotemailer Output","$body");

#mail("rob@irelocation.com","Call Center Quotemailer Ran","Call Center Quotemailer Ran");

/* turn on for trouble shooting
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r";
$mail_body = ereg_replace("<br />\n","",$body);
mail("rob@irelocation.com","**NEW** QUOTEMAILER OUTPUT","$mail_body","$headers");
*/

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>