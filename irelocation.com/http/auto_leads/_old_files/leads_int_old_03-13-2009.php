 

<?
/* 
************************FILE INFORMATION**********************
* File Name:  leads.php
**********************************************************************
* Description:  This file is for inputting leads into our system - for those that don't use SOAP.  A1AUTO (a1autotransport.com) uses this
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	03/10/2009 - Markus - Made Destination Country additions (Any mods are under 'Markus')
	09/10/2008 - Rob - undid the exclusivity code
	06/02/2008 - Rob - added a better email check
	08/05/2008 - Rob - Made it so that 1000 A1Auto leads go exclusively to CarMoves
	
**********************************************************************
*/

	//accept an auto lead from affiliates.

	function checkEmailValid($email) { 
		//-- Checks to see if there is an MX record for the domain and if we can make a socket connection to to SMTP port
	
		list($userName, $mailDomain) = split("@", $email);  //-- split email address into the username and domain. 
		
		if (!checkdnsrr($mailDomain, "MX")) { //-- this checks to see if there is an MX DNS record for the domain
			return false;
		}

		return true;
	}


	include "../inc_mysql.php";
		
	if ($_REQUEST['method'] == 'post')
	{
		extract($_POST);
		if ($rejectpage == "")
		{
			echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
			echo "<a href='leads.php?method=request'>use REQUEST/GET variables</a><Br/>\n";
		}			
		//--mail("rob@irelocation.com","Posted Auto Lead",print_r($_POST,true));
	}
	else
	{
		extract($_REQUEST);
		if ($rejectpage == "")
		{
			echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
			echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
		}
		//--mail("rob@irelocation.com","Requested Auto Lead",print_r($_REQUEST,true));
	}
	
	$fname = trim($fname);
	$lname = trim($lname);
	
	
	$msg = "";
	if (strlen($fname) == 0)
	{
		$msg .= "<strong>fname</strong>: A First Name is Required<Br/>\n";
	}
	else
		$fname = ucwords(strtolower($fname));
		
	if (strlen($lname) == 0)
	{
		$msg .= "<strong>lname</strong>: A Last Name is Required<Br/>\n";
	}
	else
		$lname = ucwords(strtolower($lname));
		
	if (strlen($email) == 0)
	{
		$msg .= "<strong>email</strong>: An email address is Required<Br/>\n";
	}
#	else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))	//-- old email check that only checked format
	else if( !checkEmailValid($email) )	
	{
		$msg .= "<strong>email</strong>: A valid email address is Required<Br/>\n";
	}
	
	if (strlen($phone)  != 10)
	{
		$msg .= "<strong>phone</strong>: A Phone Number is Required: 10 digits, only numbers<Br/>\n";
	}
	else
	{
		//Phone Validation.	
		$p_area   = substr($phone,0,3);
		$p_prefix = substr($phone,3,3);	
		
		if ($p_area != 800 && $p_area != 866 && $p_area != 877 && $p_area != 888)
		{
			$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array())
				$msg = "invalid phone: Please provide a valid phone number.";		
		}
	}
	
	if (strlen($origin_city) == 0)
	{
		$msg .= "<strong>origin_city</strong>: An Origin City is Required<Br/>\n";
	}
	else
		$origin_city = ucwords(strtolower($origin_city));
		
	if (strlen($origin_state) != 2)
	{
		$msg .= "<strong>origin_state</strong>: An Origin State is Required: 2 letter code<Br/>\n";
	}
	
	if (strlen($origin_zip) == 0)
	{
		$sql = "select * from movingdirectory.zip_codes where city = '$origin_city' and state = '$origin_state'";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$origin_zip = $rs->myarray['zip'];
		}
		else
			$msg .= "<strong>origin_zip</strong>: An Origin Zip Code or a valid City-State combination.<Br/>\n";
	}
	else
	{
		$sql = "select * from movingdirectory.zip_codes where zip = $origin_zip";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$origin_city = $rs->myarray['city'];
			$origin_state = $rs->myarray['state'];
			$msg = str_replace("<strong>origin_city</strong>: An Origin City is Required<Br/>\n","",$msg);
			$msg = str_replace("<strong>origin_state</strong>: An Origin State is Required: 2 letter code<Br/>\n","",$msg);
		}
		else
			$msg .= "<strong>destination_zip</strong>: A Destination Zip Code or a valid City-State combination.<Br/>\n";
	}

	if (strlen($destination_city) == 0)
	{
		$msg .= "<strong>destination_city</strong>: A Destination City is Required<Br/>\n";
	}
	else
		$destination_city = ucwords(strtolower($destination_city));
		
	if (strlen($destination_state) != 2)
	{
		$msg .= "<strong>destination_state</strong>: A Destination State is Required: 2 letter code<Br/>\n";
	}

	// Markus - If Destination State is International
	if ($destination_state == "XX"){
		// Uncomment fake Zip if needed, if other script act on Zipcodes
		// $destination_zip = "99999"
	}else{
		if (strlen($destination_zip) == 0)
		{
			$sql = "select * from zip_codes where city = '$destination_city' and state = '$destination_state'";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
			{
				$destination_zip = $rs->myarray['zip'];
			}
			else
				$msg .= "<strong>destination_zip</strong>: A Destination Zip Code or a valid City-State combination.<Br/>\n";
		}
		else
		{
			$sql = "select * from movingdirectory.zip_codes where zip = '$destination_zip'";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
			{
				$destination_city = $rs->myarray['city'];
				$destination_state = $rs->myarray['state'];
				$msg = str_replace("<strong>destination_city</strong>: A Destination City is Required<Br/>\n","",$msg);
				$msg = str_replace("<strong>destination_state</strong>: A Destination State is Required: 2 letter code<Br/>\n","",$msg);
			}
			else
				$msg .= "<strong>destination_zip</strong>: A Destination Zip Code or a valid City-State combination.<Br/>\n";
		}
	}

	if (strlen($type) == 0) {
		if (strtolower($source) == "a1auto") {
			$type = "Car";
		} else {
			$msg .= "<strong>type</strong>: A Vehicle type is required.<Br/>\n";
		}
	} else {
		if (strtolower($source) == "a1auto") {
			//-- NOTE - 2/17/09 12:10 PM - TEMP SOLUTION - A1Auto is having issues with getting the vehicle type to us, so we are forcing it to go to CAR until they fix it.
/*
			$types = array('Sm-Car/Med-Car' => 'Car','XL-SUV/XL-PU' => 'Sport Utility','LgSUV/LgPU' => 'Sport Utility','XXL-SUV/XXL-PU' => 'Oversize','LgCar/SmSUV/SmPU' => 'Pick-up Truck');
			$type = $types[$type];//reformat it to our types for easier dupe checking.
*/
			//-- REMOVE THIS WHEN A1AUTO FIXES THEIR VEHICLE TYPE ISSUE
			$type = "Car";
		} else { 
			$type = ucwords(strtolower($type));	
		}
	}
	
	
	if (strlen($year) != 4)
		$msg .= "<strong>year</strong>: A Vehicle year is required. (4 digits) <Br/>\n";
	if (strlen($make) == 0)
		$msg .= "<strong>make</strong>: A Vehicle make is required.<Br/>\n";
	else
		$make = ucwords(strtolower($make));			
		
	if (strlen($model) == 0)
		$msg .= "<strong>model</strong>: A Vehicle model is required.<Br/>\n";
	else
		$model = ucwords(strtolower($model));		
		
	if (strlen($est_move_date) != 10)
		$msg .= "<strong>est_move_date</strong>: An Estimated Move Date is required. MM-DD-YYYY <Br/>\n";
	
	
	if ($running != "yes" && $running != "no")
	{		
		$msg .= "<strong>running</strong>: Vehicle condition required: yes or no<Br/>\n";
	}
	else
	{
		$running = (($running=="yes")?"r":"n");
	}
	
	if (strlen($source) == 0)
	{
		$msg .= "<strong>source</strong>: A Source code is required.<Br/>\n";
	}


	if ($contact == "")
		$contact = "email";
	
	$save_source = "aff_$source";

	if (strlen($msg) == 0)
	{
		$two_days_ago = date("YmdHis",(time() - (2*24*60*60)));
		$check = "select * from marble.auto_quotes where (lower(email) = '".strtolower($email)."' or phone = '$phone') and ".
					" vmake like '$make%' and origin_zip = '$origin_zip' ".
					" and destination_zip = '$destination_zip' and received > '$two_days_ago' ";
		$rs = new mysql_recordset($check);
		//mail("david@irelocation.com","Auto Lead Submission Check",$check);
		if ($rs->fetch_array())
			$msg = "Duplicate: Please wait for the Auto Transporters to contact you.";
	}
	
	if (strlen($msg) == 0)
	{
		define(TIMEZONE_OFFSET,18000);
		$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);
	
		//-- prevent sql injection attack
		$fname = mysql_real_escape_string($fname);
		$lname = mysql_real_escape_string($lname);
		$email = mysql_real_escape_string($email);
		$phone = mysql_real_escape_string($phone);
		$origin_city = mysql_real_escape_string($origin_city);
		$destination_city = mysql_real_escape_string($destination_city);
		$est_move_date = mysql_real_escape_string($est_move_date);
		$destination_state = mysql_real_escape_string($destination_state);
		$destination_zip = mysql_real_escape_string($destination_zip);
		$destination_country = mysql_real_escape_string($destination_country); // Markus 03/10/2009
		$origin_zip = mysql_real_escape_string($origin_zip);
		$origin_state = mysql_real_escape_string($origin_state);
		$contact = mysql_real_escape_string($contact);
		$save_source = mysql_real_escape_string($save_source);
		$type = mysql_real_escape_string($type);
		$model = mysql_real_escape_string($model);
		$year = mysql_real_escape_string($year);
		$make = mysql_real_escape_string($make);
		$running = mysql_real_escape_string($running);
		
		// Markus - 03/10/2009 Added Destination Country
		$sql = "insert into marble.auto_quotes set remote_ip = '".$_SERVER['REMOTE_ADDR']."', ".
		" received = '$now', fname = '$fname', lname ='$lname', ".
		" email = '$email', phone = '$phone', origin_city = '$origin_city', ".
		" destination_city = '$destination_city', est_move_date = '$est_move_date', ".
		" destination_state = '$destination_state', ".
		" destination_zip = '$destination_zip', ".
		" destination_country = '$destination_country', ".
		" origin_zip = '$origin_zip', ".
		" origin_state = '$origin_state', ".
		" contact = '$contact', customer_comments = '', ".
		" source = '$save_source', vtype = '$type', vmodel = '$model', vyear = '$year', ".
		" vmake = '$make', running = '$running' ";

//================== NEW SECTION 8-5-2008 *************
//-- This new section has been deactivated per Vinnie, who no longer wants to get any exclusive leads from A1Auto.  If this ever gets reactivated, make sure to comment out the rand and If condition immediately below this 'new code'
/*
		//-- if it's an A1AUTO lead, then we need to make 35 of these have a campaign of "carmoves" for the exclusive leads that Vinnie's co gets.
		$rand = rand(0,100);
		$for_email = "rand = $rand<br />\n\n";
		
		$day = date("Ymd");
		$for_email = "day = $day<br />\n\n";
		$exclusive_count = "select count(quote_id) as lead_count from marble.auto_quotes where left(received,8) = '$day' and campaign = 'carmoves' ";
		$for_email = "exclusive_count: $exclusive_count<br />\n\n";
		
		$rs = new mysql_recordset($exclusive_count);
		$rs->fetch_array();
		$lead_count = $rs->myarray["lead_count"];
		$for_email = "lead_count = $lead_count<br />\n\n";

		if ( $lead_count < 35 && eregi("a1auto",$save_source )  ) { //-- if carmoves has received less than 35 leads AND the lead is from a1auto then we include them in the randomization
			switch ( $rand ) {
				case $rand < 10: // make campaign ATUS
					$sql .= " , campaign = 'atus' ";		
					break;
			
				case $rand > 60: // make campaign AUTO
					$sql .= " , campaign = 'auto' ";
					break;
			
				default: 
					$sql .= " , campaign = 'carmoves' ";
					break;
			}
			
		} else { //-- we don't include carmoves 
			if ($rand < 10) {
				$sql .= " , campaign = 'atus' ";				
			} else {
				$sql .= " , campaign = 'auto' ";
			}
			
		}
		
		$for_email = "$sql<br />\n\n";
		$for_email = "<hr />\n\n";
		//-- mail me for testing data

*/
//================== END NEW SECTION 8-5-2008 *************

		//-- Vinnie doesn't want exclusive leads anymore, so we re-activate this.
		$rand = rand(0,100);
		if ($rand < 10)
			$sql .= " , campaign = 'atus' ";				
		else
			$sql .= " , campaign = 'auto' ";



		$rs = new mysql_recordset($sql);		
		
		//--mail("rob@irelocation.com","Ext Auto Leads - $source",$sql);
		
		if (isset($thankyou) && substr_count($thankyou,"http") > 0)
		{			
			header("Location: $thankyou?source=irelo&trackingid=".
					$rs->last_insert_id());
			exit();
		}
		else
		{
			echo "Lead Accepted: ".$rs->last_insert_id();
		}
		
	}
	else
	{
		//-- We are now inserting bad leads into the marble.auto_quotes_failed table to track these (not really sure we need this time offset, but what the heck...
		
		define(TIMEZONE_OFFSET,18000);
		$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);
	
/*
		Markus - 03/10/2009 Commented block added dude to being redundant

		//-- prevent sql injection attack
		$fname = mysql_real_escape_string($fname);
		$lname = mysql_real_escape_string($lname);
		$email = mysql_real_escape_string($email);
		$phone = mysql_real_escape_string($phone);
		$origin_city = mysql_real_escape_string($origin_city);
		$destination_city = mysql_real_escape_string($destination_city);
		$est_move_date = mysql_real_escape_string($est_move_date);
		$destination_state = mysql_real_escape_string($destination_state);
		$destination_zip = mysql_real_escape_string($destination_zip);
		$origin_zip = mysql_real_escape_string($origin_zip);
		$origin_state = mysql_real_escape_string($origin_state);
		$contact = mysql_real_escape_string($contact);
		$save_source = mysql_real_escape_string($save_source);
		$type = mysql_real_escape_string($type);
		$model = mysql_real_escape_string($model);
		$year = mysql_real_escape_string($year);
		$make = mysql_real_escape_string($make);
		$running = mysql_real_escape_string($running);
*/		

		// Markus - 03/10/2009 Added Destination Country
		$sql = "insert into marble.auto_quotes_failed set remote_ip = '".$_SERVER['REMOTE_ADDR']."', ".
		" received = '$now', fname = '$fname', lname ='$lname', ".
		" email = '$email', phone = '$phone', origin_city = '$origin_city', ".
		" destination_city = '$destination_city', est_move_date = '$est_move_date', ".
		" destination_state = '$destination_state', ".
		" destination_zip = '$destination_zip', ".
		" destination_country = '$destination_country', ".
		" origin_zip = '$origin_zip', ".
		" origin_state = '$origin_state', ".
		" contact = '$contact', customer_comments = '', ".
		" source = '$save_source', vtype = '$type', vmodel = '$model', vyear = '$year', ".
		" vmake = '$make', running = '$running' ";
		
		$rand = rand(0,100);
		if ($rand < 10) {
			$sql .= " , campaign = 'atus' ";				
		} else {
			$sql .= " , campaign = 'auto' ";
		}
		
		//-- Add Fail Reason to this SQL
		$sql .= " , fail_reason = '$msg' ";
			
		$rs = new mysql_recordset($sql);


		//--mail("rob@irelocation.com","Lead Fail","$email \n$source \n$msg");
		
		echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
		
		echo "<br/><strong>Note:</strong><Br/> Append a variable <strong>thankyou</strong> to have us redirect the users browser to your thankyou page.<br/> we will append tracking information for you to save:<Br/><Br/> &nbsp; &nbsp; &nbsp; [your_thank_you_page]?source=irelo&tracking_id=[unique_tracking_id]<br/>";
		echo "<br/>Append a variable <strong>rejectpage</strong> to have us redirect the user to a page if the lead fails submission<Br/>";
		echo "<Br/>All leads are US State to US State.<Br/>";
		echo "<Br/><strong>Duplicates:</strong>We Check for duplicate leads: if the following information is the same the lead is considered a duplicate.<Br/>";
		echo "<ul><li>(Phone Number or Email Address) AND</li><li>Origin Zip Code AND</li><li>Destination City AND </li>".
		"<li>Vehicle Make AND </li><li>Vehicle Model AND </li><li>Vehicle Year AND</li><li>Less than 2 Days old</li></ul>";
	}	
?>