<?
	include_once "../inc_mysql.php";
	
	session_start();
	define(EMAIL_REGEX,"^[_a-z0-9-]+(\.[\._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
	define(NUMBER_REGEX,"[^0-9]");
	define(DUPE_DATE_RANGE,48*60*60);//two days


	function cleanText($in)
	{		
		$in = str_replace("'","\'",$in);
		return trim(ereg_replace("[^a-zA-Z '-\.]","",$in));
	}
		
	function injectionTest($in)
	{
		if (substr_count($in,"href=") > 0 || substr_count($in,"url=") > 0)
			return -1;
		else
		{
			$in = str_replace("'","\'",$in);
			$in = str_replace("insert","add",$in);
			$in = str_replace("update","change",$in);
			$in = str_replace("delete","remove",$in);
			$in = str_replace("create","make",$in);
			$in = str_replace("drop","release",$in);
			$in = str_replace("select","choose",$in);
		}
	}	
		
	function submit($post_array,$site)
	{
		
		if (!is_array($post_array))
		{
			return "nodata";			
		}
		$array_keys = array_keys($post_array);
		foreach($array_keys as $key)
		{
			$post_array[$key] = trim($post_array[$key]);
			$_SESSION[$key] = $post_array[$key];
		}		
		extract($post_array);
		
		$fname = cleanText($fname);
		$lname = cleanText($lname);
		$destination_city = cleanText($destination_city);
		$vmake = cleanText($vmake);			
		if (strlen($comments) > 0)
		{
			$comments = injectionTest($comments);
			if ($comments < 0)
				return "linkinjection";						
		}
		
		if (!is_numeric($fname) && strlen($fname) > 2)
		{}//valid
		else
			return "fname";
		
		if (!is_numeric($lname) && strlen($lname) > 2)
		{}//valid
		else
			return "lname";
		
		if (eregi(EMAIL_REGEX, $email))
		{}//valid
		else
			return "email";
		
		if (strlen(ereg_replace(NUMBER_REGEX,"",$phone)) == 10)
		{
			$p_area   = substr($phone,0,3);
			$p_prefix = substr($phone,3,3);	
			
			$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array())
				return "phone_scrubbed";
		}
		else
			return "phone";
			
		if (strlen(ereg_replace(NUMBER_REGEX,"",$origin_zip)) == 5)
		{
			$sql = "select * from movingdirectory.zip_codes where zip = '$origin_zip' ";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array())
				return "origin_zip_scrubbed";
			else
			{
				$origin_city = $rs->myarray['city'];
				$origin_state = $rs->myarray['state'];
			}
		}
		else
			return "origin_zip";
		
		if (strlen($destination_state) == 2)
		{}//valid
		else
			return "destination_state";

		if (strlen($destination_city) >= 3)
		{}//valid
		else
			return "destination_city";
		
		$check_city = $destination_city;
		do 
		{
			$sql = "select * from movingdirectory.zip_codes where state = '$destination_state' and city like '$check_city%'";
			$rs = new mysql_recordset($sql);
			$check_city = substr($check_city,strlen($check_city)-3);
		}
		while(!$rs->fetch_array() && strlen($check_city) > 0);

		if (strlen($rs->myarray['city']) > 0)//found an entry.
		{
			$destination_city = $rs->myarray['city'];
			$destination_zip = $rs->myarray['zip'];
		}
		else
			return "destination_scrubbed";

		if (strlen($move_day) == 2 && is_numeric($move_day))
		{}//valid
		else
			return "est_move_date";

		if (strlen($move_month) == 2 && is_numeric($move_month))
		{}//valid
		else
			return "est_move_date";

		if (strlen($move_year) == 4 && is_numeric($move_year))
		{}//valid
		else
			return "est_move_date";
		
		$est_move_date = $move_month."-".$move_day."-".$move_year;
		if (($est_move_date) > date("m-d-Y"))
		{}//valid
			else	 
			return "est_move_date";

		if ($vtype != "XX")//drop down list.
		{}//valid 
		else
			return "vtype";		

		if 	(strlen($vmake) > 1)
		{}//valid 
		else
			return "vmake";

		if 	(strlen($vmodel) > 1)
		{}//valid 
		else
			return "vmodel";

		if (strlen($vyear) == 4 && is_numeric($vyear))
		{}//valid
		else
			return "vyear";

		$running = strtoupper($running);
		if ($running != 'R' && $running != 'N')
			return "running";
		
		//if it gets to here its valid so far. check for duplicates.

		$twodaysago = date("Ymdhis",time()-DUPE_DATE_RANGE);
		
		
		//return "valid";
		
		if ($site == "atus")//movingdirectory.quotes
		{
			$check = "select * from movingdirectory.quotes where ".
					" (email = '$email' or phone_home = '$phone') AND ".
					" origin_zip = '$origin_zip' AND destination_city = '$destination_city' ".
					" (received = '' OR received > '$twodaysago') AND ".
					" comments like '%Vehicle Model: $vmodel%' AND comments like '%Vehicle Make: $vmake%' ";				
			$rs = new mysql_recordset($check);
		}
		else//marble.auto_quotes
		{
			$check = "select * from marble.auto_quotes where (lower(email) = '$email' or phone = '$phone') and ".
					" vmake = '$vmake' and vmodel = '$vmodel' and origin_zip = '$origin_zip' ".
					" and destination_city = '$destination_city' and received > '$twodaysago' ";
			$rs = new marble($check);
		}
		
		if ($rs->fetch_array())
		{
			return "duplicate";		
		}
		
		if ($site == "atus")
		{			
			$est_move_date = $move_year."-".$move_month."-".$move_day;//quotes table stores it differantly.
			$customer_comments = $comments;
			
			$sql_comments = "Vehicle Type: $vtype
Vehicle Year: $vyear
Vehicle Make: $vmake
Vehicle Model: $vmodel

Comments: ".(($running=="R")?"running ":"not-running")." - $customer_comments";
			
			$sql = "insert into movingdirectory.quotes set name = '".ucwords($fname." ".$lname)."' ".
					" email = '$email', phone_home = '$phone', origin_city = '$origin_city', ".
					" destination_city = '$destination_city', est_move_date = '$est_move_date', ".
					" destination_state = '$destination_state', origin_zip = '$origin_zip', ".				
					" origin_state = '$origin_state', contact = '$contact', comments = '$sql_comments', ".
					" source = '$source', cat_id = '1', ready_to_send = '1'; ";
			$rs = new mysql_recordset($sql);
			return "valid";			
		}
		else
		{
			$sql = "insert into marble.auto_quotes set remote_ip = '".$_SERVER['REMOTE_ADDR']."', ".
					" received = '".date("YmdHis")."', fname = '$fname', lname ='$lname', ".
					" email = '$email', phone = '$phone', origin_city = '$origin_city', ".
					" destination_city = '$destination_city', est_move_date = '$est_move_date', ".
					" destination_state = '$destination_state', ".
					" destination_zip = '$destination_zip', origin_zip = '$origin_zip', ".
					" origin_state = '$origin_state', contact = '$contact', customer_comments = '$comments', ".
					" source = '$source', vtype = '$vtype', vmodel = '$vmodel', vyear = '$vyear', ".
					" vmake = '$vmake', running = '$running'; ";
			$rs = new marble($sql);
			return "valid";
		}	
	}

	$post_array = $_POST;
	$site = $post_array["source"];
	if (substr_count("_",$site) > 0)
	{	
		$site = split("_",$site,2);
		$site = $site[0];
	}	
	
	echo submit($post_array,$site);

?>
