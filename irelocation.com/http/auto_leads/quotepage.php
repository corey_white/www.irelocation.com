<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<strong>All Auto Quote Pages need to follow the following Naming Method:</strong>
<hr/>
	<Table>
		<Tr>
			<th>Field Name</th>
			<th>html field name</th>
			<th>Validation Type</th>
			<th>Description</th>
		</Tr>
		<Tr>
			<td>First Name</td>
			<td>fname</td>
			<td><code>!is_numeric($fname) && strlen($fname) > 2</code></td>
			<td>The first name of the lead</td>
		</Tr>
		<Tr>
			<td>Last Name</td>
			<td>lname</td>
			<td><code>!is_numeric($lname) && strlen($lname) > 2</code></td>
			<td>The Last name of the lead</td>
		</Tr>
		<tr>
			<td>Email Address</td>
			<td>email</td>
			<td><code>eregi(EMAIL_REGEX, $email)</code></td>
			<td>The leads email address</td>
		</tr>
		<tr>
			<td>Phone Number</td>
			<td>phone</td>
			<td><code>strlen(ereg_replace(PHONE_REGEX,"",$phone)) == 10</code></td>
			<td>The leads 10 digit US Phone number</td>
		</tr>
		<tr>
			<td>Origin Zip Code</td>
			<td>origin_zip</td>
			<td><code>validatie zip code, gather origin_city and origin_state</code></td>
			<td>The 5 digit zip code for the origin of the move.</td>
		</tr>
		<tr>
			<td>Destination City</td>
			<td>destination_city</td>
			<td><code>validated with destination_state variable</code></td>
			<td>The destination city of the move</td>
		</tr>
		<tr>
			<td>Destination State</td>
			<td>destination_state</td>
			<td><code>validated with destination_city variable</code></td>
			<td>The destination state of the move</td>
		</tr>
		<tr>
			<td>Vehicle Type</td>
			<td>vtype</td>
			<td><code>use a drop down list, not left unselected.</code></td>
			<td>car, truck, oversize, etc.</td>
		</tr>
		<tr>
			<Td>Vehicle Make</Td>
			<td>vmake</td>
			<td><code>(strlen($vmake) > 2)</code></td>
			<td>the maker of the vehicle, Ford, Mazda, Toyota, DMC, etc.</td>
		</tr>
		<tr>
			<td>Vehicle Model</td>
			<td>vmodel</td>
			<td><code>(strlen($vmake) > 1)</code></td>
			<td>The model of the car, Ranger, Rx-8, Mustang, etc</td>
		</tr>
		<tr>
			<td>Vehicle Year</td>
			<td>vyear</td>
			<td><code>(strlen($vyear) == 4 && is_numeric($vyear))</code></td>
			<td>The year of the car, Ranger, Rx-8, Mustang, etc</td>
		</tr>
		<tr>
			<td>Running Condition</td>
			<td>running</td>
			<td><code>($running == 'R' || $running == 'N')</code></td>
			<td>R if the car is operable, N otherwise.</td>
		</tr>
		<tr>
			<td>Estimated Move Day</td>
			<td>move_day</td>
			<td><code>(strlen($move_day) == 2 && is_numeric($move_day))</code></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Estimated Move Month</td>
			<td>move_month</td>
			<td><code>(strlen($move_month) == 2 && is_numeric($move_month))</code></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Estimated Move Year</td>
			<td>move_year</td>
			<td><code>(strlen($move_year) == 4 && is_numeric($move_year))</code></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Move Date Validation</td>
			<td>N/A</td>
			<td>
				<code>
					$est_move_date = $move_month."-".$move_day."-".$move_year;<br/>
					(($est_move_date) > date("m-d-Y"))
				</code>
			</td>
			<td>Forcing the move date to be in the future, (rejects today as a move date.)</td>
		</tr>
		<tr>
			<td>Customer Comments</td>
			<td>comments</td>
			<td><code>no link or sql injecting bastards</code></td>
			<td>*Optional</td>
		</tr>
	</Table>
</body>
</html>
