<?
	include('../nusoap/nusoap.php');
	
	define(NAMESPACE, 'http://adcenter.microsoft.com/syncapis');
    define(URI, 'https://beta6.api.idss.msn.com' );// Sandbox
    // For production use https://adcenterapi.microsoft.com

    define(ADMIN_PROXY,URI.'/v3/Administration/Administration.asmx');

    // Update the following to use your credentials.
    define(USER, 'irelocation-API');
    define(PWD, 'password');
    define(TOKEN, '36R7VKXYF4');

    $header = '<ApiUserAuthHeader xmlns="'.NAMESPACE.'">
        <UserName>'.USER.'</UserName>
        <Password>'.PWD.'</Password>
        <UserAccessKey>'.ACCESSKEY.'</UserAccessKey>
        </ApiUserAuthHeader>';

    $adminClient =  new soapclient(ADMIN_PROXY.'?wsdl', 'wsdl');
    $adminClient->soap_defencoding = 'UTF-8';
    $adminClient->setHeaders($header); 

    $arguments = array("APIFlags" => 0);

    $data = $adminClient->call('GetQuota', $arguments);
    $err = $adminClient->getError();
    if ($err)
    {
      echo $err."<br/>";
		exit();
    }

    $quota = $data['GetQuotaResult'];

    $data = $adminClient->call('GetQuotaBalance', $arguments);
    $err = $adminClient->getError();
    if ($err)
    {
    	echo $err."<br/>";
		exit();
    }

    $quotaBalance = $data['GetQuotaBalanceResult'];

    printf("The quota is %d and the quota balance is %d.", $quota, $quotaBalance);
?>