 
<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left" valign="top" bgcolor="#E5E5E8"> 
    <td height="106" colspan="3"> 
      <p align="center"><font color="#CC3333" size="5" face="Arial, Helvetica, sans-serif"><strong>iRelocation 
        will help you find your dream home!</strong></font></p>
      <p align="center"><font face="Arial, Helvetica, sans-serif"> <strong><font color="#666699">We 
        have over 20,000+ hand-selected Realtors from the nation's leading <br>
        Real Estate Companies ready to help you find your new home.</font></strong></font></p>
      <p align="center"><font size="2" face="Arial, Helvetica, sans-serif"> <font color="#000000"><strong>A 
        consultant will contact you first to discuss your needs, and then put 
        you in contact with a <br>
        Realtor that best fits you! Most of our realtors have exclusive listings 
        available near you.</strong></font><br>
        <br>
        </font></p>
      </td>
  </tr>
  <tr align="left" valign="top"> 
    <td width="223" rowspan="2" align="center" bgcolor="#E5E5E8"><img src="images/house.jpg" width="223" height="184"><br>
      <br>
      <a href="http://1stmovingdirectory.com/privacy_policy.php"><img src="images/trust.jpg" width="114" height="50" border="0"></a> 
    </td>
    <td colspan="2"><img src="images/housebg.jpg" width="537" height="22"></td>
  </tr>
  <tr> 
    <td width="359" height="123" align="center" valign="top" bgcolor="#E5E5E8"> 
      <form method="POST" action="realtor_submit.php">
	  <? if (strlen($_REQUEST['source']) > 0) { ?>
	  	<input type="hidden" name="source" value="<?= $_REQUEST['source'] ?>" />
		<? }  else { ?>
			<input type="hidden" name="source" value="<?= $_SESSION['source'] ?>" />
		<? } ?>
        <table width="359" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
          <?
			$assignTo = "";
		  	if (strlen($_REQUEST['assignTo']) > 0)
			{
				$assignTo = $_REQUEST['assignTo'];
				$_SESSION['assignTo'] = $assignTo;
			}
			else if (strlen($_SESSION['assignTo']) > 0)
				$assignTo = $_SESSION['assignTo'];
				
			if ($assignTo != "")
				echo "<input type='hidden' name='assignTo' value='$assignTo' />\n";
				
		  	if (isset($_REQUEST['msg']))
			{
				$msg = $_REQUEST['msg'];
				$msgs["fname"] = "Please enter your first name.";
				$msgs["lname"] = "Please enter your last name.";
				$msgs["dphone"] = "Please enter your phone number.";
				$msgs["invalid_dphone"] = "Please enter a valid phone number.";
				$msgs["email"] = "Please enter your email address.";				
				$msgs["buying_value"] = "Please Select how much you would like to spend.";
				$msgs["selling_value"] = "Please Select how much you would like to receive for your house.";
				$msgs["help"] = "Please Select how we can help you.";
				$msgs["duplicate"] = "We see that you have already submitted a quote, please wait for our agents to call you.";
				$error_msg = $msgs[$msg];
				if (strlen($error_msg) > 0)
				?>
				<tr bgcolor="#A2AFC7"> 
		           <td height="29" colspan="2" align="center">
				   	<font color="#FF0000" size="2" face="Arial, Helvetica, sans-serif">
						<strong>
							<?= $error_msg ?>
						</strong>
					</font>
					</td>
	          </tr>				
				<?
						
			
			}
		  
		  ?>
		  
		  <tr bgcolor="#A2AFC7"> 
		  
            <td height="29" colspan="2"><font color="#FFFFFF" size="2" face="Arial, Helvetica, sans-serif"><strong>Tell 
              us a little bit about what you are looking for.</strong></font></td>
          </tr>
          <tr> 
            <td width="118" align="left" <?= ((substr_count($msg,"fname")>0)?"class='errortext' ":"class='maintext' ") ?>>First 
              Name:</td>
            <td width="241"><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input type="text" name="fname" value="<?= $_SESSION['fname'] ?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">
              * </font></td>
          </tr>
          <tr> 
            <td align="left" <?= ((substr_count($msg,"lname")>0)?"class='errortext' ":"class='maintext' ") ?>>Last Name:</td>
            <td><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input type="text" name="lname" value="<?= $_SESSION['lname'] ?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">
              * </font></td>
          </tr>
          <tr> 
            <td align="left" <?= ((substr_count($msg,"email")>0)?"class='errortext' ":"class='maintext' ") ?>>Email:</font></td>
            <td><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input type="text" name="email" value="<?= $_SESSION['email'] ?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">
              * </font></td>
          </tr>
          <tr> 
            <td align="left" <?= ((substr_count($msg,"dphone")>0)?"class='errortext' ":"class='maintext' ") ?>>Daytime Phone:</font></td>
            <td><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input type="text" name="phone" value="<?= $_SESSION['phone'] ?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">
              * </font></td>
          </tr>
          <tr> 
            <td align="left" <?= ((substr_count($msg,"ephone")>0)?"class='errortext' ":"class='maintext' ") ?>>Evening Phone:</font></td>
            <td><font face="Arial, Helvetica, sans-serif" size="2"> 
              <input type="text" name="phone2" value="<?= $_SESSION['phone2'] ?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">
              * </font></td>
          </tr>
          <tr> 
            <td colspan="2"></td>
          </tr>
        </table>
        <table width="359" border="0" align="center" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
          <tr> 
            <td width="8%" align="right"> <input type="checkbox" name="buying" value="yes"<? if($_SESSION['buying']=='yes'){echo " CHECKED";}?>></td>
            <td width="90%" align="left" <?= ((substr_count($msg,"buying")>0)?"class='errortext' ":"class='maintext' ") ?>>Buying a home in</font> 
            </td>
          </tr>
          <tr> 
		  <? 
		  	$buying_state = $_SESSION['buying_state']; 
			$selling_state = $_SESSION['selling_state']; 
	 	  ?>
            <td width="8%" align="right">&nbsp;</td>
            <td><select name="buying_state" size="1" style="font-family: Tahoma; font-size: 8pt;">
                <option value="XX"<? if($buying_state==''){echo " selected";}?>>Select 
                One</option>
                <option value="AL"<? if($buying_state=='AL'){echo " selected";}?>>Alabama</option>
                <option value="AK"<? if($buying_state=='AK'){echo " selected";}?>>Alaska</option>
                <option value="AZ"<? if($buying_state=='AZ'){echo " selected";}?>>Arizona</option>
                <option value="AR"<? if($buying_state=='AR'){echo " selected";}?>>Arkansas</option>
                <option value="CA"<? if($buying_state=='CA'){echo " selected";}?>>California</option>
                <option value="CO"<? if($buying_state=='CO'){echo " selected";}?>>Colorado</option>
                <option value="CT"<? if($buying_state=='CT'){echo " selected";}?>>Connecticut</option>
                <option value="DE"<? if($buying_state=='DE'){echo " selected";}?>>Delaware</option>
                <option value="DC"<? if($buying_state=='DC'){echo " selected";}?>>District 
                Of Columbia</option>
                <option value="FL"<? if($buying_state=='FL'){echo " selected";}?>>Florida</option>
                <option value="GA"<? if($buying_state=='GA'){echo " selected";}?>>Georgia</option>
                <option value="HI"<? if($buying_state=='HI'){echo " selected";}?>>Hawaii</option>
                <option value="ID"<? if($buying_state=='ID'){echo " selected";}?>>Idaho</option>
                <option value="IL"<? if($buying_state=='IL'){echo " selected";}?>>Illinois</option>
                <option value="IN"<? if($buying_state=='IN'){echo " selected";}?>>Indiana</option>
                <option value="IA"<? if($buying_state=='IA'){echo " selected";}?>>Iowa</option>
                <option value="KS"<? if($buying_state=='KS'){echo " selected";}?>>Kansas</option>
                <option value="KY"<? if($buying_state=='KY'){echo " selected";}?>>Kentucky</option>
                <option value="LA"<? if($buying_state=='LA'){echo " selected";}?>>Louisiana</option>
                <option value="ME"<? if($buying_state=='ME'){echo " selected";}?>>Maine</option>
                <option value="MD"<? if($buying_state=='MD'){echo " selected";}?>>Maryland</option>
                <option value="MA"<? if($buying_state=='MA'){echo " selected";}?>>Massachusetts</option>
                <option value="MI"<? if($buying_state=='MI'){echo " selected";}?>>Michigan</option>
                <option value="MN"<? if($buying_state=='MN'){echo " selected";}?>>Minnesota</option>
                <option value="MS"<? if($buying_state=='MS'){echo " selected";}?>>Mississippi</option>
                <option value="MO"<? if($buying_state=='MO'){echo " selected";}?>>Missouri</option>
                <option value="MT"<? if($buying_state=='MT'){echo " selected";}?>>Montana</option>
                <option value="NE"<? if($buying_state=='NE'){echo " selected";}?>>Nebraska</option>
                <option value="NV"<? if($buying_state=='NV'){echo " selected";}?>>Nevada</option>
                <option value="NH"<? if($buying_state=='NH'){echo " selected";}?>>New 
                Hampshire</option>
                <option value="NJ"<? if($buying_state=='NJ'){echo " selected";}?>>New 
                Jersey</option>
                <option value="NM"<? if($buying_state=='NM'){echo " selected";}?>>New 
                Mexico</option>
                <option value="NY"<? if($buying_state=='NY'){echo " selected";}?>>New 
                York</option>
                <option value="NC"<? if($buying_state=='NC'){echo " selected";}?>>North 
                Carolina</option>
                <option value="ND"<? if($buying_state=='ND'){echo " selected";}?>>North 
                Dakota</option>
                <option value="OH"<? if($buying_state=='OH'){echo " selected";}?>>Ohio</option>
                <option value="OK"<? if($buying_state=='OK'){echo " selected";}?>>Oklahoma</option>
                <option value="OR"<? if($buying_state=='OR'){echo " selected";}?>>Oregon</option>
                <option value="PA"<? if($buying_state=='PA'){echo " selected";}?>>Pennsylvania</option>
                <option value="RI"<? if($buying_state=='RI'){echo " selected";}?>>Rhode 
                Island</option>
                <option value="SC"<? if($buying_state=='SC'){echo " selected";}?>>South 
                Carolina</option>
                <option value="SD"<? if($buying_state=='SD'){echo " selected";}?>>South 
                Dakota</option>
                <option value="TN"<? if($buying_state=='TN'){echo " selected";}?>>Tennessee</option>
                <option value="TX"<? if($buying_state=='TX'){echo " selected";}?>>Texas</option>
                <option value="UT"<? if($buying_state=='UT'){echo " selected";}?>>Utah</option>
                <option value="VT"<? if($buying_state=='VT'){echo " selected";}?>>Vermont</option>
                <option value="VA"<? if($buying_state=='VA'){echo " selected";}?>>Virginia</option>
                <option value="WA"<? if($buying_state=='WA'){echo " selected";}?>>Washington</option>
                <option value="WV"<? if($buying_state=='WV'){echo " selected";}?>>West 
                Virginia</option>
                <option value="WI"<? if($buying_state=='WI'){echo " selected";}?>>Wisconsin</option>
                <option value="WY"<? if($buying_state=='WY'){echo " selected";}?>>Wyoming</option>
              </select> &nbsp; <select name="buying_value" size="1" style="font-family: Tahoma; font-size: 8pt;">
                <option value="XX"<? if($buying_value==''){echo " selected";}?>>Estimated 
                Home Value</option>
                <? for($v=125;$v<800;$v+=25) {?>
                <option value="<?= $v ?>"<? if($buying_value==$v){echo " selected";}?>><?echo "\$".$v.",000";?></option>
                <?}?>
                <option value="800"<? if($buying_value=='800'){echo " selected";}?>>$800,000+</option>
              </select></td>
          </tr>
          <tr> 
            <td width="8%" align="right"> <input type="checkbox" name="selling" value="yes"<? if($selling=='yes'){echo " CHECKED";}?>></td>
            <td width="90%" align="left" <?= ((substr_count($msg,"selling")>0)?"class='errortext' ":"class='maintext' ") ?>>Selling a home in</font> 
            </td>
          </tr>
          <tr> 
            <td width="8%" align="right">&nbsp;</td>
            <td><select name="selling_state" size="1" style="font-family: Tahoma; font-size: 8pt;">
                <option value="XX"<? if($selling_state==''){echo " selected";}?>>Select 
                One</option>
                <option value="AL"<? if($selling_state=='AL'){echo " selected";}?>>Alabama</option>
                <option value="AK"<? if($selling_state=='AK'){echo " selected";}?>>Alaska</option>
                <option value="AZ"<? if($selling_state=='AZ'){echo " selected";}?>>Arizona</option>
                <option value="AR"<? if($selling_state=='AR'){echo " selected";}?>>Arkansas</option>
                <option value="CA"<? if($selling_state=='CA'){echo " selected";}?>>California</option>
                <option value="CO"<? if($selling_state=='CO'){echo " selected";}?>>Colorado</option>
                <option value="CT"<? if($selling_state=='CT'){echo " selected";}?>>Connecticut</option>
                <option value="DE"<? if($selling_state=='DE'){echo " selected";}?>>Delaware</option>
                <option value="DC"<? if($selling_state=='DC'){echo " selected";}?>>District 
                Of Columbia</option>
                <option value="FL"<? if($selling_state=='FL'){echo " selected";}?>>Florida</option>
                <option value="GA"<? if($selling_state=='GA'){echo " selected";}?>>Georgia</option>
                <option value="HI"<? if($selling_state=='HI'){echo " selected";}?>>Hawaii</option>
                <option value="ID"<? if($selling_state=='ID'){echo " selected";}?>>Idaho</option>
                <option value="IL"<? if($selling_state=='IL'){echo " selected";}?>>Illinois</option>
                <option value="IN"<? if($selling_state=='IN'){echo " selected";}?>>Indiana</option>
                <option value="IA"<? if($selling_state=='IA'){echo " selected";}?>>Iowa</option>
                <option value="KS"<? if($selling_state=='KS'){echo " selected";}?>>Kansas</option>
                <option value="KY"<? if($selling_state=='KY'){echo " selected";}?>>Kentucky</option>
                <option value="LA"<? if($selling_state=='LA'){echo " selected";}?>>Louisiana</option>
                <option value="ME"<? if($selling_state=='ME'){echo " selected";}?>>Maine</option>
                <option value="MD"<? if($selling_state=='MD'){echo " selected";}?>>Maryland</option>
                <option value="MA"<? if($selling_state=='MA'){echo " selected";}?>>Massachusetts</option>
                <option value="MI"<? if($selling_state=='MI'){echo " selected";}?>>Michigan</option>
                <option value="MN"<? if($selling_state=='MN'){echo " selected";}?>>Minnesota</option>
                <option value="MS"<? if($selling_state=='MS'){echo " selected";}?>>Mississippi</option>
                <option value="MO"<? if($selling_state=='MO'){echo " selected";}?>>Missouri</option>
                <option value="MT"<? if($selling_state=='MT'){echo " selected";}?>>Montana</option>
                <option value="NE"<? if($selling_state=='NE'){echo " selected";}?>>Nebraska</option>
                <option value="NV"<? if($selling_state=='NV'){echo " selected";}?>>Nevada</option>
                <option value="NH"<? if($selling_state=='NH'){echo " selected";}?>>New 
                Hampshire</option>
                <option value="NJ"<? if($selling_state=='NJ'){echo " selected";}?>>New 
                Jersey</option>
                <option value="NM"<? if($selling_state=='NM'){echo " selected";}?>>New 
                Mexico</option>
                <option value="NY"<? if($selling_state=='NY'){echo " selected";}?>>New 
                York</option>
                <option value="NC"<? if($selling_state=='NC'){echo " selected";}?>>North 
                Carolina</option>
                <option value="ND"<? if($selling_state=='ND'){echo " selected";}?>>North 
                Dakota</option>
                <option value="OH"<? if($selling_state=='OH'){echo " selected";}?>>Ohio</option>
                <option value="OK"<? if($selling_state=='OK'){echo " selected";}?>>Oklahoma</option>
                <option value="OR"<? if($selling_state=='OR'){echo " selected";}?>>Oregon</option>
                <option value="PA"<? if($selling_state=='PA'){echo " selected";}?>>Pennsylvania</option>
                <option value="RI"<? if($selling_state=='RI'){echo " selected";}?>>Rhode 
                Island</option>
                <option value="SC"<? if($selling_state=='SC'){echo " selected";}?>>South 
                Carolina</option>
                <option value="SD"<? if($selling_state=='SD'){echo " selected";}?>>South 
                Dakota</option>
                <option value="TN"<? if($selling_state=='TN'){echo " selected";}?>>Tennessee</option>
                <option value="TX"<? if($selling_state=='TX'){echo " selected";}?>>Texas</option>
                <option value="UT"<? if($selling_state=='UT'){echo " selected";}?>>Utah</option>
                <option value="VT"<? if($selling_state=='VT'){echo " selected";}?>>Vermont</option>
                <option value="VA"<? if($selling_state=='VA'){echo " selected";}?>>Virginia</option>
                <option value="WA"<? if($selling_state=='WA'){echo " selected";}?>>Washington</option>
                <option value="WV"<? if($selling_state=='WV'){echo " selected";}?>>West 
                Virginia</option>
                <option value="WI"<? if($selling_state=='WI'){echo " selected";}?>>Wisconsin</option>
                <option value="WY"<? if($selling_state=='WY'){echo " selected";}?>>Wyoming</option>
              </select> &nbsp; <select name="selling_value" size="1" style="font-family: Tahoma; font-size: 8pt;">
                <option value="XX"<? if($selling_value==''){echo " selected";}?>>Estimated 
                Home Value</option>
                <? for($v=125; $v<800; $v+=25){?>
                <option value="<?= $v ?>"<? if($selling_value==$v ){echo " selected";}?>><?echo "\$".$v.",000";?></option>
                <?}?>
                <option value="800"<? if($selling_value=='800'){echo " selected";}?>>$800,000+</option>
              </select></td>
          </tr>
          <tr> 
            <td width="98%" colspan="2" height="23"><b><font face="Arial, Helvetica, sans-serif" size="1">Do 
              you have any comments that you would like to add?</font></b></td>
          </tr>
          <tr> 
            <td width="8%">&nbsp;</td>
            <td width="90%"> <textarea name="comments" cols="48" rows="5" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8"><?echo $comments;?></textarea></td>
          </tr>
          <tr> 
            <td colspan="2" align="center" bgcolor="#FFFFFF"><input name="image" type="image" src="images/submit1.gif" width="150" height="26" border="0"></td>
          </tr>
        </table>
      </form></td>
    <td width="178" align="center" valign="top" bgcolor="#E5E5E8"><img src="images/logos.jpg" width="178" height="313"></td>
  </tr>
  
</table>
</html>