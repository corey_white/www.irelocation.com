<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>The iRelocation Network - Terms & Conditions</title>
<!-- <?
//include_once("inc_session.php");
?>
</head>

<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#C0C0C0">

<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%" bgcolor="#FFFFFF">
	<tr>
		<td height="53">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td><a target="_blank" href="http://www.irelocation.com">
				<img border="0" src="images/logo_small.gif" width="144" height="53"></a></td>
				<td>
				<img border="0" src="images/slogan.gif" width="341" height="13" align="right"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td background="images/bar.gif" height="18">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td>
		<table border="0" width="100%" id="table3" cellspacing="0" cellpadding="0" height="100%">
			<tr>
				<td width="25">&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td valign="top">
				<blockquote>
&nbsp;<p>
	<font face="Tahoma" size="5">Terms &amp; Conditions</font></p>
					
              <p><b><font face="Tahoma" size="1">DISCLAIMER CONCERNING USE OF 
                THIS WEBSITE </font></b></p>
					<p><font face="Tahoma" size="1">Notice: This site and the 
					content contained on this site are protected under federal 
					laws, state laws and international treaties. Permission to 
					use this site, the information and documents contained 
					herein are granted only for purposes intended, under the 
					terms as stated herein. This website and all content on this 
					website are owned by The iRelocation Network, LLC. Use of 
					this site, the content on this site and the information 
					herein are being provided to the user of this site under 
					license to use the information contained herein only for the 
					purposes intended by The iRelocation Network and permitted 
					under law. The user acknowledges and agrees to this license 
					and to use this site and the content contained herein only 
					for the purpose intended. Unauthorized use, reproduction or 
					distribution of the content on this site, or any portion of 
					it, is strictly forbidden and may result in severe civil and 
					criminal penalties, and will be prosecuted to the maximum 
					extent possible under the law. </font></p>
					<p><font face="Tahoma" size="1">Notice specific to 
					information on this website Any person is hereby authorized 
					to view the information available from this web server for 
					informational purposes only and only for purposes intended, 
					under the terms as stated herein. Use of the information 
					from this site is for the purposes intended for this site 
					and will not be copied or posted on any network computer or 
					broadcast in any media, or used for any pecuniary gain 
					except as permitted under the terms of use with the 
					information provider. No part of the information on this 
					server may be redistributed, copied, or reproduced without 
					prior written consent of The iRelocation Network. </font>
					</p>
					<p><font face="Tahoma" size="1">Notice specific to documents 
					available on this website. Permission to use documents from 
					this server (&quot;server&quot;) is granted according to the terms 
					outlined herein provided that (1) the below copyright notice 
					appears in all copies and that both the copyright notice and 
					this permission notice appear; (2) use of such documents 
					from this server is for the purposes intended for this site 
					and will not be copied or posted on any network computer or 
					broadcast in any media, or used for any pecuniary gain 
					except as permitted under the terms of use with the document 
					provider; and, (3) no modifications of any documents are 
					made. Use for any other purpose is expressly prohibited by 
					law, and may result in severe civil and criminal penalties. 
					Violators will be prosecuted to the maximum extent possible.
					</font></p>
					<p><font face="Tahoma" size="1">Documents specified above do 
					not include the design or layout of The iRelocation Network 
					website or any other site owned, operated, licensed or 
					controlled by The iRelocation Network or any of its 
					affiliated entities. Elements of The iRelocation Network 
					websites are protected by trade dress, trademark, unfair 
					competition, and other federal and state laws and may not be 
					copied or imitated in whole or in part. No logo, graphic, 
					sound or image from any The iRelocation Network website may 
					be copied or retransmitted unless expressly permitted by The 
					iRelocation Network. </font></p>
					<p><font face="Tahoma" size="1">Limitation of warranty; no 
					endorsements The iRelocation Network and/or its respective 
					supplier's make no representations about the suitability of 
					the content including, but not limited to, descriptions or 
					information contained in the documents and related graphics 
					published on this server for any purpose. All such documents 
					and related graphics are provided &quot;as is&quot; without warranty 
					or endorsements of any kind including, but not limited to, 
					the implied warranties of merchantability, fitness for a 
					particular purpose, or non-infringement. The iRelocation 
					Network and/or its respective suppliers hereby disclaim all 
					warranties and conditions with regard to this information, 
					including all implied warranties and conditions of 
					merchantability, fitness for a particular purpose, title and 
					non-infringement. In no event shall The iRelocation Network 
					and/or its respective suppliers be liable for any special, 
					indirect or consequential damages or any damages whatsoever 
					resulting from loss of use, data or profits, whether in an 
					action of contract, negligence or other tortious action, 
					arising out of or in connection with the use or performance 
					of information available from this server. </font></p>
					<p><font face="Tahoma" size="1">Descriptions of, or 
					references to, products, services or publications within The 
					iRelocation Network web server does not imply endorsement 
					approval of that product, service or publication. The 
					iRelocation Network makes no warranty of any kind with 
					respect to the subject matter included herein, the products 
					listed herein, or the completeness or accuracy of the 
					information. The iRelocation Network specifically disclaims 
					all warranties, express, implied or otherwise, including 
					without limitation, all warranties of merchantability and 
					fitness for a particular purpose. </font></p>
					<p><font face="Tahoma" size="1">The content, including but 
					not limited to, documents and related graphics published on 
					this server could include technical inaccuracies or 
					typographical errors. Changes are periodically added to the 
					information herein without prior notice. The iRelocation 
					Network and/or its respective suppliers may make 
					improvements and/or changes in the product(s) and/or the 
					program(s) described herein at any time. </font></p>
					<p><font face="Tahoma" size="1">In no event shall The 
					iRelocation Network and/or its respective suppliers be 
					liable for any special, indirect or consequential damages or 
					any damages whatsoever resulting from loss of use, data or 
					profits, whether in an action of contract, negligence or 
					other tortious action, arising out of or in connection with 
					the use or performance of software, documents, provision of 
					or failure to provide services, or information available 
					from this server. </font></p>
					<p><font face="Tahoma" size="1">Links to third party sites. 
					Any links in this area will let you leave The iRelocation 
					Network site. The linked sites are not under the control of 
					The iRelocation Network and The iRelocation Network is not 
					responsible for the content of any linked site or any link 
					contained in a linked site, or any changes or updates to 
					such sites. The iRelocation Network is not responsible for 
					webcasting or any other form of transmission received from 
					any linked site. The iRelocation Network is providing these 
					links to you only as a convenience, and the inclusion of any 
					link does not imply endorsement by The iRelocation Network 
					of the site. </font></p>
					<p><font face="Tahoma" size="1">References to corporations, 
					their services and products, are provided &quot;as is&quot; without 
					warranty of any kind, either expressed or implied. In no 
					event shall The iRelocation Network be liable for any 
					special, incidental, indirect or consequential damage of any 
					kind, or any damages whatsoever resulting from loss of use, 
					data or profits, whether or not advised of the possibility 
					of damage, and on any theory of liability, arising out of or 
					in connection with the use or performance of this 
					information. Any rights not expressly granted herein are 
					reserved. </font></p>
				</blockquote>
				</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="25">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="20">
		<table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0" height="20">
			<tr>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#BEC3BD">
				<p align="center"> <img border="0" src="images/clear.gif" width="1" height="1"><b><font face="Tahoma" color="#5E5E5E" size="1">The 
              iRelocation Network � 2004-<?echo Date("Y");?> </font></b></p>
				</td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="1">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td height="3">
		<table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" height="3">
			<tr>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#CBBDB9">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</body>

</html>
