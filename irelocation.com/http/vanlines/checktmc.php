<?
//	define(EMAILS,"david@irelocation.com");
	define(EMAILS,"david@irelocation.com,".
					"bill.sheehan@relocation.com,".
					"joe.gilhawley@relocation.com");

	define(CELL_PHONES,"9176570171@cingularme.com,".
						"9175740900@cingularme.com,".
						"9175732948@cingularme.com");

	function quickCurl($url)
	{	
		$ch = curl_init($url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		return curl_exec($ch);	
	}
	
	function sendErrorMessage($msg)
	{
		mail(EMAILS,"!TopMovingCompanies Error!",$msg);
		mail(CELL_PHONES,"TMC ERROR",$msg);
	}

	function sendWarningMessage($msg)
	{
		mail(EMAILS,"TopMovingCompanies Alert",$msg);
		mail(CELL_PHONES,"TMC ALERT",$msg);
	}
	
	$urls = array();
	$urls[] = array("http://topmovingcompanies.com/",15000);
	$urls[] = array("http://www.topmovingcompanies.com/autoquotes",20000);
	$urls[] = array("http://www.topmovingcompanies.com/movingquotes",22000);
	//$urls[] = array("http://www.topmovingcompanies.com/a",1000);

	/*
	foreach($urls as $data)
	{
		$result = quickCurl($data[0]);
		echo "Result Length: ".strlen($result)."<br/>";
		echo "Should be Well Over: ".$data[1]."<br/>";
		if (substr_count($result,"<title>404 Not Found</title>") > 0)
		{
			sendErrorMessage("Error! ".$data[0]." was not found! (404'ed!)");
		}
		else if (strlen($result) < $data[1])//did something change?
		{
			sendWarningMessage("TopMovingCompanies ".$data[0]."  is Much Smaller than Expected.");
		}
	}
	*/
	
?>