  $(document).ready(function() {
 
    //Change these values to style your modal popup
    var source = "quote.php";
    var width = 450;
    var align = "center";
    var top = 100;
    var padding = 5;
    var backgroundColor = "#FFFFFF";
    var borderColor = "#000000";
    var borderWeight = 4;
    var borderRadius = 5;
    var fadeOutTime = 300;
    var disableColor = "#666666";
    var disableOpacity = 40;
    var loadingImage = "/webdesign/images/loading.gif";
 
    //This method initialises the modal popup
    $(".modal").click(function() {
 
        modalPopup( align,
		    top,
		    width,
		    padding,
		    disableColor,
		    disableOpacity,
		    backgroundColor,
		    borderColor,
		    borderWeight,
		    borderRadius,
		    fadeOutTime,
		    source,
		    loadingImage );
 
    });	
 
    //This method hides the popup when the escape key is pressed
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            closePopup(fadeOutTime);
        }
    });
 
  });
