function formCheck(form)
{	
	var valid = true;
	
	$('input[type="text"]').each(function(index)
	{
		$(this).removeClass('error');
	    if($(this).hasClass('req'))
	    {
		    if($(this).val()==$(this).attr('title'))
		    {
	    		valid=false;
	    		$(this).addClass('error');
	    	}
	    }
	});
	
	if(valid==false)
	{
		alert('Please complete the highlighted fields.');
    	return false;
	}
	else
	{
		var name = form.full_name.value;
		var email = form.email.value;
		var phone1 = form.phone1.value; 
		var phone2 = form.phone2.value; 
		var phone3 = form.phone3.value; 
		var website = form.website.value;
		var comments = form.comments.value;
		
		
		if(name.length<3 || name == undefined){
			valid=false;
			$('#full_name').addClass('error');
		}
		else
		{
			$('#full_name').removeClass('error');
		}
		
		if(email != ''){
	        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	        if(regex.test(email) == false) {
	            valid=false;
	            $("#email").addClass("error");
	            
	        }
	        else{
	            $("#email").removeClass("error")
	        }
	    }
	    else{
	        if(email.length<3){
	            valid=false;
	            $("#email").addClass("error");
	        }
	    }
	    
	    var phone1_1 = parseInt(phone1.substr(0, 1));
	    if(phone1.length != 3 || phone1 < 0 || phone1_1 < 2) {
	        valid=false;
	        $('#phone1').addClass('error');
	    }
	    else{
	        $("#phone1").removeClass("error");
	    } 
	    
	    var phone2_1 = parseInt(phone2.substr(0, 1));
	    if(phone2.length != 3 || phone2 < 1 || phone2_1 < 2) {
	        valid=false;
	        $('#phone2').addClass('error');
	    }
	    else{
	        $("#phone2").removeClass("error");
	    }
	    
	    if(phone3.length != 4 || phone3 < 1) {
	        valid = false;
	        $('#phone3').addClass('error');
	    } 
	    else{
	        $("#phone3").removeClass("error");
	    }
	    
	    if(valid==true){
			
			window.optimizely = window.optimizely || [];
  			window.optimizely.push(['trackEvent', 'submit']);
			
			var data = $(form).serialize();
			$.ajax({
				type: 'POST',
			  	url: 'email.php',
			  	data: data
			}).done(function(){
				$('.form').hide();
				$('#conv').attr('src','conv.php');
				$(".message").empty().html("<p class='formtitle'>Thanks!</p><p>We've received your inquiry and will be in contact with you shortly.</h2>");
			});
			
			
			return false;
					
		}
		else
		{
			alert('Please complete the required fields.');
			return false;
		}
	}
}
