<?php

function html_email($to, $from, $subject, $message) {
    $html = <<<EOHTML
<html>
<head>
<title>$subject</title>
</head>
<body>
    $message
</body>
</html>
EOHTML;
    $headers[] = "From: $from";
    $headers[] = "Content-type: text/html; charset=iso-8859-1";

    $headers_str = join("\n", $headers);

    $to = split(',', $to);
    foreach ($to as $email) {
        mail(
            $email,
            $subject,
            $html,
            $headers_str
        );
    }
}


if($_SERVER['REQUEST_METHOD'] == 'POST'){
   
    //tack on a couple variables
    $ip = $_SERVER['REMOTE_ADDR'];
    $user = $_SERVER['HTTP_USER_AGENT'];
    
    //Email the comments
    $msg = "
        <h1>{$_POST['full_name']} - iRelo Web Design Lead</h1>
        <p>Name: {$_POST['full_name']}</p>
        <p>Email: {$_POST['email']}</p>
        <p>Phone: {$_POST['phone1']} {$_POST['phone2']} {$_POST['phone3']}</p>
        <p>Comments:</p> <p>".($_POST['comments'] != '' ? $_POST['comments'] : '')."</p>
        <p>Website: ".($_POST['website'] != 'Website' ? $_POST['website'] : '')."</p>
        <p style='font-size:10px'>{$ip}<br />{$user}</p>
    ";
        
    @html_email('belinda@irelocation.com,tory@irelocation.com,alyonachirinos@gmail.com','irelocation.com/webdesign','Lead Form',$msg);
}