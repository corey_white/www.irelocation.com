<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<title>iRelo | Website Conversion Specialists</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery1.8.1.js"></script>
<?php include('optimizely.php'); ?>
<script type="text/javascript" src="js/modal.popup.js"></script>
<script type="text/javascript" src="js/slideshow.js"></script>
<script type="text/javascript" src="js/popup.js"></script>
<script type="text/javascript" src="js/modernizr-1.6.min.js?ver=3.4"></script>
<script type="text/javascript" src="js/form.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35525013-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body class="index2">
<!-- Start wrapper --><div id="wrapper">
<!-- Start header --><header class="header">
<!-- Start logo --><div id="logo"><img src="images/logo.png" alt="iRelo" width="173" height="54"></div><!-- end logo -->
<div id="pitch2">
<div class="tagline2">Top Quality Web Design at Affordable Prices </div>

</div>

</header><!-- end header -->
<!-- Start main --><div id="main2">
<div id="main-right2">
<div id="title">Hurry! Get 55% Off Now<span class="black">*</span> </div>
<div style="font-size:14px; padding-left:50px;"><span class="black">(*Limited time offer. Expires October 31st, 2012)</span></div>


<!-- Start button -->
<div id="button2"><a class="modal" href="javascript:void(0);"><img src="images/click-here.png" alt="Click here for a free consultation" width="297" height="79" style="margin-left:150px;"></a></div>
<div style="clear:both;"></div>
<div id="talk">Or Call Us Now To Talk<br />
To a Live Consultant: (801) 623-8187</div></div>


<!-- Start content -->
<div id="content">

<div id="about2">
<div><img src="images/logos.png" alt="As Seen On" width="960" height="85"></div>
<p>iRelo, founded in 1998, is an innovative internet marketing company specializing in lead generation. Known for its successful marketing methodologies and systems used with internet lead generation, we are fanatical about cutting-edge design that can not only drive traffic to your website but that will generate leads and sales for your company. By combining intelligent marketing strategies, eye-popping design and sophisticated web development, we are positive your new website design will deliver tangible results. </p>

</div>
<div style="clear;"><img src="images/line.png" alt="line" width="960" height="1"></div>
<h3 class="subtitle">How iRelo Websites Helped These Businesses Succeed</h3>
<div style="clear:both;"></div>
<!-- Start slideshow --><div id="slideshow">

<div id="slidesContainer">
 <div class="slide">
      <img src="images/slide2.jpg" alt="Top Finance Advisors" width="310" height="227">
	 <div class="info2">
	  <h5>Top Finance Advisors<br />
	  <span class="gray">Chandler, Arizona</span></h5>
	  <div style="clear:both;"></div>
	 <p> Key benefits of hiring iRelo:</p>
	 <ul>
	 <li>Established online presence</li>
	 <li>Generated leads for local finance advisors </li>
	 <li>Increased sales</li>
	
	 </ul>
	 </div>
    </div>
    <div class="slide">
     <img src="images/slide1.jpg" alt="Alliance Worldwide Van Lines" width="310" height="227">
	 <div class="info2">
	  <h5>Alliance Worldwide Van Lines<br />
	  <span class="gray">Woodside, NY</span></h5>
	  <div style="clear:both;"></div>
	 <p> Key benefits of hiring iRelo:</p>
	 <ul>
	 <li>Increased sales</li>
	 <li>Professional online presence</li>
	 <li>Increased web traffic by 100%</li>
	
	 </ul>
	 </div>
	  </div>
	   
   
    <div class="slide">
       <img src="images/slide3.jpg" alt="High Sierra Auto Transport" width="310" height="227">
	 <div class="info2">
	  <h5>High Sierra Auto Transport<br />
	  <span class="gray">Reno, NV</span></h5>
	  <div style="clear:both;"></div>
	 <p> Key benefits of hiring iRelo:</p>
	 <ul>
	 <li>Increased sales by 100%</li>
	 <li>Expanded customer reach</li>
	 <li>Established credibility</li>
	
	 </ul>
	 </div>
    </div>
   
  </div>

</div><!-- end slideshow -->
<!-- Start testimonials --><div id="testimonials2"><h4>Testimonials</h4>
<div style="clear:both;"></div>
<p>"We love, love, love the website!  We really like how you put it all together and we have received many compliments on how professional our site looks compared to our competitors. You did a fabulous job!  Thank you!” </p>
<p style="float:right;"> --Kristine - High Sierra Auto Transport  </p>

</div>
<!-- end testimonials -->
</div><!-- end content -->
<div style="clear:both;"></div>

<footer id="footer">iRelo  •  Lead Generation Specialists  •  © Copyright 2012.  All rights reserved.   •  335 N. Austin Drive, #1, Chandler, AZ 85226  </footer>
</div><!-- end main -->
</div><!-- end wrapper -->
</body>
</html>
