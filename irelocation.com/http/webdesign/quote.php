<!DOCTYPE html>
<head>
<meta charset="UTF-8" />
<title>iRelo | Website Conversion Specialists| Quote Proposal</title>
<link href="style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/modernizr-1.6.min.js?ver=3.4"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35525013-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>

<body>
<iframe id="conv" src=""></iframe>
<div id="form">
    <div class="message">
    <span class="formtitle">Free Quote Proposal</span>
    <div class="clear">&nbsp;</div>
    <p>Please fill out the form below to receive your FREE quote proposal. We will be in contact within 48 hours.</p>
    <p>&nbsp;</p>
    </div>
    
    <div class="form">
        <form name="form1" method="post" action="" onSubmit="return formCheck(this);">
            <label>Full Name:</label>
            <input type="text" name="full_name" id="full_name" value="<?php echo $_GET['full_name'] ?>" />
        
            <div class="clear">&nbsp;</div>
        
            <label>Valid Email:</label>
            <input type="text" name="email" id="email" value="<?php echo $_GET['email'] ?>"/>
        
            <div class="clear">&nbsp;</div>
        
            <label>Valid Phone:</label>
            <div class="phone_inputs">
                <input type="text" name="phone1" id="phone1" maxlength="3" value="<?php echo $_GET['phone1'] ?>" />
                <input type="text" name="phone2" id="phone2" maxlength="3" value="<?php echo $_GET['phone2'] ?>" />
                <input type="text" name="phone3" id="phone3"  maxlength="4" value="<?php echo $_GET['phone3'] ?>" />
            </div>
            
            <div class="clear">&nbsp;</div>
        
            <label>Website:</label>
            <input type="text" name="website" id="website" value="<?php echo $_GET['website'] ?>" />
            
            <div class="clear">&nbsp;</div>
            <label>Additional information:</label>
            <textarea name="comments" id="comments" cols="50" rows="4" style="font-family:Arial,Helvetica,sans-serif; font-size:15px;"><?php if (urldecode($_GET['comments']) !== "") echo $_GET['comments']; ?></textarea>
        
            <div id="submit"><input type="image" src="images/submit.png" alt="Get Free Quote!" style="border:0;" /></div>
        </form>
    </div>
    <div align="right"><a href="javascript:closePopup(300);">[Close]</a></div>
</div>
</body>
</html>
