<?php
	/*
	 SUPER CLASS FOR CRITERION
	*/

	class APIlityCriterion {
		// class attributes
		var $belongsToAdGroupId;
		var $criterionType;
		var $destinationUrl;
		var $id;
		var $language;
		var $isNegative;
		var $status;

		// constructor
		function APIlityCriterion($id, $belongsToAdGroupId, $criterionType, $isNegative, $status, $language, $destinationUrl) {
			$this->id = $id;
			$this->belongsToAdGroupId = $belongsToAdGroupId;
			$this->criterionType = $criterionType;
			$this->status = $status;
			$this->language = $language;
			$this->destinationUrl = $destinationUrl;
			$this->isNegative = convertBool($isNegative);
		}

		// get functions
		function getBelongsToAdGroupId() {
			return $this->belongsToAdGroupId;
		}

		function getCriterionType() {
			return $this->criterionType;
		}

		function getDestinationUrl() {
			return $this->destinationUrl;
		}

		function getId() {
			return $this->id;
		}

		function getLanguage() {
			return $this->language;
		}

		function getIsNegative() {
			return $this->isNegative;
		}

		function getStatus() {
			return $this->status;
		}

		function getCriterionStats($startDate, $endDate, $inPST = false) {
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			if ($inPST) $inPST = "true"; else $inPST = "false";
			$soapParameters = "<getCriterionStats>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<criterionIds>".$this->getId()."</criterionIds>
														<startDay>".$startDate."</startDay>
														<endDay>".$endDate."</endDay>
														<inPST>".$inPST."</inPST>
												 </getCriterionStats>";
			// get criterion stats from the google servers
			$criterionStats = $someSoapClient->call("getCriterionStats", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCriterionStats()", $soapParameters);
		    return false;
			}
			// if we have a keyword criterion add keyword text to the returned stats for the sake of clarity
			if (strcasecmp($this->criterionType, "Keyword") == 0) $criterionStats['getCriterionStatsReturn']['text'] = $this->getText();
			// transform micros to currency units
			$criterionStats['getCriterionStatsReturn']['cost'] = $criterionStats['getCriterionStatsReturn']['cost'] / EXCHANGE_RATE;
			return $criterionStats['getCriterionStatsReturn'];
		}

		// set functions
		function setLanguage($newLanguage) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			if (isset($this->text)) $criterionType = "Keyword"; else $criterionType = "Website";
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<updateCriteria>
														<criteria>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>".$criterionType."</criterionType>
															<negative>".$isNegative."</negative>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<language>".$newLanguage."</language>
														</criteria>
													</updateCriteria>";
			// update the keyword on the google servers
			$someSoapClient->call("updateCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLanguage()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->language = $newLanguage;
			return true;
		}

		function setDestinationUrl($newDestinationUrl) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			if (isset($this->text)) $criterionType = "Keyword"; else $criterionType = "Website";
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<updateCriteria>
														<criteria>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>".$criterionType."</criterionType>
															<negative>".$isNegative."</negative>
															<destinationUrl>".$newDestinationUrl."</destinationUrl>
														</criteria>
													</updateCriteria>";
			// update the keyword on the google servers
			$someSoapClient->call("updateCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDestinationUrl()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->destinationUrl = $newDestinationUrl;
			return true;
		}

		function setIsNegative($newFlag) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			if (isset($this->text)) $criterionType = "Keyword"; else $criterionType = "Website";
			// make sure bool gets transformed into string correctly
			if ($newFlag) $newFlag = "true"; else $newFlag = "false";
			// danger! think in micros
			$soapParameters = "<updateCriteria>
														<criteria>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<negative>".$newFlag."</negative>
															<criterionType>".$criterionType."</criterionType>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
														</criteria>
													</updateCriteria>";
			// update the keyword on the google servers
			$someSoapClient->call("updateCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setIsNegative()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->isNegative = convertBool($newFlag);
			return true;
		}

	}

	/*
		KEYWORD CRITERION
	*/

	class APIlityKeywordCriterion extends APIlityCriterion {
		// keyword class attributes
		var $text;
		var $maxCpc;
		var $type;

		// constructor
		function APIlityKeywordCriterion($text, $id, $belongsToAdGroupId, $type, $criterionType, $isNegative, $maxCpc, $minCpc, $status, $language, $destinationUrl) {
			// we need to construct the superclass first, this is php-specific object-oriented behaviour
			APIlityCriterion::APIlityCriterion($id, $belongsToAdGroupId, $criterionType, $isNegative, $status, $language, $destinationUrl);
			// now construct the keyword criterion which inherits all other criterion attributes
			$this->text = $text;
			$this->maxCpc =  $maxCpc;
			$this->minCpc =  $minCpc;
			$this->type = $type;
		}

		// XML output
		function toXml() {
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			$xml = "<KeywordCriterion>
	<text>".$this->getText()."</text>
	<id>".$this->getId()."</id>
	<belongsToAdGroupId>".$this->getBelongsToAdGroupId()."</belongsToAdGroupId>
	<type>".$this->getType()."</type>
	<criterionType>".$this->getCriterionType()."</criterionType>
	<isNegative>".$isNegative."</isNegative>
	<status>".$this->getStatus()."</status>
	<maxCpc>".$this->getMaxCpc()."</maxCpc>
	<minCpc>".$this->getMinCpc()."</minCpc>
	<language>".$this->getLanguage()."</language>
	<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
</KeywordCriterion>";
			return $xml;
		}

		// get functions
		function getText() {
			return $this->text;
		}

		function getMaxCpc() {
			return $this->maxCpc;
		}

		function getMinCpc() {
			return $this->minCpc;
		}

		function getType() {
			return $this->type;
		}

		function getCriterionData() {
			$criterionData = array(
													'text' => $this->getText(),
													'id' => $this->getId(),
													'belongsToAdGroupId' => $this->getBelongsToAdGroupId(),
													'type' => $this->getType(),
													'criterionType' => $this->getCriterionType(),
													'isNegative' => $this->getIsNegative(),
													'maxCpc' => $this->getMaxCpc(),
													'minCpc' => $this->getMinCpc(),
													'status' => $this->getStatus(),
													'language' => $this->getLanguage(),
													'destinationUrl' => $this->getDestinationUrl()
												);
			return $criterionData;
		}

		function getEstimate() {
			// this function is located in TrafficEstimate.php
			return getKeywordEstimate($this);
		}

		// set functions
		function setText($newText) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			// changing the text is not provided by the api so we need to emulate this by removing and re-creating
			// then re-create the keyword with the new text set
			// make sure bool gets correctly transformed to string
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! we need to think in micros so we need to transform the object maxcpc to micros
			$soapParameters = "<addCriteria>
														<criteria>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>Keyword</criterionType>
															<type>".$this->getType()."</type>
															<text>".$newText."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$this->getMaxCpc()* EXCHANGE_RATE."</maxCpc>
															<language>".$this->getLanguage()."</language>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
														</criteria>
													</addCriteria>";
			// add criterion to the google servers
			$someCriterion = $someSoapClient->call("addCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setText()", $soapParameters);
		    return false;
			}
			// first delete current keyword
			$soapParameters = "<removeCriteria>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<criterionIds>".$this->getId()."</criterionIds>
												 </removeCriteria>";
			// talk to the google servers
			$someSoapClient->call("removeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setText()", $soapParameters);
		    return false;
			}
			// update local object
			$this->text = $newText;
			// changing the text of a keyword will change its id, so update object id data
			$this->id = $someCriterion['addCriteriaReturn']['id'];
			return true;
		}

		function matchMaxCpcToMinCpc() {
			if ($this->getMaxCpc() < $this->getMinCpc()) $this->setMaxCpc($this->getMinCpc());
		}

		function setType($newType) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			// changing the type is not provided by the api so emulate this by deleting and re-creating the keyword
			// then re-create the keyword with the new text set
			// make sure bool gets correctly transformed to string
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! we need to think in micros so we need to transform the object maxcpc to micros
			$soapParameters = "<addCriteria>
														<criteria>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>Keyword</criterionType>
															<type>".$newType."</type>
															<text>".$this->getText()."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$this->getMaxCpc()* EXCHANGE_RATE."</maxCpc>
															<language>".$this->getLanguage()."</language>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
														</criteria>
													</addCriteria>";
			// add criterion to the google servers
			$someCriterion = $someSoapClient->call("addCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setType()", $soapParameters);
		    return false;
			}
			// first delete current keyword
			$soapParameters = "<removeCriteria>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<criterionIds>".$this->getId()."</criterionIds>
												 </removeCriteria>";
			// talk to the google servers
			$someSoapClient->call("removeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setType()", $soapParameters);
		    return false;
			}
			// update local object
			$this->type = $newType;
			// changing the type of a keyword will change its id, so update object id data
			$this->id = $someCriterion['addCriteriaReturn']['id'];
			return true;
		}

		function setMaxCpc($newMaxCpc) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<updateCriteria>
														<criteria>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>Keyword</criterionType>
															<maxCpc>".$newMaxCpc * EXCHANGE_RATE."</maxCpc>
															<negative>".$isNegative."</negative>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
														</criteria>
													</updateCriteria>";
			// update the keyword on the google servers
			$someSoapClient->call("updateCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxCpc()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->maxCpc = $newMaxCpc;
			return true;
		}
	}

	/*
		WEBSITE CRITERION
	*/

	class APIlityWebsiteCriterion extends APIlityCriterion {
		// website class attributes
		var $maxCpm;
		var $url;

		// constructor
		function APIlityWebsiteCriterion($url, $id, $belongsToAdGroupId, $criterionType, $isNegative, $maxCpm, $status, $language, $destinationUrl) {
			// we need to construct the superclass first, this is php-specific object-oriented behaviour
			APIlityCriterion::APIlityCriterion($id, $belongsToAdGroupId, $criterionType, $isNegative, $status, $language, $destinationUrl);
			// now construct the website criterion which inherits all other criterion attributes
			$this->maxCpm = $maxCpm;
			$this->url = $url;
		}

		// XML output
		function toXml() {
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			$xml = "<WebsiteCriterion>
	<url>".$this->getUrl()."</url>
	<id>".$this->getId()."</id>
	<belongsToAdGroupId>".$this->getBelongsToAdGroupId()."</belongsToAdGroupId>
	<criterionType>".$this->getCriterionType()."</criterionType>
	<isNegative>".$isNegative."</isNegative>
	<status>".$this->getStatus()."</status>
	<maxCpm>".$this->getMaxCpm()."</maxCpm>
	<language>".$this->getLanguage()."</language>
	<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
</WebsiteCriterion>";
			return utf8_encode($xml);
		}

		// get functions
		function getMaxCpm() {
			return $this->maxCpm;
		}

		function getUrl() {
			return $this->url;
		}

		function getCriterionData() {
			$criterionData = array(
													'id' => $this->getId(),
													'url' => $this->getUrl(),
													'belongsToAdGroupId' => $this->getBelongsToAdGroupId(),
													'criterionType' => $this->getCriterionType(),
													'isNegative' => $this->getIsNegative(),
													'maxCpm' => $this->getMaxCpm(),
													'status' => $this->getStatus(),
													'language' => $this->getLanguage(),
													'destinationUrl' => $this->getDestinationUrl()
												);
			return $criterionData;
		}

		// set functions
		function setUrl($newUrl) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			// changing the url is not provided by the api so we need to emulate this by removing and re-creating
			// then re-create the website with the new url set
			// make sure bool gets correctly transformed to string
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! we need to think in micros so we need to transform the object maxcpc to micros
			$soapParameters = "<addCriteria>
														<criteria>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>Website</criterionType>
															<negative>".$isNegative."</negative>
															<maxCpm>".$this->getMaxCpm()* EXCHANGE_RATE."</maxCpm>
															<language>".$this->getLanguage()."</language>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<url>".$newUrl."</url>
														</criteria>
													</addCriteria>";
			// add criterion to the google servers
			$someCriterion = $someSoapClient->call("addCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setUrl()", $soapParameters);
		    return false;
			}
			// first delete current website
			$soapParameters = "<removeCriteria>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<criterionIds>".$this->getId()."</criterionIds>
												 </removeCriteria>";
			// talk to the google servers
			$someSoapClient->call("removeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setUrl()", $soapParameters);
		    return false;
			}
			// update local object
			$this->url = $newUrl;
			// changing the text of a keyword will change its id, so update object id data
			$this->id = $someCriterion['addCriteriaReturn']['id'];
			return true;
		}

		function setMaxCpm($newMaxCpm) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			// danger! think in micros
			$soapParameters = "<updateCriteria>
														<criteria>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<criterionType>Website</criterionType>
															<maxCpm>".$newMaxCpm * EXCHANGE_RATE."</maxCpm>
															<negative>".$this->getIsNegative()."</negative>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
														</criteria>
													</updateCriteria>";
			// update the keyword on the google servers
			$someSoapClient->call("updateCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxCpm()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->maxCpm = $newMaxCpm;
			return true;
		}
	}

	/*
  	GENERIC CLASS FUNCTIONS FOR BOTH KEYWORD AND WEBSITE CRITERIONS
	*/

	// add keyword criterion on google servers and create local object
	function addKeywordCriterion() {
		// emulating overloading here. dynamically react differently if exemption request is given or not
		// expected argument sequence: $text, $belongsToAdGroupId, $type, $isNegative, $maxCpc, $language, $destinationUrl, ($exemptionRequest)

		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		// populate variables with function arguments
		$text = func_get_arg(0);
		$belongsToAdGroupId = func_get_arg(1);
		$type = func_get_arg(2);
		$isNegative = func_get_arg(3);
		$maxCpc = func_get_arg(4);
		// thinking in micros here
		$maxCpc = $maxCpc * EXCHANGE_RATE;
		$language = func_get_arg(5);
		$destinationUrl = func_get_arg(6);
		// make sure bool gets transformed to string correctly
		if ($isNegative) $isNegative = "true"; else $isNegative = "false";
		// if no exemption request
		if (func_num_args() == 7) {
			$soapParameters = "<addCriteria>
														<criteria>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<criterionType>Keyword</criterionType>
															<type>".$type."</type>
															<text>".$text."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$maxCpc."</maxCpc>
															<language>".$language."</language>
															<destinationUrl>".$destinationUrl."</destinationUrl>
														</criteria>
													</addCriteria>";
		}
		// if with exemption request
		else if (func_num_args() == 8){
			$exemptionRequest = func_get_arg(7);
			$soapParameters = "<addCriteria>
														<criteria>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<exemptionRequest>".$exemptionRequest."</exemptionRequest>
															<criterionType>Keyword</criterionType>
															<type>".$type."</type>
															<text>".$text."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$maxCpc."</maxCpc>
															<language>".$language."</language>
															<destinationUrl>".$destinationUrl."</destinationUrl>
														</criteria>
													</addCriteria>";
		}
		// add criterion to the google servers
		$someCriterion = $someSoapClient->call("addCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addKeywordCriterion()", $soapParameters);
	    return false;
		}
		// create local object
		// danger! think in currency units
		$criterionObject = new APIlityKeywordCriterion ($someCriterion['addCriteriaReturn']['text'], $someCriterion['addCriteriaReturn']['id'], $someCriterion['addCriteriaReturn']['adGroupId'], $someCriterion['addCriteriaReturn']['type'], $someCriterion['addCriteriaReturn']['criterionType'], $someCriterion['addCriteriaReturn']['negative'], $someCriterion['addCriteriaReturn']['maxCpc'] / EXCHANGE_RATE, $someCriterion['addCriteriaReturn']['minCpc'] / EXCHANGE_RATE, $someCriterion['addCriteriaReturn']['status'], $someCriterion['addCriteriaReturn']['language'], $someCriterion['addCriteriaReturn']['destinationUrl']);
		return $criterionObject;
	}

	function addKeywordCriterionList($criteria) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();

		$soapParameters = "<addCriteria>";

		foreach ($criteria as $criterion) {
			// make sure integer is transformed to string correctly
			if ($criterion['isNegative']) $criterion['isNegative'] = "true"; else $criterion['isNegative'] = "false";
			// think in micros
			$criterion['maxCpc'] *= EXCHANGE_RATE;
			if (isset($criterion['exemptionRequest'])) {
				// with exemption request
				$soapParameters .= "<criteria>
															<exemptionRequest>".$criterion['exemptionRequest']."</exemptionRequest>
															<adGroupId>".$criterion['belongsToAdGroupId']."</adGroupId>
															<type>".$criterion['type']."</type>
															<criterionType>Keyword</criterionType>
															<text>".$criterion['text']."</text>
															<negative>".$criterion['isNegative']."</negative>
															<maxCpc>".$criterion['maxCpc']."</maxCpc>
															<language>".$criterion['language']."</language>
															<destinationUrl>".$criterion['destinationUrl']."</destinationUrl>
														</criteria>";
			}
			else {
				// without exemption request
				$soapParameters .= "<criteria>
															<type>".$criterion['type']."</type>
															<adGroupId>".$criterion['belongsToAdGroupId']."</adGroupId>
															<criterionType>Keyword</criterionType>
															<text>".$criterion['text']."</text>
															<negative>".$criterion['isNegative']."</negative>
															<maxCpc>".$criterion['maxCpc']."</maxCpc>
															<language>".$criterion['language']."</language>
															<destinationUrl>".$criterion['destinationUrl']."</destinationUrl>
														</criteria>";
			}
		}
		$soapParameters .= "</addCriteria>";
		// add criteria to the google servers
		$someCriteria = $someSoapClient->call("addCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addCriteriaList()", $soapParameters);
	    return false;
		}

		// when we have only one keyword return a (one keyword element) array  anyway
		if (isset($someCriteria['addCriteriaReturn']['id'])) {
			$saveArray = $someCriteria['addCriteriaReturn'];
			unset($someCriteria);
			$someCriteria['addCriteriaReturn'][0] = $saveArray;
		}

		// create local objects
		$criterionObjects = array();
		foreach($someCriteria['addCriteriaReturn'] as $someCriterion) {
			$criterionObject = new APIlityKeywordCriterion ($someCriterion['text'], $someCriterion['id'], $someCriterion['adGroupId'], $someCriterion['type'], $someCriterion['criterionType'], $someCriterion['negative'], $someCriterion['maxCpc'] / EXCHANGE_RATE, $someCriterion['minCpc'] / EXCHANGE_RATE, $someCriterion['status'], $someCriterion['language'], $someCriterion['destinationUrl']);
			array_push($criterionObjects, $criterionObject);
		}
		return $criterionObjects;
	}

	// this won't fail completely if only one criterion fails
	// but causes a lot soap overhead
	function addKeywordCriteriaOneByOne($criteria) {
		// this is basically just a wrapper to the addKeywordCriterion function
		$criterionObjects = array();
		foreach ($criteria as $criterion) {
			if (isset($criterion['exemptionRequest'])) {
				// with exemption request
				$criterionObject = addKeywordCriterion($criterion['text'], $criterion['belongsToAdGroupId'], $criterion['type'], $criterion['isNegative'], $criterion['maxCpc'], $criterion['language'], $criterion['destinationUrl'], $criterion['exemptionRequest']);
			}
			else {
				// without exemption request
				$criterionObject = addKeywordCriterion($criterion['text'], $criterion['belongsToAdGroupId'], $criterion['type'], $criterion['isNegative'], $criterion['maxCpc'], $criterion['language'], $criterion['destinationUrl']);
			}
			array_push($criterionObjects, $criterionObject);
		}
		return $criterionObjects;
	}

	function getAllCriteria($adGroupId) {
	 	global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
	 	$soapParameters = "<getAllCriteria>
	 												<adGroupId>".$adGroupId."</adGroupId>
	 										 </getAllCriteria>";
	 	// query the google servers for all criteria
	 	$allCriteria = array();
	 	$allCriteria = $someSoapClient->call("getAllCriteria", $soapParameters);
	 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCriteria()", $soapParameters);
	    return false;
		}

		// when we have only one criterion in the adgroup return a (one criterion element) array  anyway
		if (isset($allCriteria['getAllCriteriaReturn']['id'])) {
			$saveArray = $allCriteria['getAllCriteriaReturn'];
			unset($allCriteria);
			$allCriteria['getAllCriteriaReturn'][0] = $saveArray;
		}
		// when there are several criteria in the adgroup just return them normally	(i.e. array of criteria)
		$allCriterionObjects = array();
		if (isset($allCriteria['getAllCriteriaReturn'])) foreach ($allCriteria['getAllCriteriaReturn'] as $criterion) {
			// create generic object attributes
			$belongsToAdGroupId = $criterion['adGroupId'];
			$criterionType = $criterion['criterionType'];
			$destinationUrl = $criterion['destinationUrl'];
			$id = $criterion['id'];
			$language = $criterion['language'];
			$isNegative = $criterion['negative'];
			$status = $criterion['status'];

			// if we have a keyword criterion create its object attributes
			if (strcasecmp($criterion['criterionType'], "Keyword") == 0) {
				$maxCpc = $criterion['maxCpc'];
				$minCpc = $criterion['minCpc'];
	      $type = $criterion['type'];
	      $text = $criterion['text'];
	      $criterionObject = new APIlityKeywordCriterion($text, $id, $belongsToAdGroupId, $type, $criterionType, $isNegative, $maxCpc / EXCHANGE_RATE, $minCpc / EXCHANGE_RATE, $status, $language, $destinationUrl);
	    }
	    // else create the website criterion's object attributes
	    else {
      	$maxCpm = $criterion['maxCpm'];
				$url = $criterion['url'];
				$criterionObject = new APIlityWebsiteCriterion($url, $id, $belongsToAdGroupId, $criterionType, $isNegative, $maxCpm / EXCHANGE_RATE, $status, $language, $destinationUrl);
      }
			array_push($allCriterionObjects, $criterionObject);
		}

		return $allCriterionObjects;
	}

	// remove criterion on google servers and delete local object
	function removeCriterion(&$criterionObject) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		$soapParameters = "<removeCriteria>
													<adGroupId>".$criterionObject->getBelongsToAdGroupId()."</adGroupId>
													<criterionIds>".$criterionObject->getId()."</criterionIds>
											 </removeCriteria>";
		// talk to the google servers
		$someSoapClient->call("removeCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":removeCriterion()", $soapParameters);
	    return false;
		}
		// delete remote calling object
		$criterionObject = @$GLOBALS['criterionObject'];
		unset($criterionObject);
		return true;
	}

	function createCriterionObject($givenAdGroupId, $givenCriterionId) {
		// this will create a local criterion object that we can play with
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		// prepare soap parameters
		$soapParameters = "<getCriteria>
													<adGroupId>".$givenAdGroupId."</adGroupId>
													<criterionIds>".$givenCriterionId."</criterionIds>
											 </getCriteria>";
		// execute soap call
		$someCriterion = $someSoapClient->call("getCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createCriterionObject()", $soapParameters);
	    return false;
		}
		// invalid ids are silently ignored. this is not what we want so put out a warning and return without doing anything.
		if (sizeOf($someCriterion) == 0) {
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning: </b>Invalid Criterion ID or AdGroup ID. No Criterion found.";
			return false;
		}
		// create generic object attributes
		$belongsToAdGroupId = $someCriterion['getCriteriaReturn']['adGroupId'];
		$criterionType = $someCriterion['getCriteriaReturn']['criterionType'];
		$destinationUrl = $someCriterion['getCriteriaReturn']['destinationUrl'];
		$id = $someCriterion['getCriteriaReturn']['id'];
		$language = $someCriterion['getCriteriaReturn']['language'];
		$isNegative = $someCriterion['getCriteriaReturn']['negative'];
		$status = $someCriterion['getCriteriaReturn']['status'];

		// if we have a keyword criterion create its object attributes
		if (strcasecmp($someCriterion['getCriteriaReturn']['criterionType'], "Keyword") == 0) {
			$maxCpc = $someCriterion['getCriteriaReturn']['maxCpc'];
			$minCpc = $someCriterion['getCriteriaReturn']['minCpc'];
      $type = $someCriterion['getCriteriaReturn']['type'];
      $text = $someCriterion['getCriteriaReturn']['text'];
      $criterionObject = new APIlityKeywordCriterion($text, $id, $belongsToAdGroupId, $type, $criterionType, $isNegative, $maxCpc / EXCHANGE_RATE, $minCpc / EXCHANGE_RATE, $status, $language, $destinationUrl);
    }
    // else create the website criterion's object attributes
    else {
    	$maxCpm = $someCriterion['getCriteriaReturn']['maxCpm'];
			$url = $someCriterion['getCriteriaReturn']['url'];
			$criterionObject = new APIlityWebsiteCriterion($url, $id, $belongsToAdGroupId, $criterionType, $isNegative, $maxCpm / EXCHANGE_RATE, $status, $language, $destinationUrl);
    }
    return $criterionObject;
	}

	// add keyword criterion on google servers and create local object
	function addWebsiteCriterion($url, $belongsToAdGroupId, $isNegative, $maxCpm, $destinationUrl) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();

		// thinking in micros here
		$maxCpm = $maxCpm * EXCHANGE_RATE;
		// make sure bool gets transformed to string correctly
		if ($isNegative) $isNegative = "true"; else $isNegative = "false";

		$soapParameters = "<addCriteria>
													<criteria>
														<adGroupId>".$belongsToAdGroupId."</adGroupId>
														<url>".$url."</url>
														<criterionType>Website</criterionType>
														<negative>".$isNegative."</negative>
														<maxCpm>".$maxCpm."</maxCpm>
														<destinationUrl>".$destinationUrl."</destinationUrl>
													</criteria>
												</addCriteria>";
		// add criterion to the google servers
		$someCriterion = $someSoapClient->call("addCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addWebsiteCriterion()", $soapParameters);
	    return false;
		}
		// create local object
		// danger! think in currency units
		$criterionObject = new APIlityWebsiteCriterion ($someCriterion['addCriteriaReturn']['url'], $someCriterion['addCriteriaReturn']['id'], $someCriterion['addCriteriaReturn']['adGroupId'], $someCriterion['addCriteriaReturn']['criterionType'], $someCriterion['addCriteriaReturn']['negative'], $someCriterion['addCriteriaReturn']['maxCpm'] / EXCHANGE_RATE, $someCriterion['addCriteriaReturn']['status'], $someCriterion['addCriteriaReturn']['language'], $someCriterion['addCriteriaReturn']['destinationUrl'] );
		return $criterionObject;
	}

	function addWebsiteCriteriaOneByOne($criteria) {
		// this is basically just a wrapper to the addWebsiteCriterion function
		$criterionObjects = array();
		foreach ($criteria as $criterion) {
			$criterionObject = addWebsiteCriterion($criterion['url'], $criterion['belongsToAdGroupId'], $criterion['isNegative'], $criterion['maxCpm'], $criterion['destinationUrl']);
			array_push($criterionObjects, $criterionObject);
		}
		return $criterionObjects;
	}

	function addWebsiteCriterionList($criteria) {
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		$criterionObjects = array();
		// prepare soap parameters
		$soapParameters = "<addCriteria>";
		foreach ($criteria as $criterion) {
			// update the google servers

			// thinking in micros here
			$criterion['maxCpm'] = $criterion['maxCpm'] * EXCHANGE_RATE;
			// make sure bool gets transformed to string correctly
			if ($criterion['isNegative']) $criterion['isNegative'] = "true"; else $criterion['isNegative'] = "false";
			$soapParameters .= "<criteria>
														<adGroupId>".$criterion['belongsToAdGroupId']."</adGroupId>
														<url>".$criterion['url']."</url>
														<criterionType>Website</criterionType>
														<negative>".$criterion['isNegative']."</negative>
														<maxCpm>".$criterion['maxCpm']."</maxCpm>
														<destinationUrl>".$criterion['destinationUrl']."</destinationUrl>
													</criteria>";

		}
		$soapParameters .= "</addCriteria>";
		// add criteria to the google servers
		$someCriteria = $someSoapClient->call("addCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addWebsiteCriterionList()", $soapParameters);
	    return false;
		}
		// create local objects
		// danger! think in currency units
		// when we have only one criterion return a (one criterion element) array  anyway
		if (isset($someCriteria['addCriteriaReturn']['id'])) {
			$saveArray = $someCriteria['addCriteriaReturn'];
			unset($someCriteria);
			$someCriteria['addCriteriaReturn'][0] = $saveArray;
		}

		// create local objects
		$criterionObjects = array();
		foreach($someCriteria['addCriteriaReturn'] as $someCriterion) {
			$criterionObject = new APIlityWebsiteCriterion ($someCriterion['url'], $someCriterion['id'], $someCriterion['adGroupId'], $someCriterion['criterionType'], $someCriterion['negative'], $someCriterion['maxCpm'] / EXCHANGE_RATE, $someCriterion['status'], $someCriterion['language'], $someCriterion['destinationUrl']);
			array_push($criterionObjects, $criterionObject);
		}
		return $criterionObjects;
	}
?>
