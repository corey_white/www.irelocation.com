<?
	define(DEBUG,false);

	function test()
	{
		phpinfo();
	}

	function proxy($args)
	{
		//print_r($args);
		$url = $args[0];
		$function = $args[1];
		$hiturl = $url."?function=".$args[1];
		array_shift($args);
		array_shift($args);
		if (count($args) > 1)
			$hiturl .= "&arg[]=".implode("&arg[]=",$args);
		else if (count($args) == 1)
			$hiturl .= "&arg[]=".$args;
		//echo "<br/>";
		//echo $hiturl;
		$ch = curl_init($hiturl);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		$response = curl_exec($ch);
		return $response;
	}

	if (strlen($_REQUEST['function']) > 0)
	{
		//get the function name
		$call = $_REQUEST['function'];
		//and arguments
		$arg = $_REQUEST['arg'];
		if ($call == "proxy")
			$arg[0] = $arg;
		
		//get the list of user defined functions
		$f = get_defined_functions();
		//filter it
		$f = $f['user'];
		//flatten it
		$list = "(-".implode("-.-",$f)."-)";
		//make sure the call is one of those, so hackers `can't` get in.
		if  (substr_count($list,"-".strtolower($call)."-") == 1)
		{
			//call it,
			$result = call_user_func_array($call,$arg);		
			//debug mail results
			if (DEBUG) mail("david@irelocation.com","CHATFORM",$call." ".print_r($_REQUEST['arg'],true));
			if ($call != "proxy")
				echo str_replace("'","\'",$result);
			else
				//return it.
				echo $result;//already escaped.
		
	}
?>