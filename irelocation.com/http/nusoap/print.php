<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	include_once "Base.php";
 
 	function printHeader($names)
 	{
		//$names = $report->names;
		$header = "<tr>";
		
		if(count($names) > 2)
			$header .= "<td>&nbsp;</td>";	
		
		for ($i = 0; $i < count($names); $i++)
			$header .= "<td>".$names[$i]."</td>";	
		$header .= "</tr>";
		return $header;
 	}
 
 	function printRow($names,$myarray,$i=DEFAULT_PARAMETER)
 	{
 		$row = "<tr>";
 		
 		if($i != DEFAULT_PARAMETER)
			$header .= "<td>$i</td>";	
 		
		for ($i = 0; $i < count($names); $i++)
			$row .= "<td>".$myarray[$names[$i]]."</td>";	
		$row .= "</tr>";
		return $row;
 	} 
 	
 	function printFooter($columns,$rows)
 	{
 		if ($columns > 2) $columns++;
 		$row = "<tr><td colspan='$columns' align='right'>"; 	 				
		$row .= "$rows row(s) returned.</td></tr>";
		return $row;
 	}
 	
 	
 	function printHTML($report)
 	{
 		$results = $report->getSQL();
 		if (count($results) > 0)
 		{
 			$sql = $results[0];
 			$names = $results[1];
 			echo "<table border='1'>";
			echo printHeader($names);
 			$rs = new mysql_recordset($sql);
 			if ($report->format != LEAD_DETAILS)
 				$rows = 0;
 			else
 				$rows = "*";
 			while($rs->fetch_array())
 			{
 				echo printRow($names,$rs->myarray,$rows);
 				if ($report->format != LEAD_DETAILS)
 					$rows++; 					
 			}
 			if ($report->format != LEAD_DETAILS)
 				echo printFooter(count($names),$rows);
 			echo "</table>";
 		}
 	}
?>
