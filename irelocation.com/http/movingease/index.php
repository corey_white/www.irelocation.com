<?include 'header.html';?>
<span class="text">
<h3>Ease of Service:</h3>
Storage units are brought to you where you fill them as you like.
Your storage units will then be stored in a secure facility until they are delivered when you want,
where you want to your chosen U.S. destination. You unlock and unpack at your leisure.<br><br>
<h3>Quality Service:</h3>
In doing a self-move you are assured that your valuables are packed the way you want them packed
to avoid damage.
You are in charge of the way the units are packed so that your valuables don't shuffle around in transit.
You take your time to pack and load and make sure that things are done right.
This is what we call <a href="">Moving Ease</a>!
<h3>Why Do Self-Move?</h3>
Self-move units are convenient, easy to pack (they're close to the ground) and inexpensive.
You load and lock at your convenience - no trips to the storage yard.  If you are moving, when
your unit(s) are filled, they are picked up and delivered where and when you want.
</span>
<?include 'footer.html';?>
