<?php
function curl_download($Url){
 
    // is cURL installed yet?
    if (!function_exists('curl_init')){
        die('Sorry cURL is not installed!');
    }
 
    // OK cool - then let's create a new cURL resource handle
    $ch = curl_init();
 
    // Now set some options (most are optional)
 
    // Set URL to download
    curl_setopt($ch, CURLOPT_URL, $Url);
 
    // Set a referer
    curl_setopt($ch, CURLOPT_REFERER, "http://www.example.org/yay.htm");
 
    // User agent
    curl_setopt($ch, CURLOPT_USERAGENT, "MozillaXYZ/1.0");
 
    // Include header in result? (0 = yes, 1 = no)
    curl_setopt($ch, CURLOPT_HEADER, 0);
 
    // Should cURL return or print out the data? (true = return, false = print)
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 
    // Timeout in seconds
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
 
    // Download the given URL, and return output
    $output = curl_exec($ch);
 
    // Close the cURL resource, and free system resources
    curl_close($ch);
 
    return $output;
}

//$test = curl_download('http://localhost/soap/index.php?wsdl');
$newts = date("Y-m-d H:i:s",time() - 7200);

print_r($newts);

/*
?>

<Script>
	var date = new Date();
	var m = date.getMonth()+1;
	if (m < 10) m = "0"+m;	
	var d = date.getDay();
	if (d < 10)	d = "0"+d;
	var y = date.getYear() + 1900;
	window.location = "http://irelocation.com/moving/percentages.php?month="+y+m+d;
</Script>
*/
?>
