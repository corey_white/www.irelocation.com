$(function() {
    var input = $('input[type=text]');

	input.focus(function() {
    	if($(this).val()==$(this).attr('title')){
    		$(this).val('');
    	}
    }).blur(function() {
         var el = $(this);
         if(el.val() == ''){
         	el.val(el.attr('title'));
         }    
    });
});

function formCheck(form)
{	
	var valid = true;
	
	$('input[type="text"]').each(function(index)
	{
		$(this).removeClass('error');
	    if($(this).hasClass('req'))
	    {
		    if($(this).val()==$(this).attr('title'))
		    {
	    		valid=false;
	    		$(this).addClass('error');
	    	}
	    }
	});
	
	if(valid==false)
	{
		alert('Please complete the highlighted fields.');
    	return false;
	}
	else
	{
		var name = form.full_name.value;
		var email = form.email.value;
		var phone1 = form.phone.value.substring(0,3); /* get first 3 characters from the field */
		var phone2 = form.phone.value.substring(3,6); /* get between 3 - 6 characters from the field */
		var phone3 = form.phone.value.substring(6,10); /* get last 4 characters from the field */
		var ext = form.ext.value;
		var company_name = form.company_name.value;
		var website = form.website.value;
		
		if(name.length<3 || name == undefined){
			valid=false;
			$('#full_name').addClass('error');
		}
		else
		{
			$('#full_name').removeClass('error');
		}
		
		if(email != ''){
	        var regex = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	        if(regex.test(email) == false) {
	            valid=false;
	            $("#email").addClass("error");
	            
	        }
	        else{
	            $("#email").removeClass("error")
	        }
	    }
	    else{
	        if(email.length<3){
	            valid=false;
	        }
	    }
	    
	    var phone1_1 = parseInt(phone1.substr(0, 1));
	    if(phone1.length != 3 || phone1 < 0 || phone1_1 < 2) {
	        valid=false;
	        $('#phone').addClass('error');
	    }
	    else{
	        $("#phone").removeClass("error");
	    } 
	    
	    var phone2_1 = parseInt(phone2.substr(0, 1));
	    if(phone2.length != 3 || phone2 < 1 || phone2_1 < 2) {
	        valid=false;
	        $('#phone').addClass('error');
	    }
	    else{
	        $("#phone").removeClass("error");
	    }
	    
	    if(phone3.length != 4 || phone3 < 1) {
	        valid = false;
	        $('#phone').addClass('error');
	    } 
	    else{
	        $("#phone").removeClass("error");
	    }
	    
	    
	    if(valid==true){
			var data = $(form).serialize();
			
			$.ajax({
			  type: 'POST',
			  url: 'email.php',
			  data: data
			}).done(function(){
				$('#conv').attr('src','conv.php');
				$('.form_back').empty().html('<h2>Thanks! We\'ve received your submission and will be in contact with you shortly.</h2>');
			});	
		}
		else
		{
			alert('Please complete the required fields.');
		}
	}
	return false;
}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}