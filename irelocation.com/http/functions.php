<?php

if ($_REQUEST['email']) {
	echo checkEmailValid($_REQUEST['email']);
}



function checkEmailValid($email) {
    //-- Checks to see if there is an MX record for the domain and if we can make a socket connection to to SMTP port

    list($userName, $mailDomain) = split("@", $email);  //-- split email address into the username and domain.
    
    if (!checkdnsrr($mailDomain, "MX")) { //-- this checks to see if there is an MX DNS record for the domain
        $valid = "false";
    } else {
		$valid = "true";
	}
      
    return $valid;
}


?>