<?php
/* 
************************FILE INFORMATION**********************
Created by Mark Anderson 8-28-11

This code sends out an email to auto customers with the phone numbers of those who be quoting their lead

updated by MAA on 9-1-11
Fixed by Rob on 11-29-2011

Rob Note: According to the code, no emails should be going out unless they have more than 7 companies assigned to the lead.  
**********************************************************************
*/
include_once('class.phpmailer.php');
include_once("../inc_mysql.php");


//$to_email = "stoneaglewolf@gmail.com";
//$LeadUID = "11464881"; //-- this lead id should 8 clients

require( "xmlLib.php" );


/* This is Marks time code that we don't need.
$ts = time() - 3600;
		//subtract 1 hour.
$time = date("YmdHis",$ts);
$ts2 = time() - 300;
		//subtract 5min.
$time2 = date("YmdHis",$ts2);
*/

//-- new format: 2011-11-29 16:18:02
//-- new format: Y-m-d H:i:s
$newts = date("Y-m-d H:i:s",time() - 3600);


//select leads from database
#$sql = "select * from irelocation.lemailer where sent = 'no' and date > '$time' and date < '$time2' "; This is the old sql from Mark.  It doesn't work because we have different servers posting different times.  Some of these times are hours off.  Decided to go with server time on the server where the Db is located, so that there is a consistent time.

$sql = "select * from irelocation.lemailer where sent = 'no' and dbtime > '$newts' ";
echo $sql;	
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array()) {

			
			$site = $rs->myarray["site"];
			$to_email = $rs->myarray["email"];
			$date = $rs->myarray["date"];
			$LeadUID = $rs->myarray["leid"];
		
			echo $LeadUID."<br />";


				#error_reporting(E_ERROR | E_WARNING | E_PARSE); //-- show runtime errors
				error_reporting(0); //-- show no errors

				//-- this part pulls the clients for a specific lead.

				//-- get XML data from Clickpoint
				$xml = new XMLToArray( "https://api.leadexec.net/service.asmx/GetClientsForSend?Key=69EE07F8CF8C98117418A913187E017E7A208AB99D529C77&LeadUID=$LeadUID", array(), array( "ClientSendInfo" => "_array_" ), true, false );

				$myarray = $xml->getArray();
				/*
				echo "<pre>";
				print_r($myarray);
				echo "</pre><br />";

				*/

				$CountArr = count($myarray[ArrayOfClientSendInfo][ClientSendInfo]);
				echo "CountArr = $CountArr<br /><br />";

				if ($CountArr > 7) {
					

					//-- Assign Companies ID into array
					$company = array();
					for ( $i = 0; $i < $CountArr; $i++ ) {
						$company[$i] = $myarray[ArrayOfClientSendInfo][ClientSendInfo][$i][ClientUID];
					}


					/*
					//-- used for debugging
					for ( $i = 0; $i < $CountArr; $i++ ) {
						echo "Company $i = $company[$i]<br />";
					}
					echo "<pre>";
					print_r( $xml->getArray() );
					echo "</pre><br />";
					*/


					//-- This part pulls the company name, email and phone from clickpoint based on client ID
					$companyName = array();
					$companyPhone = array();
					$companyEmail = array();

					foreach($company as $key => $value){
						$xml = new XMLToArray( "https://api.leadexec.net/service.asmx/GetClientData?Key=69EE07F8CF8C98117418A913187E017E7A208AB99D529C77&ClientUID=$value", array(), array( "ClientFieldType" => "_array_" ), true, false );
						$myarray = $xml->getArray();

						$companyName[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][0][FieldValue];
						$companyPhone[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][6][FieldValue]; //-- this is the Alternate Phone field
						//$companyEmail[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][3][FieldValue];
					}

					$content = "";
					$content .= "<table>";

					
						for ( $i = 0; $i < $CountArr; $i++ ) {
							
								$companyN = eregi_replace("TOPAUTO ", "", $companyName[$i]);
								$companyN = eregi_replace("ATUS ", "", $companyN);
								$companyN = eregi_replace("AUTOGAIN ", "", $companyN);
								
								$content .= "<tr><td><font size='2'><b>Company: $companyN </b><br />";
								$content .= "<b>Phone:</b> $companyPhone[$i]</font><br /><br /></td></tr> ";
								//$content .= "<b>Email: </b>$companyEmail[$i] <br /><br /></td></tr>";
						}
					
					$content .= "</table><br /><br />";
					
					echo $content;

				} else {
					echo "Not 8 companies to send to yet.";
					$content = "";
				}




				/*
				//-- used for debugging
				echo "<pre>";
				print_r( $xml->getArray() );
				echo "</pre><br />";
				*/

				//build email

				
				
				
	//$emails_to_send .= "stoneaglewolf@gmail.com \n";

	//$quote_id = $rs->myarray["quote_id"];
	

	//echo "$quote_id - ";
	if ($content != "") { //content is populated only if there are 8 or more companies.

		$mail    = new PHPMailer();

		if ($site == "carshipping.com") {
			$body    = $mail->getFile('carshipping.html');
		} else if ($site == "autoshipping.com") {
			$body    = $mail->getFile('autoshipping.html');
		} else if ($site == "movemycar.com") {
			$body    = $mail->getFile('movemycar.html');
		} else if ($site == "carshippingquote.com") {
			$body    = $mail->getFile('carshippingquote.html');
		} else if ($site == "autogain.net") {
			$body    = $mail->getFile('autogain.html');
		}
		
		$body    = eregi_replace("[\]","",$body);
		$body    = eregi_replace("##content##","$content",$body);
		
		if ($site == "carshipping.com") {
			$mail->From     = "no_reply@carshipping.com";
			$mail->FromName = "CarShipping.com";
		} else if ($site == "autoshipping.com") {
			$mail->From     = "no_reply@autoshipping.com";
			$mail->FromName = "AutoShipping.com";
		} else if ($site == "movemycar.com") {
			$mail->From     = "no_reply@movemycar.com";
			$mail->FromName = "MoveMyCar.com";
		} else if ($site == "carshippingquote.com") {
			$mail->From     = "no_reply@carshippingquote.com";
			$mail->FromName = "CarShippingQuote.com";
		} else if ($site == "autogain.net") {
			$mail->From     = "no_reply@autogain.net";
			$mail->FromName = "AutoGain.net";
		}
		
		$mail->Subject = "Thank you for your information";
		
		$mail->AltBody = "The following companies will be contacting you about shipping your vehicle.".$content; // Alt Text
		
		$mail->MsgHTML($body);
		
		$mail->AddAddress("$to_email");

		//$mail->AddBCC("stoneeaglewolf@yahoo.com");
		
		if(!$mail->Send()) {
		  echo "Failed to send mail <br />";
		} else {
		  echo "Mail sent <br />";

			$sql2 = "update irelocation.lemailer set sent = 'yes' where leid = '$LeadUID' ";

			echo $sql2;
		
			$rs2 = new mysql_recordset($sql2);
		}
	}

}

	// end email build
		
?>
