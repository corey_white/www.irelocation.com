<?
/*
<?xml version="1.0" encoding="utf-8"?>
<!-- This information and system is restricted. Copyright(c)2006 A AAAdvantage Auto Transport. -->
<quote>
  <rate>1075</rate>
</quote>
*/
define(URL,"http://www.automoves.com/xml_get_quote.php?auth=0800200c9a66");
define(VARIABLE,.2);

function getXMLAutoquote()
{
	$o = getObject();
	$fc = $o->myarray['origin_city'];
	$fs = $o->myarray['origin_state'];
	$tc = $o->myarray['destination_city'];
	$ts = $o->myarray['destination_state'];
	$make = $o->myarray['make'];
	$model = $o->myarray['model'];
	$url = buildURL($fc,$fs,$tc,$ts,$make,$model);
	$ch = curl_init($url);
	//echo $url;
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$response = curl_exec($ch);
	//echo "<textarea>".$response."</texarea>";
	$splitat = strpos($response,"<rate>");
	$endat = strpos($response,"</rate>") - $splitat;

	$price = ereg_replace("[^0-9\.]","",substr($response,$splitat,$endat));

	return $price;
}

function formatPriceResponse($price)
{
	if ($price == 1) return "";
	else
	{
		$low = (1-VARIABLE)*$price;
		$high = (1+VARIABLE)*price;

		return "<span class='username'>".getName()."</span>: Our system shows that shipping ".
				"your vehicle will cost from \$".number_format($low,2,".",",")." to \$".
				 number_format($low,2,".",",").". More Specific Quotes will follow from ".		
				"6 top auto shippers.";
	}
}

function buildURL($fc,$fs,$tc,$ts,$make,$model)
{
	return URL."&fc=$fc&fs=$fs&tc=$tc&ts=$ts&make=$make&model=$model";
}
?>