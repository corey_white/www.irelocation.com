	var current_user = "";
	var current_question = 'fname';
	var linecount = 0;
	var current_conversation = new Array();
	
	function init()
	{
		pausecomp(750+(Math.random()*1750));//give a little delay.
		call('process.php','init','form_callback');	
		pausecomp(Math.random()*2000+750);
		getQuestion();	
	}

	function getQuestion()
	{		
		call('process.php','getNR','form_nr_callback',current_question);		
	}

	function getUsersName()
	{
		call('process.php','getClientName','name_callback');
	}

	function name_callback(result)
	{
		current_user = result;
	}

	function form_nr_callback(result)
	{
		updateMessageTime();
		if (result.length > 0)
		{
			pausecomp(Math.random(500));
			append(result); 
		}
		if (DEBUG) debug('debug_area','form_nr_callback('+result+');');
		pausecomp(Math.random(750)+500);
		call('process.php','getQuestion','form_callback',current_question);
		if ((current_question == '|CAR|' && current_user == '') || current_question == 'lname')//after name
			getUsersName();
	}

	function pausecomp(millis)
	{
		if (DEBUG) debug('debug_area','pausecomp('+millis+');');
		var date = new Date();
		var curDate = null;
		
		do { curDate = new Date(); }
		while(curDate-date < millis);
	} 

	function updateMessageTime()
	{
		var time = getCurrentTime();
		var output = document.getElementById('time');
		output.innerHTML = "Last message received on: " + time;
	}

	function done()
	{
		if (DEBUG) debug('debug_area','done();');
		call('process.php','validateAndFinish','form_callback');
	}

	function nextquestion(q)
	{
		if (DEBUG) debug('debug_area','nextquestion('+q+');');
		if (q != 'done' && isNaN(q))
		{
			current_question = q;
			getQuestion();
		}
		else if (q == "done")
		{
			done();
		}
		else if (q == "-1")//bad response.
		{
			call('process.php','askAgain','form_callback',current_question);		
		}
		else if (q == "-2")//bad question keyword.. ???
		{
			alert("Internal Error, Please try again later. \n\t Sorry :( ");
		}
	}

	function user_response_callback(response)
	{
		if (DEBUG) debug('debug_area','user_response_callback('+response+');');
		append(response);
	}

	function form_callback(response)
	{
		updateMessageTime();
		r = response;
		pausecomp(Math.random()*1000);
		if (DEBUG) debug('debug_area','form_callback('+r+');');
		append(response);
		//alert("First Char of Response: " + current_question.charAt(0));
		if (current_question.charAt(0) == '|')//area header...
		{
			pausecomp(Math.random()*750+500);
			call('process.php','processResponse','nextquestion',current_question,current_question);
		}
	}

	function append(text)
	{
		if (text.length == 0) return;
		if (DEBUG) debug('debug_area','append('+text+');');
		linecount++;
		current_conversation.push(text);
		var output = document.getElementById('output');		

		if (linecount > 10)
		{
			var data =  "";			
			for (var i = linecount - 8; (i < linecount && i < current_conversation.length); i = i+1)
				data += current_conversation[i]+"<br/>";
			output.innerHTML = data;
		}	
		else
			output.innerHTML += text+"<br/>";			
	}

	function getCurrentTime()
	{
		Stamp = new Date();		
		var Seconds = Stamp.getSeconds();
		var Hours = Stamp.getHours();
		var Mins = Stamp.getMinutes();
		var Time;
		
		if (Hours >= 12) 
			Time = "p.m.";		
		else 
			Time = "a.m.";
		
		if (Hours > 12) 
			Hours -= 12;
				
		if (Hours == 0) 
			Hours = 12;
		
		if (Mins < 10) 
			Mins = "0" + Mins;
	
		if (Seconds < 10) 
			Seconds = "0" + Seconds;

		return Hours + ":" + Mins + ":" + Seconds + " " + Time;
	}

	function process(field,key)
	{
		var keyCode = key;
		if (key.keyCode > 0)
			keyCode = key.keyCode;
		else if (field.value.length == 0)
			return; 
		var output = document.getElementById('output');
		if (keyCode == '13')//enter
		{
			if (DEBUG) debug('debug_area','process(field['+field.name+','+field.value+'],'+keyCode+');');
			call('process.php','userResponse','user_response_callback',field.value);
			call('process.php','processResponse','nextquestion',current_question,field.value);
			/*
			if (current_user == "")
				append(field.value);
			else
				append("<strong>"+current_user+"</strong>: "+field.value);
			*/
			field.value = "";
		}
	}
	
