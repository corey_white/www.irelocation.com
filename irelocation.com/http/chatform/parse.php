<?
	include "../inc_mysql.php";
	define(STATES,"AL|AK|AZ|AR|CA|CO|CT|DE|DC|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY");
	//give me a textual location and i will try to find a zip code corresponding to it.	
	function location($input)
	{
		$possible_zip = ereg_replace("[^0-9]","",$input);
		if (strlen($possible_zip) == 5 || strlen($possible_zip)  == 9)//85282 or 85282-0000
		{
			$zip = substr($possible_zip,0,5);
			$sql = " select concat(city,', ',state,' ',zip) 'location' from movingdirectory.zip_codes where zip = '$zip' and length(city) > 3 limit 1";
			$rs = new mysql_recordset($sql);
			if($rs->fetch_array())//found!
			{
				return $rs->myarray['location'];
			}
		}
		if (substr_count($input,",") > 0)//maybe <city>, <ST> ? or just a comma in a sentance.
		{
			$city_state_ereg = ".*(".STATES.").*";
			if (ereg($city_state_ereg,$input))
			{
				$state = ereg_replace($city_state_ereg,"\\1",$input);	
					return $state;
			}
		}		
		else return "not found: $input ";
	}
		
	function state($in)
	{
		$in = ucwords($in);
		
		$states = array('Alabama' => 'AL',
					'Alaska' => 'AK',
					'Arizona' => 'AZ',
					'Arkansas' => 'AR',
					'California' => 'CA',
					'Colorado' => 'CO',
					'Connecticut' => 'CT',
					'Delaware' => 'DE',
					'District Of Columbia' => 'DC',
					'Florida' => 'FL',
					'Georgia' => 'GA',
					'Hawaii' => 'HI',
					'Idaho' => 'ID',
					'Illinois' => 'IL',
					'Indiana' => 'IN',
					'Iowa' => 'IA',
					'Kansas' => 'KS',
					'Kentucky' => 'KY',
					'Louisiana' => 'LA',
					'Maine' => 'ME',
					'Maryland' => 'MD',
					'Massachusetts' => 'MA',
					'Michigan' => 'MI',
					'Minnesota' => 'MN',
					'Mississippi' => 'MS',
					'Missouri' => 'MO',
					'Montana' => 'MT',
					'Nebraska' => 'NE',
					'Nevada' => 'NV',
					'New Hampshire' => 'NH',
					'New Jersey' => 'NJ',
					'New Mexico' => 'NM',
					'New York' => 'NY',
					'North Carolina' => 'NC',
					'North Dakota' => 'ND',
					'Ohio' => 'OH',
					'Oklahoma' => 'OK',
					'Oregon' => 'OR',
					'Pennsylvania' => 'PA',
					'Rhode Island' => 'RI',
					'South Carolina' => 'SC',
					'South Dakota' => 'SD',
					'Tennessee' => 'TN',
					'Texas' => 'TX',
					'Utah' => 'UT',
					'Vermont' => 'VT',
					'Virginia' => 'VA',
					'Washington' => 'WA',
					'West Virginia' => 'WV',
					'Wisconsin' => 'WI',
					'Wyoming' => 'WY');		

		$out = $states[$in];

		if (strlen($out) > 0)
			return $out;
		else
		{
			$state_list = implode(' ',array_values($states));
			$in = strtoupper($in);
			if (substr_count($state_list,$in) > 0)//allready abbreviated.
				return $in;
		}
	
		return "";
	}

	echo location($_REQUEST['input']);
?>

