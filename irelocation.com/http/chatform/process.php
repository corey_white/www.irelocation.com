<?
	session_start();
	define(maybe,(rand(0,50)>50));

	include "../inc_mysql.php";
	
	
	define(MARBLE,"marble.auto_quotes");
	define(ATUS,"movingdirectory.quotes");

	define(LEAD_TABLE,MARBLE);

	define(LEAD_SOURCE,"");
	define(NEGATIVE,"wrong ,incorrect ,misspelled ,invalid ,nope ,no ,nah ,not even close ,bad ,mistake ,messed ,screwed ,missed ");
	define(SWEAR_WORDS," shit , ass , damn , fuck , penis , fucking , bitch , asshole , dumbass ");
	define(FILLER,"$!&# ,it ,it's ,a ,an ,is ,its ,my ,car ,vehicle ,truck ,van ,our ,we ,have ,think ,maybe ,pos ,the ");//filler words to clean out.
	define(NAMES,"Jack,Jesse,Dave,Mark,Mike,Tom,Pearl,Ben,Tony,Travis");
	define(VEHICLE_TYPES,"car,truck,sport utility vehicle,sports car,van,sedan,suv");
	define(NO_NSRS,"fname,condition,origin_state,email,|CAR|,|SHIPPING|,|CONTACT|");
	define(ORDER,"fname,lname,|CAR|,condition,type,make,model,year,|SHIPPING|,origin_state,origin_city,destination_state,destination_city,est_move_date,|CONTACT|,email,phone,summary");	
	define(STATES,"AL|AK|AZ|AR|CA|CO|CT|DE|DC|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY");
	
	function setFixingState($value)
	{
		$_SESSION['fixing'] = $value;
	}

	function getFixingState() { return $_SESSION['fixing']; }

	


	function userResponse($response)
	{
		$response =  " ".$response." ";
		$words = split(",",SWEAR_WORDS);
		foreach($words as $w) $response = str_replace($w," $!&# ",$response);

		$name = getClientName();
		if (strlen($name) > 0) $response = "<span class='username'>".$name."</span>: ".trim($response);
		else $response = trim($response);
		return $response;
	}
	
	function testPhone($phone)
	{
		$area = substr($phone,0,3);
		$pre = substr($phone,3,3);
		$sql = "select country from movingdirectory.areacodes where npa = '$area' and nxx = '$pre' and country = 'US' limit 1";
		$rs = new mysql_recordset($sql);
		return $rs->fetch_array();
	}

	function init()
	{
		$_SESSION['web_user'] = "";
		$_SESSION['fixing'] = false;
		$_SESSION['name'] = "";
		$name = getName();

		if (date("H") > 12) $afternoon = true;
		else $afternoon = false;
		
		$start = array("Hello, ","Hi, ","Greetings, ");
		if ($afternoon)
		{
			$start[] = "Good afternoon, ";
			$start[] = "Good day, ";			
		}
		else
		{
			$start[] = "Good morning, ";
			$start[] = "Morning, ";
		}
		$names = array("my name is $name.","I am $name.","I'm $name.","you can call me $name.");
		$last = array("I am here to help you ship your car.",
						"I am here to help you transport your vehicle.",
						"I'm going to ask a few questions about shipping your vehicle.");
		shuffle($start);
		shuffle($names);
		shuffle($last);
		
		$greeting = "<span class='username'>$name</span>: ".$start[0].$names[0]." &nbsp;".$last[0];
	
		return $greeting;
	}

	function getName()
	{
		if ($_SESSION['name'] == "")
		{
			$array = split(",",NAMES);
			shuffle($array);
			$_SESSION['name'] = $array[0];
		}
		return $_SESSION['name'];
	}
	
	function getClientName()
	{
		$o = getObject();
		return ucwords($o->myarray['fname']);
	}	

	class autoquote
	{
		var $firstname = "";
		var $myarray;
		var $next_question;
		var $last_error;
		
		function autoquote()
		{
			$this->myarray = array();
			$this->next_question = "fname";
		}

		function getNext() { return $this->next_question; }

		function save($key,$value)
		{	
			if (substr_count(ORDER,$key) == 1)
			{
				$value = trim($value);
				$valid = false;
				if ($key == $value && $key[0] == "|") $valid = true;
				else
					$valid = $this->test($key,$value);
				if ($valid && getFixingState())//fixed the problem.
					return "summary";
				if ($valid)
				{	//get next keyword.
					if ($key == "fname")
						mail("David@irelocation.com","CHAT",print_r($this,true));
					if ($key == "fname" && strlen($this->myarray['lname']) > 0)
						$key = "lname";//skip lname
					
					if ($key == "summary")
						return getPrice();
					else
					{
						$order = split(",",ORDER);
						$next = array_search($key,$order)+1;
						if ($next < count($order))
						{
							$this->next_question = $order[$next];
							return $this->getNext();
						}
					}
				}
				else 
				{
					if ($key == "summary")
					{
						return "fix:".$this->fixvalue;
					}
					else
						return -1;
				}
			}
			else return -2;
		}
		
		function test($key,$value)
		{
			if ($key == "summary")
			{
				$response = " ".$value." ";
				$ek = split(",",NEGATIVE);				
				foreach($ek as $k)
				{
					if (substr_count($response,$k) > 0)
					{
						$valid = false;
						$kws = split(",",ORDER);		
						foreach($kws as $kw)
						{
							if ($kw[0] == "|") continue;//skip headers.
							if (substr_count($response,$kw) > 0)//keyword found!.
								$this->fixvalue = $kw;
							return false;
						}
					}
				}
				return true;//looks like a valid response!
			}
			else if ($key == "fname")
			{
				if (substr_count($value," ") > 0)
				{
					if (substr_count($value," ") > 1)//multiple spaces, "Mary Lou Samsonite",
					{
						list($f,$m,$l) = split(" ",$value,3);						
						if (strlen($f." ".$m) > 4 && strlen($l) > 1) 
						{
							$this->myarray['fname'] = $f." ".$m;
							$this->firstname = $f." ".$m;
							$this->myarray['lname'] = $l;
							return true;
						}
						else
						{
							$this->last_error = "shortname";
							return false;
						}
					}
					else
					{
						list($f,$l) = split(" ",$value,2);
						
						if (strlen($f) > 2 && strlen($l) > 1)
						{
							$this->myarray['fname'] = $f;
							$this->firstname = $f;
							$this->myarray['lname'] = $l;
							return true;
						}
						else
						{
							$this->last_error = "shortname";
							return false;
						}
					}
				}
				else
				{
					$this->myarray['fname'] = trim($value);
					return true;
				}
			}
			else if ($key == "lname")
			{
				$this->myarray['lname'] = trim($value);
				return true;
			}
			else if ($key == 'email')
			{
				if (strlen($value) > 6)
				{	$this->myarray['email'] = $value; return true; }
				else
				{	$this->last_error="bademail"; return false;		}
			}
			else if ($key == 'origin_zip')
			{
				$zip = ereg_replace("[^0-9]","",$value);
				if (strlen($value) == 5)
				{
					$r = testZipCode($zip);
					if (count($r) == 3)
					{
						$this->myarray['origin_city'] = $r[0];
						$this->myarray['origin_state'] = $r[1];
						$this->myarray['origin_zip'] = $r[2];
						return true;
					}
				}
				else
				{	$this->last_error="badoriginzip"; return false;		}			
			}
			else if ($key == 'destination_zip')
			{
				$zip = ereg_replace("[^0-9]","",$value);
				if (strlen($value) == 5)
				{
					$r = testZipCode($zip);
					if (count($r) == 3)
					{
						$this->myarray['destination_city'] = $r[0];
						$this->myarray['destination_state'] = $r[1];
						$this->myarray['destination_zip'] = $r[2];
						return true;
					}
				}
				else
				{	$this->last_error="baddestinationzip"; return false;		}			
			}
			else if ($key == 'phone')
			{
				$phone = ereg_replace("[^0-9]","",$value);
				if (strlen($phone) == 11 && $phone[0] == 1)//1 602 ...
					$phone = substr($phone,1);//trim it off..
				if (strlen($phone) == 10)
				{						
					if (testPhone($phone))
					{
						$this->myarray['phone'] = $phone; return true; 						
					}
					else
					{
						$this->last_error = "phonescrubbed";
						return false;
					}
				}
				else
				{	$this->last_error="shortphone"; return false;		}
			}	
			
			else if ($key == "origin_state")
			{				
				$state = cleanState($value);
				if (strlen($state ) == 2) 
				{
					$this->myarray['origin_state'] = $state;
					return true;	
				}			
				else
				{
					$this->last_error = 'statenotfound';
					return false;				
				}
			}
			else if ($key == "destination_state")
			{				
				$state = cleanState($value);
				if (strlen($state) == 2) 
				{
					$this->myarray['destination_state'] = $state;
					return true;	
				}			
				else
				{
					$this->last_error = 'statenotfound';
					return false;				
				}
			}
			else if ($key == "origin_city")
			{				
				$results = testCityState($value,$this->myarray['origin_state']);
				mail("david@irelocation.com","Origin",print_r($results,true));
				if (count($results) == 3)	
				{
					$this->myarray['origin_city'] = $results[0];
					$this->myarray['origin_state'] = $results[1];
					$this->myarray['origin_zip'] = $results[2];
					return true;
				}
				else
				{
					$this->last_error = 'origincitynotfound';
					return false;				
				}
			}
			else if ($key == "destination_city")
			{				
				$results = testCityState($value,$this->myarray['destination_state']);
				mail("david@irelocation.com","Destination",print_r($results,true));
				if (count($results) == 3)	
				{
					$this->myarray['destination_city'] = $results[0];
					$this->myarray['destination_state'] = $results[1];
					$this->myarray['destination_zip'] = $results[2];
					return true;
				}
				else
				{
					$this->last_error = 'destinationcitynotfound';
					return false;				
				}
			}
			else if ($key == "est_move_date")
			{
				$timetamp = strtotime($value);
				if ($timestamp == -1)
				{ $this->last_error = "estmovedate"; return false; }
				else
				{
					$this->myarray['est_move_date'] = split(" ",date("m d Y",$timestamp));
					return true;
				}
			}
			else if ($key == "condition")
			{
				$value = strtolower($value);
				if ($value[0] == 'y')
				{ $this->myarray['condition'] = 'r'; return true; }
				else if ($value[0] == 'n')
				{ $this->myarray['condition'] = 'n'; return true; }
				else
				{
					if (substr_count($value,"yes") > 0 || substr_count($value,"i think so") > 0 || substr_count($value,"affirmative") > 0)
					{	$this->myarray['condition'] = "r"; return true; }
					else if (substr_count($value,"no") > 0 || substr_count($value,"negative") > 0 || substr_count($value,"i don't think so") > 0)
					{	$this->myarray['condition'] = "n"; return true; }
					else
					{
						$this->last_error = "condition"; 
						return false;
					}
				}
			}
			else if ($key == 'year')
			{
				//echo "VALUE: $value<br/>";
				$year = ereg_replace("[^0-9]","",$value);
				//echo "YEAR: $year<Br/>";
				//echo "STRLEN(YEAR) = ".strlen($year)."<Br/>";

				if (strlen($year) == 4 && intval($year) > (date("Y")-60))//valid
				{
					$this->myarray['year'] = $year;
					//echo "4 DIG YEAR: $year<Br/>";
					return true;
				}
				//echo "STRLEN(YEAR) = ".strlen($year)."<Br/>";
				if (strlen($year) == 2)
				{
					//echo "2<Br/>";
					if (intval($year) < intval(date('y')+1)) 
						$this->myarray['year'] = "20".$year;
					else 
						$this->myarray['year'] = "19".$year;
					
					//echo "2(4) DIG YEAR: ".$this->myarray['year']."<Br/>";
					return true;
				}
				
				if (strlen($year) == 1)
				{
					$this->myarray['year'] = "200".$year;
					//echo "1(4) DIG YEAR: ".$this->myarray['year']."<Br/>";
					return true;
				}
				
				
				//work on text years later...
				//echo "BAD YEAR: ".$value."<Br/>";
				$this->last_error="year"; return false;
				
			}
			else if ($key == "type")
			{
				$v_type = strtolower(cleanVehicleDetails($value));
				$types = split(",",VEHICLE_TYPES);
				foreach($types as $type)
					if (substr_count($v_type,$type) > 0)//found!
					{	$this->myarray['type'] = $type; return true; }
				$this->last_error = "type";
				return false;
			}
			else if ($key == "make")
			{
				$make = cleanVehicleDetails($value);
				if (strlen($make) > 0) 
				{
					$this->myarray['make'] = $make;
					return true;
				}
				else	{	$this->last_error = "make"; return false;	}	
			}
			else if ($key == "model")
			{
				$model = cleanVehicleDetails($value);
				if (strlen($model) > 0) 
				{
					$this->myarray['model'] = $model;
					return true;
				}
				else	{	$this->last_error = "model"; return false;	}	
			}
			else return true;//catch all..
		}

		function getSQL($table="marble.auto_quotes")
		{
			extract($this->myarray);
			$now = date("YmdHis");
			if ($table == "marble.auto_quotes")
			{
				$ip = $_SESSION['ip'];
				$referer = $_SESSION['referer'];
				$est_move_date = $est_move_date[0]."-".$est_move_date[1]."-".$est_move_date[2];
				$sql = "insert into marble.auto_quotes set fname = '$fname', lname = '$lname', ".
						"email = '$email', phone = '$phone', contact = 'email', est_move_date = '$est_move_date', ".
						"origin_zip = '$origin_zip', origin_state = '$origin_state', origin_city = '$origin_city', ".
						"destination_zip = '$destination_zip',destination_state = '$destination_state', ".
						"destination_city = '$destination_city', vtype='$type', vyear = '$vyear', vmake = '$make', ".
						"vmodel = '$model', running = '$condition', received = '$now', source = '".LEAD_SOURCE."',".
						"remote_ip = '$ip', referrer = '$referer', comments = ''";
			}
			else
			{
				$est_move_date = $est_move_date[2]."-".$est_move_date[0]."-".$est_move_date[1];
				$sql_comments = "Vehicle Type: $type\nVehicle Year: $year\nVehicle Make: $make\n".
								"Vehicle Model: $model\n\n".
								"Comments: ".(($condition=='r')?"running":"not-running")." - ";

				$sql = "insert into movingdirectory.quotes set name = '$fname $lname', cat_id = 1, ".
						"email = '$email', phone_home = '$phone', contact = 'email', est_move_date = '$est_move_date', ".
						"origin_zip = '$origin_zip', origin_state = '$origin_state', origin_city = '$origin_city', ".
						"destination_state = '$destination_state', destination_city = '$destination_city',  ".						
						"comments = '$sql_comments', source = 'atus_".LEAD_SOURCE."' ";
			}	
		}
	}	

	function getPrice()
	{
		return formatPriceResponse(getXMLAutoquote());
	}

	
	function getNR($question)
	{
		$o = getObject();
		$username = $o->myarray['fname'];	
		$nsr = array("All right... ","Let's see what's next.","good...","Ok.",
					"Ok, moving on... ","Thanks","Thank you $username.",
					 "Thanks $username");
			
		shuffle($nsr);
		if (rand(0,100) > 65 && substr_count(NO_NSRS,$question) == 0)
			return "<span class='username'>".getName()."</span>: ".$nsr[0];
		else
			return "";
	}

	function testZipCode($zip)
	{
		$sql = "select city,state,zip from movingdirectory.zip_codes where zip = '$zip' ".
				" limit 1";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return array($rs->myarray['city'],$rs->myarray['state'],$rs->myarray['zip']);
		}
		else
		{
			return array();
		}
	}

	function testCityState($city,$state)
	{
		$sql = "select city,state,zip from movingdirectory.zip_codes where state = '$state' ".
				" and city = '$city' limit 1";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return array($rs->myarray['city'],$rs->myarray['state'],$rs->myarray['zip']);
		}
		else
		{
			return array();
		}
	}

	function cleanState($in)
	{
		$in = ucwords($in);
		
		$states = array('Alabama' => 'AL',
					'Alaska' => 'AK',
					'Arizona' => 'AZ',
					'Arkansas' => 'AR',
					'California' => 'CA',
					'Colorado' => 'CO',
					'Connecticut' => 'CT',
					'Delaware' => 'DE',
					'District Of Columbia' => 'DC',
					'Florida' => 'FL',
					'Georgia' => 'GA',
					'Hawaii' => 'HI',
					'Idaho' => 'ID',
					'Illinois' => 'IL',
					'Indiana' => 'IN',
					'Iowa' => 'IA',
					'Kansas' => 'KS',
					'Kentucky' => 'KY',
					'Louisiana' => 'LA',
					'Maine' => 'ME',
					'Maryland' => 'MD',
					'Massachusetts' => 'MA',
					'Michigan' => 'MI',
					'Minnesota' => 'MN',
					'Mississippi' => 'MS',
					'Missouri' => 'MO',
					'Montana' => 'MT',
					'Nebraska' => 'NE',
					'Nevada' => 'NV',
					'New Hampshire' => 'NH',
					'New Jersey' => 'NJ',
					'New Mexico' => 'NM',
					'New York' => 'NY',
					'North Carolina' => 'NC',
					'North Dakota' => 'ND',
					'Ohio' => 'OH',
					'Oklahoma' => 'OK',
					'Oregon' => 'OR',
					'Pennsylvania' => 'PA',
					'Rhode Island' => 'RI',
					'South Carolina' => 'SC',
					'South Dakota' => 'SD',
					'Tennessee' => 'TN',
					'Texas' => 'TX',
					'Utah' => 'UT',
					'Vermont' => 'VT',
					'Virginia' => 'VA',
					'Washington' => 'WA',
					'West Virginia' => 'WV',
					'Wisconsin' => 'WI',
					'Wyoming' => 'WY');		

		$out = $states[$in];

		if (strlen($out) > 0)
			return $out;
		else
		{			
			$in = strtoupper($in);
			if (substr_count(STATES,$in) > 0)//allready abbreviated.
				return $in;
		}
	
		return "";
	}

	function askAgain($q)
	{
		$name = getName();
		$o = getObject();
		$username = $o->myarray['fname'];
		$error = $o->last_error;
		
			if ($error == "shortname")
				$msg = "<span class='username'>$name</span>: That is a very short name, would you please give me your full name?";
			else if ($error == "nospace")
				$msg = "<span class='username'>$name</span>: It looks like that's your first or last name, but not both. would you please give me your first and last name?";
			else if ($error == "bademail")
				$msg = "<span class='username'>$name</span>: I really need a valid email address. We will not use this address for anything besides finding you a great mover. Can i please have your e-mail address?";
			else if ($error == "shortphone")
				$msg = "<span class='username'>$name</span>: That number is too short, i need your full Phone Number including the area code.";
			else if ($error == "phonescrubbed")
				$msg = "<span class='username'>$name</span>: I'm sorry our system couldn't validate that phone number, try entering it again or even a differant number.";			
			else //generic error
			{
				$apologies = array("I'm sorry $username, ","Sorry, ","Unfortunately ");
				$bodies = array("I couldn't understand what you were trying to say, ","I didn't understand that, ", "That isn't exactly what i wanted... ");
				$again = array("please try again.", "can you please try again?", "please try again, sorry for the inconveniance, $username");
		
				shuffle($apologies);
				shuffle($bodies);
				shuffle($again);		
		
				$msg = "<span class='username'>$name</span>: ".$apologies[0]." ".$bodies[0]." ".$again[0];
			}	
		return $msg;
	}

	function saveObject($o)
	{
		$_SESSION['web_user'] = serialize($o);
	}

	function getObject()
	{		
		if ($_SESSION['web_user'] == "")//initialize
		{
			$a = new autoquote();
			$_SESSION['web_user'] = serialize($a);
			//mail("david@irelocation.com","Serialized Object",$_SESSION['web_user']);
		}
		else
		{
			$a = unserialize($_SESSION['web_user']);
			//if (!is_object($a)) mail("david@irelocation.com","Serialized Object",$_SESSION['web_user']);
		}
		return $a;
	} 

	function cleanVehicleDetails($value)
	{
		$value = " ".$value;
		$fillers = split(",",FILLER);
		foreach($fillers as $f)
			$value = str_replace($f," ",$value);
		return trim($value);
	}
	
	function processResponse($question,$response)
	{
		$object = getObject();
		if (is_object($object))
		{
			$result = $object->save($question,$response);		
			saveObject($object);
			return $result;
		}
		else 
			return "\$object is not an Object!<br/>".print_r($object,true).".";
	}

	function getQuestion($field)
	{		
		if ($field == "summary") return summary();

		if (substr($field,0,4) == "fix:")
		{
			mail("david@irelocation.com","chat fix",$field);
			setFixingState(true);
			$field = str_replace("fix:","",$field);
		}	
		$questions['|CONTACT|'] = array("Ok, Now let's get some contact information so we can reach you.");

		$questions['|SHIPPING|'] = array("Now let's move on to where you want to move this vehicle...",
										"Now let's go on to the transportation details.",
										"Now let's talk about where you want this vehicle picked up.");

		$questions['|CAR|'] = array("Let's start off with what vehicle you would like to move.",
									"Let's talk about what car you need shipped.");

		$questions['fname'] = array("What is your name?","What is your full name?");
		$questions['lname'] = array("What is your last name?");
		$questions['email'] = array("What is your E-Mail Address?");
		$questions['phone'] = array("What is your phone number?","What is a telephone number we can reach you at?");	

		$questions['origin_state'] = array("What state do you want to ship your vehicle from?","What state is your car in now?");
		$questions['origin_city'] = array("and what city?","What city are you shipping your vehicle from?","Where would you like your vehicle picked up?");
		$questions['origin_zip'] = array("What zip code are you moving from?","What zip code is this move shipping from?","What is your origin's zip code?");
		
		$questions['destination_zip'] = array("What zip code are you moving to?","What zip code is this move going to?","What is your destination's zip code?");
		$questions['destination_state'] = array("What state would you like your vehicle shipped to?","what state would you like your vehicle to be moved to?","To what state would you like your vehicle shipped?");
		$questions['destination_city'] = array("What city would you like your vehicle shipped to?","what city would you like your vehicle to be moved to?","To what city would you like your vehicle shipped?");			
		
		$questions['est_move_date'] = array("When would you like to ship your car?");
		$questions['condition'] = array("Is your vehicle currently operable?","Does your vehicle run on its own?","Does your vehicle run?");
		
		$questions['type'] = array("Is your vehicle a Car, Truck, or SUV?","What type of vehicle do you want to ship?","What type of vehicle is it?","What type of automobile is it?");		

		if ($field == "model" || $field == "year" || $field == "make")
		{
			$o = getObject();
			$type = $o->myarray['type'];
		}
		else $type = "vehicle";

		$questions['make'] = array("What brand of $type do you have?","What company made your $type?","Who made your $type?","What is the make of your $type?");
		$questions['model'] = array("What model is your $type?");		
		$questions['year'] = array("When was your $type made?","What year was your $type made?","What year was it made?", "What year is your $type?");

		return "<span class='username'>".getName()."</span>: ".randomize($questions[$field]);
	}

	function validateAndFinish()
	{
		$o = getObject();
		if (is_object($o))
		{
			$sql = $o->getSQL(LEAD_TABLE);
			if (LEAD_TABLE == MARBLE)
			 	$rs = new marble($sql);
			else if (LEAD_TABLE == ATUS)
				$rs = new mysql_recordset($sql);
			else 
				$rs = "";
		}

		if (is_object($rs))
			return print_r($o,true);
		else
			return "ERROR!! INVALID CAMPAIGN SELECTED!";
	}

	function randomize($input_array)
	{		
		shuffle($input_array);
		$result = $input_array[0];	
		return $result;
	}

	function summary()
	{
		$o = getObject();
		$summary = "So lets review <span class='username'>".$o->myarray['fname']." ".$o->myarray['lname']."</span>.  ".
					"You would like to ship your <span class='username'>".$o->myarray['year']." ".$o->myarray['make']." ".$o->myarray['model']."</span>.  ".
					"It is going to be picked up in <span class='username'>".$o->myarray['origin_city'].", ".$o->myarray['origin_state']."</span> on or around ".
					"<span class='username'>".str_replace("-","/",$o->myarray['est_move_date'])."</span>, and will be delivered to <span class='username'>".$o->myarray['destination_city'].", ".
					$o->myarray['destination_state']."</span>. ";
		if ($o->myarray['condition'] == 'n') $summary .= "  Also i remember you saying that your vehicle <span class='username'>doesn't run very well.</span> ";
		$summary .= " Is this all correct?";
		return $summary;
	}
	
	include "autoquote.php";
	include "agent.php";
?>