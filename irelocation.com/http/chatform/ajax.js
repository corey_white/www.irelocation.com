function LTrim( value ) {
	
	var re = /\s*((\S+\s*)*)/;
	return value.replace(re, "$1");
	
}

// Removes ending whitespaces
function RTrim( value ) {
	
	var re = /((\s*\S+)*)\s*/;
	return value.replace(re, "$1");
	
}

// Removes leading and ending whitespaces
function trim( value ) {
	
	return LTrim(RTrim(value));
	
}

function getElement(aID)
{ 
    if (aID != null)
		 return (document.getElementById) ? document.getElementById(aID)
                                      : document.all[aID];
} 

function debug(area, text)
{
	getElement(area).innerHTML += text+"<br/>";
}

function call(page,func,callback)
{
	
	var xmlHttp = getAjaxObject();
	
	xmlHttp.onreadystatechange=function()
	{
		if(xmlHttp.readyState==4)
		{
			if (DEBUG) debug('debug_area',"Response: "+xmlHttp.responseText);
			eval(callback+"('"+trim(xmlHttp.responseText)+"')");
		}
	}
	
	var url = page+"?function="+func;
	if (arguments.length > 3)
	for (var i = 3; i< arguments.length; i++)
		url += "&arg[]="+escape(arguments[i]);
	//alert("URL: "+url);
	if (DEBUG) debug('debug_area',"call('"+url+"','"+callback+"');");
	xmlHttp.open("GET",url,true);
	xmlHttp.send(null);
}

function getAjaxObject()
{
	var xmlHttp;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp=new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			try
			{
				xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser does not support AJAX!");
				return false;
			}
		}
	}
	return xmlHttp;
}