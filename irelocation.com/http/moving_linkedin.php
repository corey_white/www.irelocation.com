<?php
$pageTitle = "Long Distance Moving Leads | iRelocation Network";
$metaDescription = "The iRelocation Network sells quality local and long distance moving leads at reasonable rates. Contact us to find out more about our moving lead pricing today.";
$metaTags = "moving leads, moving, local moving leads, home moving leads, corporate moving leads, long distance moving leads, household moving leads";
include("irelocation/includes/header.php");
?>
<div class="int_content">
    <h1>Household and Business Moving Leads</h1>
    <p>iRelocation Network specializes in selling the top moving leads, both local and long distance. Our simple moving quote forms give you all the information you need to book a sale. We match you up with customers looking to move now from homes up to five or more bedrooms! We also sell business moving leads for movers looking to work with corporate and small business customers.</p>
    <p>Have a question about our moving leads? Donʼt be afraid to contact us and find out more about how we do business. You can also find out more about lead availability in different regions of the country. iReloʼs moving leads are among the best in the industry, which is exemplified by our years of experience and great customer service. Start testing our moving leads today at a reasonable rate.</p>
</div>
<div class="int_form">
    <?php include('irelocation/int_form_moving.php');?>
</div>
</div>
<? include'irelocation/includes/footer.php'; ?>