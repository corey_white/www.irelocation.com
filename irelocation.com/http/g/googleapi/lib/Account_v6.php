<?php
  function getManagersClientAccounts() {
    global $authenticationContext;
    // we want to get the current manager's account clients so temporarily unset any eventually existing clientEmail setting
    $saveClientEmail = $authenticationContext->getClientEmail();
    $authenticationContext->setClientEmail("");
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getClientAccounts></getClientAccounts>";
    // execute soap call
    $clientAccounts = $someSoapClient->call("getClientAccounts", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getManagersClientAccounts()", $soapParameters);
      // in case of an error make sure that at least the clientEmail gets restored
      $authenticationContext->setClientEmail($saveClientEmail);
      return false;
    }
    // restore clientEmail
    $authenticationContext->setClientEmail($saveClientEmail);
    // make sure we really return an array
 		if(is_array($clientAccounts['getClientAccountsReturn'])) {
	    return $clientAccounts['getClientAccountsReturn'];
		}
		else {
	    return array($clientAccounts['getClientAccountsReturn']);
		}
  }

  function getClientsClientAccounts() {
    global $soapClients;
    global $authenticationContext;
    $someSoapClient = $soapClients->getAccountClient();
    // we want to get the current client's account clients so make sure that the clientEmail is set at all
    if ($authenticationContext->getClientEmail() != "") {
      // prepare soap parameters
      $soapParameters = "<getClientAccounts></getClientAccounts>";
      // execute soap call
      $clientAccounts = $someSoapClient->call("getClientAccounts", $soapParameters);
      $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
      if ($someSoapClient->fault) {
        pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getClientsClientAccounts()", $soapParameters);
        return false;
      }
      // make sure we really return an array
	 		if(is_array($clientAccounts['getClientAccountsReturn'])) {
		    return $clientAccounts['getClientAccountsReturn'];
			}
			else {
		    return array($clientAccounts['getClientAccountsReturn']);
			}
    }
    else return false;
  }

  function getAccountInfo() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getAccountInfo></getAccountInfo>";
    // execute soap call
    $accountInfo = $someSoapClient->call("getAccountInfo", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAccountInfo()", $soapParameters);
      return false;
    }
    return $accountInfo['getAccountInfoReturn'];
  }

  function updateAccountInfo($defaultAdsCoverage, $descriptiveName, $emailPromotionsPreferences, $languagePreference, $primaryBusinessCategory) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
		if ($defaultAdsCoverage) $defaultAdsCoverageXml = "<defaultAdsCoverage>".$defaultAdsCoverage."</defaultAdsCoverage>"; else $defaultAdsCoverageXml = '';
    if ($descriptiveName) $descriptiveNameXml = "<descriptiveName>".$descriptiveName."</descriptiveName>"; else $descriptiveNameXml = '';
    if ($emailPromotionsPreferences) $emailPromotionsPreferencesXml = "<emailPromotionsPreferences>".$emailPromotionsPreferences."</emailPromotionsPreferences>"; else $emailPromotionsPreferencesXml = '';
		if ($languagePreference) $languagePreferenceXml = "<languagePreference>".$languagePreference."</languagePreference>"; else $languagePreferenceXml = '';
		if ($primaryBusinessCategory) $primaryBusinessCategoryXml = "<primaryBusinessCategory>".$primaryBusinessCategory."</primaryBusinessCategory>"; else $primaryBusinessCategoryXml = '';
    $soapParameters = "<updateAccountInfo>
    									 	 <account>".
											     $defaultAdsCoverageXml.
											     $descriptiveNameXml.
											     $emailPromotionsPreferencesXml.
											     $languagePreferenceXml.
											     $primaryBusinessCategoryXml."
											   </account>
    									 </updateAccountInfo>";
    // execute soap call
    $accountInfo = $someSoapClient->call("updateAccountInfo", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":updateAccountInfo()", $soapParameters);
      return false;
    }
    return true;
  }

  function getLocalTimezone() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getLocalTimezone></getLocalTimezone>";
    // execute soap call
    $localTimezone = $someSoapClient->call("getBillingAddress", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getLocalTimezone()", $soapParameters);
      return false;
    }
    return $localTimezone['getLocalTimezoneReturn'];
  }

  function getTimezoneEffectiveDate() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getTimezoneEffectiveDate></getTimezoneEffectiveDate>";
    // execute soap call
    $timezoneEffectiveDate = $someSoapClient->call("getTimezoneEffectiveDate", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getTimezoneEffectiveDate()", $soapParameters);
      return false;
    }
    return $timezoneEffectiveDate['getTimezoneEffectiveDateReturn'];
  }

  function getBillingAddress() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getBillingAddress></getBillingAddress>";
    // execute soap call
    $billingAddress = $someSoapClient->call("getBillingAddress", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getBillingAddress()", $soapParameters);
      return false;
    }
    return $billingAddress['getBillingAddressReturn'];
  }

  function getAccountCurrency() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getAccountCurrency></getAccountCurrency>";
    // execute soap call
    $accountCurrency = $someSoapClient->call("getAccountCurrency", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAccountCurrency()", $soapParameters);
      return false;
    }
    return $accountCurrency['getAccountCurrencyReturn'];
  }

  function getDefaultAdsCoverage() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getDefaultAdsCoverage></getDefaultAdsCoverage>";
    // execute soap call
    $defaultAdsCoverage = $someSoapClient->call("getDefaultAdsCoverage", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getDefaultAdsCoverage()", $soapParameters);
      return false;
    }
    $saveArray = $defaultAdsCoverage['getDefaultAdsCoverageReturn'];
    unset($defaultAdsCoverage);
    $defaultAdsCoverage['isActiveInSearch'] = $saveArray['optInSearchNetwork'];
    $defaultAdsCoverage['isActiveInContent'] = $saveArray['optInContentNetwork'];
    return $defaultAdsCoverage['getDefaultAdsCoverageReturn'];
  }

  function getEmailPromotionsPreferences() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getEmailPromotionsPreferences></getEmailPromotionsPreferences>";
    // execute soap call
    $emailPromotionsPreferences = $someSoapClient->call("getEmailPromotionsPreferences", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getEmailPromotionPreferences()", $soapParameters);
      return false;
    }
    return $emailPromotionsPreferences['getEmailPromotionsPreferencesReturn'];
  }

  function getLanguagePreference() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getLanguagePreference></getLanguagePreference>";
    // execute soap call
    $languagePreference = $someSoapClient->call("getLanguagePreference", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getLanguagePreferences()", $soapParameters);
      return false;
    }
    return $languagePreference['getLanguagePreferenceReturn'];
  }

  function getPrimaryBusinessCategory() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getPrimaryBusinessCategory></getPrimaryBusinessCategory>";
    // execute soap call
    $primaryBusinessCategory = $someSoapClient->call("getPrimaryBusinessCategory", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getPrimaryBusinessCategory()", $soapParameters);
      return false;
    }
    return $primaryBusinessCategory['getPrimaryBusinessCategoryReturn'];
  }

  function getTermsAndConditions() {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<getTermsAndConditions></getTermsAndConditions>";
    // execute soap call
    $termsAndConditions = $someSoapClient->call("getTermsAndConditions", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getTermsAndConditions()", $soapParameters);
      return false;
    }
    return $termsAndConditions['getTermsAndConditionsReturn'];
  }

  function setDefaultAdsCoverage($isActiveInContent, $isActiveInSearch) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    if ($isActiveInContent) $isActiveInContent = "true"; else $isActiveInContent = "false";
    if ($isActiveInSearch) $isActiveInSearch = "true"; else $isActiveInSearch = "false";
    $soapParameters = "<setDefaultAdsCoverage><coverage><optInContentNetwork>".$isActiveInContent."</optInContentNetwork><optInSearchNetwork>".$isActiveInSearch."</optInSearchNetwork></coverage></setDefaultAdsCoverage>";
    // execute soap call
    $someSoapClient->call("setDefaultAdsCoverage", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDefaultAdsCoverage()", $soapParameters);
      return false;
    }
    return true;
  }

  function setLocalTimezone($timezoneId) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<setLocalTimezone><timezoneID>".$timezoneId."</timezoneID></setLocalTimezone>";
    // execute soap call
    $localTimezone = $someSoapClient->call("setLocalTimezone", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLocalTimezone()", $soapParameters);
      return false;
    }
    return true;
  }

  function setEmailPromotionsPreferences($marketResearchEnabled, $newsletterEnabled, $promotionsEnabled) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    if ($marketResearchEnabled) $marketResearchEnabled = "true"; else $marketResearchEnabled = "false";
    if ($newsletterEnabled) $newsletterEnabled = "true"; else $newsletterEnabled = "false";
    if ($promotionsEnabled) $promotionsEnabled = "true"; else $promotionsEnabled = "false";
    $soapParameters = "<setEmailPromotionsPreferences><prefs><marketResearchEnabled>".$marketResearchEnabled."</marketResearchEnabled><newsletterEnabled>".$newsletterEnabled."</newsletterEnabled><promotionsEnabled>".$promotionsEnabled."</promotionsEnabled></prefs></setEmailPromotionsPreferences>";
    // execute soap call
    $someSoapClient->call("setEmailPromotionsPreferences", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setEmailPromotionPreferences()", $soapParameters);
      return false;
    }
    return true;
  }

  function setLanguagePreference($languagePreference) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<setLanguagePreference><langaugePref>".$languagePreference ."</langaugePref></setLanguagePreference>";
    // execute soap call
    $someSoapClient->call("setLanguagePreference", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLanguagePreferences()", $soapParameters);
      return false;
    }
    return true;
  }

  function setPrimaryBusinessCategory($primaryBusinessCategory) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<setPrimaryBusinessCategory><bizType>".$primaryBusinessCategory."</bizType></setPrimaryBusinessCategory>";
    // execute soap call
    $someSoapClient->call("setPrimaryBusinessCategory", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setPrimaryBusinessCategory()", $soapParameters);
      return false;
    }
    return true;
  }

  function setLoginInfo($newLoginEmail, $newPassword) {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<setLoginInfo><login>".$newLoginEmail."</login><newPassword>".$newPassword."</newPassword></setLoginInfo>";
    // execute soap call
    $someSoapClient->call("setLoginInfo", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLoginInfo()", $soapParameters);
      return false;
    }
    return true;
  }

  function createEmailPreferencesXml($marketResearchEnabled, $newsletterEnabled, $promotionsEnabled) {
  	if ($marketResearchEnabled) $marketResearchEnabled = "true"; else $marketResearchEnabled = "false";
  	if ($newsletterEnabled) $newsletterEnabled = "true"; else $newsletterEnabled = "false";
  	if ($promotionsEnabled) $promotionsEnabled = "true"; else $promotionsEnabled = "false";
    return   "<marketResearchEnabled>".$marketResearchEnabled."</marketResearchEnabled>
             <newsletterEnabled>".$newsletterEnabled."</newsletterEnabled>
             <promotionsEnabled>".$promotionsEnabled."</promotionsEnabled>";
  }

  function createDefaultAdsCoverageXml($isActiveInSearch, $isActiveinContent) {
  	if ($isActiveInSearch) $isActiveInSearch = "true"; else $isActiveInSearch = "false";
  	if ($isActiveinContent) $isActiveinContent = "true"; else $isActiveinContent = "false";
    return   "<optInContentNetwork>".$isActiveinContent."</optInContentNetwork>
             <optInSearchNetwork>".$isActiveInSearch."</optInSearchNetwork>";
  }

  function createCardInfoXml($cardNumber, $cardType, $cardVerificationNumber, $expirationMonth, $expirationYear, $startMonth, $startYear, $issueNumber, $status, $taxNumber) {
    return    "<cardNumber>".$cardNumber."</cardNumber>
              <cardType>".$cardType."</cardType>
              <cardVerificationNumber>".$cardVerificationNumber."</cardVerificationNumber>
              <expirationMonth>".$expirationMonth."</expirationMonth>
              <expirationYear>".$expirationYear."</expirationYear>
              <issueNumber>".$issueNumber."</issueNumber>
              <startMonth>".$startMonth."</startMonth>
              <startYear>".$startYear."</startYear>
              <status>".$status."</status>
              <taxNumber>".$taxNumber."</taxNumber>";
  }

  function createContactInfoXml($companyName, $name, $addressLine1, $addressLine2, $city, $state, $postalCode, $countryCode, $phoneNumber, $faxNumber, $emailAddress = '') {
    return   "<addressLine1>".$addressLine1."</addressLine1>
              <addressLine2>".$addressLine2."</addressLine2>
              <city>".$city."</city>
              <companyName>".$companyName."</companyName>
              <countryCode>".$countryCode."</countryCode>
              <faxNumber>".$faxNumber."</faxNumber>
              <name>".$name."</name>
              <phoneNumber>".$phoneNumber."</phoneNumber>
              <postalCode>".$postalCode."</postalCode>
              <state>".$state."</state>
              <emailAddress>".$emailAddress."<emailAddress>";
  }

  function createAdWordsAccount($loginEmail, $password, $languagePreference, $emailPreferencesXml, $currencyCode, $cardInfoXml, $contactInfoXml, $defaultAdsCoverageXml, $timeZoneId = '') {
    global $soapClients;
    $someSoapClient = $soapClients->getAccountClient();
    // prepare soap parameters
    $soapParameters = "<createAdWordsAccount>
                          <loginEmail>".$loginEmail."</loginEmail>
                          <password>".$password."</password>
                          <languagePreference>".$languagePreference."</languagePreference>
                          <emailPrefs>".$emailPreferencesXml."</emailPrefs>
                          <currencyCode>".$currencyCode."</currencyCode>
                          <cardInfo>".$cardInfoXml."</cardInfo>
                          <contactInfo>".$contactInfoXml."</contactInfo>
                          <defaultAdsCoverage>".$defaultAdsCoverageXml."</defaultAdsCoverage>
                          <timeZoneId>".$timeZoneId."</timeZoneId>
                        </createAdWordsAccount>";
    // execute soap call
    $someSoapClient->call("createAdWordsAccount", $soapParameters);
    $soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
    if ($someSoapClient->fault) {
      pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createAdWordsAccount()", $soapParameters);
      return false;
    }
    return true;
  }

?>
