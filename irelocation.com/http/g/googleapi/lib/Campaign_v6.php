<?php
	class APIlityCampaign {
		// class attributes
		var $name;
		var $id;
		var $status;
		var $startDate;
		var $endDate;
		var $dailyBudget;
		var $languages = array();
		var $geoTargets = array();
		var $isEnabledOptimizedAdServing;
		// this are API v3 features
		var $networkTargeting = array();
		var $isEnabledSeparateContentBids;

		// constructor
		function APIlityCampaign ($name, $id, $status, $startDate, $endDate, $dailyBudget, $networkTargeting, $languages, $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids, $campaignNegativeKeywordCriteria = array(), $campaignNegativeWebsiteCriteria = array()) {
			$this->name = $name;
			$this->id = $id;
			$this->status = $status;
			$this->startDate = $startDate;
			$this->endDate = $endDate;
			$this->dailyBudget = $dailyBudget;
			$this->isEnabledSeparateContentBids = convertBool($isEnabledSeparateContentBids);
			$this->networkTargeting = convertToArray($networkTargeting);
			$this->languages = convertToArray($languages);
			$this->geoTargets = $geoTargets;
			$this->campaignNegativeKeywordCriteria = convertToArray($campaignNegativeKeywordCriteria);
			$this->campaignNegativeWebsiteCriteria = convertToArray($campaignNegativeWebsiteCriteria);
			$this->isEnabledOptimizedAdServing = convertBool($isEnabledOptimizedAdServing);
		}

		// XML output
		function toXml() {
			if ($this->getIsEnabledSeparateContentBids()) $isEnabledSeparateContentBids = "true"; else $isEnabledSeparateContentBids = "false";

			$networkTargetingXml = "";
			foreach ($this->getNetworkTargeting() as $networkTarget) {
				$networkTargetingXml .= "\t\t<networkTarget>".$networkTarget."</networkTarget>\n";
			}

			$languagesXml = "";
			foreach ($this->getLanguages() as $language) {
				$languagesXml .= "\t\t<language>".$language."</language>\n";
			}

			$geoTargetsXml = "";
			$geoTargets = $this->getGeoTargets();
			foreach ($geoTargets['countries'] as $country) {
				$geoTargetsXml .= "\t\t<countries>".$country."</countries>\n";
			}
			foreach ($geoTargets['regions'] as $region) {
				$geoTargetsXml .= "\t\t<regions>".$region."</regions>\n";
			}
			foreach ($geoTargets['metros'] as $metro) {
				$geoTargetsXml .= "\t\t<metros>".$metro."</metros>\n";
			}
			foreach ($geoTargets['cities'] as $city) {
				$geoTargetsXml .= "\t\t<cities>".$city."</cities>\n";
			}

			$negativeWebsiteCriteriaXml = "";
			foreach ($this->getCampaignNegativeWebsiteCriteria() as $criterion) {
				$negativeWebsiteCriteriaXml .= "\t\t<negativeKeywordCriterion>\n\t\t\t<url>".$criterion['url']."</url>\n\t\t</negativeKeywordCriterion>\n";
			}

			$negativeKeywordCriteriaXml = "";
			foreach ($this->getCampaignNegativeKeywordCriteria() as $criterion) {
				$negativeKeywordCriteriaXml .= "\t\t<negativeKeywordCriterion>\n\t\t\t<text>".$criterion['text']."</text>\n\t\t\t<type>".$criterion['type']."</type>\n\t\t</negativeKeywordCriterion>\n";
			}

			$xml = "<Campaign>
	<name>".$this->getName()."</name>
	<id>".$this->getId()."</id>
	<status>".$this->getStatus()."</status>
	<startDate>".$this->getStartDate()."</startDate>
	<endDate>".$this->getEndDate()."</endDate>
	<dailyBudget>".$this->getDailyBudget()."</dailyBudget>
	<isEnabledSeparateContentBids>".$isEnabledSeparateContentBids."</isEnabledSeparateContentBids>
	<networkTargeting>\n".$networkTargetingXml."\t</networkTargeting>
	<languages>\n".$languagesXml."\t</languages>
	<geoTargets>\n".$geoTargetsXml."\t</geoTargets>
	<negativeKeywordCriteria>\n".$negativeKeywordCriteriaXml."\t</negativeKeywordCriteria>
	<negativeWebsiteCriteria>\n".$negativeWebsiteCriteriaXml."\t</negativeWebsiteCriteria>
</Campaign>";
			return $xml;
		}

		// get functions
		function getName() {
			return $this->name;
		}

		function getId() {
			return $this->id;
		}

		function getStatus() {
			return $this->status;
		}

		function getStartDate() {
			return $this->startDate;
		}

		function getEndDate() {
			return $this->endDate;
		}

		function getDailyBudget() {
			// thinking in currency units here
			return $this->dailyBudget;
		}

		function getNetworkTargeting() {
			return $this->networkTargeting;
		}

    function getIsEnabledSeparateContentBids() {
    	// make sure bool is transformed correctly to integer
			return (integer) $this->isEnabledSeparateContentBids;
		}

		function getLanguages() {
			return $this->languages;
		}

		function getGeoTargets() {
			return $this->geoTargets;
		}

		function getIsEnabledOptimizedAdServing() {
			// make sure bool is transformed correctly to integer
			return (integer) $this->isEnabledOptimizedAdServing;
		}

		function getEstimate() {
			// this function is located in TrafficEstimate.php
			return getCampaignEstimate($this);
		}

		// report function
		function getCampaignData() {
			$campaignData = array(
														'name' => $this->getName(),
														'id' => $this->getId(),
														'status' => $this->getStatus(),
														'startDate' => $this->getStartDate(),
														'endDate' => $this->getEndDate(),
														'dailyBudget' => $this->getDailyBudget(),
														'networkTargeting' => $this->getNetworkTargeting(),
														'languages' => $this->getLanguages(),
														'geoTargets' => $this->getGeoTargets(),
														'isEnabledSeparateContentBids' => $this->getIsEnabledSeparateContentBids(),
														'isEnabledOptimizedAdServing' => $this->getIsEnabledOptimizedAdServing(),
														'campaignNegativeWebsiteCriteria' => $this->getCampaignNegativeWebsiteCriteria(),
														'campaignNegativeKeywordCriteria' => $this->getCampaignNegativeKeywordCriteria()
														);
			return $campaignData;
		}

		function getCampaignStats($startDate, $endDate, $inPST = false) {
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			if ($inPST) $inPST = "true"; else $inPST = "false";
			$soapParameters = "<getCampaignStats>
														<campaignIds>".$this->getId()."</campaignIds>
														<startDay>".$startDate."</startDay>
														<endDay>".$endDate."</endDay>
														<inPST>".$inPST."</inPST>
											 	 </getCampaignStats>";
			// query the google servers for the campaign stats
			$campaignStats = $someSoapClient->call("getCampaignStats", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCampaignStats()", $soapParameters);
		    return false;
			}
			// add campaign name to the stats for the sake of clarity
			$campaignStats['getCampaignStatsReturn']['name'] = $this->getName();
			// think in currency units
			$campaignStats['getCampaignStatsReturn']['cost'] = $campaignStats['getCampaignStatsReturn']['cost'] / EXCHANGE_RATE;
			return $campaignStats['getCampaignStatsReturn'];
		}

		function getAllAdGroups() {
			global $soapClients;
			$someSoapClient = $soapClients->getAdGroupClient();
			$soapParameters = "<getAllAdGroups>
														<campaignID>".$this->getId()."</campaignID>
												 </getAllAdGroups>";
			// query the google servers for all adgroups of the campaign
			$allAdGroups = array();
			$allAdGroups = $someSoapClient->call("getAllAdGroups", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllAdGroups()", $soapParameters);
		    return false;
			}

			// when we have only one adgroup in the campaign return a (one adgroup element) array  anyway
			if (isset($allAdGroups['getAllAdGroupsReturn']['id'])) {
				$saveArray = $allAdGroups['getAllAdGroupsReturn'];
				unset($allAdGroups);
				$allAdGroups['getAllAdGroupsReturn'][0] = $saveArray;
			}

			// return only paused and active adgroups
			$allAdGroupObjects = array();
			if (isset($allAdGroups['getAllAdGroupsReturn'])) foreach($allAdGroups['getAllAdGroupsReturn'] as $adGroup) {
				if (($adGroup['status'] == "Enabled") || ($adGroup['status'] == "Paused")) {
			  if (isset($adGroup['maxCpc'])) $maxCpc = $adGroup['maxCpc'] / EXCHANGE_RATE; else $maxCpc = NULL;
			  if (isset($adGroup['maxCpm'])) $maxCpm = $adGroup['maxCpm'] / EXCHANGE_RATE; else $maxCpm = NULL;
			  if (isset($adGroup['maxContentCpc'])) $maxContentCpc = $adGroup['maxContentCpc'] / EXCHANGE_RATE; else $maxContentCpc = NULL;
				$adGroupObject = new APIlityAdGroup ($adGroup['name'], $adGroup['id'], $adGroup['campaignId'], $maxCpc, $maxCpm, $maxContentCpc, $adGroup['status']);
				array_push($allAdGroupObjects, $adGroupObject);
				}
			}

			return $allAdGroupObjects;
		}

		function getCampaignNegativeWebsiteCriteria() {
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			$soapParameters = "<getCampaignNegativeCriteria>
														<campaignId>".$this->getId()."</campaignId>
												 </getCampaignNegativeCriteria>";
			$allCampaignNegativeCriteria = $someSoapClient->call("getCampaignNegativeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCampaignNegativeWebsiteCriteria()", $soapParameters);
		    return false;
			}
			// if we have only one campaign negative criterion return a one-element array anyway
			if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'])) {
				$saveNegativeCriteria = $allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'];
			}
			else {
				$saveNegativeCriteria = array();
			}
			if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn']['id'])) {
    		unset($allCampaignNegativeCriteria);
    		$allCampaignNegativeCriteria = array();
    		if (isset($saveNegativeCriteria['url'])) $allCampaignNegativeCriteria[0] = array('url' => $saveNegativeCriteria['url']);
    	}
    	else {
    		unset($allCampaignNegativeCriteria);
    		$allCampaignNegativeCriteria = array();
    		foreach ($saveNegativeCriteria as $negativeCriterion) {
    			if (isset($negativeCriterion['url'])) array_push($allCampaignNegativeCriteria, array('url' => $negativeCriterion['url']));
    		}
    	}
			return $allCampaignNegativeCriteria;
		}

		function getCampaignNegativeKeywordCriteria() {
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();
			$soapParameters = "<getCampaignNegativeCriteria>
														<campaignId>".$this->getId()."</campaignId>
												 </getCampaignNegativeCriteria>";
			$allCampaignNegativeCriteria = $someSoapClient->call("getCampaignNegativeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCampaignNegativeKeywordCriteria()", $soapParameters);
		    return false;
			}
			// if we have only one campaign negative criterion return a one-element array anyway
			if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'])) {
				$saveNegativeCriteria = $allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'];
			}
			else {
				$saveNegativeCriteria = array();
			}
			if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn']['id'])) {
    		unset($allCampaignNegativeCriteria);
    		$allCampaignNegativeCriteria = array();
    		if (isset($saveNegativeCriteria['text'])) $allCampaignNegativeCriteria[0] = array('text' => $saveNegativeCriteria['text'], 'type' => $saveNegativeCriteria['type']);
    	}
    	else {
    		unset($allCampaignNegativeCriteria);
    		$allCampaignNegativeCriteria = array();
    		foreach ($saveNegativeCriteria as $negativeCriterion) {
    			if (isset($negativeCriterion['text'])) array_push($allCampaignNegativeCriteria, array('text' => $negativeCriterion['text'], 'type' => $negativeCriterion['type']));
    		}
    	}
			return $allCampaignNegativeCriteria;
		}

		// set functions
		function setCampaignNegativeWebsiteCriteria($newCampaignNegativeCriteria) {
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();

			// we need to save potentially existing negative KEYWORD criteria
			// as they will be deleted when we set new negative WEBSITE criteria
			$saveNegativeKeywordCriteria = array();
			$saveNegativeKeywordCriteria = $this->getCampaignNegativeKeywordCriteria();
			$saveNegativeKeywordCriteriaXml = "";
			if (!empty($saveNegativeKeywordCriteria)) {
				foreach ($saveNegativeKeywordCriteria as $saveNegativeKeywordCriterion) {
					$saveNegativeKeywordCriteriaXml .= "<criteria>
																								<criterionType>Keyword</criterionType>
																								<id>0</id>
																								<adGroupId>0</adGroupId>
																								<language></language>
																								<type>Broad</type>
																								<maxCpc>0</maxCpc>
																								<negative>true</negative>
																								<type>".trim($saveNegativeKeywordCriterion['type'])."</type>
																								<text>".trim($saveNegativeKeywordCriterion['text'])."</text>
																							</criteria>";
			  }
			}
			// end of saving negative KEYWORD criteria

			// expecting array('url' => "none.de", 'url' => "of.com", 'url' => "these.net")
			$newCampaignNegativeCriteriaXml = "";
			$soapParameters = "<setCampaignNegativeCriteria>
													 <campaignId>".$this->getId()."</campaignId>";
			if (!empty($newCampaignNegativeCriteria)) {
				foreach ($newCampaignNegativeCriteria as $newCampaignNegativeCriterion) {
					// update google servers
					$newCampaignNegativeCriteriaXml .= "<criteria>
																								<criterionType>Website</criterionType>
																								<id>0</id>
																								<adGroupId>0</adGroupId>
																								<language></language>
																								<maxCpm>0</maxCpm>
																								<negative>true</negative>
																								<url>".trim($newCampaignNegativeCriterion['url'])."</url>
																							</criteria>";
			  }
			}
			// attach saved negative KEYWORD criteria
			$soapParameters .= $saveNegativeKeywordCriteriaXml;
			// close soap parameters
			$soapParameters .= $newCampaignNegativeCriteriaXml."</setCampaignNegativeCriteria>";
			$someSoapClient->call("setCampaignNegativeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setCampaignNegativeWebsiteCriteria()", $soapParameters);
		    return false;
			}
			return true;
		}

		function setCampaignNegativeKeywordCriteria($newCampaignNegativeCriteria) {
			global $soapClients;
			$someSoapClient = $soapClients->getCriterionClient();

			// we need to save potentially existing negative WEBSITE criteria
			// as they will be deleted when we set new negative KEYWORD criteria
			$saveNegativeWebsiteCriteria = array();
			$saveNegativeWebsiteCriteria = $this->getCampaignNegativeWebsiteCriteria();
			$saveNegativeWebsiteCriteriaXml = "";
			if (!empty($saveNegativeWebsiteCriteria)) {
				foreach ($saveNegativeWebsiteCriteria as $saveNegativeWebsiteCriterion) {
					$saveNegativeWebsiteCriteriaXml .= "<criteria>
																								<criterionType>Website</criterionType>
																								<id>0</id>
																								<adGroupId>0</adGroupId>
																								<language></language>
																								<maxCpm>0</maxCpm>
																								<negative>true</negative>
																								<url>".trim($saveNegativeWebsiteCriterion['url'])."</url>
																							</criteria>";
			  }
			}
			// end of saving negative WEBSITE criteria

			// expecting array(array('text' => "none", 'type' => "Phrase"), array('text' => "of", 'type' => "Exact"), array('text' => "these", 'type' => "Broad"))
			$newCampaignNegativeCriteriaXml = "";
			$soapParameters = "<setCampaignNegativeCriteria>
													 <campaignId>".$this->getId()."</campaignId>";
			if (!empty($newCampaignNegativeCriteria)) {
				foreach ($newCampaignNegativeCriteria as $newCampaignNegativeCriterion) {
					// update google servers
					$newCampaignNegativeCriteriaXml .= "<criteria>
																								<criterionType>Keyword</criterionType>
																								<id>0</id>
																								<adGroupId>0</adGroupId>
																								<language></language>
																								<type>".trim($newCampaignNegativeCriterion['type'])."</type>
																								<maxCpc>0</maxCpc>
																								<negative>true</negative>
																								<text>".trim($newCampaignNegativeCriterion['text'])."</text>
																							</criteria>";
			  }
			}
			// attach saved negative WEBSITE criteria
			$soapParameters .= $saveNegativeWebsiteCriteriaXml;
			// close soap parameters
			$soapParameters .= $newCampaignNegativeCriteriaXml."</setCampaignNegativeCriteria>";
			$someSoapClient->call("setCampaignNegativeCriteria", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setCampaignNegativeKeywordCriteria()", $soapParameters);
		    return false;
			}
			return true;
		}

		function setName ($newName) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<name>".$newName."</name>
														</campaign>
													</updateCampaign>";
			// set the new name on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setName()", $soapParameters);
		    return false;
			}
			// update local object
			$this->name = $newName;
			return true;
		}

		function setEndDate ($newEndDate) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<endDay>".$newEndDate."</endDay>
														</campaign>
													</updateCampaign>";
			// set the new end date on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setEndDate()", $soapParameters);
		    return false;
			}
			// update local object
			$this->endDate = $newEndDate;
			return true;
		}

		function setDailyBudget ($newDailyBudget) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<dailyBudget>".($newDailyBudget * EXCHANGE_RATE)."</dailyBudget>
														</campaign>
													</updateCampaign>";
			// set the new name on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDailyBudget()", $soapParameters);
		    return false;
			}
			// update local object
			$this->dailyBudget = $newDailyBudget;
			return true;
		}

		function setIsEnabledSeparateContentBids($newFlag) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// make sure bool is transformed to string correctly
			if ($newFlag) $newFlag="true"; else $newFlag="false";
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<enableSeparateContentBids>".$newFlag."</enableSeparateContentBids>
														</campaign>
												 </updateCampaign>";
			// set the active in content flag on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setIsEnabledSeparateContentBids()", $soapParameters);
		    return false;
			}
			// update local object
			$this->isEnabledSeparateContentBids = convertBool($newFlag);
			return true;
		}

		function setNetworkTargeting($networkTargeting) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
														<networkTargeting>";
			foreach($networkTargeting as $networkTarget) {
				$soapParameters .=   "<networkTypes>".trim($networkTarget)."</networkTypes>";
			}
			$soapParameters .=	 "</networkTargeting>
														<id>".$this->getId()."</id>
													</campaign>
												</updateCampaign>";
			// set the network targets on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setNetworkTargeting()", $soapParameters);
		    return false;
			}
			// update local object
			$this->networkTargeting = $networkTargeting;
			return true;
		}

		function setLanguages ($newLanguages) {
			// expecting languages as array("en", "de", "fr")
			// update google servers
			$newLanguagesXml = "";
			if (strcasecmp(trim($newLanguages[0]), "all") != 0) {
				foreach ($newLanguages as $newLanguage) {
					// build the new languages xml
					$newLanguagesXml .= "<languages>".trim($newLanguage)."</languages>";
				}
			}
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<languageTargeting>".$newLanguagesXml."</languageTargeting>
														</campaign>
													</updateCampaign>";
			// set the new languages on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLanguages()", $soapParameters);
		    return false;
			}
			// update local object
			if (strcasecmp(trim($newLanguages[0]), "all") != 0) $this->languages = $newLanguages;	else $this->languages = array();
			return true;
		}

		function setGeoTargets ($newGeoTargets) {
			// expecting geoTargets as array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array())
			$newGeoTargetsXml = "";

			foreach($newGeoTargets['countries'] as $country) {
				$newGeoTargetsXml .= "<countries>".trim($country)."</countries>";
			}
			foreach($newGeoTargets['regions'] as $region) {
				$newGeoTargetsXml .= "<regions>".trim($region)."</regions>";
			}
			foreach($newGeoTargets['metros'] as $metro) {
				$newGeoTargetsXml .= "<metros>".trim($metro)."</metros>";
			}
			foreach($newGeoTargets['cities'] as $city) {
				$newGeoTargetsXml .= "<cities>".trim($city)."</cities>";
			}

			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! think in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<geoTargeting>".$newGeoTargetsXml."</geoTargeting>
														</campaign>
													</updateCampaign>";
			// set the new geo targets on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setGeoTargets()", $soapParameters);
		    return false;
			}
			// update local object
			$this->geoTargets = $newGeoTargets;
			return true;
		}

		function setIsEnabledOptimizedAdServing($newFlag) {
			// update google servers
			// make sure bool gets transformed to string correctly
			if ($newFlag) $newFlag = "true"; else $newFlag = "false";
			$soapParameters = "<setOptimizeAdServing>
														<campaignId>".$this->getId()."</campaignId>
														<enable>".$newFlag."</enable>
											 	 </setOptimizeAdServing>";
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// set the new optimize adserving flag on the server
			$someSoapClient->call("setOptimizeAdServing", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setIsEnabledOptimizedAdServing()", $soapParameters);
		    return false;
			}
			// update local object
			$this->isEnabledOptimizedAdServing = convertBool($newFlag);
			return true;
		}

		function setStatus($newStatus) {
			// update google servers
			global $soapClients;
			$someSoapClient = $soapClients->getCampaignClient();
			// danger! thinking in micros
			$soapParameters = "<updateCampaign>
														<campaign>
															<id>".$this->getId()."</id>
															<status>".$newStatus."</status>
														</campaign>
												 </updateCampaign>";
			// set the new status on the google servers
			$someSoapClient->call("updateCampaign", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setStatus()", $soapParameters);
		    return false;
			}
			// update local object
			$this->status = $newStatus;
			return true;
		}
	}

	// creates a local campaign object we can play with
	function createCampaignObject($givenCampaignId) {
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		// prepare soap parameters
		$soapParameters = "<getCampaign>
													<id>".$givenCampaignId."</id>
											 </getCampaign>";
		// execute soap call
		$someCampaign = $someSoapClient->call("getCampaign", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createCampaignObject()", $soapParameters);
	    return false;
		}

		// invalid ids are silently ignored. this is not what we want so put out a warning and return without doing anything.
		if (sizeOf($someCampaign) == 0) {
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning: </b>Invalid Campaign ID. No Campaign with the ID ".$givenCampaignId." found.";
			return false;
		}
		// populate class attributes
		$name = $someCampaign['getCampaignReturn']['name'];
		$id = $someCampaign['getCampaignReturn']['id'];
		$status = $someCampaign['getCampaignReturn']['status'];
		$startDate = $someCampaign['getCampaignReturn']['startDay'];
		$endDate = $someCampaign['getCampaignReturn']['endDay'];
		// think in currency units
		$dailyBudget = $someCampaign['getCampaignReturn']['dailyBudget'] / EXCHANGE_RATE;
		$networkTargeting = $someCampaign['getCampaignReturn']['networkTargeting']['networkTypes'];
		$languages = $someCampaign['getCampaignReturn']['languageTargeting']['languages'];
		// determine the geoTargets
		$geoTargets = array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array());
	  if (@is_array($someCampaign['getCampaignReturn']['geoTargeting']['countries'])) {
	  	foreach ($someCampaign['getCampaignReturn']['geoTargeting']['countries'] as $country) {
	  		array_push($geoTargets['countries'], $country);
	  	}
	  }
	  if (@is_array($someCampaign['getCampaignReturn']['geoTargeting']['regions'])) {
	  	foreach ($someCampaign['getCampaignReturn']['geoTargeting']['regions'] as $region) {
	  		array_push($geoTargets['regions'], $region);
	  	}
	  }
	  if (@is_array($someCampaign['getCampaignReturn']['geoTargeting']['metros'])) {
	  	foreach ($someCampaign['getCampaignReturn']['geoTargeting']['metros'] as $metro) {
	  		array_push($geoTargets['metros'], $metro);
	  	}
	  }
	  if (@is_array($someCampaign['getCampaignReturn']['geoTargeting']['cities'])) {
	  	foreach ($someCampaign['getCampaignReturn']['geoTargeting']['cities'] as $city) {
	  		array_push($geoTargets['cities'], $city);
	  	}
	  }

		if (IS_ENABLED_OPTIMIZED_AD_SERVING_ATTRIBUTE) {
			// isEnabledOptimizedAdServing?
			// this is not an object attribute but we make it be one. as we can change it we want to see its value
			$soapParameters = "<getOptimizeAdServing>
														<campaignId>".$id."</campaignId>
												 </getOptimizeAdServing>";
			// query the google servers whether the campaign is optimize adserving
			$isEnabledOptimizedAdServing = $someSoapClient->call("getOptimizeAdServing", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createCampaignObject()", $soapParameters);
		    return false;
			}
			$isEnabledOptimizedAdServing = $isEnabledOptimizedAdServing['getOptimizeAdServingReturn'];
		}
		else $isEnabledOptimizedAdServing = NULL;
		$isEnabledSeparateContentBids = $someCampaign['getCampaignReturn']['enableSeparateContentBids'];

		$campaignNegativeKeywordCriteria = null;
		$campaignNegativeWebsiteCriteria = null;
		if (INCLUDE_CAMPAIGN_NEGATIVE_CRITERIA) {
			$campaignNegativeKeywordCriteria = getExplicitCampaignNegativeKeywordCriteria($id);
			$campaignNegativeWebsiteCriteria = getExplicitCampaignNegativeWebsiteCriteria($id);
		}
		// end of populate class attributes

		// now we can create the object
		$campaignObject = new APIlityCampaign ($name, $id, $status, $startDate, $endDate, $dailyBudget, $networkTargeting, $languages, $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids, $campaignNegativeKeywordCriteria, $campaignNegativeWebsiteCriteria);
		return $campaignObject;
	}

	function getAllCampaigns() {
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		// just need a dummy argument here. don't tell this to the real world and just keep it inside
		$soapParameters = "<getAllAdWordsCampaigns>
													<dummy>0</dummy>
											 </getAllAdWordsCampaigns>";
		// query the google server for all campaigns
		$allCampaigns = array();
		$allCampaigns = $someSoapClient->call("getAllAdWordsCampaigns", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCampaigns()", $soapParameters);
	    return false;
		}

		// if we have only one campaign return (one element) array of campaigns anyway
		if (isset($allCampaigns['getAllAdWordsCampaignsReturn']['id'])) {
			$saveArray = $allCampaigns['getAllAdWordsCampaignsReturn'];
			unset($allCampaigns);
			$allCampaigns['getAllAdWordsCampaignsReturn'][0] = $saveArray;
		}
		$allCampaignObjects = array();
		// return only active or paused campaigns
		if (!isset($allCampaigns['getAllAdWordsCampaignsReturn'])) return $allCampaignObjects; else foreach ($allCampaigns['getAllAdWordsCampaignsReturn'] as $campaign) {
			if (($campaign['status'] == "Active") || ($campaign['status'] == "Paused") || ($campaign['status'] == "Pending")) {
				// determine the geoTargets
				$geoTargets = array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array());
				if (@is_array($campaign['geoTargeting']['countries'])) {
			  	foreach ($campaign['geoTargeting']['countries'] as $country) {
			  		array_push($geoTargets['countries'], $country);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['regions'])) {
			  	foreach ($campaign['geoTargeting']['regions'] as $region) {
			  		array_push($geoTargets['regions'], $region);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['metros'])) {
			  	foreach ($campaign['geoTargeting']['metros'] as $metro) {
			  		array_push($geoTargets['metros'], $metro);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['cities'])) {
			  	foreach ($campaign['geoTargeting']['cities'] as $city) {
			  		array_push($geoTargets['cities'], $city);
			  	}
			  }

				$isEnabledSeparateContentBids = $campaign['enableSeparateContentBids'];
				if (IS_ENABLED_OPTIMIZED_AD_SERVING_ATTRIBUTE) {
					// isEnabledOptimizedAdServing?
					// this is not an object attribute but we make it be one. as we can change it we want to see its value
					$soapParameters = "<getOptimizeAdServing>
																<campaignId>".$campaign['id']."</campaignId>
														 </getOptimizeAdServing>";
					// query the google servers whether the campaign is optimize adserving
					$isEnabledOptimizedAdServing = $someSoapClient->call("getOptimizeAdServing", $soapParameters);
					$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
					if ($someSoapClient->fault) {
				    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllCampaigns()", $soapParameters);
				    return false;
					}
					$isEnabledOptimizedAdServing = $isEnabledOptimizedAdServing['getOptimizeAdServingReturn'];
				}
				else $isEnabledOptimizedAdServing = NULL;

				$campaignNegativeKeywordCriteria = null;
				$campaignNegativeWebsiteCriteria = null;
				if (INCLUDE_CAMPAIGN_NEGATIVE_CRITERIA) {
					$campaignNegativeKeywordCriteria = getExplicitCampaignNegativeKeywordCriteria($campaign['id']);
					$campaignNegativeWebsiteCriteria = getExplicitCampaignNegativeWebsiteCriteria($campaign['id']);
				}

				$campaignObject = new APIlityCampaign ($campaign['name'], $campaign['id'], $campaign['status'], $campaign['startDay'], $campaign['endDay'], $campaign['dailyBudget'] / EXCHANGE_RATE, $campaign['networkTargeting']['networkTypes'], $campaign['languageTargeting']['languages'], $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids, $campaignNegativeKeywordCriteria, $campaignNegativeWebsiteCriteria);

				array_push($allCampaignObjects, $campaignObject);
			}
		}
		// if we have an empty account just return a notice and quit
		if (sizeOf($allCampaignObjects) == 0) {
			global $authenticationContext;
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Notice:</b> This account contains no, or only deleted Campaigns.<br />Client Manager <b>'".$authenticationContext->getEmail()."'</b> is accessing Client <b>'".$authenticationContext->getClientEmail()."'</b><br />";
		}
		return $allCampaignObjects;
	}

	function getCampaignList($campaignIds) {
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		// just need a dummy argument here. don't tell this to the real world and just keep it inside
		$soapParameters = "<getCampaignList>";
		foreach($campaignIds as $campaignId) {
			$soapParameters .= "<ids>".$campaignId."</ids>";
		}
		$soapParameters .= "</getCampaignList>";
		// query the google server for all campaigns
		$campaigns = array();
		$campaigns = $someSoapClient->call("getCampaignList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCampaignList()", $soapParameters);
	    return false;
		}

		// if we have only one campaign return (one element) array of campaigns anyway
		if (isset($campaigns['getCampaignListReturn']['id'])) {
			$saveArray = $campaigns['getCampaignListReturn'];
			unset($campaigns);
			$campaigns['getCampaignListReturn'][0] = $saveArray;
		}
		$campaignObjects = array();
		// return only active or paused campaigns
		if (!isset($campaigns['getCampaignListReturn'])) return $campaignObjects; else foreach ($campaigns['getCampaignListReturn'] as $campaign) {
			if (($campaign['status'] == "Active") || ($campaign['status'] == "Paused")) {
				// determine the geoTargets
				$geoTargets = array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array());
				if (@is_array($campaign['geoTargeting']['countries'])) {
			  	foreach ($campaign['geoTargeting']['countries'] as $country) {
			  		array_push($geoTargets['countries'], $country);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['regions'])) {
			  	foreach ($campaign['geoTargeting']['regions'] as $region) {
			  		array_push($geoTargets['regions'], $region);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['metros'])) {
			  	foreach ($campaign['geoTargeting']['metros'] as $metro) {
			  		array_push($geoTargets['metros'], $metro);
			  	}
			  }
			  if (@is_array($campaign['geoTargeting']['cities'])) {
			  	foreach ($campaign['geoTargeting']['cities'] as $city) {
			  		array_push($geoTargets['cities'], $city);
			  	}
			  }
				$isEnabledSeparateContentBids = $campaign['enableSeparateContentBids'];
				if (IS_ENABLED_OPTIMIZED_AD_SERVING_ATTRIBUTE) {
					// isEnabledOptimizedAdServing?
					// this is not an object attribute but we make it be one. as we can change it we want to see its value
					$soapParameters = "<getOptimizeAdServing>
																<campaignId>".$campaign['id']."</campaignId>
														 </getOptimizeAdServing>";
					// query the google servers whether the campaign is optimize adserving
					$isEnabledOptimizedAdServing = $someSoapClient->call("getOptimizeAdServing", $soapParameters);
					$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
					if ($someSoapClient->fault) {
				    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getCampaignList()", $soapParameters);
				    return false;
					}
					$isEnabledOptimizedAdServing = $isEnabledOptimizedAdServing['getOptimizeAdServingReturn'];
				}
				else $isEnabledOptimizedAdServing = NULL;

				$campaignNegativeKeywordCriteria = null;
				$campaignNegativeWebsiteCriteria = null;
				if (INCLUDE_CAMPAIGN_NEGATIVE_CRITERIA) {
					$campaignNegativeKeywordCriteria = getExplicitCampaignNegativeKeywordCriteria($campaign['id']);
					$campaignNegativeWebsiteCriteria = getExplicitCampaignNegativeWebsiteCriteria($campaign['id']);
				}

				$campaignObject = new APIlityCampaign ($campaign['name'], $campaign['id'], $campaign['status'], $campaign['startDay'], $campaign['endDay'], $campaign['dailyBudget'] / EXCHANGE_RATE, $campaign['networkTargeting']['networkTypes'], $campaign['languageTargeting']['languages'], $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids, $campaignNegativeKeywordCriteria, $campaignNegativeWebsiteCriteria);

				array_push($campaignObjects, $campaignObject);
			}
		}
		// if we have an empty account just return a notice and quit
		if (sizeOf($campaignObjects) == 0) {
			global $authenticationContext;
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Notice:</b> This account contains no, or only deleted Campaigns.<br />Client Manager <b>'".$authenticationContext->getEmail()."'</b> is accessing Client <b>'".$authenticationContext->getClientEmail()."'</b><br />";
		}
		return $campaignObjects;
	}

	function removeCampaign(&$campaignObject) {
		// update google servers
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		// danger! think in micros
		$soapParameters = "<updateCampaign>
													<campaign>
														<id>".$campaignObject->getId()."</id>
														<status>Deleted</status>
													</campaign>
											 </updateCampaign>";
		// delete the campaign on the google servers
		$someSoapClient->call("updateCampaign", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":removeCampaign()", $soapParameters);
	    return false;
		}
		// delete remote calling object
		$campaignObject = @$GLOBALS['campaignObject'];
		unset($campaignObject);
		return true;
	}

	function addCampaign($name, $status, $startDate, $endDate, $dailyBudget, $networkTargeting, $languages, $geoTargets, $isEnabledSeparateContentBids = false) {
		// update the google server
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		$languagesXml = "";
		$networkTargetingXml = "";
		// expecting array("target1", "target2")
		foreach($networkTargeting as $networkTarget)	{
			$networkTargetingXml .= "<networkTypes>".trim($networkTarget)."</networkTypes>";
		}
		$geoTargetsXml ="";
		foreach($geoTargets['countries'] as $country) {
			$geoTargetsXml .= "<countries>".trim($country)."</countries>";
		}
		foreach($geoTargets['regions'] as $region) {
			$geoTargetsXml .= "<regions>".trim($region)."</regions>";
		}
		foreach($geoTargets['metros'] as $metro) {
			$geoTargetsXml .= "<metros>".trim($metro)."</metros>";
		}
		foreach($geoTargets['cities'] as $city) {
			$geoTargetsXml .= "<cities>".trim($city)."</cities>";
		}
		// expecting array("en", "fr", "gr")
		if (strcasecmp ($languages[0], "all") == 0) {
			$languagesXml = "";
		}
		else {
			foreach ($languages as $language) {
				$languagesXml .= "<languages>".trim($language)."</languages>";
			}
		}
		// make sure bool is transformed to string correctly
		if ($isEnabledSeparateContentBids) $isEnabledSeparateContentBids = "true"; else $isEnabledSeparateContentBids = "false";
		// only send a start day if it is necessary
		if ($startDate) $startDateXml = "<startDay>".$startDate."</startDay>"; else $startDateXml = "";
		// think in micros
		$dailyBudget = $dailyBudget * EXCHANGE_RATE;
		$soapParameters = "<addCampaign>
													<campaign>
														<dailyBudget>".$dailyBudget."</dailyBudget>
														<name>".$name."</name>
														<status>".$status."</status>".
														$startDateXml."
														<endDay>".$endDate."</endDay>
														<networkTargeting>".$networkTargetingXml."</networkTargeting>
														<languageTargeting>".$languagesXml."</languageTargeting>
														<geoTargeting>".$geoTargetsXml."</geoTargeting>
														<enableSeparateContentBids>".$isEnabledSeparateContentBids."</enableSeparateContentBids>
												 </campaign>
											 </addCampaign>";
	  // add the campaign to the google servers
		$someCampaign = $someSoapClient->call("addCampaign", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addCampaign()", $soapParameters);
	    return false;
		}
		// populating object attributes
		$name = $someCampaign['addCampaignReturn']['name'];
		$id = $someCampaign['addCampaignReturn']['id'];
		$status = $someCampaign['addCampaignReturn']['status'];
		$startDate = $someCampaign['addCampaignReturn']['startDay'];
		$endDate = $someCampaign['addCampaignReturn']['endDay'];
		// think in currency units
		$dailyBudget = $someCampaign['addCampaignReturn']['dailyBudget'] / EXCHANGE_RATE;
		$networkTargeting = $someCampaign['addCampaignReturn']['networkTargeting']['networkTypes'];
		$languages = $someCampaign['addCampaignReturn']['languageTargeting']['languages'];
		// determine the geoTargets
		$geoTargets = array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array());
		if (@is_array($someCampaign['addCampaignReturn']['geoTargeting']['countries'])) {
	  	foreach ($someCampaign['addCampaignReturn']['geoTargeting']['countries'] as $country) {
	  		array_push($geoTargets['countries'], $country);
	  	}
	  }
	  if (@is_array($someCampaign['addCampaignReturn']['geoTargeting']['regions'])) {
	  	foreach ($someCampaign['addCampaignReturn']['geoTargeting']['regions'] as $region) {
	  		array_push($geoTargets['regions'], $region);
	  	}
	  }
	  if (@is_array($someCampaign['addCampaignReturn']['geoTargeting']['metros'])) {
	  	foreach ($someCampaign['addCampaignReturn']['geoTargeting']['metros'] as $metro) {
	  		array_push($geoTargets['metros'], $metro);
	  	}
	  }
	  if (@is_array($someCampaign['addCampaignReturn']['geoTargeting']['cities'])) {
	  	foreach ($someCampaign['addCampaignReturn']['geoTargeting']['cities'] as $city) {
	  		array_push($geoTargets['cities'], $city);
	  	}
	  }

		$isEnabledSeparateContentBids = $someCampaign['addCampaignReturn']['enableSeparateContentBids'];
		// isEnabledOptimizedAdServing default is true for new campaigns
		// we have this as an campaign object attribute as we can change it so we want to see its value
		$isEnabledOptimizedAdServing = true;

		// finished with populating object attributes
		// create object now
		$campaignObject = new APIlityCampaign($name, $id, $status, $startDate, $endDate, $dailyBudget, $networkTargeting, $languages, $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids);
		return $campaignObject;
	}

	function addCampaignList($campaigns) {
		// update the google server
		global $soapClients;
		$someSoapClient = $soapClients->getCampaignClient();
		$soapParameters = "<addCampaignList>";
		foreach ($campaigns as $campaign) {
			$geoTargetsXml = "";
			$languagesXml = "";
			$networkTargetingXml = "";
			foreach($campaign['geoTargets']['countries'] as $country) {
				$geoTargetsXml .= "<countries>".trim($country)."</countries>";
			}
			foreach($campaign['geoTargets']['regions'] as $region) {
				$geoTargetsXml .= "<regions>".trim($region)."</regions>";
			}
			foreach($campaign['geoTargets']['metros'] as $metro) {
				$geoTargetsXml .= "<metros>".trim($metro)."</metros>";
			}
			foreach($campaign['geoTargets']['cities'] as $city) {
				$geoTargetsXml .= "<cities>".trim($city)."</cities>";
			}

			// expecting array("en", "fr", "gr")
			if (strcasecmp($campaign['languages'][0], "all") != 0) {
				foreach ($campaign['languages'] as $language) {
					$languagesXml .= "<languages>".trim($language)."</languages>";
				}
			}
			foreach($campaign['networkTargeting'] as $networkTargeting) {
				$networkTargetingXml .= "<networkTypes>".trim($networkTargeting)."</networkTypes>";
			}
			// make sure bool is transformed to string correctly
			if (@$campaign['isEnabledSeparateContentBids']) $campaign['isEnabledSeparateContentBids'] = "true"; else $campaign['isEnabledSeparateContentBids'] = "false";
			// only send a start day if it is necessary
			if (@$campaign['startDate']) $startDateXml = "<startDay>".$campaign['startDate']."</startDay>"; else $startDateXml = "";
			// think in micros
			$campaign['dailyBudget'] = $campaign['dailyBudget'] * EXCHANGE_RATE;
			$soapParameters .= "<campaigns>
															<dailyBudget>".$campaign['dailyBudget']."</dailyBudget>
															<name>".$campaign['name']."</name>
															<status>".$campaign['status']."</status>".
															$startDateXml."
															<endDay>".$campaign['endDate']."</endDay>
															<networkTargeting>".$networkTargetingXml."</networkTargeting>
															<enableSeparateContentBids>".$campaign['isEnabledSeparateContentBids']."</enableSeparateContentBids>
															<languageTargeting>".$languagesXml."</languageTargeting>
															<geoTargeting>".$geoTargetsXml."</geoTargeting>
													</campaigns>";
		}
		$soapParameters .= "</addCampaignList>";
	  // add the campaigns to the google servers
		$someCampaigns = $someSoapClient->call("addCampaignList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addCampaignList()", $soapParameters);
	    return false;
		}

		// when we have only one campaign return a (one campaign element) array  anyway
		if (isset($someCampaigns['addCampaignListReturn']['id'])) {
			$saveArray = $someCampaigns['addCampaignListReturn'];
			unset($someCampaigns);
			$someCampaigns['addCampaignListReturn'][0] = $saveArray;
		}

		// create local objects
		$campaignObjects = array();
		foreach($someCampaigns['addCampaignListReturn'] as $someCampaign) {
			// populating object attributes
			$name = $someCampaign['name'];
			$id = $someCampaign['id'];
			$status = $someCampaign['status'];
			$startDate = $someCampaign['startDay'];
			$endDate = $someCampaign['endDay'];
			// think in currency units
			$dailyBudget = $someCampaign['dailyBudget'] / EXCHANGE_RATE;
			$isEnabledSeparateContentBids = $someCampaign['enableSeparateContentBids'];
			$languages = $someCampaign['languageTargeting']['languages'];
			// determine the geoTargets

			$geoTargets = array('countries' => array(), 'regions' => array(), 'metros' => array(), 'cities' => array());
			if (@is_array($someCampaign['geoTargeting']['countries'])) {
		  	foreach ($someCampaign['geoTargeting']['countries'] as $country) {
		  		array_push($geoTargets['countries'], $country);
		  	}
		  }
		  if (@is_array($someCampaign['geoTargeting']['regions'])) {
		  	foreach ($someCampaign['geoTargeting']['regions'] as $region) {
		  		array_push($geoTargets['regions'], $region);
		  	}
		  }
		  if (@is_array($someCampaign['geoTargeting']['metros'])) {
		  	foreach ($someCampaign['geoTargeting']['metros'] as $metro) {
		  		array_push($geoTargets['metros'], $metro);
		  	}
		  }
		  if (@is_array($someCampaign['geoTargeting']['cities'])) {
		  	foreach ($someCampaign['geoTargeting']['cities'] as $city) {
		  		array_push($geoTargets['cities'], $city);
		  	}
		  }
			$networkTargeting = $someCampaign['networkTargeting']['networkTypes'];
			// isEnabledOptimizedAdServing default is true
			// we have this as an campaign object attribute as we can change it so we want to see its value
			$isEnabledOptimizedAdServing = true;

			// finished with populating object attributes
			// create object now
			$campaignObject = new APIlityCampaign($name, $id, $status, $startDate, $endDate, $dailyBudget, $networkTargeting, $languages, $geoTargets, $isEnabledOptimizedAdServing, $isEnabledSeparateContentBids);
			array_push($campaignObjects, $campaignObject);
		}
		return $campaignObjects;
	}

	function addCampaignsOneByOne($campaigns) {
		// this is just a wrapper to the addCampaign function
		$campaignObjects = array();

		foreach ($campaigns as $campaign) {
			$campaignObject = addCampaign($campaign['name'], $campaign['status'], $campaign['startDate'], $campaign['endDate'], $campaign['dailyBudget'], $campaign['networkTargeting'], $campaign['languages'], $campaign['geoTargets'], @$campaign['isEnabledSeparateContentBids']);
			array_push($campaignObjects, $campaignObject);
		}
		return $campaignObjects;
	}

	function getExplicitCampaignNegativeWebsiteCriteria($id) {
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		$soapParameters = "<getCampaignNegativeCriteria>
													<campaignId>".$id."</campaignId>
											 </getCampaignNegativeCriteria>";
		$allCampaignNegativeCriteria = $someSoapClient->call("getCampaignNegativeCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getExplicitCampaignNegativeWebsiteCriteria()", $soapParameters);
	    return false;
		}
		// if we have only one campaign negative criterion return a one-element array anyway
		if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'])) {
			$saveNegativeCriteria = $allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'];
		}
		else {
			$saveNegativeCriteria = array();
		}
		if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn']['id'])) {
  		unset($allCampaignNegativeCriteria);
  		$allCampaignNegativeCriteria = array();
  		if (isset($saveNegativeCriteria['url'])) $allCampaignNegativeCriteria[0] = array('url' => $saveNegativeCriteria['url']);
  	}
  	else {
  		unset($allCampaignNegativeCriteria);
  		$allCampaignNegativeCriteria = array();
  		foreach ($saveNegativeCriteria as $negativeCriterion) {
  			if (isset($negativeCriterion['url'])) array_push($allCampaignNegativeCriteria, array('url' => $negativeCriterion['url']));
  		}
  	}
		return $allCampaignNegativeCriteria;
	}

	function getExplicitCampaignNegativeKeywordCriteria($id) {
		global $soapClients;
		$someSoapClient = $soapClients->getCriterionClient();
		$soapParameters = "<getCampaignNegativeCriteria>
													<campaignId>".$id."</campaignId>
											 </getCampaignNegativeCriteria>";
		$allCampaignNegativeCriteria = $someSoapClient->call("getCampaignNegativeCriteria", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getExplicitCampaignNegativeKeywordCriteria()", $soapParameters);
	    return false;
		}
		// if we have only one campaign negative criterion return a one-element array anyway
		if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'])) {
			$saveNegativeCriteria = $allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn'];
		}
		else {
			$saveNegativeCriteria = array();
		}
		if (isset($allCampaignNegativeCriteria['getCampaignNegativeCriteriaReturn']['id'])) {
  		unset($allCampaignNegativeCriteria);
  		$allCampaignNegativeCriteria = array();
  		if (isset($saveNegativeCriteria['text'])) $allCampaignNegativeCriteria[0] = array('text' => $saveNegativeCriteria['text'], 'type' => $saveNegativeCriteria['type']);
  	}
  	else {
  		unset($allCampaignNegativeCriteria);
  		$allCampaignNegativeCriteria = array();
  		foreach ($saveNegativeCriteria as $negativeCriterion) {
  			if (isset($negativeCriterion['text'])) array_push($allCampaignNegativeCriteria, array('text' => $negativeCriterion['text'], 'type' => $negativeCriterion['type']));
  		}
  	}
		return $allCampaignNegativeCriteria;
	}
?>