<?php
	function getKeywordsFromSite($url, $includeLinkedPages, $languages, $countries) {
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordToolClient();

		$languagesXml = "";
		$countriesXml = "";
		// process languages
		if ($languages[0] == "all") {
			$languagesXml = "";
		}
		else {
			foreach($languages as $language) {
				$languagesXml .= "<languages>".trim($language)."</languages>";
			}
		}
		// process countries
		if ($countries[0] == "all") {
			$countriesXml = "";
		}
		else {
			foreach($countries as $country) {
				$countriesXml .= "<countries>".trim($country)."</countries>";
			}
		}
		if ($includeLinkedPages) $includeLinkedPages = "true"; else $includeLinkedPages = "false";

		$soapParameters = "<getKeywordsFromSite>
													<url>".$url."</url>
													<includeLinkedPages>".$includeLinkedPages."</includeLinkedPages>".
													$languagesXml.
													$countriesXml."
											 </getKeywordsFromSite>";
		// query the google servers for the keywords from a site
		$keywordsFromSite = $someSoapClient->call("getKeywordsFromSite", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getKeywordsFromSite()", $soapParameters);
	    return false;
		}
		return $keywordsFromSite['getKeywordsFromSiteReturn'];
	}

	function getKeywordVariations($seedKeywords, $useSynonyms, $languages, $countries) {
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordToolClient();

		$languagesXml = "";
		$countriesXml = "";
		$seedKeywordsXml = "";
		// process seed keywords
		foreach($seedKeywords as $seedKeyword) {
			if ($seedKeyword['isNegative']) $seedKeyword['isNegative'] = "true"; else $seedKeyword['isNegative'] = "false";
			$seedKeywordsXml .= "<seedKeywords>
														 <negative>".trim($seedKeyword['isNegative'])."</negative>
														 <text>".trim($seedKeyword['text'])."</text>
														 <type>".trim($seedKeyword['type'])."</type>
													 </seedKeywords>";
		}
		// process languages
		if ($languages[0] == "all") {
			$languagesXml = "";
		}
		else {
			foreach($languages as $language) {
				$languagesXml .= "<languages>".trim($language)."</languages>";
			}
		}
		// process countries
		if ($countries[0] == "all") {
			$countriesXml = "";
		}
		else {
			foreach($countries as $country) {
				$countriesXml .= "<countries>".trim($country)."</countries>";
			}
		}
		// make sure boolean gets transferred to string correctly
		if ($useSynonyms) $useSynonyms = "true"; else $useSynonyms = "false";

		$soapParameters = "<getKeywordVariations>".
													$seedKeywordsXml."
													<useSynonyms>".$useSynonyms."</useSynonyms>".
													$languagesXml.
													$countriesXml."
											 </getKeywordVariations>";
		$keywordVariations = $someSoapClient->call("getKeywordVariations", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getKeywordVariations()", $soapParameters);
	    return false;
		}
		return $keywordVariations['getKeywordVariationsReturn'];
	}
?>