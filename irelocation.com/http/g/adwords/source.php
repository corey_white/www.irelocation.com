<?php
	//define this before you call this page.
	//define(SITE_BASE,"hsr");
	
	session_start();
	$engines = array("google","altavista","lycos","yahoo","search.aol","search.msn","overture","excite","directhit","webcrawler","alexa");
	
	if (!isset($_SESSION["source"]))//no source yet.
	{
		if (debug) echo "<!--- source not set --->\n";
		
		if (isset($_REQUEST["source"]))
		{
			if (debug) echo "<!--- request source set --->\n";
			$source = trim(strtolower($_REQUEST["source"]));
			
			if (substr_count($source,SITE_BASE) == 0)
			{
				if (debug) echo "<!--- site_base not included --->\n";
				$source = SITE_BASE."_$source";
			}		
		}
		else if (isset($_POST["source"]))
		{
			$source = trim(strtolower($_POST["source"]));
			if (substr_count($source,SITE_BASE) == 0)
			{
				$source = SITE_BASE."_$source";
			}		
		}
		if(isset($source))
		{
			$_SESSION["source"] = $source;
			if (debug) echo "<!--- setting source --->\n";
		}
	}
	if (debug) echo "<!--- referer: -".strtolower($HTTP_REFERER)."- --->\n";
	if (!isset($_SESSION["referer"]))//no referer yet, only get it as soon as they come to the site.
	{
		$referer = strtolower($HTTP_REFERER);	
		if (debug) echo "<!--- referer not set --->\n";
		if(substr_count($referer,"google")>0){$key="q=";}
		elseif(substr_count($referer,"altavista")>0){$key="q=";}
		elseif(substr_count($referer,"lycos")>0){$key="query=";}
		elseif(substr_count($referer,"yahoo")>0){$key="p=";}
		elseif(substr_count($referer,"search.aol")>0){$key="query=";}
		elseif(substr_count($referer,"search.msn")>0){$key="q=";}
		elseif(substr_count($referer,"overture")>0){$key="keywords=";}
		elseif(substr_count($referer,"excite")>0){$key="qkw=";}
		elseif(substr_count($referer,"directhit")>0){$key="qry=";}
		elseif(substr_count($referer,"webcrawler")>0){$key="qkw=";}
		elseif(substr_count($referer,"alexa")>0){$key="p=";}
		
		if($key!='')		
		{
			if (debug) echo "<!--- keywords not included --->\n";
			$temp=explode("$key",$referer);
			$temp2=explode("&",$temp[1]);
			$_SESSION["keyword"] = trim(str_replace("+"," ",str_replace("%2C"," ",str_replace("%20"," ",str_replace("%22","",$temp2[0])))));
		}
		
		$_SESSION["referer"] = $referer;
		
		if (!isset($_SESSION["source"]))
		{
			if (debug) echo "<!--- source not set --->\n";
			$engines = array("google","altavista","lycos","yahoo","search.aol","search.msn","overture","excite","directhit","webcrawler","alexa");
			foreach($engines as $e)
			{
				if(substr_count($referer,$e)>0)
				{
					$_SESSION["source"] = SITE_BASE."_search_$e";
					if (debug) echo "<!--- source found, free lead. --->\n";
					break;
				}
			}
		}
	}
	
	
	if ($debug == 1)
	{
		echo "<!--- --->\n";
		echo "<!---source : -".$_SESSION["source"]."- --->\n";
		echo "<!---referer: -".$_SESSION["referer"]."- --->\n";
		echo "<!---keyword: -".$_SESSION["keyword"]."- --->\n";
		echo "<!--- --->\n";
	}

?>