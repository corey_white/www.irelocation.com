<?php
	class Authentication {
		// class variables
		var $clientEmail;
		var $email;
		var $password;
		var $token;
		var $userAgent;

		// constructor
		function Authentication($email, $password, $token, $clientEmail) {
			$this->clientEmail = $clientEmail;
			$this->email = $email;
			$this->password = $password;
			$this->token = $token;
			// hard-wire this one
			$this->userAgent = "Google APIlity PHP Library for AdWords";

			// set the headers upon authentication context creation if soapclients already exist
			global $soapClients;
			if (isset($soapClients)) $soapClients->setSoapHeaders($this);
		}

		// get functions
		function getClientEmail() {
			return $this->clientEmail;
		}

		function getEmail() {
			return $this->email;
		}

		function getPassword() {
			return $this->password;
		}

		function getToken() {
			return $this->token;
		}

		function getUserAgent() {
			return $this->userAgent;
		}

		// this will return a valid header for soap clients
		function getHeader() {
			return "<email>".$this->getEmail()."</email><password>".$this->getPassword()."</password><useragent>".$this->getUserAgent()."</useragent><token>".$this->getToken()."</token><clientEmail>".$this->getClientEmail()."</clientEmail>";
		}

		// set functions
		function setClientEmail($newClientEmail) {
			$this->clientEmail = $newClientEmail;
			global $soapClients;
			$soapClients->setSoapHeaders($this);
		}

		function setEmail($newEmail) {
			$this->email = $newEmail;
			global $soapClients;
			$soapClients->setSoapHeaders($this);
		}

		function setPassword($newPassword) {
			$this->password = $newPassword;
			global $soapClients;
			$soapClients->setSoapHeaders($this);
		}

		function setToken($newToken) {
			$this->token = $newToken;
			global $soapClients;
			$soapClients->setSoapHeaders($this);
		}
	}
?>