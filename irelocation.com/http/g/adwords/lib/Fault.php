<?php

	// this defines the maximum size of the fault stack
	// only the last N fault objects are kept
	define("FAULT_STACK_SIZE", 10);

	class Fault {

		// class attributes
		var $faultString;
		var $faultDetail;
		var $faultCode;
		var $faultTrigger;
		var $faultOrigin;
		var $faultViolations;

		// constructor
		function Fault ($faultCode, $faultString, $faultDetail, $faultTrigger, $faultOrigin, $soapParameters, $faultViolations) {
			$this->faultCode = $faultCode;
			$this->faultString = $faultString;
			$this->faultDetail = $faultDetail;
			$this->faultTrigger = $faultTrigger;
			$this->faultOrigin = $faultOrigin;
			$this->soapParameters = preg_replace('/\s*/', '', $soapParameters);
			$this->faultViolations = $faultViolations;
		}

		// get functions
		function getFaultCode() {
			return $this->faultCode;
		}

		function getFaultString() {
			return $this->faultString;
		}

		function getFaultDetail() {
			return $this->faultDetail;
		}

		function getFaultTrigger() {
			return $this->faultTrigger;
		}

		function getFaultOrigin() {
			return $this->faultOrigin;
		}

		function getSoapParameters() {
			return $this->soapParameters;
		}

		function getFaultViolations() {
			return $this->faultViolations;
		}

		function printFault() {
			echo $this->getFault();
		}

		// this provides error reporting depending on the way errors should be reported (settings.ini)
		function getFault() {
			// return debuggable fault objects
			if (strcmp(DISPLAY_ERROR_STYLE, "Plaintext") == 0) {
				return "\n\n<p>FaultCode: ".$this->getFaultCode()."\n<br>FaultString: ".$this->getFaultString()."\n<br>FaultDetail: ".$this->getFaultDetail()."\n<br>FaultTrigger: ".$this->getFaultTrigger()."\n<br>FaultOrigin: ".$this->getFaultOrigin()."\n<br>SoapParameters: ".$this->getSoapParameters()."\n<br>FaultViolations: ".$this->getFaultViolations()."\n<p>";
			}

			// return beautiful html errors
			else if (strcmp(DISPLAY_ERROR_STYLE, "HTML") == 0) {
				$faultMessage = "<small>\n<p>\n<font color='blue'><b>Ouch!</b></font> I am not proud to announce the following\n</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color='maroon'><u>".$this->getFaultString()."</u></font>\n</p>\n";
				$faultMessage .= "<p>\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>".$this->getFaultDetail()."</u>\n<br />\n";
				$faultMessage .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(".$this->getFaultTrigger().")\n</p>\n";
				$faultMessage .= "<p>\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;This error's code is: <b><font color='blue'>".$this->getFaultCode()."</font></b>\n</p>\n";
				$faultMessage .= "<p>\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The error occured in <u><font color='blue'>".$this->getFaultOrigin()."</font></u>\n<br />\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;while calling <u><font color='maroon'>".utf8_encode(str_replace("�", "&#x20AC;", htmlspecialchars($this->getSoapParameters())))."</font></u>\n</p>\n</small>\n<br />\n";
				if ($this->getFaultCode() == 21) {
					 $faultMessage .= "<small>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The following policy violation was detected: <u><font color='maroon'>".htmlspecialchars(utf8_encode(html_entity_decode($this->getFaultViolations())))."</font></u>\n</p>\n</small>\n<br />\n";
				}
				return $faultMessage;
			}

			// return xml fault messages
			else if (strcmp(DISPLAY_ERROR_STYLE, "XML") == 0) {
				$xml = 	"<adWordsApiError>
	<faultCode>".$this->getFaultCode()."</faultCode>
	<faultString>".$this->getFaultString()."</faultString>
	<faultDetail>".$this->getFaultDetail()."</faultDetail>
	<faultTrigger>".$this->getFaultTrigger()."</faultTrigger>
	<faultOrigin>".$this->getFaultOrigin()."</faultOrigin>
	<soapParameters>\n\t\t".str_replace('�', '&#x20AC;', $this->getSoapParameters())."\n\t</soapParameters>";
	if (strcasecmp($this->getFaultViolations(), "") != 0) $xml .=	"\n\t<faultViolations>".html_entity_decode($this->getFaultViolations())."</faultViolations>";
	$xml .= "\n</adWordsApiError>\n";
				return utf8_encode($xml);
			}
			return false;
		}
	}

	function pushFault($someSoapClient, $faultOrigin, $soapParameters) {
		// avoid annoying warnings as not all values are always set
		if (!isset($someSoapClient->detail['code'])) $someSoapClient->detail['code'] = "";
		if (!isset($someSoapClient->faultcode)) $someSoapClient->faultcode = "";
		if (!isset($someSoapClient->faultstring)) $someSoapClient->faultstring = "";
		if (!isset($someSoapClient->detail['trigger'])) $someSoapClient->detail['trigger'] = "";
		if (!isset($faultOrigin)) $faultOrigin = "";
		if (!isset($soapParameters)) $soapParameters = "";
		if (!isset($someSoapClient->detail['violations'])) $someSoapClient->detail['violations'] = "";

		// access the global error stack
		global $faultStack;
		// and push a fault object in
		$faultObject = new Fault($someSoapClient->detail['code'], $someSoapClient->faultcode, $someSoapClient->faultstring, $someSoapClient->detail['trigger'], $faultOrigin, utf8_encode(str_replace("�", "&#x20AC;", $soapParameters)), utf8_encode(html_entity_decode($someSoapClient->detail['violations'])));

		// by default print the fault message, else when running in silence stealth mode be quiet
		if (!SILENCE_STEALTH_MODE) $faultObject->printFault();

		// push the fault object in the fault stack and keep only the last #FAULT_STACK_SIZE error messages
		array_push($faultStack, $faultObject);
		if (sizeof($faultStack) <= FAULT_STACK_SIZE) return;
		while (sizeof($faultStack) > FAULT_STACK_SIZE) {
			array_shift($faultStack);
		}
	}

?>