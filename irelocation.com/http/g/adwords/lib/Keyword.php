<?php
	class Keyword {
		// class attributes
		var $text;
		var $id;
		var $belongsToAdGroupId;
		var $type;
		var $isNegative;
		var $maxCpc;
		var $minCpc;
		var $status;
	  var $language; // think we are not using this yet but include it anyway
	  var $destinationUrl;

		// constructor
		function Keyword ($text, $id, $belongsToAdGroupId, $type, $isNegative, $maxCpc, $minCpc, $status, $language, $destinationUrl) {
			$this->text = $text;
			$this->id = $id;
			$this->belongsToAdGroupId = $belongsToAdGroupId;
			$this->type = $type;
			$this->isNegative = convertBool($isNegative);
			$this->maxCpc = $maxCpc;
			$this->minCpc = $minCpc;
			$this->status = $status;
			$this->language = $language; // no implementation yet
			$this->destinationUrl = $destinationUrl;
		}

		// XML output
		function toXml() {
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			$xml = "<Keyword>
	<text>".$this->getText()."</text>
	<id>".$this->getId()."</id>
	<belongsToAdGroupId>".$this->getBelongsToAdGroupId()."</belongsToAdGroupId>
	<type>".$this->getType()."</type>
	<isNegative>".$isNegative."</isNegative>
	<status>".$this->getStatus()."</status>
	<maxCpc>".$this->getMaxCpc()."</maxCpc>
	<minCpc>".$this->getMinCpc()."</minCpc>
	<language>".$this->getLanguage()."</language>
	<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
</Keyword>";
			return $xml;
		}

		// get functions
		function getText() {
			return $this->text;
		}

		function getId() {
			return $this->id;
		}

		function getBelongsToAdGroupId() {
			return $this->belongsToAdGroupId;
		}

		function getType() {
			return $this->type;
		}

		function getIsNegative() {
			// make sure boolean is readable so do type cast to integer
			return (integer) $this->isNegative;
		}

		function getMaxCpc() {
			return $this->maxCpc;
		}

		function getMinCpc() {
			return $this->minCpc;
		}

		function getStatus() {
			return $this->status;
		}

		// no implementation yet
		function getLanguage() {
			return $this->language;
		}

		function getDestinationUrl() {
			return $this->destinationUrl;
		}

		// report function
		function getKeywordData() {
			$keywordData = array( 'text'=>$this->getText(),
														'id'=>$this->getId(),
														'belongsToAdGroupId'=>$this->getBelongsToAdGroupId(),
														'type'=>$this->getType(),
														'isNegative'=>$this->getIsNegative(),
														'maxCpc'=>$this->getMaxCpc(),
														'minCpc'=>$this->getMinCpc(),
														'status'=>$this->getStatus(),
														'language'=>$this->getLanguage(),
														'destinationUrl'=>$this->getDestinationUrl());
			return $keywordData;
		}

		function getKeywordStats($startDate, $endDate) {
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			$soapParameters = "<getKeywordStats>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<keywordIds>".$this->getId()."</keywordIds>
														<start>".$startDate."</start>
														<end>".$endDate."</end>
												 </getKeywordStats>";
			// get keyword stats from the google servers
			$keywordStats = $someSoapClient->call("getKeywordStats", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		    pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getKeywordStats()", $soapParameters);
		    return false;
			}
			// add keyword text to the returned stats for the sake of clarity
			$keywordStats['getKeywordStatsReturn']['text'] = $this->getText();
			// transform micros to currency units
			$keywordStats['getKeywordStatsReturn']['cost'] = $keywordStats['getKeywordStatsReturn']['cost'] / EXCHANGE_RATE;
			return $keywordStats['getKeywordStatsReturn'];
		}

		function getEstimate() {
			// this function is located in TrafficEstimate.php
			return getKeywordEstimate($this);
		}

		// set functions
		function setText ($newText) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// changing the text is not provided by the api so we need to emulate this by removing and re-creating
			// then re-create the keyword with the new text set
			// make sure bool gets correctly transformed to string
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! we need to think in micros so we need to transform the object maxcpc to micros
			$soapParameters = "<addKeyword>
														<newKeyword>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<type>".$this->getType()."</type>
															<text>".$newText."</text>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<negative>".$isNegative."</negative>
															<language>".$this->getLanguage()."</language>
															<maxCpc>".($this->getMaxCpc() * EXCHANGE_RATE)."</maxCpc>
														</newKeyword>
												 </addKeyword>";
			// add the keyword on the google servers
			$someKeyword = $someSoapClient->call("addKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setText()", $soapParameters);
		    return false;
			}
			// first delete current keyword
			$soapParameters = "<removeKeyword>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<keywordId>".$this->getId()."</keywordId>
												 </removeKeyword>";
			// call the google servers and remove the keyword
			$someSoapClient->call("removeKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setText()", $soapParameters);
		    return false;
			}
			// update local object
			$this->text = $newText;
			// changing the text of a keyword will change its id, so update object id data
			$this->id = $someKeyword['addKeywordReturn']['id'];
			return true;
		}

		function setMaxCpc ($newMaxCpc) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// thinking in micros here
			$soapParameters = "<setKeywordListSingleMaxCpc>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<keywordIds>".$this->getId()."</keywordIds>
														<maxCpc>".($newMaxCpc * EXCHANGE_RATE)."</maxCpc>
												 </setKeywordListSingleMaxCpc>";
			// talk to the google servers
			$someSoapClient->call("setKeywordListSingleMaxCpc", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setMaxCpc()", $soapParameters);
		    return false;
			}
			// update local object
			$this->maxCpc = $newMaxCpc;
			return true;
		}

		function matchMaxCpcToMinCpc() {
			if ($this->getMaxCpc() < $this->getMinCpc()) $this->setMaxCpc($this->getMinCpc());
			return true;
		}

		function setType ($newType) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// changing the type is not provided by the api so emulate this by deleting and re-creating the keyword
			// then recreate it with the new type set
			// make sure bool gets transformed into string correctly
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<addKeyword>
														<newKeyword>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<type>".$newType."</type>
															<text>".$this->getText()."</text>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<negative>".$isNegative."</negative>
															<language>".$this->getLanguage()."</language>
															<maxCpc>".($this->getMaxCpc() * EXCHANGE_RATE)."</maxCpc>
														</newKeyword>
												 </addKeyword>";
			// add the keyword to the google servers
			$someKeyword = $someSoapClient->call("addKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setType()", $soapParameters);
		    return false;
			}
			// first delete current keyword
			$soapParameters = "<removeKeyword>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<keywordId>".$this->getId()."</keywordId>
												 </removeKeyword>";
			// delete the keyword on the google servers
			$someSoapClient->call("removeKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setType()", $soapParameters);
		    return false;
			}
			// update local object
			$this->type = $newType;
			// changing the type of a keyword will change its id, so update object id data
			$this->id = $someKeyword['addKeywordReturn']['id'];
			return true;
		}

		function setIsNegative ($newFlag) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// make sure bool gets transformed into string correctly
			if ($newFlag) $newFlag = "true"; else $newFlag = "false";
			// danger! think in micros
			$soapParameters = "<updateKeyword>
														<keyword>
															<id>".$this->getId()."</id>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<maxCpc>".($this->getMaxCpc() * EXCHANGE_RATE)."</maxCpc>
															<negative>".$newFlag."</negative>
														</keyword>
													</updateKeyword>";
			// update the keyword on the google servers
			$someSoapClient->call("updateKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setIsNegative()", $soapParameters);
		  	return false;
			}
			// update local object
			$this->isNegative = convertBool($newFlag);
			return true;
		}

		function setDestinationUrl ($newDestinationUrl) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// make sure bool gets transformed into string correctly
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<updateKeyword>
													 <keyword>
													 	 <id>".$this->getId()."</id>
													 	 <adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
													 	 <maxCpc>".($this->getMaxCpc() * EXCHANGE_RATE)."</maxCpc>
													 	 <destinationUrl>".$newDestinationUrl."</destinationUrl>
													 	 <negative>".$isNegative."</negative>
													 </keyword>
												</updateKeyword>";
			// update the keyword on the google servers
			$someSoapClient->call("updateKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setDestinationUrl()", $soapParameters);
		    return false;
			}
			// update local object
			$this->destinationUrl = $newDestinationUrl;
			return true;
		}

		// no implementation yet
		function setLanguage ($newLanguage) {
			// update the google servers
			global $soapClients;
			$someSoapClient = $soapClients->getKeywordClient();
			// changing the language is not provided by the api so emulate this by deleting and re-creating the keyword
			// then recreate it with the new language set
			// make sure bool gets transformed into string correctly
			if ($this->getIsNegative()) $isNegative = "true"; else $isNegative = "false";
			// danger! think in micros
			$soapParameters = "<addKeyword>
														<newKeyword>
															<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
															<type>".$this->getType()."</type>
															<text>".$this->getText()."</text>
															<destinationUrl>".$this->getDestinationUrl()."</destinationUrl>
															<negative>".$isNegative."</negative>
															<language>".$newLanguage."</language>
															<maxCpc>".($this->getMaxCpc() *EXCHANGE_RATE)."</maxCpc>
														</newKeyword>
													</addKeyword>";
			// add the keyword to the google servers
			$someKeyword = $someSoapClient->call("addKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLanguage()", $soapParameters);
		    return false;
			}
			// first delete current keyword
			$soapParameters = "<removeKeyword>
														<adGroupId>".$this->getBelongsToAdGroupId()."</adGroupId>
														<keywordId>".$this->getId()."</keywordId>
												 </removeKeyword>";
			// remove the keyword on the google servers
			$someSoapClient->call("removeKeyword", $soapParameters);
			$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
			if ($someSoapClient->fault) {
		  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":setLanguage()", $soapParameters);
		    return false;
			}
			// update local object
			$this->language = $newLanguage;
			// changing the language of a keyword will change its id, so update object id data
			$this->id = $someKeyword['addKeywordReturn']['id'];
			return true;
		}

	}

	// add keyword on google servers and create local object
	function addKeyword() {
		// emulating overloading here. dynamically react differently if exemption request is given or not
		// expected argument sequence: $text, $belongsToAdGroupId, $type, $isNegative, $maxCpc, $language, $destinationUrl ($exemptionRequest)

		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();
		// populate variables with function arguments
		$text = func_get_arg(0);
		$belongsToAdGroupId = func_get_arg(1);
		$type = func_get_arg(2);
		$isNegative = func_get_arg(3);
		$maxCpc = func_get_arg(4);
		// thinking in micros here
		$maxCpc = $maxCpc * EXCHANGE_RATE;
		$language = func_get_arg(5);
		$destinationUrl = func_get_arg(6);
		// make sure bool gets transformed to string correctly
		if ($isNegative) $isNegative = "true"; else $isNegative = "false";
		// if no exemption request
		if (func_num_args() == 7) {
			$soapParameters = "<addKeyword>
														<newKeyword>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<type>".$type."</type>
															<text>".$text."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$maxCpc."</maxCpc>
															<language>".$language."</language>
															<destinationUrl>".$destinationUrl."</destinationUrl>
														</newKeyword>
													</addKeyword>";
		}
		// if with exemption request
		else if (func_num_args() == 8){
			$exemptionRequest = func_get_arg(7);
			$soapParameters = "<addKeyword>
														<newKeyword>
															<adGroupId>".$belongsToAdGroupId."</adGroupId>
															<exemptionRequest>".$exemptionRequest."</exemptionRequest>
															<type>".$type."</type>
															<text>".$text."</text>
															<negative>".$isNegative."</negative>
															<maxCpc>".$maxCpc."</maxCpc>
															<language>".$language."</language>
															<destinationUrl>".$destinationUrl."</destinationUrl>
														</newKeyword>
													</addKeyword>";
		}
		// add keyword to the google servers
		$someKeyword = $someSoapClient->call("addKeyword", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addKeyword()", $soapParameters);
	    return false;
		}
		// create local object
		// danger! think in currency units
		$keywordObject = new Keyword ($someKeyword['addKeywordReturn']['text'], $someKeyword['addKeywordReturn']['id'], $someKeyword['addKeywordReturn']['adGroupId'], $someKeyword['addKeywordReturn']['type'], $someKeyword['addKeywordReturn']['negative'], $someKeyword['addKeywordReturn']['maxCpc'] / EXCHANGE_RATE, $someKeyword['addKeywordReturn']['minCpc'] / EXCHANGE_RATE, $someKeyword['addKeywordReturn']['status'], $someKeyword['addKeywordReturn']['language'], $someKeyword['addKeywordReturn']['destinationUrl']);
		return $keywordObject;
	}

	// this won't fail completely if only one keyword fails
	// but causes a lot soap overhead
	function addKeywordsOneByOne($keywords) {
		// this is basically just a wrapper to the addKeyword function
		$keywordObjects = array();
		foreach ($keywords as $keyword) {
			if (isset($keyword['exemptionRequest'])) {
				// with exemption request
				$keywordObject = addKeyword($keyword['text'], $keyword['belongsToAdGroupId'], $keyword['type'], $keyword['isNegative'], $keyword['maxCpc'], $keyword['language'], $keyword['destinationUrl'], $keyword['exemptionRequest']);
			}
			else {
				// without exemption request
				$keywordObject = addKeyword($keyword['text'], $keyword['belongsToAdGroupId'], $keyword['type'], $keyword['isNegative'], $keyword['maxCpc'], $keyword['language'], $keyword['destinationUrl']);
			}
			array_push($keywordObjects, $keywordObject);
		}
		return $keywordObjects;
	}

	// this will fail completely if only one keyword fails
	// but won't cause soap overhead
	function addKeywordList($keywords) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();

		$soapParameters = "<addKeywordList>
												 <adGroupId>".$keywords[0]['belongsToAdGroupId']."</adGroupId>";
		foreach ($keywords as $keyword) {
			// make sure integer is transformed to string correctly
			if ($keyword['isNegative']) $keyword['isNegative'] = "true"; else $keyword['isNegative'] = "false";
			// think in micros
			$keyword['maxCpc'] *= EXCHANGE_RATE;
			if (isset($keyword['exemptionRequest'])) {
				// with exemption request
				$soapParameters .= "<newKeywords>
															<exemptionRequest>".$keyword['exemptionRequest']."</exemptionRequest>
															<type>".$keyword['type']."</type>
															<text>".$keyword['text']."</text>
															<negative>".$keyword['isNegative']."</negative>
															<maxCpc>".$keyword['maxCpc']."</maxCpc>
															<language>".$keyword['language']."</language>
															<destinationUrl>".$keyword['destinationUrl']."</destinationUrl>
														</newKeywords>";
			}
			else {
				// without exemption request
				$soapParameters .= "<newKeywords>
															<type>".$keyword['type']."</type>
															<text>".$keyword['text']."</text>
															<negative>".$keyword['isNegative']."</negative>
															<maxCpc>".$keyword['maxCpc']."</maxCpc>
															<language>".$keyword['language']."</language>
															<destinationUrl>".$keyword['destinationUrl']."</destinationUrl>
														</newKeywords>";
			}
		}
		$soapParameters .= "</addKeywordList>";
		// add keywords to the google servers
		$someKeywords = $someSoapClient->call("addKeywordList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":addKeywordList()", $soapParameters);
	    return false;
		}

		// when we have only one keyword return a (one keyword element) array  anyway
		if (isset($someKeywords['addKeywordListReturn']['id'])) {
			$saveArray = $someKeywords['addKeywordListReturn'];
			unset($someKeywords);
			$someKeywords['addKeywordListReturn'][0] = $saveArray;
		}

		// create local objects
		$keywordObjects = array();
		foreach($someKeywords['addKeywordListReturn'] as $someKeyword) {
			$keywordObject = new Keyword ($someKeyword['text'], $someKeyword['id'], $someKeyword['adGroupId'], $someKeyword['type'], $someKeyword['negative'], $someKeyword['maxCpc'] / EXCHANGE_RATE, $someKeyword['minCpc'] / EXCHANGE_RATE, $someKeyword['status'], $someKeyword['language'], $someKeyword['destinationUrl']);
			array_push($keywordObjects, $keywordObject);
		}
		return $keywordObjects;
	}

	// remove keyword on google servers and delete local object
	function removeKeyword(&$keywordObject) {
		// update the google servers
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();
		$soapParameters = "<removeKeyword>
													<adGroupId>".$keywordObject->getBelongsToAdGroupId()."</adGroupId>
													<keywordId>".$keywordObject->getId()."</keywordId>
											 </removeKeyword>";
		// talk to the google servers
		$someSoapClient->call("removeKeyword", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":removeKeyword()", $soapParameters);
	    return false;
		}
		// delete remote calling object
		$keywordObject = @$GLOBALS['keywordObject'];
		unset($keywordObject);
		return true;
	}

	function getAllKeywords($adGroupId) {
	 	global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();
	 	$soapParameters = "<getAllKeywords>
	 												<adGroupId>".$adGroupId."</adGroupId>
	 										 </getAllKeywords>";
	 	// query the google servers for all keywords
	 	$allKeywords = $someSoapClient->call("getAllKeywords", $soapParameters);
	 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllKeywords()", $soapParameters);
	    return false;
		}

		// when we have only one keyword in the adgroup return a (one keyword element) array  anyway
		if (isset($allKeywords['getAllKeywordsReturn']['id'])) {
			$saveArray = $allKeywords['getAllKeywordsReturn'];
			unset($allKeywords);
			$allKeywords['getAllKeywordsReturn'][0] = $saveArray;
		}
		// when there are several keywords in the adgroup just return them normally	(i.e. array of keywords)
		$allKeywordObjects = array();
		if (is_array($allKeywords['getAllKeywordsReturn'])) foreach ($allKeywords['getAllKeywordsReturn'] as $keyword) {
			// create keyword objects
			$keywordObject = new Keyword ($keyword['text'], $keyword['id'], $keyword['adGroupId'], $keyword['type'], $keyword['negative'], $keyword['maxCpc'] / EXCHANGE_RATE , $keyword['minCpc'] / EXCHANGE_RATE , $keyword['status'], $keyword['language'], $keyword['destinationUrl']);
			array_push($allKeywordObjects, $keywordObject);
		}

		return $allKeywordObjects;
	}

	function getAllActiveKeywords($adGroupId) {
	 	global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();
	 	$soapParameters = "<getActiveKeywords>
	 												<adGroupId>".$adGroupId."</adGroupId>
	 										 </getActiveKeywords>";
	 	// query the google servers for the active keywords only
	 	$allKeywords = $someSoapClient->call("getActiveKeywords", $soapParameters);
	 	$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":getAllActiveKeywords()", $soapParameters);
	    return false;
		}

		// when we have only one keyword in the adgroup return a (one keyword element) array  anyway
		if (isset($allKeywords['getActiveKeywordsReturn']['id'])) {
			$saveArray = $allKeywords['getActiveKeywordsReturn'];
			unset($allKeywords);
			$allKeywords['getActiveKeywordsReturn'][0] = $saveArray;
		}
		// when there are several keywords in the adgroup just return them normally	(i.e. array of keywords)
		$allKeywordObjects = array();
		if (is_array($allKeywords['getActiveKeywordsReturn'])) foreach ($allKeywords['getActiveKeywordsReturn'] as $keyword) {
			// create keyword objects
			$keywordObject = new Keyword ($keyword['text'], $keyword['id'], $keyword['adGroupId'], $keyword['type'], $keyword['negative'], $keyword['maxCpc'] / EXCHANGE_RATE , $keyword['minCpc'] / EXCHANGE_RATE , $keyword['status'], $keyword['language'], $keyword['destinationUrl']);
			array_push($allKeywordObjects, $keywordObject);
		}

		return $allKeywordObjects;
	}

	function createKeywordObject($givenAdGroupId, $givenKeywordId) {
		// this will create a local keyword object that we can play with
		global $soapClients;
		$someSoapClient = $soapClients->getKeywordClient();
		// prepare soap parameters
		$soapParameters = "<getKeywordList>
													<adGroupId>".$givenAdGroupId."</adGroupId>
													<keywordIds>".$givenKeywordId."</keywordIds>
											 </getKeywordList>";
		// execute soap call
		$someKeyword = $someSoapClient->call("getKeywordList", $soapParameters);
		$soapClients->updateSoapRelatedData(extractSoapHeaderInfo($someSoapClient->getHeaders()));
		if ($someSoapClient->fault) {
	  	pushFault($someSoapClient, $_SERVER['PHP_SELF'].":createKeywordObject()", $soapParameters);
	    return false;
		}
		// invalid ids are silently ignored. this is not what we want so put out a warning and return without doing anything.
		if (sizeOf($someKeyword) == 0) {
			if (!SILENCE_STEALTH_MODE) echo "<br /><b>APIlity PHP library => Warning: </b>Invalid Keyword ID or AdGroup ID. No Keyword found.";
			return false;
		}
		// populate class attributes
		$text = $someKeyword['getKeywordListReturn']['text'];
		$id = $someKeyword['getKeywordListReturn']['id'];
		$belongsToAdGroupId = $someKeyword['getKeywordListReturn']['adGroupId'];
		$type = $someKeyword['getKeywordListReturn']['type'];
		$isNegative = $someKeyword['getKeywordListReturn']['negative'];
		$minCpc = $someKeyword['getKeywordListReturn']['minCpc'] / EXCHANGE_RATE;
		$maxCpc = $someKeyword['getKeywordListReturn']['maxCpc'] / EXCHANGE_RATE;
		$status = $someKeyword['getKeywordListReturn']['status'];
		$language = $someKeyword['getKeywordListReturn']['language'];
		$destinationUrl = $someKeyword['getKeywordListReturn']['destinationUrl'];

		// create object
		$keywordObject = new Keyword ($text, $id, $belongsToAdGroupId, $type, $isNegative, $maxCpc, $minCpc, $status, $language, $destinationUrl);
		return $keywordObject;
	}
?>