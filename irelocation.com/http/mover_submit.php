<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>The iRelocation Network - Find A Mover - Thank You!</title>
<SCRIPT LANGUAGE="JavaScript">
<!-- Overture Services, Inc
var pm_tagname = "conversionTag.txt";
var pm_tagversion = "1.4";
window.pm_customData = new Object();
window.pm_customData.segment = "name=conversion, transId=, amount=7.2";
// -->
</SCRIPT>
<?
$DO_NOT_TRACK_PAGE_VIEW=true;
include_once("inc_session.php");

#
# Clean up fields
#
$sess_fname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($fname))));
$sess_lname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($lname))));
$sess_email=trim(str_replace(" ","",str_replace("\\","",str_replace("'","",strtolower($email)))));
$sess_phone=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone));
$sess_phone2=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone2));

$from_zip=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$from_zip));
$to_zip=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$to_zip));
$comments=trim(str_replace("'","",strtolower($comments)));

#
# Check required fields
#
if($bedrooms=='XX') $msg="Please select your number of bedrooms!";

//validate the zip codes and get the city/state's while you're at it!
$link = mysql_connect("localhost", "admin2", "triad8920") or die("Could not connect to database : " . mysql_error());
mysql_select_db("movingdirectory") or die("Could not select database");
$result = mysql_query("select city,state from zip_codes where zip='$from_zip'") or die("Query failed : " . mysql_error());
$row=mysql_fetch_array($result);
if(mysql_num_rows($result)==0) $msg="Your moving from zip code was not found! Please use our zip code finder to locate an alternate.";
else{$from_city=$row["city"]; $from_state=$row["state"];}

$result = mysql_query("select city,state from zip_codes where zip='$to_zip'") or die("Query failed : " . mysql_error());
$row=mysql_fetch_array($result);
if(mysql_num_rows($result)==0) $msg="Your moving to zip code was not found! Please use our zip code finder to locate an alternate.";
else{$to_city=$row["city"]; $to_state=$row["state"];}

if(strlen($sess_phone)<10 || strlen($sess_phone)>11) $msg="Please enter a valid phone number including the area code!";
if(substr_count($sess_email,'@')==0) $msg="Please enter a valid email address!";
if($sess_fname=='' || $sess_lname=='' || $sess_email=='' || $sess_phone=='') $msg="Please fill in all required fields!";

if($msg!='')
{
  include("mover.php");
  exit;
}

#
# Get original campaign, referer, & keyword of this user
#
$rs=new mysql_recordset("select * from stats_pageviews where sid='$sid' and (campaign!='' or referer!='' or keyword!='')");
if($rs->fetch_array())
{
  $campaign=$rs->myarray["campaign"];
  $referer=$rs->myarray["referer"];
  $keyword=$rs->myarray["keyword"];
}

if($campaign!='')
{
  $campaign=$site_id."_".$campaign;
}
else
{
  $campaign=$site_id;
}

#
# Build the rooms field
#

//ADD SPACES AFTER COMMA'S LATER ONCE USING NEW DB
$rooms=$bedrooms." ";
if($attic=='1' || $basement=='1' || $den=='1' || $dining=='1' || $kitchen=='1' || $living=='1' || $office=='1' || $patio=='1' || $play=='1' || $shed=='1') $rooms.="with ";
if($attic=='1') $rooms.="attic,";
if($basement=='1') $rooms.="basement,";
if($den=='1') $rooms.="den,";
if($dining=='1') $rooms.="dining,";
if($kitchen=='1') $rooms.="kitchen,";
if($living=='1') $rooms.="living,";
if($office=='1') $rooms.="office,";
if($patio=='1') $rooms.="patio,";
if($play=='1') $rooms.="play,";
if($shed=='1') $rooms.="shed,";
$rooms=substr($rooms,0,-1);

$move_date=$move_month."-".$move_day."-".$move_year;

#
# Enter lead in database
#

  //$rs=new mysql_recordset("insert into leads_mover (received,fname,lname,email,phone,phone2,from_city,from_state,from_zip,to_city,to_state,to_zip,move_date,type_of_move,type_of_home,stairs,elevator,rooms,comments,campaign,referer,keyword) values (NOW(),'$sess_fname','$sess_lname','$sess_email','$sess_phone','$sess_phone2','$from_city','$from_state','$from_zip','$to_city','$to_state','$to_zip','$move_date','$type_of_move','$type_of_home','$stairs','$elevator','$rooms','$comments','$campaign','$referer','$keyword')");
  
  //***********************************************
  //TEMPORARILY ENTER THE MOVING LEADS INTO THE OLD DATABASE
  $link = mysql_connect("localhost", "admin2", "triad8920") or die("Could not connect to database : " . mysql_error());
  mysql_select_db("movingdirectory") or die("Could not select database");
  $move_date=date("Y-m-d",mktime(0,0,0,$move_month,$move_day,$move_year));
  if($stairs==''){$stairs="no";}
  if($elevator==''){$elevator="no";}
  $mycomments="Type of Move: ".ucwords($type_of_move)." Move\nType of Home: ".ucwords($type_of_home)."\nFurnished Rooms: $rooms\nElevator: $elevator\nStairs: $stairs\n\nComments/Unique Items: $comments";
  $query = "insert into quotes (cat_id,name,email,phone_home,contact,est_move_date,origin_city,origin_state,origin_zip,destination_city,destination_state,comments,ready_to_send,source) values ('2','$sess_fname $sess_lname','$sess_email','$sess_phone','phone','$move_date','$from_city','$from_state','$from_zip','$to_city','$to_state','$mycomments','1','irelo_big')";
  $result_cities = mysql_query($query) or die("Query failed : " . mysql_error());
  //***********************************************
  
  
#
# Direct the user to the thank you page with a close button
#
$sess_mover_complete=true;
?>
</head>

<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#C0C0C0">
<?
if($sess_realtor_complete!=true && $sess_mortgage_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 324px; top: 318px" id="layer3">
        <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
                <tr>
                        <td width="71%">
                        <img border="0" src="images/find_realtor.gif" width="100" height="12"></td>
                </tr>
                <tr>
                        <td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Get
                        CASH BACK on the sale of your home when you find
                        a real estate agent through our service!</font></td>
                </tr>
                <tr>
                        <td width="71%" height="23">
                        <p align="right"><font face="Tahoma" size="2">
                        <a href="realtor.php" style="text-decoration: none">
                        <font color="#000000">start now &gt;</font></a></font></p>
                        </td>
                </tr>
        </table>
</div>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 64px; top: 318px" id="layer3">
        <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
                <tr>
                        <td width="71%">
                        <img border="0" src="images/find_mortgage.gif" width="114" height="12"></td>
                </tr>
                <tr>
                        <td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Save $300
                        with a reduced closing cost credit! We match you with the nation&#39;s leading
                        lenders who can get you great rates!</font></td>
                </tr>
                <tr>
                        <td width="71%" height="23">
                        <p align="right"><font face="Tahoma" size="2">
                        <a href="mortgage.php" style="text-decoration: none">
                        <font color="#000000">start now &gt;</font></a></font></p>
                        </td>
                </tr>
        </table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
        <hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
        <hr>&nbsp;</div>
<?
}
elseif($sess_realtor_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 198px; top: 317px" id="layer3">
        <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
                <tr>
                        <td width="71%">
                        <img border="0" src="images/find_realtor.gif" width="100" height="12"></td>
                </tr>
                <tr>
                        <td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Get
                        CASH BACK on the sale of your home when you find
                        a real estate agent through our service!</font></td>
                </tr>
                <tr>
                        <td width="71%" height="23">
                        <p align="right"><font face="Tahoma" size="2">
                        <a href="realtor.php" style="text-decoration: none">
                        <font color="#000000">start now &gt;</font></a></font></p>
                        </td>
                </tr>
        </table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
        <hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
        <hr>&nbsp;</div>
<?
}
elseif($sess_mortgage_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 198px; top: 317px" id="layer3">
        <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
                <tr>
                        <td width="71%">
                        <img border="0" src="images/find_mortgage.gif" width="114" height="12"></td>
                </tr>
                <tr>
                        <td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Save $300
                        with a reduced closing cost credit! We match you with the nation&#39;s leading
                        lenders who can get you great rates!</font></td>
                </tr>
                <tr>
                        <td width="71%" height="23">
                        <p align="right"><font face="Tahoma" size="2">
                        <a href="mortgage.php" style="text-decoration: none">
                        <font color="#000000">start now &gt;</font></a></font></p>
                        </td>
                </tr>
        </table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
        <hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
        <hr>&nbsp;</div>
<?
}
?>
<div style="position: absolute; width: 520px; height: 139px; z-index: 1; left: 41px; top: 144px" id="layer2">
        <p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Your request
        to get a moving quote has been received.&nbsp; You can expect a quick response within
        the next 24 hours, during normal business days.&nbsp;
        <?if($from_state==$to_state){?>
        Since you're moving within the same state, we will be forwarding your information to 5 certifed moving companies
        that service your area. Our network of intrastate movers have great reputation in the industry, just like our larger
        interstate moving partners.
        <?}else{?>This program was specifically
        designed by The iRelocation Network with North American Van Lines, Wheaton Van Lines,
        Atlas Van Lines, and Allied Van Lines to provide consumers
        with one-stop access to the best moving companies nationwide while providing them with great service at a reasonable price.
        <?}?>
        </font></p>
        <p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Additional information
        will be emailed to you at:<b><br>
        <?echo $sess_email;?></b> <br>
        within the next 10 minutes, so please check your email!</font></p>
</div>
<div style="position: absolute; width: 162px; height: 43px; z-index: 1; left: 219px; top: 85px" id="layer1">
        <font face="Tahoma" size="6" color="#6AC877">Thank You!</font></div>
<div style="position: absolute; width: 129px; height: 29px; z-index: 1; left: 241px; top: 431px" id="layer5">
        <b><font face="Tahoma" color="#6AC877">
        <a href="#" onclick="window.close(); return false;"><font color="#6AC877">close
        window</font></a></font></b></div>
<table border="0" width="600" cellspacing="0" cellpadding="0" height="500" bgcolor="#FFFFFF">
        <tr>
                <td height="53">
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                        <tr>
                                <td><a target="_blank" href="http://www.irelocation.com">
                                <img border="0" src="images/logo_small.gif" width="144" height="53"></a></td>
                                <td>
                                <img border="0" src="images/slogan.gif" width="341" height="13" align="right"></td>
                        </tr>
                </table>
                </td>
        </tr>
        <tr>
                <td background="images/bar.gif" height="18">
                <img border="0" src="images/clear.gif" width="1" height="1"></td>
        </tr>
        <tr>
                <td>
                <table border="0" width="100%" id="table3" cellspacing="0" cellpadding="0" height="100%">
                        <tr>
                                <td width="25">&nbsp;</td>
                                <td bgcolor="#E1E3E0" width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td>&nbsp;</td>
                                <td bgcolor="#E1E3E0" width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td width="25">&nbsp;</td>
                        </tr>
                </table>
                </td>
        </tr>
        <tr>
                <td height="20">
                <table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0" height="20">
                        <tr>
                                <td bgcolor="#E1E3E0" width="25">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td bgcolor="#BEC3BD">
                                <p align="center">
                                <img border="0" src="images/clear.gif" width="1" height="1"><b><font face="Tahoma" color="#5E5E5E" size="1">The
                                iRelocation Network � 2004-<?echo Date("Y");?> � Privacy Policy � Terms &amp; Conditions</font></b></p>
                                </td>
                                <td width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td bgcolor="#E1E3E0" width="25">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                        </tr>
                </table>
                </td>
        </tr>
        <tr>
                <td height="1">
                <img border="0" src="images/clear.gif" width="1" height="1"></td>
        </tr>
        <tr>
                <td height="3">
                <table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" height="3">
                        <tr>
                                <td bgcolor="#E7E0DF" width="25">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td bgcolor="#CBBDB9">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td width="1">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                                <td bgcolor="#E7E0DF" width="25">
                                <img border="0" src="images/clear.gif" width="1" height="1"></td>
                        </tr>
                </table>
                </td>
        </tr>
</table>

</body>

</html>