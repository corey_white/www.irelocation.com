<?php 
/* 
************************FILE INFORMATION**********************
* File Name:  stats.php
**********************************************************************
* Description:  quick redirect for the server stats page - simpler URL to remember
**********************************************************************
* Creation Date:  1/15/10
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

header("location: http://www.irelocation.com/nuadmin/minutereport");

?>