<?
	$keyword = $_REQUEST['key'];	
	$source = $_REQUEST['source'];
	
	function getCar($keyword,$ana=false)
	{
		$roots = array("car","vehicle","auto");
		$result = "car";
		foreach($roots as $r)
		{
			if (substr_count(strtolower($keyword),$r) > 0)
			{
				$result = $r;
			}
		}
		
		if ($ana)
		{
			if ($result == "auto")
				return "an auto";
			else
				return "a $result";
		}
		else
			return $result;
	}

	function getShipping($keyword,$tense="")
	{
		$roots = array("shipp","mov","transport");
		$root = "ship";
		
		foreach($roots as $r)
		{
			if (substr_count(strtolower($keyword),$r) > 0)
			{
				$root = $r;
				break;
			}
		}
		
		switch($tense)
		{
			case "ing":
			case "er":
			case "ers":
			case "ed":
				return $root.$tense;
			case "":
				if ($root=="shipp") return "ship";
				if ($root=="mov") return "move";
				else return $root;
			default:
				return "shipping";
		}
		
	}

?>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title><?php print ucwords(strtolower($keyword));?></title>
<meta name="keywords" content="<?php print $keyword;?>"/>
<meta name="description" content=" The best Site on the Web for <?= ucwords(getCar($keyword)." ".getShipping($keyword,"ing")) ?> Information & Resources "/>
</head>
<body>

<div align="left">
<table width="562" border="0" align="left" bgcolor="#DFE0FF">
      <tr>
        <td width="499">
			<font face='verdana' color="navy" size="2">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?= ucwords(getShipping($keyword,"ing")) ?> your <?= getCar($keyword) ?> has never been easier! AutoShipping.com understands how important your <?= getCar($keyword) ?> is to you and your family. With this in mind, we provided up to 7 quotes from only licensed and bonded <?= getCar($keyword) ?> <?= getShipping($keyword,"ers") ?>. By comparing rates and <?= getShipping($keyword,"ing") ?> services you can be certain to find the <?= getShipping($keyword,"er") ?> that best meets your specific needs.</p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         Our service providers have access to state of the art <?= getCar($keyword) ?> <?= getShipping($keyword,"ing") ?> equipment including both open and closed trailers, GPS tracking, and
have been educated on the handling of your <?= getCar($keyword) ?>. This ensures that your <?= getCar($keyword) ?> will be <?= getShipping($keyword,"ed") ?> in a safe, secure manner, within a reasonable amount of time for a fair price.</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         Auto Shipping’s reliable service makes us the right choice when shopping for <?= getCar($keyword,true) ?> <?= getShipping($keyword,"er") ?> because customer service is more than what we say- it's what we do!  </p>
		 </font>
		</td>
	</tr>
</table>
</div>
