<?php
    @session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
<title><?= $pageTitle ?></title>
<meta name="description" content="<?= $metaDescription ?>" />
<meta name="keywords" content="<?= $metaTags ?>" />
<link href="/irelocation/styles.css" rel="stylesheet" type="text/css" />
<!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35525013-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body class="index">
<div id="frame">
    <div id="header">
        <a href="http://www.irelocation.com"><img src="http://www.irelocation.com/irelocation/images/logo.png" alt="i-relocation network" width="168" height="78" style="padding-left:25px; padding-top:15px; float:left" /></a>
    </div>
    <div id="content">
        <div id="nav">
            <div id="nav-sub">
                <ul>
                    <li><a href="http://www.irelocation.com/company.php" title="Who We Are">Who We Are</a></li>
                    <li><a href="http://www.irelocation.com/leads.php" title="Lead Generation">Lead Generation</a></li>
                    <li><a href="http://www.irelocation.com/auto_transport_leads.php" title="Auto Transport Leads">Auto Transport</a></li>
                    <li><a href="http://www.irelocation.com/moving_leads.php" title="Moving Leads">Moving</a></li>
                    <li><a href="http://www.irelocation.com/medical_alert_leads.php" title="Medical Alert Systems Leads">Medical Alert</a></li>
                    <li><a href="http://www.irelocation.com/security_leads.php" title="Home Security Leads">Home Security</a></li>
                    <li><a href="http://www.irelocation.com/contact.php" title="Contact Us">Contact Us</a></li>
                </ul>
            </div>
        </div>