<?php
    $error_msg = $_REQUEST["msg"];
    $msgs["name"] = "Please enter your Full Name.";
    $msgs["phone"] = "Please enter your Phone Number.";
    $msgs["company"] = "Please enter your Company Name.";
    $msgs["email"] = "Please enter your Email Address.";
    $msgs["lead_type"] = "Please select your type of lead.";
    $msgs["thankyou"] = "Thank you. We will contact you shortly.";
    $msgs["verify"] = "Verification code is invalid.";
                
    $msg = $msgs[$error_msg];
    
    extract($_POST);
    $page = $_SERVER['PHP_SELF'];
    
    // is the form ready to check and send            
    if ($send == "yes") { 
        
        // error checking
        if ($name == "") {
            header("Location: ".$page."?msg=name&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else if ($email == "") {
            header("Location: ".$page."?msg=email&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else if ($company == "") {
            header("Location: ".$page."?msg=company&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else if ($phone == "") {
            header("Location: ".$page."?msg=phone&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else if ($lead_type == "") {
            header("Location: ".$page."?msg=lead_type&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else if ($_SESSION['answer'] != $verify) {
            header("Location: ".$page."?msg=verify&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
            
        } else {
            $to = "katrina@irelocation.com, travis@irelocation.com, vsmith@irelocation.com, markb@irelocation.com";
            //$to = "tory@irelocation.com";
            $subject = "iRelocation PPC Lead Form";
            $message = "Name: ".$name."\n\n";
            $message .= "Company: ".$company."\n\n";
            $message .= "Email: ".$email."\n\n";
            $message .= "Phone: ".$phone."\n\n";
            $message .= "Lead Type: ".$lead_type."\n\n";
            $message .= "Comment: ".$comment;
            mail($to, $subject, $message, "From: $email");
            header("Location: thank_you.php");
            exit;
        }
    }
    //create numbers for human verification
    $q1 = rand(0,10);    
    $q2 = rand(0,10);
    $_SESSION['answer'] = $q1 + $q2;
    ?>
    <h2>Get Started Today!</h2>
    <form id="form1" method="post" action="">
        <input name="send" type="hidden" value="yes" />
        <table width="100%" border="0" cellpadding="1" cellspacing="0">
            <?
            $errmsg='
            <tr>
              <td align="center" colspan="2"><font color="red">'.$msg.'</font></td>
            </tr>
            ';
            if($msg!="")
            {
                echo $errmsg;
            }
            ?>
            <tr>
                <td width="51%"><p>Name: <span class="orange">* </span></p></td>
                <td width="49%"><label>
                    <input name="name" type="text" id="name" value="<?= $_REQUEST["name"]; ?>" />
                </label></td>
            </tr>
            <tr>
                <td><p>Company: <span class="orange">*</span> </p></td>
                <td><input name="company" type="text" id="company" value="<?= $_REQUEST["company"]; ?>" /></td>
            </tr>
            <tr>
                <td><p>Email: <span class="orange">*</span> </p></td>
                <td><input name="email" type="text" id="email" value="<?= $_REQUEST["email"]; ?>" />          </td>
            </tr>
            <tr>
                <td><p>Phone: <span class="orange">*</span> </p></td>
                <td><input name="phone" type="text" id="phone" value="<?= $_REQUEST["phone"]; ?>" /></td>
            </tr>
            <tr>
                <td><p>Type of Leads: </p></td>
                <td><label>
                <select name="lead_type">
                    <option value="Select" <? if($lead_type=='Select'){echo " selected";}?>>Select</option>
                    <option value="Car Transport" <? if($lead_type=='Car Transport'){echo " selected";}?>>Auto Transport</option>
                    <option value="Moving" <? if($lead_type=='Moving'){echo " selected";}?>>Moving</option>
                    <option value="Home Security" <? if($lead_type=='Home Security'){echo " selected";}?>>Home Security</option>
                    <option value="Medical Alert" <? if($lead_type=='Medical Alert'){echo " selected";}?>>Medical Alert</option>
                    <?php /*
                    <option value="Insurance" <? if($lead_type=='Insurance'){echo " selected";}?>>Insurance</option>
                    <option value="Solar" <? if($lead_type=='Solar'){echo " selected";}?>>Solar</option>
                    <option value="Home Senior Care" <? if($lead_type=='Home Senior Care'){echo " selected";}?>>Home Senior Care</option>
                    <option value="Other" <? if($lead_type=='Other'){echo " selected";}?>>Other</option>
                     * 
                     */
                     ?>
                </select>
            </label></td>
            </tr>
            <tr>
                <td valign="top"><p>Human Verification:</p></td>
                <td valign="top"><span style="color: #EFA163;">What is <?=$q1 ?> + <?=$q2 ?>?</span> <input name="verify" type="text" id="verify" value="" style="width: 25px;" /></td>
            </tr>
            <tr>
                <td colspan="2" valign="bottom"><input name="submit" type="image" src="irelocation/images/contact_btn2.png" style="width:221px;height:66px;margin:0 auto;display:block" /></td>
            </tr>
        </table>
    </form>  