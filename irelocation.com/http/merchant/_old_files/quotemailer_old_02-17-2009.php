<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for MERCHANT campaign
**********************************************************************
* Creation Date:  1/30/09 12:32 PM
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOTES:  This set of scripts in this folder are cloned from the moving folder and modified as needed.

*/

//include library functions
include_once "../inc_mysql.php";
include_once "../cronlib/.functions.php";
include "CompanyClasses.php";

updateCronStatus("merchant","started");

define(LIVE,true);//signal this IS a live quotemailer.


#
# Delete Test Leads.  //-- this funciton has not been altered
#
$sql="delete from movingdirectory.leads_merchant where ( (lower(first_name) like '%test%')  && (lower(last_name) like '%test%') ) or email = 'test@test.com' ";
$body .=  "Deleting Test Leads: $sql <br />\n<br />\n";
$rs=new mysql_recordset($sql);


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");

$sql="select * from movingdirectory.leads_merchant where (received='' or received='0') and email not like '%tessting@gmail.com%'";
$body .=  "SELECT FOR CLEANING UP ENTRIES: $sql <br />\n<br />\n";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{

/* OLD CLEANUP
	$newfirstname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["first_name"])));
	$newlastname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["last_name"])));
	$newphone=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone"]);
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newcompany=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["company"]);
	$newzip=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["zip"]);
	$newcurrently_accept_cards=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["currently_accept_cards"]);
	$newreceive_cc_orders=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["receive_cc_orders"]);
	$newcard_types=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["card_types"]);
	$newtypical_value=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["typical_value"]);
	$newcard_volume=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["card_volume"]);
	$newcard_terminal_info=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["card_terminal_info"]);
	$newtime_frame=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["time_frame"]);
	$newcomments=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["comments"]);
	$newsource=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["source"]);
*/
	
	$newfirstname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["first_name"])));
	$newlastname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["last_name"])));
	$newphone=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone"]);
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newcompany=mysql_real_escape_string($rs->myarray["company"]);
	$newzip=mysql_real_escape_string($rs->myarray["zip"]);
	$newcurrently_accept_cards=mysql_real_escape_string($rs->myarray["currently_accept_cards"]);
	$newreceive_cc_orders=mysql_real_escape_string($rs->myarray["receive_cc_orders"]);
	$newtypical_value=mysql_real_escape_string($rs->myarray["typical_value"]);
	$newcard_volume=mysql_real_escape_string($rs->myarray["card_volume"]);
	$newtime_frame=mysql_real_escape_string($rs->myarray["time_frame"]);
	$newcomments=mysql_real_escape_string($rs->myarray["comments"]);
	$newsource= mysql_real_escape_string($rs->myarray["source"]);
	
	$update_sql = "update movingdirectory.leads_merchant set first_name='$newfirstname', last_name='$newlastname', email='$newemail', phone='$newphone', company='$newcompany', zip='$newzip', currently_accept_cards='$newcurrently_accept_cards', receive_cc_orders='$newreceive_cc_orders', typical_value='$newtypical_value', card_volume='$newcard_volume', time_frame='$newtime_frame', comments='$newcomments', received = '$mytime', source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
	$body .=  "UPDATE SQL $update_sql<br />\n<br />\n";
	$rs2=new mysql_recordset($update_sql);
}	


/*  begin processing */


$mytime = substr($mytime,0,10); //YYYYMMDDHH

//select all leads that are ready to go, and moving or internationals.
$sql = "select * from movingdirectory.leads_merchant where ready_to_send = 1 AND received like '$mytime%' ";

// TEMP SQL PULL that replaces the above for testing
#$sql = "select * from movingdirectory.leads_merchant where quote_id = XYZZY ";

$body .=  "SELECTING ALL QUOTES: $sql <br />\n";

$rs = new mysql_recordset($sql);
$count = 0;

$body .=  "BEGIN LOOP OF QUOTES<br />\n";

while ($rs->fetch_array()) { //-- BEGIN LOOP OF QUOTES

	$body .=  "<br />\n";
	$body .=  "first_name: " . $rs->myarray["first_name"] . "<br />\n";
	$body .=  "last_name: " . $rs->myarray["last_name"] . "<br />\n";
	$body .=  "phone: " . $rs->myarray["phone"] . "<br />\n";
	$body .=  "email: " . $rs->myarray["email"] . "<br />\n";
	$body .=  "company: " . $rs->myarray["company"] . "<br />\n";
	$body .=  "zip: " . $rs->myarray["zip"] . "<br />\n";
	
	$body .=  "currently_accept_cards: " . $rs->myarray["currently_accept_cards"] . "<br />\n";
	$body .=  "receive_cc_orders: " . $rs->myarray["receive_cc_orders"] . "<br />\n";
	$body .=  "typical_value: " . $rs->myarray["typical_value"] . "<br />\n";
	$body .=  "card_volume: " . $rs->myarray["card_volume"] . "<br />\n";
	$body .=  "time_frame: " . $rs->myarray["time_frame"] . "<br />\n";
	
	$body .=  "comments: " . $rs->myarray["comments"] . "<br />\n";
	$body .=  "<br />\n";
	

	$count++;
		
		$data = new MerchantQuoteData($rs->myarray);  //-- insantiate the class
		$body .=  "<br />\n<br />\n<br />\n<br />\n";
		$body .=  "=====================================================<br />\n";
		$body .=  "Processing Merchant-Quote<br />\n<br />\n";
		$body .=  "=====================================================<br />\n";
		
		/*-- process:
				THERE ARE 2 TYPES OF CLIENTS
						1. those that get all leads
						2. those that only get some leads, based on a daily quota
						
				EACH LEAD WILL GO TO 4 CLIENTS
					3 slots for those that get all leads
					The 4th slot will be one of the daily quota clients, if any are below their daily limit

				So,
				1. DETERMINE if there are any Type 2 clients that need to get leads, if not, then need to pull 4 Type 1 leads.
				2. Determine how many Type 1 clients will be getting leads.
				
	-	Pull all pull_order=2
	-	Loop thru them, checking if they have received all their leads for the day
	-	IF they still need leads
		-	then add to array
		-	update movingdirectory.leads_merchant set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	-	IF array is empty
		-	then all_count = 4
	-	ELSE
		-	all_count = 3
		-	shuffle array
		-	send lead to first lead_id in array
		
	-	Pull either 3 or 4 leads for pull_order=1 
	-	loop thru and send them each a lead
	-	update movingdirectory.leads_merchant set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??

		*/
		
		//-- $body .=  "<br />\nXXXXX<br />\n";
		//-- $body .=  "<br />\n";
		
		//-- define some vars based on the current quote_id
		$quote_id = $rs->myarray["quote_id"];
		$sent_to_leads = array();
		$campaign_month = date("Ym");
		$current_date = date("Ymd");

		$lead_count = 0;
		
		//-- output a little info
		$body .=  "<br />\nquote_id = $quote_id<br />\n";
		$body .=  "<br />\ncampaign_month = $campaign_month<br />\n";
		$body .=  "<br />\nlead_count = $lead_count<br />\n";
		$body .=  "<br />\ncampaign_month = $campaign_month<br />\n";
		$body .=  "<br />\ncurrent_date = $current_date<br />\n";
		$body .=  "<br />\n";

		
		//-- process daily quota clients
		$body .=  "<br />\nPROCESSING DAILY QUOTA CLIENTS<br />\n";
		
		$site_id = "merch";
		$body .=  "<br />\nsite_id = $site_id<br />\n<br />\n";
		
		//-- get the quota companies
		$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
			(select email from movingdirectory.rules 
				where lead_id = lf.lead_id) as 'email',
			lf.lead_id,
			format_type,
			lf.cat_id, 
			if(lead_body < 0,
				(select lead_body from movingdirectory.lead_format 
					where lead_id = lf.lead_body),
				lf.lead_body) as 'lead_body' 
		from
			movingdirectory.lead_format as lf 
			join 
			movingdirectory.directleads as d
			on d.lead_id = lf.lead_id 
			join 
			movingdirectory.campaign as ca
			on d.lead_id = ca.lead_id	

		where	
			ca.active
			and lf.cat_id = 6 
			and ca.month = '$campaign_month'
			and ca.site_id = 'merch'
			and ca.pull_order = 2
		group by	
			ca.lead_id
		order by rand()";
		
		$body .=  "SQL FOR TYPE 2 (QUOTA) CLIENTS:<br />\n$comp_sql<br />\n<br />\n";
		
		$comp_rs = new mysql_recordset($comp_sql);
		
		$quota_arr = array();
		
		//-- Loop through those that have been pulled and if any still need leads for today, stick them in an array.
		$body .=  "LOOPING THRU QUOTA CLIENTS<br />\n";
		while ($comp_rs->fetch_array()) {
			# $XXXX = $comp_rs->myarray["XXXX"];
			$lead_id = $comp_rs->myarray["lead_id"];
			$leads_per_day = $comp_rs->myarray["leads_per_day"];
			$body .=  "$lead_id can get $leads_per_day per day<br />\n";
			
			$w_sql = "select * from movingdirectory.leads_merchant where lead_ids like '%$lead_id%' and left(received,8) = '$current_date' ";
			$rs_w = new mysql_recordset($w_sql);
			$w_count = $rs_w->rowcount();
			
			$body .=  "Daily lead count for $lead_id = $w_count<br />\n";
			
			//-- if still need leads, add them to array
			if ( $w_count < $leads_per_day ) {
				$body .=  "------- Added $lead_id to array<br />\n";
				$quota_arr[] = $lead_id;
			} 


		}
		
			$body .=  "QUOTA ARRAY AFTER CHECKING DAILY LEADS TOTALS:<br />\n<pre>";
			$body .= print_r($quota_arr, true);
			$body .=  "</pre><br />\n";

			//-- HERE IS WHERE WE CAN REMOVE LEAD_IDS FOR THOSE THAT ONLY WANT CERTAIN CRITERIA FOR LEADS
			$body .=  "rs->myarray[receive_cc_orders] = " . $rs->myarray["receive_cc_orders"] . "<br />\n";
			
			if ( $rs->myarray["receive_cc_orders"] != "In Person" ) {
				$body .=  "NOT an In Person lead, checking for 1664 in the Quota Array<br />\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1664" ) {
						unset($quota_arr[$key]);
						$body .=  "REMOVING 1664 FROM THE ARRAY<br />\n";
					}
				}
			} 

			$body .=  "QUOTA ARRAY AFTER CHECKING FOR \"IN PERSON\" LEADS:<br />\n<pre>";
			$body .= print_r($quota_arr, true);
			$body .=  "</pre><br />\n";

		
			//-- if there are no lead_ids in the array, set all_count to 4
			if ( sizeof($quota_arr) == 0 ) {
				$all_count = 4;
				
			} else { //-- else seet all_count = 3, shuffle array, send lead to first lead_id
				$all_count = 3;
				shuffle($quota_arr);
				$body .=  "QUOTA ARRAY AFTER SHUFFLING:<br />\n<pre>";
				$body .= print_r($quota_arr,true);
				$body .=  "</pre><br />\n";

				$Qlead_id = $quota_arr[0];
				
				$body .=  "SELECTING $Qlead_id for the quota lead<br />\n<br />\n";
				
				//-- get lead data for just the one lead_id
				$lead_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
					(select email from movingdirectory.rules 
						where lead_id = lf.lead_id) as 'email',
					lf.lead_id,
					format_type,
					lf.cat_id, 
					if(lead_body < 0,
						(select lead_body from movingdirectory.lead_format 
							where lead_id = lf.lead_body),
						lf.lead_body) as 'lead_body' 
				from
					movingdirectory.lead_format as lf 
					join 
					movingdirectory.directleads as d
					on d.lead_id = lf.lead_id 
					join 
					movingdirectory.campaign as ca
					on d.lead_id = ca.lead_id	
		
				where	
					ca.active
					and lf.cat_id = 6 
					and ca.month = '$campaign_month'
					and ca.site_id = 'merch'
					and ca.pull_order = 2
					and ca.lead_id = $Qlead_id
				group by	
					ca.lead_id";
					
				$body .=  "GETTING SINGLE LEAD_ID FOR QUOTA SLOT: $lead_sql <br />\n<br />\n";
				
				$leads_rs = new mysql_recordset($lead_sql);
				$leads_rs->fetch_array();
		
$body .=  "LEADS ARRAY<pre>";
$body .= print_r($leads_rs->myarray,true);
$body .=  "</pre><br />\n";

				$lead_count++;
				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$body .=  "<br />\nINTERNAL TEST - NOT BEING SENT<br />\n";
				} else {
					$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
					$body .=  "<br />\nSent to Processing (\$data->process)<br />\n";
				}
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				$body .=  "<br />\nSent to Finishing (\$data->procfinishLeadess)<br />\n";
*/
				
				$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
				$body .=  "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
				$body .=  "<br />\n<br />\n";

			}
			


			//-- Now, send the lead to the Type 1 pull order leads
			$body .=  "<br />\nPROCESSING CLIENTS THAT GET ALL LEADS<br />\n";
			
			$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
				(select email from movingdirectory.rules 
					where lead_id = lf.lead_id) as 'email',
				lf.lead_id,
				format_type,
				lf.cat_id, 
				if(lead_body < 0,
					(select lead_body from movingdirectory.lead_format 
						where lead_id = lf.lead_body),
					lf.lead_body) as 'lead_body' 
			from
				movingdirectory.lead_format as lf 
				join 
				movingdirectory.directleads as d
				on d.lead_id = lf.lead_id 
				join 
				movingdirectory.campaign as ca
				on d.lead_id = ca.lead_id	
	
			where	
				ca.active
				and lf.cat_id = 6 
				and ca.month = '$campaign_month'
				and ca.site_id = 'merch'
				and ca.pull_order = 1
			group by	
				ca.lead_id
			order by rand()
			limit  $all_count ";
			
			$body .=  "ALL LEADS COMPANIES SQL:<br />\n$comp_sql<br />\n";
			
			$lead_array = new mysql_recordset($comp_sql);
			
			//-- loop through and send out lead
			while ($lead_array->fetch_array()) {
				# $XXXX = $lead_array->myarray["XXXX"];
				$lead_id = $lead_array->myarray["lead_id"];
				
				$lead_count++;

				$body .=  "<b>PROCESSING COMPANY $lead_count: $lead_id</b><br />\n";

				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$body .=  "<br />\nINTERNAL TEST - NOT BEING SENT<br />\n";
				} else {
					$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
					$body .=  "<br />\nSent to Processing (\$data->process)<br />\n";
				}
				
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				$body .=  "<br />\nSent to Finishing (\$data->procfinishLeadess)<br />\n";
*/
				
				$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
				$body .=  "sent to " . $lead_array->myarray['lead_id'] . "<br />\n";
				$body .=  "<br />\n<br />\n";
				
			}
	

		
		
		$body .=  "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br />\n";
		foreach($sent_to_leads as $key => $value){
			$body .=  "Lead: $key sent to: $value<br />\n";
		}

		//-- Hopefully this is where it needs to be
		$data->finishLead(); //-- update ready_to_send = 2
		$body .=  "<br />\nSent to Finishing (\$data->procfinishLeadess)<br />\n";
	
	
} //-- end the while loop (needs to stay)


/* ===== we can leave this ===== */
if ($count > 0) {
	$body .=  $count." leads processed.";
} else {
	$body .=  "no leads processed.";
}
	
	

echo  $body;
mail("rob@irelocation.com","Merchant Quotemailer Output","$body");

#mail("rob@irelocation.com","Merchant Quotemailer Ran","Merchant Quotemailer Ran");

/* turn on for trouble shooting
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r";
$mail_body = ereg_replace("<br />\n","",$body);
mail("rob@irelocation.com","**NEW** QUOTEMAILER OUTPUT","$mail_body","$headers");
*/

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>