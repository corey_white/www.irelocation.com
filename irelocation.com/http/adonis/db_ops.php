<?php

require ("connect.php");

// *********************************************************************************************
// DETERMINE IF START ZIP AND FINISH ZIP ARE IN NATIONAL ZIP CODE DATABASE AND GET INFO IF FOUND
// *********************************************************************************************

// Get Lat and Long of START zip code, city and state

$start_coords    =  "SELECT * FROM zip_code WHERE zip_code = $start_zip";
$start_coords_q  =  mysql_query ($start_coords);

// CHECK TO MAKE SURE THAT BOTH ZIP CODES ARE IN NATIONAL DATABASE

If ($start_coords_q) {
  While ($start = mysql_fetch_array ($start_coords_q)) {
    $a_start_lat   = $start["lattitude"];
    $a_start_lon   = $start["longitude"];
    $a_start_city  = $start["city"];
    $a_start_state = $start["state_prefix"];
  }
  $start_city = $a_start_city . ", " . $a_start_state . "  " . $start_zip;
}

// Get Lat and Long of FINISH zip code, city and state

$finish_coords    =  "SELECT * FROM zip_code WHERE zip_code = $finish_zip";
$finish_coords_q  =  mysql_query ($finish_coords);

If ($finish_coords_q) {
  While ($finish = mysql_fetch_array ($finish_coords_q)) {
    $a_finish_lat   = $finish["lattitude"];
    $a_finish_lon   = $finish["longitude"];
    $a_finish_city  = $finish["city"];
    $a_finish_state = $finish["state_prefix"];
  }
  $finish_city = $a_finish_city . ", " . $a_finish_state . "  " . $finish_zip;
}

// DETERMINE CIRCLE RADIUS FOR start zip codes
  
  // Max and Min Lattitude
  $min_s_lat = abs($a_start_lat) - 2;
  $max_s_lat = abs($a_start_lat) + 2;
  // Max and Min Longitude
  $min_s_lon = abs($a_start_lon) - 2;
  $max_s_lon = abs($a_start_lon) + 2;
  
// DETERMINE CIRCLE RADIUS FOR finish zip codes
  
  // Max and Min Lattitude
  $min_f_lat = abs($a_finish_lat) - 2;
  $max_f_lat = abs($a_finish_lat) + 2;
  // Max and Min Longitude
  $min_f_lon = abs($a_finish_lon) - 2;
  $max_f_lon = abs($a_finish_lon) + 2;
  
// APPLY RADIUS CIRCLES TO PRICE CHART
  
  $pr_range   = "SELECT * FROM prices
                 WHERE d_f_state = '$a_start_state' AND abs(d_f_lat) BETWEEN least('$min_s_lat', '$max_s_lat') && greatest('$min_s_lat', '$max_s_lat') AND 
                 abs(d_f_lon) BETWEEN least('$min_s_lon', '$max_s_lon') && greatest('$min_s_lon', '$max_s_lon')
                 AND abs(d_t_state) = '$a_finish_state' AND abs(d_t_lat) BETWEEN least('$min_f_lat', '$max_f_lat') && greatest('$min_f_lat', '$max_f_lat')
                 AND abs(d_t_lon) BETWEEN least('$min_f_lon', '$max_f_lon') && greatest('$min_f_lon', '$max_f_lon') LIMIT 1";
  $pr_range_q = mysql_query ($pr_range);
  
  If ($pr_range_q) {
    While ($pr = mysql_fetch_array ($pr_range_q)) {
      $rangef_zip   = $pr["d_fzip"];
      $rangef_city  = $pr["d_f_city"];
      $rangef_state = $pr["d_f_state"];
      $rangef_lat   = $pr["d_f_lat"];
      $rangef_lon   = $pr["d_f_lon"];
      $ranget_zip   = $pr["d_tzip"];
      $ranget_city  = $pr["d_t_city"];
      $ranget_state = $pr["d_t_state"];
      $ranget_lat   = $pr["d_t_lat"];
      $ranget_lon   = $pr["d_t_lon"];
      $ranget_price = $pr["d_price"];
    }
  }
  If (!empty($rangef_zip)) {
    $pr_startcity  = $rangef_city . ", " . $rangef_state . "  " . $rangef_zip;
    $pr_finishcity = $ranget_city . ", " . $ranget_state . "  " . $ranget_zip;
    $pr_range_results = 1;
  } Else {
    $pr_range_results = 0;
  }
  

mysql_close($connect);

?>
