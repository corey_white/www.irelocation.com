<?php

// *****************************************************************
// CHANGE THE NUMBER BELOW TO ADD AN ACROSS THE BOARD PRICE INCREASE
// FOR RARE CIRCUMSTANCES

// ***********************************************************
// THIS IS THE URL FOR AUTOQUOTING
// http://www.adonisauto.com/autoquote.php?f_zip=XXXXX&t_zip=XXXXX&operable=X&email=XXXXX&type=x&leadco=xxxx

$atb = 1.0;

// *****************************************************************

// *****************************************************************
// FOR IDENTIFICATION OF BOUGHT LEADS
// 

$leadid = $leadco;

// *****************************************************************

// INCOMING ZIP CODES

$start_zip  = $f_zip;
$finish_zip = $t_zip;

$time = time();
$qn = rand(1, 99);
$quote_num = $time . $qn;

If ($operable == "No" || $operable == "N" || $operable == "n") {
  $running = "INOPERABLE - No power. Does roll, brake and steer" AND $operable="N";
} Else {
  $running = "OPERABLE" AND $operable ="Y";
}

$type = "t1";

// DATABASE OPERATIONS

include ("db_ops.php");

// FIGURE PRICE

If ($operable == "Non-Running" || $operable == "0" || $operable == "N" || $operable == "n") {
  $run_extra = 150;
} Else {
  $run_extra = 0;
}

$num = $ranget_price * $atb + $run_extra;
$num = round($num);  //round off decimals 
if($num % 25)  // if it's not already a multiple of 25 
{$num += 25 - $num % 25;  // add enough to bring it up to the next multiple of 25 
} else { $num = $num+0;
}

$transport_price=$num;

$vtype = "Regular Passenger Car or Sedan";

$enclosed_price = $transport_price + 575;

$transport_price = number_format ($transport_price, 2);
$enclosed_price  = number_format ($enclosed_price, 2);

$price_o = $transport_price;
$price_c = $enclosed_price;

$quote_date = date ('y-m-d');

// DETERMINE WHICH EMAIL TO SEND

If (empty($a_start_lat)) {

    // EMAIL THEM AND LET THEM KNOW START ZIP WAS NOT VALID
    
    mail ("$email", 'ADONIS AUTO TRANSPORT - INSTANT QUOTE', "Quote Number: $quote_num

FROM :     $start_zip
TO   :     $finish_zip
Vehicle Type:  $vtype
Running Status:  $running

We're sorry, but the ORIGIN zip code you provided does not exist in our recognized zip code database.  
    
For another Instant Quote please call us at 602-867-5855 or visit our website www.usacartransport.com and provide us with zip codes that are perhaps closer to major cities in your area.
 
Our services include a fully insured, door-to-door transport with a licensed, dependable and experienced carrier. We have 24-hour dispatch of trucks nationwide. Just give us a call and let us handle the rest. We specialize in transporting vehicles to the customer's satisfaction. 
    
Please call us with any additional questions or concerns.
 
We can almost always meet or beat our competitors' services and pricing. I want you to be comfortable with me overseeing the transport of your vehicle -- that's why we are a member of the BBB. I can answer any questions you have about our services or auto transporting in general. 

Please call us at 602-867-5855 with any questions or you can START YOUR ORDER online at www.usacartransport.com. You can now get INSTANT QUOTES 24/7/365 at our website!
 
Thank you,
Jennifer
 
 
**Although we try our best to give accurate quotes quickly, on occassion we may misquote. We will apologize, but we will not be held to any typographical or clerical errors.

---------------------
Adonis Auto Transport LLC
Bonded, MC#426282
Insured and Experienced Carriers
Save the miles and the headaches - let us move your vehicle for you.
Nationwide Shipper, 24-Hour Dispatch
602-867-5855", 'From: adoniss@cox.net');

    
} Elseif (empty($a_finish_lat))  {

    // EMAIL THEM AND LET THEM KNOW DESTINATION ZIP WAS NOT VALID
    
    mail ("$email", 'ADONIS AUTO TRANSPORT - INSTANT QUOTE', "Quote Number: $quote_num

FROM  :    $start_zip
TO  :      $finish_zip
Vehicle Type:  $vtype
Running Status:  $running

We're sorry, but the DESTINATION zip code you provided does not exist in our zip code database.  For another Instant Quote for the origin and destination you are seeking, please call us at 602-867-5855 or visit our website www.usacartransport.com and provide us with zip codes that are perhaps closer to major cities in your area.
 
Our services include a fully insured, door-to-door transport with a licensed, dependable and experienced carrier. We have 24-hour dispatch of trucks nationwide. Just give us a call and let us handle the rest. We specialize in transporting vehicles to the customer's satisfaction. Please call us with any additional questions or concerns.
 
We can almost always meet or beat our competitors' services and pricing. I want you to be comfortable with me overseeing the transport of your vehicle -- that's why we are a member of the BBB. I can answer any questions you have about our services or auto transporting in general. 

Please call us at 602-867-5855 with any questions or you can START YOUR ORDER online at www.usacartransport.com. You can now get INSTANT QUOTES 24/7/365 at our website!
 
Thank you,
Jennifer
 
 
**Although we try our best to give accurate quotes quickly, on occassion we may misquote. We will apologize, but we will not be held to any typographical or clerical errors.

---------------------
Adonis Auto Transport LLC
Bonded, MC#426282
Insured and Experienced Carriers
Save the miles and the headaches - let us move your vehicle for you.
Nationwide Shipper, 24-Hour Dispatch
602-867-5855", 'From: adoniss@cox.net');

    
} Else {

    If ( ($pr_range_results > 0) && ($ranget_price > 0) ) {
  
    // INSERT QUOTE INTO DATABASE
    require ("connect.php");
    $aquote = "INSERT INTO quotes VALUES ('', '$quote_num', '$quote_date', '$start_zip', '$finish_zip', '$leadid', '$operable', '$price_o', '$price_c', '$email')";
    If (!mysql_query ($aquote)) { }
    mysql_close($connect);

    // SEND ACCURATE QUOTE

    mail ("$email", 'ADONIS AUTO TRANSPORT - INSTANT QUOTE', "Quote Number: $quote_num

FROM  :    $start_city
TO    :    $finish_city
Vehicle Type:  $vtype (Additional costs will apply to larger or specialized vehicles)
Vehicle Status: $running

THANK YOU for the opportunity to submit a quote to move your vehicle. 

BASED ON the information provided, we can move this car fully insured, door-to-door for $$transport_price on an OPEN carrier ($$enclosed_price for an ENCLOSED carrier). This amount includes a fully insured, door-to-door transport with a licensed, dependable and experienced carrier. We have 24-hour dispatch of trucks nationwide. Just give us a call and let us handle the rest. We specialize in transporting vehicles to the customer's satisfaction. Please call us with any additional questions or concerns.

We can almost always meet or beat our competitors' services and pricing. I want you to be comfortable with me overseeing the transport of your vehicle -- that's why we are a member of the BBB. I can answer any questions you have about our services or auto transporting in general. 

Please call us at 602-867-5855 with any questions or you can START YOUR ORDER online at www.usacartransport.com. You can now get INSTANT QUOTES 24/7/365 at our website!
 
Thank you,
Jennifer
 
 
**Although we try our best to give accurate quotes quickly, on occassion we may misquote. We will apologize, but we will not be held to any typographical or clerical errors.

---------------------
Adonis Auto Transport LLC
Bonded, MC#426282
Insured and Experienced Carriers
Save the miles and the headaches - let us move your vehicle for you.
Nationwide Shipper, 24-Hour Dispatch
602-867-5855", 'From: adoniss@cox.net');
   
    } Else {
  
    // EMAIL THEM AND LET THEM KNOW THEIR REQUEST NOT IN PRICE CHART
    
    mail ("$email", 'ADONIS AUTO TRANSPORT - INSTANT QUOTE', "Quote Number: $quote_num

FROM  :    $start_city
TO    :    $finish_city
Vehicle Type:  $vtype
Running Status:  $running

WE'RE SORRY, but the origin and destination zip codes for which you have requested a Quote are not in our price database.  
    
For another Instant Quote for the origin and destination you are seeking, please call us at 602-867-5855 or visit www.usacartransport.com and provide us with zip codes that are perhaps closer to major cities in your area.
 
Our services include a fully insured, door-to-door transport with a licensed, dependable and experienced carrier. We have 24-hour dispatch of trucks nationwide. Just give us a call and let us handle the rest. We specialize in transporting vehicles to the customer's satisfaction. Please call us with any additional questions or concerns.
 
We can almost always meet or beat our competitors' services and pricing. I want you to be comfortable with me overseeing the transport of your vehicle -- that's why we are a member of the BBB. I can answer any questions you have about our services or auto transporting in general. 

Please call us at 602-867-5855 with any questions or you can START YOUR ORDER online at www.usacartransport.com. You can now get INSTANT QUOTES 24/7/365 at our website!
 
Thank you,
Jennifer
 
 
**Although we try our best to give accurate quotes quickly, on occassion we may misquote. We will apologize, but we will not be held to any typographical or clerical errors.

---------------------
Adonis Auto Transport LLC
Bonded, MC#426282
Insured and Experienced Carriers
Save the miles and the headaches - let us move your vehicle for you.
Nationwide Shipper, 24-Hour Dispatch
602-867-5855", 'From: adoniss@cox.net');
    }
    
// end main if statement
}


?>