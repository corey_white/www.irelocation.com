<?php

	function getMapLink($myarray)
	{
		return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
	}
	
	$myarray["address"] = "2012 N. Normal Ave";
	$myarray["city"] = "Tempe";
	$myarray["state"] = "Tempe";
	$myarray["zip"] = "Tempe";

	$link = getMapLink($myarray);
	$ch = curl_init($link);
	$result = curl_exec($ch);
	echo $result;
?>