<?
	include "../inc_mysql.php";
	define(ASSIGN_TO,"connie");
	if ($_REQUEST['method'] == 'post')
	{
		extract($_POST);
		if ($rejectpage == "")
		{
			echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
			echo "<a href='leads.php?method=request'>use REQUEST/GET variables</a><Br/>\n";
		}			
	}
	else
	{
		extract($_REQUEST);
		if ($rejectpage == "")
		{
			echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
			echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
		}
	}
	
	$msg = "";
	if (strlen($fname) == 0)
	{
		$msg .= "<strong>fname</strong>: A First Name is Required<Br/>\n";
	}
	else
		$fname = ucwords(strtolower($fname));
		
	if (strlen($lname) == 0)
	{
		$msg .= "<strong>lname</strong>: A Last Name is Required<Br/>\n";
	}
	else
		$lname = ucwords(strtolower($lname));
		
	if (strlen($email) == 0)
	{
		$msg .= "<strong>email</strong>: An email address is Required<Br/>\n";
	}
	else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))	
	{
		$msg .= "<strong>email</strong>: A valid email address is Required<Br/>\n";
	}
	
	if (strlen(ereg_replace("[^0-9]","",$phone))  != 10)
	{
		$msg .= "<strong>phone</strong>: A Phone Number is Required: 10 digits, only numbers<Br/>\n";
	}
	else
	{
		//Phone Validation.	
		$p_area   = substr($phone,0,3);
		$p_prefix = substr($phone,3,3);	
		
		$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())
			$msg = "<strong>phone</strong>: invalid phone: Please provide a valid phone number.";		
	}	
		
	if (strlen(ereg_replace("[^0-9]","",$phone2))  != 10)
	{
		$msg .= "<strong>phone2</strong>: An Alternate Phone Number is Required: 10 digits, only numbers<Br/>\n";
	}
	else
	{
		//Phone Validation.	
		$p_area   = substr($phone2,0,3);
		$p_prefix = substr($phone2,3,3);	
		
		$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())
			$msg .= "<strong>phone2</strong>: invalid phone: Please provide a valid phone number.";		
	}	

	$valid_buying_state = (strlen($buying_state) == 2);
	$valid_buying_value = (strlen($buying_value) > 0);

	$valid_selling_state = (strlen($selling_state) == 2);
	$valid_selling_value = (strlen($selling_value) > 0);


	if (!$valid_buying_state && $valid_buying_value)
	{
		$msg .= "<strong>buying_state</strong>: A Buying State is Required if you specify a buying value.<Br/>\n";
	}
	if ($valid_buying_state && !$valid_buying_value)
	{
		$msg .= "<strong>buying_value</strong>: A Buying Value is Required if you specify a buying state.<Br/>\n";
	}

	if (!$valid_selling_state && $valid_selling_value)
	{
		$msg .= "<strong>selling_state</strong>: A selling State is Required if you specify a selling value.<Br/>\n";
	}
	if ($valid_selling_state && !$valid_selling_value)
	{
		$msg .= "<strong>selling_value</strong>: A selling Value is Required if you specify a selling state.<Br/>\n";
	}	

	$buy = ($valid_buying_state && $valid_buying_value);
	$sell = ($valid_buying_state && $valid_buying_value);
	
	if ($buy || $sell)
	{
		if ($buy && !$sell)
			$leadTypeCode = 1005;
		else if ($sell && !$buy)
			$leadTypeCode = 1006;
		else
			$leadTypeCode = 1007;
	}
	else
	{	
		$msg .= "<strong>ERROR</strong> You must specify a Valid Buying State/Value pair OR a valid Selling State/Value Pair.<Br/>\n";
		$msg .= "<strong>buying_state</strong>: 2 Character Buying State Code.<br/>\n";
		$msg .= "<strong>buying_value</strong>: Numerical Buying Value in thousands of dollars eg 375 = 375,000 US Dollars <br/>\n";
		$msg .= "<strong>selling_state</strong>: 2 Character Selling State Code.<br/>\n";
		$msg .= "<strong>selling_value</strong>: Numerical Selling Value in thousands of dollars eg 375 = 375,000 US Dollars <br/>\n";
	}	

	
	if (strlen($source) == 0)
	{
		$msg .= "<strong>source</strong>: A Source code is required.<Br/>\n";
	}
	
	$save_source = "aff_$source";

	if (strlen($msg) == 0)
	{
		$two_days_ago = date("YmdHis",(time() - (2*24*60*60)));
	}
	
	$sql = "select * from movecomp.leads_prd where email = '$email' OR homePhone = '$phone'; ";
	$rs = new movecompanion($sql);
	if ($rs->fetch_array())
	{
		$msg .= "<strong>Duplicate</strong>: This lead was found in our database.";
	}

	if (strlen($msg) == 0)
	{
		$date = date("Y/m/d G:i:s");
		$leadSourceCode = "1075";
		$trafficSourceCode = "100";
		$leadStatusCode = "1002";
		$qualDate = $date;	
		$mytime = $date;
		$sql = "insert into leads_prd (leadDate,firstName,lastName,email,homePhone,CellPhone,moveState,".
		"sellState,sellPrice,comments,leadStatusCode,qualDate,updateDate,updateUser,sessionID,clickThruDate,".
		"leadTypeCode,leadSourceCode, trafficSourceCode,purchasePrice,campaign,assignedTo) values ('$mytime','$fname','$lname',".
		"'$email','$phone','$phone2','$buying_state','$selling_state','$selling_value','',".
		"'$leadStatusCode','$qualDate','now()','0','','$date','$leadTypeCode','$leadSourceCode', ".
		"'$trafficSourceCode','$buying_value','$source','".ASSIGN_TO."')";
		//mail("david@irelocation.com","Irelo Test","$old\n\n$assignTo\n\n$sql");
		$rs=new movecompanion($sql);
		$response = $rs->last_insert_id();
		echo "Lead Accepted: $response";
	}
	else
	{
		echo "<strong><font color='#FF0000'>Lead Parsing Error</font>:</strong><br>\n<hr/>\n".$msg;
		
		echo "<br/><strong>Note:</strong><Br/> Append a variable <strong>thankyou</strong> to have us redirect the users browser to your thankyou page.<br/> we will append tracking information for you to save:<Br/><Br/> &nbsp; &nbsp; &nbsp; [your_thank_you_page]?source=irelo&tracking_id=[unique_tracking_id]<br/>";
		echo "<br/>Append a variable <strong>rejectpage</strong> to have us redirect the user to a page if the lead fails submission<Br/>";		
		echo "<Br/><strong>Duplicates:</strong>if the email address and/or the phone of the lead coming in is/are allready in our database, it is a duplicate.<Br/>";

		echo "<Br/><Strong>Warning</strong>: If you do not receive a message letting you know that we accepted the lead, followed by a unique tracking id for that lead, we <font color='#FF0000'>DID NOT</font> receive your lead. It is your responsibility to parse and record this code.";
	}	
?>