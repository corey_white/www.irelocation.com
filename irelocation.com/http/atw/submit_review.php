<?php

if($submit_review=='1')
{
  $to="submit_review@autotransportwatch.com";

  //pass in $to
  if(isset($to))
  {
    //Check if we have something POSTed by the form.
    if (isset($HTTP_POST_VARS))
    {

      //Start with an empty body for the mail message

      $body = '';

      $vals=explode('&',$QUERY_STRING);
      foreach($vals as $myval)
      {
        list($key, $value) = explode('=',$myval);
        $body .= $key . ': ' . $value . "\n";
      }

      //Build up some nice From/Reply Headers
      $headers = "From: admin@autotransportwatch.com";

      //Mail the message out.
      $success = mail($to, "Review Submission " . date("m/d/Y"), "$REMOTE_ADDR \n\n$body", $headers);

      //Always check return codes from functions.
    }
  }

  header("Location: http://www.autotransportwatch.com");

}
else
{
  include("inc_submit_review.htm");
}
?>