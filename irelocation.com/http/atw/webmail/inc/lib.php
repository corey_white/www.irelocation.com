<?
$phpver = phpversion();
$phpver = doubleval($phpver[0].".".$phpver[2]);

if($phpver >= 4.1) {
        extract($_POST,EXTR_SKIP);
        extract($_GET,EXTR_SKIP);
        extract($_FILES);
}


if(strlen($f_pass) > 0) {

        if($allow_user_change_theme) {
                if($tem != "") $tid = $tem;
                else { $tid = $default_theme; }
        } else
                $tid = $default_theme;

        if($allow_user_change_language) {
                if($lng != "") $lid = $lng;
                else { $lid = $default_language; }
        } else
                $lid = $default_language;
}

if(!is_numeric($tid) || $tid >= count($themes)) $tid = $default_theme;
if(!is_numeric($lid) || $lid >= count($languages)) $lid = $default_language;


$selected_theme         = $themes[$tid]["path"];
if (!$selected_theme) die("<br><br><br><div align=center><h3>Invalid theme, configure your \$default_theme</h3></div>");
$selected_language         = $languages[$lid]["path"];
if (!$selected_language) die("<br><br><br><div align=center><h3>Invalid language, configure your \$default_language</h3></div>");
function simpleoutput($p1) { printf($p1); }
$func = strrev("tuptuoelpmis");


function get_usage_graphic($used,$aval) {
        if($used >= $aval) {
                $redsize = 100;
                $graph = "<img src=images/red.gif height=10 width=$redsize>";
        } elseif($used == 0) {
                $greesize = 100;
                $graph = "<img src=images/green.gif height=10 width=$greesize>";
        } else  {
                $usedperc = $used*100/$aval;
                $redsize = ceil($usedperc);
                $greesize = ceil(100-$redsize);
                $red = "<img src=images/red.gif height=10 width=$redsize>";
                $green = "<img src=images/green.gif height=10 width=$greesize>";
                $graph = $red.$green;
        }
        return $graph;
}



// sort an multidimension array
function array_qsort2 (&$array, $column=0, $order="ASC", $first=0, $last= -2) {
        if($last == -2) $last = count($array) - 1;
        if($last > $first) {
                $alpha = $first;
                $omega = $last;
                $guess = $array[$alpha][$column];
                while($omega >= $alpha) {
                        if($order == "ASC") {
                                while(strtolower($array[$alpha][$column]) < strtolower($guess)) $alpha++;
                                while(strtolower($array[$omega][$column]) > strtolower($guess)) $omega--;
                        } else {
                                while(strtolower($array[$alpha][$column]) > strtolower($guess)) $alpha++;
                                while(strtolower($array[$omega][$column]) < strtolower($guess)) $omega--;
                        }
                        if(strtolower($alpha) > strtolower($omega)) break;
                        $temporary = $array[$alpha];
                        $array[$alpha++] = $array[$omega];
                        $array[$omega--] = $temporary;
                }
                array_qsort2 ($array, $column, $order, $first, $omega);
                array_qsort2 ($array, $column, $order, $alpha, $last);
        }
}


class Session {

        var $temp_folder;
        var $sid;

        function Load() {
                $sessionfile = $this->temp_folder."_sessions/".$this->sid.".usf";
                $result      = Array();
                if(file_exists($sessionfile)) {
                        clearstatcache();
                        $fp = fopen($sessionfile,"rb");
                        $result = fread($fp,filesize($sessionfile));
                        fclose($fp);
                        $result = unserialize(base64_decode($result));
                }
                return $result;
        }

        function Save(&$array2save) {
                $content = base64_encode(serialize($array2save));
                if(!is_writable($this->temp_folder)) die("<h3>The folder \"".$this->temp_folder."\" is not writtable or does not exist!!!</h3>");
                $sessiondir = $this->temp_folder."_sessions/";
                if(!file_exists($sessiondir)) mkdir($sessiondir,0777);
                $f = fopen("$sessiondir".$this->sid.".usf","wb") or die("<h3>Could not open session file</h3>");
                fwrite($f,$content);
                fclose($f);
                return 1;
        }
        function Kill() {
                $sessionfile = $this->temp_folder."_sessions/".$this->sid.".usf";
                return @unlink($sessionfile);
        }
}


// load settings
function load_prefs() {

        global         $userfolder,
                        $sess,
                        $default_preferences;

        extract($default_preferences);

        $pref_file = $userfolder."_infos/prefs.upf";

        if(!file_exists($pref_file)) {
                $prefs["real-name"]     = UCFirst(substr($sess["email"],0,strpos($sess["email"],"@")));
                $prefs["reply-to"]      = $sess["email"];
                $prefs["save-to-trash"] = $send_to_trash_default;
                $prefs["st-only-read"]  = $st_only_ready_default;
                $prefs["empty-trash"]   = $empty_trash_default;
                $prefs["save-to-sent"]  = $save_to_sent_default;
                $prefs["sort-by"]       = $sortby_default;
                $prefs["sort-order"]    = $sortorder_default;
                $prefs["rpp"]           = $rpp_default;
                $prefs["add-sig"]       = $add_signature_default;
                $prefs["signature"]     = $signature_default;
                $prefs["timezone"]                = $timezone_default;
                $prefs["display-images"]= $display_images_deafult;
                $prefs["editor-mode"]        = $editor_mode_default;
                $prefs["refresh-time"]        = $refresh_time_default;
        } else {
                $prefs = file($pref_file);
                $prefs = join("",$prefs);
                $prefs = unserialize(~$prefs);
        }
        return $prefs;
}

//save preferences
function save_prefs($prefarray) {
        global $userfolder;
        $pref_file = $userfolder."_infos/prefs.upf";
        $f = fopen($pref_file,"w");
        fwrite($f,~serialize($prefarray));
        fclose($f);
}




//get only headers from a file
function get_headers_from_file($strfile) {
        if(!file_exists($strfile)) return;
        $f = fopen($strfile,"rb");
        while(!feof($f)) {
                $result .= ereg_replace("\n","",fread($f,100));
                $pos = strpos($result,"\r\r");
                if(!($pos === false)) {
                        $result = substr($result,0,$pos);
                        break;
                }
        }
        fclose($f);
        unset($f); unset($pos); unset($strfile);
        return ereg_replace("\r","\r\n",trim($result));
}


function save_file($fname,$fcontent) {
        if($fname == "") return;
        $tmpfile = fopen($fname,"w");
        fwrite($tmpfile,$fcontent);
        fclose($tmpfile);
        unset($tmpfile,$fname,$fcontent);
}




/********************************************************
Templates
********************************************************/

$message_list_template     = "themes/$selected_theme/messagelist.htm";      // Listagem de mensagens
$read_message_template     = "themes/$selected_theme/readmsg.htm";          // Ler a mensagem
$folder_list_template      = "themes/$selected_theme/folders.htm";          // Listagem de pastas
$search_template           = "themes/$selected_theme/search.htm";           // Formul�rio/Resultado da busca
$login_template            = "themes/$selected_theme/login.htm";            // Tela inicial (Login)
$bad_login_template        = "themes/$selected_theme/bad-login.htm";        // Falha de login
$error_template            = "themes/$selected_theme/error.htm";            // Erro do sistema
$newmsg_template           = "themes/$selected_theme/newmsg.htm";           // Enviar mensagem
$newmsg_result_template    = "themes/$selected_theme/newmsg-result.htm";    // Resultado da mensagem enviada
$attach_window_template    = "themes/$selected_theme/upload-attach.htm";    // Pop-Up para anexar arquivos
$quick_address_template    = "themes/$selected_theme/quick_address.htm";    // Pop-Up de acesso r�pido aos endere�os
$address_form_template     = "themes/$selected_theme/address-form.htm";     // Formul�rio para adicionar/editar os contatos
$address_display_template  = "themes/$selected_theme/address-display.htm";  // Exibir detalhes de um contato
$address_list_template     = "themes/$selected_theme/address-list.htm";     // Listar os contatos
$address_results_template  = "themes/$selected_theme/address-results.htm";  // Resultado das a��es tomadas nos contatos (excluir, editar, etc)
$headers_window_template   = "themes/$selected_theme/headers-window.htm";   // Janela de cabe�alhos
$preferences_template      = "themes/$selected_theme/preferences.htm";      // Preferencias
$adv_editor_template       = "themes/$selected_theme/advanced-editor.htm";  // Advanced HTML Editor
$catch_address_template    = "themes/$selected_theme/catch-address.htm";    // Address catcher
$print_message_template    = "themes/$selected_theme/print-message.htm";    // Print friendly version


$lg = file("langs/".$selected_language.".txt");

while(list($line,$value) = each($lg)) {
        if($value[0] == "[") break;
        if(strpos(";#",$value[0]) === false && ($pos = strpos($value,"=")) != 0 && trim($value) != "") {
                $varname  = trim(substr($value,0,$pos));
                $varvalue = trim(substr($value,$pos+1));
                ${$varname} = $varvalue;
        }
}

function print_struc($obj) {
        echo("<pre>");
        print_r($obj);
        echo("</pre>");
}
?>