<?
########################################################################
# Language & themes settings
########################################################################

$allow_user_change_theme                        = no; //allow users select theme
$default_theme                                                = 0; //index of theme, starting with zero
$allow_user_change_language                        = no; //allow users select language
$default_language                                        = 0; //index of language, starting with zero

$themes[] = Array(
        "name"                 => "Default",
        "path"                 => "default"
);

$languages[] = Array(
        "name"                 => "English",
        "path"                 => "en_UK"
);

?>