﻿<?php

$pageTitle = "Search Engine Optimization | iRelo";

$metaDescription = "Google averages over 70 billion a month. 90% of all search engine traffic goes to the sites on the first page of the search results.  How much traffic are you getting?";

$metaTags = "SEO, search engine optimization";

include("irelocation/includes/header.php");

?>
  
  <div id="subpagetop"><img class="icon" src="irelocation/images/keywords_icon.png" alt="Web Development" width="75" height="46" align="left">
  <h1>Search Engine Optimization  </h1>
</div><div id="subpage">
  <img src="irelocation/images/seo_pic.jpg" alt="it could take up to 2.3 years for someone to find your website out of 200 million.  We can cut that down to 5 seconds." width="878" height="223" style="margin-left:13px;"><br />
<div id="lipsum">
 
    
    
  
   
      <br>
      <p>Google averages over 70 billion searches worldwide per month. Consider that roughly 90% of all search engine traffic goes to the pages on the first page of the search results.  How much traffic are you getting?
<strong><br />
<br />
What Is SEO?</strong><br />


The process of getting your website to the top of the search engines in the natural/organic portion of search results is known as search engine optimization (SEO). </p>
      <p><br />
        Search engines use algorithms to calculate and weigh hundreds of factors to rank websites.   A few of the most important factors are keywords in content, amount, and relevance of content to the query, and the number of inbound links to a website. Other factors including the quality of the links, as well as the internal site structure, meta tags etc. also play a role. </p>
      <p><br />
        Every website, and every niche is different and has different requirements, so along with an understanding of search engines, an SEO firm must have a thorough grasp of client industries, and website configurations to provide a definitive customized solution with long term results.  These results are reached over time through expertise, hard work, and dedication. </p>
      <p><br />
        It is estimated that the Internet is growing at an exponential rate of 7 million Web pages per day.  If your site is not optimized properly search engines will ignore it, making it impossible for customers to find it. How’s your site?</p>
    <p><br />
      <a href="contact.php">Contact iRelo today</a> to find out how our team of experienced, SEO/SEM professionals can help clients find you! </p>
      <p><br>
  </p>
   </p>
</div>
<p>&nbsp;</p>
  </div>
  <div id="subpagebottom"></div>
  </DIV>
<? include'irelocation/includes/footer.php'; ?>