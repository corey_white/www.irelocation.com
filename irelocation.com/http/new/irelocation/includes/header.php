<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3c.org/TR/1999/REC-html401-19991224/loose.dtd">
<HTML xmlns="http://www.w3.org/1999/xhtml">
<HEAD>
<META http-equiv=Content-Type content="text/html; charset=utf-8">

<link href="irelocation/styles.css" rel="stylesheet" type="text/css">
<title><?php echo $pageTitle; ?></title>

<META name="description" content="<?php echo $metaDescription; ?>" />


<META name="keywords" content="<?php echo $metaTags; ?>" />
<style type="text/css">
<!--
.style1 {
	color: #E8960E;
	font-weight: bold;
}
-->
</style>
<!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->

</HEAD>
<BODY class=index>

<DIV id=frame>

  <!-- begin: MASTHEAD -->
  <DIV id="header">
    <div align="left"><img src="irelocation/images/logo.png" alt="i-relocation network" width="168" height="78" style="padding-left:25px; padding-top:15px;">  </div>
  </DIV>
    <!-- end: MASTHEAD -->
<DIV id=content><!-- begin: NAVIGATION -->
<DIV id=nav><div id="nav-sub">
 
      <li><a href="index.php">Home</a></li>
      <li><a href="company.php" title="Who We Are">Who We Are</a></li>
	   <li><a href="leads.php" title="Lead Generation">Lead Generation</a></li>
	    <li><a href="ppc.php" title="Pay Per Click">Pay Per Click</a></li>
		 <li><a href="seo.php" title="Search Engine Optimization">Search Engine Optimization</a></li>
		  <li><a href="web.php" title="Web Development">Web Development</a></li>
		   <li><a href="contact.php" title="Contact Us">Contact Us</a></li>
     </div>
  </DIV>

  <!-- end: NAVIGATION -->
