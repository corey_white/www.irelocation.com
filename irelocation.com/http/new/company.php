﻿<?php

$pageTitle = "Who We Are | About iRelo";

$metaDescription = "Known for successful marketing methodologies in lead generation, iRelo has mastered the balance between advertising with PPC, SEO, & customer service.";

$metaTags = "lead generation, B2B, B2C, ";

include("irelocation/includes/header.php");

?>
  
  <div id="subpagetop">
    <h1>Who We Are    </h1>
</div><div id="subpage">
  <div align="center"><img src="irelocation/images/about_pic.jpg" alt="There are two types of lead generation companies in the world today.  We're the other one." width="878" height="223" /><br />
  </div>
  <div id="lipsum">
 
    
    
  
   
      <br>
      <p>iRelo, founded in 1998, is an innovative internet marketing company specializing in <a href="leads.php" title=" Lead Generation">lead generation</a>.   Known for its successful marketing methodologies and systems used with internet lead generation, iRelocation has conquered the connectivity between advertising through Pay-Per-Click (Sponsored Links) on Google, Yahoo, and MSN, SEO, and customer relationship management. <br />
        <br />

 As a leader in the lead generation industry, we currently facilitate the acquisition of information from consumers and distribute those potential customers to our partners.  We primarily deal with large privately and publicly held companies that have a nationwide presence.  iRelocation has a proven track record within the internet marketing platform reaching millions each month. The networks competitive expertise in lead generation has been a driving force behind the rapid growth in consumer traffic and to their overall success. <br />
 <br />

The iRelocation Network affiliates in the Moving, Real Estate, Home and Commercial Security, B2B, and Auto Shipping Industries, just to name a few, share in our realization of being part of a thriving industry now used by millions. Our campaigns encompass an assorted and diverse collection of new and upcoming businesses strategies while continuing to service our long lasting business partners.</p>
</div>
<p>&nbsp;</p>
  </div>
  <div id="subpagebottom"></div>
  </DIV>
<? include'irelocation/includes/footer.php'; ?>