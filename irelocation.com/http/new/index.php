﻿<?php

$pageTitle = "Welcome to iRelo";

$metaDescription = "iRelocation’s customized lead generation programs are designed to increase your ROI and increase your conversion rate!";

$metaTags = " SEO, Search Engine Optimization, Lead Generation,Lead Gen, Web Development, Pay Per Click, PPC";

include("irelocation/includes/header.php");

?>

  <!-- begin: FLASH -->
  <script src="Scripts/AC_RunActiveContent.js" type="text/javascript"></script>
  
 <DIV id=flash>
   <div class=flashbox>
     <div align="center">
       <p>
         <script type="text/javascript">
AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0','width','434','height','272','src','slideshow','quality','high','pluginspage','http://www.macromedia.com/go/getflashplayer','movie','slideshow' ); //end AC code
</script><noscript><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="434" height="272">
           <param name="movie" value="slideshow.swf" />
           <param name="quality" value="high" />
           <embed src="slideshow.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="434" height="272"></embed>
         </object></noscript>
       </p>
     </div>
   </div>
   <div class=rounded-box>
 <h1>iRelo generated well over 1 Million leads last year. </h1> <strong>How many of them were yours?</strong> <br />

 
Imagine pre-qualified leads flowing into your inbox, or your phone literally ringing off the hook with customers calling you! Instead of spending hour after hour cold calling potential clients, or sitting at trade shows, let us bring the business to you! iRelocation’s customized lead generation programs are designed to increase your ROI and improve your conversion rate! Want to know more? <strong>Contact us now!

 </strong></div>
 </DIV>


  <!-- end: FLASH -->
  <!-- begin: HEADLINES -->

<UL id="headlines">
  <LI>
    <a href="leads.php"><img src="irelocation/images/pic_1.jpg" width="214" height="145" border="0" alt="Lead Generation"></a> </LI>
  <LI><a href="seo.php"><img src="irelocation/images/pic_2.jpg" width="226" height="145" border="0" alt="SEO"></a></LI>
  <LI>
    <a href="web.php"><img src="irelocation/images/pic_3.jpg" width="230" height="145" border="0" alt="Web Development"></a> </LI>
  <LI>
    <a href="ppc.php"><img src="irelocation/images/pic_4.jpg" width="223" height="145" border="0" alt="Pay Per Click"></a> </LI>
  <LI>
    <H1><A 
  href="leads.php" title="Lead Generation">Lead Generation </A></H1>
     <p>Why spend hours cold calling  a list of prospects who may or may not be interested, when decision makers with a specific need can come to you? Our PPC, email, and SEO campaigns are an affordable way to acquire the best ROI.&nbsp; <br />
       <strong>How's your ROI?</strong>&nbsp; </p>
       <div align="right"><a href="leads.php" title="Lead Generation"><img src="irelocation/images/learn.png" alt="Learn More about lead generation" width="81" height="37" border="0" /></a></div>
  </LI>
  <LI>
  <H1><A href="seo.php" title="Search Engine Optimization">SEO</A></H1>
  
  <p>Roughly 90% of all search engine traffic goes to the  sites on the first page of the search results.   The process of getting your website to the top of the search engines is  known as search engine optimization (SEO).&nbsp; <br />
    <strong>How's your traffic flow? </strong>&nbsp;  </p>
  <div align="right"><a href="seo.php" title=" SEO"><img src="irelocation/images/learn.png" alt="Learn More about SEO" width="81" height="37" border="0"/></a></div>
  </LI>
  <LI>
  <H1><A href="web.php" title="Web Development">Web 
  Development</A></H1>
  <p>As the saying goes, &quot;You never have a second chance to make a first impression.&quot;&nbsp; You need a website that delivers results! <br />
    Let us renovate your website and instantly establish value and credibility.  <br />
    <strong>What's your site say about you?</strong>&nbsp;</p>
  <div align="right"><a href="web.php" title="Web Development"><img src="irelocation/images/learn.png" alt="Learn More about web development" width="81" height="37" border="0" /></a></div>
  <br>
  </LI>
  <LI>
  <H1><A 
  href="ppc.php" title="Pay Per Click">Pay Per Click  </A></H1>
  <p>Every month over 40 million people use online searches to find the products and services that they need. With the use of customized PPC campaigns, iRelo can put your business where the customers are searching!&nbsp; <br />
    <strong>Who’s looking for you?</strong><br />
  </p>

  <div align="right"><a href="ppc.php" title="Pay Per Click"><img src="irelocation/images/learn.png" alt="Learn More about Pay Per Click" width="81" height="37" border="0" /></a></div>
  <br>
  <br>
  </LI></UL>
<!-- end: HEADLINES -->
</DIV>
<? include'irelocation/includes/footer.php'; ?>