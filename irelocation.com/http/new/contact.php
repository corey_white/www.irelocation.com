﻿<?php

$pageTitle = "Contact Us | iRelo";

$metaDescription = "My page description, less than 160 characters.";

$metaTags = "a list, of appropriate, tags, separated with, commas";

include("irelocation/includes/header.php");

?>
  
  <div id="subpagetop">
    <h1>Contact Us </h1>
</div><div id="subpage">
  <img src="irelocation/images/contact_pic.jpg" alt="Web Design" width="878" height="223" style="margin-left:13px;"><br />

 <?php if ($send == "yes") { 
 
 	// Your email address

	$to = "mark@irelocation.com";

	// The subject

	$subject = "iRelocation Contact Form";

	// The message

	$message = "Name: ".$name."\n\n";
	$message .= "Phone: ".$phone."\n\n";
	$message .= "Email: ".$email."\n\n";
	$message .= "City: ".$city."\n\n";
	$message .= "State: ".$state."\n\n";
	$message .= "Area of Interest: ".$interest."\n\n";
	$message .= "Comment: ".$comment;
	
	

	mail($to, $subject, $message, "From: $email");

	
 
 ?>
       <br>
       <p><h1> &nbsp;&nbsp;&nbsp;Thank You!</h1></p>
    <p>Thank you for your email. We will be contacting you shortly.<br />
        <br /></p>
      
 <?php } else { ?>

<div id="form">
   
      <br>
    <p>For questions, comments or more information, please use the form below:<br />
        <br />
        <strong>i-Relocation Network</strong><br />
        P.O. Box 11018<br />
        Tempe, AZ 85284<br />
      P.&nbsp; 480.785.7400      <br />
      <br />
      
     
      
      
      <strong>Fields marked with an * are required. </strong></p>
      <form id="form1" method="post" action="contact.php">
      <input name="send" type="hidden" value="yes" />
        <table width="75%" border="0" cellspacing="3" cellpadding="3">
          <tr>
            <td width="48%">Name:&nbsp;</td>
            <td width="52%"><input name="name" type="text" id="name" /> 
            <span class="red">*</span> </td>
          </tr>
          <tr>
            <td>Phone Number: </td>
            <td><input name="phone" type="text" id="phone" /> 
            <span class="red"> *</span> </td>
          </tr>
          <tr>
            <td>Email:</td>
            <td><input name="email" type="text" id="email" /> 
            <span class="red">*</span> </td>
          </tr>
          <tr>
            <td>City: </td>
            <td><input name="city" type="text" id="city" /></td>
          </tr>
          <tr>
            <td>State:</td>
            <td><input name="State" type="text" id="State" size="2" maxlength="2" /></td>
          </tr>
          <tr>
            <td>Area(s) of Interest:</td>
            <td><select name="interest" size="1" id="interest">
              <option value="General" selected="selected">General</option>
              <option value="Lead Generation">Lead Generation</option>
              <option value="SEO">SEO</option>
              <option value="Pay Per Click">Pay Per Click</option>
              <option value="Web Development">Web Development</option>
              <option value="Other">Other</option>
            </select>            </td>
          </tr>
          <tr>
            <td>Comments:</td>
            <td><label>
              <textarea name="comments" cols="50" rows="4" id="comments"></textarea>
            </label></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><label>
              <div align="center">
                <input type="submit" name="Submit" value="Submit" />
                </div>
            </label></td>
          </tr>
        </table>
        <br />
      </form>
  </div>
  
<?php }  ?>


<p>&nbsp;</p>
  </div>
  <div id="subpagebottom"></div>
  </DIV>
<? include'irelocation/includes/footer.php'; ?>