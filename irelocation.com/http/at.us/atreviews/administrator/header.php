<?
  include '../config.php';
  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<html>
<head>
	<title>Auto Transport Reviews - <?=$company?></title>
<meta name="description" content="Auto Transport Reviews has as its' primary goal to promote the industry through consumer driven ratings.">
<meta name="keywords" content="auto transport,car shipping reviews,car transport reviews,auto shipping reviews,vehicle transport reviews,auto transport review,automobile shipping,vehicle shipping,car mover,car transporter,moving companies,movers,professional movers,moving checklist,shipping automobile,auto carriers,moving,automobile transporter,transport trucks,open transport,relocation information,trucking, carrier reviews, trucking reviews, reviews by consumers, reviews by customers">

<style type="text/css">
body,td	{margin:0px;font-family:arial,sans-serif;font-size:12px;color:navy}
.header	{font-family:arial,sans-serif;font-size:12px;color:navy;font-weight:bold}
a.reviews	{font-family:arial,sans-serif;font-size:12px;color:navy;font-weight:bold}
.heading3	{font-family:arial,sans-serif;font-size:16px;color:navy;font-weight:bold}
.heading4	{font-family:arial,sans-serif;font-size:13px;color:navy;font-weight:bold}
.tips	{font-family:arial,sans-serif;font-size:12px;color:navy;}
input,textarea,select	{border:thin solid navy;background-color:#F8E6A8}
</style>
<link REL="shortcut icon" HREF="images/icon.png" TYPE="image/x-icon">
</head>

<body bgcolor="#FFFFFF">
		<table width="760" cellspacing="0" cellpadding="0" border=0>
		    <tr>
		      <td valign="top" colspan=2><img src="images/header.jpg" border="0" width="760"></td>
		      <tr><td colspan=2>
		       <table border="0" cellspacing="0" width="760" cellpadding="0" height="26" bgcolor="#F7BA28">
		        <tr>
		          <td width="100%">&nbsp;&nbsp;&nbsp;
			      <a href="index.php" class="header">HOME</a> | 
			      <a href="index.php?req=managereviews" class="header">MANAGE REVIEWS</a> | 
			      <a href="index.php?req=managecompanies" class="header">MANAGE COMPANIES</a></td>
			      </td>
		        </tr>
		      </table>
		     </td>
		    </tr>
		    <tr><td valign="top">