<h1 align="center">Moving Services and Auto Transport Links</h1>
<h4 align="center">We would like to provide our customers with helpful links 
related to the moving services and auto transport industry.</h4>
<ul>
  <?
$irelolinks='
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.topmoving.com">Top Moving</a> - Compare moving estimates with 4 free moving quotes from
	<a target="_blank" href="http://www.topmoving.com">Top Moving</a>: Allied Van Lines, Atlas Van Lines, North American Van Lines and Wheaton Van Lines.</h5>
	</li>
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.promoving.com">Pro Moving</a> - One form, 4 quotes.  Compare and save up to 50% on moving quotes from
	<a target="_blank" href="http://www.promoving.com">Pro Moving</a>: Allied Van Lines, Bekins Van Lines, North American Van Lines and Paul Arpin Van Lines.</h5>
	</li>
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.carshipping.com">Car Shipping</a> - One Form, 5 Auto Transport Quotes.  Compare vehicle shipping estimates from certified auto transport companies with 
	<a target="_blank" href="http://www.carshipping.com">Car Shipping</a>.</h5>
	</li>
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.movemycar.com">Move My Car</a> - Shipping a car? Compare and save with 5 auto transport quotes from certified auto transport companies at 
	<a target="_blank" href="http://www.movemycar.com">Move My Car</a>.</h5>
	</li>
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.irelocation.com">iRelocation</a> - Looking for a real estate agent or mortgage broker? Our partner at Sirva will connect you with agents from Century 21 and Coldwell Banker. Visit  
	<a target="_blank" href="http://www.irelocation.com">iRelocation Real Estate and Mortgage</a>.</h5>
	</li>
	<li>
	<h5 align="left">
	<a target="_blank" href="http://www.1stmovingdirectory.com">1st Moving Directory</a> - Search for the best moving companies and auto transport companies at   
	<a target="_blank" href="http://www.1stmovingdirectory.com">1st Moving Directory</a>.</h5>
	</li>
	';
if($_GET['isatr']==true)
{
	echo $irelolinks;	
}
?>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.auto-transport.us">Auto-Transport.us 
      </a> - One Form, 5 Auto Transport Quotes. Compare vehicle shipping estimates 
      from certified auto transport companies with <a target="_blank" href="http://www.auto-transport.us"> 
      www.Auto-Transport.us</a>.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.movingease.com">Moving 
      Ease </a> - One Form, 4 Moving Quotes. You save when moving companies compete 
      for your business. Get FREE moving quotes at <a target="_blank" href="http://www.movingease.com"> 
      www.MovingEase.com</a>.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.autotransportreviews.com">Auto 
      Transport Reviews</a> - Do you have a short story you would like to share 
      concerning your experience with an Auto Transport Company?&nbsp; Submit 
      your review at <a target="_blank" href="http://www.autotransportreviews.com"> 
      www.AutoTransportReviews.com</a>.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=moving&STYPE=S&CP=Travel+%26+Transportation%5EMoving+%26+Storage%5EHousehold+Goods%5E&MC=1&OO=1&F=1"> 
      SuperPages: Household Goods Moving &amp; Storage</a> - Visit <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=moving&STYPE=S&CP=Travel+%26+Transportation%5EMoving+%26+Storage%5EHousehold+Goods%5E&MC=1&OO=1&F=1"> 
      www.SuperPages.com</a> for Household Moving Service listings</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=auto+transport&STYPE=S&CP=Automotive^Transport&F=1&RC=1&search=Find+It"> 
      SuperPages: Auto Transport</a> - Visit <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=auto+transport&STYPE=S&CP=Automotive^Transport&F=1&RC=1&search=Find+It"> 
      www.SuperPages.com</a> for dependable Auto Transport listings.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=real+estate&STYPE=S&CP=Real+Estate%5EAgencies+%26+Brokerage%5EReal+Estate+Agents%5E&MC=1&OO=1&F=1"> 
      SuperPages: Real Estate Agents</a> - Visit <a target="_blank" href="http://yellowpages.superpages.com/listings.jsp?C=real+estate&STYPE=S&CP=Real+Estate%5EAgencies+%26+Brokerage%5EReal+Estate+Agents%5E&MC=1&OO=1&F=1"> 
      www.SuperPages.com</a> to find Real Estate Agents nationwide.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.magicyellow.com/Listings/25066145.htm"> 
      MagicYellow: Pro Moving Quotes</a> - One form, 4 quotes.&nbsp; Compare and 
      save up to 50% on moving quotes from <a target="_blank" href="http://www.magicyellow.com/Listings/25066145.htm"> 
      www.MagicYellow.com</a>: Allied Van Lines, Bekins Van Lines, North American 
      Van Lines and Paul Arpin Van Lines.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.magicyellow.com/Listings/16726775.htm"> 
      MagicYellow: Top Moving Quotes</a> - Compare moving estimates with 4 free 
      moving quotes from <a target="_blank" href="http://www.magicyellow.com/Listings/16726775.htm"> 
      www.MagicYellow.com</a>: Allied Van Lines, Atlas Van Lines, North American 
      Van Lines and Wheaton Van Lines.</h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.movingboxsupply.com"> 
      Moving Box Supply</a> - Save money on discount moving boxes for shipping, 
      packaging, and moving from <a target="_blank" href="http://www.movingboxsupply.com"> 
      www.MovingBoxSupply.com</a></h5>
  </li>
  <li> 
    <h5 align="left"> <a target="_blank" href="http://www.escapeartist.com/moving/moving.htm"> 
      EscapeArtist.com</a> - Worldwide Moving Companies: Overseas Movers - Worldwide 
      Moving Services at <a target="_blank" href="http://www.escapeartist.com/moving/moving.htm"> 
      www.EscapeArtist.com</a>.</h5>
  </li>
  <li>
    <h5 align="left"> <a target="_blank" href="http://www.autohopper.com"> 
      AutoHopper.com</a> - Online Car buying made easy- Auto Buying and Selling 
      Directory Services at <a target="_blank" href="http://www.autohopper.com"> 
      www.AutoHopper.com</a>.</h5>
  </li>
</ul>
