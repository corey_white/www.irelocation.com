﻿<?php

$pageTitle = "Sitemap | iRelo";

$metaDescription = "Sitemap for iRelo ";

$metaTags = "Irelo's Sitemap. ";

include("irelocation/includes/header.php");

?>
  
  <div id="subpagetop">
    <h1>Sitemap</h1>
</div><div id="subpage"><br />
<div id="lipsum">
      <br>
      <p>Want to know where to go?&nbsp; <br />
        <br />
        <a href="index.php">Home</a><br />
        iRelo’s customized lead generation programs are designed to increase your ROI and increase your conversion rate!<br />
        <br />
        <a href="company.php">Who We Are</a><br />
        iRelo, founded in 1998, is an innovative internet marketing company specializing in lead generation via PPC, SEO, and Email campaigns.<br />
        <br />
        <a href="leads.php">Lead Generation</a><br />
        iRelo's PPC, email, and SEO lead generation campaigns are an affordable way to acquire the best ROI for your budget.<br />
        <br />
        <a href="seo.php">Search Engine Optimization</a><br />
    iRelo's SEO experts can take your site to the top of the search engines, and get the traffic your site deserves! <br />
    <br />
    <a href="web.php">Website Development</a><br />
    iRelo's experienced web development team can renovate your website and instantly establish value and credibility.<br />
    <br />
    <a href="ppc.php">Pay Per Click</a><br />
    With the use of customized PPC campaigns, iRelo can put your business where the customers are searching! <br />
    <br />
    <a href="contact.php">Contact Us</a><br />
    Want to know how iRelo can help you to meet your business goals? Contact us today!<br />
    <br />
    <a href="privacy.php">Privacy Policy</a><br />
    Your privacy is important to us! Want to know how we secure your personal info? Read the privacy policy.</p>
      <p>&nbsp; </p>
</div>
<p>&nbsp;</p>
  </div>
  <div id="subpagebottom"></div>
  </DIV>
<? include'irelocation/includes/footer.php'; ?>