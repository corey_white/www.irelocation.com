<?
	//include "../reporting/moving_quotemailer.php";

	include_once("../inc_mysql.php");	
	include_once("../cronlib/.functions.php"); 
	include_once(".lib.php");
	//include "thankyouemail.php";
	
	updateCronStatus("selfmove", "started");
	
	run();
	
	function sendLead($msg, $email="X",$subject="Self Moving Quote",$from="XX")
	{
		if ($from == "XX")
			$from = "From: The iRelocation Network <no_reply@irelocation.com>\n";
			
		if ($email == "X")
		{
			//simple curl post, only for ABF.
			$ch=curl_init("$msg");				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			return $response;
		}
		else
		{
			mail($email,$subject,$msg,$from);	
			mail("rob@irelocation.com","Self Moving Quote sent to $email","To: $email\n\n".$msg,"From: The iRelocation Network <no_reply@irelocation.com>\n");	
		}
	}
	
	function run()
	{
		$mytime = updateReceived();//set the timestamp of new leads.		
		$sql = "select * from irelocation.leads_selfmove where received = '".$mytime."'";//get those leads.
		$rs = new mysql_recordset($sql);	
		$count = 0;
		
		while($rs->fetch_array())	
		{
			$lead_ids = array();
			$array = $rs->myarray;
			//change fields to customer relevant data.

			#
			#   EMAIL LEAD SEND EXAMPLE
			#
/*
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$msg = getEmailMessage($array); //-- format the Email Message
				sendLead($msg,"rob@irelocation.com","Self-Service Move","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = TESTLEAD; //-- Lead_ID pulled from .lib.php
			
			} else { //-- we only need this here at the beginning just to let us know that a local lead squeaked through
			
				mail("rob@irelocation.com","Local SMQ Lead!","a Local lead has snuck through! (Quote ID: ".$array["quote_id"].")");
			}
*/
			
			

			#
			#   SelfMoves USA
			#
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$msg = getEmailMessage($array); //-- format the Email Message
				sendLead($msg,"websales@selfmovesusa.com,selfmovesusa@rstarmail.com","Self-Service Move Quote for SelfMoves USA","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = SELFMOVESUSA; //-- Lead_ID pulled from .lib.php
				echo "Sent lead to SelfMoves USA<br /><br />";
			
			} else { //-- we only need this here at the beginning just to let us know that a local lead squeaked through
			
				mail("rob@irelocation.com","Local SMQ Lead!","a Local lead has snuck through! (Quote ID: ".$array["quote_id"].")");
			}

								
			#
			#   PODS
			#
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$msg = getEmailMessage($array); //-- format the Email Message
				sendLead($msg,"wkurze@pods.com","Self-Service Move Quote for PODS","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = PODS; //-- Lead_ID pulled from .lib.php
				echo "Sent lead to PODS<br /><br />";
			
			} 

								
			#
			#   Albert Moving
			#
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$msg = getEmailMessage($array); //-- format the Email Message
				sendLead($msg,"Purchasedleads@albertmoving.com","Self-Service Move Quote for Albert Moving","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = ALBERTMOVING; //-- Lead_ID pulled from .lib.php
				echo "Sent lead to Albert Moving<br /><br />";
			
			} 

								
			#
			#   WeHaul Moving
			#
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$xml = format_for_wehaul($array); //-- if they want XML, this will send out to a function for XML formatting
				sendLead($xml,"autoquotes@nrs-corps.com","Self-Service Move","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = WEHAUL; //-- Lead_ID pulled from .lib.php
				echo "Sent lead to WeHaul Moving<br /><br />";
			
			} 
								
			#
			# 	SAM
			#
			#	Long Distance, Specified zip codes.
				#if ( checkZipCodes($array["origin_zip"],$array["destination_zip"],SAM) && ($array["destination_state"] != $array["origin_state"]) )
				if ($array["destination_state"] != $array["origin_state"]) 
				{ 
					$msg = getEmailMessage($array); //-- format the Email Message
					sendLead($msg,"storeandmove@unigroupinc.com","Self-Service Move Quote for SAM","From: quotes@SelfMovingQuotes.com");
					$lead_ids[] = SAM;
					echo "Sent lead to SAM<br /><br />";
				}



			#
			#	 XML LEAD SEND EXAMPLE
			#
/*
			if ($array["destination_state"] != $array["origin_state"]) {
			
				$xml = format_for_wehaul($array); //-- if they want XML, this will send out to a function for XML formatting
				sendLead($xml,"rob@irelocation.com,autoquotes@nrs-corps.com","Self-Service Move","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = WEHAUL; //-- Lead_ID pulled from .lib.php
			
			} else { //-- we only need this here at the beginning just to let us know that a local lead squeaked through
			
				mail("rob@irelocation.com","Local SMQ Lead!","a Local lead has snuck through! (".$array["quote_id"].")");
			}
*/

			#
			#	WeHaul Moving
			#
/*
			if ($array["destination_state"] != $array["origin_state"])
			{
				$xml = format_for_wehaul($array);
				sendLead($xml,"rob@irelocation.com,autoquotes@nrs-corps.com","Self-Service Move","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = WEHAUL;
			}
			else {
				mail("rob@irelocation.com","Local SMQ Lead!","a Local lead has snuck through! (".$array["quote_id"].")");
			}
*/

			
			
			
			#
			#	TEXT MESSAGE EXAMPLE
			#				
			/*
			if ($array["destination_state"] != $array["origin_state"] && canSendToTopRelo($array["origin_state"],$array["destination_state"],$array["size_of_move"]))
			{
				$msg =  getTextMessage($array);			
				sendLead($msg,"leads@toprelo.com");
				$lead_ids[] = TOPRELO;
			}
			
			*/
			
			
			
			#
			# 	SPECIFIC ZIP CODE EXAMPLE
			#
			#	Long Distance, Specified zip codes.
/*
				if (
					checkZipCodes($array["origin_zip"],$array["destination_zip"],DOOR2DOOR) 
					&& 
					($array["destination_state"] != $array["origin_state"])
				   )
				{ 
					$email = "moveme@doortodoor.com";
					$msg = getDoor2DoorMessage($array);
					sendLead($msg,$email);
					echo "<br/>\nEmail to : $email<Br/>\n";
					$lead_ids[] = DOOR2DOOR;
				}
*/
			
			
			
			
			#
			# 	EXCLUDE STATES EXAMPLE
			#
/*
				if ($array["origin_state"] != 'HI' && $array["origin_state"] != 'AK' && $array["destination_state"] != 'HI' && $array["destination_state"] != 'AK' && ($array["destination_state"] != $array["origin_state"]))
				{
					$email = "saleslogix@movex.com";
					$msg = getMovexMessage($array);
					sendLead($msg,$email);
					echo "<br/>\nEmail to : $email<Br/>\n";
					$lead_ids[] = MOVEX;
				}
*/





			#
			#	FAT URL EXAMPLE
			#
/*
				if ($array["destination_state"] != $array["origin_state"])
				{
					$fat_url = getABFQueryMessage($array);
					$response = sendLead($fat_url);
					if ($response != "True")
					{
						mail("david@irelocation.com","ABF Response",$response."\n\n\nLead:$fat_url","From: The iRelocation Network <no_reply@irelocation.com>\n");	
						save_response($array["quote_id"], $response, ABFUPACK,0,"smq");
					}
					else
						save_response($array["quote_id"], "True", ABFUPACK,1,"smq");
					
					$lead_ids[] = ABFUPACK;
				}
*/

	
	
	
			$count++;
			sort($lead_ids);
			echo "<br/>\n\n";

			//-- Send to Moving Staffers
			if ($array["moving_staffer"] >= 1)
			{
				$msg = getEmailMessage($array); //-- format the Email Message
				sendLead($msg,"purchasedleads@movingstaffers.com","Self-Service Move Quote for Moving Staffers","From: quotes@SelfMovingQuotes.com");
				$lead_ids[] = MOVINGSTAFFERS;
				
				echo "<br/>\nSent to Moving Staffers<br /><br />\n";
			}
			
			updateLeadIds(implode("|",$lead_ids) ,$array["quote_id"],"irelocation.leads_selfmove");//save it

			#sendThankyouEmail($array,"smq"); //-- BYPASSING FOR NOW
		}
	}

	
	#------------------------------------------------------------------------
	#
	#		Update received stamp, this all returns the timestamp
	#		Leads with this timestamp will be sent out.
	#
	#------------------------------------------------------------------------
	function updateReceived()
	{
		$mytime = date("YmdHis");
		$sql="update irelocation.leads_selfmove set received='".$mytime."' where (received='' or received='0')  and scrub_is_valid = 1 and LOWER(fname) not like 'test%'";//and LOWER(fname) != 'test' ";
		echo $sql."<Br/>\n";
		$rs=new mysql_recordset($sql);
		return $mytime;
	}
	
	function canSendToTopRelo($origin_state,$destination_state,$size_of_move)
	{
		if (strtolower($size_of_move) == "studio")
			return false;
		
		$states = array("ID","MT","WY","NE","SD","ND","MN");
		
		return (!array_search($origin_state,$states) && !array_search($destination_state,$states));
	}
	
	
	/* WE HAUL XML */
	function format_for_wehaul($array)
	{
		extract($array);
		$xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?><Lead_Household>".
				"<src></src><Estimated_Move_Date>".str_replace("-","/",$estimated_move_date)."</Estimated_Move_Date>".
				"<FirstName>$fname</FirstName><LastName>$lname</LastName><Email>$email</Email><Phone_Day>".formatPhone($phone1)."</Phone_Day>".
				"<Phone_Eve>".formatPhone($phone2)."</Phone_Eve><Phone_Mobile></Phone_Mobile><Moving_From_Address></Moving_From_Address>".
				"<Moving_From_City>$origin_city</Moving_From_City><Moving_From_State>$origin_state</Moving_From_State>".
				"<Moving_From_Zip>$origin_zip</Moving_From_Zip><Moving_From_Country>United States of America</Moving_From_Country>".
				"<Moving_To_Address></Moving_To_Address><Moving_To_City>$destination_city</Moving_To_City>".
				"<Moving_To_State>$destination_state</Moving_To_State><Moving_To_Zip>$destination_zip</Moving_To_Zip><Moving_To_Country>United States of America</Moving_To_Country>".
				"<Stairs>N</Stairs><Rooms>$Size_of_move</Rooms><Cars>0</Cars><Car_Type></Car_Type><Weight></Weight>".
				"<Manifest></Manifest><Comments>$comments</Comments></Lead_Household>";
		return $xml;
	}

	function getMovingLaborServicesMessage($array)
	{
		extract($array);		
		$parts = split("-",$estimated_move_date);
		
		$year=$parts[2];
    	$month=$parts[0];
	    $day=$parts[1];
		
		$htmlmsg="<html><body><table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
		"<tr><td width='200'>Received: </td><td>".getReceived($received)."</td></tr>";
		
		$htmlmsg .= "<tr><td width='200'>Lead Type: </td><td>Moving Labor Request</td></tr>".
		"<tr><td width='200'>Customer Name: </td><td>".$fname." ".$lname."</td></tr>";
		
		$htmlmsg .= "<tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$email."'>".$email."</a></td></tr>".
		"<tr><td width='200'>Phone Number: </td><td>".formatPhone($phone1)."</td></tr>".
		"<tr><td width='200'>Alternate Phone: </td><td>".formatPhone($phone2)."</td></tr>".
		"<tr><td width='200' valign='top'>Moving From: </td><td>$origin_city,$origin_state<Br/>$origin_zip</td></tr>".
		"<tr><td width='200' valign='top'>Moving To: </td><td>$destination_city,$destination_state<Br/>$destination_zip</td></tr>".
		"<tr><td width='200'>-----</td><td></td></tr>".
		"<tr><td width='200'>Estimated Move Date:</td><td> $month/$day/$year </td></tr>".
		"<tr><td width='200'>Size of Move:</td><td>$size_of_move</td></tr>".
		"<tr><td width='200'>Labor Needs:</td><td>".getMovingLaborType($moving_staffer)."</td></tr>".
		"<tr><td colspan='2'>Customer Comments:</td></tr>".
		"<tr><td colspan='2'>$comments</td></tr></table></body></html>";
		
		return $htmlmsg;
	}

	function getTextMessage($array)
	{
		extract($array);
		$p_area=substr($phone1,0,3);
    	$p_prefix=substr($phone1,3,3);
    	$p_suffix=substr($phone1,6,4);
		$parts = split("-",$estimated_move_date);
		
		$year=$parts[2];
    	$month=$parts[0];
	    $day=$parts[1];
		echo "Month: $month<Br/>\n";
		$month = get_month_name($month);
		echo "Month: $month<Br/>\n";
		$format = "Full Name: $fname $lname\n\n".
		"Email Address: $email\n\n".
		"Daytime Phone: $p_area-$p_prefix-$p_suffix\n\n".
		"Origin City: $origin_city\n\n".
		"Origin State: $origin_state - ".get_state_name($origin_state)."\n\n".
		"Destination City: $destination_city\n\n".
		"Destination State: $destination_state - ".get_state_name($destination_state)."\n\n".
		"Approx Size: $size_of_move\n\n".
		"Moving Month: ".$month."\n\n".
		"Moving Day: $day\n\n".
		"Moving Year: $year\n\n";
		return $format;
	}
		
	function getMovingStaffersQuote($array)
	{
		$email = "leads@movingstaffers.com";
		return getTextMessage($array);		
	}
		
	function getGoSmartMoveXML($array)
	{
		$email = "philip@nestingen.com";//not right, just a temp for now.
		
		
		
		if(strlen($array["phone2"]) == 10)
		{			
			$p2 = formatPhone($array["phone2"]);
		}
		
		if ($array["size_of_move"] == "4+")
		{
			$size = "4 or more Bedrooms";
		}
		else
			$size = $array["size_of_move"];
			
		$msg = "<?xml version='1.0' encoding='iso-8859-1'?>
	<Lead_Household>
	<companyName>Smart Move</companyName>
	<Lead_ID>".$array["quote_id"]."</Lead_ID>
	<Estimated_Move_Date>".str_replace("-","/",$array["estimated_move_date"])."</Estimated_Move_Date>
	<FirstName>".xmlentities(ucwords($array["fname"]))."</FirstName>
	<LastName>".xmlentities(ucwords($array["lname"]))."</LastName>
	<Email>".xmlentities($array["email"])."</Email>
	<Eve_Phone>".formatPhone($array["phone1"])."</Eve_Phone>
	<Day_Phone>$p2</Day_Phone>
	<Moving_From_City>".xmlentities(strtoupper($array["origin_city"]))."</Moving_From_City>
	<Moving_From_State>".strtoupper($array["origin_state"])."</Moving_From_State>
	<Moving_From_Zip>".strtoupper($array["origin_zip"])."</Moving_From_Zip>
	<Moving_To_City>".xmlentities(strtoupper($array["destination_city"]))."</Moving_To_City>
	<Moving_To_State>".strtoupper($array["destination_state"])."</Moving_To_State>
	<Moving_To_Zip>".strtoupper($array["destination_zip"])."</Moving_To_Zip>
	<Rooms>".xmlentities($size)."</Rooms>
	<Comments>".xmlentities(ucwords($array["comments"]))."</Comments>
	<DateSubmitted>".getReceived($array["received"])."</DateSubmitted>
	</Lead_Household>";
		return $msg;
	}
	
	function getDoor2DoorMessage($array)
	{		
		$msg = "Name: ".$array["fname"]." ".$array["lname"]."\n\n".
		"Email: ".$array["email"]."\n\n".
		"Phone: ".formatPhone($array["phone1"])."\n\n".
		"Moving\n\n-----------------------\n\n".
		"Estimated Move Date: ".$array["estimated_move_date"]."\n\n".
		"Moving From: ".$array["origin_city"].", ".$array["origin_state"]."  ".$array["origin_zip"]."\n\n".
		"Moving To: ".$array["destination_city"].", ".$array["destination_state"]."  ".$array["destination_zip"]."\n\n".
		"Residence Type: house\n\n".
		"Number of Rooms: ".$array["size_of_move"];
		return $msg;
	}
	
	function getEmailMessage($array)		
	{
		extract($array);
		
		$p_area=substr($phone1,0,3);
    	$p_prefix=substr($phone1,3,3);
    	$p_suffix=substr($phone1,6,4);
		$p2_area=substr($phone2,0,3);
    	$p2_prefix=substr($phone2,3,3);
    	$p2_suffix=substr($phone2,6,4);
		$parts = split("-",$estimated_move_date);
		
		$year=$parts[2];
    	$month=$parts[0];
	    $day=$parts[1];
		echo "Month: $month<Br/>\n";
		$month = get_month_name($month);
		echo "Month: $month<Br/>\n";
		$format = "Full Name: $fname $lname\n\n".
		"Email Address: $email\n\n".
		"Daytime Phone: $p_area-$p_prefix-$p_suffix\n\n".
		"Alternate Phone: $p2_area-$p2_prefix-$p2_suffix\n\n".
		"Origin City: $origin_city\n\n".
		"Origin State: $origin_state - ".get_state_name($origin_state)."\n\n".
		"Origin Zip: ".$origin_zip."\n\n".
		"Destination City: $destination_city\n\n".
		"Destination State: $destination_state - ".get_state_name($destination_state)."\n\n".
		"Destination Zip: ".$destination_zip."\n\n".
		"Approx Size: $size_of_move\n\n".
		"Needs: " . ucwords($needs) . "\n\n".
		"Moving Month: ".$month."\n\n".
		"Moving Day: $day\n\n".
		"Moving Year: $year\n\n".
		"Comments: $comments\n\n";
		return $format;
		
	}
	
	
	function getMovexMessage($array)
	{
		return getTextMessage($array);
		$email = "saleslogix@movex.com";
	}
	
	
	/*
		http://www.upack.com/leads/
	
		Here is the information needed to pass ABF U-Pack Moving a direct lead. Basically, 
		we have a web page that is hit with a formatted querystring. Below is the list of
		querystring values that we accept to receive a quote into our system directly from
		your web site.
		
		ID 	Referral ID (Assigned by ABF U-Pack Moving)
		Name 	Customers name (First and Last)
		HomePhone 	Customers home phone number
		WorkPhone 	Customers work phone number
		Email 	Customers email address
		BestTime 	Best time to contact the customer (nights, weekends, any time, etc.)
		FromCity 	City that the customer is moving from
		FromState 	State that the customer is moving from
		FromZip 	Zip code that the customer is moving from
		ToCity 	City that the customer is moving to
		ToState 	State that the customer is moving to
		ToZip 	Zip code that the customer is moving to
		ContactBy 	Best way to contact them, home phone, work phone, e-mail.
		MoveDate 	Date that the customer wants to, preferred format MM/DD/YYYY.
		Weight 	Approximate weight of household goods in pounds
		Residence 	What type of residence the customer is moving out of (Apartment 1 Bdr, House 3 Bdr, Storage Bldg, etc.)
		Notes 	Any notes/comments that the customer would like to give (competitors price, special needs, extra services)
		
		Example Querystring:

		http://www.upack.com/ref_quote.asp?ID=COMPANYID&Name=John+Doe &HomePhone=5555555555&
		WorkPhone=9999999999&Email=johndoe@user.com &BestTime=Any+Time&FromCity=Denver&FromState=CO&
		FromZip=80202 &ToCity=Fort+Smith&ToState=AR&ToZip=72903&MoveDate=05/12/2006 &Weight=4000&
		Residence=Apartment+1+Bdr&ContactBy=Email &Notes=Please+give+me+a+good+price. 
		
	*/
	function getABFQueryMessage($array)
	{
		$base = "http://www.upack.com/ref_quote.asp?ID=SELFMVAUTO";//doesnt change.
		extract($array);
		$base .= "&BestTime=Any+Time&Weight=4000&ContactBy=Email";//possibly change weight, depending on size..
		$base .= "&Name=".urlencode("$fname $lname");
		$base .= "&HomePhone=$phone1";
		if (strlen($phone2) > 0)
			$base .= "&WorkPhone=$phone2";
		$base .= "&Email=".urlencode($email);			
		
		$base .= "&FromCity=".urlencode($origin_city);
		$base .= "&FromState=".urlencode($origin_state);
		$base .= "&FromZip=".urlencode($origin_zip);
		
		$base .= "&ToCity=".urlencode($destination_city);
		$base .= "&ToState=".urlencode($destination_state);
		$base .= "&ToZip=".urlencode($destination_zip);
		$base .= "&MoveDate=".str_replace("-","/",$estimated_move_date);		
		$base .= "&Residence=".urlencode($size_of_move);## TEMPORARY FIX!!		
		$base .= "&Notes=".urlencode($comments);
		
		return $base;
	}

	updateCronStatus("selfmove", "ended");
	
	checkCronStatus("security","self");
	//checkCronStatus("main","self");
	checkCronStatus("local","self");
?>