<?
	include_once("../inc_mysql.php");	
	define(PACKPRO,9000);
	define(DOOR2DOOR,9001);//zip code list.
	define(MOVEX,9002);
	define(GOSMARTMOVE,9003);//zip code list.
	define(ABFUPACK,9004);
			
	//run();
	
	function sendLead($msg, $email="X")
	{
		if ($email == "X")
		{
			//simple curl post, only for ABF.
			$ch=curl_init("$msg");				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			mail("david@irelocation.com","Self Moving Quote","ABF URL:\n\n".$msg,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");				
		}
		else
		{
			mail($email,"Self Moving Quote",$msg,"From: The iRelocation Network <no_reply@irelocation.com>\n");	
			mail("david@irelocation.com","Self Moving Quote","To: $email\n".$msg,"From: The iRelocation Network <no_reply@irelocation.com>\n");	
		}
	}
	
	function getGoSmartMoveXML($array)
	{
		$email = "philip@nestingen.com";//not right, just a temp for now.
		
		$p_area=substr($array["phone1"],0,3);
    	$p_prefix=substr($array["phone1"],3,3);
    	$p_suffix=substr($array["phone1"],6,4);
		
		if(strlen($array["phone2"]) == 10)
		{
			$p2_area=substr($array["phone2"],0,3);
			$p2_prefix=substr($array["phone2"],3,3);
			$p2_suffix=substr($array["phone2"],6,4);
			$p2 = "$p2_area-$p2_prefix-$p2_suffix";
		}
		
		if ($array["size_of_move"] == "4+")
		{
			$size = "4 or more Bedrooms";
		}
		else
			$size = $array["size_of_move"];
		$msg = "<?xml version='1.0' encoding='iso-8859-1'?>
	<Lead_Household>
	<companyName>Smart Move</companyName>
	<Lead_ID>".$array["quote_id"]."</Lead_ID>
	<Estimated_Move_Date>".str_replace("-","/",$array["estimated_move_date"])."</Estimated_Move_Date>
	<FirstName>".xmlentities(ucwords($array["fname"]))."</FirstName>
	<LastName>".xmlentities(ucwords($array["lname"]))."</LastName>
	<Email>".xmlentities($array["email"])."</Email>
	<Eve_Phone>$p_area-$p_prefix-$p_suffix</Eve_Phone>
	<Day_Phone>$p2</Day_Phone>
	<Moving_From_City>".strtoupper($array["origin_city"])."</Moving_From_City>
	<Moving_From_State>".strtoupper($array["origin_state"])."</Moving_From_State>
	<Moving_From_Zip>".strtoupper($array["origin_zip"])."</Moving_From_Zip>
	<Moving_To_City>".strtoupper($array["destination_city"])."</Moving_To_City>
	<Moving_To_State>".strtoupper($array["destination_state"])."</Moving_To_State>
	<Moving_To_Zip>".strtoupper($array["destination_zip"])."</Moving_To_Zip>
	<Rooms>".xmlentities($size)."</Rooms>
	<Comments>".xmlentities(ucwords($array["comments"]))."</Comments>
	<DateSubmitted>".getReceived($array["received"])."</DateSubmitted>
	</Lead_Household>";
		return $msg;
	}
	
	
	function run()
	{
		//$mytime = updateReceived();//set the timestamp of new leads.		
		$sql = "select * from irelocation.leads_selfmove where scrub_is_valid = 1;";
		$rs = new mysql_recordset($sql);	
		
		while($rs->fetch_array())	
		{			
			#
			# 	Movex
			#
				$array = $rs->myarray;
				$email = "saleslogix@movex.com";
				$msg = getMovexMessage($array);
				sendLead($msg,$email);
				echo "<br/>\nEmail to : $email<Br/>\n";
				
		}
	}
		
	#------------------------------------------------------------------------
	#
	#		Update Lead ids after the lead has been sent out to all peoples.
	#
	#------------------------------------------------------------------------
	
	function updateLeadIds($ids, $quote_id)
	{		
		$sql="update irelocation.leads_selfmove set lead_ids='".$ids."' where quote_id = $quote_id";
		echo $sql."<Br/>\n";
		$rs=new mysql_recordset($sql);
	}	
	
	#------------------------------------------------------------------------
	#
	#		Update received stamp, this all returns the timestamp
	#		Leads with this timestamp will be sent out.
	#
	#------------------------------------------------------------------------
	
	
	function checkZipCodes($zip1,$zip2,$lead_id)
	{
		$sql = "select * from selfmoving_zips where (zip = '$zip1' or zip = '$zip2') AND lead_ids like '%$lead_id%'";
		$rs=new mysql_recordset($sql);
		return ($rs->rowcount() >= 2);
	}
	
	function updateReceived()
	{
		$mytime = date("YmdHis");
		$sql="update irelocation.leads_selfmove set received='".$mytime."' where (received='' or received='0') ";//and LOWER(fname) != 'test' ";
		echo $sql."<Br/>\n";
		$rs=new mysql_recordset($sql);
		return $mytime;
	}
	
	function getTextMessage($array)
	{
		extract($array);
		$p_area=substr($phone1,0,3);
    	$p_prefix=substr($phone1,3,3);
    	$p_suffix=substr($phone1,6,4);
		$parts = split("-",$estimated_move_date);
		
		$year=$parts[2];
    	$month=$parts[0];
	    $day=$parts[1];
		echo "Month: $month<Br/>\n";
		$month = get_month_name($month);
		echo "Month: $month<Br/>\n";
		$format = "Full Name: $fname $lname\n\n".
		"Email Address: $email\n\n".
		"Daytime Phone: $p_area-$p_prefix-$p_suffix\n\n".
		"Origin City: $origin_city\n\n".
		"Origin State: $origin_state - ".get_state_name($origin_state)."\n\n".
		"Destination City: $destination_city\n\n".
		"Destination State: $destination_state - ".get_state_name($destination_state)."\n\n".
		"Approx Size: 3 Bedrooms\n\n".
		"Moving Month: ".$month."\n\n".
		"Moving Day: $day\n\n".
		"Moving Year: $year\n\n";
		return $format;
	}
		
	function getMovingStaffersQuote($array)
	{
		$email = "leads@movingstaffers.com";
		return getTextMessage($array);		
	}
	
	
	function getDoor2DoorMessage($array)
	{		
		$msg = "Name: ".$array["fname"]." ".$array["lname"]."\n\n".
		"Email: ".$array["email"]."\n\n".
		"Phone: ".formatPhone($array["phone1"])."\n\n".
		"Moving\n\n-----------------------\n\n".
		"Estimated Move Date: ".$array["estimated_move_date"]."\n\n".
		"Moving From: ".$array["origin_city"].", ".$array["origin_state"]."  ".$array["origin_zip"]."\n\n".
		"Moving To: ".$array["destination_city"].", ".$array["destination_state"]."  ".$array["destination_zip"]."\n\n".
		"Residence Type: house\n\n".
		"Number of Rooms: ".$array["size_of_move"];
		return $msg;
	}
	
	function getPackProMessage($array)		
	{
		$email = "smq@packpro.com";
		return getTextMessage($array);	
	}
	function getMovexMessage($array)
	{
		return getTextMessage($array);
		$email = "saleslogix@movex.com";
	}
	
	
	/*
		http://www.upack.com/leads/
	
		Here is the information needed to pass ABF U-Pack Moving a direct lead. Basically, 
		we have a web page that is hit with a formatted querystring. Below is the list of
		querystring values that we accept to receive a quote into our system directly from
		your web site.
		
		ID 	Referral ID (Assigned by ABF U-Pack Moving)
		Name 	Customers name (First and Last)
		HomePhone 	Customers home phone number
		WorkPhone 	Customers work phone number
		Email 	Customers email address
		BestTime 	Best time to contact the customer (nights, weekends, any time, etc.)
		FromCity 	City that the customer is moving from
		FromState 	State that the customer is moving from
		FromZip 	Zip code that the customer is moving from
		ToCity 	City that the customer is moving to
		ToState 	State that the customer is moving to
		ToZip 	Zip code that the customer is moving to
		ContactBy 	Best way to contact them, home phone, work phone, e-mail.
		MoveDate 	Date that the customer wants to, preferred format MM/DD/YYYY.
		Weight 	Approximate weight of household goods in pounds
		Residence 	What type of residence the customer is moving out of (Apartment 1 Bdr, House 3 Bdr, Storage Bldg, etc.)
		Notes 	Any notes/comments that the customer would like to give (competitors price, special needs, extra services)
		
		Example Querystring:

		http://www.upack.com/ref_quote.asp?ID=COMPANYID&Name=John+Doe &HomePhone=5555555555&
		WorkPhone=9999999999&Email=johndoe@user.com &BestTime=Any+Time&FromCity=Denver&FromState=CO&
		FromZip=80202 &ToCity=Fort+Smith&ToState=AR&ToZip=72903&MoveDate=05/12/2006 &Weight=4000&
		Residence=Apartment+1+Bdr&ContactBy=Email &Notes=Please+give+me+a+good+price. 
		
	*/
	function getABFQueryMessage($array)
	{
		$base = "http://www.upack.com/ref_quote.asp?ID=SELFMVAUTO";//doesnt change.
		extract($array);
		$base .= "&BestTime=Any+Time&Weight=4000&ContactBy=Email";//possibly change weight, depending on size..
		$base .= "&Name=".urlencode("$fname $lname");
		$base .= "&HomePhone=$phone1";
		if (strlen($phone2) > 0)
			$base .= "&WorkPhone=$phone2";
		$base .= "&Email=".urlencode($email);			
		
		$base .= "&FromCity=".urlencode($origin_city);
		$base .= "&FromState=".urlencode($origin_state);
		$base .= "&FromZip=".urlencode($origin_zip);
		
		$base .= "&ToCity=".urlencode($destination_city);
		$base .= "&ToState=".urlencode($destination_state);
		$base .= "&ToZip=".urlencode($destination_zip);
		$base .= "&MoveDate=".str_replace("-","/",$estimated_move_date);		
		$base .= "&Residence=".urlencode($size_of_move);## TEMPORARY FIX!!		
		$base .= "&Notes=".urlencode($comments);
		
		return $base;
	}

	#
	#
	#	HELPER FUNCTIONS
	#
	#
	#
	
	function getMoveDate($date)
	{
		str_replace("-","/",$date);
		
	
	}
	
	function get_month_name($month)
	{
		$month = intval($month);
	
		switch ($month)
		{
			case 1: return "January";
			case 2: return "February";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";
		}
	}
	
	#**************************************
	function get_state_name($state)
	{
	  switch($state)
	  {
		case "AK": $mystate="Alaska"; break;
		case "AL": $mystate="Alabama"; break;
		case "AR": $mystate="Arkansas"; break;
		case "AZ": $mystate="Arizona"; break;
		case "CA": $mystate="California"; break;
		case "CO": $mystate="Colorado"; break;
		case "CT": $mystate="Connecticut"; break;
		case "DC": $mystate="Washington D.C."; break;
		case "DE": $mystate="Delaware"; break;
		case "FL": $mystate="Florida"; break;
		case "GA": $mystate="Georgia"; break;
		case "HI": $mystate="Hawaii"; break;
		case "IA": $mystate="Iowa"; break;
		case "ID": $mystate="Idaho"; break;
		case "IL": $mystate="Illinois"; break;
		case "IN": $mystate="Indiana"; break;
		case "KS": $mystate="Kansas"; break;
		case "KY": $mystate="Kentucky"; break;
		case "LA": $mystate="Louisiana"; break;
		case "MA": $mystate="Massachusetts"; break;
		case "MD": $mystate="Maryland"; break;
		case "ME": $mystate="Maine"; break;
		case "MI": $mystate="Michigan"; break;
		case "MN": $mystate="Minnesota"; break;
		case "MO": $mystate="Missouri"; break;
		case "MS": $mystate="Mississippi"; break;
		case "MT": $mystate="Montana"; break;
		case "NC": $mystate="North Carolina"; break;
		case "ND": $mystate="North Dakota"; break;
		case "NE": $mystate="Nebraska"; break;
		case "NH": $mystate="New Hampshire"; break;
		case "NJ": $mystate="New Jersey"; break;
		case "NM": $mystate="New Mexico"; break;
		case "NV": $mystate="Nevada"; break;
		case "NY": $mystate="New York"; break;
		case "OH": $mystate="Ohio"; break;
		case "OK": $mystate="Oklahoma"; break;
		case "OR": $mystate="Oregon"; break;
		case "PA": $mystate="Pennsylvania"; break;
		case "RI": $mystate="Rhode Island"; break;
		case "SC": $mystate="South Carolina"; break;
		case "SD": $mystate="South Dakota"; break;
		case "TN": $mystate="Tennessee"; break;
		case "TX": $mystate="Texas"; break;
		case "UT": $mystate="Utah"; break;
		case "VA": $mystate="Virginia"; break;
		case "VT": $mystate="Vermont"; break;
		case "WA": $mystate="Washington"; break;
		case "WI": $mystate="Wisconsin"; break;
		case "WV": $mystate="West Virginia"; break;
		case "WY": $mystate="Wyoming"; break;
	  }
	  return $mystate;
	}

	// XML Entity Mandatory Escape Characters
	function xmlentities ( $string )
	{
	   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
	}	

	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "$p_area-$p_prefix-$p_suffix";
		}
		else
			return $phone;
	}
	
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		if ($r_hour > 11)
		{
			$pm = true;
			if ($r_hour > 12)
				$r_hour -= 12;
		}
		$msg = "$r_month/$r_day/$r_year $r_hour:$r_min";
		if ($pm)
			$msg .= " PM";
		else
			$msg .= " AM";
		return $msg;
	}	
?>
