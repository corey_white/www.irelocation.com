<?php 

/*
This script will receive a lead from ClickGen, capture it and insert into a Db table, then send it out to Hadhaus Call Center.

Sample GET URL:

http://www.irelocation.com/clickgen_import.php?FirstName=Bob&LastName=Smith&EmailAddress=bob.smith%40domain.com&Phone=8887779999&Zipcode=85258&OwnRent=own

*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$EmailAddress = $_GET['EmailAddress'];
$Phone = $_GET['Phone'];
$Zipcode = urlencode($_GET['Zipcode']);
$OwnRent = urlencode($_GET['OwnRent']);


$MSG_BODY = "Data from Import:\n
FirstName = $FirstName
LastName = $LastName
EmailAddress = $EmailAddress
Phone = $Phone
Zipcode = $Zipcode
OwnRent = $OwnRent";


echo nl2br($MSG_BODY);

/*

//-- Build Bayshore Body
$bayshore_body = "xmlRequest=<EstimateRequest><ReferralCode>IRELO01861</ReferralCode><PrimaryContact><Email>$EmailAddress</Email><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><PrimaryPhoneType>H</PrimaryPhoneType><PreferredContactTime>E</PreferredContactTime><HomePhone>$HomePhone</HomePhone><WorkPhone>$WorkPhone</WorkPhone><WorkPhoneExt></WorkPhoneExt><CellPhone></CellPhone><FaxPhone></FaxPhone></PrimaryContact><PickupAddress><Address1>Ask Customer</Address1><Address2></Address2><City>$FromCity</City><State>$FromState</State><Zip>$FromZip</Zip></PickupAddress><MoveDetails><PickupZip>$FromZip</PickupZip><DeliveryZip>$ToZip</DeliveryZip><MoveDate>$MoveDate</MoveDate><DwellingType>$MoveSize</DwellingType><AmountOfFurnishings>M</AmountOfFurnishings><PickupShuttle>N</PickupShuttle><DeliveryShuttle>N</DeliveryShuttle></MoveDetails><EstimateDetails><HasVehicles>N</HasVehicles><RequestedEstimateDate>3D</RequestedEstimateDate><RequestedEstimateTimeOfDay>E</RequestedEstimateTimeOfDay><SpecialtyItems></SpecialtyItems><Comments>$Comments</Comments></EstimateDetails></EstimateRequest>";

//-- send to Bayshore
$url = "http://ballpark.allied.com/RAE/RequestAnEstimate.asmx/SendEstimateRequestXmlString";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: 100'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://ballpark.allied.com/RAE/SendEstimateRequestXml"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($bayshore_body));

$response = curl_exec($ch);
curl_close($ch);


#mail("rob@irelocation.com","BAYSHORE LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$bayshore_body");

echo $response;

*/


?>