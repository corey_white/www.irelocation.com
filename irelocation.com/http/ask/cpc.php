<?php

	include "../inc_mysql.php";
	include('../nusoap/nusoap.php');

	define(LIVE,true);
	
	if (LIVE)
	{
		define(TOKEN,"5b78623b42008f22e198ec3dd61be354");
		define(URL,"https://api-secure.sponsoredlistings.ask.com/api/v2/reports/service.wsdl");
	}
	else
	{
		define(TOKEN,"39c9751fd8d6e7f80fe41cccc949d3df");
		define(URL,"https://api-sandbox.sponsoredlistings.ask.com/api/v2/reports/service.wsdl");
	}
	//Campaign ID's
	define(PROMOVING,493301);
	define(TOPSECURITYCOMPANIES,542197);
	define(TOPMOVING,468474);
//	define(IRELOCATION,492162); //NOTHING HERE...
	define(AUTOSHIPPING,492138);
	//easy loop-style access..
	$accounts = array(PROMOVING,TOPSECURITYCOMPANIES,TOPMOVING,AUTOSHIPPING);
	//hash of sql names.
	$account_names = array(PROMOVING => "pm",TOPSECURITYCOMPANIES => "tsc",
							TOPMOVING => "tm",AUTOSHIPPING => "asc");
	
	// Pull in the NuSOAP code


	$date = date("Y-m-d",time()-24*60*60);
	$ts = date("Ymd",time()-24*60*60);

	$sql_check = "select distinct(site) from movingdirectory.costperclick where search_engine = 'ask' and ts = '$ts' ";
	$rs = new mysql_recordset($sql_check);
	echo "Previous Sites: <strong>";
	while ($rs->fetch_array())
	{
		$sites[] = $rs->myarray['site'];
		echo $rs->myarray['site']." ";
	}	
	echo "</strong><br/>";
	if (count($sites) == count($accounts))
	{
		echo "All Ask.com Data has been pulled in for this day.";
		exit();
	}
	else
	{
		if (is_array($sites) && count($sites) > 0)
			$sites_already_have = implode(",",$sites);
		else
			$sites_already_have = "";
		
		$accountclient = new soapclient(URL, true);
		$accountclient->soap_defencoding = 'utf-8';
		$proxy = $accountclient->getProxy();
	
		echo "Gathering data for $date <br/>";
		$sql = "insert into movingdirectory.costperclick (search_engine,site,cost,clicks,ts) values ";
		foreach($accounts as $account)
		{
			$account_name = $account_names[$account];
			if (substr_count($sites_already_have,$account_name) == 0)
			{
				$result = $proxy->getCampaignReport(TOKEN, $account, $date , $date);
				if (is_array($result) && count($result) == 9)
					$sql .= "\n('ask','".$account_name."','".$result[cost]."','".$result[clicks]."','".$ts."'), ";	
			}
			else
			{
				echo "Site Data already retreived for <strong>$account_name</strong><br/>\n";
			}
		}
	
		$sql = substr($sql,0,strlen($sql)-2);
		echo "<pre>$sql</pre>";
		
		$rs = new mysql_recordset($sql);
		echo "Sql Statement Executed. CPC Data Saved.";
	}
	
	/*
	Array
	(
		[account_id] => 485135
		[budget] => 166.67
		[reference_number] => 
		[max_cpc] => 3.5
		[cost] => 501.72
		[campaign_id] => 493301
		[campaign_state] => on
		[clicks] => 1039
		[name] => Promoving
	)
	*/

?>


