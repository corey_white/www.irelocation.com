<?
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	
	extract($_POST);
	
	/*
	$cat_id [1-4]
	$lead_id [0-9]{4,4}
	$quote_id [0-9]{4,*}
	*/
	
	if ($cat_id != "" && is_numeric($cat_id) && 
		$lead_id != "" && is_numeric($lead_id) && 
		$quote_id != "" && is_numeric($quote_id))
	{
		$when = date("YmdHis");
		$sql = " insert into movingdirectory.lead_receipts ".
				"(cat_id,lead_id,quote_id,received) values ".
				"($cat_id,$lead_id,$quote_id,'$when');";
		$rs = new mysql_recordset($sql);
		$id = $rs->last_insert_id();
		$rs->close();
		
		echo "Accepted:$id";	
	}
	else
	{
		echo "Rejected<Br/>\n";	
		?>
<Strong>Error: Required Fields Missing</Strong>
<ul>
<li><strong>cat_id</strong>: category numeric id for the lead</li>
<li><strong>lead_id</strong>: your unique company numeric id</li>
<li><strong>quote_id</strong>: a unique numeric id we provide for each quote</li>
</ul>					
		<?	
	}
?>