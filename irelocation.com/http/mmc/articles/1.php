<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td><p class="heading">What You Should Know About Car Shipping</p>
      <p class="maintext">For many of us a move simply means that we get in our 
        cars and drive from our old home to our new one. In certain cases, however, 
        such as long-distance or international moves we need to get our vehicles 
        moved for us separately. Some of us will also be looking to move RVs, 
        caravans, motorhomes, motorcycles, boats and other vehicles. Chances are 
        we'll have to get the experts in rather than do it ourselves.<br>
        <br>
        Moving vehicles is an expert job and can be a simple and secure process 
        in the right hands. Your first task should be to find a suitably qualified 
        company that can manage your move for you. As with all moving specialists 
        you should be looking for one that has done it before. Depending on your 
        individual needs you might be looking for companies with experience of 
        moving boats, large RVs, motorcycles, motor homes or even moving internationally. 
        Some vehicles such as boats need special lifting equipment to put them 
        on to their moving trailer so you do really need to deal with a company 
        that can offer this to minimize the chances of something going wrong. 
        Moving within your home country is generally simple enough but an international 
        move may require special skills. You'll need to find someone that can 
        help you with the documentation, fees, restrictions and regulations of 
        your chosen destination country as well as get your vehicle there in one 
        piece.<br>
        <br>
        The key thing to remember with vehicle movers is that it isn't always 
        just a case of somebody driving your vehicle from A to B. Most companies 
        use large vehicle transporters for standard-sized vehicles and your car 
        may go all over the country delivering other loads before it reaches your 
        new home. In some countries the transporter will also have to go through 
        spot checks at specific points. And, the position of your vehicle on the 
        transporter can also dictate when you get it. This can all add to the 
        time it takes to deliver the vehicle. So, don't be surprised if you have 
        to wait days rather than hours for it to arrive. Your chosen company will 
        advise on whether they can give a fixed delivery date or an estimate within 
        a fixed period of a few days. You may also find that the mover can't guarantee 
        a door to door service for the transportation. The vehicles they use are 
        often large and find it hard to negotiate residential streets. You may 
        be offered an alternative of a pick-up point or the driver may use his 
        judgement to unload the car in a safe and suitable location close to your 
        home.<br>
        <br>
        The types of transporter used will vary from company to company and according 
        to the vehicle you need to move. Options include closed transporters, 
        open transporters, simple trucks, trailers and, in the cases of international 
        moves, freight containers. There really isn't much to choose between the 
        options. You can expect your vehicle to be firmly attached to its allocated 
        space and secured against damage no matter how it is transported. Some 
        people do prefer to use a closed vehicle as it means that your vehicle 
        isn't exposed to the elements but this is up to your personal preferences. 
        Your primary concerns should be the safety and security of your vehicle 
        - talk to your chosen mover in-depth about how they transport and what 
        measures they take to protect vehicles.<br>
        <br>
        Before you choose a provider you'll also need to talk to them about insurance 
        cover to check exactly how protected your vehicle is. Also take the time 
        to talk to your vehicle insurer to assess your coverage during transit. 
        In both cases you're looking to ferret out exclusions and restrictions 
        so you can make sure you have the fullest possible coverage and you avoid 
        nasty surprises should an accident occur. Once again, check out the small 
        print of the company's terms and conditions carefully BEFORE you agree 
        to hire them! You may need to pay more for cover that suits the value 
        of your vehicle.<br>
        <br>
        Once you've chosen your vehicle mover you'll need to start preparing your 
        vehicle. Most companies will check your vehicle out before they load it 
        for existing damage and so forth. They will then agree the current state 
        of the vehicle with you (you do need to be present for this) and you'll 
        sign an agreement that the check has been accurately done. This covers 
        them in the event that you need to make a damage claim after the move. 
        It also covers you if they do damage your vehicle! It's a good tip to 
        wash your vehicle before they do this check so that they can see exactly 
        what it looks like! Finally, you should talk to the company before they 
        take your vehicle to check exactly what they need you to do to it before 
        it's loaded. General tips include:
      <ul class="maintext">
        <li> Empty out all of your personal possessions, documents etc including 
          those you store in the trunk - many companies are not insured for damage 
          or loss of personal goods.<br>
        </li>
        <li> For larger RVs or mobile homes batten down the hatches and make sure 
          everything is as secure as possible!<br>
        </li>
        <li> Make sure you have enough antifreeze in the engine and check your 
          battery is fixed in place.<br>
        </li>
        <li> If your vehicle is going by transporter or trailer empty out most 
          of your fuel. In some countries car transporters are weighed at checkpoints 
          and they can be delayed or fined for being overweight. Losing a bit 
          of fuel can be a real help!<br>
        </li>
        <li> If you have customized your vehicle at all with special aerials, 
          antennae, spoilers and so on remove them or make sure they are secure. 
          You basically want the vehicle as streamlined as possible to avoid knocks 
          and damage.<br>
        </li>
        <li>Finally, turn off any alarm systems.<br>
        </li>
      </ul>
      
      </p></td>
  </tr>
</table>
