<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p class="heading">Preparing Your Car</p>
      <p class="maintext">Most companies will not allow you to carry more than 
        100 lbs of personal belongings in the vehicle during shipping, and it 
        more than likely has to be in the trunk. Personal items inside the vehicle 
        will not be covered if lost or stolen.<br>
        <br>
        Make sure you inspect your vehicle with the shipping company. All exterior 
        and interior defects should be noted. If no inspection is done, then the 
        transport company probably won't be liable for any damages. Also, when 
        you pick up your vehicle at your destination, you will do another inspection. 
        Make sure the transport company washes off your vehicle so that you can 
        do a proper inspection. Do not pick up your car if you are in a rush!<br>
        <br>
        Make sure that you vehicle is personally insured. Most transport companies 
        will not repair damages due to normal road conditions. For example, if 
        your vehicle is struck while sitting on a lot, even under the control 
        of the carrier, your insurance would have to cover the damages. If the 
        vehicle was damaged directly by the carrier, then the carrier would be 
        responsible.<br>
        <br>
        It is a good idea to have your vehicle serviced and the gas tank low (not 
        empty) prior to transport.<br>
        <br>
        There will be an extra charge if your vehicle is inoperable, or if there 
        are any other additions to your vehicle such as camper shells, lifted/oversized 
        trucks, and other things that you did not tell the company prior to shipping. 
        Make sure you specify any and all modifications to your vehicle for a 
        proper quote.<br>
        <br>
        Make sure that you have a complete set of keys for the transport company, 
        and a full extra set for yourself.<br>
        <br>
        If you are transporting a classic, custom, rare, exotic, or otherwise 
        expensive vehicle, go for the enclosed transport if you can... it will 
        be worth the extra cost!</p>
      <p></p>
      <p class="heading">&nbsp; </p>
      </td>
  </tr>
</table>
