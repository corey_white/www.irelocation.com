<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p class="heading">Auto Shipping Delivery Dates</p>
      <p class="maintext">Ask for estimated pick-up and delivery times from your 
        car transport company. You should be able to get a 3-day window for pick-up 
        &#8211; during the busy summer or snow bird season the window may be larger, 
        but the car transport company should warn you of possible delays ahead 
        of time. <br>
        <br>
        BE SKEPTICAL OF EXACT PICK-UP AND DELIVERY DATES PROMISED BY CAR TRANSPORT 
        COMPANIES &#8211; IF YOU ARE PROMISED EXACT DATES, GET IT IN WRITING AND 
        STATE THE REMEDY IF SERVICE IS NOT PERFORMED ON PROMISED DATES <br>
        <br>
        ($50 discount, no fee to cancel, reimbursement for rental car, etc.) . 
        <br>
        <br>
        Once pick-up is made, your car transport company should provide you with 
        a fairly precise delivery date. If you need an exact date for pick-up, 
        most car transport services can pick-up your car and hold it at a terminal 
        for an additional charge. </p>
      </td>
  </tr>
</table>
