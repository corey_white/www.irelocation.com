<?
$h1="Car Shipping Company";
$h2="enclosed carrier quote";
$desc="If you want to make sure that your car stays clean,
and avoids all highway wear and tear, then select a car
shipping company that offers an enclosed carrier service.
This option is best for expensive or rare vehicles.";
$quote_type="enclosed";
$dir="";
$title="Enclosed Carrier Quote";
$keywords="enclosed carrier car shipping quote";
$description="get quotes for enclosed car shipping services nationwide";
include($dir."header.php");
include($dir."quote.php");
include($dir."footer.php");
?>