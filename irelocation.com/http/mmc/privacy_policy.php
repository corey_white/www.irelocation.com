<table width="760" border="0" align="center" cellpadding="0" cellspacing="0" background="image/mainbg.jpg" bgcolor="#FFFFFF">
  <tr>
    <td align="center" valign="top"><table width="760" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="213" align="center" valign="top"><table width="186" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td align="center" valign="top"> 
                  <?include 'companies.php';?>
                  <br>
                </td>
              </tr>
              <tr> 
                <td align="center" valign="top"><img src="image/seal.jpg" alt="Auto Transport" width="142" height="156" hspace="5" vspace="10"></td>
              </tr>
              <tr>
                <td align="center" valign="top"><br>
                  <?include 'atr_link.php';?>
                  <br>
                  <br>
                </td>
              </tr>
            </table></td>
          <td width="547" align="center" valign="top"><table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr> 
                <td><p><font face="Tahoma" size="5">Privacy Statement </font></p>
                  <p><font size="2" face="Tahoma"><b>Our Commitment to Privacy:</b> 
                    Your privacy is important to us. To better protect your privacy 
                    we provide this notice explaining our online information practices 
                    and the choices you can make about the way your information 
                    is collected and used. To make this notice easy to find, we 
                    make it available on our homepage and at every point where 
                    personally identifiable information may be requested. </font></p>
                  <p><font size="2" face="Tahoma"><b>The Information We Collect:</b> 
                    This notice applies to all information collected or submitted 
                    on The iRelocation Network websites. You can visit The iRelocation 
                    Network websites without revealing any personal information. 
                    However, we do require certain personal information if you 
                    wish to register for a members account, receive newsletters, 
                    or to receive a quote for moving related services. On some 
                    pages, you can request to be contacted for the purpose of 
                    responding to questions, comments or information relating 
                    to our website and/or services. On some pages, you can register 
                    with our website if you would like to receive free quotes 
                    from our certified partners. The information you submit via 
                    the online quote form will not be used for any purpose unless 
                    you complete the registration form. </font></p>
                  <p><font size="2" face="Tahoma"><b>The Way We Use Information:</b> 
                    We use the information you provide about yourself and/or your 
                    company to respond to your questions and/or comments or to 
                    send to our certified partners to request free quotes. We 
                    use non-identifying and aggregate information to better design 
                    our website and to share with our affiliates/advertisers. 
                    For example, we may tell an advertiser that X number of individuals 
                    visited our website, or that Y number of individuals clicked 
                    on an advertiser's banner or link. We do not disclose anything 
                    that could be used to identify those individuals. Finally, 
                    we never use or share the personally identifiable information 
                    provided to us online in ways unrelated to the ones described 
                    above without also providing you an opportunity to opt-out 
                    or otherwise prohibit such unrelated uses. </font></p>
                  <p><font size="2" face="Tahoma"><b>Our Commitment to Data Security:</b> 
                    To prevent unauthorized access, maintain data accuracy, and 
                    ensure the correct use of information, we have put in place 
                    appropriate physical, electronic, and managerial procedures 
                    to safeguard and secure the information we collect online. 
                    </font></p>
                  <p><font size="2" face="Tahoma"><b>Our Commitment to Children's 
                    Privacy:</b> Protecting the privacy of the very young is especially 
                    important. For that reason, we never collect or maintain information 
                    at our website from those we actually know are under 13, and 
                    no part of our website is structured to attract anyone under 
                    13. </font></p>
                  <p><font size="2" face="Tahoma"><b>Our Service Mark Information:</b> 
                    TopMoving, ProMoving, CarShipping, CarShippingQuote, 1stMovingDirectory, 
                    USAutoTransport, and iRelocation are Service Marks of The 
                    iRelocation Network. All content, graphics and code included 
                    in the electronic web pages of The iRelocation Network, are 
                    the exclusive ownership of The iRelocation Network, All rights 
                    reserved. No part of The iRelocation Network websites may 
                    be reproduced in any way or by any means, including print 
                    and electronic, without the prior written permission of The 
                    iRelocation Network. The information provided herein may not 
                    be used to construct any mailing list, to create any promotional 
                    materials, to create any links, or for any other commercial 
                    purpose without the prior expressed written permission of 
                    The iRelocation Network. </font></p>
                  <p><font size="2" face="Tahoma"><b>Use Of The iRelocation Network 
                    Websites:</b> Use of The iRelocation Network websites are 
                    offered to you conditioned on your acceptance without modification 
                    of the terms, conditions, and notices contained herein. Your 
                    use of this website constitutes your agreement to all of the 
                    Terms and Conditions of Use. This website contains links to 
                    other web pages and websites operated either by The iRelocation 
                    Network or Third Parties. Use of this website is subject to 
                    the Terms and Conditions of Use as contained herein. You agree 
                    to review and abide by the Terms and Conditions of Use contained 
                    herein. If you do not consent to the collection, use or disclosure 
                    of your personal information as outlined in this statement, 
                    please do not provide any personal information. </font></p>
                  <p><font size="2" face="Tahoma"><b>Our Terms And Conditions 
                    Of Use:</b> This website is for your personal and non-commercial 
                    use. Except as expressly agreed herein, you may not display, 
                    reproduce, copy, modify, license, sell or disseminate in any 
                    manner any information obtained from this website. This website 
                    may contain inaccurate or incorrect information. The iRelocation 
                    Network may make modifications to the content presented on 
                    this website at any time without notice. You agree that no 
                    joint venture, partnership, employment, or agency relationship 
                    exists between you and The iRelocation Network as a result 
                    of use of this website or the existence of the Terms and Conditions 
                    of Use. Any rights not expressly granted herein are reserved. 
                    The iRelocation Network reserves the right to revise the Terms 
                    and Conditions and/or Privacy Policy at any time without notice 
                    by updating this posting. Use of any information or data provided 
                    by this website, without prior written permission by The iRelocation 
                    Network, is expressly prohibited and may result in civil and/or 
                    criminal action. Violators will be prosecuted to the maximum 
                    extent possible. Under no circumstances will The iRelocation 
                    Network be liable for any damages whatsoever resulting from 
                    loss of use or data, whether in an action of contract or in 
                    connection with the use or performance of information available 
                    from this website. Any action related to this website will 
                    be governed by Arizona law and controlling U.S. federal law. 
                    You hereby consent to the exclusive jurisdiction and venue 
                    of courts in Maricopa County, Arizona for all disputes arising 
                    out of or relating to this website. </font></p>
                  <p><font size="2" face="Tahoma"><b>Third Party Websites:</b> 
                    This website contains links to other Third Party websites, 
                    which are not operated by The iRelocation Network. These Third 
                    Party websites are not under the control of The iRelocation 
                    Network and The iRelocation Network is not responsible for 
                    the contents of any linked Third Party website or any link 
                    contained in a Third Party website. The iRelocation Network 
                    provides these links to you only as a convenience, and the 
                    inclusion of any link does not imply endorsement by The iRelocation 
                    Network. </font></p>
                  <p><font size="2" face="Tahoma"><b>Disclaimer:</b> The data 
                    that is provided by The iRelocation Network is intended solely 
                    for informational purposes. The iRelocation Network does not 
                    necessarily endorse or support any data provided by its affiliates/advertisers. 
                    Any products or services offered or sold by The iRelocation 
                    Network affiliates/advertisers are the sole responsibility 
                    of the affiliates/advertisers and are not offered or supported 
                    by The iRelocation Network. As The iRelocation Network websites 
                    are solely an information resource, The iRelocation Network 
                    is not responsible for the actual quality of any product or 
                    service offered by any iRelocation Network affiliates/advertisers 
                    that is received, including any expressed or implied warranties 
                    offered by such affiliates/advertisers. It is the individual 
                    customer's complete responsibility and obligation to independently 
                    assess the quality of any services or products that they purchase, 
                    and to make sure that these services or products meet the 
                    individual's needs. Users agree that The iRelocation Network 
                    is not liable in any way for any damages that may occur from 
                    the use of current or former member's products or services, 
                    or for any errors or omissions that may appear on the The 
                    iRelocation Network websites. </font></p>
                  <p class="heading">&nbsp;</p></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
