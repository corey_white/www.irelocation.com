<?
	session_start();
	if ($_SERVER['REMOTE_ADDR'] == "84.19.188.68")
	{
		mail("david@irelocation.com","Link Injecter IP","Session:\n".print_r($_SESSION,true)."Server:\n".print_r($_SERVER,true)."Request:\n".print_r($_REQUEST,true));
		echo "Please stop. This is not a bulletin board site or a forum.  Your links will never be seen.";
		exit();	
	}
	
	include_once("settings.php");
	include_once("quote_class.php");
	include_once("retention.php");
	include_once 'inc_mysql.php';
	
	
	define(VALID_CHARS,"^([a-zA-Z \-\.']*)*$");
	define(VOWELS,"^(.*)?[aeiouyAEIOUY](.*)?$");
	define(INITIALS,"^[A-Za-z]\.( [A-Za-z]\.)?$");
	define(SHORTEST_TO_CHECK,3);
		
	function testName($name)
	{
		if (ereg(VALID_CHARS,$name))
		{
			if (strlen($name) > SHORTEST_TO_CHECK)
			{			
				if (ereg(VOWELS,$name)>0 || ereg(INITIALS,$name) > 0)
					return 1;				
				else
					return 0;
			}
			else return 1;
		}
		else return 0;		
	}
	
	//clean all fields
	$valid = true;
	$REDIRECT = "Location: quote.php";
	
	$sql = "select value from movingdirectory.irelo_variables where name = 'atus_percentage' limit 1";
	$rs = new mysql_recordset($sql);
	if($rs->fetch_array())
		define(ATUS_PERCENTAGE,((int) $rs->myarray['value']));
	else
	{
		define(ATUS_PERCENTAGE,5);
		mail("david@irelocation.com","ATUS Percentage Missing",$_SERVER['SERVER_NAME']);
	}
	
	function fail($reason)
	{
		header("Location: quote.php?msg=$reason");
		exit();
	}
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$thispage = $_SERVER['SCRIPT_NAME'];
		$server = $_SERVER['SERVER_NAME'];

		$email = "IP: $ip\n\nUserAgent: $useragent\n\nPage: $thispage\n\nServer: $server\n\nTime: ".date("YmdHis")."\n\n";
		$email .= "Posted Data: ".strtolower(print_r($_POST,true));
		//mail("mark@irelocation.com,david@irelocation.com","***FORM LINK INJECTION - ".$type,$email);
		header("Location: quote.php?msg=injection");
		exit();
	}
	
	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ", "ass ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");					
	}		
	testInjection();	
	 //echo "54<br/>";
	$_POST["vehicle_make"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["vehicle_make"]))));
	$_POST["vehicle_model"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["vehicle_model"]))));
	$_POST["vehicle_year"] = ereg_replace("[^0-9]","",trim($_POST["vehicle_year"]));
	$_POST["from_zip"] = ereg_replace("[^0-9]","",trim($_POST["from_zip"]));
	$_POST["to_city"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["to_city"]))));
	$_POST["to_state"]=eregi_replace('[[:punct:]]','',trim($_POST["to_state"]));
	$_POST["customer_name"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["customer_name"]))));
	$_POST["customer_email"]=strtolower(eregi_replace("'",'',trim($_POST["customer_email"])));
	$_POST["customer_phone"]= ereg_replace("[^0-9]","",$_POST["customer_phone"]);
	
	$keys = array_keys($_POST);
	
	foreach($keys as $k)
	{
		$_POST[$k] = str_replace(array("<",">"),array("&lt;","&gt;"),trim($_POST[$k]));//kill all tags
		$_POST[$k] = str_replace(array("select","delete","insert","create","update","drop"),
								 array("choose","remove","add","make","change","fumble"),
								 $_POST[$k]);//kill all tags
		$_SESSION[$k] = $_POST[$k];
	}
	
	extract($_POST);

	if (strlen($vehicle_make) < 2)
	{
		fail(make);
	}
	
	if (strlen($vehicle_model) < 2)
	{
		fail(model);
	}
	
	if (strlen($vehicle_year) < 2)
	{
		fail(year);
	}
	//echo "test 102<br/>";
	if (strlen($customer_name) < 1 || substr_count(trim($customer_name)," ") <= 0)
	{
		fail(name);
	}
	list($fname,$lname) = split(" ",$customer_name);
	$lname = trim($lname);
	
	if (testName($fname) == 0 || strlen($lname) == 0)
	{
		fail(name);
	}
	
	
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $customer_email) )
	{
		fail(email);
	} 

	if (substr($customer_phone,0,1) == 1)
		$customer_phone = substr($customer_phone,1);
	if (strlen($customer_phone) != 10)
	{
		fail(phone);
	}
	
	//Phone Validation.	
	$p_area   = substr($customer_phone,0,3);
    $p_prefix = substr($customer_phone,3,3);	
	
	$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
	$rs = new mysql_recordset($sql);
	
	if (!$rs->fetch_array())
	{
		fail(invalid_phone);
	}
	
	if (strlen($from_zip) < 5 || !is_numeric($from_zip))
	{
		fail(zip);
	}
	

	$rs = new mysql_recordset("select * from movingdirectory.zip_codes where zip = '$from_zip' and length(city) > 3");
	if (!$rs->fetch_array())
	{
		$rs = new mysql_recordset("select * from movingdirectory.zip_codes where zip = '$from_zip'");
		if (!$rs->fetch_array())
		{		
			fail(from_zip_invalid);
		}
		else
		{
			$from_city = $rs->myarray["city"];
			$from_state = $rs->myarray["state"];
		}
	}
	else
	{
		$from_city = $rs->myarray["city"];
		$from_state = $rs->myarray["state"];
	}
	
	
	$move_date = $estimate_year.$estimate_month.$estimate_day;
	
	$y = date("Y");
	$m = date("m");
	$d = date("d");
	$date = $y.$m.$d;
	
	if (intval($move_date) <= intval($date))
	{
		fail(estimate_month);
	}
	//to_city
	if (strlen($to_city) < 3)
	{
		fail(city);
	}
	
	if (is_numeric($to_city) && strlen($to_city) == 5)
	{
		$sql = "select * from movingdirectory.zip_codes where zip = '$to_city'";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$_SESSION["to_city"] = $rs->myarray["city"];
			$to_city = $rs->myarray["city"];
			$_SESSION["to_state"] = $rs->myarray["state"];
			$to_state = $rs->myarray["state"];
		}
		else
		{
			$_SESSION["to_city"] == "";
			fail(invalid_zip);
		}
	}
	
	if ($to_state == "XX")
	{
		fail(state);
	}

	
	if ($mysource == '')
	{
		$referer = $_SERVER["HTTP_REFERER"];
		
		$mysource = 'mmc';
		//mail("dave@irelocation.com","Blank Source from mmc","Name:$customer_name;\nServer Data:".print_r($_SERVER,true));
	}
	
//	function checkForDupe($site)
//	{
	$move_date = $_POST["estimate_year"]."-".$_POST["estimate_month"].'-'.$_POST["estimate_day"];

	$ts = time() - (24 * 3600);//subtract a day.
	$time = date("YmdHis",$ts);
	
	$sql = "select quote_id, processed from marble.auto_quotes where received > '".$time."' ".
	"and vyear like '%$vehicle_year%' and vmake like '%$vehicle_make%' ".
	"and vmodel like '%$vehicle_model%' and (email = '$customer_email' or phone = '$customer_phone')".
	"and origin_zip = '$from_zip' and destination_city = '$to_city' ";

	$rs = new marble($sql);		
	
	if ($rs->fetch_array())
	{
		fail(duplicate);
	}		
	
	if (strlen(trim($to_city)) < 1)
	{
		fail(city);
	}

	$destination_zip = getDestinationZip($to_city,$to_state);
	
	
	$move_date = "$estimate_month-$estimate_day-$estimate_year";
	$auto_move_date = "$estimate_year-$estimate_month-$estimate_day";
	
	
	if (rand(0,100) < ATUS_PERCENTAGE)//disabled for now.
		$campaign = "atus";		
	else
		$campaign = "auto";//70% to top auto leads.

	define(TIMEZONE_OFFSET,18000);
	$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);
	$ip = $_SERVER['REMOTE_ADDR'];

	$sql2 = "insert into marble.auto_quotes set received = '$now', ".
			" fname = '$fname', lname = '$lname', email = '$customer_email', ".
			" source = '$mysource', remote_ip = '$ip', referrer = '$referer', ".
			" phone = '$customer_phone', origin_city = '$from_city', origin_state = ".
			" '$from_state', origin_zip = '$from_zip', destination_city = '$to_city', ".
			" destination_state = '$to_state', customer_comments = '$comments',".
			" destination_zip = '$destination_zip', est_move_date = '$move_date', ".
			" contact ='email', running = '$condition', vmake = '$vehicle_make', ".
			" vmodel = '$vehicle_model', vyear = '$vehicle_year', vtype ='$vehicle_type', ".
			" campaign = '$campaign'; ";
	//mail("david@irelocation.com","mmc Sql",$sql2);				
	$rs2 = new mysql_recordset($sql2);

	
	//HAVE THESE SESSION NAMES CONSISTANT ACCROSS ALL SITES..		
	$_SESSION['origin_zip'] = $from_zip;
	$_SESSION['destination_zip'] = $destination_zip;
	$_SESSION['type'] = $vehicle_type;
	$_SESSION['do_autoquote'] = "yes";
	
	

	$params = "email=$customer_email&phone=$customer_phone&origin_zip=$from_zip&destination_city=$to_city&destination_state=$to_state&move_month=$estimate_month&move_day=$estimate_day&move_year=$estimate_year&source=$mysource&no_auto=1";
  if($moving=='yes')
  {
    $msg="Your AUTO TRANSPORT quote has been received! Please fill out the following form to receive multiple household moving quotes!";
    $url="http://www.promoving.com/household_mover_quote.php?"."name=$customer_name&".$params."&msg=".$msg;
    header("location: $url");
  }
  else
  {
	//"name=$customer_name"
	$name_parts = split(" ",$customer_name,2);
	$fname = $name_parts[0];
	$lname = $name_parts[1];
	$params = "fname=$fname&lname=$lname&".$params;
    $params = str_replace("=","EQ",$params);
	$params = str_replace("&","AMPER",$params);		
    header("location: thankyou.php?params=".$params);
  }


?>