<style>

	.formcolor
	{
		color:#186799;
	}


	.smalltext
	{
		font-size:10px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		text-decoration: none;
	}

	.heading
	{
		font-size: 13px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:bold;
		text-transform: none;
		color:#000000;
		text-decoration: none;
	}

	.maintext
	{
		font-size:12px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		color:#186799;
		text-decoration: none;
	}

	.maintext a
	{
		font-size:12px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		color:#186799;
		text-decoration: none;
	}

	.errortext
	{
		font-size:14px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:bold;
		font-variant: normal;
		text-transform: none;
		color:#EE8810;
		text-decoration: none;
	}
</style>

	<?php
		
		$keys = array_keys($_REQUEST);
		foreach ($keys as $key)
		{
			$_SESSION[$key] = $_REQUEST[$key];
		}
		if ($_REQUEST["source"] == "smq")
		{
			$msg = "Thank you for filling out a Self Moving Quote! Representatives will contact your shortly. &nbsp;Here is a form ".
			"to requesting a CAR SHIPPING Estimate from 6 Top Auto Transport Companies";
		}
		else
		{
			if (strlen($_SESSION['vtype'] == 0))
				$_SESSION['vtype'] = "Car";
			$error_msg = $_REQUEST["msg"];
			$msgs["link_inject"] = "Please do not include Live Links in your comments.";
			$msgs["duplicate"] = "We see that you have allready submitted a quote today.  Please wait for these companies to contact you.";
			$msgs["name"] = "Please enter your Full Name";
			$msgs["phone"] = "Please enter your Phone Number";
			$msgs["email"] = "Please enter your Email Address";
			$msgs["est_move_date"] = "Please select your a Future Moving Date.";
			$msgs["zip"] = "Please enter your Starting Zip Code.";
			$msgs["destination_city"] = "Please enter you Destination City.";
			$msgs["destination_state"] = "Please enter you Destination State.";
			$msgs["vyear"] = "Please Enter the Year of your ".$_SESSION["vtype"];
			$msgs["vmake"] = "Please Enter the Make of your ".$_SESSION["vtype"];
			$msgs["vmodel"] = "Please Enter the Model of your ".$_SESSION["vtype"];
			$msgs["retagain"] = "Welcome back, would you like to quote another vehicle?";
			$msgs["origin_zip_scrubbed"] = "The Starting Zip code you entered was not found.";
			$msgs["phone_scrubbed"] = "Sorry, we could not validate your Area Code or Prefix.";
			$msg = $msgs[$error_msg];
		}
	?>

<table width="760" border="0" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="29" background="images/cleftbg.jpg"><img src="images/spacer.gif" width="29"></td>
    <td width="409" align="center" valign="top"><table width="409" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="100%" align="center"><img src="image/save50.jpg" alt="Auto Shipping" width="409" height="69"></td>
        </tr>
      </table><form method="post" action="<? echo $dir; ?>submit.php">
			
		<? 
			if ($_SESSION['mysource'] == "")
				$_SESSION['mysource'] = $_SESSION['source'];
			if ($_SESSION['mysource'] == "")
				$_SESSION['mysource'] = $_REQUEST['source'];
		?>
		<input type='hidden' name="source" value="<?= $_SESSION['mysource'] ?>" />


        <table width="409" border="0" align="center" cellpadding="3" cellspacing="1">
          <?
      	$errmsg='
      	<tr>
      	  <td align="center" colspan="4" ><table width="80%"><tr><td class="errortext">'.$msg.'</td></tr></table></td>
      	</tr>
		<tr><td colspan="4">&nbsp;</td><tr>';
      	if($msg!="")
      	{
      		echo $errmsg;
      	}
      	?>
          <tr> 
            <td colspan="2" height="16" class='heading'>Vehicle to be shipped:</td>
            <td height="16" colspan="2" align="right" class='heading'><font color="#EE8810" size="1">* 
              Required Items</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"type") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?> >Type</td>
            <td width="33%" height="22" class="maintext"> <select size="1" name="vtype" tabindex="0">
                <option value="Car"<? if($vtype=='Car'){echo " selected";}?>>Car</option>
                <option value="Sport Utility"<? if($vtype=='Sport Utility'){echo " selected";}?>>Sport 
                Utility</option>
                <option value="Pick-up Truck"<? if($vtype=='Pick-up Truck'){echo " selected";}?>>Pick-up 
                Truck</option>
                <option value="Oversize"<? if($vtype=='Oversize'){echo " selected";}?>>Oversize</option>
                <option value="Other Truck"<? if($vtype=='Other Truck'){echo " selected";}?>>Other 
                Truck</option>
                <option value="RV"<? if($vtype=='RV'){echo " selected";}?>>RV</option>
                <option value="Convertible"<? if($vtype=='Convertible'){echo " selected";}?>>Convertible</option>
                <option value="Van"<? if($vtype=='Van'){echo " selected";}?>>Van</option>
                <option value="Mini-Van"<? if($vtype=='Mini-Van'){echo " selected";}?>>Mini-Van</option>
                <option value="Other"<? if($vtype=='Other'){echo " selected";}?>>Other</option>
              </select> <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"year") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Year</td>
            <td width="39%" height="22"> <input type="text" name="vyear" size="7" maxlength="4" value="<?echo $vyear;?>"> 
              <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"make") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Make</td>
            <td width="33%" height="22"> <input type="text" name="vmake" size="15" value="<? echo $_SESSION["vmake"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"condition") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Condition</td>
            <td width="39%" height="22"> <select size="1" name="running">
                <option value="R"<? if($running=='R'){echo " selected";}?>>Running</option>
                <option value="N"<? if($running=='N'){echo " selected";}?>>Non-Running</option>
              </select></td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"model") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Model</td>
            <td width="33%" height="22"> <input type="text" name="vmodel" size="15" value="<? echo $_SESSION["vmodel"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
      <td width="13%" height="22" align="right"> <font color="#186799" size="2" face="Arial">Contact</font></td>
      <td width="34%" height="22">
			<select name="contact">
				<option value="email">Email</option>
				<option value="phone">Phone</option>
			</select>
	</td>
          </tr>
          <tr> 
            <td width="13%" height="22">&nbsp;</td>
            <td width="33%" height="22">&nbsp;</td>
            <td width="15%" height="22">&nbsp;</td>
            <td width="39%" height="22">&nbsp;</td>
          </tr>
<tr> 
            <td colspan="2" height="16" class='heading'>Contact information  <font color="#EE8810" size="1">*</font></td>
            
          </tr>          
<tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"fname") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>First Name</td>
            <td width="33%" height="22"> <input type="text" name="fname" size="15" value="<? echo $_SESSION["fname"]; ?>">
            
            <td height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"lname") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> 
              Last Name</td>
            <td height="22"> <input type="text" name="lname" size="15" value="<? echo $_SESSION["lname"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
          </tr>
          
          <tr> 

            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"email") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> 
              Email </td>
            <td width="33%" height="22"> <input type="text" name="email" size="15" value="<? echo $_SESSION["email"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"phone") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> Phone</td>
            <td height="22">
              <input type="text" name="phone" size="15" value="<? echo $_SESSION["phone"]; ?>">
              <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td width="33%" height="19">&nbsp;</td>
            <td width="15%" height="19">&nbsp;</td>
            <td width="39%" height="19">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" height="16" class='heading'>Moving from:</td>
            <td colspan="2" height="16" class='heading'>Moving to:</td>
          </tr>
          <tr> 
            <td width="13%" height="20" align="right" valign="middle" <?php if(substr_count($error_msg,"zip") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Zip 
            </td>
            <td width="33%" height="20" class="maintext"> <input type="text" name="origin_zip" size="17" value="<?= $origin_zip ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="20" align="right"<?php if(substr_count($error_msg,"city") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>City 
            </td>
            <td width="39%" height="20" class="maintext"><input type="text" name="destination_city" size="15" value="<?= $_SESSION["destination_city"] ?>"> 
              <font color="#EE8810" size="1"> *</font> </td>
          </tr>
          <tr> 
            <td width="13%" align="right" height="22" <?php if(substr_count($error_msg,"est_move_date") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Moving 
              Date:</td>
            <td width="33%" height="22"> 
              <?php
		  	if (strlen($_SESSION["move_month"]) < 1)
			{
				$e_month = date("n");
				$e_day = date("j");
				$e_year = date("Y");
			}
			else
			{
				$e_month = intval($_SESSION["move_month"]);
				$e_day = intval($_SESSION["move_day"]);
				$e_year = intval($_SESSION["move_year"]);
		  	}

		  ?>
              <select name="move_month" tabindex="12">
                <option value="01"<? if($e_month=='1'){echo " selected";}?>>Jan</option>
                <option value="02"<? if($e_month=='2'){echo " selected";}?>>Feb</option>
                <option value="03"<? if($e_month=='3'){echo " selected";}?>>Mar</option>
                <option value="04"<? if($e_month=='4'){echo " selected";}?>>Apr</option>
                <option value="05"<? if($e_month=='5'){echo " selected";}?>>May</option>
                <option value="06"<? if($e_month=='6'){echo " selected";}?>>Jun</option>
                <option value="07"<? if($e_month=='7'){echo " selected";}?>>Jul</option>
                <option value="08"<? if($e_month=='8'){echo " selected";}?>>Aug</option>
                <option value="09"<? if($e_month=='9'){echo " selected";}?>>Sep</option>
                <option value="10"<? if($e_month=='10'){echo " selected";}?>>Oct</option>
                <option value="11"<? if($e_month=='11'){echo " selected";}?>>Nov</option>
                <option value="12"<? if($e_month=='12'){echo " selected";}?>>Dec</option>
              </select> <select name="move_day" tabindex="13">
                <?
              for($i=1;$i<=31;$i++)
              {
                  if($i==$e_day)
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\" selected>0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\" selected>$i</option>";
                    }
                  }
                  else
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\">0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\">$i</option>";
                    }
                  }
              }
              ?>
              </select> <select name="move_year" tabindex="14">
                <?
              for($i=date("Y");$i<=date("Y")+3;$i++)
              {
                   if ($i == $e_year)
					    echo "<option value=\"$i\" selected >$i</option>";
				   else
					   echo "<option value=\"$i\">$i</option>";
              }
          ?>
              </select> <font color="#EE8810" size="1">*</font></td>
            <td width="15%" height="22" align="right" <?php if(substr_count($error_msg,"state") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>State</td>
            <td width="39%" height="22"><select name="destination_state" tabindex="8">
                <option value="XX">Please Select</option>
                <option value="AL"<? if($destination_state=='AL'){echo " selected";}?>>Alabama</option>
                <option value="AK"<? if($destination_state=='AK'){echo " selected";}?>>Alaska</option>
                <option value="AZ"<? if($destination_state=='AZ'){echo " selected";}?>>Arizona</option>
                <option value="AR"<? if($destination_state=='AR'){echo " selected";}?>>Arkansas</option>
                <option value="CA"<? if($destination_state=='CA'){echo " selected";}?>>California</option>
                <option value="CO"<? if($destination_state=='CO'){echo " selected";}?>>Colorado</option>
                <option value="CT"<? if($destination_state=='CT'){echo " selected";}?>>Connecticut</option>
                <option value="DE"<? if($destination_state=='DE'){echo " selected";}?>>Delaware</option>
                <option value="FL"<? if($destination_state=='FL'){echo " selected";}?>>Florida</option>
                <option value="GA"<? if($destination_state=='GA'){echo " selected";}?>>Georgia</option>
                <option value="HI"<? if($destination_state=='HI'){echo " selected";}?>>Hawaii</option>
                <option value="ID"<? if($destination_state=='ID'){echo " selected";}?>>Idaho</option>
                <option value="IL"<? if($destination_state=='IL'){echo " selected";}?>>Illinois</option>
                <option value="IN"<? if($destination_state=='IN'){echo " selected";}?>>Indiana</option>
                <option value="IA"<? if($destination_state=='IA'){echo " selected";}?>>Iowa</option>
                <option value="KS"<? if($destination_state=='KS'){echo " selected";}?>>Kansas</option>
                <option value="KY"<? if($destination_state=='KY'){echo " selected";}?>>Kentucky</option>
                <option value="LA"<? if($destination_state=='LA'){echo " selected";}?>>Louisiana</option>
                <option value="ME"<? if($destination_state=='ME'){echo " selected";}?>>Maine</option>
                <option value="MD"<? if($destination_state=='MD'){echo " selected";}?>>Maryland</option>
                <option value="MA"<? if($destination_state=='MA'){echo " selected";}?>>Massachusetts</option>
                <option value="MI"<? if($destination_state=='MI'){echo " selected";}?>>Michigan</option>
                <option value="MN"<? if($destination_state=='MN'){echo " selected";}?>>Minnesota</option>
                <option value="MS"<? if($destination_state=='MS'){echo " selected";}?>>Mississippi</option>
                <option value="MO"<? if($destination_state=='MO'){echo " selected";}?>>Missouri</option>
                <option value="MT"<? if($destination_state=='MT'){echo " selected";}?>>Montana</option>
                <option value="NE"<? if($destination_state=='NE'){echo " selected";}?>>Nebraska</option>
                <option value="NV"<? if($destination_state=='NV'){echo " selected";}?>>Nevada</option>
                <option value="NH"<? if($destination_state=='NH'){echo " selected";}?>>New 
                Hampshire</option>
                <option value="NJ"<? if($destination_state=='NJ'){echo " selected";}?>>New 
                Jersey</option>
                <option value="NM"<? if($destination_state=='NM'){echo " selected";}?>>New 
                Mexico</option>
                <option value="NY"<? if($destination_state=='NY'){echo " selected";}?>>New 
                York</option>
                <option value="NC"<? if($destination_state=='NC'){echo " selected";}?>>North 
                Carolina</option>
                <option value="ND"<? if($destination_state=='ND'){echo " selected";}?>>North 
                Dakota</option>
                <option value="OH"<? if($destination_state=='OH'){echo " selected";}?>>Ohio</option>
                <option value="OK"<? if($destination_state=='OK'){echo " selected";}?>>Oklahoma</option>
                <option value="OR"<? if($destination_state=='OR'){echo " selected";}?>>Oregon</option>
                <option value="PA"<? if($destination_state=='PA'){echo " selected";}?>>Pennsylvania</option>
                <option value="PR"<? if($destination_state=='PR'){echo " selected";}?>>Puerto 
                Rico</option>
                <option value="RI"<? if($destination_state=='RI'){echo " selected";}?>>Rhode 
                Island</option>
                <option value="SC"<? if($destination_state=='SC'){echo " selected";}?>>South 
                Carolina</option>
                <option value="SD"<? if($destination_state=='SD'){echo " selected";}?>>South 
                Dakota</option>
                <option value="TN"<? if($destination_state=='TN'){echo " selected";}?>>Tennessee</option>
                <option value="TX"<? if($destination_state=='TX'){echo " selected";}?>>Texas</option>
                <option value="UT"<? if($destination_state=='UT'){echo " selected";}?>>Utah</option>
                <option value="VT"<? if($destination_state=='VT'){echo " selected";}?>>Vermont</option>
                <option value="VA"<? if($destination_state=='VA'){echo " selected";}?>>Virginia</option>
                <option value="WA"<? if($destination_state=='WA'){echo " selected";}?>>Washington</option>
                <option value="DC"<? if($destination_state=='DC'){echo " selected";}?>>Washington 
                DC</option>
                <option value="WV"<? if($destination_state=='WV'){echo " selected";}?>>West 
                Virginia</option>
                <option value="WI"<? if($destination_state=='WI'){echo " selected";}?>>Wisconsin</option>
                <option value="WY"<? if($destination_state=='WY'){echo " selected";}?>>Wyoming</option>
              </select> <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td width="33%" height="19">&nbsp;</td>
            <td width="15%" height="19">&nbsp;</td>
            <td width="39%" height="19">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" height="16" class="heading" >Additional information:</td>
            <td colspan="2" height="16" class="heading" >&nbsp;</td>
          </tr>
          <tr> 
            <td width="13%" height="107" align="right" valign="top" class="maintext"> 
              <p>&nbsp;</p>
              <p>&nbsp; </p></td>
            <td height="107" colspan="3" class="maintext"> Comments<br> <textarea rows="5" name="comments" cols="35" tabindex="15"><?php echo $_SESSION["comments"]; ?></textarea> 
            </td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td height="19" colspan="3" class="maintext">Please describe any major 
              vehicle problems.<br> <br> </td>
          </tr>
          <? if ($_SESSION["nohouse"] != 1) { ?>
          <tr> 
            <td height="19" colspan="4" align="center" class="maintext"> 
              <?php $moving = $_SESSION["moving"]; ?>
              Are you moving any <b>Household Goods</b>? <br>
              Would you also like to get multiple <b>Household Moving</b> quotes 
              <select size="1" name="moving">
                <option value="no"<? if($moving=='no'){echo " selected";}?>>No</option>
                <option value="yes"<? if($moving=='yes'){echo " selected";}?>>Yes</option>
              </select> <br> &nbsp;</td>
          </tr>
          <? }  ?>
          <tr> 
            <td height="19" colspan="4" align="center"> <input type="submit" value="Submit Quote Request" name="Submit" tabindex="16"></td>
          </tr>
        </table>
       
    </form> </td>
    <td width="254" valign="top" background="images/crightbg2.jpg"><img src="images/crightbg.jpg" width="322" height="648"> 
      <table width="300" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center"><img src="image/seal.jpg" alt="Auto Shipping and Car shipping Guide" width="142" height="156" hspace="5" vspace="10"></td>
        </tr>
      </table></td>
  </tr>
</table>
