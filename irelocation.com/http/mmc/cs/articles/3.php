<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p class="heading">Car Shipping Insurance</p>
      <p class="maintext">Your vehicle(s) should be insured against damage and 
        theft by your car transport company. ASK FOR A COPY OF INSURANCE COVERAGE. 
        Ask if the carrier's insurance is PRIMARY or SECONDARY to your own insurance 
        in the event of damage during car transport. Also ask if there is a damage 
        deductible &#8211; VERIFY ALL IN WRITING. <br>
        <br>
        In addition, your own car insurance may cover your vehicle in transit 
        &#8211; check with your car insurance company to verify. Keep in mind 
        that many car transport companies may discourage or prohibit you from 
        leaving personal articles in your vehicle. Any personal articles left 
        in the car are not insured &#8211; your home policy may cover these articles, 
        but you should check with your home insurance agent to verify. <br>
        <br>
        ALSO NOTE THAT OVERLOADING YOUR TRUNK WITH HEAVY ITEMS MAY RESULT IN DAMAGE 
        TO YOUR CAR'S UNDERCARRIAGE. Your car transport company may not cover 
        damage in that instance. </p>
      </td>
  </tr>
</table>
