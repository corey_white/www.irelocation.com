<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p class="heading">Selecting a Car Shipping Company</p>
      
      <ol>
        <li><span class="maintext"> PRICE The 
          first question you'll want answered is &quot;what is the total cost?&quot;. 
          Obtain quotes from several car transport companies and be prepared to 
          provide: </span>
          <ul>
            <li><span class="maintext">origination/destination 
              </span></li>
            <li><span class="maintext">approximate 
              departure date </span></li>
            <li><span class="maintext">type of 
              vehicle(s) </span></li>
            <li><span class="maintext">type of 
              car transport service desired (e.g., door-to-door car transport 
              vs. terminal-to-terminal) </span></li>
            <li><span class="maintext">special 
              car transport services (e.g., enclosed car transport) </span></li>
          </ul>
          <span class="maintext"><br>
          &nbsp;</span></li>
        <li><span class="maintext"> PAYMENT METHOD 
          While some car transport companies may not require payment in advance, 
          others require a deposit (10-25% of the total cost), or full payment 
          in advance. If a deposit is given, amount due is always required at 
          time of delivery � oftentimes with a cashiers check or cash. Sometimes 
          a surcharge of 2-3% is required when using a credit card with a car 
          transport company. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> CONTRACT READ 
          YOUR CONTRACT CAREFULLY!!! IMPORTANT NOTE!!! Do not rely on any verbal 
          promises from your car transport company. After you sign your contract, 
          your car transport company will ALWAYS refer you to the contract for 
          any complaints you may have, including cost to cancel your shipment. 
          The main thing to look for is a clause that says there is no guaranteed 
          pickup or delivery date. Even though most cars are delivered by car 
          transport companies within the expected timeframe, most will not guarantee 
          a delivery date. You can ask for the following things to protect yourself 
          against verbal promises that you would have otherwise relied upon (also 
          see #5 below):  </span>
          <ul>
            <li><span class="maintext">Ask for 
              a contract clause (in writing) that waives the cancellation fee 
              if the car is not picked up by your car transport company within 
              some number of days of the scheduled pickup date. If you cancel 
              within the allotted window, expect to pay some fee, which may vary 
              from $50 to $250. Although most cars are picked up on the scheduled 
              date or within a few days of that date, a 1 or 2 week delay may 
              occasionally happen. Your cancellation clause should allow for at 
              least a week beyond scheduled pickup date. </span></li>
            <li><span class="maintext">It is difficult 
              for any car transport company to promise a delivery date, however, 
              if your car transport company offers a guarantee that covers a rental 
              car, find out the cost of the guarantee (if any) and the maximum 
              dollars per day covered. </span></li>
          </ul>
          <span class="maintext"><br>
          &nbsp;</span></li>
        <li><span class="maintext"> INSURANCE Your 
          vehicle(s) should be insured against damage and theft by your car transport 
          company. ASK FOR A COPY OF INSURANCE COVERAGE. Ask if the carrier's 
          insurance is PRIMARY or SECONDARY to your own insurance in the event 
          of damage during car transport. Also ask if there is a damage deductible 
          � VERIFY ALL IN WRITING. In addition, your own car insurance may cover 
          your vehicle in transit � check with your car insurance company to verify. 
          Keep in mind that many car transport companies may discourage or prohibit 
          you from leaving personal articles in your vehicle. Any personal articles 
          left in the car are not insured � your home policy may cover these articles, 
          but you should check with your home insurance agent to verify. ALSO 
          NOTE THAT OVERLOADING YOUR TRUNK WITH HEAVY ITEMS MAY RESULT IN DAMAGE 
          TO YOUR CAR'S UNDERCARRIAGE. Your car transport company may not cover 
          damage in that instance. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> PICK-UP AND DELIVERY DATES 
          Ask for estimated pick-up and delivery times from your car transport 
          company. You should be able to get a 3-day window for pick-up � during 
          the busy summer or snow bird season the window may be larger, but the 
          car transport company should warn you of possible delays ahead of time. 
          BE SKEPTICAL OF EXACT PICK-UP AND DELIVERY DATES PROMISED BY CAR TRANSPORT 
          COMPANIES � IF YOU ARE PROMISED EXACT DATES, GET IT IN WRITING AND STATE 
          THE REMEDY IF SERVICE IS NOT PERFORMED ON PROMISED DATES ($50 discount, 
          no fee to cancel, reimbursement for rental car, etc.) . Once pick-up 
          is made, your car transport company should provide you with a fairly 
          precise delivery date. If you need an exact date for pick-up, most car 
          transport services can pick-up your car and hold it at a terminal for 
          an additional charge. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> SAYING GOODBYE TO YOUR CAR 
          Prior to leaving your car with a car transport company, be sure you 
          receive an &quot;Original Inspection Report.&quot; This report provides 
          pick-up and delivery information, current mileage, and MOST IMPORTANTLY 
          shows the condition of your car at time of pick-up � pre-existing scratches 
          and dents, cracked glass/mirrors, general paint condition, etc. Keep 
          this report and use it when you receive your vehicle. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> ACCEPTING YOUR VEHICLE FOR DELIVERY 
          At the time of delivery, inspect your vehicle SLOWLY AND CAREFULLY and 
          compare the condition and mileage against the &quot;Original Inspection 
          Report&quot; provided by your car transport company at origination (ALSO 
          INSPECT THE ROOF AND UNDER FRONT AND REAR BUMPERS). If there are inconsistencies, 
          note them as exceptions and be sure the driver signs it. NEVER ACCEPT 
          YOUR VEHICLE AT NIGHT IF YOU CANNOT VERIFY ITS CONDITION � without being 
          viewed and signed by the driver, you have little recourse if damage 
          had occurred during transit. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> IF DAMAGE SHOULD OCCUR 
          The majority of car transport experiences are worry-free, with your 
          damage-free vehicle arriving at the estimated time. On occasion, a vehicle 
          may become damaged in transit. If this should happen, note all damage 
          on your inspection report (see item above), obtain the driver's signature, 
          and contact your car transport company. The car transport company should 
          then work with you to reimburse you for verifiable damage done to your 
          car. <br>
          <br>
          &nbsp;</span></li>
        <li><span class="maintext"> DISPUTES If 
          you believe that a car transport company has not treated you fairly, 
          first try to work it out with the company. Car transport companies are 
          more likely to cooperate with you if they believe you are trying to 
          work fairly with them. Be reasonable, but firm. If all else fails, you 
          may consider filing a complaint with the Better Business Bureau in the 
          city in which the car transport company is listed. The U.S. Dept. of 
          Transportation � http://www.dot.gov � regulates car transport companies 
          � however they will usually not get involved until after you have a 
          court judgment against a car transport company (in small claims court, 
          for example). </span></li>
      </ol>
      
      </td>
  </tr>
</table>
