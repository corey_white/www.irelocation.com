<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><p class="heading">Car Shipping Contracts</p>
      <p class="maintext">READ YOUR CONTRACT CAREFULLY!!! Do not rely on any verbal 
        promises from your car transport company. After you sign your contract, 
        your car transport company will ALWAYS refer you to the contract for any 
        complaints you may have, including cost to cancel your shipment. The main 
        thing to look for is a clause that says there is no guaranteed pickup 
        or delivery date. Even though most cars are delivered by car transport 
        companies within the expected timeframe, most will not guarantee a delivery 
        date. You can ask for the following things to protect yourself against 
        verbal promises that you would have otherwise relied upon. <br>
        <br>
        Ask for a contract clause (in writing) that waives the cancellation fee 
        if the car is not picked up by your car transport company within some 
        number of days of the scheduled pickup date. If you cancel within the 
        alloted window, expect to pay some fee, which may vary from $50 to $250. 
        Although most cars are picked up on the scheduled date or within a few 
        days of that date, a 1 or 2 week delay may occassionally happen. Your 
        cancellation clause should allow for at least a week beyond scheduled 
        pickup date. <br>
        <br>
        It is difficult for any car transport company to promise a delivery date, 
        however, if your car transport company offers a guarantee that covers 
        a rental car, find out the cost of the guarantee (if any) and the maximum 
        dollars per day covered. <br>
      </p>
      </td>
  </tr>
</table>
