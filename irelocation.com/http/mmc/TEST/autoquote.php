<?
/*

<?xml version="1.0" encoding="utf-8"?>
<!-- This information and system is restricted. Copyright(c)2006 A AAAdvantage Auto Transport. -->
<quote>
  <rate>1075</rate>
</quote>


Error Case:
<?xml version="1.0" encoding="utf-8"?>
<!-- This information and system is restricted. Copyright(c)2007 A AAAdvantage Auto Transport. -->
ERROR!

*/

include "../inc_mysql.php";
define(DISCLAIMER,"This price is only an estimate. &nbsp;You will be contact shortly with actual quotes from great Auto Transporters.");
define(LARGE_VEHICLE_MOD,150);
/*
if (isset($_REQUEST['debug']))
{
	define(DEBUG,false);
	if ($_REQUEST['debug'] == 'high')
		define(DEBUG_HIGH,true);
	else 
		define(DEBUG_HIGH,false);
}
else
	define(DEBUG,false);
*/
define(DEBUG,false);
define(DEBUG_HIGH,false);
define(URL,"http://www.automoves.com/xml_get_quote.php?auth=0800200c9a66");
define(VARIABLE,.2);

function getQuoteText($price, $type)
{
	if ($price == 0)//not found.
	{
		return "Sorry, we couldn't find a price for that origin and destination. Our Auto Transporters will contact you with their quotes shortly.";
	}
	else if ($price == LARGE_VEHICLE_MOD+1)//no price found.. shouldnt happen
	{
		mail("david@irelocation.com","Auto Quote Error!");
		return "Sorry, we couldn't find a price for that origin and destination. Our Auto Transporters will contact you with their quotes shortly.";		
	}	
	else
	{
		$low = (1-VARIABLE) * $price;
		$high = (1+VARIABLE) * $price;
		return "It will cost \$".number_format($low,2,".",",")." - \$".number_format($high,2,".",",")." to ship your ".ucwords($type).". &nbsp;".DISCLAIMER;
	}
}

function wrapper($ozip,$dzip,$type)
{
	$type = strtolower($type);	
	$make = "honda";
	$model  = "accord";
	if ($type != "car")//if not a car, add $150.
		$modifier = LARGE_VEHICLE_MOD;
		
	$cities = getBackupCities($ozip,$dzip);
	$states = getStates($ozip, $dzip);		
	
	$fs = $states[$ozip];
	$ts = $states[$dzip];

	$done = false;
	$ccount = count($cities[$ozip]);
	if (DEBUG_HIGH) "Origin City Count: $ccount<br/>";
	$dcount = count($cities[$dzip]);
	if (DEBUG_HIGH) "Destination City Count: $dcount<br/>";
	for ($ci = 0; $ci < $ccount && !$done; $ci++)//loop through each origin city till we get a match
		for ($di = 0; $di < $dcount && !$done; $di++)//loop through each destination city till we get a match
		{
			$array = array(urlencode($cities[$ozip][$ci]),$fs,urlencode($cities[$dzip][$di]),$ts,$make,$model);
			if (DEBUG_HIGH) echo print_r($array,true)."<br/>";
			$price = getXMLAutoquote($array);
			if (intval($price) > 0)		
				return ($modifier+$price);				
		}		
	return 0;
}

function getBackupCities($ozip,$dzip)
{
	$sql = "select zip,city from movingdirectory.zip_codes where zip in ('$dzip','$ozip') and length(city) > 3 ";
	if (DEBUG_HIGH) echo $sql."<br/>";
	$rs = new mysql_recordset($sql);	
	while($rs->fetch_array())
	{
		$data[$rs->myarray['zip']][] = $rs->myarray['city'];
	}	
	return $data;
}

function getStates($ozip, $dzip)
{
	$sql = "select state, zip from movingdirectory.zip_codes where zip in ('$dzip','$ozip') group by state;";
	if (DEBUG_HIGH) echo  $sql."<br/>";
	$rs = new mysql_recordset($sql);	
	if ($rs->rowcount() == 1)//same state.
	{
		$rs->fetch_array();
		$data[$ozip] = $rs->myarray['state'];
		$data[$dzip] = $rs->myarray['state'];
	}
	else//two states.. most likely.
	{
		while($rs->fetch_array())
			$data[$rs->myarray['zip']] = $rs->myarray['state'];
	}

	return $data;
}

function getXMLAutoquote($array)
{		
	if (DEBUG_HIGH) print_r($array);
	$fc = $array[0];
	$fs = $array[1];
	$tc = $array[2];
	$ts = $array[3];
	$make = $array[4];
	$model = $array[5];
	$url = buildURL($fc,$fs,$tc,$ts,$make,$model);

	if (DEBUG_HIGH) echo  "URL: $url<br/>";
	$ch = curl_init($url);	
	$_SESSION['url'] = $url;
	if (DEBUG_HIGH) echo  $url;
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
	$response = curl_exec($ch);
	if (DEBUG_HIGH) echo  "<textarea>$response</textarea><Br/>";
	if (DEBUG_HIGH) echo  "Error: ".(substr_count($response,"ERROR") > 0)?"yes":"no";
	if (DEBUG_HIGH) echo  "<br/>";
	curl_close($ch);
	
	if (DEBUG_HIGH) echo  "<textarea>".$response."</texarea><Br/>";
	$splitat = strpos($response,"<rate>");
	$endat = strpos($response,"</rate>") - $splitat;

	$price = ereg_replace("[^0-9\.]","",substr($response,$splitat,$endat));
	//mail("dave@irelocation.com","MMC AutoQuote",$url." ".$price." ".$response);
	return $price;
}

function buildURL($fc,$fs,$tc,$ts,$make,$model)
{
	return URL."&fc=$fc&fs=$fs&tc=$tc&ts=$ts&make=$make&model=$model";
}

if (strlen($_REQUEST['ozip']) == 5 && strlen($_REQUEST['dzip']) == 5 && 
	strlen($_REQUEST['type']) > 1)
{
	extract($_REQUEST);
	if (DEBUG_HIGH) print_r($_REQUEST);
	echo getQuoteText(wrapper($ozip,$dzip,$type),$type);
}
?>