<style>

	.formcolor
	{
		color:#186799;
	}


	.smalltext
	{
		font-size:10px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		text-decoration: none;
	}

	.heading
	{
		font-size: 13px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:bold;
		text-transform: none;
		color:#000000;
		text-decoration: none;
	}

	.maintext
	{
		font-size:12px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		color:#186799;
		text-decoration: none;
	}

	.maintext a
	{
		font-size:12px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:normal;
		text-transform: none;
		color:#186799;
		text-decoration: none;
	}

	.errortext
	{
		font-size:14px;
		font-family: Arial;
		font-style:normal;
		line-height: normal;
		font-weight:bold;
		font-variant: normal;
		text-transform: none;
		color:#FF0000;
		text-decoration: none;
	}
</style>

	<?php
		
		if (strlen($_REQUEST['id']) > 6)
		{
			$id = substr($_REQUEST['id'],0,5);
			$sql = "select email from movingdirectory.retention where id = '$id' limit 1";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
				$customer_email = $rs->myarray['email'];
		}
		if ($_SESSION['source'] != "") $mysource = $_SESSION['source'];
		$keys = array_keys($_REQUEST);
		foreach ($keys as $key)
		{
			$_SESSION[$key] = $_REQUEST[$key];
		}
		if ($_REQUEST["source"] == "smq")
		{
			$msg = "Thank you for filling out a Self Moving Quote! Representatives will contact your shortly. &nbsp;Here is a form ".
			"to requesting a CAR SHIPPING Estimate from 6 Top Auto Transport Companies";
		}
		else
		{
			if (strlen($_SESSION['vehicle_type']) == 0)
				$_SESSION['vehicle_type'] = "Car";
			$error_msg = $_REQUEST["msg"];
			$msgs["link_inject"] = "Please do not include Live Links in your comments.";
			$msgs["duplicate"] = "We see that you have allready submitted a quote today.  Please wait for these companies to contact you.";
			//$msgs["fname"] = "Please enter your First Name";
			$msgs["name"] = "Please enter your Full Name";
			$msgs["phone"] = "Please enter your Phone Number";
			$msgs["email"] = "Please enter your Email Address";
			$msgs["estimate_month"] = "Please select your a Future Moving Date.";
			$msgs["zip"] = "Please enter your Starting Zip Code.";
			$msgs["city"] = "Please enter you Destination City.";
			$msgs["state"] = "Please enter you Destination State.";
			$msgs["year"] = "Please Enter the Year of your ".$_SESSION["vehicle_type"];
			$msgs["make"] = "Please Enter the Make of your ".$_SESSION["vehicle_type"];
			$msgs["model"] = "Please Enter the Model of your ".$_SESSION["vehicle_type"];
			$msgs["retagain"] = "Welcome back, would you like to quote another vehicle?";
			$msgs["from_zip_invalid"] = "The Starting Zip code you entered was not found.";
			$msgs["invalid_phone"] = "Sorry, we could not validate your Area Code or Prefix.";
			$msg = $msgs[$error_msg];
		}
	?>

<table width="760" border="0" bgcolor="#ffffff" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="29" background="images/cleftbg.jpg"><img src="images/spacer.gif" width="29"></td>
    <td width="409" align="center" valign="top"><table width="409" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="100%" align="center"><img src="image/save50.jpg" alt="Auto Shipping" width="409" height="69"></td>
        </tr>
      </table><form method="post" action="<? echo $dir; ?>submit_quote.php">
			<input type='hidden' name="mysource" value="<?= $_SESSION['source'] ?>" />



        <table width="409" border="0" align="center" cellpadding="3" cellspacing="1">
          <?
      	$errmsg='
      	<tr>
      	  <td align="center" colspan="4" ><table width="80%"><tr><td class="errortext">'.$msg.'</td></tr></table></td>
      	</tr>
		<tr><td colspan="4">&nbsp;</td><tr>';
      	if($msg!="")
      	{
      		echo $errmsg;
      	}
      	?>
          <tr> 
            <td colspan="2" height="16" class='heading'>Vehicle to be shipped:</td>
            <td height="16" colspan="2" align="right" class='heading'><font color="#EE8810" size="1">* 
              Required Items</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"type") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?> >Type</td>
            <td width="33%" height="22" class="maintext"> <select size="1" name="vehicle_type" >
                <option value="Car"<? if($vehicle_type=='Car'){echo " selected";}?>>Car</option>
                <option value="Sport Utility"<? if($vehicle_type=='Sport Utility'){echo " selected";}?>>Sport 
                Utility</option>
                <option value="Pick-up Truck"<? if($vehicle_type=='Pick-up Truck'){echo " selected";}?>>Pick-up 
                Truck</option>
                <option value="Oversize"<? if($vehicle_type=='Oversize'){echo " selected";}?>>Oversize</option>
                <option value="Other Truck"<? if($vehicle_type=='Other Truck'){echo " selected";}?>>Other 
                Truck</option>
                <option value="RV"<? if($vehicle_type=='RV'){echo " selected";}?>>RV</option>
                <option value="Convertible"<? if($vehicle_type=='Convertible'){echo " selected";}?>>Convertible</option>
                <option value="Van"<? if($vehicle_type=='Van'){echo " selected";}?>>Van</option>
                <option value="Mini-Van"<? if($vehicle_type=='Mini-Van'){echo " selected";}?>>Mini-Van</option>
                <option value="Other"<? if($vehicle_type=='Other'){echo " selected";}?>>Other</option>
              </select> <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"year") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Year</td>
            <td width="39%" height="22"> 
				<select name="vehicle_year">
				<? 	
					for($i = date("Y")+1; $i > date("Y")-60; $i--)
					{
						 echo "<option value='$i' ";
						if ($i == $vehicle_year) echo "selected ";
						echo ">$i</option>";
					}
				?>		
				</select> 
              <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"make") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Make</td>
            <td width="33%" height="22"> <input type="text" name="vehicle_make" size="15" value="<? echo $_SESSION["vehicle_make"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"condition") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Condition</td>
            <td width="39%" height="22"> <select size="1" name="condition">
                <option value="r"<? if($condition=='r'){echo " selected";}?>>Running</option>
                <option value="n"<? if($condition=='n'){echo " selected";}?>>Non-Running</option>
              </select></td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"model") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Model</td>
            <td width="33%" height="22"> <input type="text" name="vehicle_model" size="15" value="<? echo $_SESSION["vehicle_model"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"carrier") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> 
              Carrier Type: </td>
            <td width="39%" height="22"> <select size="1" name="quote_type">
                <option value="open"<? if($quote_type=='open'){echo " selected";}?>>Open</option>
                <option value="enclosed"<? if($quote_type=='enclosed'){echo " selected";}?>>Enclosed</option>
                <option value="expedited"<? if($quote_type=='expedited'){echo " selected";}?>>Expedited</option>
              </select></td>
          </tr>
          <tr> 
            <td width="13%" height="22">&nbsp;</td>
            <td width="33%" height="22">&nbsp;</td>
            <td width="15%" height="22">&nbsp;</td>
            <td width="39%" height="22">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" height="16" class='heading'>Contact information:</td>
            <td width="15%" height="22">&nbsp;</td>
            <td width="39%" height="22">&nbsp;</td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"name") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Name</td>
            <td width="33%" height="22"> <input type="text" name="customer_name" size="15" value="<? echo $_SESSION["customer_name"]; ?>">
              <font color="#EE8810" size="1">*</font></td>
            <td height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"phone") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> 
              Phone</td>
            <td height="22"> <input type="text" name="customer_phone" size="15" value="<? echo $_SESSION["customer_phone"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="22" align="right" valign="middle" <?php if(substr_count($error_msg,"email") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>> 
              Email </td>
            <td width="33%" height="22"> <input type="text" name="customer_email" size="15" value="<? echo $_SESSION["customer_email"]; ?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="19">&nbsp;</td>
            <td width="39%" height="19">&nbsp;</td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td width="33%" height="19">&nbsp;</td>
            <td width="15%" height="19">&nbsp;</td>
            <td width="39%" height="19">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" height="16" class='heading'>Moving from:</td>
            <td colspan="2" height="16" class='heading'>Moving to:</td>
          </tr>
          <tr> 
            <td width="13%" height="20" align="right" valign="middle" <?php if(substr_count($error_msg,"zip") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Zip 
            </td>
            <td width="33%" height="20" class="maintext"> <input type="text" name="from_zip" size="17" value="<?echo $from_zip;?>"> 
              <font color="#EE8810" size="1">*</font> </td>
            <td width="15%" height="20" align="right"<?php if(substr_count($error_msg,"city") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>City 
            </td>
            <td width="39%" height="20" class="maintext"><input type="text" name="to_city" size="15" value="<? echo $_SESSION["to_city"];?>"> 
              <font color="#EE8810" size="1"> *</font> </td>
          </tr>
          <tr> 
            <td width="13%" align="right" height="22" <?php if(substr_count($error_msg,"est") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>Moving 
              Date:</td>
            <td width="33%" height="22"> 
              <?php
		  	if (strlen($_SESSION["estimate_month"]) < 1)
			{
				$e_month = date("n");
				$e_day = date("j");
				$e_year = date("Y");
			}
			else
			{
				$e_month = intval($_SESSION["estimate_month"]);
				$e_day = intval($_SESSION["estimate_day"]);
				$e_year = intval($_SESSION["estimate_year"]);
		  	}

		  ?>
              <select name="estimate_month" >
                <option value="01"<? if($e_month=='1'){echo " selected";}?>>Jan</option>
                <option value="02"<? if($e_month=='2'){echo " selected";}?>>Feb</option>
                <option value="03"<? if($e_month=='3'){echo " selected";}?>>Mar</option>
                <option value="04"<? if($e_month=='4'){echo " selected";}?>>Apr</option>
                <option value="05"<? if($e_month=='5'){echo " selected";}?>>May</option>
                <option value="06"<? if($e_month=='6'){echo " selected";}?>>Jun</option>
                <option value="07"<? if($e_month=='7'){echo " selected";}?>>Jul</option>
                <option value="08"<? if($e_month=='8'){echo " selected";}?>>Aug</option>
                <option value="09"<? if($e_month=='9'){echo " selected";}?>>Sep</option>
                <option value="10"<? if($e_month=='10'){echo " selected";}?>>Oct</option>
                <option value="11"<? if($e_month=='11'){echo " selected";}?>>Nov</option>
                <option value="12"<? if($e_month=='12'){echo " selected";}?>>Dec</option>
              </select> <select name="estimate_day" >
                <?
              for($i=1;$i<=31;$i++)
              {
                  if($i==$e_day)
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\" selected>0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\" selected>$i</option>";
                    }
                  }
                  else
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\">0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\">$i</option>";
                    }
                  }
              }
              ?>
              </select> <select name="estimate_year" >
                <?
              for($i=date("Y");$i<=date("Y")+3;$i++)
              {
                   if ($i == $e_year)
					    echo "<option value=\"$i\" selected >$i</option>";
				   else
					   echo "<option value=\"$i\">$i</option>";
              }
          ?>
              </select> <font color="#EE8810" size="1">*</font></td>
            <td width="15%" height="22" align="right" <?php if(substr_count($error_msg,"state") > 0) echo "class='errortext' "; else echo " class='maintext'; "; ?>>State</td>
            <td width="39%" height="22"><select name="to_state" >
			<? if ($to_state ==  "" && $_REQUEST['state'] != "") $to_state = strtoupper($_REQUEST['state']); ?>
                <option value="XX">Please Select</option>
                <option value="AL"<? if($to_state=='AL'){echo " selected";}?>>Alabama</option>
                <option value="AK"<? if($to_state=='AK'){echo " selected";}?>>Alaska</option>
                <option value="AZ"<? if($to_state=='AZ'){echo " selected";}?>>Arizona</option>
                <option value="AR"<? if($to_state=='AR'){echo " selected";}?>>Arkansas</option>
                <option value="CA"<? if($to_state=='CA'){echo " selected";}?>>California</option>
                <option value="CO"<? if($to_state=='CO'){echo " selected";}?>>Colorado</option>
                <option value="CT"<? if($to_state=='CT'){echo " selected";}?>>Connecticut</option>
                <option value="DE"<? if($to_state=='DE'){echo " selected";}?>>Delaware</option>
                <option value="FL"<? if($to_state=='FL'){echo " selected";}?>>Florida</option>
                <option value="GA"<? if($to_state=='GA'){echo " selected";}?>>Georgia</option>
                <option value="HI"<? if($to_state=='HI'){echo " selected";}?>>Hawaii</option>
                <option value="ID"<? if($to_state=='ID'){echo " selected";}?>>Idaho</option>
                <option value="IL"<? if($to_state=='IL'){echo " selected";}?>>Illinois</option>
                <option value="IN"<? if($to_state=='IN'){echo " selected";}?>>Indiana</option>
                <option value="IA"<? if($to_state=='IA'){echo " selected";}?>>Iowa</option>
                <option value="KS"<? if($to_state=='KS'){echo " selected";}?>>Kansas</option>
                <option value="KY"<? if($to_state=='KY'){echo " selected";}?>>Kentucky</option>
                <option value="LA"<? if($to_state=='LA'){echo " selected";}?>>Louisiana</option>
                <option value="ME"<? if($to_state=='ME'){echo " selected";}?>>Maine</option>
                <option value="MD"<? if($to_state=='MD'){echo " selected";}?>>Maryland</option>
                <option value="MA"<? if($to_state=='MA'){echo " selected";}?>>Massachusetts</option>
                <option value="MI"<? if($to_state=='MI'){echo " selected";}?>>Michigan</option>
                <option value="MN"<? if($to_state=='MN'){echo " selected";}?>>Minnesota</option>
                <option value="MS"<? if($to_state=='MS'){echo " selected";}?>>Mississippi</option>
                <option value="MO"<? if($to_state=='MO'){echo " selected";}?>>Missouri</option>
                <option value="MT"<? if($to_state=='MT'){echo " selected";}?>>Montana</option>
                <option value="NE"<? if($to_state=='NE'){echo " selected";}?>>Nebraska</option>
                <option value="NV"<? if($to_state=='NV'){echo " selected";}?>>Nevada</option>
                <option value="NH"<? if($to_state=='NH'){echo " selected";}?>>New 
                Hampshire</option>
                <option value="NJ"<? if($to_state=='NJ'){echo " selected";}?>>New 
                Jersey</option>
                <option value="NM"<? if($to_state=='NM'){echo " selected";}?>>New 
                Mexico</option>
                <option value="NY"<? if($to_state=='NY'){echo " selected";}?>>New 
                York</option>
                <option value="NC"<? if($to_state=='NC'){echo " selected";}?>>North 
                Carolina</option>
                <option value="ND"<? if($to_state=='ND'){echo " selected";}?>>North 
                Dakota</option>
                <option value="OH"<? if($to_state=='OH'){echo " selected";}?>>Ohio</option>
                <option value="OK"<? if($to_state=='OK'){echo " selected";}?>>Oklahoma</option>
                <option value="OR"<? if($to_state=='OR'){echo " selected";}?>>Oregon</option>
                <option value="PA"<? if($to_state=='PA'){echo " selected";}?>>Pennsylvania</option>
                <option value="PR"<? if($to_state=='PR'){echo " selected";}?>>Puerto 
                Rico</option>
                <option value="RI"<? if($to_state=='RI'){echo " selected";}?>>Rhode 
                Island</option>
                <option value="SC"<? if($to_state=='SC'){echo " selected";}?>>South 
                Carolina</option>
                <option value="SD"<? if($to_state=='SD'){echo " selected";}?>>South 
                Dakota</option>
                <option value="TN"<? if($to_state=='TN'){echo " selected";}?>>Tennessee</option>
                <option value="TX"<? if($to_state=='TX'){echo " selected";}?>>Texas</option>
                <option value="UT"<? if($to_state=='UT'){echo " selected";}?>>Utah</option>
                <option value="VT"<? if($to_state=='VT'){echo " selected";}?>>Vermont</option>
                <option value="VA"<? if($to_state=='VA'){echo " selected";}?>>Virginia</option>
                <option value="WA"<? if($to_state=='WA'){echo " selected";}?>>Washington</option>
                <option value="DC"<? if($to_state=='DC'){echo " selected";}?>>Washington 
                DC</option>
                <option value="WV"<? if($to_state=='WV'){echo " selected";}?>>West 
                Virginia</option>
                <option value="WI"<? if($to_state=='WI'){echo " selected";}?>>Wisconsin</option>
                <option value="WY"<? if($to_state=='WY'){echo " selected";}?>>Wyoming</option>
              </select> <font color="#EE8810" size="1">*</font> </td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td width="33%" height="19">&nbsp;</td>
            <td width="15%" height="19">&nbsp;</td>
            <td width="39%" height="19">&nbsp;</td>
          </tr>
          <tr> 
            <td colspan="2" height="16" class="heading" >Additional information:</td>
            <td colspan="2" height="16" class="heading" >&nbsp;</td>
          </tr>
          <tr> 
            <td width="13%" height="107" align="right" valign="top" class="maintext"> 
              <p>&nbsp;</p>
              <p>&nbsp; </p></td>
            <td height="107" colspan="3" class="maintext"> Comments<br> <textarea rows="5" name="comments" cols="35" ><?php echo $_SESSION["comments"]; ?></textarea> 
            </td>
          </tr>
          <tr> 
            <td width="13%" height="19">&nbsp;</td>
            <td height="19" colspan="3" class="maintext">Please describe any major 
              vehicle problems.<br> <br> </td>
          </tr>
          <? if ($_SESSION["nohouse"] != 1) { ?>
          <tr> 
            <td height="19" colspan="4" align="center" class="maintext"> 
              <?php $moving = $_SESSION["moving"]; ?>
              Are you moving any <b>Household Goods</b>? <br>
              Would you also like to get multiple <b>Household Moving</b> quotes
              <select size="1" name="moving">
                <option value="no"<? if($moving=='no'){echo " selected";}?>>No</option>
                <option value="yes"<? if($moving=='yes'){echo " selected";}?>>Yes</option>
              </select>              <br> 
               &nbsp;</td>
          </tr>
          <? }  ?>
          <tr> 
            <td height="19" colspan="4" align="center"> <input type="submit" value="Get Instant Quote" name="Submit" ></td>
          </tr>
        </table>
       
    </form> </td>
    <td width="254" valign="top" background="images/crightbg2.jpg"><img src="images/crightbg.jpg" width="322" height="648"> 
      <table width="300" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
        <tr> 
          <td align="center"><img src="image/seal.jpg" alt="Auto Shipping and Car shipping Guide" width="142" height="156" hspace="5" vspace="10"></td>
        </tr>
      </table></td>
  </tr>
</table>
