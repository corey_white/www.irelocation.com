<table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr> 
    <td background="images/cleftbg.jpg"><img src="images/spacer.gif" width="29" height="1"></td>
    <td bgcolor="#FFFFFF"> <br>
      <table width="702" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="213" align="center" valign="top"><table width="186" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td align="left" valign="top" class="maintext"> <p><a href="article.php?art=1">What 
                    You Should Know...</a></p>
                  <p><a href="article.php?art=2">Finding a Car Shipping Company</a></p>
                  <p><a href="article.php?art=7">Selecting a Car Shipping...</a></p>
                  <p><a href="article.php?art=3">Car Shipping insurance</a></p>
                  <p><a href="article.php?art=4">Car Shipping Contracts</a></p>
                  <p><a href="article.php?art=5">Auto Shipping Delivery Dates</a></p>
                  <p><a href="article.php?art=6">Preparing Your Car</a></p>
                  <p><br>
                  </p></td>
              </tr>
              <tr> 
                <td align="center" valign="top"><table width="186" border="0" cellspacing="0" cellpadding="0">
                    <!--<tr> 
                      <td align="center" valign="top"> 
                        <?include 'companies.php';?>
                        <br> </td>
                    </tr>
					-->
                    <tr> 
                      <td align="center" valign="top"><img src="image/seal.jpg" alt="Auto Transport Companies" width="142" height="156" hspace="5" vspace="10"></td>
                    </tr>
                    <tr> 
                      <td align="center" valign="top">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
          <td width="547" align="center" valign="top"><table width="504" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td height="113" align="center" valign="top"> 
                  <?php
	$page ="articles/";
	$page .=$art;
	$page .=".php";
		include($page);
	
?>
                </td>
              </tr>
              <tr> 
                <td height="276" align="center" valign="middle"> <table width="344" height="226" border="0" cellpadding="0" cellspacing="0">
                    <tr> 
                      <td align="center" valign="top" background="images/cquotebg.jpg"><br> 
                        <br> <form name="form1" method="post" action="quote.php">
                          <table width="266" border="0" cellspacing="0" cellpadding="3">
                            <tr> 
                              <td width="73" height="29" align="right" class="maintext">Email</td>
                              <td width="181"><input type="text" name="customer_email" size="25" tabindex="10"></td>
                            </tr>
                            <tr> 
                              <td align="right" class="maintext">Move Date</td>
                              <td><select name="estimate_month" tabindex="2">
                                  <option value="01"<? if($e_month=='1'){echo " selected";}?>>Jan</option>
                                  <option value="02"<? if($e_month=='2'){echo " selected";}?>>Feb</option>
                                  <option value="03"<? if($e_month=='3'){echo " selected";}?>>Mar</option>
                                  <option value="04"<? if($e_month=='4'){echo " selected";}?>>Apr</option>
                                  <option value="05"<? if($e_month=='5'){echo " selected";}?>>May</option>
                                  <option value="06"<? if($e_month=='6'){echo " selected";}?>>Jun</option>
                                  <option value="07"<? if($e_month=='7'){echo " selected";}?>>Jul</option>
                                  <option value="08"<? if($e_month=='8'){echo " selected";}?>>Aug</option>
                                  <option value="09"<? if($e_month=='9'){echo " selected";}?>>Sep</option>
                                  <option value="10"<? if($e_month=='10'){echo " selected";}?>>Oct</option>
                                  <option value="11"<? if($e_month=='11'){echo " selected";}?>>Nov</option>
                                  <option value="12"<? if($e_month=='12'){echo " selected";}?>>Dec</option>
                                </select> <select name="estimate_day" tabindex="13">
                                  <?
              for($i=1;$i<=31;$i++)
              {
                  if($i==$e_day)
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\" selected>0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\" selected>$i</option>";
                    }
                  }
                  else
                  {
                    if($i<10)
                    {
                      echo "<option value=\"0$i\">0$i</option>";
                    }
                    else
                    {
                      echo "<option value=\"$i\">$i</option>";
                    }
                  }
              }
              ?>
                                </select> <select name="estimate_year" tabindex="14">
                                  <?
              for($i=date("Y");$i<=date("Y")+3;$i++)
              {
                   if ($i == $e_year)
					    echo "<option value=\"$i\" selected >$i</option>";
				   else
					   echo "<option value=\"$i\">$i</option>";
              }
          ?>
                                </select></td>
                            </tr>
                            <tr> 
                              <td>&nbsp;</td>
                              <td><input type="submit" name="Submit" value="GO &gt;&gt;"></td>
                            </tr>
                          </table>
                        </form></td>
                    </tr>
                  </table></td>
              </tr>
              <tr> 
                <td align="center" valign="top">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table> 
    </td>
    <td background="images/cright3bg.jpg"><img src="images/spacer.gif" width="29" height="1"></td>
  </tr>

</table>
