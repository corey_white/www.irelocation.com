<?php
include("header.php");
?>

<script language=javascript>
<!--
function validate(form)
{
  if (form.fname.value == "")
  {
    alert("Please enter your first name");
    form.fname.focus()
    return false;
  }

  if (form.lname.value == "")
  {
    alert("Please enter your last name");
    form.lname.focus()
    return false;
  }

  var theStr = new String(form.email.value)
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
    {
      //pass
    }
    else
    {
      alert("Please enter a valid email address");
      form.email.focus()
      return false;
    }
  }
  else
  {
    alert("Please enter a valid email address");
    form.email.focus()
    return false;
  }
  
  if (form.phone.value == "")
  {
    alert("Please enter a phone number");
    form.phone.focus()
    return false;
  }

  var GoodChars = "0123456789"
  var i = 0
  var TempPhone = ""
  var LastChar = ""
  var count = 0
  for (i =0; i <= form.phone.value.length -1; i++) {
    if (GoodChars.indexOf(form.phone.value.charAt(i)) != -1)
    {
      if((TempPhone == "") && ((form.phone.value.charAt(i) == "1") || (form.phone.value.charAt(i) == "0")))
      {
      	//get rid of beginning 1 or 0
      }
      else
      {

        if(form.phone.value.charAt(i)==LastChar)
        {
          count=count+1;
          if(count==10)
          {
            alert("Please enter a valid 10-digit phone number");
            form.phone.focus()
            return false;
          }
        }
        else
        {
          count=0;
        }

        if(LastChar=="")
        {
          count=1;
        }

        TempPhone+=form.phone.value.charAt(i);
      }
      LastChar=form.phone.value.charAt(i);
    }
  }

  form.phone.value=TempPhone

  if(form.phone.value.match(/^[ ]*[(]{0,1}[ ]*[0-9]{3,3}[ ]*[)]{0,1}[-]{0,1}[ ]*[0-9]{3,3}[ ]*[-]{0,1}[ ]*[0-9]{4,4}[ ]*$/)==null)
  {
    alert("Please enter a valid 10-digit phone number");
    form.phone.focus()
    return false;
  }
  if (form.origin_zip.value == "")
  {
    alert("Please enter your origin zip code");
    form.origin_zip.focus()
    return false;
  }
  if (form.destination_city.value == "")
  {
    alert("Please enter your destination city");
    form.destination_city.focus()
    return false;
  }
  if (form.destination_city.value.match(/^\d{1,}$/)!=null)
  {
    alert("Please enter a valid destination city");
    form.destination_city.focus()
    return false;
  }
  if (form.destination_state.value == "XX")
  {
    alert("Please select your destination state");
    form.destination_state.focus()
    return false;
  }
  if (form.year.value == "")
  {
    alert("Please enter your vehicle year.");
    form.year.focus()
    return false;
  }
  if (form.make.options[form.make.selectedIndex].value=="false"){
    alert("Please select the vehicle's make");
    form.make.focus()
    return false;
  }
  if (form.model.options[form.model.selectedIndex].value=="false")
  {
    alert("Please select the vehicle's model");
    form.make.focus()
    return false;
  }


}
function inactive()
{
	alert("This quote page is not yet active.  Please revisit this site soon.");
}
// -->
</script>
<script language="javascript" type="text/javascript" src="vehicles.js"></script>
          <form name="quote_form" method="post" action="submit_quote.php" onSubmit="return validate(this)">
          <p align="center"><br>
          <img border="0" src="images/car_side.jpg"></p>
          <p align="center"><font class="text_dark"><?if(isset($msg)){echo '<b><font color="#FF0000">'.$msg.'</font></b><br>';}?>
          Simply fill out our one form to receive competitive quotes from multiple car shipping companies by e-mail.</font></b></font></p>
          <table border="0" cellspacing="0" width="100%" class="text_form1" cellpadding="4"><tr><td>
          <table border="0" width="100%" id="table09" cellspacing="0" cellpadding="4" class="text_form1">
            <tr>
              <td width="170" colspan="2" class="text_dark"><b>
              Please enter
          your contact information:</b></td><td></td>
            </tr>
            <tr>
              <td width="170">First Name:</td>
              <td>
              <input name="fname" size="20" class="field1" value="<?echo $fname;?>"></td>
            </tr>
	    <tr>
              <td width="170">Last Name:</td>
              <td>
              <input name="lname" size="20" class="field1" value="<?echo $lname;?>"></td>
            </tr>
            <tr>
              <td width="170">E-Mail Address:</td>
              <td>
              <input name="email" size="32" class="field1" value="<?echo $email;?>"></td>
            </tr>
            <tr>
              <td width="170">Phone Number:</td>
              <td>
              <input name="phone" size="20" class="field1" value="<?echo $phone;?>"></td>
            </tr>
			<?
			#
			# I know it's a bit of an overkill for displaying dates, but at least it's consistant
			#
			?>
			<tr>
				<td width="170">Approximate Moving Date:</td>
				<td><select size="1" name="move_month" class="field1">
				<?for($i=1;$i<=12;$i++){ $mytime=mktime(0,0,0,$i,1,1);?>
				<option value="<?echo date("m",$mytime);?>"<?if($month==date("m",$mytime)){echo " selected";}elseif(($month=='') && (date("m",$mytime)==date("m",time()+1296000))){echo " selected";}?>><?echo date("M",$mytime);?></option>
				<?}?>
				</select><select size="1" name="move_day" class="field1">
				<?for($i=1;$i<=31;$i++){ $mytime=mktime(0,0,0,1,$i,1);?>
				<option value="<?echo date("d",$mytime);?>"<?if($day==date("d",$mytime)){echo " selected";}elseif(($day=='') && (date("d",$mytime)==date("d",time()+1296000))){echo " selected";}?>><?echo date("j",$mytime);?></option>
				<?}?>
				</select><select size="1" name="move_year" class="field1">
				<?for($i=date("Y");$i<=(date("Y")+1);$i++){ $mytime=mktime(0,0,0,1,1,$i);?>
				<option value="<?echo date("Y",$mytime);?>"<?if($year==date("Y",$mytime)){echo " selected";}elseif(($year=='') && (date("Y",$mytime)==date("Y",time()+1296000))){echo " selected";}?>><?echo date("Y",$mytime);?></option>
				<?}?>
				</select></td>
			</tr>
            <tr>
              <td width="170">
              Moving From:</td>
              <td class="text_dark">
			  <span style="font-weight: 400">zip/postal code
              <input size="9" name="origin_zip" class="field1" maxlength="5" value="<?echo $origin_zip;?>"></span></td>
            </tr>
            <tr>
              <td width="170">
              Moving To:</td>
              <td class="text_dark">
              <span style="font-weight: 400">city
              <input size="9" name="destination_city" class="field1" value="<?echo $destination_city;?>"></span>
              <select size="1" name="destination_state"<?if($agent_level!='low'){echo ' class="field1"';}?>>
              <option value="XX">- Please Select -</option>
              <option value="AL"<?if($destination_state=='AL'){echo " selected";}?>>Alabama</option>
              <option value="AK"<?if($destination_state=='AK'){echo " selected";}?>>Alaska</option>
              <option value="AZ"<?if($destination_state=='AZ'){echo " selected";}?>>Arizona</option>
              <option value="AR"<?if($destination_state=='AR'){echo " selected";}?>>Arkansas</option>
              <option value="CA"<?if($destination_state=='CA'){echo " selected";}?>>California</option>
              <option value="CO"<?if($destination_state=='CO'){echo " selected";}?>>Colorado</option>
              <option value="CT"<?if($destination_state=='CT'){echo " selected";}?>>Connecticut</option>
              <option value="DC"<?if($destination_state=='DC'){echo " selected";}?>>DC</option>
              <option value="DE"<?if($destination_state=='DE'){echo " selected";}?>>Delaware</option>
              <option value="FL"<?if($destination_state=='FL'){echo " selected";}?>>Florida</option>
              <option value="GA"<?if($destination_state=='GA'){echo " selected";}?>>Georgia</option>
              <option value="HI"<?if($destination_state=='HI'){echo " selected";}?>>Hawaii</option>
              <option value="ID"<?if($destination_state=='ID'){echo " selected";}?>>Idaho</option>
              <option value="IL"<?if($destination_state=='IL'){echo " selected";}?>>Illinois</option>
              <option value="IN"<?if($destination_state=='IN'){echo " selected";}?>>Indiana</option>
              <option value="IA"<?if($destination_state=='IA'){echo " selected";}?>>Iowa</option>
              <option value="KS"<?if($destination_state=='KS'){echo " selected";}?>>Kansas</option>
              <option value="KY"<?if($destination_state=='KY'){echo " selected";}?>>Kentucky</option>
              <option value="LA"<?if($destination_state=='LA'){echo " selected";}?>>Louisiana</option>
              <option value="ME"<?if($destination_state=='ME'){echo " selected";}?>>Maine</option>
              <option value="MD"<?if($destination_state=='MD'){echo " selected";}?>>Maryland</option>
              <option value="MA"<?if($destination_state=='MA'){echo " selected";}?>>Massachusetts</option>
              <option value="MI"<?if($destination_state=='MI'){echo " selected";}?>>Michigan</option>
              <option value="MN"<?if($destination_state=='MN'){echo " selected";}?>>Minnesota</option>
              <option value="MS"<?if($destination_state=='MS'){echo " selected";}?>>Mississippi</option>
              <option value="MO"<?if($destination_state=='MO'){echo " selected";}?>>Missouri</option>
              <option value="MT"<?if($destination_state=='MT'){echo " selected";}?>>Montana</option>
              <option value="NE"<?if($destination_state=='NE'){echo " selected";}?>>Nebraska</option>
              <option value="NV"<?if($destination_state=='NV'){echo " selected";}?>>Nevada</option>
              <option value="NH"<?if($destination_state=='NH'){echo " selected";}?>>New Hampshire</option>
              <option value="NJ"<?if($destination_state=='NJ'){echo " selected";}?>>New Jersey</option>
              <option value="NM"<?if($destination_state=='NM'){echo " selected";}?>>New Mexico</option>
              <option value="NY"<?if($destination_state=='NY'){echo " selected";}?>>New York</option>
              <option value="NC"<?if($destination_state=='NC'){echo " selected";}?>>North Carolina</option>
              <option value="ND"<?if($destination_state=='ND'){echo " selected";}?>>North Dakota</option>
              <option value="OH"<?if($destination_state=='OH'){echo " selected";}?>>Ohio</option>
              <option value="OK"<?if($destination_state=='OK'){echo " selected";}?>>Oklahoma</option>
              <option value="OR"<?if($destination_state=='OR'){echo " selected";}?>>Oregon</option>
              <option value="PA"<?if($destination_state=='PA'){echo " selected";}?>>Pennsylvania</option>
              <option value="RI"<?if($destination_state=='RI'){echo " selected";}?>>Rhode Island</option>
              <option value="SC"<?if($destination_state=='SC'){echo " selected";}?>>South Carolina</option>
              <option value="SD"<?if($destination_state=='SD'){echo " selected";}?>>South Dakota</option>
              <option value="TN"<?if($destination_state=='TN'){echo " selected";}?>>Tennessee</option>
              <option value="TX"<?if($destination_state=='TX'){echo " selected";}?>>Texas</option>
              <option value="UT"<?if($destination_state=='UT'){echo " selected";}?>>Utah</option>
              <option value="VT"<?if($destination_state=='VT'){echo " selected";}?>>Vermont</option>
              <option value="VA"<?if($destination_state=='VA'){echo " selected";}?>>Virginia</option>
              <option value="WA"<?if($destination_state=='WA'){echo " selected";}?>>Washington</option>
              <option value="WV"<?if($destination_state=='WV'){echo " selected";}?>>West Virginia</option>
              <option value="WI"<?if($destination_state=='WI'){echo " selected";}?>>Wisconsin</option>
              <option value="WY"<?if($destination_state=='WI'){echo " selected";}?>>Wyoming</option>
              </select></td>
            </tr>
            <tr>
              <td width="170">
              Preferred Contact Method:&nbsp;</td>
              <td>
              <select size="1" name="contact"<?if($agent_level!='low'){echo ' class="field1"';}?>>
              <option value="email" selected>email</option>
              <option value="phone">phone</option>
              </select></td>
            </tr>
            <tr>
              <td width="170">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="170" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Vehicle Information:</b></td><td></td>
            </tr>
            </table>
            <table border="0" width="100%" id="table10" cellspacing="0" cellpadding="4" class="text_form2">
            <tr>
              <td width="106">Vehicle Type:</td>
              <td><select size="1" name="type" tabindex="1" class="field2">
                  <option value="Car">Car</option>
                  <option value="Sport Utility">Sport Utility</option>
                  <option value="Pick-up Truck">Pick-up Truck</option>
                  <option value="Oversize">Oversize</option>
                  <option value="Other Truck">Other Truck</option>
                  <option value="RV">RV</option>
                  <option value="Convertible">Convertible</option>
                  <option value="Van">Van</option>
                  <option value="Mini-Van">Mini-Van</option>
                  <option value="Other">Other</option>
                  </select></td>
            </tr>
            <tr>
              <td width="106">Vehicle Year:</td>
              <td><input type="text" name="year" maxlength="4" size="8" class="field2" value="<?echo $year;?>"></td>
            </tr>
            <tr>
              <td width="106">Vehicle Make:</td>
              <td>
              <select size="1" name="make" class="field2" onChange="setupModels(this.form, 0);" >
			<option value="false">Select A Make -�</option>
			</select></td>
            </tr>
            <tr>
              <td width="106">Vehicle Model:</td>
              <td><select size="1" name="model" class="field2" onChange="setupNums(this.form);">
			<option value="false">Select A Model -�</option>
			<option value="0">-Car Not Listed-</option>
			<option value="0">-Truck Not Listed-</option>
			</select></td>
            </tr>
            <tr>
              <td width="106">Vehicle Condition:</td>
              <td><select name="condition" class="field2">
                <option value="running">Running</option>
                <option value="non-Running">Non-Running</option>
              </select></td>
            </tr>
            <tr>
              <td width="106">Please add any special comments and/or describe any major vehicle problems:</td>
              <td><textarea rows="7" name="comments" cols="44" class="field2"><?echo $comments;?></textarea></td>
            </tr>
            <tr>
              <td width="106">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            </table>
            </td></tr>
            <tr><td align="center">Are you moving any household goods? <br>
          	Would you also like to get multiple moving quotes
          	<select size="1" name="moving">
          	<option value="yes">Yes</option>
          	<option value="no" selected>No</option>
          	</select><br>&nbsp;
          	</td></tr>
            <tr><td align="center"><input type="submit" value="Submit Quote Request" name="submit"<?if($agent_level!='low'){echo ' class="button1"';}?>>
            </td></tr></table>
          <p align="center">&nbsp;</p>
<input type="hidden" name="makenum" value="">
<input type="hidden" name="modelnum" value="">
<input type="hidden" name="makedescription" value="">
<input type="hidden" name="modeldescription" value="">
<input type="hidden" name="referer" value="<?echo $mysource;?>">
<input type="hidden" name="keyword" value="<?echo $mykeyword;?>">

<script language=javascript>
<!--
setupForm(document.quote_form);
// -->
</script>
</form>
<?
include("footer.php");
?>
