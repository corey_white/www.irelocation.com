<?
include("header.php");
?>
<h2 align="center"><font face="Arial,Sans-serif">Auto Transport FAQ</h2>
<h3><font face="Arial"><a name="1">How much will it cost to ship my
vehicle?</a></font></h3>
<p><font face="Arial" size="2">Price is determines by several factors which 
consist of the size of your vehicle, if your vehicle is running or inoperable, 
optional enclosed or open trailer transport and of course where your vehicle is 
being shipped from and too. </font></p>
<p><font face="Arial" size="2">As of November 2003, the average price to ship 
your vehicle from Los Angeles, CA to New York, NY: </font></p>
<blockquote>
  <p><font face="Arial" size="2"><i><b>Price for shipping a 2003 Honda Accord</b></i>
  <br>
  Open Trailer, Door to Door: $950 <br>
  Open Trailer, Terminal to Terminal: $825 <br>
  Open Trailer, Non-Running: $1145 <br>
  Enclosed Trailer, Door to Door: $1895 </font></p>
</blockquote>
<p><font face="Arial" size="2">Don't be taken advantage of regarding price. If 
you receive an outrageously low quote, it's most likely to good to be true. A 
perfect example of this is from a company called Southerland Auto Transport, aka 
Movingmycar.com. </font></p>
<p><font face="Arial" size="2">This company currently implements bait-and-switch 
tactics that have been well documented on MSNBC.com. They offer you &quot;the 
consumer&quot; a much lower price then any of there competition, so you sign-up 
thinking you're getting a great deal. You proceed to pay $200 to hold a spot on 
the truck - &quot;this is the bait&quot;. </font></p>
<p><font face="Arial" size="2">Two months later, when you don't have your car 
and you've never been more upset in your entire life, this company will tell you 
to spend another $200 for an express service. Don't worry - law enforcement, 
elderly folks and everyday people have all fallen victim to this scheme. The 
only reason this company is still in business and not in jail is because all 
this information is clearly written in their contract. READ YOUR CONTRACT!
</font></p>
<h3><a name="2"><font face="Arial,Sans-Serif">How far in advance should I
schedule?</font></h3></a>
<p><font face="Arial" size="2">
Car carriers ask that you attempt to order transportation service at least 10 
days prior to your requested pick-up date. By placing your order in advance, 
carriers have a greater chance of picking up your vehicle on or as close as 
possible to your requested date.&nbsp; Since your vehicle is 1 of 10 cars that 
need to be coordinated to a single location, delays can come from either short 
notice or difficult rural locations.</font></p>
<p><font face="Arial" size="2">Avoid these delay by booking early with a 
reliable broker or carrier.&nbsp; Traditionally, pick-up of your vehicle will be 
between 5 and 10 days. If, however, your vehicle is available immediately, most 
companies will attempt to work with you on your preferred dates.</font></p>
<h3><a name="3"><font face="Arial,Sans-serif">What should I look for when
selecting a company?</font></h3></a>
<p><font face="Arial" size="2">Auto transport companies and car shipping companies
can come in very useful when you are looking to ship your vehicle. They will 
relocate your vehicle for a fee. Auto transport companies and car shipper 
companies have state of the art carriers capable of shipping many vehicles at 
once, safely and quickly. </font></p>
<p><font face="Arial" size="2">There are some basic pieces of information you 
need to know while looking for auto transport and car shipping companies and our 
Auto Transport guide is designed to provide you with that information. </font>
</p>
<p><font face="Arial" size="2">You can find vehicle transport companies 
advertised in your local papers, phone book and of course the Internet. 
AutoTransportSolutions.com is committed to working with industry professionals 
only and if you are in need for a vehicle transport company and required 
unmatched service, simply submit a request for quote on this site. </font></p>
<p align="left">&nbsp;</p>

<?
include("footer.php");
?>
