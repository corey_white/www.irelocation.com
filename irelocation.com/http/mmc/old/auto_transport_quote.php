<?php
include("header.php");

	
	$error_msg = $_REQUEST["msg"];
	$msgs["link_inject"] = "Please do not include Live Links in your Comments";
	$msgs["duplicate"] = "We see that you have allready submitted a quote today.  Please wait for these companies to contact you.";
	$msgs["invalid_phone"] = "Sorry, we could not validate your Area Code or Prefix.";
	$msgs["fname"] = "Please enter your First Name";
	$msgs["lname"] = "Please enter your Last Name";
	$msgs["phone"] = "Please enter your Phone Number";
	$msgs["email"] = "Please enter your Email Address";
	$msgs["move_month"] = "Please select your Move Month.";
	$msgs["origin_zip"] = "Please enter your Starting Zip Code.";
	$msgs["destination_city"] = "Please enter you Destination City.";
	$msgs["destination_state"] = "Please enter you Destination State.";
	$msgs["destination"] = "Your destination could not be validated, please try again.";
	$msgs["year"] = "Please Enter the Year of your ".$_SESSION["type"];
	$msgs["make"] = "Please Enter the Make of your ".$_SESSION["type"];
	$msgs["model"] = "Please Enter the Model of your ".$_SESSION["type"];
	$msgs["origin_zip_invalid"] = "The Starting Zip code you entered was not found.";
	$msg = $msgs[$error_msg];
?>

          <!--- <form name="quote_form" method="post" action="submit_quote.php" onSubmit="return validate(this)"> --->
		  <form name="quote_form" method="post" action="submit_quote.php" >
          <p align="center"><br>
          <img border="0" src="images/car_side.jpg"></p>
			<p align="center">
				<font class="text_dark">					
					Simply fill out our one form to receive competitive quotes from multiple car shipping companies by e-mail.
				</font>
			</p>
			
          <table border="0" cellspacing="0" width="100%" class="text_form1" cellpadding="4"><tr><td>
          <table border="0" width="100%" id="table09" cellspacing="0" cellpadding="4" class="text_form2">
	 	   <? if(isset($msg)) {  ?>
            <tr class="errortext">
				<td>&nbsp;</td>
				<td colspan="2">
				<? echo $msg;?>
				</td>
			</tr>
			<? } ?>
			<tr>
              <td width="170" colspan="2" class="text_dark"><b>
              Please enter
          your contact information:</b></td><td></td>
            </tr>
            <tr>
              <td width="170" <?php if (substr_count($error_msg,"fname") > 0) echo " class='errortext' "; ?> >First Name:</td>
              <td>
              <input name="fname" size="20" class="field1" value="<? echo $_SESSION["fname"];?>">
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
	    <tr>
              <td width="170" <?php if (substr_count($error_msg,"lname") > 0) echo " class='errortext' "; ?>>Last Name:</td>
              <td>
              <input name="lname" size="20" class="field1" value="<? echo $_SESSION["lname"];?>">
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
            <tr>
              <td width="170" <?php if (substr_count($error_msg,"email") > 0) echo " class='errortext' "; ?>>E-Mail Address:</td>
              <td>
              <input name="email" size="32" class="field1" value="<? echo $_SESSION["email"];?>">
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
            <tr>
              <td width="170" <?php if (substr_count($error_msg,"phone") > 0) echo " class='errortext' "; ?>>Phone Number:</td>
              <td>
              <input name="phone" size="20" class="field1" value="<? echo $_SESSION["phone"];?>">
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
			<?
			#
			# I know it's a bit of an overkill for displaying dates, but at least it's consistant
			#
			?>
			<tr>
				<td width="170" <?php if (substr_count($error_msg,"move_month") > 0) echo " class='errortext' "; ?>>Approximate Moving Date:</td>
				<td>
				<select size="1" name="move_month" class="field1">
				<? for($i=1;$i<=12;$i++)
					{ 
						$mytime=mktime(0,0,0,$i,1,1); 
					?>
						<option value="<?= date("m",$mytime);?>"
					<? 
						if ($month == date("m",$mytime) )
						{
							echo " selected";
						}
						else if (($month=='') && (date("m",$mytime) == date("m",time()+1296000)))
						{
							echo " selected";
						} 
					?>>
					<?= date("M",$mytime); ?>
					</option>
				<? } ?>
				</select><select size="1" name="move_day" class="field1">
				<?for($i=1;$i<=31;$i++){ $mytime=mktime(0,0,0,1,$i,1);?>
				<option value="<?echo date("d",$mytime);?>"<? if($day==date("d",$mytime)){echo " selected";}elseif(($day=='') && (date("d",$mytime)==date("d",time()+1296000))){echo " selected";} ?>><?echo date("j",$mytime);?></option>
				<?} ?>
				</select><select size="1" name="move_year" class="field1">
				<?for($i=date("Y");$i<=(date("Y")+1);$i++){ $mytime=mktime(0,0,0,1,1,$i);?>
				<option value="<?echo date("Y",$mytime);?>"<? if($year==date("Y",$mytime)){echo " selected";}elseif(($year=='') && (date("Y",$mytime)==date("Y",time()+1296000))){echo " selected";} ?>><?echo date("Y",$mytime);?></option>
				<?} ?>
				</select>
              <font color="#FF0000" size="1">Required</font></td>
			</tr>
            <tr>
              <td width="170" <?php if (substr_count($error_msg,"origin_zip") > 0) echo " class='errortext' "; ?>>
	              Moving From:
			   </td>
              
            <td class="text_dark"> <span style="font-weight: 400">zip/postal code 
              <input size="9" name="origin_zip" class="field1" maxlength="5" value="<? echo $_SESSION["origin_zip"]; ?>">
              <font color="#FF0000" size="1">Required</font></span></td>
            </tr>
            <tr>
              <td width="170" <?php if (substr_count($error_msg,"destination") > 0) echo " class='errortext' "; ?>>
             	 Moving To:
			  </td>
              <td class="text_dark">
              <span style="font-weight: 400">city
              <input size="9" name="destination_city" class="field1" value="<? echo $_SESSION["destination_city"]; ?>">
              </span> <font color="#FF0000" size="1">Required</font> 
              <select size="1" name="destination_state"<? if($agent_level!='low'){echo ' class="field1"';} ?>>
              <option value="XX">- Please Select -</option>
              <option value="AL"<? if($_SESSION["destination_state"]=='AL'){echo " selected";} ?>>Alabama</option>
              <option value="AK"<? if($_SESSION["destination_state"]=='AK'){echo " selected";} ?>>Alaska</option>
              <option value="AZ"<? if($_SESSION["destination_state"]=='AZ'){echo " selected";} ?>>Arizona</option>
              <option value="AR"<? if($_SESSION["destination_state"]=='AR'){echo " selected";} ?>>Arkansas</option>
              <option value="CA"<? if($_SESSION["destination_state"]=='CA'){echo " selected";} ?>>California</option>
              <option value="CO"<? if($_SESSION["destination_state"]=='CO'){echo " selected";} ?>>Colorado</option>
              <option value="CT"<? if($_SESSION["destination_state"]=='CT'){echo " selected";} ?>>Connecticut</option>
              <option value="DC"<? if($_SESSION["destination_state"]=='DC'){echo " selected";} ?>>DC</option>
              <option value="DE"<? if($_SESSION["destination_state"]=='DE'){echo " selected";} ?>>Delaware</option>
              <option value="FL"<? if($_SESSION["destination_state"]=='FL'){echo " selected";} ?>>Florida</option>
              <option value="GA"<? if($_SESSION["destination_state"]=='GA'){echo " selected";} ?>>Georgia</option>
              <option value="HI"<? if($_SESSION["destination_state"]=='HI'){echo " selected";} ?>>Hawaii</option>
              <option value="ID"<? if($_SESSION["destination_state"]=='ID'){echo " selected";} ?>>Idaho</option>
              <option value="IL"<? if($_SESSION["destination_state"]=='IL'){echo " selected";} ?>>Illinois</option>
              <option value="IN"<? if($_SESSION["destination_state"]=='IN'){echo " selected";} ?>>Indiana</option>
              <option value="IA"<? if($_SESSION["destination_state"]=='IA'){echo " selected";} ?>>Iowa</option>
              <option value="KS"<? if($_SESSION["destination_state"]=='KS'){echo " selected";} ?>>Kansas</option>
              <option value="KY"<? if($_SESSION["destination_state"]=='KY'){echo " selected";} ?>>Kentucky</option>
              <option value="LA"<? if($_SESSION["destination_state"]=='LA'){echo " selected";} ?>>Louisiana</option>
              <option value="ME"<? if($_SESSION["destination_state"]=='ME'){echo " selected";} ?>>Maine</option>
              <option value="MD"<? if($_SESSION["destination_state"]=='MD'){echo " selected";} ?>>Maryland</option>
              <option value="MA"<? if($_SESSION["destination_state"]=='MA'){echo " selected";} ?>>Massachusetts</option>
              <option value="MI"<? if($_SESSION["destination_state"]=='MI'){echo " selected";} ?>>Michigan</option>
              <option value="MN"<? if($_SESSION["destination_state"]=='MN'){echo " selected";} ?>>Minnesota</option>
              <option value="MS"<? if($_SESSION["destination_state"]=='MS'){echo " selected";} ?>>Mississippi</option>
              <option value="MO"<? if($_SESSION["destination_state"]=='MO'){echo " selected";} ?>>Missouri</option>
              <option value="MT"<? if($_SESSION["destination_state"]=='MT'){echo " selected";} ?>>Montana</option>
              <option value="NE"<? if($_SESSION["destination_state"]=='NE'){echo " selected";} ?>>Nebraska</option>
              <option value="NV"<? if($_SESSION["destination_state"]=='NV'){echo " selected";} ?>>Nevada</option>
              <option value="NH"<? if($_SESSION["destination_state"]=='NH'){echo " selected";} ?>>New Hampshire</option>
              <option value="NJ"<? if($_SESSION["destination_state"]=='NJ'){echo " selected";} ?>>New Jersey</option>
              <option value="NM"<? if($_SESSION["destination_state"]=='NM'){echo " selected";} ?>>New Mexico</option>
              <option value="NY"<? if($_SESSION["destination_state"]=='NY'){echo " selected";} ?>>New York</option>
              <option value="NC"<? if($_SESSION["destination_state"]=='NC'){echo " selected";} ?>>North Carolina</option>
              <option value="ND"<? if($_SESSION["destination_state"]=='ND'){echo " selected";} ?>>North Dakota</option>
              <option value="OH"<? if($_SESSION["destination_state"]=='OH'){echo " selected";} ?>>Ohio</option>
              <option value="OK"<? if($_SESSION["destination_state"]=='OK'){echo " selected";} ?>>Oklahoma</option>
              <option value="OR"<? if($_SESSION["destination_state"]=='OR'){echo " selected";} ?>>Oregon</option>
              <option value="PA"<? if($_SESSION["destination_state"]=='PA'){echo " selected";} ?>>Pennsylvania</option>
              <option value="RI"<? if($_SESSION["destination_state"]=='RI'){echo " selected";} ?>>Rhode Island</option>
              <option value="SC"<? if($_SESSION["destination_state"]=='SC'){echo " selected";} ?>>South Carolina</option>
              <option value="SD"<? if($_SESSION["destination_state"]=='SD'){echo " selected";} ?>>South Dakota</option>
              <option value="TN"<? if($_SESSION["destination_state"]=='TN'){echo " selected";} ?>>Tennessee</option>
              <option value="TX"<? if($_SESSION["destination_state"]=='TX'){echo " selected";} ?>>Texas</option>
              <option value="UT"<? if($_SESSION["destination_state"]=='UT'){echo " selected";} ?>>Utah</option>
              <option value="VT"<? if($_SESSION["destination_state"]=='VT'){echo " selected";} ?>>Vermont</option>
              <option value="VA"<? if($_SESSION["destination_state"]=='VA'){echo " selected";} ?>>Virginia</option>
              <option value="WA"<? if($_SESSION["destination_state"]=='WA'){echo " selected";} ?>>Washington</option>
              <option value="WV"<? if($_SESSION["destination_state"]=='WV'){echo " selected";} ?>>West Virginia</option>
              <option value="WI"<? if($_SESSION["destination_state"]=='WI'){echo " selected";} ?>>Wisconsin</option>
              <option value="WY"<? if($_SESSION["destination_state"]=='WI'){echo " selected";} ?>>Wyoming</option>
              </select>
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
            <tr>
              <td width="170">
              Preferred Contact Method:&nbsp;</td>
              <td>
              <select size="1" name="contact"<? if($agent_level!='low'){echo ' class="field1"';} ?>>
              <option value="email" selected>email</option>
              <option value="phone">phone</option>
              </select></td>
            </tr>
            <tr>
              <td width="170">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="170" colspan="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              Vehicle Information:</b></td><td></td>
            </tr>
            </table>
            <table border="0" width="100%" id="table10" cellspacing="0" cellpadding="4" class="text_form2">
            <tr>
              <td width="169">Vehicle Type:</td>
              <td width="777"><select size="1" name="type" tabindex="1" class="field2">
                  <option value="Car"  <?php if ($_SESSION["type"] == "Car") echo " selected "; ?>>Car</option>
                  <option value="Sport Utility" <?php if ($_SESSION["type"] == "Sport Utility") echo "selected ";?> >Sport Utility</option>
                  <option value="Pick-up Truck" <?php if ($_SESSION["type"] == "Pick-up Truck") echo "selected ";?> >Pick-up Truck</option>
                  <option value="Oversize" <?php if ($_SESSION["type"] == "Oversize") echo "selected ";?>>Oversize</option>
                  <option value="Other Truck" <?php if ($_SESSION["type"] == "Other Truck") echo "selected ";?>>Other Truck</option>
                  <option value="RV" <?php if ($_SESSION["type"] == "RV") echo "selected ";?>>RV</option>
                  <option value="Convertible" <?php if ($_SESSION["type"] == "Convertible") echo "selected ";?>>Convertible</option>
                  <option value="Van" <?php if ($_SESSION["type"] == "Van") echo "selected ";?>>Van</option>
                  <option value="Mini-Van" <?php if ($_SESSION["type"] == "Mini-Van") echo "selected ";?>>Mini-Van</option>
                  <option value="Other" <?php if ($_SESSION["type"] == "Other") echo "selected ";?>>Other</option>
                  </select></td>
            </tr>
			 <tr>
              <td width="169" <?php if (substr_count($error_msg,"year") > 0) echo " class='errortext' "; ?>>Vehicle Year:</td>
              <td><input type="text" name="year" maxlength="4" size="8" class="field2" value="<? echo $_SESSION["year"];?>">
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
						
            <tr>
              <td width="169" <?php if (substr_count($error_msg,"make") > 0) echo " class='errortext' "; ?>>Vehicle Make:</td>
              <td><input type="text" name="make" value="<? echo $_SESSION["make"]; ?>"  class="field2"/>
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
            <tr>
              <td width="169" <?php if (substr_count($error_msg,"model") > 0) echo " class='errortext' "; ?>>Vehicle Model:</td>
              <td><input type="text" name="model" value="<? echo $_SESSION["model"]; ?>"  class="field2"/>
              <select name="condition" class="field2">
                <option value="running" <?php if ($_SESSION["condition"] == "running") echo " selected "; ?>>Running</option>
                <option value="non-Running" <?php if ($_SESSION["condition"] == "non-Running") echo " selected "; ?>>Non-Running</option>
              </select>
              <font color="#FF0000" size="1">Required</font></td>
            </tr>
			
            <tr>
              <td width="169">Vehicle Condition:</td>
              
            <td>&nbsp;</td>
            </tr>
            <tr>
              <td width="169">Please add any special comments and/or describe any major vehicle problems:</td>
              <td><textarea rows="7" name="comments" cols="44" class="field2"><? echo $_SESSION["comments"] ?></textarea></td>
            </tr>
            <tr>
              <td width="169">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            </table>
            </td></tr>
            <tr><td align="center">Are you moving any household goods? <br>
          	Would you also like to get multiple moving quotes
          	<select size="1" name="moving">          	
          	<option value="no" <?php if ($_SESSION["moving"] == "no") echo " selected "; ?> >No</option>
			<option value="yes" <?php if ($_SESSION["moving"] == "yes") echo " selected "; ?>>Yes</option>
          	</select><br>&nbsp;
          	</td></tr>
            <tr><td align="center"><input type="submit" value="Submit Quote Request" name="submit" <? if($agent_level!='low'){echo ' class="button1"';} ?>>
            </td></tr></table>
          <p align="center">&nbsp;</p>
<input type="hidden" name="makenum" value="">
<input type="hidden" name="modelnum" value="">
<input type="hidden" name="makedescription" value="">
<input type="hidden" name="modeldescription" value="">
<input type="hidden" name="referer" value="<?echo $mysource;?>">
<input type="hidden" name="keyword" value="<?echo $mykeyword;?>">

<script language=javascript>
<!--
setupForm(document.quote_form);
// -->
</script>
</form>
<?
include("footer.php");
?>

