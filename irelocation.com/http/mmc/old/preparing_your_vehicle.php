<?
include("header.php");
?>
<p><b><font face="Arial" color="#176FA8">Preparing Your Vehicle</font></b></p>
<p><i><b><font size="2" face="Arial">Most auto transport companies will require 
that you do some preparation in advance before shipping.&nbsp; He are a few 
quick tips!</font></b></i></p>
<ul>
  <li><font size="2" face="Arial">Most companies will not allow you to carrier 
  more than 100 lbs of personal belongings in the vehicle during shipping, and 
  it more than likely has to be in the trunk.&nbsp; Personal items inside the 
  vehicle will not be covered if lost or stolen.</font></li>
  <li><font size="2" face="Arial">Make sure you inspect your vehicle with the 
  shipping company.&nbsp; All exterior and interior defects should be noted.&nbsp; 
  If no inspection is done, then the transport company probably won't be liable 
  for any damages.&nbsp; Also, when you pick up your vehicle at your 
  destination, you will do another inspection.&nbsp; Make sure the transport 
  company washes off your vehicle so that you can do a proper inspection.&nbsp; 
  Do not pick up your car if you are in a rush!</font></li>
  <li><font size="2" face="Arial">Make sure that you vehicle is personally 
  insured.&nbsp; Most transport companies will not repair damages due to normal 
  road conditions.&nbsp; For example, if your vehicle is struck while sitting on 
  a lot, even under the control of the carrier, your insurance would have to 
  cover the damages.&nbsp; If the vehicle was damaged directly by the carrier, 
  then the carrier would be responsible.</font></li>
  <li><font size="2" face="Arial">It is a good idea to have your vehicle 
  serviced and the gas tank low (not empty) prior to transport.</font></li>
  <li><font size="2" face="Arial">There will be an extra charge if your vehicle 
  is inoperable, or if there are any other additions to your vehicle such as 
  camper shells, lifted/oversized trucks, and other things that you did not tell 
  the company prior to shipping.&nbsp; Make sure you specify any and all 
  modifications to your vehicle for a proper quote.</font></li>
  <li><font size="2" face="Arial">Make sure that you have a complete set of keys 
  for the transport company, and a full extra set for yourself.</font></li>
  <li><font size="2" face="Arial">If you are transporting a classic, custom, 
  rare, exotic, or otherwise expensive vehicle, go for the enclosed transport if 
  you can... it will be worth the extra cost!</font></li>
</ul>
<p align="left">&nbsp;</p>

<?
include("footer.php");
?>