<?php
	session_start();
	include_once ('inc_mysql.php');
	include_once ('retention.php');
	
	extract($_POST);
	$_SESSION['customer_email'] = $customer_email;
	$_SESSION['estimate_month'] = $estimate_month;
	$_SESSION['estimate_day'] = $estimate_day;
	$_SESSION['estimate_year'] = $estimate_year;
	
	$retention = saveData($customer_email, "mmc", $_SESSION['mysource']);
	
	if ($retention == DUPLICATE)	
		header("Location: quote.php?msg=retagain");	
	else
	{
		$_SESSION['retention'] = $retention;
		header("Location: quote.php?email=$customer_email&move_day=$estimate_day&move_month=$estimate_month&move_year=$estimate_year");
	}

?>