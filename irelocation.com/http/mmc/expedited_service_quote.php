<?
$h1="Car Shipping Company";
$h2="expedited service quote";
$desc="If you need to move your vehicle quickly or if
need to guarantee the delivery date, then select a car
shipping company that offers an expedited service.
The average charge is typically around $200 extra,
which might include partial reimbursement and any rental car fees.";
$quote_type="expedited";
$dir="";
$title="Expedited Service Quote";
$keywords="expedited services car shipping quote";
$description="get quotes for expedited car shipping services nationwide";
include($dir."header.php");
include($dir."quote.php");
include($dir."footer.php");
?>