/*
	Name: 		row_cars_funcs.js
	Author: 	Markus Diersbock
	Details: 	Clones or deletes make/model rows, adding
				incrementing attributes.
			
	Revisions:	2008/06/03 - Created
*/
function numberRows(tID){
	var tBody = document.getElementById(tID).tBodies[0];

	return tBody.rows.length;
}

function addRow(tID){
	var tBody = document.getElementById(tID).tBodies[0];

	if (tBody.rows.length==5){
		alert("If you require more than 5 vehicles moved, please call (800)233-4875\nto speak with a Sales Agent for full-load pricing.");
	}else{
		var newNode = tBody.rows[0].cloneNode(true);
		tBody.appendChild(newNode);
		var last_row = tBody.rows.length;
		
		if(tBody.rows[last_row - 1].cells[0].childNodes[0].nodeType == 1){
			tBody.rows[last_row - 1].cells[0].childNodes[0].setAttribute("id", "make" + last_row);
			tBody.rows[last_row - 1].cells[1].childNodes[0].setAttribute("id", "model" + last_row);
			// tBody.rows[last_row - 1].cells[1].childNodes[0].length = 0;
		}else{
			tBody.rows[last_row - 1].cells[0].childNodes[1].setAttribute("id", "make" + last_row);
			tBody.rows[last_row - 1].cells[1].childNodes[1].setAttribute("id", "model" + last_row);
			// tBody.rows[last_row - 1].cells[1].childNodes[1].length = 0;
		}
	}
}

function deleteLastRow(tID){
	var tBody = document.getElementById(tID).tBodies[0];
	var i=tBody.rows.length - 1;

	if(i>0){
		tBody.deleteRow(i);
	}
}
