<?	
	define(DEGUB_TSC_EMAIL,"david@irelocation.com,mark@irelocation.com");
	define(APEX_PREFIX,"ap_field_");
	
	
	
	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{			
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}		

//Maa

	class Aaaalarms extends SecurityClass
	{

		function test()//only Manatoba and ALberta until 1/31/2008
		{			
			if($this->thearray['state_code'] == "AB" || $this->thearray['state_code'] == "MB")
			{
				return true;			
			}
			else
				return false;
		}


		function Aaaalarms()
		{
			$this->lead_id = AAAAALARMS;
			$this->email = "cindy.thompson@aaaalarms.ca";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			mail("mark@irelocation.com","Security Lead - Copy",$this->message,$this->headers);
		}
	
	}
//maa end	
	
	
	class Brinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()//only BC Leads as of 6/1/7
		{			
			if($this->thearray['state_code'] == "BC")
			{
				$own_rent = $this->thearray['own_rent'];
				$type = $this->thearray['quote_type'];
				
				if ($own_rent == "Own" || $type == "com" )
					return true;			
			}
			else
				return false;
		}
		
		/*	
			Residential
			 <option value="0">0 - 1,499 ft</option>
		  <option value="15">1,500 - 2,999 ft</option>
		  <option value="30">3,000 - 4,999 ft</option>
		  <option value="50">5,000 - 19,999 ft</option>		  
		  <option value="200">20,000+ ft</option>
		  
			Commercial
			<option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>
		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		  <option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>

		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		*/
		/*
		function sqrFootage($type,$sq)
		{

			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		*/
		function Brinks()
		{
			$this->url = "ftp.brinks.com";
			$this->lead_id = BRINKS;
		}	
			
		/* OLD */			
		/*
		function format()
		{
			$this->thearray["sqr_footage"] = str_replace(",","",$this->thearray["sqr_footage"]);
			$this->thearray = str_replace(array(",","\n","'")," ",$this->thearray);
			
			
			extract($this->thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$this->thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own")
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "Type:".(($quote_type=="com")?"Commercial":"Residential")." ";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type ";
			$newcomments .=	"Sqr. Footage: $sqr_footage ";
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
				
			
			if (strlen($comments) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			
			$this->lead_body = "$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,$phone2,$email,$newcomments,$renter,N733,".$this->thearray["quote_id"].",";			
		}
		*/
		
		/* NEW FORMAT */
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			$e21 = "";
			$e22 = "";
			$e23 = "";
			$e24 = "";
			$e25 = "";
			$e26 = "";
			$e27 = "";
			$e28 = "";
			$e29 = "";
			
			if ($this->thearray["source"] == "aff_p4p" || $this->thearray["source"] == "aff_rlc")
				$marketsource = "N751";
			else			
				$marketsource = "N733";			
			
			
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,$marketsource,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";		
			//mail("david@irelocation.com","Brinks #".$thearray["quote_id"],$this->lead_body);
		}
			
		/* END NEW FORMAT */	
			
		function send()
		{
			$result = true;
			$quote_id = $this->thearray["quote_id"];
			//Global Variables are turned off, so i have to put it to a scope where
			//Var.php can reach it. SESSION seems most reasonable..
			$_SESSION["text"] = $this->lead_body;	
							
			$login = "irelocation";
			$password = "Jun3bug!";
			$subdir = "In/";
			$ext = ".txt";			
			
			$fp = fopen('var://SESSION/text','r');
			$conn_id = ftp_connect($this->url);
				
			$login_result = ftp_login($conn_id, $login, $password);
			
			// check connection
			if ((!$conn_id) || (!$login_result)) 
			{				
				mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				$result = false;
			}
			else
			{
				// upload the file
				$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
				
				// check upload status
				if (!$upload) 
				{
					mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = false;
					recordLeadFailure($this->thearray,1519,
							"casecurity","irelocation.leads_security");
				} 
				else
				{
					mail("leads@brinks.com","iRelocation - $quote_id",$this->lead_body);
					mail("david@irelocation.com","Brinks Lead - $quote_id",$this->lead_body);					
				}	
				
				// close the FTP stream
				ftp_close($conn_id);
				fclose($fp);
				//mail(DEGUB_TSC_EMAIL,"Brinks FTP Success","FileText: $filetext<br/>QuoteId: $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
				
				$result = true;
			}
			
			$this->processResponse($result);			
		}
		
		function processResponse($response)
		{			
			if ($response)
			{
				save_response($this->thearray["quote_id"],"FTP Accepted",BRINKS,1,"ctsc");				
			}
			else
			{
				save_response($this->thearray["quote_id"],"FTP Rejected",BRINKS,0,"ctsc");
			}
		}		
		
		#------------------------------------------------------------------------------------------------------------
		#
		#			BRINKS FORMAT
		#
		#------------------------------------------------------------------------------------------------------------
			
			
		/*
			Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
			
			Data files should be in plain text, comma-delimited format with one record per line.
			The delimiter character can be changed on a case-by-case basis. Given that commas 
			are used to delimit fields, and carriage returns are used to delimit records, care 
			must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
			returns are removed from data before being included in the file.
			
			Additional fields may be added to the specification upon agreement between Brinks and 
			the partner in question. The data file may not be jagged-that is, if a field is null, 
			it must still be included in the output file as an empty string. Each line must contain
			n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.
	
			The first line of the file should NOT contain field names. Field names are already
			known as long as the aforementioned format is followed.
	
			The file should contain no blank lines, and no lines with incomplete records.
	
			
			1.	First name (20 chars max)
			2.	Last name (20 chars max)
			3.	Street Number (20 chars max)
			4.	Street Name  (30 chars max, optional)
			5.	Additional Address (30 chars max, optional)
			6.	City (30 chars max)
			7.	State (2 char abbreviation)
			8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
			9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
			10.	Secondary Phone (optional)
			11.	E-mail address (40 chars max, optional)
			12.	Comment (255 chars max-may not contain carriage returns, optional)
			13.	Renter (1 char, 1 = true 0 = false)
	
			Sample File: brinks/sample.txt
			
		*/				
	}
	
	class BellHome extends TextEmail
	{	
		function BellHome()
		{
			$this->lead_id = BELLHOME;
			parent::TextEmail("securityleads@bellhomemonitoring.ca");
		}					
		
		function test() 
		{  					
			return true;
		}
		
		
		function send()
		{
			/*
			$this->format();
			if ($this->thearray['source'] == "aff_p4p"  || $this->thearray["source"] == "aff_rlc")							
				$subject = "OB - Security Lead";		
			else					
				$subject = "Security Lead";
				
		
			mail($this->,$subject,$this->message,$this->headers);
			*/
			//	mail("david@irelocation.com","Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
	}
	
	class FakeCompany extends TextEmail
	{
	
		function FakeCompany()
		{
			$this->lead_id = -1;
			parent::TextEmail(DEGUB_TSC_EMAIL);
		}	
		
		function send()
		{
			//parent::send();
			$this->formatHTML();
			
			mail(DEGUB_TSC_EMAIL,"Top Security Lead - HTML",
				 $this->HTMLmessage,$this->HTMLheaders);		
		}
		
		function test() { return true; }
	}
	
	class Voxcom extends TextEmail//1555
	{
		function Voxcom()
		{
			$this->lead_id = VOXCOM;
			parent::TextEmail("customer.support@voxcom.com,michael.starrett@voxcom.com");
		}	
		
		
		function send()
		{			
			$this->format();
			if ($this->thearray['source'] == "aff_p4p"  || $this->thearray["source"] == "aff_rlc")
			{
				$this->message = str_replace("Source: irelocation.com",
											 "Source: irelo - call center",
											 $this->message);
			}	
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);			
		}
		
		function test() { return true; }
	}
	
	class SafeTech extends TextEmail//1556
	{
		function SafeTech()
		{
			$this->lead_id = SAFETECH;
			parent::TextEmail("sean@safetechalarms.com,maria@safetechalarms.com");
		}	
		
		function test()//only 236K postal codes
		{
			$postal_code = $this->thearray['zip'];
			

			$sql = "select state from movingdirectory.safetech_areas where ".
					" postalcode = '$postal_code' and state != 'BC' limit 1   ";
			$rs = new mysql_recordset($sql);
			
			$safetech_zip = ($rs->fetch_array());
		
			$now_az = gmdate("Ymdhi",time()+5*60*60);	
			$midnight_az = '2007120100';
			if ($now_az >= $midnight_az)
				$province = ($this->thearray['state_code'] != "AB");			
			else
				$province = true;
			
			return ($safetech_zip && $province );
		}
		
		function send()
		{
			$this->format();
			if ($this->thearray['quote_type'] == "res")							
				$subject = "Residential ";		
			else					
				$subject = "Commercial ";
				
		
			mail($this->emailaddress,$subject."Security Lead",$this->message,$this->headers);
			//mail("david@irelocation.com",$subject."Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
			$this->autoResponder();
		}
		
		function autoResponder()
		{
			$text = "We have received your request for information; one of our advisors will contact you shortly.
	
We appreciate your business.

Regards

Safetech 
	
	";	
			$headers = "Content-Type: text/plain; \n".
					"From: \"Top Alarm Companies\" <autoresponder@irelocation.com>";
					
			//mail("david@irelocation.com","Safetech Alarms",$text,$headers);
			mail($this->myarray['email'],"Safetech Alarms",$text,$headers);
		}
	}
	
	class ApexDirect extends TextEmail//1557
	{
		var $data;
		
		//Shuts off leads at 5pm 10/31.

		
		function ApexDirect()
		{
			$this->lead_id = APEXDIRECT;
			$this->url = "https://www.aimpromote.com/remote/form_post.php";
			parent::TextEmail("leads@apexdirect.ca");
			$this->data = array
			(		
				"ss_id" => 2321,
				"action" => "feedback_post_add",
				"ostat_id" => 1,
				"org_id" => 334,
				"c_id" => 662
				//"ap_field_11697" => "123456"
			);
		}	
		
		function test() 
		{
			/*
			$own_rent = $this->thearray['own_rent'];
			$type = $this->thearray['quote_type'];
			echo "own_rent = ".$own_rent."<br/>";
			echo "type = ".$type."<br/>";
			
			$callcenter = ($this->thearray['source'] == "aff_rlc");
			
			if ($own_rent == "Own" || $type == "com" && !$callcenter)
				return true;
			*/
			return false;
		}
		
		function format()
		{
			parent::format();
		
			$this->data[APEX_PREFIX."11697"] = $this->thearray['quote_id'];
			$this->data[APEX_PREFIX."11698"] = $this->thearray['received'];
			$this->data[APEX_PREFIX."11699"] = $this->thearray['quote_type'];
			$this->data[APEX_PREFIX."11700"] = $this->thearray['name1'];
			$this->data[APEX_PREFIX."11701"] = $this->thearray['name2'];
			$this->data[APEX_PREFIX."11702"] = $this->thearray['phone1'];
			$this->data[APEX_PREFIX."11703"] = $this->thearray['phone2'];
			//$this->data[APEX_PREFIX."11772"] = $this->thearray['email'];
			$this->data[APEX_PREFIX."11394"] = $this->thearray['email'];
			$this->data[APEX_PREFIX."11704"] = $this->thearray['address'];
			$this->data[APEX_PREFIX."11774"] = $this->thearray['city'];
			$this->data[APEX_PREFIX."11705"] = $this->thearray['state_code'];
			$this->data[APEX_PREFIX."11706"] = $this->thearray['zip'];
			$this->data[APEX_PREFIX."11707"] = $this->thearray['current_needs'];
			$this->data[APEX_PREFIX."11708"] = $this->thearray['building_type'];
			$this->data[APEX_PREFIX."11709"] = $this->thearray['num_location'];
			$this->data[APEX_PREFIX."11769"] = $this->thearray['sqr_footage'];
			$this->data[APEX_PREFIX."11710"] = $this->thearray['own_rent'];
			$this->data[APEX_PREFIX."11711"] = $this->thearray['fire'];
			$this->data[APEX_PREFIX."11739"] = $this->thearray['basement'];
			$this->data[APEX_PREFIX."11740"] = $this->thearray['prewired'];
			$this->data[APEX_PREFIX."11741"] = $this->thearray['access'];
			$this->data[APEX_PREFIX."11771"] = $this->thearray['cctv'];
			$this->data[APEX_PREFIX."11773"] = $this->thearray['other_field'];
			$this->data[APEX_PREFIX."11770"] = $this->thearray['comments'];	
		}
		
		function send()
		{
			$callcenter = ($this->thearray['source'] == "aff_rlc");
			$prefix = ($callcenter)?"Call Center - ":"";
			
			//parent::send(); <- send the text email to leads@apexdirect.ca
			
			$this->format();
			mail($this->emailaddress,$prefix."Security Lead",$this->message,$this->headers);
			
			$this->formatHTML();
			
			mail("leadsapexdirect@gmail.com",$prefix."Top Security Lead",
					$this->HTMLmessage,$this->HTMLheaders);
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);					
				
			$this->processResponse($response);	
			
			return $response;
		}
		
		function processResponse($response)
		{			
			//mail("david@irelocation.com","Apex Response Code",$response);
			if (is_numeric($response))
			{
				save_response($this->thearray["quote_id"],$response,
								APEXDIRECT,1,"ctsc");	
				echo "Apex Lead Accepted<br/>";			
			}
			else
			{
				//mail("david@irelocation.com","Apex Lead Rejected:",$response."\nQuote ID: ".$this->thearray['quote_id']);
				save_response($this->thearray["quote_id"],"Lead Rejected",
								APEXDIRECT,0,"ctsc");
				echo "Apex Lead Rejected<br/>";	
			}
		}		
	}

	class TextEmail extends SecurityClass
	{
		function TextEmail($email)
		{
			$this->emailaddress = $email;//;
		}
		
		function sqrFootage($type,$sq)
		{
			echo "sqrFootage($type,$sq)<br/>";
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return 2500;
				else if ($type == "com" && $sq == 50) return 20000;
				else if ($type == "res" && $sq == 0) return 1500;
				else if ($type == "res" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		
		function send()
		{
			$this->format();
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			//mail("david@irelocation.com","Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function formatHTML()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			echo "Extras: F: $fire A: $access C: $cctv P: $prewired B: $basement <br/>";
			
			echo "Fire:-$fire- <br/>";
			
			$extra_services = array();
			if ($fire == "1")
				$extra_services[] = "fire";
			if ($access  == "1")
				$extra_services[] = "access";
			if ($cctv  == "1")
				$extra_services[] = "cctv";
			if ($prewired  == "1")
				$extra_services[] = "house is prewired";
			if ($basement  == "1")
				$extra_services[] = "basement is finished";		
			
			if (count($extra_services) > 0)
				$extras = implode(",",$extra_services);
			echo "Extras: $extras <br/>";
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->HTMLheaders = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/html; charset=iso-8859-15";
						
			$this->HTMLmessage = 
			"<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
			"<tr><td colspan='2'><strong>Top Security Lead</strong></td></tr>".
			"<tr><td colspan='2'><hr width='250' align='left'/></td></tr>".
			"<tr><td width='200'>Quote ID: </td><td>".$quote_id."</td></tr>".
			"<tr><td width='200'>Received: </td><td>".$received."</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name."</td></tr>".
			"<tr><td width='200'>E-Mail Address: </td><td>".$email."</td></tr>".
			"<tr><td width='200'>Phone Number: </td><td>".$phone1."</td></tr>".
			"<tr><td width='200'>Alternate Phone: </td><td>".$phone2."</td></tr>".
			"<tr><td width='200'>Street Address: </td><td>".$address."</td></tr>".
			"<tr><td width='200'>City: </td><td>".$city.", ".$state_code."</td></tr>".
			"<tr><td width='200'>Postal Code: </td><td>".$zip."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Quote Type: </td><td>".$type."</td></tr>".
			"<tr><td width='200'>Building Type </td><td>".$building_type."</td></tr>".
			"<tr><td width='200'>Square Footage: </td><td>".$sqr_footage."</td></tr>".
			"<tr><td width='200'>Owners: </td><td>".$own_rent."</td></tr>".
			"<tr><td width='200'>Requested Services/Features: </td><td>".$extras."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td colspan='2' align='left'>Comments:</td></tr>".
			"<tr><td colspan='2' align='left'>".$comments."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Lead Source: </td><td>iRelocation</td></tr>".
			"</table>";	
		
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
	}
	
	class AlarmTrax extends SecurityClass
	{
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 83;
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}
			/*
			else if ($this->lead_id == PINNACLE)
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";		
			}
			*/


			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = array();
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: ".$data['building_type'];

			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
			
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<"."?xml version='1.0' standalone='yes'?".">
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<lead_type_id>".$this->lead_type_id."</lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".xmlentities($data['name1'])."</fname>
					<lname>".xmlentities($data['name2'])."</lname>
					<address1>".xmlentities($data['address'])."</address1>
					<address2></address2>
					<city>".xmlentities($data['city'])."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".xmlentities($data['comments'])."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".xmlentities($extra_text)."</lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			mail("davidhaveman@gmail.com","CA AlarmTrax XML",$xml);
			return $xml;
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			//mail("david@irelocation.com","AlarmTrax XML",$xml);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			//mail("david@irelocation.com","AlarmTrax Content",$xmlcontent);
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>
			$rawresponse = $response;
			$response = substr($response,strpos($response,"<body>")+6);
			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
			if ($success == 0)
			{
				//new format?
				if (substr_count($response,"success=\"true\"") > 0)
				{
					$success = 1;
					$start = strpos($response,"<customer_id>")+13;
					$end = strpos($response,"</",$start);
					if ($end > $start && $start != -1)
						$responseId = substr($response,$start,$end);
				
					else
					{
						$success = 0;
						$responseId = "Success?";
						mail("david@irelocation.com","AlarmTrax Failure? - ".$this->lead_id,
						"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
						"\nLeadXML:".$this->xml);
						recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
					}
					
					save_response($quote_id,$responseId,$this->lead_id,$success,"tsc");	
				}
				else
				{
					$success = 0;
					$responseId = "failure";
					mail("david@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
					"\nLeadXML:".$this->xml);
					recordLeadFailure($this->thearray,
						$this->lead_id,
						"security",
						"irelocation.leads_security");
					save_response($quote_id,$responseId,$this->lead_id,$success,"ctsc");	
				}
			}
			else
			{	
				save_response($quote_id,$result,$this->lead_id,$success,"ctsc");				
			}
			
			if ($success == 0)
			{
				mail("david@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					"Lead #$quote_id Failed to Be Accepted: \nResult:$response\n".
					"LeadXML:".$this->xml);
				recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
			}
			
			
		}		
	}

	class Gaylord extends AlarmTrax
	{
		function Gaylord()
		{
			parent::AlarmTrax(GAYLORD);
			$this->lead_type_id = "3";
		}	
		
		function test()
		{
			return ($this->thearray['source'] != "aff_p4p");
		}
	}
?>