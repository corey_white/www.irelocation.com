<?		
/* 
************************FILE INFORMATION**********************
* File Name:  re_quotemailer.php
**********************************************************************
* Description:  resends ATUS leads
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/28/08 14:19 PM - Rob - changed Dave's email to Rob
**********************************************************************
*/


	define(CAMPAIGN,"atus");
	include_once "../marble/CompanyClasses.php";
	define(DATABASE,"marble");
	define(DEBUG,true);
	define(LIVE,false);		
	


	function getCompSql($lead_id)
	{
		//$email =  "'code@irelocation.com'";
		
		$email = "concat('code@irelocation.com,',(select lead_email from marble.rules ".
				" where site_id = 'tas' and lead_id = lf.lead_id))";
		
		//$email = "(select lead_email from rules where site_id = 'auto' and lead_id = lf.lead_id)";
			
		return "select format_id, $email as ".
				" 'email', lf.lead_id, format_type, lf.cat_id, ".
				" if(lead_body < 0, ( select lead_body from marble.lead_format where ".
				" lead_id = lf.lead_body and site_id = '".CAMPAIGN."' ), lf.lead_body ) ".
				" as 'lead_body' from marble.lead_format as lf where ".
				" lf.cat_id = 1 and lf.site_id = '".CAMPAIGN."' and lf.lead_id in ".
				" ($lead_id) order by rand();";
	}
	
	function reSendLeads($lead_sql,$comp_sql)
	{
		$rs = new mysql_recordset($lead_sql);

		if (is_numeric($comp_sql))
			$sql =	getCompSql($comp_sql);
		else
			$sql =	$comp_sql;
		//echo $sql;
		$rs2 = new mysql_recordset($sql);//grab this once.
		$rs2->fetch_array();
		
		while($rs->fetch_array())//loop over each lead.
		{
			$asc = new AutoShippingQuote($rs->myarray);
			//echo $rs->myarray["quote_id"].  " is being processed...<br/>\n";
			$asc->process($rs2->myarray);
		}
	}
	
	
	
	//for sending test leads out.
	if (strlen($_POST['prospect_email']) > 0 && strlen($_POST['lead_format']) > 0)
	{
		$comp_sql = "select format_id, 'code@irelocation.com,".$_POST['prospect_email']."' as ".
				"'email', lf.lead_id, format_type, lf.cat_id, ".
				" if(lead_body < 0, ( select lead_body from marble.lead_format where ".
				" lead_id = lf.lead_body and site_id = 'auto' ), lf.lead_body ) as 'lead_body' ".
				" from marble.lead_format as lf where lf.cat_id = 1 and lf.site_id = 'auto' and ".
				" lf.lead_id in (".$_POST['lead_format'].") order by rand();";
	
		$lead_sql = "select * from marble.auto_quotes where quote_id = 23793;";
		//Mike Nelson lead still exists.
		
		$rs = new mysql_recordset($lead_sql);
		if ($rs->fetch_array())
		{		
			reSendLeads($lead_sql,$comp_sql);
			header("Location: ".$_POST['return']);
			exit();
		}
		else
		{
			echo "Test Lead 'Mike Nelson' not found.<br/>";
			exit();
		}
	}
	else
	{
		if (strlen($_POST['quote_ids']) > 0 && $_POST['lead_id'] > 0)
		{
			$lead_sql = "select * from marble.auto_quotes where quote_id in (".$_POST['quote_ids'].")";
			reSendLeads($lead_sql,$_POST['lead_id']);
			header("Location: ".$_POST['return']);
			exit();
		}
		else
		{
			if ($_REQUEST['lead_id'] != "" && $_REQUEST['quote_id']!="")
			{
				$lead_id = $_REQUEST['lead_id'];
				$quote_id = $_REQUEST['quote_id'];
			}
			else
			{
				echo "Usage: re_quotemailer.php?quote_id=".
						"<strong>[quote_id<,quote_id>]</strong>".
						"&lead_id=<strong>[lead_id]</strong><br/>";
				echo "Where:<br/><ul><li><strong>quote_id</strong>: ".
						"comma seperated list of quote_ids.</li>";
				echo "<li><strong>lead_id</strong>: ".
						"4-5 digit numeric id for company</li>";
				echo "</li><br/>exiting...";
				exit();
			
			}
			
			echo "Sending lead...";
			//$lead_id = 1553;
			$lead_sql = "select * from marble.auto_quotes where campaign = '".CAMPAIGN."' ".
						" and quote_id in ($quote_id)";
			
			echo "<br />lead_sql = $lead_sql<br />";
						
			$sql = "select format_id, ".
					//" 'code@irelocation.com' as ".
					" concat('code@irelocation.com,',r.lead_email) as ".
					" 'email', lf.lead_id, format_type, lf.cat_id, ".
					" if(lead_body < 0, ( select lead_body from marble.lead_format where ".
					" lead_id = lf.lead_body and site_id = '".CAMPAIGN."' ), lf.lead_body ) ".
					" as 'lead_body' from marble.lead_format as lf join marble.rules as r 
					on r.lead_id = lf.lead_id and r.site_id = lf.site_id where lf.cat_id = 1 and ".
					" lf.site_id = '".CAMPAIGN."' and lf.lead_id in (".$lead_id.")";
			
			echo "<br />";
			echo $sql."<br/>";
			reSendLeads($lead_sql,$sql);
		}
	}
	
?>