<?
	define(CAMPAIGN,"atus");
	if (!isset($_REQUEST['maxsend']))
		define(MAX_SEND,10);
	else
		define(MAX_SEND,$_REQUEST['maxsend']);
	define(SMALLEST_INCREMENT,500);
	include "../inc_mysql.php";
	define(DEBUG,false);

	
	function getCompanyArray()
	{
		$sql = "select ca.comp_id, co.comp_name, ca.monthly_goal from marble.campaign as ca join marble.companies as ".
				" co on co.comp_id = ca.comp_id where site_id ='".CAMPAIGN."' and ca.active and ca.month = '".date("Ym")."' ".
				" order by monthly_goal desc,comp_name";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$companies = array();
		while ($rs->fetch_array())
		{
			extract($rs->myarray);
			$companies[] = array($comp_name,$monthly_goal,$comp_id);
		}
		return $companies;
	}
	
	function getTotalLeads()
	{
		$sql = "select sum(monthly_goal) 'need' from marble.campaign where ".
				" site_id ='".CAMPAIGN."' and active and active and ".
				" month = '".date("Ym")."' ";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
			return $rs->myarray['need'];
	}
	
	function meltingPot($clc)
	{		
		$total_leads = getTotalLeads();				
		
		if (($total_leads%SMALLEST_INCREMENT) != 0)
			$kilo_leads = intval(($total_leads/SMALLEST_INCREMENT)+2);
		else
			$kilo_leads = intval($total_leads/SMALLEST_INCREMENT);
			
		if (DEBUG) echo $kilo_leads."<br>\n";
		
		$lead_groups = array();
		for($i = 0; ($i < $kilo_leads); $i++)
			$lead_groups[$i] = array();
		$count = 0;
		
		foreach($clc as $c)
		{				
			$weight = $c[1]/SMALLEST_INCREMENT;
			
			while($weight > 0)
			{
				for($index = 0; ($index < count($lead_groups) && ($weight > 0)); $index++)
				{
					if (count($lead_groups[$index]) < MAX_SEND && 
						substr_count(implode(" ",$lead_groups[$index]),$c[0]) == 0)
					{
						$lead_groups[$index][] = $c[0];
						$weight--;
					}
					else
						continue;
				}	
				if (DEBUG) echo $weight."<br>\n";
			}
			$count++;
		}
		
		$count = 0;
		foreach($lead_groups as $row)
		{
			if (count($row) >0)
			{
				echo "<table><tr><td>";
				echo "<Table border=1 width='500'>\n";
				echo "<tr height='50'>\n";
				//sort($row);
				
				foreach($row as $entry)
				{
					echo "<td width='30' align='center'>$entry</td>\n";			
				}		
				echo "</tr>\n";
				echo "</table></td><td>(".SMALLEST_INCREMENT." leads)</td></tr></table>\n";
				echo "<Br/>";
				$count++;
				if ($count % 2 == 0) echo "<hr width='90%' align='left'>";
			}
		}		
		echo "<a name='needed'/>";

		echo "<strong>".($count * SMALLEST_INCREMENT)."</strong> Leads Needed Per Month.";
	}
	
	if (isset($_REQUEST['show']) || isset($_REQUEST['maxsend']))
		meltingPot(getCompanyArray());
	
?>