<?php
/* 
************************FILE INFORMATION**********************
* File Name:  linkshare_bounce.php
**********************************************************************
* Description:  the intermediate page for incoming links from LinkShare
**********************************************************************
* Creation Date:  11/19/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES:
Purpose of this script is to:

- validate the referring URL so that it's properly formed
---- siteID
---- URL

- set cookie with siteID and current timestamp in GMT

- redirect to the URL passed

*/

include_once("inc_mysql.php");

extract($_GET);

if ( !$siteID ) { //-- if no siteID, then what???
    $siteid_msg = "no siteID";
    #$redir = "http://www.irelocation.com";
    //-- WHAT DO WE DO HERE?????
} else {
	$siteid_msg = "siteID = $siteID";
}

if ( !$url ) { //-- if no url is given, go to home page
    $usr_msg = "no url";
	$redir = "http://www.irelocation.com";
} else {
	$redir = $url;
	$usr_msg = "redir = $url";
}


//-- Set Cookie
#$now_time = date("Y-m-d G:i:s T");
#echo "now_time = $now_time<br />";

$gmt_time = gmdate("Y-m-d G:i:s T");

#$cookie_exp = 63072000 + time(); //-- 2 years

//-- set siteID cookie
setcookie("siteID", $siteID, time()+63072000);

//-- set date stamp cookie
setcookie("GMT", $gmt_time, time()+63072000);


echo "redir = $redir<br />";
echo "gmt_time = $gmt_time<br />";
echo "$siteid_msg<br />";
echo "$usr_msg<br />";
echo "Cookie has been set<br />";
?>
