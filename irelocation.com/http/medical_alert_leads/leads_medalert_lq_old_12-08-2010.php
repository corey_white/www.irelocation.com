<?
/* 
************************FILE INFORMATION**********************
* File Name:  leads_medalert_lq.php
**********************************************************************
* Description:  Takes leads coming in from LeadQual and inserts it into the Local Db and LeadExec
**********************************************************************
* Creation Date:  11/12/10
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

Example URL with fields:

http://irelocation.com/medical_alert_leads/leads_medalert_lq.php?source=tmac&name1=IRELOTESTLEAD&name2=DingleHopper&phone1=4805801331&phone2=&email=johndingleTEST1@gmail.com&address=123%20TEST%20LEAD&city=SCOTTSDALE&state_code=AZ&zip=85256&system_self=Self&campaign=medalert&remote_ip=74.1.253.122


*/

	include "medical_quotes.php";

	if ($_REQUEST['method'] == 'post') {
		extract($_POST);
	} else {
		extract($_REQUEST);
	}
	
	
	$msg = "";
	if (strlen($name1) == 0) {
		$msg .= "<strong>fname</strong>: A First Name is Required<Br/>\n";
	}
	
	if (strlen($name2) == 0) {
		$msg .= "<strong>lname</strong>: A Last Name is Required<Br/>\n";
	}
	
	if (strlen($email) == 0) {
		$msg .= "<strong>email</strong>: An email address is Required<Br/>\n";
	} else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
		$msg .= "<strong>email</strong>: A valid email address is Required<Br/>\n";
	}
	
	if (strlen($phone1)  != 10) {
		$msg .= "<strong>phone</strong>: A Phone Number is Required: 10 digits, only numbers<Br/>\n";
	} else {
		//Phone Validation.	
		$p_area   = substr($phone1,0,3);
		$p_prefix = substr($phone1,3,3);	
		
		if ($p_area != 800 && $p_area != 866 && $p_area != 877 && $p_area != 888) {
			$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US' limit 1;";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array()) {
				$msg = "invalid phone: Please provide a valid phone number.<Br/>\n";	
			}
		}
	}
	
	if (strlen($zip) == 5) {
	
		$sql = "select city,state from movingdirectory.zip_codes where zip = '$zip' and length(city) > 3 limit 1";
		$rs = new mysql_recordset($sql);
		
		if ($rs->fetch_array()) {
		
			$city = $rs->myarray['city'];
			$state = $rs->myarray['state'];
			
		} else {
		
			$sql = "select city,state from movingdirectory.zip_codes where zip = '$zip' limit 1";
			$rs = new mysql_recordset($sql);
			
			if ($rs->fetch_array()) {
			
				$city = $rs->myarray['city'];
				$state_code = $rs->myarray['state'];
			} else {
				$msg .= "<strong>zip</strong>: A Zip Code or a valid City-State combination is required.<Br/>\n";
			}
		}
	} else {
	
		if (strlen($city) == 0) {
			$msg .= "<strong>city</strong>: A City is Required<Br/>\n";
		}
		
		if (strlen($state_code) != 2) {
			$msg .= "<strong>state_code</strong>: A State is Required: 2 letter code<Br/>\n";
		}

		if (strlen($state_code) == 2 && strlen($city) > 2) {
			$sql = "select zip from movingdirectory.zip_codes where city like '$city%' and state = '$state_code' limit 1";
			$rs = new mysql_recordset($sql);
			
			if ($rs->fetch_array()) {
				$zip = $rs->myarray['zip'];
			} else {
				$msg .= "<strong>zip</strong>: A Zip Code or a valid City-State combination is required.<Br/>\n";			
			}
		} else {
			$msg .= "<strong>zip</strong>: A Zip Code or a valid City-State combination is required.<Br/>\n";			
		}

	}



	
	if (strlen($source) == 0) {
		$msg .= "<strong>source</strong>: Your source code is required.<Br/>\n";
	}

	if (strlen($system_self) == 0) {
		$msg .= "<strong>system_self</strong>: system_self is required.<Br/>\n";
	}

	if (strlen($campaign) == 0) {
		$msg .= "<strong>campaign</strong>: A campaign is required.<Br/>\n";
	}

	if (strlen($remote_ip) == 0) {
		$msg .= "<strong>remote_ip</strong>: A remote_ip is required.<Br/>\n";
	}



	if (strlen($msg) == 0) {
	
		$ten_days_ago = date("YmdHis",(time() - (10*24*60*60)));
		
		$check = "select * from irelocation.leads_medicalalert where ".
		" (phone1 = '$phone1' or email = '$email') and ".
		" (received = '' or received > '$time' or received is null) ".
		" and zip = '$zip' and city = '$city' limit 1";	
	
		$rs = new mysql_recordset($check);
	
		if ($rs->fetch_array()) {
			$msg = "<strong>Duplicate</strong>";
		}
	}
	
	if (strlen($msg) > 0) {
		fail($msg);
	}
	

	$received = date("YmdHis");
		
	$quote = new quote();		

	$quote->source = $source;   
	$quote->campaign = $campaign;   
	$quote->referrer = $referer; 	
	$quote->received = $received;
	$quote->name1 = $name1;        
	$quote->name2 = $name2;        
	$quote->phone1 = $phone1;       
	$quote->phone2 = $phone2;       
	$quote->email = $email; 
	$quote->system_self = $system_self; 
	$quote->address = $address;      
	$quote->city = $city;         
	$quote->state_code = $state_code;   
	$quote->zip = $zip;          	
	$quote->scrub_notes = $scrub_notes;   
	$quote->scrub_status = $scrub_status;   

	$quote->scrub();
	$quote_id = $quote->save_quote();

	//-- Code to send to LeadExec 
	include "lead_exec_code_medalert.php";

	$_SESSION["quote_id"] = $quote_id;
	$_SESSION["quote_submitted"] == "true";

		
	$pre = "MAQ";
	echo "Lead Accepted: ".$pre.$last_insert_id;

	
	function fail($msg) { 
		echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
		exit();
	}
?>
