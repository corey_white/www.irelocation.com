<?php
#include_once "mysql.php";

//-- Code to send to LeadExec that gets put into the submit page AFTER the insertion into our Db, so we have the quote_id to pass into LE
# include "lead_exec_code_medalert.php";


//-- Clean up phone2
if ( !is_numeric($phone2) || strlen($phone2) != 10 ) {
	$phone2 = "";
}

//-- This code will hack apart the source code to get just the source root
if ( preg_match("/clkbth/i", $source) ) {
   $source = str_replace("clkbth","clkbth_",$source);
}

$source = str_replace("aff_","",$source);
$feh = explode("_",$source);
$source_root = $feh[0];

//-- set the marketing type
if ( $source_root == "clkbth" ) {
    $marketing_type = "email";
} else {
    $marketing_type = "cpc";
}


$CAMPAIGN = "medalert";			//-- which campaign these leads are for
$POST_URL = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver";			//-- Posting URL
$SITE = "topmedicalalertcompanies.com";

$LEAD_BODY =
"VID=1604&".
"AID=2909&".
"LID=552&".
"quote_id=$quote_id&".
"received=".urlencode($received)."&".
"source=".urlencode($source)."&".
"referrer=".urlencode($referer)."&".
"name1=".urlencode($name1)."&".
"name2=".urlencode($name2)."&".
"phone1=".urlencode($phone1)."&".
"phone2=".urlencode($phone2)."&".
"email=".urlencode($email)."&".
"unit_owner=".urlencode($system_self)."&".
"address=".urlencode($address)."&".
"city=".urlencode($city)."&".
"state_code=".urlencode($state_code)."&".
"zip=".urlencode($zip)."&".
"campaign=".urlencode($CAMPAIGN)."&".
"marketing_type=".urlencode($marketing_type)."&".
"scrub_notes=".urlencode($scrub_notes)."&".
"source_root=".urlencode($source_root)."&".
"remote_ip=".urlencode($remote_ip);


function leadexec_send($QUOTE_ID,$CAMPAIGN,$SITE,$POST_URL,$LEAD_BODY) {
	global $source;
	global $dup_lead_ck;

	$ch=curl_init($POST_URL."?".$LEAD_BODY);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);

	$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
	#echo "<br />-------------------------<br />LeadExec Repsonse <br />Url: ".$POST_URL."?".$LEAD_BODY." <br/>-------------------------<br/>Response: $response<br/>-------------------------<br />";

	if (substr_count(strtolower($response),"duplicate") == 1) {
			$dup_lead_ck = 1;
	}

	if (substr_count(strtolower($response),"lead was accepted") == 1) {

		mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN - $SITE","$content","FROM: noreply@topmedicalalertcompanies.com");

	} else {

		if ( $source != "alertbot" ) {
			mail("rob@irelocation.com","LeadExec Repsonse Failure - $CAMPAIGN - $SITE","$content \n\n$response\n\nsource=$source","FROM: noreply@topmedicalalertcompanies.com");
		}


	}
}


leadexec_send($QUOTE_ID,$CAMPAIGN,$SITE,$POST_URL,$LEAD_BODY);
 ?>
