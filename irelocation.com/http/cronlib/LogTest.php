<?php
/*
    Set LOG4PHP_* constants here 
*/

require_once(LOG4PHP_DIR . '/LoggerManager.php');

/*
    In a class
*/
class Log4phpTest {
    /*
        Your public and private vars
    */
    var $_logger;
    
    function Log4phpTest()
    {
        $this->_logger =& LoggerManager::getLogger('Log4phpTest');
        $this->_logger->debug('Hello!');
    }

}

function Log4phpTestFunction()
{
    $logger =& LoggerManager::getLogger('Log4phpTestFunction');
    $logger->debug('Hello again!');    
}

/*
    Your PHP code
*/

//Safely close all appenders with...

LoggerManager::shutdown();

?>