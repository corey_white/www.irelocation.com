<?php
	define(PRIMARY_CELL,"code@irelocation.com");
	define(SECONDARY_CELL,"6024357508@vtext.com");

	include_once("../inc_mysql.php");
	#------------------------------------------------------------------------
	#
	#		Update Lead ids after the lead has been sent out to all peoples.
	#
	#------------------------------------------------------------------------
	
	//use this function if moving_staffer value > 1, AKA is a Moving Labor 
	//Service Lead. still can distinguish between movingstaffers	
	//this is only needed for an affiliate of the self moving quotes. not used anymore.
	function getMovingLaborType($code)
	{
		$codes = array("No Labor","Moving Staffers","Load/Unload Long Distance","Load/Unload Local","Corporate Relo"
			,"Packing/Loading","Loading Only","Unload Only");
		return $codes[$code];
	}
	
	//grab all leads that need to be processed, mark them.
	function updateAndSelectLeads($campaign)
	{
		//$num_sent = array("atus" => 10, "auto" => 7, "tas" => 5);
		
		define(TIMEZONE_OFFSET,18000);
		$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);
		
		$update = "update marble.auto_quotes set processed = '$now' where ".
  					" campaign = '$campaign' and (processed = 0 or processed is null) ".
					" and email not like '%randomkeystrokes@gmail%' ";
					
		$rs = new mysql_recordset($update);
		$rs->close();		
		
		$sql = "select * from  marble.auto_quotes where processed = '$now' ".
				" and campaign = '$campaign' and email not like '%randomkeystrokes@gmail%' ";
		echo $sql."<br/>\n";	
		return $sql;
	}
	
	//set the lead ids of the lead.
	function updateLeadIds($ids, $quote_id, $table)
	{		
		$sql="update $table set lead_ids='".$ids."', ready_to_send = '2' where quote_id = $quote_id";
		echo $sql."<Br/>\n";
		$rs=new mysql_recordset($sql);
	}	
	
	//save the response in this table. mainy used for security leads.
	function save_response($quote_id, $result, $lead_id,$sucess,$site)
	{
		//echo "Saving Response from Customer.<br/>";
		$sql = "insert into irelocation.leads_verified (quote_id,result,lead_id,success,site) values ($quote_id,'$result',$lead_id,$sucess,'$site');";
		
		echo "==============================<br />";
		echo "SAVE RESPONSE SQL:<br />";
		echo "$sql<br />";
		echo "==============================<br />";
		
		$rs = new mysql_recordset($sql);	
		//$rs->close();
	}		
			
	
	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "$p_area-$p_prefix-$p_suffix";
		}
		else
			return $phone;
	}
	
	/* 
		output a nicely formated date string from our 
		wonderful 14 varchar.
	*/
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		if ($r_hour > 11)
		{
			$pm = true;
			if ($r_hour > 12)
				$r_hour -= 12;
		}
		$msg = "$r_month/$r_day/$r_year $r_hour:$r_min";
		if ($pm)
			$msg .= " PM";
		else
			$msg .= " AM";
		return $msg;
	}	
	
	// XML Entity Mandatory Escape Characters
	function xmlentities ( $string )
	{
	   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
	}	
	
	/*
		update telling DB has started or finished...
	*/
	function updateCronStatus($cron, $which="started")
	{
		if ($which == "started" || $which == "ended")
		{
			$now = date("YmdHis", time());
			$sql = "update movingdirectory.cron_status set $which = '$now' where cron = '$cron';";			
			//echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
		}
	}
	
	
	/*
		make sure the integrity code has run recently.
	*/
	function checkSiteCheckStatus()
	{			
		$sixtyfive = date("YmdHis", time() - (60*80));//65 minute time frame.
		$sql = "select * from movingdirectory.cron_status where cron = 'sitecheck';";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			extract($rs->myarray);
			if ($ended < $started)
			{
/*
				mail(PRIMARY_CELL.",6024357508@vtext.com",
						"SiteCheck Error",
						"sitecheck didn't finish",
						"From: 'Errors' <sitecheck@notfinish.com>");
*/
			}
			else if ($started < $sixtyfive)
			{
/*
				mail(PRIMARY_CELL.",6024357508@vtext.com",
						"SiteCheck Error",
						"sitecheck didn't start",
						"From: 'Errors' <sitecheck@notstart.com>");
*/
			}
		}
	}	
	
	/*
		make sure [cron] has run, if not send a message
		out telling them that [watchdog] caught [cron] 
		napping on the job..
	*/
	function checkCronStatus($cron,$watchdog)
	{
		
		$minus_twenty = date("YmdHis", time() - (60*20));
		$minus_fifty = date("YmdHis", time() - (60*50));
		
		$sql = "select * from movingdirectory.cron_status where cron = '$cron';";
		//echo $sql."<br/>";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			extract($rs->myarray);
			
			if ($ended < $started && $started < ($now - 25))
			{
				//didnt finish, and didnt just start this split second. 2nd: HIGHLY unlikely.
				//sendError($cron." didn't finish");
				
/*
				mail(PRIMARY_CELL,
						"$watchdog caught CRON Error",
						$cron." didn't finish",
						"From: 'Errors' <e@irelo.com>");
*/
			}
			else if ($started < $minus_twenty)
			{
				sendError($watchdog." - ".$cron." didn't start",PRIMARY_CELL);
			}
			else if ($started < $minus_fifty)
			{
				sendError($watchdog." - ".$cron." didn't start!!","6024357508@vtext.com,".PRIMARY_CELL);
			}
		}
	}

	function checkMoveCompCronStatus()
	{
		//runs every 15 minutes.			
		$twenty = date("YmdHis",(time()-2*60*60 - 20*60));
		$fifty = date("YmdHis",(time()-2*60*60 - 55*60));
		
		$now = date("YmdHis",(time()-2*60*60));
		
		$sql = "select * from movecomp.movecomp_variables where name like 'irelo_cron%';";
		//echo $sql."<br/>";
		$crons = array();
		$rs = new movecompanion($sql);
		while ($rs->fetch_array())
			$crons[$rs->myarray['name']] = $rs->myarray['value'];
		$rs->close();
		
		$end = $crons['irelo_cron_end'];
		$start = $crons['irelo_cron_start'];
		
		//didnt finish and hasn't run in last 20 seconds.
		if ($end < $start && $start < $twenty)				
		{
			echo $end." ".$twenty."<br/>";
			echo "didnt finish!<Br/>";
/*
			mail(PRIMARY_CELL,
				"MC Cron","MC Cron Didn't Finish",
				"From: 'Errors' <e@irelo.com>");
*/
		}
		else
		if ($end  < $twenty)
		{
			echo $end." " .$twenty."<br/>";
			echo "didnt Start 25!<Br/>";
/*
			mail(PRIMARY_CELL,
			"MC Cron - 25","MC Cron Didn't Start",
			"From: e25@irelo.com>");
*/
		}		
		else
		if ($end < $fifty)
		{
			echo $end." " .$fifty."<br/>";
			echo "didnt Start 55!<Br/>";
/*
			mail(PRIMARY_CELL,
			"MC Cron - 55","MC Cron Didn't Start",
			"From: e55@irelo.com>");
*/
		}	
	}

	/*
		helper to send out messages real easy.
	*/
	function sendError($msg,$to=PRIMARY_CELL)
	{
		#mail($to,"IRELO CRON Error",$msg,"From: 'Errors' <e@irelo.com>");
	}				

	function get_month_name($month)
	{
		$month = intval($month);
	
		switch ($month)
		{
			case 1: return "January";
			case 2: return "February";
			case 3: return "March";
			case 4: return "April";
			case 5: return "May";
			case 6: return "June";
			case 7: return "July";
			case 8: return "August";
			case 9: return "September";
			case 10: return "October";
			case 11: return "November";
			case 12: return "December";
		}
	}
	
	#**************************************
	function get_state_name($state)
	{
	  switch($state)
	  {
		case "AK": $mystate="Alaska"; break;
		case "AL": $mystate="Alabama"; break;
		case "AR": $mystate="Arkansas"; break;
		case "AZ": $mystate="Arizona"; break;
		case "CA": $mystate="California"; break;
		case "CO": $mystate="Colorado"; break;
		case "CT": $mystate="Connecticut"; break;
		case "DC": $mystate="Washington D.C."; break;
		case "DE": $mystate="Delaware"; break;
		case "FL": $mystate="Florida"; break;
		case "GA": $mystate="Georgia"; break;
		case "HI": $mystate="Hawaii"; break;
		case "IA": $mystate="Iowa"; break;
		case "ID": $mystate="Idaho"; break;
		case "IL": $mystate="Illinois"; break;
		case "IN": $mystate="Indiana"; break;
		case "KS": $mystate="Kansas"; break;
		case "KY": $mystate="Kentucky"; break;
		case "LA": $mystate="Louisiana"; break;
		case "MA": $mystate="Massachusetts"; break;
		case "MD": $mystate="Maryland"; break;
		case "ME": $mystate="Maine"; break;
		case "MI": $mystate="Michigan"; break;
		case "MN": $mystate="Minnesota"; break;
		case "MO": $mystate="Missouri"; break;
		case "MS": $mystate="Mississippi"; break;
		case "MT": $mystate="Montana"; break;
		case "NC": $mystate="North Carolina"; break;
		case "ND": $mystate="North Dakota"; break;
		case "NE": $mystate="Nebraska"; break;
		case "NH": $mystate="New Hampshire"; break;
		case "NJ": $mystate="New Jersey"; break;
		case "NM": $mystate="New Mexico"; break;
		case "NV": $mystate="Nevada"; break;
		case "NY": $mystate="New York"; break;
		case "OH": $mystate="Ohio"; break;
		case "OK": $mystate="Oklahoma"; break;
		case "OR": $mystate="Oregon"; break;
		case "PA": $mystate="Pennsylvania"; break;
		case "RI": $mystate="Rhode Island"; break;
		case "SC": $mystate="South Carolina"; break;
		case "SD": $mystate="South Dakota"; break;
		case "TN": $mystate="Tennessee"; break;
		case "TX": $mystate="Texas"; break;
		case "UT": $mystate="Utah"; break;
		case "VA": $mystate="Virginia"; break;
		case "VT": $mystate="Vermont"; break;
		case "WA": $mystate="Washington"; break;
		case "WI": $mystate="Wisconsin"; break;
		case "WV": $mystate="West Virginia"; break;
		case "WY": $mystate="Wyoming"; break;
	  }
	  return $mystate;
	}

	function sendThankyouEmail($myarray,$site)
	{
		if ($site == "tsc") {
			security_sendThankyouEmail($myarray);
		} else if ($site == "uk") {
			uksecurity_sendThankyouEmail($myarray);	
		}
	}
	

	function security_sendThankyouEmail($myarray)
	{				
		$quote_id = $myarray["quote_id"];
		$receipt = "<img src='http://www.irelocation.com/receipts/email.php".
		"?cat_id=4&quote_id=$quote_id' width='1' height='1'/>";
		
		$websource="TopSecurityCompanies.com";
		
		if ($myarray["quote_type"] == "res")
			$name = $myarray["name1"]." ".$myarray["name2"];
		else
			$name  = $myarray["name2"];//business contact.
					
		$affiliate[] = "<td valign='top' allign='center'>
					<table width='180' border='0' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF' style='border: 1px solid #999999;'><tr><td><img src='http://www.whitefence.com/qsrimages/bannersPartner/genAll180x150.gif' width='178' height='72'></td></tr><tr><td height='76' style='padding-top: 0px; padding-bottom: 0px; padding-left: 10px; padding-right: 2px;'><a href='http://www.whitefence.com?bpID=1049129&eID=1049165' style='font: bold 12px Arial, Helvetica, sans-serif; color: #0000FF;'>Order Electricity, Phone, Cable, Satellite &amp; more<br> online in minutes! </a></td></tr></table>
				</td>";			
						
		$affiliate[] = "<td valign='top' width='200' allign='center'>
					<a href='http://carshipping.com/?source=tsc' target='_blank'>
						<img src='http://irelocation.com/images/cs-banner-box.jpg' border='0' />
					</a>
						</td>";
		
		shuffle($affiliate);
		$count = (count($affiliate) * 2) + 1;
	
		$msg="<table border='2' width='100%' cellspacing='0' cellpadding='0' bordercolor='#000000'>
			<tr>
				<td>
				<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
					<tr>
						<td align='center' valign='top' colspan='$count' >
							<img border='0' src='http://www.irelocation.com/images/irelo%20logo.jpg' width='191' height='79' alt='The iRelocation Network'><br>
							<img border='0' src='http://www.irelocation.com/images/irelo_slogan.gif' width='413' height='79' alt='Partering customers with premier relocation services since 1998!'>
							<hr>
						</td>
					</tr>
					<tr valign='top'>
						<td align='left' valign='top' colspan='$count' >
							<b><font face='Arial' size='2'>$name,</font></b>
							<p>
								<b>
									<font face='Arial' size='2'>
										Thank you for requesting a Security Estimate through www.TopSecurityCompanies.com.&nbsp; Your information has been
										submitted and you will receive estimates from our affiliate
										companies shortly.
									</font>
								</b>
							</p>
							<p>
								<b>
									<font face='Arial' size='2'>
										Regards,<br/>
										The iRelocation Team
									</font>
								</b>
							</p>
							<p>
								<font face='Arial' size='1'>
									You will no longer receive any more emails from iRelocation.
									&nbsp; Your information is private and will
									not be distributed to third-party companies.
								</font>
							</p>
						</td>
					</tr>
					<tr><td colspan='5'>$receipt</td></tr>";
					
					
					
		if ($myarray["quote_type"] == "res")
		{
			$msg .= "<tr><td colspan='5'><hr/></td></tr>";
			$msg .= "<tr>
						<td>&nbsp;</td>
						<td colspan='4'>
							<font face='Arial' size='3'>
								<Strong>For more great services click on our affiliates below!</strong>
							</font>
						</td>
					</tr>";
			$msg .= "<tr>";
			$msg .= "<td width='20'>&nbsp;</td>";		
			$msg .= $affiliate[0];
			$msg .= "<td width='20'>&nbsp;</td>";				
			$msg .= $affiliate[1];
			$msg .= "<td width='20'>&nbsp;</td>";		
		}
		$msg .= "</tr></table></td></tr></table>";
		
		echo $msg;
		mail($myarray["email"],"$websource: Thank You", $msg, "From: '1st Moving Directory' <not_monitored@irelocation.com>\nReply-to: '1st Moving Directory' <quotes@1stmovingdirectory.com>\nContent-Type: text/html; charset=iso-8859-15");
		//mail("david@irelocation.com","$websource: Thank You", $msg, "From: '1st Moving Directory' <not_monitored@irelocation.com>\nReply-to: '1st Moving Directory' <not_monitored@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
	}

	function uksecurity_sendThankyouEmail($myarray)
	{				
		$quote_id = $myarray["quote_id"];
		$receipt = "<img src='http://www.irelocation.com/receipts/email.php".
		"?cat_id=4&quote_id=$quote_id' width='1' height='1'/>";
		
		$websource="UK.TopSecurityCompanies.com";
		
		if ($myarray["quote_type"] == "res")
			$name = $myarray["name1"]." ".$myarray["name2"];
		else
			$name  = $myarray["name2"];//business contact.
					
		$affiliate[] = "";		//-- removed affilaite as it was US only	
						
		$affiliate[] = "";		//-- removed affilaite as it was US only
		
		shuffle($affiliate);
		$count = (count($affiliate) * 2) + 1;
	
		$msg="<table border='2' width='100%' cellspacing='0' cellpadding='0' bordercolor='#000000'>
			<tr>
				<td>
				<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
					<tr>
						<td align='center' valign='top' colspan='$count' >
							<img border='0' src='http://www.irelocation.com/images/irelo%20logo.jpg' width='191' height='79' alt='The iRelocation Network'><br>
							<img border='0' src='http://www.irelocation.com/images/irelo_slogan.gif' width='413' height='79' alt='Partering customers with premier relocation services since 1998!'>
							<hr>
						</td>
					</tr>
					<tr valign='top'>
						<td align='left' valign='top' colspan='$count' >
							<b><font face='Arial' size='2'>$name,</font></b>
							<p>
								<b>
									<font face='Arial' size='2'>
										Thank you for requesting a Security Estimate through UK.TopSecurityCompanies.com.&nbsp; Your information has been
										submitted and you will receive estimates from our affiliate
										companies shortly.
									</font>
								</b>
							</p>
							<p>
								<b>
									<font face='Arial' size='2'>
										Regards,<br/>
										The iRelocation Team
									</font>
								</b>
							</p>
							<p>
								<font face='Arial' size='1'>
									You will no longer receive any more emails from iRelocation.
									&nbsp; Your information is private and will
									not be distributed to third-party companies.
								</font>
							</p>
						</td>
					</tr>
					<tr><td colspan='5'>$receipt</td></tr>";
					
					
/*  DIsabled the affilate ads in the email as we don't have any right now.
					
		if ($myarray["quote_type"] == "res")
		{
			$msg .= "<tr><td colspan='5'><hr/></td></tr>";
			$msg .= "<tr>
						<td>&nbsp;</td>
						<td colspan='4'>
							<font face='Arial' size='3'>
								<Strong>For more great services click on our affiliates below!</strong>
							</font>
						</td>
					</tr>";
			$msg .= "<tr>";
			$msg .= "<td width='20'>&nbsp;</td>";		
			$msg .= $affiliate[0];
			$msg .= "<td width='20'>&nbsp;</td>";				
			$msg .= $affiliate[1];
			$msg .= "<td width='20'>&nbsp;</td>";		
		}
*/

		$msg .= "</tr></table></td></tr></table>";
		
		echo $msg;
		mail($myarray["email"],"$websource: Thank You", $msg, "From: '$websource' <not_monitored@irelocation.com>\nReply-to: '$websource' <quotes@1stmovingdirectory.com>\nContent-Type: text/html; charset=iso-8859-15");
	}

	/*

	function selfmoving_sendThankyouEmail($myarray)
	{		
		$quotetype="a Self Moving";
		$websource="SelfMovingQuotes.com";
		$name = $myarray["fname"]." ".$myarray["lname"];
		//$name = "Testy McTesterson";
	
		$affiliate[] = "<td valign='top' allign='center'>
					<table width='180' border='0' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF' style='border: 1px solid #999999;'><tr><td><img src='http://www.whitefence.com/qsrimages/bannersPartner/genAll180x150.gif' width='178' height='72'></td></tr><tr><td height='76' style='padding-top: 0px; padding-bottom: 0px; padding-left: 10px; padding-right: 2px;'><a href='http://www.whitefence.com?bpID=1049129&eID=1049165' style='font: bold 12px Arial, Helvetica, sans-serif; color: #0000FF;'>Order Electricity, Phone, Cable, Satellite &amp; more<br> online in minutes! </a></td></tr></table>
				</td>";
		
		
		$affiliate[] = "<td valign='top' width='200' allign='center'>
					<a href='http://topsecuritycompanies.com/residential_security_quote.php?source=smq' target='_blank'>
						<img src='http://selfmovingquotes.com/images/tsc_banner.gif' border='0' />
					</a>
						</td>";
		
		shuffle($affiliate);
		$count = (count($affiliate) * 2) + 1;
	
		$msg="<table border='2' width='100%' cellspacing='0' cellpadding='0' bordercolor='#000000'>
			<tr>
				<td>
				<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
					<tr>
						<td align='center' valign='top' colspan='$count' >
							<img border='0' src='http://www.irelocation.com/images/irelo%20logo.jpg' width='191' height='79' alt='The iRelocation Network'><br>
							<img border='0' src='http://www.irelocation.com/images/irelo_slogan.gif' width='413' height='79' alt='Partering customers with premier relocation services since 1998!'>
							<hr>
						</td>
					</tr>
					<tr valign='top'>
						<td align='left' valign='top' colspan='$count' >
							<b><font face='Arial' size='2'>$name,</font></b>
							<p>
								<b>
									<font face='Arial' size='2'>
										Thank you for requesting a Self Moving Quote through www.SelfMovingQuotes.com.&nbsp; Your information has been
										submitted and you will receive estimates from our affiliate
										companies shortly.
									</font>
								</b>
							</p>
							<p>
								<font face='Arial' size='1'>
									You will no longer receive any more emails from iRelocation.
									&nbsp; Your information is private and will
									not be distributed to third-party companies.
								</font>
							</p>
						</td>
					</tr>
					<tr><td colspan='5'>&nbsp;</td></tr>
					<tr><td colspan='5'><hr/></td></tr>
					<tr>
						<td>&nbsp;</td>
						<td colspan='4'>
							<font face='Arial' size='3'>
								<Strong>For more great services click on our affiliates below!</strong>
							</font>
						</td>
					</tr>
					<tr>";
					
			$msg .= "<td width='20'>&nbsp;</td>";		
			$msg .= $affiliate[0];
			$msg .= "<td width='20'>&nbsp;</td>";				
			$msg .= $affiliate[1];
			$msg .= "<td width='20'>&nbsp;</td>";		
			
		$msg .= "</tr></table></td></tr></table>";
		
		echo $msg;
		mail($myarray["email"],"$websource: Thank You", $msg, "From: '1st Moving Directory' <not_monitored@irelocation.com>\nReply-to: '1st Moving Directory' <quotes@1stmovingdirectory.com>\nContent-Type: text/html; charset=iso-8859-15");
		//mail("david@irelocation.com","$websource: Thank You", $msg, "From: '1st Moving Directory' <not_monitored@irelocation.com>\nReply-to: '1st Moving Directory' <not_monitored@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
	}
	
	*/
?>