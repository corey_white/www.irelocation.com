<?

	/*
		this is old formatting code from Markus D.
	*/
include "../inc_mysql.php";
$sql = "select * from marble.auto_quotes where email like '%escamill0%'";
$rs = new marble($sql);
$data = create_broadcast_body(16,$rs);

echo $data[1];

function create_broadcast_body($camp_id, $rs){
			//create message body
			$r_year=substr($rs->myarray["received"],0,4);
			$r_month=substr($rs->myarray["received"],4,2);
			$r_day=substr($rs->myarray["received"],6,2);
			$r_hour=substr($rs->myarray["received"],8,2);
			$r_min=substr($rs->myarray["received"],10,2);

			$temp_year=substr($rs->myarray["est_move_date"],0,4);
			$temp_month=substr($rs->myarray["est_move_date"],5,2);
			$temp_day=substr($rs->myarray["est_move_date"],8,2);

			list($e_month,$e_day,$e_year)=explode("-",date("m-d-Y",mktime(0,0,0,$temp_month,$temp_day,$temp_year)));

			$p_area=substr($rs->myarray["phone_home"],0,3);
			$p_prefix=substr($rs->myarray["phone_home"],3,3);
			$p_suffix=substr($rs->myarray["phone_home"],6,4);

			$mycomments=$rs->myarray["comments"];
			$mycomments=str_replace('Vehicle Type: ','',$mycomments);
			$mycomments=str_replace('Vehicle Year: ','',$mycomments);
			$mycomments=str_replace('Vehicle Make: ','',$mycomments);
			$mycomments=str_replace('Vehicle Model: ','',$mycomments);
			$mycomments=str_replace('Comments: ','',$mycomments);
			$details=explode("\n",$mycomments);
			$newcomments="<tr><td width='200'>Vehicle Type: </td><td>$details[0]</td></tr>
			<tr><td width='200'>Vehicle Year: </td><td>$details[1]</td></tr>
			<tr><td width='200'>Vehicle Make: </td><td>$details[2]</td></tr>
			<tr><td width='200'>Vehicle Model: </td><td>$details[3]</td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			<tr><td width='200'>Comments: </td><td>$details[5]</td></tr>
			";

			$broadcast_body="<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
			<tr><td width='200'>Received: </td><td>$r_month/$r_day/$r_year $r_hour:$r_min</td></tr>
			<tr><td width='200'>Customer Name: </td><td>".$rs->myarray["name"]."</td></tr>
			<tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$rs->myarray["email"]."'>".$rs->myarray["email"]."</a></td></tr>
			<tr><td width='200'>Phone Number: </td><td>$p_area-$p_prefix-$p_suffix</td></tr>
			<tr><td width='200'>Preferred Contact Method: </td><td>".$rs->myarray["contact"]."</td></tr>
			<tr><td width='200'>Estimated Move Date: </td><td>".$rs->myarray["est_move_date"]."</td></tr>
			<tr><td width='200'>Moving From: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["origin_city"]."&state=".$rs->myarray["origin_state"]."&zipcode=".$rs->myarray["origin_zip"]."&country=US&cid=lfmaplink'>".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."</a></td></tr>
			<tr><td width='200'>Moving To: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["destination_city"]."&state=".$rs->myarray["destination_state"]."&country=US&cid=lfmaplink'>".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."</a></td></tr>
			<tr><td width='200'><a href='http://www.mapquest.com/directions/main.adp?1c=".$rs->myarray["origin_city"]."&2c=".$rs->myarray["destination_city"]."&1s=".$rs->myarray["origin_state"]."&2s=".$rs->myarray["destination_state"]."&1z=".$rs->myarray["origin_zip"]."&1y=US&2y=US&cid=lfddlink'>Driving Directions</a></td><td></td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			$newcomments
			</table>
			";

		switch($camp_id){
				case(3):
					#-----------------------------------------------
					#Custom for AAA Auto Movers
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];

						$mail_headers = "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(5):
					#-----------------------------------------------
					#Custom for Across USA Auto Transport
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];

						$mail_headers = "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(2):
					#-----------------------------------------------
					#Custom for A-AAA Auto Transport
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];

						$mail_headers = "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(19):
					#-----------------------------------------------
					#Custom for Trusted Auto Movers
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];
						$mail_headers = "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(14):
					#-----------------------------------------------
					#Custom for North American Auto Transport
					#-----------------------------------------------
						$mycomments=$rs->myarray["comments"];
						$mycomments=str_replace('Vehicle Type: ','',$mycomments);
						$mycomments=str_replace('Vehicle Year: ','',$mycomments);
						$mycomments=str_replace('Vehicle Make: ','',$mycomments);
						$mycomments=str_replace('Vehicle Model: ','',$mycomments);
						$mycomments=str_replace('Comments: ','',$mycomments);
						$details=explode("\n",$mycomments);

						$dest_zip = getDestinationZip($rs->myarray["destination_city"],$rs->myarray["destination_state"]);

						$broadcast_body="<lead>
											<est_move_date>$e_month/$e_day/$e_year</est_move_date>
											<name>".str_replace('&','and',trim($rs->myarray["name"]))."</name>
											<email>".trim($rs->myarray["email"])."</email>
											<phone>".trim($rs->myarray["phone_home"])."</phone>
											<contact>".trim($rs->myarray["contact"])."</contact>
											<origin_city>".trim($rs->myarray["origin_city"])."</origin_city>
											<origin_state>".trim($rs->myarray["origin_state"])."</origin_state>
											<origin_zip>".trim($rs->myarray["origin_zip"])."</origin_zip>
											<dest_city>".str_replace('&','',trim($rs->myarray["destination_city"]))."</dest_city>
											<dest_state>".trim($rs->myarray["destination_state"])."</dest_state>
											<dest_zip>".$dest_zip."</dest_zip>
											<vehicle_type>".trim($details[0])."</vehicle_type>
											<vehicle_year>".trim($details[1])."</vehicle_year>
											<vehicle_make>".trim($details[2])."</vehicle_make>
											<vehicle_model>".trim($details[3])."</vehicle_model>
											<comments>".str_replace('&','and',trim($details[4]))."</comments>
										</lead>";

							$mail_headers = "From: '1st Moving Directory - Car Shipping Leads' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(10):
					#-----------------------------------------------
					#Custom for Cross Country Auto Transport
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];

						$mail_headers = "From: '1st Moving Directory - CarShipping Leads' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				case(16):
					#-----------------------------------------------
					#Custom for Specialty Mobile Systems
					#-----------------------------------------------
						$broadcast_body="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nCustomer Name: ".$rs->myarray["name"]."\nE-Mail Address: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone_home"]."\nPreferred Contact Method: ".$rs->myarray["contact"]."\nEstimated Move Date: ".$rs->myarray["est_move_date"]."\n\nMoving From: ".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."\nMoving To: ".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."\n\n".$rs->myarray["comments"];

						$mail_headers = "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"];
					break;

				default:
					  $broadcast_body="<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
										  <tr><td width='200'>Received: </td><td>$r_month/$r_day/$r_year $r_hour:$r_min</td></tr>
										  <tr><td width='200'>Customer Name: </td><td>".$rs->myarray["name"]."</td></tr>
										  <tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$rs->myarray["email"]."'>".$rs->myarray["email"]."</a></td></tr>
										  <tr><td width='200'>Phone Number: </td><td>$p_area-$p_prefix-$p_suffix</td></tr>
										  <tr><td width='200'>Preferred Contact Method: </td><td>".$rs->myarray["contact"]."</td></tr>
										  <tr><td width='200'>Estimated Move Date: </td><td>".$rs->myarray["est_move_date"]."</td></tr>
										  <tr><td width='200'>Moving From: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["origin_city"]."&state=".$rs->myarray["origin_state"]."&zipcode=".$rs->myarray["origin_zip"]."&country=US&cid=lfmaplink'>".$rs->myarray["origin_city"].", ".$rs->myarray["origin_state"]." ".$rs->myarray["origin_zip"]."</a></td></tr>
										  <tr><td width='200'>Moving To: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$rs->myarray["destination_city"]."&state=".$rs->myarray["destination_state"]."&country=US&cid=lfmaplink'>".$rs->myarray["destination_city"].", ".$rs->myarray["destination_state"]."</a></td></tr>
										  <tr><td width='200'><a href='http://www.mapquest.com/directions/main.adp?1c=".$rs->myarray["origin_city"]."&2c=".$rs->myarray["destination_city"]."&1s=".$rs->myarray["origin_state"]."&2s=".$rs->myarray["destination_state"]."&1z=".$rs->myarray["origin_zip"]."&1y=US&2y=US&cid=lfddlink'>Driving Directions</a></td><td></td></tr>
										  <tr><td width='200'>-----</td><td></td></tr>
										  $newcomments
									  </table>";

						$mail_headers = "From: '1st Moving Directory - CarShipping Leads' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rs->myarray["email"]."\nContent-Type: text/html; charset=iso-8859-15";
					break;
		}

			$ary_body_headers[0]=$mail_headers;
			$ary_body_headers[1]=$broadcast_body;

			return $ary_body_headers;
}

	function getDestinationZip($city,$state)
	{
		$destination_zip="";
		if((trim($city)!='') && ($state!=''))
		{
			$nomore=0;
			$sql="select zip from zip_codes where lower(city) like '%".strtolower(trim($city))."%' and state='".$state."'";
			$rs2=new mysql_recordset($sql);
			if($rs2->rowcount()>0)
			{
				$rs2->fetch_array();
				$destination_zip=$rs2->myarray["zip"];
			}
			else
			{
				$city_length=strlen(trim($city));
				$mycount=10;

				if($city_length<10)
					{$mycount=$city_length;}

				for($i=$mycount;$i>=3;$i--)
				{
					if($nomore==0)
					{
						for($c=0;$c<$city_length-$i+1;$c++)
						{
							if($nomore==0)
							{
								$city_like=substr(strtolower(trim($city)),$c,$i);
								$sql="select zip from zip_codes where lower(city) like '%$city_like%' and state='".$state."'";
								$rs2=new mysql_recordset($sql);
								if($rs2->rowcount()>0)
								{
										$rs2->fetch_array();
										$destination_zip=$rs2->myarray["zip"];
										$nomore=1;
								}
							}
						}
					}
				}
			}
			if($destination_zip=="")
			{
				if(intval($city)>0)
				{
					$destination_zip=intval($city);
					$sql="select city,state from zip_codes where zip='$destination_zip'";
					$rs2=new mysql_recordset($sql);
					$rs2->fetch_array();
					$destination_city=$rs2->myarray["city"];
				}
			}
			else
			{
				$destination_city=$city;				
			}
			return $destination_zip;
		}
	}

?>