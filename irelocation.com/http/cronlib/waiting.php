<?	
	define(MELTING_POT_MAX_SEND,7);
	define(ATUS_MAX_SEND,10);
	define(TAS_MAX_SEND,5);
	define(PRIMARY_CELL,"code@irelocation.com");
	define(SECONDARY_CELL,"6024357508@vtext.com");

	if (!defined(CAMPAIGN))
	{	
		define(CAMPAIGN,"auto");
		define(MAX_SEND,MELTING_POT_MAX_SEND);
	}
	
	if (CAMPAIGN == "tas")
		define(MAX_SEND,TAS_MAX_SEND);
		
	include_once "../inc_mysql.php";
	
	define(DEBUG,false);
	define(DATABASE,"marble");
	
	define(SMALLEST_INCREMENT,100);

	/*
		verify all lead_Destination entries for this campaign have the right number of companies.
	*/
	function verify($site)
	{		
		$sql = "select distinct(lead_ids) lead_ids from ".DATABASE.".lead_destinations where site_code = '$site'";
		$rs = new mysql_recordset($sql);
		$counter = 0;
		while($rs->fetch_array())
		{
			$ids = $rs->myarray["lead_ids"];
			//echo $ids."<br/>";
			$list = split(",",$ids);
			$list = array_unique($list);
			if (count($list) != MAX_SEND)
			{
				echo "Invalid lead list: $ids!! ".print_r($list,true)."<br/>";			
				$counter++;
			}
		}
		echo $counter." sets of invalid lists.";
	}
	
	//returns a comma(,) delimeted list of lead_ids.	
	function grabNextList($site_code)
	{
		$sql = "select * from ".DATABASE.".lead_destinations where site_code = ".
				"'".$site_code."' order by use_count asc,dest_id asc limit 1"; 
				
		echo $sql."<br/>\n";
		$rs = new mysql_recordset($sql);
		if($rs->fetch_array())
		{
			$dest_id = $rs->myarray["dest_id"];
			$sql = "update ".DATABASE.".lead_destinations set ".
										"use_count = use_count+1 where dest_id = $dest_id";
			echo $sql."<br/>\n";
			$rs2 = new mysql_recordset($sql);
			//$lead_ids = $rs->myarray["lead_ids"];
			return $rs->myarray["lead_ids"];

		}
		else
		{
			mail(PRIMARY_CELL,"$site_code NO MAPPInG!",
					"$site_code MAPPING PROBLEM");
			exit();//no lists just quit.
		}
	}
	
	//number of days in the given month...
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	
	
	//how many leads have they gotten today.	
	function countToday($site_code)
	{
		
		$today = date("Ymd");
		$sql = "select 
					c.lead_id,
					count(q.quote_id) 'today'
				from
					".DATABASE.".campaign as c
					join 
					".DATABASE.".auto_quotes as q
					on locate(c.lead_id,q.lead_ids) > 0
				where 
					q.campaign = '".CAMPAIGN."'
					and c.site_id = '$site_code'
					AND q.received like '$today%'	
					and c.active
					and c.month = '".date("Ym")."'
				group by
					c.lead_id";
		echo "<pre>$sql</pre><Br/>\n";
		$numbers = array();
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
			$numbers[$rs->myarray["lead_id"]] = $rs->myarray["today"];
		$rs->close();
		return $numbers;
	}
	function countActual($site_code)
	{
		$this_month = date("Ym");
		$days_left = getNDays() - date("d") + 1;
		echo "Days Left: $days_left<br/>\n";
		
		$sql = "select 	
					c.lead_id,
					(c.monthly_goal+c.temp_goal) 'needed',
					ceiling((c.monthly_goal+c.temp_goal)/ ".$days_left." ) 'per_day'
				from
					".DATABASE.".campaign as c
				where 
					c.site_id = '$site_code'
					and active
					and c.month = '".date("Ym")."'
				group by
					c.lead_id
				";
		echo "<pre>$sql</pre><Br/>\n";
		$rs = new mysql_recordset($sql);		
		$numbers = array();
		while($rs->fetch_array())
		{			
			$a = array();
			extract($rs->myarray);
			$a["lead_id"] = $lead_id;
			if ($needed > 0)
				$a["needed"] = $needed;
			else
				$a["needed"] = 0;
			if ($per_day > 0)
				$a["per_day"] = $per_day;
			else
				$a["per_day"] = 0;
			$numbers[] = $a;
		}		
		
		
		$sql = "select 
					c.lead_id,
					(c.monthly_goal+c.temp_goal) - count(q.quote_id) needed,
					ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ $days_left ) 'per_day'
				from
					".DATABASE.".campaign as c
					join 
					".DATABASE.".auto_quotes as q
					on locate(c.lead_id,q.lead_ids) > 0
				where 
					q.campaign = '".CAMPAIGN."'
					and c.site_id = '$site_code'
					AND q.received like '$this_month%'					
					and c.active
					and c.month = '".date("Ym")."'
				group by
					c.lead_id";
					
		echo "<pre>$sql</pre><Br/>\n";		
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
		{
			$valid = true;
			$a = array();
			extract($rs->myarray);
			$a["lead_id"] = $lead_id;
			if ($needed > 0)
				$a["needed"] = $needed;
			else
				$a["needed"] = 0;
			if ($per_day > 0)
				$a["per_day"] = $per_day;
			else
				$a["per_day"] = 0;
			$numbers[] = $a;
		}
	
		return $numbers;
	}

/*
	old lead mapping code.

	function callAtMidnight($site_code)
	{
		$current_numbers = countActual($site_code);		
		foreach($current_numbers as $num)
		{
			extract($num);
			$sql = "update ".DATABASE.".campaign set leads_per_day = '$per_day', needed = $needed ".
					"where lead_id = $lead_id and site_id = '$site_code' and month = '".date("Ym")."'";
			echo "<pre>$sql</pre><Br/>\n";
			$rs = new mysql_recordset($sql);
		}	
		
		$sql = "delete from ".DATABASE.".lead_destinations ".
				"where site_code = '".CAMPAIGN."'";
		echo "Deletion Code: $sql <br/>";
		$rs = new mysql_recordset($sql);
		buildNewDestinations($site_code);
	}

	
	function testList($list,$new_lead_id,$non_compete_list="")
	{				
		if (substr_count($non_compete_list,",") > 0)
			$non_compete = split(",","");
		else if (strlen($non_compete_list) > 0)
			$non_compete = array($non_compete_list);
		else
			$non_compete = array();
	
		if (substr_count($list,$new_lead_id) == 0)
		{
			$valid = true;
			foreach($non_compete as $nc)
			{	
				$valid = (substr_count($list,$nc) == 0);				
				if (!$valid)
					return false;
			}		
			return true;
		}
		else
			return false;
	}
	
	
	function buildNewDestinations($site_code)
	{
		$sql = "select lead_id,(monthly_goal+temp_goal) 'monthly_goal',needed,leads_per_day,".
				"non_compete from ".DATABASE.".campaign where site_id = '$site_code' and active ".
				"and `month` = '".date("Ym")."' order by sort_order asc, needed desc ";
		echo "<pre>$sql</pre><Br/>\n";
		echo "build new destinations<br/>\n";
		$leads_today = countToday($site_code);
		$rs = new mysql_recordset($sql);
		$monthly_need = 0;
		
		if ($site_code == "atus")
			$max_send = ATUS_MAX_SEND;
		else		
		{
			if (CAMPAIGN == "auto")
				$max_send = MELTING_POT_MAX_SEND;
			else
				$max_send = TAS_MAX_SEND;
		}
			
		
		$already = array();
		
		while($rs->fetch_array())
		{
			$array = array();
			$already[] = $rs->myarray["lead_id"];
			$array[] = $rs->myarray["lead_id"];		
			$array[] = $rs->myarray["needed"];	//chagned, change back to monthly_goal if not worken
			$monthly_need += ($rs->myarray["needed"]/$max_send);
			$array[] = $rs->myarray["leads_per_day"] - $leads_today[$rs->myarray["lead_id"]];		
			$array[] = $rs->myarray["monthly_goal"];
			$array[] = $rs->myarray["non_compete"];
			if (strlen($rs->myarray["non_compete"]) > 0)
			echo "<br/>\nNon Compete:".$rs->myarray["non_compete"]."<br/>\n";
		
			$companies[] = $array;
		}
			
		
		$days_in_month = getNDays();
		$days_left_in_month = $days_in_month - date("d");	
		
		if ($days_left_in_month == 0)
			$days_left_in_month = 1;
			
		$monthly_need = ceil($monthly_need);
		if (DEBUG) echo "We need $monthly_need leads by the end of this month.<br/>\n";
		
		$leads_per_day = ceil($monthly_need/$days_left_in_month) + 10;
		if (DEBUG) echo "We need $leads_per_day leads per day.<br/>\n";
		
		$lead_lists = array();
		
		$lead_lists = array();
			for($i = 0; $i < $leads_per_day; $i++)
				$lead_lists[$i] =  array();	
		
		$full_lists = array();
		$company_list = array();
		$company_goals = array();
		
		foreach($companies as $c)
		{			
			echo $c[0]."<br>\n";
			$company_list[] = $c[0];//comp_id
			$company_goals[$c[0]] = $c[3];// [lead_id] = goal
			$non_compete_lists[$c[0]] = $c[4];
			$shift = intval(rand(0,1));
	
			if ($c[2] > 0)//leads_per_day
			{
				$ind_leads_per_day = $c[2];//leads_per_day
			}
			else
			{				
				echo "Company ".$c[0]." has reached its lead count for $site_code ";				
				continue;
			}
			
			$frequency = ceil($leads_per_day / $ind_leads_per_day);
			$allotted = 0;
			$ind_index = 0;
			$loop_count = 0;
			$invalid_spots = 0;		
			
			while ($allotted < $ind_leads_per_day && $invalid_spots < count($lead_lists))
			{
				for ($i = ($loop_count + $shift); 
						  ($i < count($lead_lists)) && ($i < $leads_per_day) && ($allotted < $ind_leads_per_day); 
						  $i += $frequency
					)
				{
	
					if (!testList(implode(" ",$lead_lists[$i]),$c[0],$c[4]))
					//if (substr_count(implode(" ",$lead_lists[$i]),$c[0]) > 0 )
					//if (array_search($c[0],$lead_lists[$i]))//bad
					{
						$invalid_spots++;
						continue;
					}
					else if (count($lead_lists[$i]) == $max_send)//bad
					{
						$full_lists[] = $lead_lists[$i];
						array_splice($lead_lists,$i,1);
						$i--;					
						continue;
					}
					else
					{
						$lead_lists[$i][] = $c[0];
						$allotted++;												
					}
				}
				
				if (count($lead_lists) > 40)		
					$loop_count++;
				else
				{
					$loop_count = 0;
					$shift = 0;
					$frequency = 1;
				}
	
			}		
			$count++;
			if (DEBUG) echo $c[0]." Allotted: $allotted Needed: $ind_leads_per_day<br/>\n";
		}
		$exit = false;
		$index = 0;
		
		//pull out full lists.
		while (!$exit && $index < count($lead_lists))
		{	
			if (count($lead_lists[$index]) == $max_send)
			{
				$full_lists[] = $lead_lists[$index];
				array_splice($lead_lists,$index,1);
				$index--;
			}
			else
				$index++;
		}
		
		
		
		for ($i = 0; $i <  count($lead_lists); $i++)
		{
			$needs = $max_send - count($lead_lists[$i]);
			shuffle($company_list);
			foreach($company_list as $c)
			{
				echo $company_goals[$c]."<br/>";
				
				if ($company_goals[$c] == SMALLEST_INCREMENT*5)
				{
					echo "$c ".$company_goals[$c]."<Br/>";
					continue;
				}
					
				if ($needs == 0)
					break;
				
				if (!testList(implode(" ",$lead_lists[$i]),$c,$non_compete_lists[$c]))
				//if (substr_count(implode(" ",$lead_lists[$i]),$c) != 0)
				//if (array_search($c,$lead_lists[$i]))
					continue;				
				else
				{
					$lead_lists[$i][] = $c;
					$needs--;
				}
			}		
		}
		
		$all = array_merge($full_lists,$lead_lists);
			
		if (DEBUG) echo "<pre>".print_r($all,true)."</pre>";
		
		$actual = array();
		$SQL_BULK = "insert into ".DATABASE.".lead_destinations (site_code,lead_ids) values ";
		shuffle($company_list);
		
		foreach($all as $lead)
		{
			sort($lead);			
			$lead = array_unique($lead);

			if (count($lead) != $max_send)
				echo "short list! { ".print_r($lead,true)." }<br>\n";

			$lead = fill($lead, $company_list, $max_send);

			if (count($lead) != $max_send)
				echo "STILL SHORT LIST!!! ".print_r($lead,true);
			$SQL_BULK .= " ('$site_code','".implode(",",$lead)."'), ";

			foreach($lead as $a)
				$actual[$a]++;
		}
	
		$SQL_BULK = substr($SQL_BULK,0,strlen($SQL_BULK)-2).';';
		echo $SQL_BULK."<br>\n";
		$now = microtime();
		$rs = new mysql_recordset($SQL_BULK);
		$done = microtime() - $now;
		echo "Insert to $done seconds.";
		
		if (DEBUG) echo "<pre>".print_r($actual,true)."</pre>";		
		//mail("david@irelocation.com","Mapping for ".date("Y m d"),print_r($actual,true));
	}
		
	
		
	function fill($list, $companies, $fillto)
	{
		sort($list);
		shuffle($companies);// <---- YOU HAVE TO SHUFFLE THIS LIST 
		//or one company gets the extras every time, bad!!!
		
		$list_as_string = implode(",",$list);
		
		if (count($list) != $fillto)
		{
			echo "Company List: ".implode(",",$companies)."<br/>";
			foreach($companies as $c)
			{
				if (substr_count($list_as_string,$c) == 0)
				{
					$list[] = $c;					
					sort($list);
					$list_as_string = implode(",",$list);					
					if (count($list) == $fillto)
					{
						echo "List Extended: $list_as_string<br/>";
						return $list;
					}
				}			
			}
			if (count($list) != $fillto)
			{
				$msg = "List: ".print_r($list,true)."\n\nCompanies: ".
					print_r($companies,true);	
					
				mail("david@irelocation.com","Lead List Fill Error!",$msg);								
				echo $msg;
				exit();
			}				
		}		
		else
			return $list;
	}
		
	*/
?>