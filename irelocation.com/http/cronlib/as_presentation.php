<?

	/*
		this is old formatting code from Markus D.
	*/

	// ~~~~~~~~~~~~~~ Debug Sample BEGIN ~~~~~~~~~~~~~~~~~~~~~
			$aryrows[1]->name = "David Lead";
			$aryrows[1]->email = "david@irelcoation.com";
			$aryrows[1]->phone = "8002334875";
			$aryrows[1]->contact = "email";
			$aryrows[1]->origin_city = "tempe";
			$aryrows[1]->origin_state = "AZ";
			$aryrows[1]->origin_zip = "85281";
			$aryrows[1]->dest_city = "mission viejo";
			$aryrows[1]->dest_state = "CA";
			$aryrows[1]->dest_zip = "92691";
			$aryrows[1]->type = "car";
			$aryrows[1]->year = "2001";
			$aryrows[1]->make = "lexus";
			$aryrows[1]->model = "rx300";
			$aryrows[1]->move_date = "11/12/2006";
			$aryrows[1]->table_id = "";

			$ary_body_headers = create_broadcast_body(33, $aryrows, 1);

			echo "<textarea rows='20' name='S1' cols='59'>";
			echo $ary_body_headers[0];
			echo "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
			echo $ary_body_headers[1];
			echo "</textarea>";
			
			mail("leads@professionalvehicleshipping.com","Auto Shipping Quote",$ary_body_headers[1],$ary_body_headers[0]);
			mail("david@irelocation.com","Auto Shipping Quote",$ary_body_headers[1],$ary_body_headers[0]);
			
	// ~~~~~~~~~~~~~~ Debug Sample END ~~~~~~~~~~~~~~~~~~~~~~~

function create_broadcast_body($camp_id, $aryrows, $loopit){

		// Populate Fields from Array
		$ary_name = explode(" ", $aryrows[$loopit]->name);

		$firstname = $ary_name[0];
		$lastname = $ary_name[1];

		$email = $aryrows[$loopit]->email;
		$phone = $aryrows[$loopit]->phone;
		$contact = $aryrows[$loopit]->contact;
		$origin_city = $aryrows[$loopit]->origin_city;
		$origin_state = $aryrows[$loopit]->origin_state;
		$origin_zip = $aryrows[$loopit]->origin_zip;
		$destination_city = $aryrows[$loopit]->dest_city;
		$destination_state = $aryrows[$loopit]->dest_state;
		$destination_zip = $aryrows[$loopit]->dest_zip;
		$type = $aryrows[$loopit]->type;
		$year = $aryrows[$loopit]->year;
		$make = $aryrows[$loopit]->make;
		$model = $aryrows[$loopit]->model;
		$move_date = $aryrows[$loopit]->move_date;
		$table_id = $aryrows[$loopit]->id;

		$broadcast_body  ="partnerid: ASC\nfirstname: $firstname\nlastname: $lastname\n";

		$broadcast_body .="email: ".$email."\n";
		$broadcast_body .="phone: $phone\ncontact: $contact\nmove_date: $move_date\norigin_city: $origin_city\norigin_state: $origin_state\norigin_zip: $origin_zip\ndestination_city: $destination_city\ndestination_state: $destination_state\ndestination_zip: $destination_zip\ntype: $type\nyear: $year\nmake: $make\nmodel: $model\ncondition: $condition\n\n";
		$broadcast_body .="Reply Via Email:\n";
		$broadcast_body .="mailto:".$email."\n\n";
		$broadcast_body .="View OCity OState:\n";
		$broadcast_body .="http://www.autoshipping.com/map.php?c=".urlencode($origin_city)."&s=".urlencode($origin_state)."\n\n";
		$broadcast_body .="View DCity DState:\n";
		$broadcast_body .="http://www.autoshipping.com/map.php?c=".urlencode($destination_city)."&s=".urlencode($destination_state)."\n\n";

		switch($camp_id){
				case(17): // Adonis

					// $adonis_url = "http://www.michaelnitro.com/adonis/autoquote.php?f_zip=".$origin_zip."&t_zip=".$destination_zip."&operable=Y&email=".$email."&type=".$type."&leadco=irelo";
					$broadcast_body = "http://www.adonisauto.com/autoquote.php?f_zip=".$origin_zip."&t_zip=".$destination_zip."&operable=Y&email=".$email."&type=".$type."&leadco=irelo";

					break;

				case(12): // NAA
					$mail_headers = "From:".$email;

					$broadcast_body ="<lead>
										 <est_move_date>".str_replace('-','/',$move_date)."</est_move_date>
										 <name>".$firstname." ".$lastname."</name>
										 <email>".$email."</email>
										 <phone>".$phone."</phone>
										 <contact>".$contact."</contact>
										 <origin_city>".$origin_city."</origin_city>
										 <origin_state>".$origin_state."</origin_state>
										 <origin_zip>".$origin_zip."</origin_zip>
										 <dest_city>".$destination_city."</dest_city>
										 <dest_state>".$destination_state."</dest_state>
										 <dest_zip>".$destination_zip."</dest_zip>
										 <vehicle_type>".$type."</vehicle_type>
										 <vehicle_year>".$year."</vehicle_year>
										 <vehicle_make>".$make."</vehicle_make>
										 <vehicle_model>".$model."</vehicle_model>
										 <comments></comments>
										 <source>asc</source>
								 	</lead>";

					break;

				case(33): // Affordable

					$mail_headers = "From: AutoShipping <".$email.">\nContent-Type: text/html; charset=iso-8859-15";

					$htmlmsg .="<html><body><table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
								<tr><td width='200'>Received: </td><td>".date("Y/m/d H:i:s",time()-1500)."</td></tr>
								<tr><td width='200'>Customer Name: </td><td>".$firstname." ".$lastname."</td></tr>
								<tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$email."'>".$email."</a></td></tr>
								<tr><td width='200'>Phone Number: </td><td>".$phone."</td></tr>
								<tr><td width='200'>Preferred Contact Method: </td><td>".$contact."</td></tr>
								<tr><td width='200'>Estimated Move Date: </td><td>".str_replace('-','/',$move_date)."</td></tr>
								<tr><td width='200'>Moving From: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$origin_city."&state=".$origin_state."&zipcode=".$origin_zip."&country=US&cid=lfmaplink'>".$origin_city.", ".$origin_state." ".$origin_zip."</a></td></tr>
								<tr><td width='200'>Moving To: </td><td><a href='http://www.mapquest.com/maps/map.adp?city=".$destination_city."&state=".$destination_state."&country=US&cid=lfmaplink'>".$destination_city.", ".$destination_state."</a></td></tr>
								<tr><td width='200'><a href='http://www.mapquest.com/directions/main.adp?1c=".$origin_city."&2c=".$destination_city."&1s=".$destination_zip."&2s=".$destination_state."&1z=".$origin_zip."&1y=US&2y=US&cid=lfddlink'>Driving Directions</a></td><td></td></tr>
								<tr><td width='200'>-----</td><td></td></tr>
								<tr><td width='200'>Vehicle Type: </td><td>".$type."</td></tr>
								<tr><td width='200'>Vehicle Year: </td><td>".$year."</td></tr>
								<tr><td width='200'>Vehicle Make: </td><td>".$make."</td></tr>
								<tr><td width='200'>Vehicle Model: </td><td>".$model."</td></tr>
								<tr><td width='200'>-----</td><td></td></tr>
								<tr><td width='200'>Comments: </td><td></td></tr>
								</table>
								</body></html>
								\n\n";

					break;
				default: // Everyone Else
					$mail_headers= "From:AutoShipping.com <no_reply@autoshipping.com>\nReply-to:".$email;
					break;
			}

			$ary_body_headers[0]=$mail_headers;
			$ary_body_headers[1]=$broadcast_body;

			return $ary_body_headers;
}
?>

