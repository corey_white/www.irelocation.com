<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for TRANSCRIPTION campaign
**********************************************************************
* Creation Date:  4/22/09 15:38 PM
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOTES:  This set of scripts in this folder are CLONED from the CALLCENTER and modified as needed. 

*/

//include library functions
include_once "../inc_mysql.php";
include_once "../cronlib/.functions.php";
include "CompanyClasses.php";

updateCronStatus("transcribe","started");

define(LIVE,true);//signal this IS a live quotemailer.


#
# Delete Test Leads.  //-- this funciton has not been altered
#
$sql="delete from movingdirectory.leads_transcription where ( (lower(first_name) like '%test%')  && (lower(last_name) like '%test%') ) or email = 'test@test.com' ";
$output_body .=  "Deleting Test Leads: $sql \n\n";
$rs=new mysql_recordset($sql);


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");

$sql="select * from movingdirectory.leads_transcription where (received='' or received='0') and email not like '%tessting@gmail.com%' and ready_to_send = 1 ";
$output_body .=  "SELECT FOR CLEANING UP ENTRIES: $sql \n\n";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{

	
	$newfirstname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["first_name"])));
	$newlastname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["last_name"])));
	$newcompany=mysql_real_escape_string($rs->myarray["company"]);
	$newzip=mysql_real_escape_string($rs->myarray["zip"]);
	$newphone=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone"]);
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newcontact=mysql_real_escape_string($rs->myarray["contact"]);
	$newtimezone=mysql_real_escape_string($rs->myarray["timezone"]);

	$newtranscribe_type=mysql_real_escape_string($rs->myarray["transcribe_type"]);
	$newservice_hours=mysql_real_escape_string($rs->myarray["service_hours"]);
	$newtranscribe_qty=mysql_real_escape_string($rs->myarray["transcribe_qty"]);
	$newpreferred_method=mysql_real_escape_string($rs->myarray["preferred_method"]);
	$newturnaround_avglines=mysql_real_escape_string($rs->myarray["turnaround_avglines"]);
	$newbegin_time=mysql_real_escape_string($rs->myarray["begin_time"]);

	$newcomment=mysql_real_escape_string($rs->myarray["comment"]);

	$newsource= mysql_real_escape_string($rs->myarray["source"]);
	
	$update_sql = "update movingdirectory.leads_transcription set first_name='$newfirstname', last_name='$newlastname', company='$newcompany', zip='$newzip', phone='$newphone', email='$newemail', contact='$newcontact', timezone='$newtimezone', transcribe_type='$newtranscribe_type', service_hours='$newservice_hours', transcribe_qty='$newtranscribe_qty', preferred_method='$newpreferred_method', turnaround_avglines='$newturnaround_avglines', begin_time='$newbegin_time', comment='$newcomment', received = '$mytime', source='$newsource'  	where quote_id='".$rs->myarray["quote_id"]."'";
	$output_body .=  "UPDATE SQL $update_sql\n\n";
	$rs2=new mysql_recordset($update_sql);
}	

//-- Company check - these are the companies that are in the current line up to potentially get leads
$campaign_month = date("Ym");
$comp_check = "select ca.lead_id, ca.leads_per_day, ca.monthly_goal, (select email from movingdirectory.rules where lead_id = ca.lead_id) as 'email'

from
	movingdirectory.campaign as ca
where	
	ca.active
	and ca.month = '$campaign_month'
	and ca.site_id = 'transcribe'
";
	
echo "$comp_check<br /><br />";

echo "Lead_ID, Leads per Day, Monthly Goal, Email<br />";

$check_rs = new mysql_recordset($comp_check);
while ($check_rs->fetch_array()) {
	echo $check_rs->myarray["lead_id"] . ", ";
	echo $check_rs->myarray["leads_per_day"] . ", ";
	echo $check_rs->myarray["monthly_goal"] . ", ";
	echo $check_rs->myarray["email"];
	echo "<br />";
}

echo "<hr />";

/*  begin processing */


$mytime = substr($mytime,0,10); //YYYYMMDDHH

//select all leads that are ready to go, and moving or internationals.
$sql = "select * from movingdirectory.leads_transcription where ready_to_send = 1 AND received like '$mytime%' ";

// TEMP SQL PULL that replaces the above for testing
#$sql = "select * from movingdirectory.leads_transcription where quote_id = XYZZY ";

$output_body .=  "SELECTING ALL QUOTES: $sql \n";

$rs = new mysql_recordset($sql);
$count = 0;

$output_body .=  "BEGIN LOOP OF QUOTES\n";

//-- How total many clients can get this lead?
$client_send_num = 5;

while ($rs->fetch_array()) { //-- BEGIN LOOP OF QUOTES

	$output_body .=  "\n";
	$output_body .=  "first_name: " . $rs->myarray["first_name"] . "\n";
	$output_body .=  "last_name: " . $rs->myarray["last_name"] . "\n";
	$output_body .=  "company: " . $rs->myarray["company"] . "\n";
	$output_body .=  "zip: " . $rs->myarray["zip"] . "\n";
	$output_body .=  "phone: " . $rs->myarray["phone"] . "\n";
	$output_body .=  "email: " . $rs->myarray["email"] . "\n";

	$output_body .=  "contact: " . $rs->myarray["contact"] . "\n";
	$output_body .=  "timezone: " . $rs->myarray["timezone"] . "\n";

	$output_body .=  "transcribe_type: " . $rs->myarray["transcribe_type"] . "\n";
	$output_body .=  "service_hours: " . $rs->myarray["service_hours"] . "\n";
	$output_body .=  "transcribe_qty: " . $rs->myarray["transcribe_qty"] . "\n";
	$output_body .=  "preferred_method: " . $rs->myarray["preferred_method"] . "\n";
	$output_body .=  "turnaround_avglines: " . $rs->myarray["turnaround_avglines"] . "\n";
	$output_body .=  "begin_time: " . $rs->myarray["begin_time"] . "\n";
	
	$output_body .=  "service_type: " . $rs->myarray["service_type"] . "\n";
	$output_body .=  "comment: " . $rs->myarray["comment"] . "\n";

	$output_body .=  "subcampaign: " . $rs->myarray["subcampaign"] . "\n";

	
	$output_body .=  "\n";
	
	
	//-- ESTABLISH WHICH LEAD FORMAT TO PULL
	//-- The Transcription lead campaign is different from other campaigns as every client that gets "all" leads will have more than 1 lead format.  this is due to different sites dumping into the same table, but with different data.  And the data needs to be outputted on the lead format differently.
	
	if ( $rs->myarray["subcampaign"] == "medical" ) { //-- medical transcription
		$cat_id = 10;
	} else { //-- Root transcription
		$cat_id = 7;
	}
	

	$count++;
		
		$data = new TranscribeQuoteData($rs->myarray);  //-- insantiate the class
		$output_body .=  "\n\n\n\n";
		$output_body .=  "=====================================================\n";
		$output_body .=  "Processing Transcribe Quote\n\n";
		$output_body .=  "=====================================================\n";
		
		/*-- process:
				THERE ARE 2 TYPES OF CLIENTS
						1. those that get all leads (PULL_ORDER 1)
						2. those that only get some leads, based on a daily quota (PULL_ORDER 2)
						
				EACH LEAD WILL GO TO $client_send_num CLIENTS
					5 slots for those that get all leads
					The 6th slot will be one of the daily quota clients, if any are below their daily limit

				So,
				1. DETERMINE if there are any Type 2 clients that need to get leads, if not, then need to pull 6 Type 1 leads.
				2. Determine how many Type 1 clients will be getting leads.
				
	-	Pull all pull_order=2
	-	Loop thru them, checking if they have received all their leads for the day
	-	IF they still need leads
		-	then add to array
		-	update movingdirectory.leads_transcription set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	-	IF array is empty
		-	then all_count = $client_send_num
	-	ELSE
		-	all_count = ($client_send_num-1)
		-	shuffle array
		-	send lead to first lead_id in array
		
	-	Pull either $client_send_num-1 or $client_send_num leads for pull_order=1 
	-	loop thru and send them each a lead
	-	update movingdirectory.leads_transcription set lead_ids = concat(lead_ids,'|222') where quote_id = XXX ?? not sure if needed ??
	
	
	NEW CHANGES
	Need to make the following happen:
	
	1. When either lead_id 1672 or 1687 comes up, the other lead_id has to also be one of the lead_ids getting the lead.  IOW, 1672 & 1687 always get leads together if either of them are selected.
	
	2. Certain companies don't want certain types of leads.  Need to filter those out.
	
	

		*/
		
		//-- $output_body .=  "\nXXXXX\n";
		//-- $output_body .=  "\n";
		
		//-- define some vars based on the current quote_id
		$quote_id = $rs->myarray["quote_id"];
		$sent_to_leads = array();
		$campaign_month = date("Ym");
		$current_date = date("Ymd");
		$current_month = $campaign_month;

		$lead_count = 0;
		
		//-- output a little info
		$output_body .=  "\nquote_id = $quote_id\n";
		$output_body .=  "\ncampaign_month = $campaign_month\n";
		$output_body .=  "\nlead_count = $lead_count\n";
		$output_body .=  "\ncampaign_month = $campaign_month\n";
		$output_body .=  "\ncurrent_date = $current_date\n";
		$output_body .=  "\n";

//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS
//-- ============== BEGIN QUOTA CLEINTS


		
		//-- process daily quota clients
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		$output_body .=  "\nPROCESSING DAILY QUOTA CLIENTS\n";
		
		$site_id = "transcribe";
		$output_body .=  "\nsite_id = $site_id\n\n";
		
		//-- get the quota companies
		//-- THIS FIRST SQL PULL is needed to so we can get a list of quota clients, and this list is used to see if any can still get leads for the day
		$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, ca.monthly_goal, 
			(select email from movingdirectory.rules 
				where lead_id = lf.lead_id) as 'email',
			lf.lead_id,
			format_type,
			lf.cat_id, 
			if(lead_body < 0,
				(select lead_body from movingdirectory.lead_format 
					where lead_id = lf.lead_body and cat_id = $cat_id),
				lf.lead_body) as 'lead_body' 
		from
			movingdirectory.lead_format as lf 
			join 
			movingdirectory.directleads as d
			on d.lead_id = lf.lead_id 
			join 
			movingdirectory.campaign as ca
			on d.lead_id = ca.lead_id	

		where	
			ca.active
			and ca.month = '$campaign_month'
			and ca.site_id = 'transcribe'
			and ca.pull_order = 2
		group by	
			ca.lead_id
		order by rand()";
		
		$output_body .=  "SQL FOR TYPE 2 (QUOTA) CLIENTS:\n$comp_sql\n\n";
		
		$comp_rs = new mysql_recordset($comp_sql);
		
		$quota_arr = array();
		
		//-- Loop through those that have been pulled and if any still need leads for today, stick them in an array.
		$output_body .=  "LOOPING THRU QUOTA CLIENTS\n";
		while ($comp_rs->fetch_array()) {
			# $XXXX = $comp_rs->myarray["XXXX"];
			$lead_id = $comp_rs->myarray["lead_id"];
			$leads_per_day = $comp_rs->myarray["leads_per_day"];
			$leads_per_month = $comp_rs->myarray["monthly_goal"];
			$output_body .=  "$lead_id can get $leads_per_day per day\n";
			$output_body .=  "$lead_id can get $leads_per_month per month\n";
			
			//-- get daily count
			$w_sql = "select * from movingdirectory.leads_transcription where lead_ids like '%$lead_id%' and left(received,8) = '$current_date' ";
			$output_body .= "$w_sql\n";
			$rs_w = new mysql_recordset($w_sql);
			$w_count = $rs_w->rowcount();
			
			$output_body .=  "Daily lead count for $lead_id = $w_count\n";
			
			//- get monthly count
			$m_sql = "select * from movingdirectory.leads_transcription where lead_ids like '%$lead_id%' and left(received,6) = '$current_month' ";
			$output_body .= "$m_sql\n";
			$rs_m = new mysql_recordset($m_sql);
			$m_count = $rs_m->rowcount();
			
			$output_body .=  "monthly lead count for $lead_id = $m_count\n";
			
			//-- if still need leads, add them to array
			if ( $w_count < $leads_per_day && $m_count < $leads_per_month ) {
				$output_body .=  "------- Added $lead_id to array\n";
				$quota_arr[] = $lead_id;
			} 


		}
		
			$output_body .=  "QUOTA ARRAY AFTER CHECKING DAILY LEADS TOTALS:\n<pre>";
			$output_body .= print_r($quota_arr, true);
			$output_body .=  "</pre>\n";



			//-- HERE IS WHERE WE CAN REMOVE LEAD_IDS FOR THOSE THAT ONLY WANT CERTAIN CRITERIA FOR LEADS
			
			$output_body .=  "rs->myarray[transcribe_type] = " . $rs->myarray["transcribe_type"] . "\n";
			
			//-- World Wide Dictation Service (1674) only wants Medical
			if ( $rs->myarray["subcampaign"] != "medical" ) {
				$output_body .=  "World Wide Dictation Service (1674) only wants Medical leads, checking for 1674 in the Quota Array\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1674" ) {
						unset($quota_arr[$key]);
						$output_body .=  "REMOVING 1674 FROM THE ARRAY\n";
					}
				}
			} 


			//-- MedPracticeFlow (1685) only wants Medical and Legal leads (if subcategory is Medical, let it pass, otherwise, if it's Root, it must be legal)
			if ( $rs->myarray["subcampaign"] == "root" && $rs->myarray["transcribe_type"] != "Legal" ) {
				$output_body .=  "MedPracticeFlow (1685) only wants Medical and Legal leads, checking for 1685 in the Quota Array\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1685" ) {
						unset($quota_arr[$key]);
						$output_body .=  "REMOVING 1685 FROM THE ARRAY\n";
					}
				}
			} 


/* EXAMPLE
			//-- OnBrand24 (1670) does not receive Answering Service leads
			$output_body .=  "rs->myarray[type_answer_service] = " . $rs->myarray["type_answer_service"] . "\n";
			
			if ( $rs->myarray["type_answer_service"] == "Yes" ) {
				$output_body .=  "OnBrand24 (1670) does not want Answering Service leads, checking for 1670 in the Quota Array\n";
				foreach($quota_arr as $key => $value) {
					if( $value == "1670" ) {
						unset($quota_arr[$key]);
						$output_body .=  "REMOVING 1670 FROM THE ARRAY\n";
					}
				}
			} 
*/



			$output_body .=  "QUOTA ARRAY AFTER CHECKING FOR \"INBOUND\" LEADS:\n<pre>";
			$output_body .= print_r($quota_arr, true);
			$output_body .=  "</pre>\n";

		
			
			if ( sizeof($quota_arr) == 0 ) { //-- if there are no lead_ids in the array, set all_count and move on
				$all_count = $client_send_num;
				
			} else { 									//-- else set all_count = 5, shuffle array, send lead to first lead_id in array - this is the quota client that gets this lead
				$all_count = ($client_send_num - 1);
				shuffle($quota_arr);
				$output_body .=  "QUOTA ARRAY AFTER SHUFFLING:\n<pre>";
				$output_body .= print_r($quota_arr,true);
				$output_body .=  "</pre>\n";

				$Qlead_id = $quota_arr[0];
				
				$output_body .=  "SELECTING $Qlead_id for the quota lead\n\n";
				
				//-- get lead data for just the one lead_id
				$lead_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
					(select email from movingdirectory.rules 
						where lead_id = lf.lead_id) as 'email',
					lf.lead_id,
					format_type,
					lf.cat_id, 
					if(lead_body < 0,
						(select lead_body from movingdirectory.lead_format 
							where lead_id = lf.lead_body and cat_id = $cat_id),
						lf.lead_body) as 'lead_body' 
				from
					movingdirectory.lead_format as lf 
					join 
					movingdirectory.directleads as d
					on d.lead_id = lf.lead_id 
					join 
					movingdirectory.campaign as ca
					on d.lead_id = ca.lead_id	
		
				where	
					ca.active
					and ca.month = '$campaign_month'
					and ca.site_id = 'transcribe'
					and ca.pull_order = 2
					and ca.lead_id = $Qlead_id
				group by	
					ca.lead_id";
					
				$output_body .=  "GETTING SINGLE LEAD_ID FOR QUOTA SLOT: $lead_sql \n\n";
				
				$leads_rs = new mysql_recordset($lead_sql);
				$leads_rs->fetch_array();
		
				$output_body .=  "LEADS ARRAY<pre>";
				$output_body .= print_r($leads_rs->myarray,true);
				$output_body .=  "</pre>\n";

				$lead_count++;
				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$output_body .=  "\nINTERNAL TEST - NOT BEING SENT\n";
				} else {
					$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
					$output_body .=  "\nSent to Processing (\$data->process)\n";
				}
				
/*
				$data->finishLead(); //-- update ready_to_send = 2
				$output_body .=  "\nSent to Finishing (\$data->finishLead)\n";
*/
				
				$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
				$output_body .=  "sent to " . $leads_rs->myarray['lead_id'] . "\n";
				$output_body .=  "\n\n";

			}
			
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS
//-- ============== END QUOTA CLEINTS

	


//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS
//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS
//-- ============== PROCESS REMINING CLEINTS THAT GET ALL LEADS

//-- NOTE, we are now randomly pulling ALL companies that are remining and counting up to $client_send_num

			//-- Now, send the lead to the Type 1 pull order leads
			$output_body .=  "\nPROCESSING CLIENTS THAT GET ALL LEADS\n";
			$output_body .=  "PROCESSING CLIENTS THAT GET ALL LEADS\n";
			$output_body .=  "PROCESSING CLIENTS THAT GET ALL LEADS\n";
			
			$comp_sql = "select format_id, ca.pull_order, ca.leads_per_day, 
				(select email from movingdirectory.rules 
					where lead_id = lf.lead_id) as 'email',
				lf.lead_id,
				format_type,
				lf.cat_id, 
				if(lead_body < 0,
					(select lead_body from movingdirectory.lead_format 
						where lead_id = lf.lead_body and cat_id = $cat_id),
					lf.lead_body) as 'lead_body' 
			from
				movingdirectory.lead_format as lf 
				join 
				movingdirectory.directleads as d
				on d.lead_id = lf.lead_id 
				join 
				movingdirectory.campaign as ca
				on d.lead_id = ca.lead_id	
	
			where	
				ca.active
				and ca.month = '$campaign_month'
				and ca.site_id = 'transcribe'
				and ca.pull_order = 1
			group by	
				ca.lead_id
			order by rand() ";
			
			$output_body .=  "ALL LEADS COMPANIES SQL:\n$comp_sql\n";
			
			$lead_array = new mysql_recordset($comp_sql);
			
			//-- loop through and send out lead
			while ($lead_array->fetch_array()) {

				$lead_id = $lead_array->myarray["lead_id"];
				
				$output_body .=  "<b>PROCESSING COMPANY $lead_count: $lead_id</b>\n";

				//-- Here's where we make the exceptions for who gets which lead				
				if ( $rs->myarray["last_name"] == "Internaltest" ) {
					$output_body .=  "INTERNAL TEST - NOT BEING SENT\n\n";
				
				} elseif ( ($rs->myarray["subcampaign"] != "medical" || $rs->myarray["transcribe_type"] == "Physical Therapy/Chiropractic" || $rs->myarray["transcribe_type"] == "Other" || $rs->myarray["transcribe_type"] == "Dental Practice" || $rs->myarray["transcribe_type"] == "Psychologist Practice") && $lead_array->myarray['lead_id'] == 1673 ) { //-- MxSecure (1673) only wants medical leads 
					$output_body .=  "MxSecure (1673) only wants medical leads, but not Physical Therapy/Chiropractic, Dental Practice, Psychologist Practice, or Other - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["subcampaign"] != "medical" && $lead_array->myarray['lead_id'] == 1677 ) { //-- Intivia, Inc (1677) only wants medical leads 
					$output_body .=  "Intivia, Inc (1677) only wants medical leads - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["subcampaign"] == "medical" && $lead_array->myarray['lead_id'] == 1694 ) { //-- Always On Time (1694) doesn't want Medical Leads
					$output_body .=  "Always On Time (1694) doesn't want Medical Leads - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["subcampaign"] != "medical" && $lead_array->myarray['lead_id'] == 1675 ) { //-- Viva (1675) only want Medical Leads
					$output_body .=  "Viva (1675) only want Medical Leads - NOT BEING SENT\n\n";
				
				} elseif ( ( $rs->myarray["subcampaign"] != "medical" || ($rs->myarray["subcampaign"] == "medical" && ($rs->myarray['transcribe_qty'] == "1-2" || $rs->myarray['transcribe_qty'] == "3-5") ) )  && $lead_array->myarray['lead_id'] == 1702 ) { //-- MedTek.net, Inc (1702) they only want medical leads with 6 physicians or more
					$output_body .=  "MedTek.net, Inc (1702) they only want medical leads with 6 physicians or more - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["preferred_method"] == "Cassette Tapes" && $lead_array->myarray['lead_id'] == 1694 ) { //-- Always On Time (1694) doesn't want Cassette Tapes
					$output_body .=  "Always On Time (1694) doesn't want Cassette Tapes - NOT BEING SENT\n\n";
				
/* EXAMPLES
				} elseif ( $rs->myarray["service_type"] != "Inbound Calls" && $lead_array->myarray['lead_id'] == 1682 ) { //-- Sunshine Communications (1682) wants Inbound leads only 
					$output_body .=  "Sunshine Communications (1682) wants Inbound leads only - NOT BEING SENT\n\n";
				
				} elseif ( $rs->myarray["service_type"] != "Outbound Calls" && $lead_array->myarray['lead_id'] == 1678 ) { //-- Focus One Sales (1678) wants Outbound leads only 
					$output_body .=  "Focus One Sales (1678) wants Outbound leads only - NOT BEING SENT\n\n";
				
*/
				} else {
					$data->process($lead_array->myarray); //-- send for processing the quote (send to lead_id)
					$output_body .=  "\nSent to Processing (\$data->process)\n";

					$sent_to_leads[$lead_count] = $lead_array->myarray['lead_id'];
					$output_body .=  "sent to " . $lead_array->myarray['lead_id'] . "\n";
					$output_body .=  "\n\n";

					$lead_count++;
					
					$output_body .= "\n";
					
					
				}
				
				if ( $lead_count == $client_send_num ) { //-- If we have sent to $client_send_num leads, break the While Loop
					break;
				} 

			}
	

		
		
		$output_body .=  "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.\n";
		foreach($sent_to_leads as $key => $value){
			$output_body .=  "Lead: $key sent to: $value\n";
		}

		//-- Hopefully this is where it needs to be
		$data->finishLead(); //-- update ready_to_send = 2
		$output_body .=  "\nSent to Finishing (\$data->finishLead)\n";
	
	
} //-- end the while loop (needs to stay)


/* ===== we can leave this ===== */
if ($count > 0) {
	$output_body .=  $count." leads processed.";
} else {
	$output_body .=  "no leads processed.";
}
	
	
#mail("rob@irelocation.com","Transcribe Quotemailer Output","$output_body");
$output_body = nl2br($output_body);
echo $output_body;

#mail("rob@irelocation.com","Transcribe Quotemailer Ran","Call Center Quotemailer Ran");

/* turn on for trouble shooting
$headers .= 'From: Transcribe Quotemailer <noreply@irelocation.com>' . "\r";
$mail_body = ereg_replace("\n","",$body);
mail("rob@irelocation.com","**NEW** QUOTEMAILER OUTPUT","$mail_body","$headers");
*/

/*
	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
*/
?>