<?
/*
NOTE:  this code derived from the moving quotes code base.  If additional functions are needed, they can be derived from that code
*/
	
	define(PIPE,"||");
	define(PIPE_LENGTH,2);

#	include_once "../nusoap/nusoap.php";
#	include_once "../jtracker/jtracker.lib.php";
#	include_once "../inc_mysql.php";
	include_once "save_response.php";	
	include_once "countryfunction.php";		
	include_once "waiting.php";
	include_once "thankyou_function.php";
	
	class GenericQuoteData
	{
		var $myarray;
		var $isLocal;
		
		function GenericQuoteData($thearray)
		{
			$this->myarray = $thearray;
			$this->isLocal = false;
		}
		
		function finishLead()
		{			
			$quote_id = $this->myarray["quote_id"];
			
			
			$sql = "update movingdirectory.leads_transcribe set ready_to_send = 2 where quote_id = $quote_id;";
			echo "UPDATING LEAD_IDS: $sql<br />";
			$rs = new mysql_recordset($sql);			
			if (LIVE)
				thankyouEmail($this->myarray);
		}
		
		//get a value from the array by name append codes to format it.
		function get($key,$url=0)
		{				
			if (substr_count($key,"url_") > 0)
			{
				$key = str_replace('url_','',$key);
				$url = 1;
			}
			if (substr_count($key,"xml_") > 0)
			{
				$key = str_replace('xml_','',$key);
				$url = 2;
			}
			
			if (strlen($this->myarray[$key]) > 0)
			{
				$v =  $this->myarray[$key];				
				if ($url == 1)
					return urlencode($v);
				else if ($url == 2)
					return $this->xmlentities($v);
				else return $v;
			}
			else			
				return "keyword '$key' not found<br/>";
		}
		//good ol xmlentities :P
		function xmlentities ( $string )
		{
		   return str_replace ( 
		   	array ( '&', '"', "'", '<', '>' ), 
			array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
			$string 
			);
		}

		//get destination zip code based on the city and state. simple.
		function getDestinationZip($city,$state)
		{
			$destination_zip="";
			if((trim($city)!='') && ($state!=''))
			{
				$nomore=0;
				$sql="select zip from movingdirectory.zip_codes where lower(city) like '%".strtolower(trim($city))."%' and state='".$state."'";
				$rs2=new mysql_recordset($sql);
				if($rs2->rowcount()>0)
				{
					$rs2->fetch_array();
					$destination_zip=$rs2->myarray["zip"];
				}
				else
				{
					$city_length=strlen(trim($city));
					$mycount=10;
	
					if($city_length<10)
						{$mycount=$city_length;}
	
					for($i=$mycount;$i>=3;$i--)
					{
						if($nomore==0)
						{
							for($c=0;$c<$city_length-$i+1;$c++)
							{
								if($nomore==0)
								{
									$city_like=substr(strtolower(trim($city)),$c,$i);
									$sql="select zip from movingdirectory.zip_codes where lower(city) like '%$city_like%' and state='".$state."'";
									$rs2=new mysql_recordset($sql);
									if($rs2->rowcount()>0)
									{
											$rs2->fetch_array();
											$destination_zip=$rs2->myarray["zip"];
											$nomore=1;
									}
								}
							}
						}
					}
				}
				if($destination_zip=="")
				{
					if(intval($city)>0)
					{
						$destination_zip=intval($city);
						$sql="select city,state from movingdirectory.zip_codes where zip='$destination_zip'";
						$rs2=new mysql_recordset($sql);
						$rs2->fetch_array();						
					}
				}
				return $destination_zip;
			}
			else if ($state != "")
			{
				$sql="select zip from movingdirectory.zip_codes where state = '$state' ";
				$rs2=new mysql_recordset($sql);
				$rs2->fetch_array();
				$destination_zip = $rs2->myarray["zip"];
				return $destination_zip;
			}
		}
		//find all the ||[variable]|| in the lead_body column in the lead_format table.
		function findAllOccurences($Haystack, $needle, $limit=0)
		{
		  $Positions = array();
		  $currentOffset = 0;
		  $count=0;
		  while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
		  {
		   $Positions[] = $pos;
		   $offset = $pos + strlen($needle);
		   $count++;
		  }
		  return $Positions;
		}
		//merge the data from this object and the lead_body string.
		function merge($type)
		{
			//if(DEBUG) echo "Merging Data<br/>\n";
			$success = 1;
			$offset = 0;
			$variables = array();
			$results = $this->findAllOccurences($this->lead_body,PIPE);
			
			for ($i = 0; $i < count($results); $i +=  2)
			{					
				$end = $results[$i+1]-$results[$i]+PIPE_LENGTH;
				$variable = substr($this->lead_body,$results[$i],$end);				
				$variable = str_replace(PIPE,"",$variable);
				$variables[] = $variable;				
			}
			
			$variables = array_unique($variables);
			
			foreach ($variables as $v)
			{
				if ($type == "xml")
					$find_variable = "xml_$v";
				else
					$find_variable = $v;
				$find_variable = str_replace("xml_xml_","xml_",$find_variable);
				$find_variable = str_replace("url_url_","url_",$find_variable);
				
				$value = $this->get($find_variable);
				if (substr_count($value,"keyword") > 0)
					$success = 0;
									
				$this->lead_body = str_replace("||$v||",$value,$this->lead_body);
			}
			return $success;
		}
		//update the lead id list as this lead has been sent to each company				
		function updateLeadIds($lead_id)
		{
			if (LIVE && !$this->isLocal)//only update if this is a live run of the quotemailer.
			{				
				$quote_id = $this->myarray["quote_id"];
				if(DEBUG) echo "$quote_id<br/>";
				if (strlen($quote_id) > 0)
				{
					$sql = "update movingdirectory.leads_transcribe set lead_ids = ".
					" CONCAT(lead_ids,'|',$lead_id), num_sent = num_sent+1 ".
					" where quote_id = $quote_id;";
					echo "UPDATE THE LEAD ID LIST AS THIS LEAD HAS BEEN SENT TO EACH COMPANY: $sql<br />";
					
					$rs = new mysql_recordset($sql);				
					$rs->close();
				}
			}
		}
		
		
		//validate the response from technologically up-to-date people
		//people who don't use email.
/*
		function validateResponse($response,$company, $rsmyarray) //-- THIS FUNCITON SHOULD NOT BE USED AT THIS TIME
		{
			$company = trim($company);
			$response = trim($response);
			$quote_id = $rsmyarray["quote_id"];
			if (strlen($company) > 0 || strlen($response) > 0) {
				#mail("rob@irelocation.com","validation 2","Company: $company\n\n"."Response: $response \n\nQuoteID:".$quote_id);

			} else {
				return;//no data to check against.
			}
			
			
			if ($company == "XXXX") {
				saveResponse($rsmyarray["quote_id"],$response,"movebekins");
				if(substr_count(strtolower($response),"transfer failed")>0) {
					#mail("code@irelocation.com,david@irelocation.com","Bekins Post Failure",$response."\nLead Data: ".print_r($this->myarray,true));
					recordLeadFailure($this->myarray,
												1388,
												"Moving",
												"movingdirectory.leads_transcribe");
				}				
			}

		}	
*/
	}
	
	class TranscribeQuoteData extends GenericQuoteData {
	
		//assign the quote data to the object, form variables we will need.
		function TranscribeQuoteData($thearray) {
		
			parent::GenericQuoteData($thearray);
			
			/*
			Working with the following fields:
			
			first_name									$this->myarray["first_name"]
			last_name									$this->myarray["last_name"]
			company										$this->myarray["company"]
			zip												$this->myarray["zip"]
			phone											$this->myarray["phone"]
			email											$this->myarray["email"]
			
			contact											$this->myarray["contact"]
			timezone										$this->myarray["timezone"]

			transcribe_type							$this->myarray["transcribe_type"]
			service_hours								$this->myarray["service_hours"]
			transcribe_qty								$this->myarray["transcribe_qty"]
			preferred_method						$this->myarray["preferred_method"]
			turnaround_avglines					$this->myarray["turnaround_avglines"]
			begin_time									$this->myarray["begin_time"]
			
			comment										$this->myarray["comment"]
			*/
			
			//-- DATE
			$r_year=substr($this->myarray["received"],0,4);
			$this->myarray["ryear"] = $r_year;
			
			$r_month=substr($this->myarray["received"],4,2);			
			$this->myarray["rmonth"] = $r_month;
			
			$r_day=substr($this->myarray["received"],6,2);
			$this->myarray["rday"] = $r_day;
			
			//-- TIME
			$r_hour=substr($this->myarray["received"],8,2);
			$r_min=substr($this->myarray["received"],10,2);
			
			//-- LEAD DATE AND TIME
			$this->myarray["received_standard"] = "$r_month/$r_day/$r_year $r_hour:$r_min";
			
			//-- PHONE
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);

			//-- PHONE WITH DASHES
			$this->myarray["phone_dashes"] = "$p_area-$p_prefix-$p_suffix";
			
			
			//-- CREATE FULLNAME
			$this->myarray["fullname"] = $this->myarray["first_name"] . " " . $this->myarray["last_name"];
			
			//-- GET CITY - STATE from ZIP
			$zipsql = "select city,state from movingdirectory.zip_codes where zip = '" . $this->myarray["zip"] . "'" ;
			$rs_zip = new mysql_recordset($zipsql);
			$rs_zip->fetch_array();
			
			if ( !$rs_zip->myarray["city"] ) {
				$this->myarray["city"] = "Not Found";
			} else {
				$this->myarray["city"] = $rs_zip->myarray["city"];
			}
			if ( !$rs_zip->myarray["state"] ) {
				$this->myarray["state"] = "Not Found";
			} else {
				$this->myarray["state"] = $rs_zip->myarray["state"];
			}
			
			
			
			//-- Check for comments
			if ( $this->myarray["comment"] == "" ) {
				$this->myarray["comment"] = "None";
			} 

		}
		

		//process this lead array, merge it and if successful, send it.
		function process($lead_array) {
		global $body;
		
		echo "BEGIN FUNCTION PROCESS<br />";
		
			$this->lead_body = $lead_array["lead_body"];

			$success = $this->merge($lead_array["format_type"]);
			
			if ($success == 1) {
			
				//lead_id,lead_body,format_type,email
				if ($lead_array["format_type"] == "email") {
				
					echo "PROCESSING TO SEND LEAD AS EMAIL<br />";
					
					list ($subject,$body) = split("\n",$this->lead_body,2);
					list($headers,$body) = split("---------------",$body,2);
					$header = trim($header);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = trim(implode("\n",$body_parts));
					$this->lead_body = $body;
					mail($lead_array["email"],$subject,trim($body),trim($headers));			
					$body .= "\n\n<br /><br />===================>> Just Emailed Lead to " . $lead_array["email"] . "<br /><br />\n\n";
					#mail("rob@irelocation.com",$subject,trim($body),trim($headers));			
					
				} else if ($lead_array["format_type"] == "xml") { // curl POST
				
					echo "PROCESSING TO SEND LEAD AS CURL POST<br />";

					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
										
					$ch=curl_init(trim($url));
					
					foreach($variables as $v)
					{
						if (strlen(trim($v)) > 0)
						{
							list($k,$va) = split(":",$v,2);							
							
							if ($k == "validation")
								$validation = $va;
							else
							{																
								if ($k == "CURLOPT_HTTPHEADER")
								{
									if (substr_count(trim($va),",") > 0)
										$va = split(",",$va);
									curl_setopt($ch,CURLOPT_HTTPHEADER,$va);
								}
								else
									curl_setopt($ch, trim($k), trim($va));
							}
						}
					}
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, trim($body));
					
					$response = curl_exec($ch);
					curl_close($ch);
					$this->lead_body = $body;
					if(DEBUG) echo "Moving Quote Response:$response for $validation<br/>\n";
					//mail("rob@irelocation.com","validation","Company: $validation\n\n"."Response: $response");
					$this->validateResponse($response,$validation, $this->myarray);
					
					#mail("rob@irelocation.com","XML Moving Lead","XML:".$body."\nQuote_id:".$this->myarray["quote_id"]."\nResponse:".$response);

				} else if ($lead_array["format_type"] == "post") { // faturl
				
					echo "PROCESSING TO SEND LEAD AS POST<br />";

					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
					
					$ch=curl_init($url."?".$body);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);
					if(DEBUG) echo "Moving Quote Response:$response for $validation<br/>\n";
					curl_close($ch);
					$this->lead_body = $body;
					//mail("david@irelocation.com","validation","Company: $validation\n\n"."Response: $response");
					$this->validateResponse($response,$validation, $this->myarray);
					
					//mail("david@irelocation.com","Fat Url Moving Post","URL:".$url."?".$body."\n"."Response:".$response);					

				} else if ($lead_array["format_type"] == "file") { //-- CSV/TSV File Attachment
					//-- send an email with the data attached to a file.
					
					//-- PROCESSING TO SEND LEAD AS FILE ATTACHMENT <<------------------------------- NOT UPDATED FOR THIS DATA SET
					//-- PROCESSING TO SEND LEAD AS FILE ATTACHMENT <<------------------------------- NOT UPDATED FOR THIS DATA SET
					//-- PROCESSING TO SEND LEAD AS FILE ATTACHMENT <<------------------------------- NOT UPDATED FOR THIS DATA SET
					echo "PROCESSING TO SEND LEAD AS FILE ATTACHMENT<br />";
					
					list ($subject,$body) = split("\n",$this->lead_body,2);
					list($headers,$body) = split("---------------",$body,2);
					$header = trim($header);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = trim(implode("\n",$body_parts));
					$this->lead_body = $body;

#					mail($lead_array["email"],$subject,trim($body),trim($headers));	

					# $this->myarray["xxx"]
					
					//-- HEADER ROW <<------------------------------- NOT UPDATED FOR THIS DATA SET
					$merge .= ucwords(ereg_replace(",","","quote id"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","received"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","first name"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","last name"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","phone"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","fax"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","email"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","company"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","address"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","city"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","state"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","zip"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","currently accept cards"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","order type"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","typical value"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","card volume"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","customer type"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","service type"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","time frame"));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","","comments"));
					$merge .= "\r\n";
					
					//-- DATA <<------------------------------- NOT UPDATED FOR THIS DATA SET
					$merge .= ucwords(ereg_replace(",","",$this->myarray["quote_id"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["received"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["first_name"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["last_name"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["phone"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["fax"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["email"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["company"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["address"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["city"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["state"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["zip"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["currently_accept_cards"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["receive_cc_orders"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["typical_value"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["card_volume"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["customer_type"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["service_type"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["time_frame"]));
					$merge .= ",";
					$merge .= ucwords(ereg_replace(",","",$this->myarray["comments"]));
					$merge .= "\r\n";


					$fileptr=fopen("tsv_data.tsv","w"); // open the file
					
					fputs($fileptr, $merge);
					
					fclose($fileptr); // closes the file at $fileptr.
					
					$fileatt = "tsv_data.tsv"; // Path to the file                  
					$fileatt_type = "application/octet-stream"; // File Type 
					$fileatt_name = "tsv_data" . $current_date . ".tsv"; // Filename that will be used for the file as the attachment
					$email_from = "no_reply@quote-it.com"; // Who the email is from 
					$email_subject = "Call Center Lead File $current_date"; // The Subject of the email 
					$email_txt = ""; // Message that the email has in it
					$email_to = $lead_array["email"]; // Who the email is to
					
					$headers = "From: ".$email_from;
					$file = fopen($fileatt,'rb'); 
					$data = fread($file,filesize($fileatt)); 
					fclose($file);
					
					$semi_rand = md5(time()); 
					$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
						
					$headers .= "\nMIME-Version: 1.0\n" . 
								"Content-Type: multipart/mixed;\n" . 
								" boundary=\"{$mime_boundary}\"";
								
					$email_message .= "This is a multi-part message in MIME format.\n\n" . 
									"--{$mime_boundary}\n" . 
									"Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
								   "Content-Transfer-Encoding: 7bit\n\n" . 
					$email_message . "\n\n";
					
					$data = chunk_split(base64_encode($data));
					
					$email_message .= "--{$mime_boundary}\n" . 
									  "Content-Type: {$fileatt_type};\n" . 
									  " name=\"{$fileatt_name}\"\n" . 
									  //"Content-Disposition: attachment;\n" . 
									  //" filename=\"{$fileatt_name}\"\n" . 
									  "Content-Transfer-Encoding: base64\n\n" . 
									 $data . "\n\n" . 
									  "--{$mime_boundary}--\n";
									  
					$ok = @mail($email_to, $email_subject, $email_message, $headers);
					#$ok = @mail("rob@irelocation.com", $email_subject, $email_message, $headers);
					
					if($ok) { 
					echo "File Attachment Email Sent.<br>"; 
					} else { 
					echo "Oops... an error was detected.  File not sent.<br>"; 
					}    

				}
				
				echo "HIT JUST BEFORE updateLeadIds " . $lead_array["lead_id"] . "<br />";
				$this->updateLeadIds($lead_array["lead_id"]);
			
			} else {
				mail("rob@irelocation.com","Failed to Format Transcribe Lead",print_r($this->myarray,true)."\n\n".$this->lead_body . "\n\nThis is being sent most likely because there is data missing from the Db.");		
			}
		}
		
	}
	
	


	
	
?>