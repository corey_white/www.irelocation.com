<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	if ($_SESSION['project']['usertype'] != "uber")
	{
		header("Location: login.php");
		exit();	
	}

	?>
	
	<table class="filterfont">
		<tr>
			<td colspan="2">
				Are you sure you want to delete all data?
			</td>
		</tr>
		<tr>
			<td>
				<a href='truncate.php'>YES</a>
			</td>
			<td>
				<a href='home.php'>NO</a>
			</td>
		</tr>
	</table>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php"; ?>