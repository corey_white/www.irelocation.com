<? 	
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	
	$three_months = date("Y-m-d",strtotime(" -3 months"));
	echo "\n<!--- $three_months --->\n";
	
	$sql = "	
		select  created as 'raw', id,'projects' as 'type',
		 substr(created,1,10) 'created', 
		substr(created,11,6) 'hour',
		title,description from marble.projects
		where created > '$three_months'
		UNION ALL
		select  created as 'raw',id,'tasks' as 'type',
		substr(created,1,10) 'created',
		substr(created,11,6) 'hour',
		title,description from  marble.tasks
		where created > '$three_months'
		UNION ALL
		select  created as 'raw',id,'notes' as 'type'
		,substr(created,1,10) 'created',
		substr(created,11,6) 'hour',
		title,note from marble.notes
		where created > '$three_months'
		UNION ALL
		select  created as 'raw',id, 'updates' as 'type',
		substr(created,1,10) 'created',
		substr(created,11,6) 'hour',
		concat('Update #',id),description from  marble.updates
		where created > '$three_months'
		order by created desc
		
	";		
	
	$rs = new mysql_recordset($sql);
	$last_date = "";
	
	echo "<table class='filterfont'><tr><td colspan='4'></tr>";
	$all = array();	
	while($rs->fetch_array())
	{
		extract($rs->myarray);
		switch($type)
		{
			case "projects":						
				$link = getProjectLink($id,$title);
			break;		
			case "notes":
				$link = printNoteLink($id,$title);
				break;
			case "tasks":			
				$link = getTaskLink($id,$title);
				break;		
			case "updates":			
				$link = getUpdateLink($id,$title);
				break;				
		}
		$text = wordwrap(substr($description,0,80),40,"<br/>");
		$all[$raw] = array("link" => $link,"text" => $text,"created" => $created,"raw" => $raw,"hour" => $hour);		
	}
	$keys = array_keys($all);
	rsort($keys);
	$last_date = "";
	foreach($keys as $k)
	{
		extract($all[$k]);
		if ($last_date != $created)
		{
			echo "<tr><td colspan='4'> &nbsp; </td></tr>";
			$last_date = $created;
			echo "<tr><td align='left'><strong>$created</strong></td><td colspan='2'>&nbsp;</td></tr>";
		}
		
		list($hour,$min) = split(":",$hour);
		$pm = false;
		if ($hour > 11)
		{
			$pm = true;
			if ($hour != 12) $hour -= 12;
			if ($hour == 0) $hour = 12;
		}
		$hour = "$hour:$min ";
		if ($pm) $hour .= " pm";
		else $hour .= " am";
		
		
		?>
			<tr>
				<td align="right"><?= $hour ?></td>
				<td><?= $link ?></td>
				<td><?= $text ?></td>
			</tr>							
		<?
	}
	
	
	echo "</table>";
?>