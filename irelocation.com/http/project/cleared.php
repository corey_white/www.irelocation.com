<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	if ($_SESSION['project']['usertype'] != "uber")
	{
		header("Location: login.php");
		exit();	
	}

	?>
	
	<table class="filterfont">
		<tr>
			<td colspan="2">
				All Data has been cleared.
			</td>
		</tr>
		<tr>
			<td>
				<a href='home.php'>Continue</a>
			</td>
			<td>
				&nbsp;
			</td>
		</tr>
	</table>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php"; ?>