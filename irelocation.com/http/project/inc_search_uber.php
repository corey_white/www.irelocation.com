<?
	//find and replace names/usernames
	$names["5"] = array("steve winum","swinum","steve","winum");
	$names["1"] = array("david haveman","dave haveman","haveman","dave","david");	
	$names["2,7"] = array("anderson");
	$names["2"] = array("mark anderson","mark");
	$names["7"] = array("toni anderson","toni");
	$names["3"] = array("katrina bailey","bailey","katrina");
	$names["4"] = array("vinny smith","vsmith","smith","vinny");
	$all = array();
	foreach($names as $n)
		foreach($n as $name)
			$all[] = $name;
	$all = array_unique($all);
	$flat_names = implode("|",$all);
	
	
	function findUserId($given)
	{
		global $names;
		$keys = array_keys($names);
		
		foreach($keys as $key)
			foreach($names[$key] as $name)
				if ($name == $given)
					return $key;	
	}
	
	$months = "jan|feb|mar|apr|may|jun|jul|aug|sept|oct|nov|dec";
	$month_array = split("\|",$months);
	
	$date_check = "(last week|today|yesterday|tomorrow|($months|[0-9][0-9]?). ?([0-9][0-9](th|st|rd)?),?. ?([0-9][0-9]([0-9][0-9])?))";
	
	$dates_regex = "/(task|tasks|project|projects)? ?(made|started|created|ending|finish|finished|finishing) ?([^by]|on|before|after|around)? ?".$date_check."?/";

	$created_by_assigned_to = "/(assigned to|given to|created by|made by) ?(ms |ms\. |miss |mr |mr\. |mrs |mrs\. )?($flat_names) (assigned to|given to|created by|made by)? ?(miss |mr |mr\. |mrs |mrs\. )?($flat_names)?/";
	
	$mods = array();
	$mods[""] = " = ";
	$mods["on"] = " = ";
	$mods["before"] = " < ";
	$mods["after"] = " > ";
	$mods["around"] = " special ";//...
	

	/*
	'1'	'dave'	'Dave Haveman'
	'2'	'mark'	'Mark Anderson'
	'3'	'katrina'	'Katrina Bailey'
	'4'	'vsmith'	'Vinnie Smith'
	'5'	'swinum'	'Steve Winum'
	'6'	'annac'	'Anna Costillo'
	'7'	'toni'	'Toni Anderson'
	'8'	'uber'	'Uber Admin'
	*/
	
	function keyDiff($f,$s)
	{
		$d = abs($f - $s);
		return $d;
	}
	


	
	$ubersearch = strtolower($ubersearch);
	$dates = array();
	if (preg_match($date_check,$ubersearch))
	{
		preg_match($dates_regex, $ubersearch,$dates);
		
		$ubersearch = str_replace($dates[0],"",$ubersearch);
	}	
	$people = array();	
	preg_match($created_by_assigned_to, $ubersearch, $people);
	
	$ubersearch = str_replace(array("about",$people[0]),"",$ubersearch);	
	
	$object = substr($dates[1],0,1);
	if ($object != "t" && $object != "p")
		$object = "x";
	
	//Pull out date value.
	if ($dates[5] != "" && $dates[6] != "" && $dates[8] != "")
	{
		$month = $dates[5];
		if (!is_numeric($month))
			$month = array_search($month_array)+1;
		$day = $dates[6];
		
		$year = $dates[8];
		if (strlen($year) == 2) $year = "20".$year;
		
		$date = $year."-".$month."-".$day;
	}
	//-----------------------
	
	//What is the action??
	if ($dates[2] == "created" || $dates[2] == "started" || $dates[2] == "made")
	{
		$created_date = $date;
		$mod = $mods[$dates[3]];
	}
	else if ($dates[2] == "finished" || $dates[2] == "finish" || $dates[2] == "finishing"  || $dates[2] == "ending")
	{
		$ending_date = $date;
		$mod = $mods[$dates[3]];
	}
	//--------------------------
		
	//keywords left
	$left = split("  ",$ubersearch);
	
	
	//People Involved
	$action = $people[1];
	if ($action == "assigned to" || $action == "given to")
		$action_1 = "creator ";
	else
		$action_1 = "assignedto ";
	$person_1 = findUserId($people[3]);
		
	$action = $people[5];
	if ($action == "assigned to" || $action == "given to")
		$action_2 = "creator ";
	else
		$action_2 = "assignedto ";
	$person_2 = findUserId($people[6]);
	/*
		Dates:
		Array
		(
			[0] => projects created on 12/25/2007
			[1] => projects
			[2] => created
			[3] => on
			[4] => 12/25/2007
			[5] => 12
			[6] => 25
			[7] => 
			[8] => 2007
		)
		People:
		Array
		(
			[0] => assigned to mrs. katrina bailey created by mr. dave haveman
			[1] => assigned to
			[2] => mrs. 
			[3] => katrina bailey
			[4] =>  bailey
			[5] => bailey
			[6] => created by
			[7] => mr. 
			[8] => dave haveman
			[9] =>  haveman
			[10] => haveman
		)
		left = all  security companies
	*/
	//all projects created on 12/25/2007 assigned to mrs. katrina bailey created by mr. dave about security companies
	
	if ($created_date != "")
	{
		//echo "$type, created $mod: $created_date. <br/>";	
		$sqls['t'][] = " t.created like '$created_date%' ";
		$sqls['p'][] = " p.created like '$created_date%' ";
	}
	else
	{
		//echo "$type, ending $mod: $ending_date. <br/>";
		$sqls['t'][] = " t.finishby like '$ending_date%' ";
		$sqls['p'][] = " p.finishby like '$ending_date%' ";
	}
	
	if ($person_1 != "")
	{
		if ($action_1 == "creator ")
		{
			$sqls['t'][] = " t.userid in ($person_1) ";
			$sqls['p'][] = " p.userid in ($person_1) ";
		}	
		else
		{
			$sqls['t'][] = " t.assignedto in ($person_1) ";
			//$assignedto = $person_1;		
			//do project assignment later
		}
	}
		
	if ($person_2 != "")
	{
		if ($action_2 == "creator ")
		{
			$sqls['t'][] = " t.userid in ($person_2) ";
			$sqls['p'][] = " p.userid in ($person_2) ";
		}	
		else
		{
			$sqls['t'][] = " t.assignedto in ($person_2) ";
			//$assignedto = $person_1;		
			//do project assignment later
		}
	}
			
	$sqls['t'][] = " t.description like '%$ubersearch%' ";
	$sqls['p'][] = " p.description like '%$ubersearch%' ";		
	
	$sqls['t'][] = " t.title like '%$ubersearch%' ";
	$sqls['p'][] = " p.title like '%$ubersearch%' ";		

	
?>