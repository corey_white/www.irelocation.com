<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";

	
	$User = new User($_SESSION['project']['id']);
	$any = false;
	
	$sql = $User->getTasksIMade();
	if ($sql != "")
	{
		echo "<div class='filterfont'>Tasks I Created:<br/>";
		getTasks($sql);
		echo "<br/><br/><br/>";
		$any = true;
	}	
	$sql = $User->getMyTasks();
	if ($sql != "")
	{
		echo "<div class='filterfont'>Tasks I am Assigned to:<br/>";			
		getTasks($sql);
		$any = true;
	}
	
	if (!$any)
		echo "<div class='filterfont'>You have no tasks associated with you.</div>";
	
	echo "<br/><br/>".printRssLink("tasks");
	
	include "skin_bottom.php";
?>