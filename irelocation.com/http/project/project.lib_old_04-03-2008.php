<?
/* 
************************FILE INFORMATION**********************
* File Name:  project.lib.php
**********************************************************************
* Description:  Main Code for the Projects site
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	2/28/08 - Added ID to the output for easier visual tracking
					Fixed table display
**********************************************************************
*/

	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	
	
	function _logaction($object,$action,$notes="")
	{
		if (defined(USER_ID))
			$userid = USER_ID;
		else
			$userid = "-1";//system
		
		$sql = " insert into marble.projectlog ".
				"(userid,logtime,object,action,notes) ".
				" values ".
				" ($userid,NOW(),'$object','$action','$notes')";
		$rs = new mysql_recordset($sql);
		$rs->close();
	}
	
	function seen($table,$id,$userid)
	{
		$sql = " update marble.objectreadby set seen = 1 where userid = $userid".
				" and objectid = $id and object = '$table' and seen = 0 ";
		$rs = new mysql_recordset($sql);
		$rs->close();
	}
	

	//returns email string for each person associated, ingoring the 
	//person whose id is the $user, if ($user) is given.
	
	function getEmail($id)
	{
		$sql = " select id,displayname,email from marble.users ".
				"where id = $id ";		
		$rs = new mysql_recordset($sql);
		$to = array();
		if($rs->fetch_array())
			return  $rs->myarray['displayname']." <".$rs->myarray['email'].">";
	}
	
	function associatedEmails($table,$id,$user="x")
	{
		//echo "$table<br/>";
		$ids = associated($table,$id);
		$sql = " select id,displayname,email from marble.users ".
				"where id in (".implode(",",$ids).") ";
		//echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$to = array();
		while($rs->fetch_array())
		{
			if ($user != "x" && $user == $rs->myarray['id']) continue;
			$to[] =  $rs->myarray['displayname']." <".$rs->myarray['email'].">";
		}
		
		if (count($to) > 0)
			$to = implode(",",$to);
		else
			$to = "";	
		return $to;
	}

	//return userid's of everyone associated with this object. (task, or project)	
	function associated($table,$id)
	{
		//echo "associated($table,$id)<br/>";
		$userids = array();
		if ($table == "tasks")
		{
			$sql = "select userid,assignedto from marble.tasks where id = $id ";
		//	echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$userids[] = $rs->myarray['userid'];
			$userids[] = $rs->myarray['assignedto'];			
		}
		else if ($table == "projects")
		{
			$sql = "select userid from marble.projects where id = $id ";
			//echo "<!--- $sql --->";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$userids[] = $rs->myarray['userid'];
			
			$sql = " select distinct(userid) from marble.assignedto ".
					"where projectid = $id ";
			//echo "<!--- $sql --->";
			$rs = new mysql_recordset($sql);
			while($rs->fetch_array())
				$userids[] = $rs->myarray['userid'];
		}
		
		return $userids;		
	}
	
	function printRemoveUserLink($uid,$pid)
	{		
		return "(<a href='".DOC_ROOT."removeuser.php?userid=$uid&projectid=$pid'>X</a>)";
	}
	
	function printCreateUpdateLink($pid,$name)
	{
		return "<a href='".DOC_ROOT."create/update.php?pid=$pid'>$name</a>";
	}
	
	function printCreateNoteLink($pid,$name)
	{
		return "<a href='".DOC_ROOT."create/note.php?pid=$pid'>$name</a>";
	}
	
	function printCancelLink($type,$id)
	{
		return "<a href='".DOC_ROOT."do_cancel.php?id=$id&type=$type'>(Cancel $type)</a>";
	}
	
	function printFinishLink($type,$id)
	{
		return "<a href='".DOC_ROOT."do_finish.php?id=$id&type=$type'>(Finish $type)</a>";
	}
	
	function printNoteLink($id,$name)
	{
		return "<a href='".DOC_ROOT."view/note.php?id=$id'>$name</a>";
	}
	
	function printUserLink($id,$name)
	{
		if ($name == USER_NAME)
			$f = "Me";
		else
			list($f,$l) = split(" ",$name,2);
		
		return "<a title='$name' href='".DOC_ROOT."view/user.php?id=$id'>$f</a>";
	}
	
	function getProjectWorkers($projectid)
	{
		$sql = " select u.id,u.displayname from marble.assignedto ".
				"as a join marble.users as u on a.userid = u.id ".
			   " where a.projectid = $projectid ";
		$data = array();
		$rs2 = new mysql_recordset($sql);
		while($rs2->fetch_array())
		{
			$data[$rs2->myarray['id']] = $rs2->myarray['displayname'];
		}
		
		return $data;
	}
	
	function getUpdateLink($id,$text)
	{
		return "<a href='".DOC_ROOT."view/update.php?id=$id'>$text</a>";
	}
	
	function getTaskLink($id,$text)
	{
		return "<a href='".DOC_ROOT."view/task.php?id=$id'>$text</a>";
	}
	
	
	function getProjectLink($id,$text)
	{
		return "<a href='".DOC_ROOT."view/project.php?id=$id'>$text</a>";
	}
	
	function getNotes($sql)
	{
		$rs = new mysql_recordset($sql);
		
		?>
			<table class='filterfont'>
				<tr>					
					<td>Project</td>					
					<td>Note Author</td>
					<td>Note title</td>
					<td>Note date</td>
				<tr>
				<tr><td colspan='4'><hr /></td></tr>
		<?
	
		while($rs->fetch_array())
		{
			extract($rs->myarray);						
			
			echo "<tr valign='top'>";				
				echo "<td>".getProjectLink($pid,$ptitle)."</td>";
				echo "<td>".printUserLink($userid,$displayname)."</td>";
				echo "<td>".printNoteLink($id,$title)."</td>";
				list($date,$time) = split(" ",$created,2);
				echo "<td title='$created'>$date</td>";
			echo "</tr>";		
		}
		echo "</table>";
	}
	
	function getTasks($sql)
	{
		$rs = new mysql_recordset($sql);
		
		?>
			<table class='filterfont'>
				<tr>					
					<td>Title</td>					
					<td>Created</td>
					<td>Creator</td>
					<td>Priority</td>
					<td>Assigned To</td>
					<td>Time Frame</td>
					<td>Finished</td>
					<td>Details</td>
				<tr>
				<tr><td colspan='8'><hr /></td></tr>
		<?
		
		while($rs->fetch_array())
		{
			extract($rs->myarray);						
			
			echo "<tr valign='top'>";		
			$star = "<font color='#FF0000'>*</font>";
			$new = ($new=="*")?$star." ":"";
			
			echo "<td>".$new.getTaskLink($id,wordwrap($title,25,"<br/>"))."</td>";
			
			$description = (strlen($description>30)?(substr($description,0,30)."..."):$description);
				list($date,$time) = split(" ",$created,2);
			
				list($fdate,$ftime) = split(" ",$finishby,2);
				
				echo "<td title='$created'>$date</td>";
				echo "<td>".printUserLink($cid,$creator_name)."</td>";
				echo "<td>$priority</td>";
				echo "<td>".printUserLink($aid,$aname)."</td>";
				
				if ($finished==BLANK_DATE)
					$finished = "No";
				else
					$finished = "Yes";
				
				echo "<td>".(($finished=="No")?formatDueIn($due_in):"---")."</td>";
				echo "<td>$finished</td>";
				$d = (strlen($description)>25)?(substr($description,0,22)."..."):($description);
			
			echo "<td>$d</td>";
			echo "</tr>";
		
		}
		echo "</table>";
	}
	
	function _printSortLink($text,$orderby)
	{	
		$t = array_pop(split("/",$_SERVER['PHP_SELF']));
		if ($orderby == $_REQUEST['ob'])
			$ddir = ("asc" == $_REQUEST['d'])?"desc":"asc";
		else
			$ddir = "asc";
		//$this_page = DOC_ROOT.$_SERVER['PHP_SELF'];
		return "<a href='$t?ob=$orderby&d=$ddir'>$text</a>";
	}
	
	function getProjects($sql)
	{
		
		$order = array();
		$order[] = "p.title";
		$order[] = "p.created";
		$order[] = "p.userid";
		$order[] = "p.priority";		
		$order[] = "p.finishby";
		$order[] = "p.description";
		
		$ob = $_REQUEST['ob'];
		$d = $_REQUEST['d'];
		
		if ($d == "" || $d == "-")
			$d = "desc";
		else
			$d == "asc";
		
		if ($ob == "")
			$ob = "p.priority";
		else
			$ob = $order[$ob];
		
		$sql .= " order by $ob $d";		
		$rs = new mysql_recordset($sql);
		
		?>
			<table class="filterfont" cellpadding="2" width="1000"  style="border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;">
				<tr align="left" style="background-color: lightgrey;">					
					<th width="50">ID</th>
					<th width="100"><?= _printSortLink("Title",0) ?></th>
					<th width="75"><?= _printSortLink("Created",1) ?></th>
					<th width="75"><?= _printSortLink("Creator",2) ?></th>
					<th width="75"><?= _printSortLink("Priority",3) ?></th>
					<th width="100">Assigned To</th>								
					<th width="75"><?= _printSortLink("Time Frame",4) ?></th>
					<td width="75">Finished</td>		
					<th width="100"><?= _printSortLink("Details",5) ?></th>
				<tr>
				<!-- <tr><td colspan="9"><hr /></td></tr> -->
		<?
		
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			
			$data = getProjectWorkers($id);
			$star = "<font color='#FF0000'>*</font>";
			$new = ($new=="*")?$star." ":"";
			echo "<tr valign=\"top\" style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\">";			
			
				echo "<td>$id</td>";
				echo "<td>".$new.getProjectLink($id,wordwrap($title,25,"<br/>"))."</td>";
			
			$description = (strlen($description>30)?(substr($description,0,30)."..."):$description);
			list($date,$time) = split(" ",$created,2);
			
				
				echo "<td title='$created'>$date</td>";
				echo "<td>".printUserLink($cid,$creator_name)."</td>";
				echo "<td>$priority</td>";
				
			echo "<td>";
				foreach($data as $id => $name)
					echo printUserLink($id,$name)."<br/>";					
			echo "</td>";
			
			if ($finished==BLANK_DATE)
				$finished = "No";
			else
				$finished = "Yes";
			
			echo "<td>".(($finished=="No")?formatDueIn($due_in):"---")."</td>";
			echo "<td>$finished</td>";
			$d = (strlen($description)>25)?(substr($description,0,100)."..."):($description);
			
			echo "<td>$d</td>";
			echo "</tr>";
		
		}
		echo "</table>";
	}
	
	function formatDueIn($due_in)
	{
		if ($due_in < 0)
			return "<font color='#ff0000'>".(-1*$due_in)." day(s) late</font>";
		else if ($due_in == 0)
			return "<font color='#FF9900'>Due Today</font>";
		else if ($due_in == 1)
			return "<font color='#FF9900'>1 Day Left!</font>";
		else
			return "<font color='#009900'>$due_in days left</font>";	
	}
	
	class General 
	{
		var $loaded = -1;
		var $data;	
		var $table = "";
		function General($id="x")
		{
			$this->data = array();		
			if (is_array($id))
			{
				$this->data = $id;				
			}
			else
				$this->load($id);	
		}
		
		function mysql($sql)
		{
			//echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			return $rs;
		}
		
		function load($id)
		{
			if ($loaded == -1 || $loaded < time()-60)
			{
				$loaded = time();
				$sql = "select * from ".DATABASE.".".$this->table." where id = '".$id."' ";
				$rs = $this->mysql($sql);
				$rs->fetch_array();
				$this->data = $rs->myarray;
				$rs->close();
			}
		}				
		
		function udpate()
		{
			$sql = "udpate ".DATABASE.".".$this->table." set ";
			$keys = array_keys($data);
			
			foreach($keys as $key)
			{
				if (is_numeric($key)) continue;
				if ($key == "id") continue;
				
				$sql .= " $key = '".$data[$key]."', ";
			}
			$sql = substr($key,0,-2)." where id = '".$data['id']."'; ";
			
			$rs = $this->mysql($sql);
			$id = $rs->last_insert_id();
			$rs->close();
		}
		
		function create($data)
		{
			$sql = "insert into ".DATABASE.".".$this->table." set ";
			$keys = array_keys($data);
			
			foreach($keys as $key)
			{
				if (is_numeric($key)) continue;
				$sql .= " $key = '".$data[$key]."', ";
			}
			$sql = substr($key,0,-2);
			
			$rs = $this->mysql($sql);
			$id = $rs->last_insert_id();
			$rs->close();
			
			return $id;
		}
		
		function setStatus($status)
		{
			if ($this->data['id'] != "")
			{
				$sql = "udpate ".DATABASE.".".$this->table." set active = ".
						(($status)?"1":"0")." where id = '".$this->data['id']."'; ";
						
				$rs = $this->mysql($sql);
				$id = $rs->last_insert_id();
				$rs->close();			
			}		
		}		
	}

	function UserLogin($username,$password)
	{
		$sql = "select id from ".DATABASE.".users where username = '$username' ".
				" and password = PASSWORD('$password') limit 1";
		//echo "$sql <br/>";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$id = $rs->myarray['id'];
			$rs->close();
			
			$sql = "update ".DATABASE.".users set lastlogin = NOW() where id = '$id' ";
			//echo "$sql <br/>";
			$rs = new mysql_recordset($sql);
			$rs->close();
			
			
			return new User($id);
		}
		else
			return FALSE;
	}

	class User extends General
	{		
		function User($id="x")
		{
			$this->table = "users";
			if (is_array($id))
				$this->data = $id;
			else
			{		
				$this->load($id);
			}
		}		
		
		function getNewItems()
		{
			/*
			$sql = "select object, count(*) 'count' from marble.objectreadby ".
					"where userid = ".$this->data['id']." and not seen group by object;";
			*/
			
			$sql = "select o.object, count(o.objectid) 'count' from marble.objectreadby as o join marble.projects as p  on o.objectid = p.id join marble.notes as n on p.id = n.projectid 
where not o.seen and p.active and o.userid = ".$this->data['id']." and 
(
(o.objectid = p.id and o.object = 'projects') 
or 
(n.projectid = p.id and o.object = 'notes')
)
group by o.object
UNION ALL
select  o.object, count(o.objectid) 'count' from marble.objectreadby as o join marble.tasks as p on o.objectid = p.id and o.object = 'tasks'
where not o.seen  and p.active and o.userid = ".$this->data['id']."  group by o.object;";
			
			
			$rs = $this->mysql($sql);
			while($rs->fetch_array())
			{
				$data[$rs->myarray['object']] = $rs->myarray['count'];
			}
			
			return $data;
		}
		
		//Tasks i created.
		function getTasksIMade($active=true)
		{
			$sql = "select count(*) 'count' from ".DATABASE.".tasks where ".
					" userid = ".$this->data['id'];
			if ($active)
				$sql .= " and active ";
			//echo "<!--- $sql --->";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$count = $rs->myarray['count'];
			if ($count != 0)
			{
				$sql = "select 
							if(o.seen=1,'','*') 'new',	
							t.*,
							DATEDIFF(t.finishby,now()) 'due_in', 
							u.id as 'cid',
							u.displayname as 'creator_name', 
							au.id as 'aid', 
							au.displayname as 'aname' 
						from
							marble.tasks as t 
							join 
							marble.users as u on t.userid = u.id 
							join 
							marble.users as au 
							on t.assignedto = au.id and t.userid = ".
							$this->data['id']."
							left join marble.objectreadby as o on o.objectid = t.id
							and o.object = 'tasks'
							and o.userid = t.assignedto
						where		
							(o.objectid = t.id and o.object = 'tasks')
							and 
							(
								t.finished = '".BLANK_DATE."'
								or 
								t.finished > DATE_ADD(NOW(),INTERVAL -2 day)
							) 
							";
				if ($active)
					$sql .= " and active ";
				$sql .= " group by t.id ";
			//	echo " $sql --->";
				return $sql;
			}
			else
				return "";
		}
					
		//Tasks im assigned to.
		function getMyTasks($active=true)
		{
			$sql = "select distinct(id) id from ".DATABASE.".tasks ".
					"where assignedto = ".$this->data['id'];
			if ($active)
				$sql .= " and active ";	
					
			//echo "<!--- $sql --->";
			$rs = $this->mysql($sql);			
			if($rs->fetch_array())				
			{
				//echo "<!--- tasks found. ";
				$sql = "select 
							if(o.seen=1,'','*') 'new',	
							t.*,
							DATEDIFF(t.finishby,now()) 'due_in', 
							u.id as 'cid',
							u.displayname as 'creator_name', 
							au.id as 'aid', 
							au.displayname as 'aname' 
						from
							marble.tasks as t 
							join 
							marble.users as u on t.userid = u.id 
							join 
							marble.users as au 
							on t.assignedto = au.id and t.assignedto = ".
							$this->data['id']."
							left join marble.objectreadby as o on o.objectid = t.id
							and o.object = 'tasks'
							and o.userid = t.assignedto
						where		
							(o.objectid = t.id and o.object = 'tasks')
							and 
							(
								t.finished = '".BLANK_DATE."' 
								or 
								t.finished > DATE_ADD(NOW(),INTERVAL -2 day)
							)
					 ";
				if ($active)
					$sql .= " and active ";
				$sql .= " group by t.id ";
				//echo " $sql --->";
			}
			else
				$sql = "";
			
			return $sql;
		}
						
		
		//projects i created.
		function getProjectsIMade($active=true)
		{
			$sql = "select count(*) 'count' from ".DATABASE.".projects where ".
					" userid = ".$this->data['id'];
			if ($active)
				$sql .= " and active ";
			//echo "<!--- $sql --->";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$count = $rs->myarray['count'];
			if ($count != 0)
			{
				/*
				$sql = " select p.*,DATEDIFF(p.finishby,now()) 'due_in', ".
				" u.id as 'cid', u.displayname as 'creator_name' ".
				"from ".DATABASE.".projects as p join ".DATABASE.".users as u ".
				" on p.userid = u.id where p.userid = ".$this->data['id'];
				if ($active)
					$sql .= " and p.active ";
				*/
				$sql = 
				"select 
					if(o.seen=1,'','*') 'new',
					p.*,
					DATEDIFF(p.finishby,now()) 'due_in', 
					u.id as 'cid',
					u.displayname as 'creator_name'
				from 
					marble.projects as p 
					join marble.users as u 	on p.userid = u.id 
					left join marble.objectreadby as o on o.objectid = p.id 
					left join marble.notes as n on p.id = n.projectid
				where 
					o.userid = ".$this->data['id']."
					and 
					(
					(o.objectid = p.id and o.object = 'projects') 
					or 
					(n.projectid = p.id and o.object = 'notes')
					)
					and 
					(
						p.finished = '".BLANK_DATE."'
						or 
						p.finished > DATE_ADD(NOW(),INTERVAL -2 day)
					)
					and p.userid = ".$this->data['id']." ";
				if ($active)
					$sql .= " and p.active ";						
				$sql .= " group by p.id ";
				//echo "<!--- $sql --->";
				return $sql;
			}
			else
				return "";
		}
		
		//projects im assigned to.
		function getMyProjects($active=true)
		{
			$sql = "select distinct(projectid) projectid from ".DATABASE.".assignedto ".
					"as a join ".DATABASE.".projects as p on p.id = a.projectid ".
					"where a.userid = ".$this->data['id'];
			if ($active)
				$sql .= " and p.active ";
			//echo "<!--- $sql --->";
			$rs = $this->mysql($sql);
			$pids = array();
			while($rs->fetch_array())
				$pids[] = $rs->myarray['projectid'];
			if (count($pids) > 0)
			{
				/*
				$sql = " select p.*,DATEDIFF(p.finishby,now()) 'due_in', u.id as 'cid',".
						" u.displayname as 'creator_name' ".
						"from ".DATABASE.".projects as p join ".DATABASE.".users as u ".
						" on p.userid = u.id where p.id ";
				*/
				$sql = 
				"select 
					if(o.seen=1,'','*') 'new',
					p.*,
					DATEDIFF(p.finishby,now()) 'due_in', 
					u.id as 'cid',
					u.displayname as 'creator_name'
				from 
					marble.projects as p 
					join marble.users as u 	on p.userid = u.id 
					left join marble.objectreadby as o on o.objectid = p.id 
					left join marble.notes as n on p.id = n.projectid
				where 
					o.userid = ".$this->data['id']." 
					and 
					(
					(o.objectid = p.id and o.object = 'projects') 
					or 
					(n.projectid = p.id and o.object = 'notes')
					)
					and 
					(
						p.finished = '".BLANK_DATE."' 
						or 
						p.finished > DATE_ADD(NOW(),INTERVAL -2 day)
					)
					and p.id ";
				if (count($pids) == 1)
					$sql .= " = ".$pids[0];
				else
					$sql .= " in (".implode(",",$pids).") ";	
				if ($active)
					$sql .= " and p.active ";						
				$sql .= " group by p.id ";				
			}
			else
				$sql = "";
			//echo "<!--- $sql --->";
			return $sql;
		}
		
		function create($username, $password, $email, $type)
		{
			$sql = "insert into ".DATABASE.".".$this->table." ".
					" (username, password, email, type) values ".
					" ('$username',password('$password'),'$email','$type');";
			
			$this->data['username'] = $username;
			$this->data['email'] = $email;
			$this->data['type'] = $type;			
			$rs = $this->mysql($sql);
			$id = $rs->last_insert_id();
			$this->data['id'] = $id;
			$rs->close();				
			
			return $id;	
		}		
	}
	
	class Project extends General
	{		
		var $Notes = array();
		var $Creator = "";
		var $AssignedTo = array();			
	
		function Project($id="x")
		{
			$this->table = "projects";
			parent::General($id);			
		}		
		
		function getUpdates()
		{
			$sql = "select u.id,u.description from marble.projects as p join ".
					" marble.assignedto as a on a.projectid = p.id join ".
					" marble.updates as u on u.assignedtoid = a.id where ".
					" p.id = ".$this->data['id'].";";
			$rs	= $this->mysql($sql);			
			$updates = array();
			while($rs->fetch_array())
				$updates[$rs->myarray['id']] = $rs->myarray['description'];
			
			return $updates;
		}
		
		function getNoteData()
		{
			$sql =  "select id,created,note 'text' from ".DATABASE.".notes where ".
					"projectid = ".$this->data['id']." order by created asc";
			$rs	= $this->mysql($sql);			
			$notes = array();
			while($rs->fetch_array())
			{
				$notes[] = array("id" => $rs->myarray['id'],
								"created" => $rs->myarray['created'],
								"text" => $rs->myarray['text']);
			}
			return $notes;
		}
		
		function load($id="x")
		{			
			$loaded = time();
			$sql = "select * from ".DATABASE.".".$this->table." where id = '".$id."' ";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->data = $rs->myarray;
			$rs->close();
								
			//load project notes.
			$sql = "select id from  ".DATABASE.".notes where ".
					"projectid = '".$this->data['id']."' ";
			$rs = $this->mysql($sql);
			while($rs->fetch_array())
				$this->Notes[] = $rs->myarray['id'];		
			
			
			//load users.
			$sql = "select u.id,u.displayName from ".DATABASE.".assignedto as a ".
					" join ".DATABASE.".users as u on u.id = a.userid where ".
					" projectid = '".$this->data['id']."' ";					
			$rs = $this->mysql($sql);
			while($rs->fetch_array())
				$this->AssignedTo[$rs->myarray['id']] = $rs->myarray['displayName'];
			
			
			//load creator;
			$sql = "select u.id,u.displayName from ".DATABASE.".users u where ".
					" id = ".$this->data['userid'];
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->Creator = new User($rs->myarray);
		}
		
	}
	
	class Note extends General
	{		
		function Note($id="x")
		{
			$this->table = "notes";
			$this->load($id);
		}		
		
		function load($id)
		{
			$loaded = time();
			$sql = "select * from ".DATABASE.".".$this->table." where id = '".$id."' ";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->data = $rs->myarray;
			$rs->close();
		
			//load creator;
			$sql = "select * from ".DATABASE.".users u where ".
					" id = ".$this->data['userid'];
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->Creator = new User($rs->myarray);
		}
	}
	
	class Task extends General
	{		
		var $Creator;
		var $AssignedTo;
	
		function Task($id="x")
		{
			$this->table = "tasks";
			$this->load($id);
		}		
		
		function load($id="x")
		{			
			$loaded = time();
			$sql = "select * from ".DATABASE.".".$this->table." where id = '".$id."' ";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->data = $rs->myarray;
			$rs->close();
			
			$sql = "select * from ".DATABASE.".users where id = ".
					$this->data['assignedto'].";";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->AssignedTo = new User($rs->myarray);
			
			$sql = "select * from ".DATABASE.".users where id = ".
					$this->data['userid'].";";
			$rs = $this->mysql($sql);
			$rs->fetch_array();
			$this->Creator = new User($rs->myarray);			
		}
	}
	
	//print_r($user);
?>