<?
/* 
************************FILE INFORMATION**********************
* File Name:  clickbooth_leads.php
**********************************************************************
* Description:  emails clickbooth leads to 
**********************************************************************
* Creation Date:  5/21/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOTE:  THIS PAGE DOESN'T EXIST IN THE NUADMIN DIR, IT'S IN IRELOCATION.COM/PROJECT/CLICKBOOTH_LEADS.PHP
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('Ymd',strtotime("$dateinput"));
	return $newDate;
}
// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}

$today = date('m/d/Y',strtotime("now"));




if ( $view_cb ) {  //-- if submitted, process
	
	$sql_date = dateYearFirst($ts);
	
	#echo "RUNNING $sql_date<br /><br />";
	
	$sql = "select received, source, name1, name2, phone1, address, city, state_code, zip from irelocation.leads_security where left(received,8) = '$sql_date' and ( campaign = 'usalarm' || campaign = 'atac' || campaign = 'ctac' ) and source like '%clkbth%' ";
	$rs = new mysql_recordset($sql);

	while ($rs->fetch_array()) {
		$received = $rs->myarray["received"];
		$source = $rs->myarray["source"];
		$name1 = $rs->myarray["name1"];
		$name2 = $rs->myarray["name2"];
		$phone1 = $rs->myarray["phone1"];
		$address = $rs->myarray["address"];
		$city = $rs->myarray["city"];
		$state_code = $rs->myarray["state_code"];
		$zip = $rs->myarray["zip"];
		
		$email_body .= "Received: $received \n";
		$email_body .= "Source: $source \n";
		$email_body .= "Name: $name1 $name2 \n";
		$email_body .= "Phone: $phone1 \n";
		$email_body .= "Address: \n";
		$email_body .= "$address \n";
		$email_body .= "$city $state_code, $zip \n";
		$email_body .= "\n\n";
		
		$body .= "Received: $received <br />";
		$body .= "Source: $source <br />";
		$body .= "Name: $name1 $name2 <br />";
		$body .= "Phone: $phone1 <br />";
		$body .= "Address: <br />";
		$body .= "$address <br />";
		$body .= "$city $state_code, $zip <br />";
		$body .= "<br /><br />";
		
	}
	
	#mail("swinum@irelocation.com","Click Booth Leads $ts","$email_body","BCC: dev@bwolf.com");
	

} else { //-- display form
    
	
	if ( !$ts ) {
		$ts = $today;
	} 
	
	$body .= "<script language=\"JavaScript\" src=\"../nuadmin/js/ts_picker4.js\"></script>";
	$body .= "<p align=\"center\">Enter Date to view ClickBooth Data.</p>";
	$body .= "<form name=\"viewCB\" action=\"clickbooth_leads.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div>
	<fieldset>
	<legend><b>Date</b></legend>
	<input type=\"text\" name=\"ts\" value=\"$ts\" readonly><a href=\"javascript:show_calendar4('document.viewCB.ts', document.viewCB.ts.value);\"><img src=\"../nuadmin/js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
	</fieldset>
	</div>
	
	
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Get Data</legend>
	<input name=\"view_cb\" type=\"submit\" value=\"View CB\">
	</fieldset>
	</div>
	
	</div>
	
	</form>";

    
}

echo $body;

?>