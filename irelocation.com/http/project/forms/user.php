<? 
	include "../lib.php";
	include "form.lib.php"; 
	include "../../inc_mysql.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden"  name="type" value="user" />
		<input type="hidden" name="action" value="<?= $action ?>" />		
	<table>
		<tr>
			<td colspan="2">Create a User</td>
		</tr>
		<tr>
			<td>Display Name</td>
			<td><input type="text" name="displayname" /></td>
		</tr>
		<tr>
			<td>Username</td>
			<td><input type="text" name="username" value="<?= $username ?>" /></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" value="<?= $password ?>" /></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" name="email" value="<?= $email ?>" /></td>
		</tr>
		<tr>
			<td>Type:</td>
			<td><? getUserTypes("usertype",$usertype); ?></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Create Task"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
