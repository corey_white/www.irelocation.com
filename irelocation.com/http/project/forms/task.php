<? 
	include "../lib.php";
	include "form.lib.php"; 
	include "../../inc_mysql.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Untitled Document</title>
</head>

<body>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden" name="created" value="<?= date("Y-m-d H:i:s") ?>" />
		<input type="hidden"  name="type" value="task" />
		<input type="hidden" name="action" value="create" />
		<input type="hidden" name="userid" value="<?= $_SESSION['userid'] ?>" />
	<table>
		<tr>
			<td colspan="2">Create a Task</td>
		</tr>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea name='description'></textarea></td>
		</tr>
		<tr>
			<td>Finish By</td>
			<td><? printCalendarPopup("taskform","finishby") ?></td>
		</tr>
		<tr>
			<td>Assign To:</td>
			<td><? getUserDropDown("assignedto"); ?></td>
		</tr>
		<tr>
			<td>Priority</td>
			<td><? getPriorities(); ?></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Create Task"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
