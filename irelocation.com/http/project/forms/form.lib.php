<?
	
	function getUserTypes($name="type",$selected="x")
	{
		echo "<select name='$name'>";
			echo "<option value='admin' ".(($selected=="admin")?"selected ":"").">".
					"admin</option>";
			echo "<option value='employee' ".(($selected=="employee")?"selected ":"").">".
					"employee</option>";
		echo "</select>";
	}
	
	function printCalendarPopup($form,$field)
	{
		echo '<input type="text" name="'.$field.'" value="" size="10" maxlength="10" onClick=\'javascript:window.open("calendar.php?form='.$form.'&field='.$field.'","","top=50,left=400,width=175,height=140,menubar=no,toolbar=no,scrollbars=no,resizable=no,status=no"); return false;\'>';		

		echo " <select name='hour'>";
		
		for($h = 1; $h < 13; $h++)
		{						
			echo "<option value='$h'>$h</option>";
		}
		echo "</select>";
		echo ":";
		echo "<select name='minute'>";		
		echo "<option value='00'>00</option>";
		echo "<option value='15'>15</option>";
		echo "<option value='30'>30</option>";
		echo "<option value='45'>45</option>";
		echo "</select>";
		echo "<select name='ampm'><option value='0'>AM</option>".
 				"<option value='12'>PM</option></select>";
	}
	
	function getPriorities($selected="3",$allownull=false)
	{		
		echo "<select name='priority'>\n";
		if ($allownull)
			echo "<option value='*'>All</option>";
			
		for ($i = 1; $i < 6; $i++)
		{
			echo "<option value='$i' ";
			if ($i == $selected)
				echo "selected ";
			echo ">$i</option>\n";					
		}
		echo "</selected>";
	}
	
	function getUserDropDown($name="userid",$type="x",$selected="x")
	{
		$sql = "select id,displayname from ".DATABASE.".users ";
		if ($type != "x")
			$sql .= " where type = '$type' ";
			
		$rs = new mysql_recordset($sql);
		echo "<select name='$name'>\n";
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<option value='$id' ";
			if ($id == $selected)
				echo "selected ";
			echo ">".ucwords($displayname)."</option>\n";					
		}
		echo "</selected>";
	}
	


?>