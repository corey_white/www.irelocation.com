<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	

	extract($_REQUEST);
	
	if ($type != "" && is_numeric($id))
	{
		//get title.
		$sql = "select title from marble.$type where id = $id ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$title = $rs->myarray['title'];		
	
		$sql = "update marble.".$type." set active = 0 where id = $id ";
		$rs = new mysql_recordset($sql);		
		$rs->close();
		
		_logAction("user","updated $type","user:".USER_ID." canceled $type:$id");
		
		$subject = ucwords(substr($type,0,-1)." Cancelation");
		$to = associatedEmails($type,$id,USER_ID);
		mailWrapper($to,$subject,"$type '$title' has been canceled by ".USER_NAME);		
		
		header("Location: ".$type.".php");
		exit();
	}
	else
	{
		header("Location: home.php");
		exit();
	}
?>