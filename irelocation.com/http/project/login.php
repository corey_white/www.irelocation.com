<?
	if ($_SERVER['REMOTE_ADDR'] == "72.201.234.217") { //-- block Steve's IP
		exit;
	}
	session_start();

	if ($_SESSION['projects']['usertype'] != "")
	{
		header("Location: home.php");
		exit();
	}
	
	if ($_REQUEST['goto'] != "" && $_REQUEST['id'] != "")
	{
		$goto = $_REQUEST['goto'];
		$id = $_REQUEST['id'];
		$forward = true;				
	}
	

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login - iRelocation Projects</title>
<link rel='stylesheet' type='text/css' href='marble.css' />
</head>
<body>
<div align="center">
	<br/><br/><br/><br/>
	<form action="do_login.php" method="post">
		<? if ($forward) { ?>
			<input type='hidden' name="goto" value="<?= $goto ?>" />
			<input type='hidden' name="id" value="<?= $id ?>" />
		<? } ?>
		<table >
			<Tr class="off">
				<th colspan="2" align="right" class="off">iRelocation Projects</th>
			</Tr>
			<tr><Td colspan="2">&nbsp;</Td></tr>
			<tr class="off">
				<td align="left">Username</td>
				<td><input type='text' name="username" /></td>
			</tr>
			<tr class="off">
				<td align="left">Password</td>
				<td><input type='password' name="password" /></td>
			</tr>
			<Tr class="off">
				<td colspan="2" align="right"><input type='submit' value="Login" /></td>
			</Tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>

				<td colspan="2" align="right">
					<a style="color:#0000FF; text-decoration:none; 
								font-family:Verdana; font-size:11px;" 
						href='password_recovery.php'>Password Recovery</a>
				</td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>			
		</table>
	</form>
</div>
</body>
</html>
