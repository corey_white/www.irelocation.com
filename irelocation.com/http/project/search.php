<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/create/form.lib.php";	
	
	
	
	extract($_REQUEST);
	if ($dosearch == "1")
		_logaction("search","searched",USER_ID." ran a search.");
	
?>
	<form name="taskform" action="search.php">
		<input type="hidden" name="dosearch" value="1" />
		<input type="hidden" name="objects" value="projects" />
		<table class='filterfont'>
		<tr>
			<td colspan="4" align="center">Search for Something.</td>
		</tr>
		<tr><td colspan="4">&nbsp;</td></tr>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" value="<?= $title ?>" /></td>
			<td>Description</td>
			<td>
				<input type="text" name="description" value="<?= $description ?>" />
			</td>
		</tr>
		<tr>
			<td>Priority</td>
			<td>
				<? getPriorities(($priority=="")?0:$priority,true); ?>				
			</td>
			<td>Finish By</td>
			<td>
				<? printCalendarPopup("taskform","finishby",true) ?>				
			</td>
		</tr>
		<tr>
			<td>Finished?</td> 
			<td><input type="radio" name="finished" value="yes"  
					<?= ($finished=="yes")?"checked='checked'":" " ?>/>yes
				<input type="radio" name="finished" value="no"
					<?= ($finished=="no")?"checked='checked'":" " ?>/>no
				<input type="radio" name="finished" value="x"
					<?= ($finished=="x" || $finished == "")?"checked='checked'":" " ?>/>both
			</td>
			<td>Project Status:</td>
			<td>
				<input type="radio" name="active" checked="checked" value="1"
					<?= ($finished=="1")?"checked='checked'":" " ?>/>active
				<input type="radio" name="active" value="0"
					<?= ($finished=="0")?"checked='checked'":" " ?>/>inactive
				<input type="radio" name="active" value="x"
					<?= ($finished=="x" || $finished == "")?"checked='checked'":" " ?>/>both
			</td>
		<tr>		
		<tr>
			<td>Created by:</td>
			<td><? getUserDropDown("creator",$assignedto); ?></td>
			<td>Assigned To:</td>
			<td><? getUserDropDown("assignedto",$assignedto); ?></td>
		</tr>
		<tr><td colspan="4">&nbsp;</td></tr>
		<? /*
		<tr>
			<td>&nbsp;</td>
			<td>Uber Search:</td>
			<td colspan="2"><input type="text" name="ubersearch" value="<?= $ubersearch ?>" /></td>
		</tr>		
		<tr><td colspan="4">&nbsp;</td></tr>
		*/ ?>
		<tr>
			<td colspan="4" align="center">
				<input type="submit" name='submit_button' value="Find items"/>
				&nbsp; &nbsp; &nbsp;				
				<input type="submit" name='submit_button' value="I'm Feeling Lucky"/>			
			</td>
		</tr>
	</table>
	</form>
	
<? 
	if ($_REQUEST['dosearch'] == 1) 
		include "inc_search.php";
?>
	
</body>
</html>
