<?
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
		
	$sql = 
		"select 'projects' as 'type', id from marble.projects where ".
		"finishby > now() AND finishby < DATE_ADD(NOW(),INTERVAL 2 DAY) ".
		" and finished = '".BLANK_DATE."' ".
		"UNION ALL ".
		"select 'tasks' as 'type', id from marble.tasks where ".
		"finishby > now() AND finishby < DATE_ADD(NOW(),INTERVAL 2 DAY)".
		" and finished = '".BLANK_DATE."' ";

	$rs = new mysql_recordset($sql);
	
	while($rs->fetch_array())
	{
		if ($rs->myarray['type'] == 'tasks')
			mailTasks($rs->myarray['id']);
		else
			mailProjects($rs->myarray['id']);		
	}
	
	function mailProjects($id)
	{
		$Project = new Project($id);
		extract($Project->data);
		
		$to = associatedEmails("projects",$id);
		echo "\n<br/>Title: $title<br/>\n";
		print_r($to);
		$msg = "Hello!\n\nProject $title is coming due soon and is not ".
				"currently marked as finished. ";
		$subject = "Project Deadline Approaching";
		
		//mailWrapper($to,$subject,$msg);
	}
	
	function mailTasks($id)
	{
		$Task = new Task($id);
		extract($Task->data);
		echo "\n<br/>Title: $title<br/>\n";
		$to = associatedEmails("tasks",$id);
		$msg = "Hello!\n\nTask $title is coming due soon and is not ".
				"currently marked as finished. ";
		$subject = "Task Deadline Approaching";
		print_r($to);
		//mailWrapper($to,$subject,$msg);	
	}
?>