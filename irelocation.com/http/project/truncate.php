<?
	session_start();
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	
	if ($_SESSION['project']['usertype'] != "uber")
	{
		header("Location: login.php");
		exit();	
	}

	function backupTable($table)
	{
		$sql = "select * from $table ";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{		
			$data = "insert into $table (";
			$array_keys = array_keys($rs->myarray);
			foreach($array_keys as $key)
			{
				if (is_numeric($key)) continue;
				$data .= "$key,";
			}
			$data = substr($data,0,-1).") values ";
			do
			{
				$data .= "(";
				foreach($array_keys as $key)
				{
					if (is_numeric($key)) continue;
					$data .= "'".$rs->myarray[$key]."',";
				}
				$data = substr($data,0,-1)."),\n";
			}
			while($rs->fetch_array());
					
			$data = substr($data,0,-2);
			return $data;
		}
		else
			return array("no data.");
	}

	$sql[] = "marble.projectlog";
	$sql[] = "marble.assignedto";
	$sql[] = "marble.objectreadby";
	$sql[] = "marble.notes";
	$sql[] = "marble.projects";
	$sql[] = "marble.tasks";

	$backups = array();
	
	
	foreach($sql as $s)
	{
		$d = backupTable($s);
		$backups[] = $d;		
		$rs = new mysql_recordset("truncate ".$s);
		$rs->close();
	}
	
	mailWrapper("david@irelocation.com","iRelo Project Backup",print_r($backups,true));
	header("Location: cleared.php");
	exit();	
	
?>