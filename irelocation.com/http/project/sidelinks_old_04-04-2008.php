<?	
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	$files['admin']['home.php'] = "Home";
	$files['admin'][0] = 0;
	$files['admin']['create/project.php'] = "Create a Project";
	$files['admin']['create/user.php'] = "Create a User";
	$files['admin']['create/assign.php'] = "Assign a User";
	$files['admin'][1] = 0;
	$files['admin']['projects.php'] = "All Projects";
	$files['admin']['search.php'] = "Search Projects";
	$files['admin'][2] = 0;
	$files['admin']['myprojects.php'] = "My Projects";
	$files['admin'][3] = 0;
	$files['admin']['finishedprojects.php'] = "Finished Projects";
	$files['admin']['finishedtasks.php'] = "Finished Tasks";
	$files['admin'][4] = 0;
	$files['admin']['timeline.php'] = "Timeline";
	$files['admin'][5] = 0;
	$files['admin']['password.php'] = "Change Info";
	$files['admin']['password_recovery.php'] = "Reset Password";
		
	$files['admin']['logout.php'] = "Logout";	

	$files['employee']['home.php'] = "Home";
	$files['employee'][0] = 0;
	$files['employee']['create/project.php'] = "Create a Project";
	$files['employee']['create/assign.php'] = "Assign a User";
	$files['employee'][1] = 0;
	$files['employee']['myprojects.php'] = "My Projects";
	$files['employee'][2] = 0;
	$files['employee']['projects.php'] = "All Projects";
	$files['employee']['search.php'] = "Search Projects";
	$files['employee'][3] = 0;
	$files['employee']['finishedprojects.php'] = "Finished Projects";
	$files['employee']['finishedtasks.php'] = "Finished Tasks";
	$files['employee'][4] = 0;
	$files['employee']['timeline.php'] = "Timeline";
	$files['employee'][5] = 0;
	$files['employee']['password.php'] = "Change Info";	
	$files['employee']['password_recovery.php'] = "Reset Password";
	$files['employee']['logout.php'] = "Logout";	
	
	$files['uber']['search.php'] = "Search Projects";
	$files['uber']['projects.php'] = "All Projects";
	$files['uber']['notes.php'] = "All Notes";
	$files['uber']['2'] = 0;
	$files['uber']['finishedprojects.php'] = "Finished Projects";
	$files['uber']['finishedtasks.php'] = "Finished Tasks";
	$files['uber'][3] = 0;
	$files['uber']['timeline.php'] = "Timeline";
	$files['uber'][5] = 0;
	$files['uber']['verify.php'] = "Clear Data";
	$files['uber']['password.php'] = "Change Info";	
	$files['uber']['password_recovery.php'] = "Reset Password";
	$files['uber']['logout.php'] = "Logout";	

	function getPageTitle($files)
	{
		$a = split("/",$_SERVER['SCRIPT_NAME']);
		//echo "<!--- ".print_r($a,true)." --->";
		if (count($a) == 4)
		{
			$file = array_pop($a);
			$folder = array_pop($a);
			$file = "$folder/$file";
		}
		else
			$file = array_pop($a);
		
		$title = $files[$_SESSION['project']['usertype']][$file];
		
		if ($title != "")
			return $title;
		
		$a = split("/",$_SERVER['SCRIPT_NAME']);
		$file = substr(array_pop($a),0,-4);
		return ucwords($file);
	}

	function printLinks($links)
	{
		$docroot = "http://www.irelocation.com/project/";
	
		$a = split("/",$_SERVER['SCRIPT_NAME']);
		//print_r($a);
		$file = array_pop($a);	
		$root = "http://www.irelocation.com/project/";
		echo "<font face='Verdana' size='2'>";
		echo "<a class='white' href='".$root."view/user.php?id=".USER_ID."' >".USER_NAME."</a><br/><br/>";
		
		$User = new User($_SESSION['project']['id']);
		$new = $User->getNewItems();
		
		if (count($new) > 0)
		{
			foreach($new as $table => $count)
			{
				if ($table == "tasks")
					echo STAR."<a class='white' href='".$root."mytasks.php'>".
							"$count new $table.</a><br/>";
				else
					echo STAR."<a class='white' href='".$root."myprojects.php'>".
							"$count new $table.</a><br/>";			
			}
			echo "<Br/>";
		}
		if ($_SESSION['project']['usertype'] != "") 
		{ 
			foreach($links[$_SESSION['project']['usertype']] as $page => $title)
			{
				if (is_numeric($page))
				{
					echo "<br/>"; 
					continue;
				}
				if ($file != $page)
					echo "<a class='white' href='".$docroot.$page."'> - $title</a><br/>";
				else 
					echo "<span class='white_selected'> - $title</span><br/>";			
			}
			echo "<br/>";
		} 	
		echo "</font>";
	}

?>