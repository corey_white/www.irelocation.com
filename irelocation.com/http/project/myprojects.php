<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";

	
	$User = new User($_SESSION['project']['id']);
	$any = false;

	$sql = $User->getProjectsIMade();
	if ($sql != "")
	{
		echo "<div class='filterfont'>Projects I Created:<br/>";
		getProjects($sql);
		echo "<br/><br/><br/>";
		$any = true;
	}	
	$sql = $User->getMyProjects();
	if ($sql != "")
	{
		echo "<div class='filterfont'>Projects I am Assigned to:<br/>";			
		getProjects($sql);
		$any = true;
	}	
	
	if (!$any)
		echo "<div class='filterfont'>You have no projects associated with you.</div>";
	
	echo "<br/><br/>".printRssLink("projects");
	
	include "skin_bottom.php";
?>