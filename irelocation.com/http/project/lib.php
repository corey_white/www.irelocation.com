<?
	define(DATABASE,"marble");	
	define(DOC_ROOT,"http://www.irelocation.com/project/");
	define(BLANK_DATE,"0000-00-00 00:00:00");
	define(FROM_EMAIL," 'Project Bot' <project@irelocation.com>");
	define(EMAIL_FOOTER,"\n\nMesssage Sent from iRelocation Projects");
	define(EMAIL_SUBJECT_PREFIX,"[iRelo Project] - ");
	define(STAR,"<span class='white_selected'>*</span>");
	define(PASSWORD_CONFIRM,1);
	define(INVALID_PASSWORD,2);
	define(PASSWORD_TOO_SHORT,3);
	define(PASSWORD_NOT_CHANGED,4);
	define(EMAIL_TOO_SHORT,5);


	function getAid($pid,$uid)
	{
		$sql = "select id from marble.assignedto where projectid = $pid ".
				" and userid = $uid ;";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray['id'];
	}
	
	function countProjects($active=true)
	{
		$sql = "select count(*) 'count' from marble.projects ";
		if ($active)
			$sql .= " where active ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray['count'];
	}
	
	function printRssLink($type="x")
	{ 
		$label = ($type=="x")?"projects and tasks":$type;
	?>
		<br/>
		<a href='http://www.irelocation.com/project/rss.php?userid=<?= USER_ID."&type=".$type ?>' class="filterfont">
			<img src="images/rss.jpg" width="16" height="16" border="0" />
			RSS Feed for your <?= $label ?>.
		</a>				
	<? 
	}
	
	function countTasks($active=true)
	{
		$sql = "select count(*) 'count' from marble.tasks ";
		if ($active)
			$sql .= " where active ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray['count'];
	}
	
	function mailWrapper($to,$subject,$message,$from=FROM_EMAIL)
	{
		mail($to,EMAIL_SUBJECT_PREFIX.$subject,$message.EMAIL_FOOTER,"From: $from");
		//mail("david@irelocation.com","DEBUG-".EMAIL_SUBJECT_PREFIX.$subject,
		//		$message.EMAIL_FOOTER,"From: $from");
	}	
?>