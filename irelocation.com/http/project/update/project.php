<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";		
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/update/form.lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";

	$pid = $_REQUEST['id'];
	
	$Project = new Project($pid);
	extract($Project->data);
	
	
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden" name="updated" value="<?= date("Y-m-d H:i:s") ?>" />
		<input type="hidden"  name="type" value="projects" />
		<input type="hidden"  name="id" value="<?= $pid ?>" />
		<input type="hidden" name="userid" value="<?= $_SESSION['project']['id'] ?>" />
	<table class='filterfont'>
		<? if ($_REQUEST['error'] != "")
		{
			echo "<tr><td colspan='2'><font color='#FF0000'>";
			echo "<strong>Please fill out Field: </strong>".$_REQUEST['error'];
			echo "</font></td></tr>";
		} ?>
		<tr>
			<td colspan="2">Update this Project</td>
		</tr>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" value="<?= $title ?>" /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea name='description'><?= $description ?></textarea></td>
		</tr>
		<tr>
			<td>Priority</td>
			<td><? getPriorities($priority); ?></td>
		</tr>
		<tr>
			<td>Finish By</td>
			<td><? printCalendarPopup("taskform","finishby",$finishby) ?></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Update Project"/></td>
		</tr>
	</table>
	</form>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php"; ?>