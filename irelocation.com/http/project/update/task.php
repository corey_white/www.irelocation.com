<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";		
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/update/form.lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";

	$tid = $_REQUEST['id'];
	
	$Task = new Task($tid);
	extract($Task->data);
	
	
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden" name="updated" value="<?= date("Y-m-d H:i:s") ?>" />
		<input type="hidden"  name="type" value="tasks" />
		<input type="hidden"  name="id" value="<?= $tid ?>" />
		<input type="hidden" name="userid" value="<?= USER_ID ?>" />
	<table class='filterfont'>
		<? if ($_REQUEST['error'] != "")
		{
			echo "<tr><td colspan='2'><font color='#FF0000'>";
			echo "<strong>Please fill out Field: </strong>".$_REQUEST['error'];
			echo "</font></td></tr>";
		} ?>
		<tr>
			<td colspan="2">Update this Task</td>
		</tr>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" value="<?= $title ?>" /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td><textarea name='description'><?= $description ?></textarea></td>
		</tr>
		<tr>
			<td>Priority</td>
			<td><? getPriorities($priority); ?></td>
		</tr>
		<tr>
			<td>Assign To:</td>
			<td><? getUserDropDown("assignedto",$assignedto); ?></td>
		</tr>
		<tr>
			<td>Finish By</td>
			<td><? printCalendarPopup("taskform","finishby",$finishby) ?></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Update Task"/></td>
		</tr>
	</table>
	</form>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php"; ?>