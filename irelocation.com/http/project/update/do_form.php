<?
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	$keys = array_keys($_POST);
	foreach($keys as $k)
	{
		if (is_numeric($k)) continue;
		$_SESSION['project']['create'][$k] = $_POST[$k];
	}
	
	$type = $_POST['type'];
	unset($_POST['type']);
	
	switch($type)
	{
		case "projects":						
			doProject();
			break;		
		case "tasks":			
			doTask();
			break;			
		case "users":
			doUser();
			break;
		case "assign";
			doAssignment();
		default:
			echo $type;
	}
	
	function _changeFinishBy()
	{
		$finishby = $_POST['finishby']." ".($_POST['hour']+$_POST['ampm']).":"
					.$_POST['minute'].":00";
		$_POST['finishby'] = $finishby;
		unset($_POST['hour'],$_POST['ampm'],$_POST['minute']);
	}
	
	function update($table,$log="")
	{	
		$sql = " update ".DATABASE.".$table set ";
		foreach($_POST as $f => $v)
		{
			if ($f != "password" && $f != "id")
				$sql .= " $f = '".trim($v)."', ";
			else if ($f == $password)
				$sql .= " password = PASSWORD('".trim($v)."'), ";
		}
		$sql = substr($sql,0,-2)." where id = ".$_POST['id'];
		$rs = new mysql_recordset($sql);
		$id = $rs->last_insert_id();
		if ($log == "")
			$log = "object $id was updated.";
		
		_logAction($table,"updated",$log);
		return $id;
	}
	
	function validate($type)
	{
		extract($_POST);
		if ($type == "projects" || $type == "tasks")
		{

			if (strlen($title) < 1)
				return array(0,"title");
			
			if (strlen($description) < 1)
				return array(0,"description");
			
			$now = date("Y-m-d H:i:s");
			if ($finishby < $now)
				return array(0,"finishby");					
				
			return array(1,"valid");
		}
		else
		{
			if (strlen($title) < 1)
				return array(0,"title");
			
			if (strlen($note) < 1)
				return array(0,"note");
			return array(1,"valid");			
		}				
	}
	
	function doTask()
	{
		$type = "tasks";
		$goto = "task.php";
		$id = $_POST['id'];
		$_POST['title'] = trim(str_replace("'","\'",$_POST['title']));
		$_POST['description'] = trim(str_replace("'","\'",$_POST['description']));
		_changeFinishBy();
		$result = validate($type);
		if ($result[0] == 1)
		{
			$log = "task #$id updated:\ntitle:".$_POST['title']."\ndescription:".$_POST['description']."\nFinish By:".$_POST['finishby'];
			update($type,$log);
			
			header("Location: ../view/$goto?id=$id");
			exit();
		}
		else
		{
			header("Location: $goto?id=$id&error=".$result[1]);
			exit();
		}	
				
	}
	
	function doProject()
	{
		$type = "projects";
		$goto = "project.php";
		$id = $_POST['id'];
		$_POST['title'] = trim(str_replace("'","\'",$_POST['title']));
		$_POST['description'] = trim(str_replace("'","\'",$_POST['description']));
		_changeFinishBy();
		$result = validate($type);
		if ($result[0] == 1)
		{
			$log = "project #$id updated:\ntitle:".$_POST['title']."\ndescription:".$_POST['description']."\nFinish By:".$_POST['finishby'];
			update($type,$log);
			
			header("Location: ../view/$goto?id=$id");
			exit();
		}
		else
		{
			header("Location: $goto?id=$id&error=".$result[1]);
			exit();
		}	
				
	}
	
?>
