<?
	
	function getUserTypes($name="usertype",$selected="x")
	{
		echo "<select name='$name'>";
			echo "<option value='admin' ".(($selected=="admin")?"selected ":"").">".
					"admin</option>";
			echo "<option value='employee' ".(($selected=="employee")?"selected ":"").">".
					"employee</option>";
		echo "</select>";
	}
	
	function getProjectDropDown($name="projectid",$selected="x")
	{
		$sql = "select id,title from ".DATABASE.".projects ";
		
		$rs = new mysql_recordset($sql);
		echo "<select name='$name'>\n";
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<option value='$id' ";
			if ($id == $selected)
				echo "selected ";
			echo ">".ucwords($title)."</option>\n";					
		}
		echo "</selected>";
	}
	
	function printCalendarPopup($form,$field,$default="x")
	{
		if ($default != "x")
		{
			list($date,$time) = split(" ",$default,2);
			list($hours,$minutes,$seconds) = split(":",$time);
			
			echo "<!--- $date $time $hours $minutes $seconds --->";
			
			if ($hours > 12)
			{
				$pm = true;
				$hours -= 12;				
			}
			else if ($hours == 12)
				$pm = true;
			else if ($hours == 0)
			{
				$pm = false;
				$hours = 12;
			}
		}
	
		echo '<input type="text" name="'.$field.'" value="'.$date.'" size="10" maxlength="10" onClick=\'javascript:window.open("../calendar/calendar.php?form='.$form.'&field='.$field.'","","top=50,left=400,width=175,height=140,menubar=no,toolbar=no,scrollbars=no,resizable=no,status=no"); return false;\'>';		

		echo " <select name='hour'>";
		
		for($h = 1; $h < 13; $h++)
		{						
			echo "<option value='$h' ".(($hours==$h)?"selected ":" ").">$h</option>";
		}
		echo "</select>";
		echo ":";
		echo "<select name='minute'>";		
		echo "<option value='00' ".(($minutes=="00")?"selected ":"").">00</option>";
		echo "<option value='15' ".(($minutes=="15")?"selected ":"").">15</option>";
		echo "<option value='30' ".(($minutes=="30")?"selected ":"").">30</option>";
		echo "<option value='45' ".(($minutes=="45")?"selected ":"").">45</option>";
		echo "</select>";
		echo "<select name='ampm'><option value='0' ".((!$pm)?"selected ":"").">AM</option>".
 				"<option value='12' ".(($pm)?"selected ":"").">PM</option></select>";
	}
	
	function getPriorities($selected="3")
	{		
		echo "<select name='priority'>\n";
		for ($i = 1; $i < 6; $i++)
		{
			echo "<option value='$i' ";
			if ($i == $selected)
				echo "selected ";
			echo ">$i</option>\n";					
		}
		echo "</selected>";
	}
	
	function getUserDropDown($name="userid",$selected="x")
	{
		$sql = "select id,displayname from ".DATABASE.".users ";
			
		$rs = new mysql_recordset($sql);
		echo "<select name='$name'>\n";
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<option value='$id' ";
			if ($id == $selected)
				echo "selected ";
			echo ">".ucwords($displayname)."</option>\n";					
		}
		echo "</selected>";
	}
	


?>