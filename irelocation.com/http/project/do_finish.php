<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	

	extract($_REQUEST);
	
	if ($type != "" && is_numeric($id))
	{
		//get title.
		$sql = "select title from marble.$type where id = $id ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$title = $rs->myarray['title'];		
	
		$sql = "update marble.".$type." set finished = NOW() where id = $id ";
		$rs = new mysql_recordset($sql);		
		$rs->close();
		_logAction("user","updated $type","user:".USER_ID." finished $type:$id");
		
		$to = associatedEmails($type,$id,USER_ID);
		mailWrapper($to,"Task Completion","$type '$title' has been finished by ".USER_NAME);
		
		
		header("Location: view/".substr($type,0,-1).".php?id=$id");
		exit();
	}
	else
	{
		header("Location: home.php");
		exit();
	}
?>