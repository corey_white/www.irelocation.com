<? 
	session_start();	
	
	$id = $_REQUEST['id'];
	if ($id == "")
	{
		header("Location: ../projects.php");
		exit();
	}
	$table = "notes";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	seen($table,$id,USER_ID);
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		

	//_logaction($table,"viewed","object $id was viewed.");
	
	
	
	$Note = new Note($id);

		
	echo "<!---".print_r($Note,true)."--->\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Note - <?= ucwords($Note->data['title']) ?></title>
</head>

<body>
	<table class='filterfont'>
		<tr>
			<td colspan='2'>
				<h2>Title: <?= ucwords($Note->data['title']) ?></h2>
			</td>
		</tr>
		<?
			list($d,$t) = split(" ",$Note->data['created']);
	
		?>
		<tr>
			<td>Created By:</td>
			<td><?=  printUserLink($Note->Creator->data['id'],$Note->Creator->data['displayname']) ?> on <?= $d ?></td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan='2'>Note:</td>
		</tr>
		<tr>
			<td colspan='2'><?= wordwrap($Note->data['note'],60,"<br/>") ?></td>
		</tr>
	</table>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";	?> 