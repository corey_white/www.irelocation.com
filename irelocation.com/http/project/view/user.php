<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	$id = $_REQUEST['id'];
	if ($id == "")
	{
		header("Location: ../projects.php");
		exit();
	}
	
	$User = new User($id);
	
	$myprojects = $User->getMyProjects();
	$projectsimade = $User->getProjectsIMade();

	$mytasks = $User->getMyTasks();
	$tasksimade = $User->getTasksIMade();	
	
	
	echo "<!---".print_r($User,true)."--->\n";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>User - <?= ucwords($User->data['displayname']) ?></title>
</head>

<body>
	<table class='filterfont'>
		<tr>
			<td colspan='2' align="left">
				<h2>User: <?= ucwords($User->data['displayname']) ?></h2>	
			</td>
		</tr>
		<tr>
			<td width='25%' align="left"><strong>Username</strong></td>
			<td width='*' align="left"><?= $User->data['username'] ?></td>
		</tr>
		<tr>
			<td align="left"><strong>Last Login:</strong></td>
			<td align="left"><?= ($User->data['lastlogin']=="")?"Never":$User->data['lastlogin'] ?>
		</tr>
		<tr>
			<td align="left"><strong>User Type:</strong></td>
			<td align="left"><?= $User->data['usertype'] ?>
		</tr>
		<tr>
			<td align="left"><strong>Email</strong></td>
			<td align="left"><?= $User->data['email'] ?>
		</tr>

	<? if ($myprojects != "") { 
		echo "<tr><td align='left' colspan='2'>&nbsp;</td></tr>";
		echo "<tr><td colspan='2'><strong>My Projects</strong></td></tr>";
		echo "<tr><td colspan='2'>";
		getProjects($myprojects);
		echo "</td></tr>";
	} 
	if ($projectsimade != "") { 
		echo "<tr><td colspan='2'>&nbsp;</td></tr>";
		echo "<tr><td align='left' colspan='2'><strong>Projects I Made</strong></td></tr>";
		echo "<tr><td colspan='2'>";
		getProjects($projectsimade);
		echo "</td></tr>";
	} 
	
	if ($mytasks != "") { 
		echo "<tr><td align='left' colspan='2'>&nbsp;</td></tr>";
		echo "<tr><td colspan='2'><strong>My Tasks</strong></td></tr>";
		echo "<tr><td colspan='2'>";
		getTasks($mytasks);
		echo "</td></tr>";
	}
	
	if ($tasksimade != "") { 
		echo "<tr><td align='left' colspan='2'>&nbsp;</td></tr>";
		echo "<tr><td colspan='2'><strong>Tasks I Made</strong></td></tr>";
		echo "<tr><td colspan='2'>";
		getTasks($tasksimade);
		echo "</td></tr>";
	}
	
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";	
?> 	