<? 
	session_start();	
	$id = $_REQUEST['id'];
	if ($id == "")
	{
		header("Location: ../tasks.php");
		exit();
	}
	$table = "tasks";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	seen($table,$id,USER_ID);
	//_logaction($table,"viewed","object $id was viewed.");
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	
	$Task = new Task($id);
	
	$sql = "select '>' as 'dir', t.id from marble.tasks t where id > $id ".
			"and assignedto = 1 limit 1 ".
			" UNION ALL ".
			"select '<' as 'dir', t.id from marble.tasks t where id < $id ".
			" and assignedto = 1 order by id desc limit 2;";
	$nav = array();
	$rs2 = new mysql_recordset($sql);
	while($rs2->fetch_array())
		$nav[$rs2->myarray['dir']] = $rs2->myarray['id'];
	$rs2->close();
	
	$aid = $Task->AssignedTo->data['id'];
	$aname = $Task->AssignedTo->data['displayname'];
?>
	<table class='filterfont'>
		<tR>
			<td colspan='2'>
				<? if ($nav['<'] != "") { ?>
					<a href='task.php?id=<?= $nav['<'] ?>'>&lt;--</a>
				<? } ?>
				<a href='../mytasks.php'>Task</a>: <?= ucwords($Task->data['title']) ?>
				
				<? if ($nav['>'] != "") { ?>
					<a href='task.php?id=<?= $nav['>'] ?>'>--&gt;</a>
				<? } ?>
			</td>
		</tR>		
		<tr><td colspan="2">&nbsp;</td></tr>
		<?	
			$cid = $Task->Creator->data['id'];
			list($cdate,$ctime) = split(" ",$Task->data['created']);
			list($fdate,$ftime) = split(" ",$Task->data['finishby']);
			$cname = $Task->Creator->data['displayname'];
		?>
		<tr>
			<td><strong>Created By</strong></td>
			<td><?=  printUserLink($cid ,$cname) ?> 
				on 
				<?= $cdate ?>
			</td>
		</tR>
		<tr>
			<td><strong>Finish By</strong></td>
			<td><?= $fdate ?></td>
		</tR>
		<?
			$finished = (($Task->data['finished']!=BLANK_DATE)?"Yes":"No");
			
		?>
		<tr>
			<td><strong>Finished</strong></td>
			<td><?= $finished ?>
				<? 
					if($finished=="No"&&USER_ID==$aid) 
						echo printFinishLink("tasks",$id); 
					else if ($finished=="No"&USER_ID==$cid)
						echo printCancelLink("tasks",$id); 
				?>
			</td>
		</tR>
		<tr>
			<td><strong>Priority</strong></td>
			<td><?= $Task->data['priority'] ?></td>
		</tR>
		
	<? if (strlen($Task->data['updated'])) { ?>
		<tr>
			<td><strong>Last Updated:</strong></td>
			<td><?= $Task->data['updated'] ?></td>
		</tr>		
	<? } ?>
		<tr>
			<td><strong>Assigned To</strong></td>
			<td><?=  printUserLink($aid,$aname) ?></td>
		</tr>
		<? if (USER_ID == $cid) 
		{ ?>			
			<tr><td colspan="2">
				<a href='../update/task.php?id=<?= $id ?>'>
					Update Project
				</a>	
			</td></tr>	
		<? } ?>	
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2"><strong>Description</strong></td>			
		</tR>		
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td colspan="2">
				<?= wordwrap($Task->data['description'],35,"<br/>") ?>
			</td>
		</tR>
	</table>		
	<? if ($aid == USER_ID) { 
		$to_name = $cname;
		$to_id = $cid;
		$type = "tasks";
		$title = ucwords($Task->data['title']);
		include "../inc_email_form.php";	
	} ?>
		

</body>
</html>
