<? 	
	session_start();	
	$id = $_REQUEST['id'];
	if ($id == "")
	{
		header("Location: ../projects.php");
		exit();
	}
	
	$table = "projects";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	seen($table,$id,USER_ID);
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";		
	//_logaction($table,"viewed","object $id was viewed.");
	

	
	$Project = new Project($id);
	
	echo "<!--- Project $id --->";
	
	$cid = $Project->Creator->data['id'];
	$pid = $Project->data['id'];
	$cname = $Project->Creator->data['displayName'];
	
	$sql = "select '>' as 'dir', p.id from marble.projects as p 
join marble.assignedto as a on p.id = a.projectid
where p.id > $id and a.userid = ".USER_ID." and active limit 1
 UNION ALL 
select '<' as 'dir', p.id from marble.projects as p 
join marble.assignedto as a on p.id = a.projectid
where p.id < $id and a.userid = ".USER_ID." and active limit 2";

	echo "\n<!--- $sql --->\n";

	$nav = array();
	$rs2 = new mysql_recordset($sql);
	while($rs2->fetch_array())
		$nav[$rs2->myarray['dir']] = $rs2->myarray['id'];
	$rs2->close();
	
	echo "<!---".print_r($Project->Creator->data,true)."--->\n";
?>


	<table class='filterfont'>
		<tr>
			<td valign="top" colspan='2'>
				<? if ($nav['<'] != "") { ?>
					<a href='project.php?id=<?= $nav['<'] ?>'>&lt;--</a>
				<? } ?>
				
				<a href='../myprojects.php'>
					Project
				</a>: 
				<strong>
					<?= ucwords($Project->data['title']) ?>
				</strong>
				
				<? if ($_SESSION['project']['id'] == $cid) 
				{ ?>				
					<a href='../update/project.php?id=<?= $pid ?>'>
						alter
					</a>						
				<? } ?>
				
				<? if ($nav['>'] != "") { ?>
					<a href='project.php?id=<?= $nav['>'] ?>'>--&gt;</a>
				<? } ?>
				
			</td>
		</tr>
		<tr>
			<td valign="top"><strong>Description:</strong></td>
			<td><?= wordwrap($Project->data['description'],60,"<br/>") ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Created By:</strong></td>
			<td><?=  printUserLink($cid,$cname) ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Created On:</strong></td>
			<td><?= $Project->data['created'] ?></td>
		</tr>
		<tr>
			<td valign="top"><strong>Finish By:</strong></td>
			<td><?= $Project->data['finishby'] ?></td>
		</tr>
		<?
			$finished = (($Project->data['finished']!=BLANK_DATE)?"Yes":"No");
			
		?>
		<tr>
			<td valign="top"><strong>Finished:</strong></td>
			<td><?= $finished ?>
				<? 
					if($finished=="No") 
						echo printFinishLink("projects",$id); 
					else if ($finished=="No"&USER_ID==$cid)
						echo printCancelLink("projects",$id); 
				?>
			</td>
		</tR>
		<tr>
			<td><strong>Priority:</strong></td>
			<td><?= $Project->data['priority'] ?></td>
		</tr>
		<? if (strlen($Project->data['updated'])) { ?>
		<tr>
			<td><strong>Last Updated:</strong></td>
			<td><?= $Project->data['updated'] ?></td>
		</tr>
		<? } ?>
		<tr><td colspan="2">&nbsp;</td></tr>		
		<tr>
			<td colspan='2'><strong>Assigned To:</strong></td>
		</tr>				
	<? if (count($Project->AssignedTo) > 0 )
	{ 
		$aids = array();
		foreach($Project->AssignedTo as $id => $name)
		{	
			$aids[] = $id;
			echo "<tr><td>&nbsp;</td><td>";
			echo printUserLink($id,$name)." ";
			if ($cid == USER_ID)
				echo printRemoveUserLink($id,$pid);
			echo "</td></tr>";
		}
		?>

		<tr><td>&nbsp;</td><td>
		<a href='../create/assign.php?projectid=<?= $pid ?>'>
			Assign user
		</a>
		</td></tr>

	<? } 
	else 
	{ ?>
		No Current Assignments<Br/>
		
		<tr><td>&nbsp;</td><td>
		<a href='../create/assign.php?projectid=<?= $pid ?>'>
			Assign user
		</a>
		</td></tr>		
	<? }
	
	$notes = $Project->getNoteData();
	$updates = $Project->getUpdates();
	if (count($aids) > 0)
		$aid = implode(",",$aids);
	else
		$aid = "";
	echo "<tr><td colspan='2'>&nbsp;</td></tr>";
	echo "<tr><td colspan='2'>Updates:</td></tr>";
	if (count($updates) > 0)
	{		
		$i = 1;
		foreach($updates  as $id => $text)
		{
			
			echo "<tr><td colspan='2'>";
			if ($id == $_REQUEST['update'])
				echo "<font color='#FF0000'>";
			echo "$i) ".wordwrap($text,50,"<br/>")."</td></tr>";		
			if ($id == $_REQUEST['update'])
				echo "</font>";
			$i++;
		}
	}
	else
		echo "<tr><td colspan='2'>&nbsp; &nbsp;none yet.</td></tr>";
	
	if (substr_count($aid,USER_ID) > 0)
	{
		echo "<tr><td colspan='2'>";
		echo printCreateUpdateLink($pid,"Add an Update");
		echo "</td></tr>";
	}	
	else
		echo "<tr><td colspan='2'>&nbsp;</td></tr>";
	
	
	if (count($notes) > 0)
	{
		echo "<tr><td colspan='2'>&nbsp;</td></tr>";
		echo "<tr><td colspan='2'>Notes:</td></tr>";
		foreach($notes as $n)
		{
			extract($n);
			list($date,$time) = split(" ",$created,2);
			echo "<tr><td valign='top'>".printNoteLink($id,$date)."</td>";
			echo "<td>".wordwrap($text,50,"<br/>")."</td></tr>";		
		}
	}
	
	echo "<tr><td colspan='2'>";
	echo printCreateNoteLink($pid,"Add a Note");
	echo "</td></tr>";
	echo "</table>";

	if (count($aids) > 0)
	{				
		$type = "projects";
		$id = $pid;
		$title = ucwords($Project->data['title']);
		
		if (substr_count($aid,USER_ID) > 0) 
		{ 
			$to_name = $cname;
			$to_id = $cid;			
			include "../inc_email_form.php";
		} 	
		else if ($cid == USER_ID)//author
		{			
			$to_name = "Assigned Workers";
			$to_id = "*";			
			include "../inc_email_form.php";
		}
		
	}
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";	
?>
