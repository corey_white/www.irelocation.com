<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	$timeframe = time() - $_SESSION['project']['logintime'];
	
	_logaction("users","logout","user was logged in for $timeframe seconds.");
	unset($_SESSION['project']);	
	header("Location: login.php");
?>