<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	$userid = $_REQUEST['userid'];
	$projectid = $_REQUEST['projectid'];
	
	
	if (is_numeric($userid) && is_numeric($projectid))
	{
		$sql = " delete from marble.assignedto where projectid = $projectid ".
				" and userid = $userid ";
		$rs = new mysql_recordset($sql);
		
		$sql = " delete from marble.objectreadby where id = $projectid ".
				" and userid = $userid and object = 'projects' ";
		$rs = new mysql_recordset($sql);	
		
		_logaction("projects","user removed",
					"user:$userid was removed from project:$projectid");
		
		$to = associatedEmails("projects",$projectid,USER_ID);
		$msg = "User #$userid has been removed from projectid# $projectid ";
		mailWrapper($to,"User Removal",$msg);
		
		
		$to = getEmail($userid);
		$msg = "You have been removed from projectid# $projectid by ".
				USER_NAME;
		mailWrapper($to,"User Removal",$msg);
		
		header("Location: view/project.php?id=$projectid");		
	}
	else
	{
		header("Location: view/projects.php");					
	}
?>