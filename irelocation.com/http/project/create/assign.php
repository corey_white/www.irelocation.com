<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/create/form.lib.php";	

	
	$projectid = $_REQUEST['projectid'];
	
	if (countProjects() == 0)
	{
	?> 
		<table class="filterfont">
			<tr>
				<td>
					There are currnently no active projects. <br/>
					click <a href='project.php'>here</a> to create one.
				</td>
			</tr>
		</table>
	<?
		exit();
	}
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden"  name="type" value="assign" />
	<table class="filterfont">
		<tr>
			<td colspan="2">Assign a User to A Project</td>
		</tr>
		<tr>
			<td>Project:</td>
			<td><? getProjectDropDown("projectid",$projectid) ?></td>
		</tr>
		<tr>
			<td>Assign To:</td>
			<td><? getUserDropDown(); ?></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Create Assignment"/></td>
		</tr>
	</table>
	</form>
<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";	?>
