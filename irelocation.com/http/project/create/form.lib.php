<?
	
	function getUserTypes($name="usertype",$selected="x")
	{
		echo "<select name='$name'>";
			echo "<option value='admin' ".(($selected=="admin")?"selected ":"").">".
					"admin</option>";
			echo "<option value='employee' ".(($selected=="employee")?"selected ":"").">".
					"employee</option>";
		echo "</select>";
	}
	
	function getProjectDropDown($name="projectid",$selected="x")
	{
		$sql = "select id,title from ".DATABASE.".projects ";
		
		$rs = new mysql_recordset($sql);
		echo "<select name='$name'>\n";
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<option value='$id' ";
			if ($id == $selected)
				echo "selected ";
			echo ">".ucwords($title)."</option>\n";					
		}
		echo "</selected>";
	}
	
	function printCalendarPopup($form,$field,$allownull=false)
	{
		
		if (is_array($_SESSION['project']['create']))
		{
			$today = $_SESSION['project']['create']['finishby'];
			$hour = $_SESSION['project']['create']['hour'];
			$min = $_SESSION['project']['create']['minute'];
			$ampm = $_SESSION['project']['create']['ampm'];
		}
		else 
		{
			if (!$allownull)
			{
				$today = date("Y-m-d");
				$hour = date("H");
				$min = 0;		
				$ampm = 0;
				if ($hour >= 12)
				{
					$ampm = 12;
					if ($hour > 12)
						$hour -= 12;
				}			
			}
		}

			
		/*
		echo '<input type="text" name="'.$field.'" value="'.$today.'" size="10" maxlength="10" onClick=\'javascript:window.open("http://irelocation.com/project/calendar/calendar.php?form='.$form.'&field='.$field.'","","top=50,left=400,width=175,height=140,menubar=no,toolbar=no,scrollbars=no,resizable=no,status=no"); return false;\'>';		
		*/
		

		echo '<input type="text" name="'.$field.'" value="'.$today.'" size="10" maxlength="10">';	
		
		echo " <select name='hour'>";
		if ($allownull)
			echo "<option value='x'>All</option>";
		for($h = 1; $h < 13; $h++)
		{						
			echo "<option value='$h' ";
			if ($h == $hour) echo "selected ";
			echo ">$h</option>";
		}
		echo "</select>";
		echo ":";
		echo "<select name='minute'>";		
		if ($allownull)
			echo "<option value='x'>All</option>";
		echo "<option value='00'".(($min=="00")?" selected ":" ").">00</option>";
		echo "<option value='15'".(($min=="15")?" selected ":" ").">15</option>";
		echo "<option value='30'".(($min=="30")?" selected ":" ").">30</option>";
		echo "<option value='45'".(($min=="45")?" selected ":" ").">45</option>";
		echo "</select>";
		echo "<select name='ampm'>";
		if ($allownull)
			echo "<option value='x'>All</option>";
		echo "<option value='0'".(($ampm=="0")?" selected ":" ").">AM</option>";
		echo "<option value='12'".(($ampm=="12")?" selected ":" ").">PM</option>";
		echo "</select>";
	}
	
	function getPriorities($selected="3",$allownull=false)
	{		
		echo "<select name='priority'>\n";
		if ($allownull)
			echo "<option value='x'>All</option>";
			
		for ($i = 1; $i < 6; $i++)
		{
			echo "<option value='$i' ";
			if ($i == $selected)
				echo "selected ";
			echo ">$i</option>\n";					
		}
		echo "</selected>";
	}
	
	function getUserDropDown($name="userid",$selected="x")
	{
		$sql = "select id,displayname from ".DATABASE.".users ".
				"where username != 'uber' ";		
			
		$rs = new mysql_recordset($sql);
		echo "<select name='$name'>\n";
		echo "<option value='xx'>-- Select --</option>\n";
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<option value='$id' ";
			if ($id == $selected)
				echo "selected ";
			echo ">".ucwords($displayname)."</option>\n";					
		}
		echo "</selected>";
	}
	


?>