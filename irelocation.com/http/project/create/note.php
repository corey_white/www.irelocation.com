<? 
	session_start();
	
	if ($_REQUEST['pid'] == "")
	{
		header("Location: ../projects.php");
		exit();
	}
	$pid = $_REQUEST['pid'];
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/create/form.lib.php";	
	
	$type = "notes";
	
	if ($_SESSION['project']['create']['type'] == $type)
	{
		if (is_array($_SESSION['project']['create']))
		{
			extract($_SESSION['project']['create']);	
			echo "<!--- ".print_r($_SESSION['project']['create'],true)." --->";
		}
	}
	else
		unset($_SESSION['project']['create']);
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden" name="created" value="<?= date("Y-m-d H:i:s") ?>" />
		<input type="hidden"  name="type" value="notes" />
		<input type="hidden"  name="projectid" value="<?= $pid ?>" />		
		<input type="hidden" name="userid" value="<?= $_SESSION['project']['id'] ?>" />
	<table class='filterfont'>
		<? if ($_REQUEST['error'] != "")
		{
			echo "<tr><td colspan='2'><font color='#FF0000'>";
			echo "<strong>Please fill out Field: </strong>".$_REQUEST['error'];
			echo "</font></td></tr>";
		} ?>
		<tr>
			<td colspan="2">Add a Note</td>
		</tr>
		<tr>
			<td>Title</td>
			<td><input type="text" name="title" /></td>
		</tr>
		<tr>
			<td>Note:</td>
			<td><textarea name='note'></textarea></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Add Note"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
