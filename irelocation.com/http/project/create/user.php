<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/create/form.lib.php";	
	
	
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden"  name="type" value="user" />
	<table class='filterfont'>
		<tr>
			<td colspan="2">Create a User</td>
		</tr>
		<tr>
			<td>Display Name</td>
			<td><input type="text" name="displayname" /></td>
		</tr>
		<tr>
			<td>Username</td>
			<td><input type="text" name="username" value="<?= $username ?>" /></td>
		</tr>
		<tr>
			<td>Password</td>
			<td><input type="password" name="password" value="<?= $password ?>" /></td>
		</tr>
		<tr>
			<td>Email</td>
			<td><input type="text" name="email" value="<?= $email ?>" /></td>
		</tr>
		<tr>
			<td>Type:</td>
			<td><? getUserTypes("usertype",$usertype); ?></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Create Task"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
