<?
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	$keys = array_keys($_POST);
	foreach($keys as $k)
	{
		if (is_numeric($k)) continue;
		$_SESSION['project']['create'][$k] = $_POST[$k];
	}
	
	$type = $_POST['type'];
	unset($_POST['type']);
		
	switch($type)
	{
		case "projects":						
			doProject();
			break;		
		case "notes":
			doNote();
			break;
		case "tasks":			
			doTask();
			break;		
		case "updates":			
			doUpdate();
			break;			
		case "users":
			doUser();
			break;
		case "assign";
			doAssignment();
		default:
			echo $type;
	}

	function doUpdate()
	{
		$type = "updates";
		$goto = "update.php";
		$aid = $_POST['assignedtoid'];		
		
		$result = validate($type);
		$id = create($type);
		$sql = "select p.id,p.title from marble.projects as p join ".
				" marble.assignedto as a on a.projectid = p.id where ".
				" a.id = $aid ";
		//echo "sql: $sql <br/>";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$project_title = $rs->myarray['title'];
		$pid = $rs->myarray['id'];
		//echo "title: $project_title <br/>";
		//link everyone in this project
		$associated = associated("projects",$pid);
		$associated = array_unique($associated);
		//to this update.
		//echo "associated: $associated <br/>";
		foreach($associated as $userid)
			addAssociation("updates",$id,$userid);
		
		//get those peoples emails.
		$to = associatedEmails("projects",$pid,USER_ID);		
		$subject = "Project Update";
		$message = USER_NAME." has posted an update to ".
					"project: $project_title, a project ".
					"you are associated with.";
		//mail them the update.
		mailWrapper($to,$subject,$message);

		header("Location: ../view/project.php?id=$pid");
		exit();
	}	

	function doAssignment()	
	{
		extract($_POST);
		
		$sql = "select * from ".DATABASE.".assignedto where projectid = $projectid".
				" and userid = $userid ";
		//echo $sql."<br/>";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			header("Location: ../view/project.php?id=$projectid");
			exit();
		}
		else
		{
			$sql = "insert into ".DATABASE.".assignedto (projectid,userid,assigned) ".
					" values ('$projectid','$userid',NOW()); ";		
			//echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			_logaction("projects","assigned",
						"user:$userid was assigned to project:$projectid");
			addAssociation('projects',$projectid,$userid);		
			
			$User = new User($userid);
			$Project = new Project($projectid);
			
			$to = associatedEmails("projects",$projectid,USER_ID);		
		
			$subject = "Project Assignment";
			$message = USER_NAME." has assigned ".$User->data['displayname'].
						" to project: ".$Project->data['title'].", a project ".
						"you are associated with.";
			mailWrapper($to,$subject,$message);
			
				
			header("Location: ../view/project.php?id=$projectid");
			exit();
		}	
	}

	function create($table)
	{	
		$sql = " insert into ".DATABASE.".$table set ";
		foreach($_POST as $f => $v)
		{
			if ($f != "password")
				$sql .= " $f = '".str_replace("'","\'",trim($v))."', ";
			else
				$sql .= " password = PASSWORD('".trim($v)."'), ";
		}
		$sql = substr($sql,0,-2);
		$rs = new mysql_recordset($sql);
		$id = $rs->last_insert_id();
		$rs->close();
		//clear out saved form data from session.
		unset($_SESSION['project']['create']);
		
		_logaction($table,"created","object $id was created");
		return $id;
	}

	function addAssociation($table,$id,$uid)
	{
		$sql = " insert into marble.objectreadby (objectid,userid,object) ".
				" values  ($id,$uid,'$table') ";
		$rs = new mysql_recordset($sql);
		$rs->close();
	}

	function createAssociation($table,$id,$object="x",$objectid="x")
	{
		if ($object == "x") $object = $table;
		if ($objectid == "x") $objectid = $id;
		
		$users = associated($object,$objectid);
		$sql = " insert into marble.objectreadby (objectid,userid,object) values ";
		if (count($users) == 1)
			$sql .= " ($id,".$users[0].",'$table'); ";
		else
		{
			foreach($users as $u)
				$sql .= " ($id,$u,'$table'), ";
			$sql = substr($sql,0,-2);
		}
		//echo "<!--- $sql --->";
		
		$rs = new mysql_recordset($sql);
		$rs->close();
	}

	function doNote()
	{
		$type = "notes";
		$goto = "note.php";
		$projectid = $_POST['projectid'];		
		
		$result = validate($type);

		if ($result[0] == 1)
		{	
			
			//get title.
			$sql = "select title from marble.projects where id = $projectid ";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$project_title = $rs->myarray['title'];					
		
		
			$id = create($type);
			createAssociation($type,$id,"projects",$projectid);		
			$to = associatedEmails("projects",$projectid);		
			$subject = "Note Creation";
			$message = USER_NAME." has created note '".$_POST['title']."' associated with project ".
						" '".$project_title."', a project you are assigned to.";
			mailWrapper($to,$subject,$message);
			header("Location: ../view/project.php?id=$projectid");
			exit();
		}
		else
		{
			header("Location: $goto?pid=$projectid&error=".$result[1]);
			exit();
		}		
	}

	function _sendMail($to,$type,$id,$assign=false)
	{
		if(substr($type,-1) == "s")
			$type = substr($type,0,-1);
		
		if (!$assign)
		{
			$subject = ucwords(" $type creation");
			$message = USER_NAME." has created $type #".$id;
		}
		else
		{
			$message = USER_NAME." has assigned you to $type #".$id;
			$subject = " User Assignment";
		}
		
		mailWrapper($to,$subject,$message);
	}	

	function doUser()
	{
		$id = create("users");
		header("Location: ../view/user.php?id=$id");
		exit();
	}

	function doTask()
	{				
		$type = "tasks";
		$goto = "task.php";
			
		$msg = USER_NAME." has created task for you ".
				"to accomplish called '".$_POST['title']."'. This task should be ".
				"completed by ";	

		$_POST['title'] = str_replace("'","\'",$_POST['title']);
		$_POST['description'] = str_replace("'","\'",$_POST['description']);
		
		_changeFinishBy();
		$result = validate($type);
		if ($result[0] == 1)
		{		
			$id = create($type);							
			createAssociation($type,$id);		
			$to = associatedEmails($type,$id,USER_ID);					
			$subject = "Task Creation";
			$message = USER_NAME." has created task '".$_POST['title'].
						"' and assigned you to it.";
			mailWrapper($to,$subject,$message);
			header("Location: ../view/$goto?id=$id");
			exit();
		}
		else
		{
			header("Location: $goto?error=".$result[1]);
			exit();
		}				
	}

	function _changeFinishBy()
	{
		$finishby = $_POST['finishby']." ".($_POST['hour']+$_POST['ampm']).":"
					.$_POST['minute'].":00";
		$_POST['finishby'] = $finishby;
		unset($_POST['hour'],$_POST['ampm'],$_POST['minute']);
	}

	function validate($type)
	{
		extract($_POST);
		if ($type == "projects" || $type == "tasks")
		{

			if (strlen($title) < 1)
				return array(0,"title");
			
			if (strlen($description) < 1)
				return array(0,"description");
			
			if ($type == "tasks")
				if ($assignedto == "xx")
					return array(0,"assignedto");	
			
			$now = date("Y-m-d H:i:s");
			if ($finishby < $now)
				return array(0,"finishby");					
				
			return array(1,"valid");
		}
		else if ($type == "notes")
		{
			if (strlen($title) < 1)
				return array(0,"title");
			
			if (strlen($note) < 1)
				return array(0,"note");
			return array(1,"valid");			
		}		
		else if ($type == "updates")
		{		
			if (strlen(trim($description)) == 0)
				return array(0,"description");
			
			return array(1,"valid");	
		}
		
	}

	function doProject()
	{
		$type = "projects";
		$goto = "project.php";
		//$_POST['title'] = str_replace("'","\'",$_POST['title']);
		//$_POST['description'] = str_replace("'","\'",$_POST['description']);
		_changeFinishBy();
		$result = validate($type);
		if ($result[0] == 1)
		{
			$id = create($type);							
			header("Location: ../view/$goto?id=$id");
			exit();
		}
		else
		{
			header("Location: $goto?error=".$result[1]);
			exit();
		}	
	}
?>