<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/create/form.lib.php";	
	
	if ($_REQUEST['pid'] == "")
	{
		header("Location: ../projects.php");
		exit();
	}
	
	$pid = $_REQUEST['pid'];
	
	$aid = getAid($pid,USER_ID);
	if ($aid == "")
	{
		header("Location: ../view/project.php?id=$pid");
		exit();
	}
	
	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	
	$type = "updates";
	
	if ($_SESSION['project']['create']['type'] == $type)
	{
		if (is_array($_SESSION['project']['create']))
		{
			extract($_SESSION['project']['create']);	
			echo "<!--- ".print_r($_SESSION['project']['create'],true)." --->";
		}
	}
	else
		unset($_SESSION['project']['create']);
	
?>
	<form name="taskform" action="do_form.php" method="post">
		<input type="hidden" name="created" value="<?= date("Y-m-d H:i:s") ?>" />
		<input type="hidden"  name="type" value="updates" />		
		<input type="hidden" name="assignedtoid" value="<?= $aid ?>" />
	<table class='filterfont'>
		<? if ($_REQUEST['error'] != "")
		{
			echo "<tr><td colspan='2'><font color='#FF0000'>";
			echo "<strong>Please fill out Field: </strong>".$_REQUEST['error'];
			echo "</font></td></tr>";
		} ?>
		<tr>
			<td colspan="2">Add an Update</td>
		</tr>
		<tr>
			<td valign="top">Description:</td>
			<td><textarea name='description'></textarea></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Add an Update"/></td>
		</tr>
	</table>
	</form>
</body>
</html>
