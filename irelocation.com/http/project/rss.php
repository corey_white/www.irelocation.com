<?	
	
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	function printRSSHeader(
		$title="iRelo Projects",
		$link="http://www.irelocation.com/projects/home.php"
	)	
	{
		header("Content-Type: text/xml");

		$date = date("D, j M Y H:i:s T");
		
		echo "<?xml version=\"1.0\" ?>\n 
				<rss version=\"0.91\">
				<channel> 
				    <title>$title</title>
					<ttl>5</ttl>
					<pubDate>$date</pubDate>
					<description>Links to your current projects</description>
				    <link>$link</link>\n";		
	}

	function xmlentities ( $string )
	{
	   return str_replace ( 
		array ( '&', '"', "'", '<', '>' ), 
		array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
		$string 
		);
	}
	function printItem($title,$link,$description)
	{
		echo "<item>\n<title>".xmlentities($title)."</title>\n";
		echo "<link>".xmlentities($link)."</link>\n";
		echo "<description>".xmlentities($description)."</description>\n</item>\n";		
	}
	
	function printFooter()
	{
		echo "</channel></rss>";
	}
	
	$ROOT_LINK = "http://www.irelocation.com/project/";
	$userid = $_REQUEST['userid'];
	$type = "projects";
	
	if ($userid == "" || !is_numeric($userid))
	{
		printRSSHeader("Invalid User Id");
		printItem("No UserId Given!",$ROOT_LINK,"dummy description");
		printFooter();
		exit();
	}
	
	$User = new User($userid);
	
	if ($type == "projects" || $type == "x")
		$projects = $User->getMyProjects();
		
	if ($type == "tasks" || $type == "x")
		$tasks = $User->getMyTasks();
	
	
	
	if ($projects != "" || $tasks != "")
	{
		if ($projects != "" && $tasks == "")
			printRSSHeader();				
		
		if ($projects != "" && $tasks != "")
			printItem("--- Home Page ---",$ROOT_LINK."home.php","user home page.");
			
		
		if ($projects != "")
		{
			$rs = new mysql_recordset($projects);
			printItem("--- Projects ---",$ROOT_LINK."myprojects.php","seperator");
			while($rs->fetch_array())
			{
				$id = $rs->myarray['id'];
				$fin = ($rs->myarray['finished'] == BLANK_DATE)?"":" - fin";
				$title = "$id) ".$rs->myarray['title']." ".$fin;
				if ($new == "*")
					$title = $new." ".$title;
				$desc = $rs->myarray['description'];
				if (strlen($desc) > 25)
					$desc = substr($desc,0,22)."...";
				$link = $ROOT_LINK."?goto=projects&id=$id";
				printItem($title,$link,$desc);
			}
		}

		printFooter();
		
	}
	else
	{
		printRSSHeader();
		printItem("--- Home Page ---",$ROOT_LINK."home.php","user home page.");
		printFooter();
	
	}
?>