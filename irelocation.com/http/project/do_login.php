<?
	session_start();
	include_once "../inc_mysql.php";
	include_once "project.lib.php";
	
	extract($_POST);
	
	$result = UserLogin($username,$password);
	if ($result !== FALSE)
	{
		$_SESSION['project']['usertype'] = $result->data['usertype'];
		$_SESSION['project']['displayname'] = $result->data['displayname'];
		$_SESSION['project']['id'] = $result->data['id'];
		$_SESSION['project']['logintime'] = time();
		
		_logaction("users","login");
		
		if ($goto != "" && $id != "")
		{
			header("Location: view/".substr($goto,0,-1).".php?id=$id");
		}
		else
			header("Location: home.php");
	}
	else
		header("Location: login.php?goto=$goto&id=$id");
	exit();
?>