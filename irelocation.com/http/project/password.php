<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";

	if (($error = $_REQUEST['error']) != "")
	{
		$color = "#FF0000";
		switch($error)
		{
			case PASSWORD_CONFIRM:
				$msg = "You must enter your new password 2x";
				break;
			case INVALID_PASSWORD:
				$msg = "You must enter your current password correctly.";
				break;
			case PASSWORD_TOO_SHORT:
				$msg = "Your new password must be at least 6 characters.";
				break;
			case PASSWORD_NOT_CHANGED:
				$msg = "If you want to change your password, change it!";
				break;
		}
				
	}
	else if (isset($_REQUEST['valid']))
	{
		$msg = "You Password has been Changed!";
		$color = "#00FF00";
	}
	
	if (strlen($_REQUEST['email_error']) > 0)
	{
		$em_msg = " Your email address is too short. try again.";
		$color = "#FF0000";
	}
	else if (isset($_REQUEST['email_valid']))
	{
		$em_msg = "You Email Address has been Changed!";
		$color = "#00FF00";
	}
	
?>
	<form name="updatepassword" action="do_password.php" method="post">
	<table class="filterfont">
		<tr>
			<td colspan="2">Change your password.</td>
		</tr>
		<? if ($msg != "") { ?>
		<tr>
			<td colspan="2">
				<font color="<?= $color ?>">
					<strong>
						<?= $msg ?>
					</strong>
				</font>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td>
		<? } ?>
		<tr>
			<td>Current Password:</td>
			<td><input type="password" name="cur"/></td>
		</tr>
		<tr>
			<td>New Password:</td>
			<td><input type="password" name="new1"/></td>
		</tr>	
		<tr>
			<td>New Password Again:</td>
			<td><input type="password" name="new2"/></td>
		</tr>	
		<tr>
			<td colspan="2"><input type="submit" value="Change Password"/></td>
		</tr>
	</table>
	</form>
	<br/>
	<br/>
	<?
		$User = new User(USER_ID);
		$email = $User->data['email'];
	
	?>
	<form name="updateemail" action="do_emailchange.php" method="post">
	<table class="filterfont">
		<tr>
			<td colspan="2">Change your Email.</td>
		</tr>
		<? if ($em_msg != "") { ?>
		<tr>
			<td colspan="2">
				<font color="<?= $color ?>">
					<strong>
						<?= $em_msg ?>
					</strong>
				</font>
			</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td>
		<? } ?>
		<tr>
			<td>Current Email:</td>
			<td><input type="text" name="cur" value="<?= $email ?>" /></td>
		</tr>
		<tr>
			<td>New Email:</td>
			<td><input type="text" name="new"/></td>
		</tr>		
		<tr>
			<td colspan="2"><input type="submit" value="Change Email"/></td>
		</tr>
	</table>
	</form>
	<? include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";	?>
