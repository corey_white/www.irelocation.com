<? 
	session_start();
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/lib.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	extract($_POST);
	
	if ($new1 != $new2)
	{
		header("Location: password.php?error=".PASSWORD_CONFIRM);
		exit();
	}
	if ($new1 == $cur)
	{
		header("Location: password.php?error=".PASSWORD_NOT_CHANGED);
		exit();
	}
	if (strlen($new1) < 6)
	{
		header("Location: password.php?error=".PASSWORD_TOO_S);
		exit();
	}

	$sql = "select id from marble.users where ".
			"password = PASSWORD('$cur') and id = ".USER_ID;
	$rs = new mysql_recordset($sql);
	if (!$rs->fetch_array())
	{
		header("Location: password.php?error=".INVALID_PASSWORD);
		exit();
	}
	$did = $rs->myarray['id'];
	$rs->close();
	
	
	$sql = "update marble.users set password = PASSWORD('$new1') where id = ".USER_ID;
	$rs = new mysql_recordset($sql);
	_logAction("user","changed password");
	header("Location: password.php?valid");
	exit();	
?>