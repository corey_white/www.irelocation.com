<?
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";	
	extract($_POST);
	
	
	if ($to != "*")
	{
		$email = "select *, if(id=$to,'to','from') 'who' from marble.users ".
					" where (id = $to) or (id = $from) ";
		
		$rs = new mysql_recordset($email);
		while($rs->fetch_array())
		{
			if ($rs->myarray['who'] == "to")
			{
				$to = $rs->myarray['displayname']." <".$rs->myarray['email'].">";
				$to_name = $rs->myarray['displayname'];
			}
			else
			{
				$from = $rs->myarray['displayname']." <".$rs->myarray['email'].">";
				$from_name =  $rs->myarray['displayname'];
			}
		}
	}
	else
	{
		//message all workers of a project.
		$email = "select displayname,email from marble.users as u ".
					" join marble.assignedto as a on u.id = a.userid ".
					" where a.projectid = $id ";
		$to = array();
		$rs = new mysql_recordset($email);
		while($rs->fetch_array())
		{
			$to[] = $rs->myarray['displayname']." <".$rs->myarray['email'].">";
			$to_names[] = $rs->myarray['displayname'];
		}
		$email = "select displayname,email from marble.users where id = $from ";
		$rs = new mysql_recordset($email);
		if($rs->fetch_array())
		{
			$from = $rs->myarray['displayname']." <".$rs->myarray['email'].">";
			$from_name =  $rs->myarray['displayname'];
		}	
		$to = implode(",",$to);
		$to_name = implode(",",$to_names);
	}
	
	_logAction("user","sent email",
				$from_name." sent an email to ".$to_name." about ".$subject);
	mailWrapper($to,$subject,$message,$from);
	header("Location: view/".substr($type,0,-1).".php?id=$id");
?>