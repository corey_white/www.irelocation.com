<title>Untitled Document</title><?
	include $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	
	$state_cache = array();
	
	function getStateCode($state)
	{
		$state = strtolower($state);	
		global $state_cache;
		if (strlen($state_cache[$state]) == 2)
			return $state_cache[$state];
		else
		{
			$sql = " select state_code from usautotransport.zip_codes where ".
					" lower(state) = lower('".$state."') limit 1";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$code = $rs->myarray['state_code'];
			$state_cache[$state] = $code;
			$rs->close();	
			return $code;
		}
	}
	
	function gethtmlsource($url,$fields='')
	{
	  $ch=curl_init($url);
	  curl_setopt($ch, CURLOPT_HEADER, 0);
	  if($fields!='') curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	  $buffer = curl_exec($ch);
	  
	  if(strlen($buffer) > 0)
	  {
		return $buffer;
	  }
	  else
	  {
		return "";
	  }
		curl_close($ch);
	}

	$truck_stop[0]['name']='Flying J';
	$truck_stop[0]['url']='http://serv2.flyingj.com/fuel/diesel_print_CF.cfm?state=US';
	//http://www.petrotruckstops.com/fuel_search.sstg
	$truck_stop[1]['name']='Petro';
	$truck_stop[1]['url']='http://www.petrotruckstops.com/fuel_search.sstg';
	
	//http://www.tatravelcenters.com/taweb/Content/DieselPricesPrint.aspx
	$truck_stop[2]['name']='TravelCenters';
	$truck_stop[2]['url']='http://www.tatravelcenters.com/taweb/Content/DieselPricesPrint.aspx';
	
	//http://www.pilotcorp.com/List_Alternate_View.aspx?view=printerFriendly&alternateViews=false
	$truck_stop[3]['name']='Pilot';
	$truck_stop[3]['url']='http://www.pilotcorp.com/Locations/Travel_Centers/Complete_Pricing_List.aspx';
	
	//http://www.loves.com/DesktopModules/Loves.FuelPrices/FuelPrices_Print.aspx?ST=QWxs-jX0L4QmieS0%3d&CY=QWxs-jX0L4QmieS0%3d&SO=QWxs-jX0L4QmieS0%3d&SC=U3RhdGU%3d-Vgg9PejFAOU%3d&SD=QVND-hBYVDJoDyJc%3d
	$truck_stop[4]['name']='Loves';
	$truck_stop[4]['url']='http://www.loves.com/DesktopModules/Loves.FuelPrices/FuelPrices_Print.aspx?ST=QWxs-jX0L4QmieS0%3d&CY=QWxs-jX0L4QmieS0%3d&SO=QWxs-jX0L4QmieS0%3d&SC=U3RhdGU%3d-Vgg9PejFAOU%3d&SD=QVND-hBYVDJoDyJc%3d';
	
	//9 states.
	$truck_stop[5]['name']='Speedway';
	$truck_stop[5]['url']='http://www.speedway.com/FindUs/StoreLocator/GasPriceSearch.aspx?State=Indiana';
	
	//http://www.ripgriffin.com/fuel_prices.asp only 3??
	$truck_stop[6]['name']='Rip Griffin';
	$truck_stop[6]['url']='http://204.255.94.145/pages5/fuel_pricing.asp';

?>
