<?php
$has_title_row = true;
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(is_uploaded_file($_FILES['csvfile']['tmp_name'])){
        $filename = basename($_FILES['csvfile']['name']);
 
        if(substr($filename, -3) == 'csv'){
            $tmpfile = $_FILES['csvfile']['tmp_name'];
            if (($fh = fopen($tmpfile, "r")) !== FALSE) {
                $i = 0;
                while (($items = fgetcsv($fh, 10000, ",")) !== FALSE) {
                    if($has_title_row === true && $i == 0){ // skip the first row if there is a tile row in CSV file
                        $i++;
                        continue;
                    }
                    #print_r($items);
                    
                    #$fields = $items[0] .":".$items[1] .":".$items[2] .":".$items[3] ;
                    #echo $fields . "<br>";
                    
                    //-- here's where the magic happens
                    //-- source is always irelo_test for all tests
                    
					if ( $items[2] == "autotest" ) { //-- auto campaign
						$VID=1463;
						$AID=2568;
						$LID=366;
						
						$Company = urlencode($items[0]);
						$ClientUID = urlencode($items[1]);
						$campaign = urlencode($items[2]);
						$first_name = urlencode($items[3]);
						$last_name = urlencode($items[4]);
						$email = urlencode($items[5]);
						$phone = urlencode($items[6]);
						$origin_city = urlencode($items[7]);
						$origin_state = urlencode($items[8]);
						$origin_zip = urlencode($items[9]);
						$desination_city = urlencode($items[10]);
						$destination_state = urlencode($items[11]);
						$destination_zip = urlencode($items[12]);
						$move_date = urlencode($items[13]);
						$vehicle_make = urlencode($items[14]);
						$vehicle_model = urlencode($items[15]);
						$vehicle_year = urlencode($items[16]);
						$vehicle_type = urlencode($items[17]);
						$running = urlencode($items[18]);
						
						$post_fields = "VID=$VID&AID=$AID&LID=$LID&source=irelo_test&fname=$first_name&lname=$last_name&email=$email&remote_ip=11.22.33.44&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$desination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$move_date&vmake=$vehicle_make&vmodel=$vehicle_model&vyear=$vehicle_year&vtype=$vehicle_type&running=$running&campaign=autotest&ClientUID=$ClientUID";
						
						
						#echo $post_fields;
						echo "<br /><br />";
						
/* we'll worry about this later
*/
						$curl = curl_init();
						curl_setopt($curl, CURLOPT_URL,"https://secure.leadexec.net/LeadImport.asmx/LeadReceiver");
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($curl, CURLOPT_POSTFIELDS, "$post_fields");
						$result = curl_exec ($curl);
						
						echo "$result,$items[0],$items[1],$items[2],$items[3],$items[4],$items[5],$items[6],$items[7],$items[8],$items[9],$items[10],$items[11],$items[12],$items[13],$items[14],$items[15],$items[16],$items[17],$items[18]";
						echo "<br /><br />";
						
						
						
					} elseif ( $items[3] == "moving" ) { //-- moving campaign
						$VID=XXX;
						$AID=XXX;
						$LID=XXX;
						
					} else {
						echo "There is no campaign for this record.<br />";
					}
                    
                    
                    $i++;
                }
            }
        }
        else{
            die('Invalid file format uploaded. Please upload CSV.');
        }
    }
    else{
        die('Please upload a CSV file.');
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="Raju Gautam" />
	<title>Test</title>
</head>
<body>
    <form enctype="multipart/form-data" action="" method="post" id="add-courses"> 
        <table cellpadding="5" cellspacing="0" width="500" border="0"> 
            <tr> 
                <td class="width"><label for="image">Upload CSV file : </label></td> 
                <td><input type="hidden" name="MAX_FILE_SIZE" value="10000000" /><input type="file" name="csvfile" id="csvfile" value=""/></td> 
                <td><input type="submit" name="uploadCSV" value="Upload" /></td> 
            </tr> 
        </table> 
    </form>
</body>
</html>