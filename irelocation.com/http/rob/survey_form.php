<?php

//-- if submitted
if ( $submit_survey ) {
    
	  // validate fields
	$error_msg = "";
	
	If (!$question1 ) {
		$validation_error = "yes";
		$question1_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question2 ) {
		$validation_error = "yes";
		$question2_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question3 ) {
		$validation_error = "yes";
		$question3_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question4 ) {
		$validation_error = "yes";
		$question4_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question5 ) {
		$validation_error = "yes";
		$question5_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question6 ) {
		$validation_error = "yes";
		$question6_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question7 ) {
		$validation_error = "yes";
		$question7_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	If (!$question8 ) {
		$validation_error = "yes";
		$question8_msg .= "<span style=\"font-weight: bold; color: red;\">MIssing</span>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$error_head_msg = "<span style=\"font-family: arial; font-size: 20px; color: red; \">Please asnwer each question</span>";
	  
	} else {
		// process form data code here
		$submitted_date = date("m-d-Y h:i A");
		
		//-- mail the info to us
		$email_msg = "The following survey has been submitted:\n\n
Did you book your auto transport with one of the auto brokers from our website? 
-- $question1

If you did book a transport, was it for a date that was 30 days or less from the time you went to the website?
-- $question2

If you did not book a transport with one of the brokers from our website, can you tell us why not? 
--$question3

In your opinion, how would you rate the number of emails that you received from the brokers on our website? 
-- $question4

In your opinion, how would you rate the number of phone calls that you received from the brokers on our website?
-- $question5

Did you receive enough information regarding auto-transport companies in order to make a decision regarding which company to choose?
-- $question6

Were all your questions answered by the brokers?
-- $question7

Were you treated in a polite and courteous manner?
-- $question8

Any comments would like to share?
-- $comments

Submitted on $submitted_date (server time)
		";
		
		$headers .= 'From: Survey Response <no_reply@irelocation.com>' . "\r\n";
#		$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
#		$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		mail("rob@irelocation.com,katrina@irelocation.com","Survey Response","$email_msg","$headers");
		
		//-- redirect to thanks
		header("location: thanks.html");

	}
} 

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quality Survey</title>
<link href="inc/survey.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="800" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td align="center">

<form action="survey_form.php" method="post" name="Survey">

<img src="images/cs.jpg" alt="Thank you for choosing CarShipping.com" width="450" height="101" />
<table align="center" width="95%" border="0" cellpadding="2" summary="This is a short survey about the online experience when shopping for an auto transporter.">
<!-- 
  <caption>
    Survey Questions
  </caption>
 -->
  <tr>
    <td>
	<?php
	if ( $validation_error == "yes" ) {
		echo "$error_head_msg";
	} else {
		echo "<p>Recently you visited CarShipping.com for a quote to transport your vehicle.  We are interested in your feedback.  Please take a minute or two to answer our survey.</p>";
	}
	?>
    
    </td>
  </tr>
<?
echo "  <tr>
    <td><fieldset>
    <legend class=\"title\">Did you book your auto transport with one of the auto brokers from our website? <br /> $question1_msg</legend>
    <div class=\"answer\">
    <input name=\"question1\" type=\"radio\" value=\"Yes\" ";
		if ( $question1 == "Yes" ) {
			echo " checked";
		} 
		echo "/>Yes &nbsp;
    <input name=\"question1\" type=\"radio\" value=\"No\" ";
if ( $question1 == "No" ) {
    echo " checked";
} 
echo "/> No</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">If you did book a transport, was it for a date that was 30 days or less from the time you went to the website? <br /> $question2_msg</legend>
    <div class=\"answer\">
    <input name=\"question2\" type=\"radio\" value=\"Yes\" ";
if ( $question2 == "Yes" ) {
    echo " checked";
} 
echo "/>Yes &nbsp;
    <input name=\"question2\" type=\"radio\" value=\"No\" ";
if ( $question2 == "No" ) {
    echo " checked";
} 
echo "/> No</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">If you did not book a transport with one of the brokers from our website, can you tell us why not? <br /> $question3_msg</legend>
    <div class=\"answer\">
    
	<input name=\"question3\" type=\"radio\" value=\"I was only looking for information / not ready to book at this time\" ";
if ( $question3 == "I was only looking for information / not ready to book at this time" ) {
    echo " checked";
} 
echo "/>I was only looking for information / not ready to book at this time<br />

	<input name=\"question3\" type=\"radio\" value=\"I thought Price was too expensive\" ";
if ( $question3 == "I thought Price was too expensive" ) {
    echo " checked";
} 
echo "/>I thought Price was too expensive<br />

	<input name=\"question3\" type=\"radio\" value=\"Became frustrated with the process\" ";
if ( $question3 == "Became frustrated with the process" ) {
    echo " checked";
} 
echo "/>Became frustrated with the process<br />

	<input name=\"question3\" type=\"radio\" value=\"Went with another broker from another website\" ";
if ( $question3 == "Went with another broker from another website" ) {
    echo " checked";
} 
echo "/>Went with another broker from another website<br />

	<input name=\"question3\" type=\"radio\" value=\"No one was able to transport within the timeframe I needed\" ";
if ( $question3 == "No one was able to transport within the timeframe I needed" ) {
    echo " checked";
} 
echo "/>No one was able to transport within the timeframe I needed<br />

	<input name=\"question3\" type=\"radio\" value=\"Didn't like any of the companies that were presented\" ";
if ( $question3 == "Didn't like any of the companies that were presented" ) {
    echo " checked";
} 
echo "/>Didn't like any of the companies that were presented<br />

    </div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">In your opinion, how would you rate the number of emails that you received from the brokers on our website? <br /> $question4_msg</legend>
    <div class=\"answer\">
    
	<input name=\"question4\" type=\"radio\" value=\"I hardly received any emails\" ";
if ( $question4 == "I hardly received any emails" ) {
    echo " checked";
} 
echo "/>I hardly received any emails<br />
	
	<input name=\"question4\" type=\"radio\" value=\"I would have liked to have received more emails regarding quotes and offers\" ";
if ( $question4 == "I would have liked to have received more emails regarding quotes and offers" ) {
    echo " checked";
} 
echo "/>I would have liked to have received more emails regarding quotes and offers<br />
	
	<input name=\"question4\" type=\"radio\" value=\"I received an appropriate amount of emails\" ";
if ( $question4 == "I received an appropriate amount of emails" ) {
    echo " checked";
} 
echo "/>I received an appropriate amount of emails<br />
	
	<input name=\"question4\" type=\"radio\" value=\"I received more emails than I needed\"";
if ( $question4 == "I received more emails than I needed" ) {
    echo " checked";
} 
echo " />I received more emails than I needed<br />
	
	<input name=\"question4\" type=\"radio\" value=\"I received an unbearable amount of emails from brokers\" ";
if ( $question4 == "I received an unbearable amount of emails from brokers" ) {
    echo " checked";
} 
echo "/>I received an unbearable amount of emails from brokers<br />
	
    </div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">In your opinion, how would you rate the number of phone calls that you received from the brokers on our website? <br /> $question5_msg</legend>
    <div class=\"answer\">
    
	<input name=\"question5\" type=\"radio\" value=\"I hardly received any phone calls\" ";
if ( $question5 == "I hardly received any phone calls" ) {
    echo " checked";
} 
echo "/>I hardly received any phone calls<br />
	
	<input name=\"question5\" type=\"radio\" value=\"I would have liked to receive more phone calls regarding quotes and offers\" ";
if ( $question5 == "I would have liked to receive more phone calls regarding quotes and offers" ) {
    echo " checked";
} 
echo "/>I would have liked to receive more phone calls regarding quotes and offers<br />
	
	<input name=\"question5\" type=\"radio\" value=\"I received an appropriate amount of phone calls\" ";
if ( $question5 == "I received an appropriate amount of phone calls" ) {
    echo " checked";
} 
echo "/>I received an appropriate amount of phone calls<br />
	
	<input name=\"question5\" type=\"radio\" value=\"I received more phone calls than I needed\" ";
if ( $question5 == "I received more phone calls than I needed" ) {
    echo " checked";
} 
echo "/>I received more phone calls than I needed<br />
	
	<input name=\"question5\" type=\"radio\" value=\"I received an unbearable amount of phone calls from brokers\" ";
if ( $question5 == "I received an unbearable amount of phone calls from brokers" ) {
    echo " checked";
} 
echo "/>I received an unbearable amount of phone calls from brokers<br />
	
	</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">Did you receive enough information regarding auto-transport companies in order to make a decision regarding which company to choose? <br /> $question6_msg</legend>
    <div class=\"answer\">
    	<input name=\"question6\" type=\"radio\" value=\"Yes\" ";
if ( $question6 == "Yes" ) {
    echo " checked";
} 
echo "/>Yes &nbsp;
    	<input name=\"question6\" type=\"radio\" value=\"No\" ";
if ( $question6 == "No" ) {
    echo " checked";
} 
echo "/> No
	</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">Were all your questions answered by the brokers? <br /> $question7_msg</legend>
    <div class=\"answer\">
    
    	<input name=\"question7\" type=\"radio\" value=\"No company answered all of my questions\" ";
if ( $question7 == "No company answered all of my questions" ) {
    echo " checked";
} 
echo "/>No company answered all of my questions<br />
    	
    	<input name=\"question7\" type=\"radio\" value=\"Some companies answered my questions\" ";
if ( $question7 == "Some companies answered my questions" ) {
    echo " checked";
} 
echo "/>Some companies answered my questions<br />
    	
    	<input name=\"question7\" type=\"radio\" value=\"Most companies answered all of my questions\" ";
if ( $question7 == "Most companies answered all of my questions" ) {
    echo " checked";
} 
echo "/>Most companies answered all of my questions<br />
    	
    	<input name=\"question7\" type=\"radio\" value=\"All companies answered all of my questions\" ";
if ( $question7 == "All companies answered all of my questions" ) {
    echo " checked";
} 
echo "/>All companies answered all of my questions<br />
    	
	</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">Were you treated in a polite and courteous manner? <br /> $question8_msg</legend>
    <div class=\"answer\">
    
    <input name=\"question8\" type=\"radio\" value=\"No companies were polite and courteous\" ";
if ( $question8 == "No companies were polite and courteous" ) {
    echo " checked";
} 
echo "/>No companies were polite and courteous<br />
    
    <input name=\"question8\" type=\"radio\" value=\"Some companies were polite and courteous\" ";
if ( $question8 == "Some companies were polite and courteous" ) {
    echo " checked";
} 
echo "/>Some companies were polite and courteous<br />
    
    <input name=\"question8\" type=\"radio\" value=\"Most companies were polite and courteous\" ";
if ( $question8 == "Most companies were polite and courteous" ) {
    echo " checked";
} 
echo "/>Most companies were polite and courteous<br />
    
    <input name=\"question8\" type=\"radio\" value=\"All companies were polite and courteous\" ";
if ( $question8 == "All companies were polite and courteous" ) {
    echo " checked";
} 
echo "/>All companies were polite and courteous<br />
    
	</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">Any comments would like to share?</legend>
    <div class=\"answer\">
    <textarea name=\"comments\" rows=\"10\" cols=\"40\"></textarea>
	</div>
    </fieldset></td>
  </tr>
  <tr>
    <td><br /></td>
  </tr>
  <tr>
    <td><fieldset>
    <legend class=\"title\">Submit Form</legend>
    <div class=\"answer\" align=\"center\"><input name=\"submit_survey\" type=\"submit\" value=\"Submit Survey\" /></div>
    </fieldset></td>
  </tr>
";
?>
</table>


</form>

</td>
  </tr>
</table>
</body>
</html>
