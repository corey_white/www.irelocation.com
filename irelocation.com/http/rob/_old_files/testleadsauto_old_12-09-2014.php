<?php
/* 
************************FILE INFORMATION**********************
* File Name:  testleadsauto.php
* URL: http://www.irelocation.com/rob/testleads.php
**********************************************************************
* Description:  a short script to post AUTO TEST leads into LeadExec from CSV data
**********************************************************************
* Initial Creation Date:  10/20/14
**********************************************************************
* Modifications (Date/Who/Details):
**********************************************************************
*/

$go = $_POST['go'];
$csvinput = $_POST['csvinput'];

if ( isset($go) && $csvinput ) {
    echo "yes<br />";


	$csvinput = str_replace("\r\n","\n",$csvinput);
	$csvinput = str_replace("\r","\n",$csvinput);
    
    $lines = explode("\n", $csvinput);
    
    #explode('\r', $value);
    
	foreach($lines as $value) {

		#echo $value;
		#echo "<br />";
		
		if (strlen($value)<10) break; 

		$data = explode(',', $value);

		$cuid=$data[0];
		$campaign=$data[1];
		$fname=urlencode($data[2]);
		$lname=urlencode($data[3]);
		$email=urlencode($data[4]);
		$phone=urlencode($data[5]);
		$origin_city=urlencode($data[6]);
		$origin_state=urlencode($data[7]);
		$origin_zip=$data[8];
		$destination_city=urlencode($data[9]);
		$destination_state=urlencode($data[10]);
		$destination_zip=$data[11];
		$est_move_date=urlencode($data[12]);
		$vmake=urlencode($data[13]);
		$vmodel=urlencode($data[14]);
		$vyear=$data[15];
		$vtype=urlencode($data[16]);
		$running=$data[17];
		$grouping=$data[18];
		
	
/*
		echo $cuid . "<br />";
		echo $fname . "<br />";
		echo $lname . "<br />";
		echo $email . "<br />";
		echo $phone . "<br />";
		echo $origin_city . "<br />";
		echo $origin_state . "<br />";
		echo $origin_zip . "<br />";
		echo $destination_city . "<br />";
		echo $destination_state . "<br />";
		echo $destination_zip . "<br />";
		echo $est_move_date . "<br />";
		echo $vmake . "<br />";
		echo $vmodel . "<br />";
		echo $vyear . "<br />";
		echo $vtype . "<br />";
		echo $running . "<br />";
		echo $grouping . "<br />";
		echo $campaign . "<br />";
*/
	echo "<br /><br />";
	
switch ( $campaign ) {
    case "auto":
		$send =  "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=1463&AID=2568&LID=366&ClientUID=$cuid&source=lead_test&referrer=&fname=$fname&lname=$lname&email=$email&remote_ip=111.111.111.111&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=phone&vmake=$vmake&vmodel=$vmodel&vyear=$vyear&vtype=$vtype&running=$running&campaign=auto&grouping=$grouping";
        
        break;

    case "atus":
		$send =  "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=1464&AID=2569&LID=699&ClientUID=$cuid&source=lead_test&referrer=&fname=$fname&lname=$lname&email=$email&remote_ip=111.111.111.111&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=phone&vmake=$vmake&vmodel=$vmodel&vyear=$vyear&vtype=$vtype&running=$running&campaign=atus&grouping=$grouping";
        
        break;

    case "auto4":
		$send =  "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=6455&AID=9987&LID=3341&ClientUID=$cuid&source=lead_test&referrer=&fname=$fname&lname=$lname&email=$email&remote_ip=111.111.111.111&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=phone&vmake=$vmake&vmodel=$vmodel&vyear=$vyear&vtype=$vtype&running=$running&campaign=auto4&grouping=$grouping";
        
        break;

    case "auto8":
 		$send =  "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=5458&AID=9978&LID=3336&ClientUID=$cuid&source=lead_test&referrer=&fname=$fname&lname=$lname&email=$email&remote_ip=111.111.111.111&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=phone&vmake=$vmake&vmodel=$vmodel&vyear=$vyear&vtype=$vtype&running=$running&campaign=auto&grouping=$grouping";
       
        break;


}
	

	echo $send;
	echo "<br />";

  $curl = curl_init();
   curl_setopt ($curl, CURLOPT_URL, $send);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

   $result = curl_exec ($curl);
   curl_close ($curl);
   print "<b>" . $result . "</b>";

	$send = "";
	echo "<br /><br />";
	
	}
    

	#echo "<br />print_r:<br />";
	#print_r($lines);


} else {
	echo "Please enter the CSV below.<br />";
}

/*
test data

4068
TESTLEAD
DONOTCALL
TESTLEAD@gmail.com
888-555-1111
Washington
DC
20001
San Jose
CA
95127
2014-10-28
Toyota
Sienna
2000
Mini-Van
Yes
2
auto

4068,auto,TESTLEAD,DONOTCALL,TESTLEAD1@gmail.com,877-555-1001,Washington,DC,20001,San Jose,CA,95127,2014-10-28,Toyota,Sienna,2000,Mini-Van,Yes,99
4068,auto,TESTLEAD,DONOTCALL,TESTLEAD2@gmail.com,877-555-1002,Washington,DC,20001,San Jose,CA,95127,2014-10-28,Toyota,Sienna,2000,Mini-Van,Yes,99
4068,auto,TESTLEAD,DONOTCALL,TESTLEAD3@gmail.com,877-555-1003,Washington,DC,20001,San Jose,CA,95127,2014-10-28,Toyota,Sienna,2000,Mini-Van,Yes,99
4068,auto,TESTLEAD,DONOTCALL,TESTLEAD4@gmail.com,877-555-1004,Washington,DC,20001,San Jose,CA,95127,2014-10-28,Toyota,Sienna,2000,Mini-Van,Yes,99

https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=1463&AID=2568&LID=366&ForceID=$cuid&source=lead_test&referrer=&fname=$fname&lname=$lname&email=$email&remote_ip=111.111.111.111&phone=$phone&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=phone&vmake=$vmake&vmodel=$vmodel&vyear=$vyear&vtype=$vtype&running=$running&campaign=$campaign&grouping=$grouping
*/

?>


<br /><br />
Paste in the CSV:<br />
<form name="input" action="" method="post">
<textarea name="csvinput" cols="160" rows="10"></textarea>
<br />
<input type="hidden" name="go" value="1">
<input type="submit" value="Submit">

</form>




