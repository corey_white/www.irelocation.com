<?php
/* 
************************FILE INFORMATION**********************
* File Name:  testleadsmoving.php
**********************************************************************
* Description:  a short script to post MOVING TEST leads into LeadExec from CSV data
**********************************************************************
* Creation Date:  11/13/14
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/
$go = $_POST['go'];
$csvinput = $_POST['csvinput'];

if ( isset($go) && $csvinput ) {
    echo "yes<br />";


	$csvinput = str_replace("\r\n","\n",$csvinput);
	$csvinput = str_replace("\r","\n",$csvinput);
    
    $lines = explode("\n", $csvinput);
    
    #explode('\r', $value);
    
	foreach($lines as $value) {

		#echo $value;
		#echo "<br />";
		
		if (strlen($value)<10) break; 

		$data = explode(',', $value);

		$cuid=$data[0];
		$cat_id=$data[1];
		$firstname=urlencode($data[2]);
		$lastname=urlencode($data[3]);
		$email=urlencode($data[4]);
		$phone_home=urlencode($data[5]);
		$origin_city=urlencode($data[6]);
		$origin_state=urlencode($data[7]);
		$origin_zip=$data[8];
		$destination_city=urlencode($data[9]);
		$destination_state=urlencode($data[10]);
		$destination_zip=$data[11];
		$est_move_date=urlencode($data[12]);
		$number_bedrooms=urlencode($data[13]);
		$poundage=urlencode($data[14]);
		
	
	echo "<br /><br />";
	$send =  "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=1216&AID=2151&LID=370&source=lead_test&firstname=$firstname&lastname=$lastname&email=$email&remote_ip=11.22.33.44&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=email&number_bedrooms=$number_bedrooms&poundage=$poundage&source_root=tm&cat_id=$cat_id&phone_home=$phone_home&move_type=Household+Move&campaign=topmoving&ClientUID=$cuid";

	echo $send;
	echo "<br />";

  $curl = curl_init();
   curl_setopt ($curl, CURLOPT_URL, $send);
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

   $result = curl_exec ($curl);
   curl_close ($curl);
   print "<b>" . $result . "</b>";

	$send = "";
	echo "<br /><br />";
	
	}
    

	#echo "<br />print_r:<br />";
	#print_r($lines);


} else {
	echo "Please enter the CSV below.<br />";
}

/*
test data


3299
2
TESTLEAD
DONOTCALL
TESTLEAD1@gmail.com
877-555-1001
Washington
DC
20001
San Jose
CA
95127
2014-10-28
3
9000

https://secure.leadexec.net/LeadImport.asmx/LeadReceiver?VID=1216&AID=2151&LID=370&source=lead_test&firstname=$firstname&lastname=$lastname&email=$email&remote_ip=11.22.33.44&origin_city=$origin_city&origin_state=$origin_state&origin_zip=$origin_zip&origin_country=US&destination_city=$destination_city&destination_state=$destination_state&destination_zip=$destination_zip&destination_country=US&est_move_date=$est_move_date&contact=email&number_bedrooms=$number_bedrooms&poundage=$poundage&source_root=tm&cat_id=$cat_id&phone_home=$phone_home&move_type=Household+Move&campaign=topmoving&ClientUID=$cuid

*/

?>


<br /><br />
Paste in the CSV:<br />
<form name="input" action="" method="post">
<textarea name="csvinput" cols="160" rows="10"></textarea>
<br />
<input type="hidden" name="go" value="1">
<input type="submit" value="Submit">

</form>




