<?php
	define(USALARM,8005);
	
	include '../inc_mysql.php';
	
	/*
	$sql = "select * from irelocation.leads_security where quote_id in (431);";
	$rs  = new mysql_recordset($sql);
	
	while($rs->fetch_array())	
	{
		$myarray = formatData($rs->myarray);		
		send_to_usalarm($myarray);
	}
	*/
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
		
		$myarray["maplink"] = getMapLink($myarray);
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}
	
	function send_to_usalarm($myarray)
	{
		mail("jen.luz@citcomm.com","Security System Quote",getHTMLMessage($myarray),"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
		mail("code@irelocation.com","Security System Quote",getHTMLMessage($myarray),"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
	}
	
	function getHTMLMessage($myarray)
	{
		extract($myarray);		
		
		$htmlmsg="<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
		"<tr><td width='200'>Received: </td><td>$received</td></tr>";
		if ($quote_type == "res" )
		{
			$htmlmsg .= "<tr><td width='200'>Lead Type: </td><td>Residential</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name1." ".$name2."</td></tr>";
		}
		else
		{
			$htmlmsg .= "<tr><td width='200'>Lead Type: </td><td>Commercial</td></tr>".
			"<tr><td width='200'>Company Name: </td><td>".$name1."</td></tr>".
			"<tr><td width='200'>Contact Name: </td><td>".$name2."</td></tr>";
		}
		$htmlmsg .= "<tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$email."'>".$email."</a></td></tr>".
		"<tr><td width='200'>Phone Number: </td><td>$phone1</td></tr>".
		"<tr><td width='200'>Alternate Phone: </td><td>$phone2</td></tr>".
		"<tr><td width='200'>Street Address: </td><td><a target='_blank' href='".$maplink."'>".$address.", ".$city.", ".$state_code." ".$zip."</a></td></tr>".
		"<tr><td width='200'>-----</td><td></td></tr>".
		"<tr><td width='200'>Current Needs:</td><td>$current_needs</td></tr>".
		"<tr><td width='200'>Square Footage:</td><td>$sqr_footage</td></tr>".
		"<tr><td width='200'>Own/Rent:</td><td>$own_rent</td></tr>";
		
		if ($quote_type == "res")
			$htmlmsg .= "<tr><td width='200'>Type of Home: </td><td>$building_type</td></tr>";							
		else
			$htmlmsg .= "<tr><td width='200'>Type of Business: </td><td>$building_type</td></tr>".
			"<tr><td width='200'>Number of Locations: </td><td>$num_location</td></tr>";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$htmlmsg .= "<tr><td width='200'>Extra Services:</td><td></td></tr>".
			"<tr><td  colspan='2'><ul>";
			
			if ($fire == 1)
				$htmlmsg .= "<li>Fire Protection</li>";
			if ($access == 1)
				$htmlmsg .= "<li>Access Control System</li>";				
			if ($cctv == 1)
				$htmlmsg .= "<li>CCTV System</li>";
			if (strlen($other_field) > 0)
				$htmlmsg .= "<li>$other_field</li>";
				
			$htmlmsg .= "</ul></td></tr>";
		}
		if (strlen(trim($comments)) > 0)
		{
			$htmlmsg .= "<tr><td width='200'>Additional Comments: </td><td></td></tr>".
			"<tr><td  colspan='2'>$comments</td></tr>";
		}
		$htmlmsg .= $newcomments."</table>";
		
		return $htmlmsg;
	}
	
	function getMapLink($myarray)
	{
		return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
	}		
	
	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "$p_area-$p_prefix-$p_suffix";
		}
		else
			return $phone;
	}
	
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	}	
	
?>