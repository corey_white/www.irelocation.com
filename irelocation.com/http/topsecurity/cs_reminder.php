<?
	//$id = '5';
	include_once '../inc_mysql.php';
	include_once 'retention.php';
	
	function buildTMReminderMessage($id,$width=600)
	{
		
		//-----------Common.---------
		$image = "<img src='http://topmoving.com/images2/logo.gif' alt='TopMoving.com' border='0' />";
		$quote_type = "Full Service Moving Quote";
		$link ="http://www.topmoving.com/moving_quote.php?id=$id";
		$site = "<a href='$link'>www.TopMoving.com</a>";
		//---------------------------

		$message = "<div align='center'><a href='$link'>$image</a></div><br/><br/>";
		
		$message .= "<font face='Arial' size='2px'>Dear Valued Customer,<Br/><br/><p>We saw that you recently visited our site ".
		"however, we noticed that you did not complete your request for a Free No-Obligation $quote_type".
		" from  Allied, Bekins, North American and Wheaton. Therefore, we would like to extend to you ".
		"the opportunity to complete your free online quote request.</P>
		
		<P>Simply, click <a href='$link'>here</a>, and you will be sent directly to the short quote form.</p>";
		
		$message .= "Thank You,<Br/><br/>Mark Anderson<br/>Director of Marketting,<br>$site</font>";
	
		return "<div align='left'><Table width='$width'><tr><td>$message</td></tr></table></div>";
	}
	
	function buildCSReminderMessage($id,$width=600)
	{
		
		
		//-----------Common.---------
		$image = "<img src='http://carshipping.com/image/logo.jpg' alt='CarShipping.com' border='0' />";
		$quote_type = "Car Shipping Quote";
		$link ="http://www.carshipping.com/quote.php?id=$id";
		$site = "<a href='$link'>www.CarShipping.com</a>";
		//---------------------------

		$message = "<div align='center'><a href='$link'>$image</a></div><br/><br/>";
		
		$message .= "<font face='Arial' size='2px'>Dear Valued Customer,<Br/><br/><p>We saw that you recently visited our site ".
		"however, we noticed that you did not complete your request for a Free No-Obligation $quote_type".
		" from Six of the best auto-shippers in the nation. Therefore, we would like to extend to you ".
		"the opportunity to complete your free online quote request.</P>
		
		<P>Simply, click <a href='$link'>here</a>, and you will be sent directly to the short quote form.</p>";
		
		$message .= "Thank You,<Br/><br/>Mark Anderson<br/>Director of Marketting,<br>$site</font>";
	
		return "<div align='left'><Table width='$width'><tr><td>$message</td></tr></table></div>";
	}
	
	function buildTSCReminderMessage($id,$width=600)
	{
		//-----------Common.---------
		$image = "<img src='http://topsecuritycompanies.com/images/tsc-logo.gif' alt='Top Security Companies' border='0' />";
		$quote_type = "Security System Quotes";
		$link ="http://www.topsecuritycompanies.com/residential_security_quote.php?id=$id";
		$site = "<a href='$link'>www.TopSecurityCompanies.com</a>";
		//---------------------------

		$message = "<div align='center'><a href='$link'>$image</a></div><br/><br/>";
		
		$message .= "<font face='Arial' size='2px'>Dear Valued Customer,<Br/><br/><p>We saw that you recently visited our site ".
		"however, we noticed that you did not complete your request for a Free No-Obligation $quote_type".
		" from ADT, Brinks, GE Security and Protection One. Therefore, we would like to extend to you ".
		"the opportunity to complete your free online quote request.</P>
		
		<P>Simply, click <a href='$link'>here</a>, and you will be sent directly to the short quote form.</p>";
		
		$message .= "Thank You,<Br/><br/>Mark Anderson<br/>Director of Marketting,<br>$site</font>";
	
		return "<div align='left'><Table width='$width'><tr><td>$message</td></tr></table></div>";
	}
	
	function getMessage($site,$id)
	{
		echo $site."<br/>";
		switch($site)
		{
			case "tsc":
				return buildTSCReminderMessage($id);
			case "cs":
				return buildCSReminderMessage($id);
			case "tm":
				return buildTMReminderMessage($id);
		}
	
	}	
	
	function run()
	{
		$yesterday = date("YmdHis",time() - 24*60*60);
		$sql = "select * from movingdirectory.retention where received < '$yesterday' and not email_sent";
		echo $sql;
		$rs = new mysql_recordset($sql);
		while ($rs->fetch_array())
		{
			extract($rs->myarray);
			$msg = getMessage($site,getEncryptedId($id));
			send($msg,$email,$site);
			update($id);
			echo $msg;			
			echo "<Br>";
		}
	}
	
	function update($id)
	{
		$sql = "update movingdirectory.retention set email_sent = 1 where id = $id";
		$rs = new mysql_recordset($sql);
	}
	
	function send($msg,$email,$site)
	{
		$headers['tm']['header'] = "From: 'TopMoving.com' <no_reply@topmoving.com>\nContent-Type: text/html; charset=iso-8859-15";
		$headers['tm']['subject'] = 'Your Full Service Moving Quote';
		
		$headers['cs']['header'] = "From: 'CarShipping.com' <no_reply@carshipping.com>\nContent-Type: text/html; charset=iso-8859-15";
		$headers['cs']['subject'] = 'Your Car Shipping Quote';
		$headers['tsc']['header'] = "From: 'TopSecurityCompanies.com' <no_reply@topsecuritycompanies.com>\nContent-Type: text/html; charset=iso-8859-15";
		$headers['tsc']['subject'] = "Your Security Quote";
		
		mail($email,$headers[$site]['subject'],$msg,$headers[$site]['header']);
		
		if ($email != "code@irelocation.com")
			mail("code@irelocation.com","Reminder - ".$headers[$site]['subject'],$msg,$headers[$site]['header']);		
	}
	
	run();
?>
