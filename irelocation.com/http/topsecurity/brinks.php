<?php
	#------------------------------------------------------------------------
	#
	#		Brinks Functions.
	#
	#------------------------------------------------------------------------
	include '../inc_mysql.php';
	//--------------
	//for streaming variables as files to Brinks. see function "send_to_brinks($myarray)".
	include 'Var.php'; 
	stream_wrapper_register( "var", "Stream_Var" );	
	//--------------
	$sql = "select l.* from leads_verified as v join leads_security as l on l.quote_id = v.quote_id where success = 0 and lead_id = 8000;";
	$rs = new mysql_recordset($sql);
	
	//iterate over each lead.
	$count = 0;
	while($rs->fetch_array())	
	{
		$myarray = formatData($rs->myarray);
		//change fields to customer relevant data.		
		send_to_brinks($myarray);	
	}
	
	function send_to_brinks($myarray)
	{
		echo "Sending to Brinks<br/>";
		$filetext = format_for_brinks($myarray);
		if (ftp_to_brinks($filetext,$myarray["quote_id"]))
		{
			//save_response($myarray["quote_id"],"FTP Accepted",BRINKS,1);
			echo "Brinks Lead - Success";
		}
		else
		{
			//save_response($myarray["quote_id"],"FTP Rejected",BRINKS,0);
			echo "Brinks Lead - Failure";
			//sendError("Brinks Lead - Failure:".$myarray["quote_id"]);
		}
	}
	
	function ftp_to_brinks($filetext,$quote_id)
	{
		echo "FTP to Brinks<br/>";
		//Global Variables are turned off, so i have to put it to a scope where
		//Var.php can reach it. SESSION seems most reasonable..
		$_SESSION["text"] = $filetext;		
		$brinks_host = "ftp.brinks.com";
		$login = "irelocation";
		$password = "Jun3bug!";
		$subdir = "In/";
		$ext = ".txt";
		
		
		$fp = fopen('var://SESSION/text','r');
		$conn_id = ftp_connect($brinks_host);
			
		$login_result = ftp_login($conn_id, $login, $password);
		
		// check connection
		if ((!$conn_id) || (!$login_result)) 
		{
			echo "FTP connection has failed!";
			echo "Attempted to connect to $ftp_server for user $ftp_user_name";
			mail("code@irelocation.com","Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
			return false;
		}
		else
		{
			// upload the file
			$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
			
			// check upload status
			if (!$upload) 
			{
				mail("code@irelocation.com","Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				// close the FTP stream
				ftp_close($conn_id);
				fclose($fp);
				return false;
			} 
			
			// close the FTP stream
			ftp_close($conn_id);
			fclose($fp);
			//mail("code@irelocation.com","Brinks FTP Success","FileText: $filetext<br/>QuoteId: $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
			
			return true;
		}
	}
	
	function format_for_brinks($myarray)
	{
		echo "Formatting Data for Brinks<br/>";
		$myarray["sqr_footage"] = str_replace(",","",$myarray["sqr_footage"]);
		$myarray = str_replace(array(",","\n","'")," ",$myarray);
		
		extract($myarray);
		$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
		$lname = substr($name2,0,20);//same with contact name, or long lastname.
		$city = substr($city,0,30);
		$email = substr($email,0,40);
		$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
		$street_number = substr($parts[0],0,20);
		$street_number = substr($street_number,0,19);		
		$street_name = substr($parts[1],0,30);
		if (strlen($phone2) < 10)
			$phone2 = "";
		if (strlen($phone1) < 10)
			$phone1 = "";
			
		if ($own_rent == "Own")
			$renter = 0;
		else
			$renter = 1;
					
		$newcomments = "Type:".(($quote_type=="com")?"Commercial":"Residential")." ";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		
		$newcomments .=	"Building Type: $building_type ";
		$newcomments .=	"Sqr. Footage: $sqr_footage ";
			
		if ($quote_type == "com" && $num_location > 1)
			$newcomments .=	"Num of Locations: $num_location ";
			
		
		if (strlen($comments) > 0)
		{			
			$length = 240 - strlen($newcomments);
			if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
				$newcomments .= "Cust Comments:".$brinks_customer_comments;
			else//trim their comments down
				$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
		}
		
		return "$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,$phone2,$email,$newcomments,$renter";
		
	}
	
	function xmlentities ( $string )
	{
	   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
	}	
	
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
				
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}

	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	}	
		

?>