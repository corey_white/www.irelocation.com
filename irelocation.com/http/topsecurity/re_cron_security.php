<?php
/* 
************************FILE INFORMATION**********************
* File Name:  re_cron_security.php
**********************************************************************
* Description:  This file will resend TOPSECURITY/TOPALARM leads 
**********************************************************************
* Creation Date:  8/1/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: you need to specify which company is going to get the leads by instantiating their class (e.g., new Brinks(), new Gaylord(), etc.)

*/


include "../cronlib/.functions.php";//Generic Functions.	
include ".lib.php";//specific to Security Cron	
include_once '../inc_mysql.php';//Database!	
include "SecurityClasses.php";//all company functions!! :P
define(LIVE,false);

$companies = new FrontPoint();

echo "<pre>";
print_r($companies);
echo "</pre><br />";


$quote_ids = $_REQUEST['quote_id'];

$sql = "select * from irelocation.leads_security where quote_id in ($quote_id) and campaign = 'us' ";
		
echo $sql."<br/>";
//exit();

$rs = new mysql_recordset($sql);

//iterate over each lead.
$count = 0;

while($rs->fetch_array()) {

	$myarray = formatData($rs->myarray);		
	
	print_r($myarray);
	$assigned = $companies->assign($myarray);
	
	if($assigned) { //assign the array, and test them.
		$companies->send();							
		$count++;

	} else {
		echo "Not assigned to company.";
	}
}

if ($count == 0) echo "<Br/>No Leads Processed.<br/>";
else echo "<Br/>$count Leads Processed.<br/>";
?>
