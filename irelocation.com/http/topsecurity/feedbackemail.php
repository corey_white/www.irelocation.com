<?


function sendFeedbackEmail($myarray)
{
	if ($myarray["quote_type"] == "res")
		$name = $myarray["name1"]." ".$myarray["name2"];
	else
		$name  = $myarray["name2"];//business contact.
		
	$from = "feedback@irelocation.com";
	
	
	$to = $myarray['email'];
	//$to = "davidhaveman@gmail.com";
	
	$building = ucwords($myarray['building_type']);
	$review_link = "<a href='http://www.topalarmcompanies.com/feedback.php'>".
						"TopAlarmCompanies.com".
					"</a>";
	
$body = "<table border='2' width='100%' cellspacing='0' cellpadding='0' bordercolor='#000000'>
			<tr>
				<td>
				<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>
					<tr>
						<td align='center' valign='top' colspan='5' >
							<img border='0' src='http://www.irelocation.com/images/irelo%20logo.jpg' width='191' height='79' alt='The iRelocation Network'><br>
							<img border='0' src='http://www.irelocation.com/images/irelo_slogan.gif' width='413' height='79' alt='Partnering customers with premier relocation services since 1998!'>
							<hr>						</td>
					</tr>
					<tr valign='top'>
						<td align='left' valign='top' colspan='5' >
							<b><font face='Arial' size='2'>$name,</font></b>
							<p>
								<b>
									<font face='Arial' size='2'>
									Recently you received multiple security quotes for your $building
								    from our Security comparison site.   Have you chosen a security system as of yet?  Please let us know by taking just a few seconds to visit our site,  
								    $review_link and fill out a review of the security company that installed your system.  If you have not yet completed the process of installing a security system, just come on back when it's done and let us know how they did.  Your feedback is greatly appreciated.									</font>								</b>							</p>
							<p>
								<b>
									<font face='Arial' size='2'>
										Regards,<br/>
										The iRelocation Team									</font>								</b>							</p>
							<p>
								<font face='Arial' size='1'>
									Your information is private and will
						not be distributed to third-party companies.								</font>							</p>						</td>
					</tr>						
					<td width='20'>&nbsp;</td></tr></table></td></tr></table>";
					
	mail($to,"Feedback Requested",$body,"From: $from\nContent-Type: text/html");
}