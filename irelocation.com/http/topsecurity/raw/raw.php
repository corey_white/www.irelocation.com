<?php
	//lead ids.fake for now..
	define(BRINKS,8000);
	define(PROTECT_AMERICA,8002);
	define(PROTECTION_ONE,8003);
	
	include '../inc_mysql.php';
	
	//--------------
	//for streaming variables as files to Brinks. see function "send_to_brinks($myarray)".
	include 'Var.php'; 
	stream_wrapper_register( "var", "Stream_Var" );	
	//--------------
	
	$mytime = updateReceived();//set the timestamp of new leads.
	//add the REPLACE(.. code for brinsk they are dinosaurish
	//$sql = "select *,REPLACE(REPLACE(comments,'\r',' '),'\n',' ') as 'brinks_customer_comments' from irelocation.leads_security where received = '".$mytime."'";//get those leads.
	$sql = "select *,REPLACE(REPLACE(comments,'\r',' '),'\n',' ') as 'brinks_customer_comments' from irelocation.leads_security where quote_id = 58";//get those leads.
	$rs = new mysql_recordset($sql);
	
	//iterate over each lead.
	while($rs->fetch_array())	
	{
		$myarray = formatData($rs->myarray);
		//change fields to customer relevant data.
		
		//send_to_brinks($myarray);
		send_to_protectAmerica($myarray);
		//$lead_ids = BRINKS."|".PROTECT_AMERICA;
		/*
		if (canSendToProtectionOne($myarray["zip"]))
		{
			send_to_protectionOne($myarray);		
			$lead_ids .= "|".PROTECTION_ONE;			
		}
		

		updateLeadIds($lead_ids ,$myarray["quote_id"]);//save it
		
		*/
	}
	
	#------------------------------------------------------------------------
	#
	#		Update Lead ids after the lead has been sent out to all peoples.
	#
	#------------------------------------------------------------------------
	
	function updateLeadIds($ids, $quote_id)
	{		
		$sql="update irelocation.leads_security set lead_ids='".$ids."', ready_to_send = '2' where quote_id = $quote_id";
		$rs=new mysql_recordset($sql);
	}	
		
	#------------------------------------------------------------------------
	#
	#		Update received stamp, this all returns the timestamp
	#		Leads with this timestamp will be sent out.
	#
	#------------------------------------------------------------------------
	
	function updateReceived()
	{
		$mytime = date("YmdHis");
		$sql="update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and scrub_is_valid = 1";
		$rs=new mysql_recordset($sql);
		return $mytime;
	}
	
	#------------------------------------------------------------------------
	#
	#		FORMATTING FUNCTIONS
	#
	#------------------------------------------------------------------------
	
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
		
		$myarray["maplink"] = getMapLink($myarray);
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}
	
	
	
	// XML Entity Mandatory Escape Characters
	function xmlentities ( $string )
	{
	   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
	}	
	
	function save_response($quote_id, $result, $lead_id,$sucess)
	{
		echo "Saving Response from Customer.<br/>";
		$sql = "insert into irelocation.leads_verified (quote_id,result,lead_id,success) values ($quote_id,'$result',$lead_id,$sucess);";
		$rs = new mysql_recordset($sql);	
	}	
	
			
	function sendError($msg,$to="6023179721@cingularme.com")
	{
		mail($to,"TSC-CRON Error",$msg,"From: 'iRelo' <error@iRelo.com>");
	}				
	
	#------------------------------------------------------------------------------------------------------------
	#
	#			BRINKS FORMAT
	#
	#------------------------------------------------------------------------------------------------------------
		
		
	/*
		Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
		
		Data files should be in plain text, comma-delimited format with one record per line.
		The delimiter character can be changed on a case-by-case basis. Given that commas 
		are used to delimit fields, and carriage returns are used to delimit records, care 
		must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
		returns are removed from data before being included in the file.
		
		Additional fields may be added to the specification upon agreement between Brinks and 
		the partner in question. The data file may not be jagged-that is, if a field is null, 
		it must still be included in the output file as an empty string. Each line must contain
		n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.

		The first line of the file should NOT contain field names. Field names are already
		known as long as the aforementioned format is followed.

		The file should contain no blank lines, and no lines with incomplete records.

		
		1.	First name (20 chars max)
		2.	Last name (20 chars max)
		3.	Street Number (20 chars max)
		4.	Street Name  (30 chars max, optional)
		5.	Additional Address (30 chars max, optional)
		6.	City (30 chars max)
		7.	State (2 char abbreviation)
		8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
		9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
		10.	Secondary Phone (optional)
		11.	E-mail address (40 chars max, optional)
		12.	Comment (255 chars max-may not contain carriage returns, optional)
		13.	Renter (1 char, 1 = true 0 = false)

		Sample File: brinks/sample.txt
		
	*/		
	
	#------------------------------------------------------------------------
	#
	#		Brinks Functions.
	#
	#------------------------------------------------------------------------
	
	function send_to_brinks($myarray)
	{
		echo "Sending to Brinks<br/>";
		$filetext = format_for_brinks($myarray);
		if (ftp_to_brinks($filetext,$myarray["quote_id"]))
		{
			save_response($myarray["quote_id"],"FTP Accepted",BRINKS,1);
			echo "Brinks Lead - Success";
		}
		else
		{
			save_response($myarray["quote_id"],"FTP Rejected",BRINKS,0);
			echo "Brinks Lead - Failure";
			sendError("Brinks Lead - Failure:".$myarray["quote_id"]);
		}
	}
	
	function ftp_to_brinks($filetext,$quote_id)
	{
		echo "FTP to Brinks<br/>";
		//Global Variables are turned off, so i have to put it to a scope where
		//Var.php can reach it. SESSION seems most reasonable..
		$_SESSION["text"] = $filetext;		
		$brinks_host = "ftp.brinks.com";
		$login = "irelocation";
		$password = "Jun3bug!";
		$subdir = "In/";
		$ext = ".txt";
		
		$fp = fopen('var://SESSION/text','r');
		$conn_id = ftp_connect($brinks_host);
			
		$login_result = ftp_login($conn_id, $login, $password);
		
		// check connection
		if ((!$conn_id) || (!$login_result)) 
		{
			echo "FTP connection has failed!";
			echo "Attempted to connect to $ftp_server for user $ftp_user_name";
			mail("code@irelocation.com","Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
			return false;
		}
		else
		{
			// upload the file
			$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
			
			// check upload status
			if (!$upload) 
			{
				mail("code@irelocation.com","Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				// close the FTP stream
				ftp_close($conn_id);
				fclose($fp);
				return false;
			} 
			
			// close the FTP stream
			ftp_close($conn_id);
			fclose($fp);
			mail("code@irelocation.com","Brinks FTP Success","FileText: $filetext<br/>QuoteId: $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
			
			return true;
		}
	}
	
	function format_for_brinks($myarray)
	{
		echo "Formatting Data for Brinks<br/>";
		$myarray["sqr_footage"] = str_replace(",","",$myarray["sqr_footage"]);
		$myarray = str_replace(array(",","\n","'")," ",$myarray);
		
		extract($myarray);
		$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
		$lname = substr($name2,0,20);//same with contact name, or long lastname.
		$city = substr($city,0,30);
		$email = substr($email,0,40);
		$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
		$street_number = $parts[0];
		$street_name = $parts[1];
		if (strlen($phone2) < 10)
			$phone2 = "";
		if (strlen($phone1) < 10)
			$phone1 = "";
			
		if ($own_rent == "Own")
			$renter = 0;
		else
			$renter = 1;
					
		$newcomments = "Type:".(($quote_type=="com")?"Commercial":"Residential")." ";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		
		$newcomments .=	"Building Type: $building_type ";
		$newcomments .=	"Sqr. Footage: $sqr_footage ";
			
		if ($quote_type == "com" && $num_location > 1)
			$newcomments .=	"Num of Locations: $num_location ";
			
		
		if (strlen($comments) > 0)
		{			
			$length = 240 - strlen($newcomments);
			if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
				$newcomments .= "Cust Comments:".$brinks_customer_comments;
			else//trim their comments down
				$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
		}
		
		return "$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,$phone2,$email,$newcomments,$renter";
		
	}


	#------------------------------------------------------------------------------------------------------------
	#
	#		PROTECT AMERICA FORMAT
	#
	#------------------------------------------------------------------------------------------------------------

	/*
		Email from Jeff Butler:
			Dave..here is the url post string.  The nodeID value, version value, and source 
			value must always remain the same.  If you can have the key3 value show up as 
			"residential" and "commercial" to ID the lead for which type it is that will work
			well for me.  The rest should be self-explanatory but let me know if you have
			questions.  Also, the lead system we have will send back an XML response to
			your server if the lead is a duplicate or if its missing required info, etc.
			
		URL:
		http://production.leadconduit.com/ap/PostLeadAction?
			xxNodeId=2b43d7b& <!--- constant
			city_name=Round%20Rock&
			email_address=testtester@protectamerica.com&
			first_name=Test&
			last_name=Tester&
			phone1=5122188833&
			phone2=5122188832&
			source=A01405&  <!--- constant
			state_name=TX&
			street_name=5100%20N%20IH35&
			userdef07=YES&  <!--- YES = Own, No = Rent
			userdef74=GOOD& <!--- Credit rating...
			version=9& <!--- constant
			zipcode=78681&
			key3=test <-- tracking field, send them quote_id
		
	*/
	
	function send_to_protectAmerica($myarray)
	{
		echo "Sending to Protect America<br/>";
		extract($myarray);
		
		if ($quote_type == "res")
			$quote_type = "residential";
		else
			$quote_type = "commercial";
			
		if ($own_rent == "own")
			$own_rent = "YES";
		else
			$own_rent = "NO";

		 $faturl = "http://production.leadconduit.com/ap/PostLeadAction";
		 $post_vars = "xxNodeId=2b43d7b&".
			"city_name=".urlencode($city)."&".
			"email_address=".urlencode($email)."&".
			"first_name=".urlencode($name1)."&".
			"last_name=".urlencode($name2)."&".
			"phone1=$phone1&".
			"phone2=$phone2&".
			"source=A01405&".
			"state_name=$state_code&".
			"street_name=".urlencode($address)."&".
			"userdef07=$own_rent&".
			"userdef74=GOOD&".
			"version=9&".
			"zipcode=$zip&".
			"key3=$quote_type";
		
		
		$ch=curl_init("$faturl?$post_vars");	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		$response = curl_exec($ch);
		curl_close($ch);
		$content = "Url: $faturl?$post_vars <br/>".
		"Response: $response<br/>";
		
		if (substr_count(strtolower($response),"lead accepted") == 0)
		{
			mail("code@irelocation.com","ProtectAmerica Lead - Failure",$content ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
			save_response($quote_id, "Failure", PROTECT_AMERICA,0);
			echo "ProtectAmerica Lead - Failure";
		}
		else 
		{
			mail("code@irelocation.com","ProtectAmerica Lead - Success",$content ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");		
			save_response($quote_id, "Lead Accepted", PROTECT_AMERICA,1);
			echo "ProtectAmerica Lead - Success";
		}
	}
	
	#------------------------------------------------------------------------
	#
	#		PROTECTION ONE Functions.
	#
	#------------------------------------------------------------------------
	
	function send_to_protectionOne($myarray)
	{
		echo "Sending to ProtectionOne<br/>";
		$parameters = getProtectionOneParameters($myarray);
		
$body ="<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Header>
<AuthHeaders xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
  <Username>pro1</Username>
  <Password>pro1</Password>
</AuthHeaders>
</soap:Header>
<soap:Body>
<ProtectionOne xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
  <Prospect>
	<Site_type>".$parameters["Site_type"]."</Site_type>
	<Name>". xmlentities($parameters["Name"])."</Name>
	<Address1>". xmlentities($parameters["Address1"])."</Address1>
	<Address2></Address2>
	<City>".xmlentities($parameters["City"])."</City>
	<State>".xmlentities($parameters["State"])."</State>
	<Zip>".$parameters["Zip"]."</Zip>
	<Phone1>".$parameters["Phone1"]."</Phone1>
	<Ext1></Ext1>
	<Phone2>".$parameters["Phone2"]."</Phone2>
	<Ext2></Ext2>
	<Email>".xmlentities($parameters["Email"])."</Email>
	<Comments>".xmlentities($parameters["Comments"])."</Comments>
	<Com_System_Type></Com_System_Type>	
	<promo_id>http://www.topsecuritycompanies.com</promo_id>
  </Prospect>
</ProtectionOne>
</soap:Body>
</soap:Envelope>";
			
		$ch=curl_init("http://www.protectionone.com/ProspectUpdate/Pro1.asmx");				
		$this_header = array("Host: www.protectionone.com", "Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($body), "SOAPAction: \"http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne\"");
		curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$response=curl_exec($ch);		
		curl_close($ch);					
		
		$start = strpos($response,"<Prospect_No>")+strlen("<Prospect_No>");
		$end = strpos($response,"</Prospect_No>");
		if ($start > -1 && $end > $start)
		{		
			$id = substr($response,$start,($end-$start));
			echo "Protection One Lead - Success \n<br/>Response Id: $id<br/>";
			save_response($myarray["quote_id"],  $id, PROTECTION_ONE,1);
			mail("code@irelocation.com,dave@irelocation.com","Protection One Lead Success","Quote Id: ".$myarray["quote_id"]." Response: $id","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
		}
		else
		{			
			echo "Protection One Lead - Failure \n<br/>Quote Id: ".$myarray["quote_id"]."<br/>";
			save_response($myarray["quote_id"], "INVALID", PROTECTION_ONE,0);			
			mail("code@irelocation.com,dave@irelocation.com","Protection One Lead Failure","Quote Id: ".$myarray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
		}
	}		

	function getProtectionOneParameters($myarray)
	{		
		echo "Building ProtectionOne Parameter Array<Br/>";
			
		extract($myarray);
		$parameters = array();
		$parameters["Site_type"] = $quote_type;
		
		if ($quote_type == "res")
			$parameters["Name"] = "$name2, $name1 ";
		else
		{
			$parameters["Name"] = "$name1";//tack Name2 onto comments...
			$commercial = true;
		}		
		$parameters["Address1"] = $address;		
		$parameters["City"] = $city;
		$parameters["State"] = $state_code;
		$parameters["Zip"] = $zip;		
		$parameters["Phone1"] = $phone1;
		if (strlen($phone2) == 10)
			$parameters["Phone2"] = $phone2;			
		
		$parameters["Email"] = $email;

		$newcomments = "";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		
		$newcomments .=	"Building Type: $building_type, ";
		$newcomments .=	"Sqr. Footage: $sqr_footage, ";
			
		if ($commercial)
		{
			$newcomments .=	"Number of Locations: $num_location, ";
			$newcomments .= "Contact Name: $name2";
		}			
		
		
		$newcomments .= "Cust Comments:".$comments;
		$parameters["Comments"] = $newcomments;
						
		return $parameters;
	}
	
	//if it find a row in the table, send it to them.
	function canSendToProtectionOne($zip)
	{
		echo "Checking ProtectionOne Zip-Codes<br/>";
		$sql = "select * from irelocation.protectionone_zips where zip = '$zip'";
		$rs = new mysql_recordset($sql);
		return $rs->fetch_array();	
	}
	
	#------------------------------------------------------------------------
	#
	#		GENERIC MESSAGE FORMAT FUNCTIONS <-- Currently Not Used.
	#
	#------------------------------------------------------------------------
	
	function getHTMLMessage($myarray)
	{
		extract($myarray);		
		
		$htmlmsg="<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
		"<tr><td width='200'>Received: </td><td>$received</td></tr>";
		if ($quote_type == "res")
		{
			$htmlmsg .= "<tr><td width='200'>Lead Type: </td><td>Residential</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name1." ".$name2."</td></tr>";
		}
		else
		{
			$htmlmsg .= "<tr><td width='200'>Lead Type: </td><td>Commercial</td></tr>".
			"<tr><td width='200'>Company Name: </td><td>".$name1."</td></tr>".
			"<tr><td width='200'>Contact Name: </td><td>".$name2."</td></tr>";
		}
		$htmlmsg .= "<tr><td width='200'>E-Mail Address: </td><td><a href='mailto:".$email."'>".$email."</a></td></tr>".
		"<tr><td width='200'>Phone Number: </td><td>$phone1</td></tr>".
		"<tr><td width='200'>Alternate Phone: </td><td>$phone2</td></tr>".
		"<tr><td width='200'>Street Address: </td><td><a target='_blank' href='".$maplink."'>".$address.", ".$city.", ".$state_code." ".$zip."</a></td></tr>".
		"<tr><td width='200'>-----</td><td></td></tr>".
		"<tr><td width='200'>Current Needs:</td><td>$current_needs</td></tr>".
		"<tr><td width='200'>Square Footage:</td><td>$sqr_footage</td></tr>".
		"<tr><td width='200'>Own/Rent:</td><td>$own_rent</td></tr>";
		
		if ($quote_type == "res")
			$htmlmsg .= "<tr><td width='200'>Type of Home: </td><td>$building_type</td></tr>";							
		else
			$htmlmsg .= "<tr><td width='200'>Type of Business: </td><td>$building_type</td></tr>".
			"<tr><td width='200'>Number of Locations: </td><td>$num_location</td></tr>";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$htmlmsg .= "<tr><td width='200'>Extra Services:</td><td></td></tr>".
			"<tr><td  colspan='2'><ul>";
			
			if ($fire == 1)
				$htmlmsg .= "<li>Fire Protection</li>";
			if ($access == 1)
				$htmlmsg .= "<li>Access Control System</li>";				
			if ($cctv == 1)
				$htmlmsg .= "<li>CCTV System</li>";
			if (strlen($other_field) > 0)
				$htmlmsg .= "<li>$other_field</li>";
				
			$htmlmsg .= "</ul></td></tr>";
		}
		if (strlen(trim($comments)) > 0)
		{
			$htmlmsg .= "<tr><td width='200'>Additional Comments: </td><td></td></tr>".
			"<tr><td  colspan='2'>$comments</td></tr>";
		}
		$htmlmsg .= $newcomments."</table>";
		
		return $htmlmsg;
	}

	function getXMLMessage($myarray)
	{	
		extract($myarray);
		//xml formating for a curl post.
		$xmlmsg="<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>".
		"<SecurityLead version=\"1.0\" source=\"iRelocation\">";
			
		if ($quote_type == "res")
		{
			$xmlmsg.="<LeadType>Residential</LeadType>".
			"<Contact>".
			"<Name>".xmlentities ($name1." ".$name2)."</Name>";
		}
		else
		{
			$xmlmsg.="<LeadType>Commercial</LeadType>".
			"<Contact>".
			"<BusinessName>".xmlentities($name1)."</BusinessName>".
			"<ContactName>".xmlentities($name2)."</ContactName>";
		}			
		$xmlmsg .= "<Address>$address</Address>".
		"<City>$city</City>".
		"<State>$state_code</State>".
		"<Zipcode>$zip</Zipcode>".
		"<Phone>$phone1</Phone>".
		"<AlternatePhone>$phone2</AlternatePhone>".			
		"<EmailAddress>$email</EmailAddress>".			
		"</Contact>";		
		$xmlmsg .= "<Details>".
			"<CustomerNeeds>$current_needs</CustomerNeeds>".
			"<SquareFootage>$sqr_footage</SquareFootage>".
			"<OwnRent>$own_rent</OwnRent>";
		if ($quote_type == "res")
			$xmlmsg .= "<TypeOfHome>$building_type</TypeOfHome>";
		else
			$xmlmsg .= "<TypeOfBusiness>$building_type</TypeOfBusiness>".
			"<NumberOfLocations>$num_location</NumberOfLocations>";
			
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$xmlmsg .= "<ExtraServices>";
			$list = array();
			
			if ($fire == 1)
				$list[] = "Fire Protection";
			if ($access == 1)
				$list[] = "Access Control System";				
			if ($cctv == 1)
				$list[] = "CCTV System";
			if (strlen($other_field) > 0)
				$list[] = xmlentities($other_field);
			
			$list = implode(",",$list);
			$xmlmsg .= "$list</ExtraServices>";
		}
			
			$xmlmsg .= "<Comments>".xmlentities($myarray["comments"])."</Comments>".
		"</Details>".
		"</SecurityLead>";
		return $xmlmsg;
	}
	
	function getMapLink($myarray)
	{
		return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
	}		
	
	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "$p_area-$p_prefix-$p_suffix";
		}
		else
			return $phone;
	}
	
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	}	
		
?>