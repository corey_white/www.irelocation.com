<?php
	include '../inc_mysql.php';
	define(PROTECTION_ONE,8003);
	run();	
	
	
	function run()
	{
		$mytime = updateReceived();//set the timestamp of new leads.
		//add the REPLACE(.. code for brinsk they are dinosaurish
		//$sql = "select *,REPLACE(REPLACE(comments,'\r',' '),'\n',' ') as 'brinks_customer_comments' from irelocation.leads_security where received = '".$mytime."'";//get those leads.
		$sql = "select * From irelocation.leads_security where (received like '20060819%' or received like '20060820%') and scrub_is_valid and quote_id != 636";//get those leads.
		$rs = new mysql_recordset($sql);
		
		while($rs->fetch_array())//iterate over each lead.
		{
			$myarray = formatData($rs->myarray);//change fields to customer relevant data.
			
			send_to_protectionOne($myarray);			
		}	
	}
	
	
	
#------------------------------------------------------------------------
	#
	#		PROTECTION ONE Functions.
	#
	#------------------------------------------------------------------------
	
function send_to_protectionOne($myarray)
	{
		echo "Sending to ProtectionOne<br/>";
		$parameters = getProtectionOneParameters($myarray);
		
$body ="<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
<soap:Header>
<AuthHeaders xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
  <Username>pro1</Username>
  <Password>pro1</Password>
</AuthHeaders>
</soap:Header>
<soap:Body>
<ProtectionOne xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
  <Prospect>
	<Site_type>".$parameters["Site_type"]."</Site_type>
	<Name>". xmlentities($parameters["Name"])."</Name>
	<Address1>". xmlentities($parameters["Address1"])."</Address1>
	<Address2></Address2>
	<City>".xmlentities($parameters["City"])."</City>
	<State>".xmlentities($parameters["State"])."</State>
	<Zip>".$parameters["Zip"]."</Zip>
	<Phone1>".$parameters["Phone1"]."</Phone1>
	<Ext1></Ext1>
	<Phone2>".$parameters["Phone2"]."</Phone2>
	<Ext2></Ext2>
	<Email>".xmlentities($parameters["Email"])."</Email>
	<Comments>".xmlentities($parameters["Comments"])."</Comments>
	<Com_System_Type></Com_System_Type>	
	<promo_id>TSCOM</promo_id>
  </Prospect>
</ProtectionOne>
</soap:Body>
</soap:Envelope>";
			
		$ch=curl_init("http://www.protectionone.com/ProspectUpdate/Pro1.asmx");				
		$this_header = array("Host: www.protectionone.com", "Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($body), "SOAPAction: \"http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne\"");
		curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$response=curl_exec($ch);		
		curl_close($ch);					
		
		$start = strpos($response,"<Prospect_No>")+strlen("<Prospect_No>");
		$end = strpos($response,"</Prospect_No>");
		if ($start > -1 && $end > $start)
		{		
			$id = substr($response,$start,($end-$start));
			echo "Protection One Lead - Success \n<br/>Response Id: $id<br/>";
			save_response($myarray["quote_id"],  $id, PROTECTION_ONE,1);
			mail("code@irelocation.com,dave@irelocation.com","Protection One Lead Success","Quote Id: ".$myarray["quote_id"]." Response: $id","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
		}
		else
		{			
			echo "Protection One Lead - Failure \n<br/>Quote Id: ".$myarray["quote_id"]."<br/>";
			save_response($myarray["quote_id"], "INVALID", PROTECTION_ONE,0);			
			mail("code@irelocation.com,dave@irelocation.com","Protection One Lead Failure","Quote Id: ".$myarray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
		}
	}		

	function getProtectionOneParameters($myarray)
	{				
			
		extract($myarray);
		$parameters = array();
		$parameters["Site_type"] = $quote_type;
		
		if ($quote_type == "res")
			$parameters["Name"] = "$name2, $name1 ";
		else
		{
			$parameters["Name"] = "$name1";//tack Name2 onto comments...
			$commercial = true;
		}		
		$parameters["Address1"] = $address;		
		$parameters["City"] = $city;
		$parameters["State"] = $state_code;
		$parameters["Zip"] = $zip;		
		$parameters["Phone1"] = $phone1;
		if (strlen($phone2) == 10)
			$parameters["Phone2"] = $phone2;			
		
		$parameters["Email"] = $email;

		$newcomments = "";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		
		$newcomments .=	"Building Type: $building_type, ";
		$newcomments .=	"Sqr. Footage: $sqr_footage, ";
			
		if ($commercial)
		{
			$newcomments .=	"Number of Locations: $num_location, ";
			$newcomments .= "Contact Name: $name2";
		}			
		
		$newcomments .= "Cust Comments:".$comments;
		$parameters["Comments"] = $newcomments;
						
		return $parameters;
	}
	
	//if it find a row in the table, send it to them.
	function canSendToProtectionOne($zip)
	{
		echo "Checking ProtectionOne Zip-Codes<br/>";
		$sql = "select * from irelocation.protectionone_zips where zip = '$zip'";
		$rs = new mysql_recordset($sql);
		return $rs->fetch_array();	
	}
		
	function save_response($quote_id, $result, $lead_id,$success)
	{
		$sql = "insert into irelocation.leads_verified (quote_id,result,lead_id, success) values ($quote_id,'$result',$lead_id,$success);";
		echo $sql."<br/>";
		$rs = new mysql_recordset($sql);	
	}	
		
		// XML Entity Mandatory Escape Characters
	function xmlentities ( $string )
	{
	   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
	}
		
	function updateReceived()
	{
		$mytime = date("YmdHis");
		$sql="update irelocation.leads_security set received='".$mytime."' where (received='' or received='0')";
		$rs=new mysql_recordset($sql);
		return $mytime;
	}
	
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
		
	
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}
	
	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "$p_area-$p_prefix-$p_suffix";
		}
		else
			return $phone;
	}
	
		
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	}
?>