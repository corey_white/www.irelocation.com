<?php
	$emails = array( "1981" => "dave@irelocation.com", "1957"=> "mainsales@irelocation.com", 
					 "1975" => "code@irelocation.com", "1948" =>"code@irelocation.com");
					 
	$names = array( "1981" => "ADT", "1957" => "Brinks", 
					 "1975" => "Protect America", "1948" =>"Protection One");
	
	$lead_ids = array_keys($emails);
	shuffle($lead_ids);
	
	/*
		Protection one can only handle certain zip codes...
	*/
	function canSendToProtectionOne($zip)
	{
		$zips = array(85028,85281,08260,85280,90210);
		return (array_search($zip,$zips) === TRUE);	
	}
	
?>