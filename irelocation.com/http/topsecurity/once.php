<?php
/*
 * Created on Jan 30, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
  
 function runCronOnce($cronFile)
 {
	 $one_day = 3600*24;
	 $quote_date = date("Ymd");
	 $yesterdays = date("Ymd",time()-$one_day); 
	 
	 $file = "/tmp/".$quote_date.".fdbck";
	 $yesterdays_file = "/tmp".$yesterdays.".fdbck";
	 
	 if (file_exists($yesterdays_file))
	 {
	 	unlink($yesterdays_file);
	 	echo "Deleted yesterdays tmp file.";
	 }
	 
	 if (file_exists($file))
	 {
	 	echo "Task $cronFile has already ran"; 	
	 }
	 else
	 {
	 	$fh = fopen($file,"w");
	 	include $cronFile;
	 	if ($fh)
	 	 	fclose($fh);
	 	
	 } 	 	
 }
 

?>
