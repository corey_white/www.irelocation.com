<?
	include "../inc_mysql.php";
	
	/*
		2 =	Two Story; 
		A =	Apartment;  
		C =	Condominium;  
		M =	Mobil Home;
		O =	Office;
		R =	Ranch;
		S =	Split Level Home;
		TH =	Townhome;
		TR =	Tri-Level Home;
	*/
	function buildingType($building)
	{
		switch($building)
		{
			case "town house": return "TH";			
			case "apartment": return "A";
			case "condo": return "C";
			case "mobile home": return "M";
			case "office": return "O";
			default: return "";
		}
	}
	

	function sqrFootage($type,$sq)
	{

		if ($sq == 0 || $sq == 50)
		{
			if ($type == "C" && $sq == 0) return 2500;
			else if ($type == "C" && $sq == 50) return 20000;
			else if ($type == "R" && $sq == 0) return 1500;
			else if ($type == "R" && $sq == 50) return 10000;
		}
		else
			switch($sq)
			{
				case 15: return 3000;
				case 25: 
				case 30: return 5000;
				case 100: return 50000;
				case 200: return 20000;
				case 500: return "50000+";
			}
			
	}


	function format($thearray)
	{
		$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
		$thearray = str_replace(array(",","\n","'")," ",$thearray);

		
		extract($thearray);
		
		$sitetype = (($quote_type=="com")?"C":"R");
		if ($sitetype == "C")
		{
			list($fname,$lname) = split(" ",$name2);
			$businessname = $name1;	
		}
		else
		{
			$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
			$lname = substr($name2,0,20);//same with contact name, or long lastname.
		}
		$sqrfootage = sqrFootage($sitetype,$thearray["sqr_footage"]);
		$city = substr($city,0,30);
		$email = substr($email,0,40);
		$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
		$street_number = substr($parts[0],0,20);
		$street_number = substr($street_number,0,19);		
		$street_name = substr($parts[1],0,20);
		if (strlen($phone2) < 10)
			$phone2 = "";
		if (strlen($phone1) < 10)
			$phone1 = "";
			
		if ($own_rent == "Own")
			$renter = 0;
		else
			$renter = 1;
					
		$newcomments = "";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		$building = buildingType($building_type);		
			
			
		if ($quote_type == "com" && $num_location > 1)
			$newcomments .=	"Num of Locations: $num_location ";
		$brinks_customer_comments = trim($comments);	
		
		if (strlen(trim($comments)) > 0)
		{			
			$length = 240 - strlen($newcomments);
			if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
				$newcomments .= "Cust Comments:".$brinks_customer_comments;
			else//trim their comments down
				$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
		}
		$e21 = "";
		$e22 = "";
		$e23 = "";
		$e24 = "";
		$e25 = "";
		$e26 = "";
		$e27 = "";
		$e28 = "";
		$e29 = "";
		
		$lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
		"$phone2,$email,$newcomments,$renter,N733,".$thearray["quote_id"].
		",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";			
		
		return trim($lead_body);
	}
	
	$sql = "select * from irelocation.leads_security where quote_id = 8771";
	$rs = new mysql_recordset($sql);
	if($rs->fetch_array())
	{
		echo format($rs->myarray);	
	
	}
	
?>