<?
	define(STATES,"AL|AK|AZ|AR|CA|CO|CT|DE|DC|FL|GA|HI|ID|IL|IN|IA|KS|KY|LA|ME|MD|MA|MI|MN|MS|MO|MT|NE|NV|NH|NJ|NM|NY|NC|ND|OH|OK|OR|PA|RI|SC|SD|TN|TX|UT|VT|VA|WA|WV|WI|WY");
		
	include "../inc_mysql.php";

	function getSql($data)
	{
		if (count($data) == 1)
			$month = $data[0];
		else if (count($data) == 2)
		{
			$month = $data[0];
			$quote_type = $data[1];
		}
		else
		{
			$month = $data[0];
			$quote_type = $data[1];
			$own_rent = $data[2];
		}
	
		$sql = " select 	
					state_code 'state',
					count(*) 'leads' 
				from 
					irelocation.leads_security 
				where 
					received like '$month%'";
		if ($quote_type != "")
			$sql .= " and quote_type = '".$quote_type."' ";
		if ($own_rent != "")
			$sql .= " and own_rent = '".$own_rent."' ";
		
		$sql .= " group by state_code";
		return $sql;
	}

	function loadStates()
	{
		$results = array();
		$states = split("\|",STATES);
		foreach($states as $state)
		{
			$results[$state] = array();
		}
		return array($states,$results);
	}
	
	list($keys,$state_array) = loadStates();
	
	$combinations = array("200703,res","200703,res,rent","200703,res,own","200703,com",
							"200704,res","200704,res,rent","200704,res,own","200704,com");
	foreach($combinations as $combo)
	{
		$sql = getSql(split(",",$combo));

		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
		{
			$state_array[$rs->myarray['state']][$combo] = $rs->myarray['leads'];
		}
		$rs->close();
	}
	
	echo "state\t".implode("\t",$combinations)."\n";
	
	

	for($i=0;$i<count($keys);$i++)
	{
		$state = $keys[$i];
		echo $state;
		foreach($combinations as $combo)		
			echo "\t".$state_array[$state][$combo];
		echo "\n";
	}
	
?>