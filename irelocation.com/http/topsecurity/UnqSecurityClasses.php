<?	
	include_once "SecurityClass.php";
	
	
	/*
		Guardian Email Addresses!
		
		sniedbala@gpsx.net
		cknorr@gpsx.net
		ktucker@gpsx.net          
		gdeehan@gpsx.net
		meakin@gpsx.net
		aryan@gpsx.net
	*/
	
	class Guardian extends SecurityClass
	{
		function Guardian()
		{
			parent::SecurityClass();
			$this->lead_email = "sniedbala@gpsx.net,cknorr@gpsx.net,ktucker@gpsx.net".
								"gdeehan@gpsx.net,meakin@gpsx.net,aryan@gpsx.net";
			$this->lead_id = GUARDIAN;
		}
		
		function format()
		{
			$this->formatHTML();
		}
		
		function send()
		{
			//$this->formatHTML();
			$content = "Content-Type:text/html";
			mail("code@irelocation.com","iRelocation Security Lead",$this->HTMLmessage,$content);
			mail($this->lead_email,"iRelocation Security Lead",$this->HTMLmessage,$content);
		}
		
	}
	
	class Defender extends SecurityClass
	{
		var $lead_email;
		var $lead_id;
		
		function Defender()
		{
			parent::SecurityClass();
			$this->lead_email = "leads@dsauthorizedadtdealer.com";//CHANGE WHEN WE GET IT		
			$this->lead_id = DEFENDER;
		}
	
		function test()
		{
			$invalid_states = " HI AK MT SD ND WY ";
			$valid_state = (substr_count($invalid_states,$this->thearray['state_code']) == 0);
			$valid = $valid_state &&
				     ($this->thearray['own_rent'] == "own") &&
					 ($this->thearray['sqr_footage'] < 500) &&
					 ($this->thearray['curent_needs'] == "new");					 
			return $valid;
		}
	
		function format()
		{
			$this->lead_body = 
			"First Name: ".$this->thearray['name1']."\n".
			"Last Name: ".$this->thearray['name2']."\n".
			"Phone: ".$this->thearray['phone1']."\n".
			"Alternate Phone: ".$this->thearray['phone2']."\n".
			"Address: ".$this->thearray['address']."\n".
			"City: ".$this->thearray['city']."\n".
			"State: ".$this->thearray['state_code']."\n".
			"Zip Code: ".$this->thearray['zip']."\n".
			"Campaign: INT-UNETX-iRLC\n".
			"For Call Back: 800.374.5289\n";		
		}
		
		function send()
		{			
			$this->format();
			$headers = "Cc: awilliams@defenderdirect.com,jhamilton@defenderdirect.com,".
							"kwhipple@defenderdirect.com " . "\r\n".
						"From: topsecurity@irelocation.com";
			mail($this->lead_email,"INT-UNETX-iRLC",$this->lead_body,$headers);			
			//if ($this->lead_email != "code@irelocation.com")
			//	mail(DEGUB_TSC_EMAIL,"INT-UNETX-iRLC",$this->lead_body,
			//			"From: topsecurity@irelocation.com");	
			
		}	
	}
?>