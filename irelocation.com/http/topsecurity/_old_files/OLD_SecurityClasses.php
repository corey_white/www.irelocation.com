<?	
	define(DEGUB_TSC_EMAIL,"code@irelocation.com");

	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}	

	class Pinnacle extends SecurityClass
	{		
		function Pinnacle()
		{
			$this->url = "http://www.alarmtrax.com/pinnacle/leadpost.php";			
			$this->lead_id = PINNACLE;	
			$this->lead_body = "";				
		}
		
		function format()
		{
			$this->lead_body = '<?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?><leads><lead><firstname>';
			$this->lead_body .= xmlentities($this->thearray["name1"]).'</firstname><lastname>'.xmlentities($this->thearray["name2"])."</lastname><address>".xmlentities($this->thearray["address"]);
			$this->lead_body .= "</address><city>".xmlentities(ucwords($this->thearray["city"]))."</city><state>".get_state_name($this->thearray["state_code"]);
			$this->lead_body .= "</state><zip>".$this->thearray["zip"]."</zip><email>".xmlentities($this->thearray["email"])."</email><primaryphone>".$this->thearray["phone1"];
			$this->lead_body .= "</primaryphone><secondaryphone>".$this->thearray["phone2"]."</secondaryphone><besttime>daytime</besttime>";
			if ($this->thearray["quote_type"] == "res")
				$this->lead_body .= "<rentown>".$this->thearray["own_rent"]."</rentown><comments>".xmlentities($this->thearray["comments"])."</comments></lead></leads>";
			else
				$this->lead_body .= "<rentown>".$this->thearray["own_rent"]."</rentown><comments>*Business*".xmlentities($this->thearray["comments"])."</comments></lead></leads>";
		}
		
		function send()
		{
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			//mail("DEGUB_TSC_EMAIL","PINNACLE FORMAT",$this->lead_body);
			curl_setopt($c, CURLOPT_POSTFIELDS, $this->lead_body);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 		
			
			$this->processResponse($page);			
		}
		
		function processResponse($result)
		{
			echo $result;
			if (substr_count($result,"Lead source: TS") > 0)
			{
				// save_response($this->thearray["quote_id"], "Accepted",PINNACLE,1,"tsc");
			}
			else
			{
				// save_response($this->thearray["quote_id"], $result,PINNACLE,0,"tsc");
			}				
		}
	}

	class ProtectAmerica extends SecurityClass
	{				
		function ProtectAmerica()
		{
			$this->url = "http://production.leadconduit.com/ap/PostLeadAction";
			$this->lead_id = PROTECT_AMERICA;
		}
	
		function format()
		{
			extract($this->thearray);
			if ($quote_type == "res")
				$quote_type = "residential";
			else
				$quote_type = "commercial";
				
			if ($own_rent == "own")
				$own_rent = "YES";
			else
				$own_rent = "NO";
				
			 $this->lead_body = "xxNodeId=2b43d7b&".
				"city_name=".urlencode($city)."&".
				"email_address=".urlencode($email)."&".
				"first_name=".urlencode($name1)."&".
				"last_name=".urlencode($name2)."&".
				"phone1=$phone1&".
				"phone2=$phone2&".
				"source=A01405&".
				"state_name=$state_code&".
				"street_name=".urlencode($address)."&".
				"userdef07=$own_rent&".
				"userdef74=GOOD&".
				"version=9&".
				"zipcode=$zip&".
				"key3=$quote_type";
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			//mail("DEGUB_TSC_EMAIL","PA Response","QuoteID: ".$this->myarray["quote_id"]." ".$response);
			if (substr_count(strtolower($response),"the lead was submitted") == 0)
			{
				mail(DEGUB_TSC_EMAIL,"ProtectAmerica Lead - Failure",$content ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
				save_response($this->thearray["quote_id"], "Failure", PROTECT_AMERICA,0,"tsc");
				echo "ProtectAmerica Lead - Failure";
			}
			else 
			{
				save_response($this->thearray["quote_id"], "Lead Accepted", PROTECT_AMERICA,1,"tsc");
				echo "ProtectAmerica Lead - Success";
			}
		}
	}
	
	

	
	class Brinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()//only owners as of 12/5/6 @ 8:45am
		{			
			return ((strtolower($this->thearray["own_rent"]) == "own") || ($this->thearray['quote_type'] == 'com'));
		}
		
		/*	
			Residential
			 <option value="0">0 - 1,499 ft</option>
		  <option value="15">1,500 - 2,999 ft</option>
		  <option value="30">3,000 - 4,999 ft</option>
		  <option value="50">5,000 - 19,999 ft</option>		  
		  <option value="200">20,000+ ft</option>
		  
			Commercial
			<option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>
		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		  <option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>

		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		*/
		/*
		function sqrFootage($type,$sq)
		{

			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		*/
		function Brinks()
		{
			$this->url = "ftp.brinks.com";
			$this->lead_id = BRINKS;
		}	
			
		/* OLD */			
		/*
		function format()
		{
			$this->thearray["sqr_footage"] = str_replace(",","",$this->thearray["sqr_footage"]);
			$this->thearray = str_replace(array(",","\n","'")," ",$this->thearray);
			
			
			extract($this->thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$this->thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own")
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "Type:".(($quote_type=="com")?"Commercial":"Residential")." ";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type ";
			$newcomments .=	"Sqr. Footage: $sqr_footage ";
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
				
			
			if (strlen($comments) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			
			$this->lead_body = "$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,$phone2,$email,$newcomments,$renter,N733,".$this->thearray["quote_id"].",";			
		}
		*/
		
		/* NEW FORMAT */
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own")
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			$e21 = "";
			$e22 = "";
			$e23 = "";
			$e24 = "";
			$e25 = "";
			$e26 = "";
			$e27 = "";
			$e28 = "";
			$e29 = "";
			
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,N733,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";			
			
//			return trim($lead_body);
		}
			
		/* END NEW FORMAT */	
			
		function send()
		{
			$result = true;
			$quote_id = $this->thearray["quote_id"];
			//Global Variables are turned off, so i have to put it to a scope where
			//Var.php can reach it. SESSION seems most reasonable..
			$_SESSION["text"] = $this->lead_body;	
							
			$login = "irelocation";
			$password = "Jun3bug!";
			$subdir = "In/";
			$ext = ".txt";			
			
			$fp = fopen('var://SESSION/text','r');
			$conn_id = ftp_connect($this->url);
				
			$login_result = ftp_login($conn_id, $login, $password);
			
			// check connection
			if ((!$conn_id) || (!$login_result)) 
			{				
				mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				$result = false;
			}
			else
			{
				// upload the file
				$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
				
				// check upload status
				if (!$upload) 
				{
					mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to $ftp_server for user $ftp_user_name \n ".date("YmdHis") ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = false;
				} 
				
				// close the FTP stream
				ftp_close($conn_id);
				fclose($fp);
				//mail(DEGUB_TSC_EMAIL,"Brinks FTP Success","FileText: $filetext<br/>QuoteId: $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
				
				$result = true;
			}
			
			$this->processResponse($result);			
		}
		
		function processResponse($response)
		{			
			if ($response)
			{
				save_response($this->thearray["quote_id"],"FTP Accepted",BRINKS,1,"tsc");				
			}
			else
			{
				save_response($this->thearray["quote_id"],"FTP Rejected",BRINKS,0,"tsc");
			}
		}		
		
		#------------------------------------------------------------------------------------------------------------
		#
		#			BRINKS FORMAT
		#
		#------------------------------------------------------------------------------------------------------------
			
			
		/*
			Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
			
			Data files should be in plain text, comma-delimited format with one record per line.
			The delimiter character can be changed on a case-by-case basis. Given that commas 
			are used to delimit fields, and carriage returns are used to delimit records, care 
			must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
			returns are removed from data before being included in the file.
			
			Additional fields may be added to the specification upon agreement between Brinks and 
			the partner in question. The data file may not be jagged-that is, if a field is null, 
			it must still be included in the output file as an empty string. Each line must contain
			n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.
	
			The first line of the file should NOT contain field names. Field names are already
			known as long as the aforementioned format is followed.
	
			The file should contain no blank lines, and no lines with incomplete records.
	
			
			1.	First name (20 chars max)
			2.	Last name (20 chars max)
			3.	Street Number (20 chars max)
			4.	Street Name  (30 chars max, optional)
			5.	Additional Address (30 chars max, optional)
			6.	City (30 chars max)
			7.	State (2 char abbreviation)
			8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
			9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
			10.	Secondary Phone (optional)
			11.	E-mail address (40 chars max, optional)
			12.	Comment (255 chars max-may not contain carriage returns, optional)
			13.	Renter (1 char, 1 = true 0 = false)
	
			Sample File: brinks/sample.txt
			
		*/				
	}
	
	//Change made: No Commercial Leads, effective 1pm 10/16/06
	/*
	class USAlarm extends SecurityClass
	{
		function USAlarm()
		{
			$this->url = "http://www.usalarm.com/leadspost/irelocation.php";
			$this->lead_id = USALARM;
		}	
			
			

		function test()
		{
			$zip = $this->thearray["zip"];
			$lead_content = "quote_id: ".$this->thearray['quote_id']."\nquote_type: ".$this->thearray["quote_type"]."\nown_rent: ".strtolower($this->thearray["own_rent"])."\nSent?";
			if ($this->thearray["quote_type"] == "res" && strtolower($this->thearray["own_rent"]) == "own")
			{
				//mail("DEGUB_TSC_EMAIL","USAlarm Test", $lead_content );
				
				$sql = "select * from irelocation.protectionone_zips where zip = '$zip' limit 1";
				echo "<br/>\n".$sql."<br/>\n";
				$rs = new mysql_recordset($sql);			
				$result = !($rs->fetch_array());
				$rs->close();
				if ($result)//protection one didnt get it, lets see if they want it...
				{
					$sql = "select * from irelocation.clearlink_zips where zip = '$zip' limit 1";
					echo "<br/>\n".$sql."<br/>\n";
					$rs = new mysql_recordset($sql);			
					$result = ($rs->fetch_array());
					$rs->close();
				}
				echo "Sent: ".($result)?"Yes":"No"."<br/>";
				return $result;
				//the opposite of Protection One.
			}
			else
			{
				return false;
				//mail("DEGUB_TSC_EMAIL","USAlarm Test", $lead_content );		
			}
		}
			
		
			http://www.usalarm.com/leadspost/post.php?kbid=1118&sub=test&FirstName=Bruce&
			LastName=Westenskow&Telephone=8019376964&TelephoneExt=6964&CellPhone=8015806096&
			Address=5202%20Douglas%20Corrigan%20Way&City=Salt%20Lake%20City&State=UT&
			Zip=84116&Email=bruce@clear-link.com&Owner=Y&CurAlarmSys=N&Residential=Y
		
			
		function format()
		{
			extract($this->thearray);		
			$own = (($own_rent == "own")?"Y":"N");		
			$current = (($needs!="upgrade")?"N":"Y");			//so it is new or replace
			
			$fields = "FirstName=".urlencode($name1)."&LastName=".urlencode($name2).
						"&Telephone=$phone1&State=$state_code&kbid=6450";
			$fields .= "&email=".urlencode($email)."&Landline=Y&Owner=".(($own_rent == "own")?"Y":"N").
						"&CurAlarmSys=$current&Residential=".(($quote_type="res")?"Y":"N");
						
			if (strlen($phone2) == 10)
				$fields .= "&CellPhone=$phone2";
				
			return $fields;
		}
	
		function send()
		{	
			$fields = $this->format();
			
			$ch=curl_init($this->url."?".$fields);				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			echo "USalarm Response: ".$response;
			curl_close($ch);
			
			$this->processResponse($response);			
		}
		
		function processResponse($response)
		{			
			
			if ($response == "1")	
			{					
				echo "USalarm Response: Success";	
				save_response($this->thearray["quote_id"], "Valid", USALARM,1,"tsc");
			}
			else
			{							
				echo "USalarm Response: Failure";	
				save_response($this->thearray["quote_id"], "INVALID:\n".$response, USALARM,0,"tsc");			
				mail("dave@irelocation.com","US ALARM Test Lead","Quote Id: ".$this->thearray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
			}
		}		

	}
*/	
	class Gaylord extends SecurityClass
	{
		function Gaylord()
		{
			$this->email = "marketing@gaylordsecurity.com";
			$this->lead_id = GAYLORD;
		}	
		
		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return "2,500 sq. ft.";
				else if ($type == "com" && $sq == 50) return "20,000 sq. ft.";
				else if ($type == "res" && $sq == 0) return "1,500 sq. ft.";
				else if ($type == "res" && $sq == 50) return "10,000 sq. ft.";
				else return $sq;
			}
			else
				switch($sq)
				{
					case 15: return "3,000 sq. ft.";
					case 25: 
					case 30: return "5,000 sq. ft.";
					case 100: return "5,0000 sq. ft.";
					case 200: return "2,0000 sq. ft.";
					case 500: return "5,0000+ sq. ft.";
					default: return "$sq sq. ft.";
				}			
		}			

		function test()
		{
			$zip = $this->thearray["zip"];			
			$sql = "select * from irelocation.protectionone_zips where zip = '$zip' limit 1";
			echo "<br/>\n".$sql."<br/>\n";
			$rs = new mysql_recordset($sql);			
			$result = !($rs->fetch_array());
			$rs->close();
			return $result;
		}
						
		function format()
		{
			extract($this->thearray);		
			
			$pa = substr($phone1,0,3);			
			$pp = substr($phone1,3,3);
			$ps = substr($phone1,6);
			$phone1 = "($pa) $pp - $ps";
			
			if (strlen($phone2) == 10)
			{
				$pa = substr($phone2,0,3);			
				$pp = substr($phone2,3,3);
				$ps = substr($phone2,6);
				$phone2 = "($pa) $pp - $ps";
			}
			
			$square_footage = $this->sqrFootage($quote_type,$sqr_footage);
			$own_rent_yes_no = (($own_rent == "own")?"Yes":"No");		
			//$current = (($needs!="upgrade")?"N":"Y");			//so it is new or replace
			
			$fullname = "$name1 $name2";
			
			$this->headerContent = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name1." <".$email.">\n".
						"Return-Path: '".$fullname."' <".$email.">\n".
						"Content-Type: text; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: ".$received."\n".
						"Lead Type: ".$quote_type."\n".
						"Lead ID: ".$quote_id."\n".
						"\n".
						"Customer: ".$fullname."\n".
						"Email: ".$email."\n".
						"Phone: ".$phone1."\n".
						"Alternate Phone: ".$phone2."\n".
						"Street Address: ".$address."\n".
						"City: ".$city."\n".
						"State: ".$state_code."\n".
						"Zip: ".$zip."\n".
						"Needs: ".$current_needs."\n".
						"Building Type: ".$building_type."\n".
						"Square Footage: ".$square_footage."\n".
						"Own: ".$own_rent_yes_no."\n".
						"Extra Services: ".$exta_services."\n".
						"Customer Comments: ".$comments."";						
		}
	
		function send()
		{	
			/*
			mail("DEGUB_TSC_EMAIL",
					"Top Security Lead",
					$this->message,
					$this->headerContent);
			*/
			mail("marketing@gaylordsecurity.com",
					"Top Security Lead",
					$this->message,
					$this->headerContent);										
		}
		
		function processResponse($response)
		{			
			//email no response :(
	
		}		

	}

	class ProtectionOne extends SecurityClass
	{
		function ProtectionOne()
		{
			$this->url = "http://www.protectionone.com/ProspectUpdate/Pro1.asmx";
			$this->lead_id = PROTECTION_ONE;
		}
		
		function test()
		{
			$zip = $this->thearray["zip"];
								
			$sql = "select * from irelocation.protectionone_zips where zip = '$zip'";
			$rs = new mysql_recordset($sql);
			return $rs->fetch_array();			
		}
	
		function format_fields()
		{		
			extract($this->thearray);
			$parameters = array();
			$parameters["Site_type"] = $quote_type;
			
			if ($quote_type == "res")
				$parameters["Name"] = "$name2, $name1 ";
			else
			{
				$parameters["Name"] = "$name1";//tack Name2 onto comments...
				$commercial = true;
			}		
			$parameters["Address1"] = $address;		
			$parameters["City"] = $city;
			$parameters["State"] = $state_code;
			$parameters["Zip"] = $zip;		
			$parameters["Phone1"] = $phone1;
			if (strlen($phone2) == 10)
				$parameters["Phone2"] = $phone2;			
			
			$parameters["Email"] = $email;
	
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type, ";
			$newcomments .=	"Sqr. Footage: $sqr_footage, ";
				
			if ($commercial)
			{
				$newcomments .=	"Number of Locations: $num_location, ";
				$newcomments .= "Contact Name: $name2";
			}			
			
			
			$newcomments .= "Cust Comments:".$comments;
			$parameters["Comments"] = $newcomments;
							
			return $parameters;	
		}
		
		function format()
		{
			echo "Sending to ProtectionOne<br/>";
			$parameters = $this->format_fields();
			
			$this->lead_body ="<?xml version=\"1.0\" encoding=\"utf-8\"?>
			<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
			<soap:Header>
			<AuthHeaders xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
			  <Username>pro1</Username>
			  <Password>pro1</Password>
			</AuthHeaders>
			</soap:Header>
			<soap:Body>
			<ProtectionOne xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
			  <Prospect>
				<Site_type>".$parameters["Site_type"]."</Site_type>
				<Name>". xmlentities($parameters["Name"])."</Name>
				<Address1>". xmlentities($parameters["Address1"])."</Address1>
				<Address2></Address2>
				<City>".xmlentities($parameters["City"])."</City>
				<State>".xmlentities($parameters["State"])."</State>
				<Zip>".$parameters["Zip"]."</Zip>
				<Phone1>".$parameters["Phone1"]."</Phone1>
				<Ext1></Ext1>
				<Phone2>".$parameters["Phone2"]."</Phone2>
				<Ext2></Ext2>
				<Email>".xmlentities($parameters["Email"])."</Email>
				<Comments>".xmlentities($parameters["Comments"])."</Comments>
				<Com_System_Type></Com_System_Type>	
				<promo_id>TSCOM</promo_id>
			  </Prospect>
			</ProtectionOne>
			</soap:Body>
			</soap:Envelope>";
		}
		
		function send()
		{
			$ch=curl_init($this->url);				
			$this_header = array("Host: www.protectionone.com", "Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($this->lead_body), "SOAPAction: \"http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne\"");
			curl_setopt($ch, CURLOPT_HEADER, 1);	
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->lead_body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
			curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			$response=curl_exec($ch);		
			curl_close($ch);					
			
			echo "<textarea cols='60' rows='30'>$this->lead_body</textarea><Br/>\n";
			echo "<textarea cols='60' rows='30'>$response</textarea><Br/>\n";
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$start = strpos($response,"<Prospect_No>")+strlen("<Prospect_No>");
			$end = strpos($response,"</Prospect_No>");
			if ($start > -1 && $end > $start)
			{		
				$id = substr($response,$start,($end-$start));
				echo "Protection One Lead - Success \n<br/>Response Id: $id<br/>";
				save_response($this->thearray["quote_id"],  $id, PROTECTION_ONE,1,"tsc");
				//mail(DEBUG_TSC_EMAIL,"Protection One Lead Success","Quote Id: ".$myarray["quote_id"]." Response: $id","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
			}
			else
			{			
				echo "Protection One Lead - Failure \n<br/>Quote Id: ".$this->thearray["quote_id"]."<br/>";
				save_response($this->thearray["quote_id"], "INVALID:\n".$response, PROTECTION_ONE,0,"tsc");			
				mail(DEBUG_TSC_EMAIL,"Protection One Lead Failure","Quote Id: ".$this->thearray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
			}
		}		
	}
	
	/*
	
	class Pinnacle extends AlarmTrax
	{
		function Pinnacle()
		{
			parent::AlarmTrax(PINNACLE);
		}	
	}
	
	class GayLord extends AlarmTrax
	{
		function GayLord()
		{
			parent::AlarmTrax(GAYLORD);
		}	
	}
	
	*/
	
	class AlarmTrax extends SecurityClass
	{
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 53;
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}
			else if ($this->lead_id == PINNACLE)
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";		
			}
			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = "";
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: $building_type";
			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
	
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<?xml version='1.0' standalone='yes'?>
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<lead_type_id></lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".$data['name1']."</fname>
					<lname>".$data['name2']."</lname>
					<address1>".$data['address']."</address1>
					<address2></address2>
					<city>".$data['city']."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".$data['comments']."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".$extra_text."</lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			return $xml;
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>

			$response = substr($response,strpos($response,"<body>")+6);
			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
						
			save_response($quote_id,$result,$this->lead_id,$success,"tsc");				
			
			if ($success == 0)
			{
				mail("code@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					"Lead #$quote_id Failed to Be Accepted:\nResult:$response\nLeadXML:".$this->xml);
			}
		}		
	}
?>