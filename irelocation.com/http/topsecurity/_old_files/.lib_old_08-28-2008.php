	<?
	//us lead ids.
	define(BRINKS,1519);//0
	define(PINNACLE,1523);//6
	define(PROTECTION_ONE,1521);//3
	define(PROTECT_AMERICA,1520);	//2
	define(MONITRONICS,1565);//added as of 3:22pm 8/6/7
	define(USALARM,1522);//5
	define(GAYLORD,1552);
	define(FAKE_COMPANY,1981);//for testing only.
	define(APX,1569);

	//austrailian lead ids.
	define(TYCO,1579);
	define(SAFEHOME,1577);
	define(SIGNATURE,1595);
	define(HARVEYNORMAN,1578);
	define(SECURITEX,1622);
	define(PROTECT24,1625);
	define(PINPOINT,1621);
	define(SPECSECSYS,1626);
	define(NATIONAL,1627);
	define(ASASECURITY,1634);

	//canadian lead ids.. they're not worth as much.
	define(VOXCOM,1555);
	define(SAFETECH,1556);
	define(APEXDIRECT,1557);
	define(BELLHOME,1566);
	define(SEARSHOME,1576);
	define(AAAALARMS,1575);

	//-- UK lead ids
	define(ADTUK,1632);
	define(ABELALARM,1631);
	define(DEFENCE,1633);
	
	
	//call center lead ids.
	define(DEFENDER,1567);
	define(GUARDIAN,1568);
	
	//--------------
	//for streaming variables as files to Brinks. see function "send_to_brinks($myarray)".
	include_once 'Var.php'; 
	stream_wrapper_register( "var", "Stream_Var" );	
	//--------------
	
	//keep track of lead results.	
	function recordLeadFailure($leaddata,$lead_id,$campaign,$table)
	{
		$quote_id = $leaddata['quote_id'];
		if ( $table == "marble.auto_quotes")
			$cat_id = 1;
		else if ($table == "irelocation.leads_security")
			$cat_id = 4;
		else
			$cat_id = $leaddata['cat_id'];
			
		$sql = "insert into `movingdirectory`.`failed_leads` 
	(`quote_id`, `cat_id`, `quote_table`, `campaign_name`, 
	`lead_id`,`failed`)
	values
	('$quote_id', '$cat_id', '$table', '$campaign', '$lead_id', 
	".date("YmdHis").");";
		//mail("code@irelocation.com","JTracker Failure SQL",$sql);
		$rs = new mysql_recordset($sql);
	}
	
	//updates all the leads in the queue to have the same received stamp, and returns that received stamp.
	//only work on those leads this time around.
	function updateReceived($campaign="us")
	{
		$mytime = date("YmdHis");
		$sql="update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and campaign = '".$campaign."' limit 10";
		echo $sql."<br/>";
		$rs=new mysql_recordset($sql);
		return $mytime;
	}
	
	
	
	#------------------------------------------------------------------------
	#
	#		FORMATTING FUNCTIONS
	#
	#------------------------------------------------------------------------
	
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
		
//		$myarray["maplink"] = getMapLink($myarray);
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}
?>