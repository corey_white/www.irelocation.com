<?	
	define(DEGUB_TSC_EMAIL,"code@irelocation.com");
	define(APEX_PREFIX,"ap_field_");
	
	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{			
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}		

//Maa

	class Tyco extends SecurityClass
	{

		function test()
		{			
			
				return true;			
		
		}


		function Tyco()
		{
			$this->lead_id = TYCO;
			$this->email = "keobrien@tycoint.com, psimitsopoulos@tycoint.com, kwilliamson@tycoint.com, tamwells@tycoint.com";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Tyco",$this->message,$this->headers);
		}
	
	}
//maa end	
//Maa

	class SafeHome extends SecurityClass
	{

		function test()
		{			
			
				return true;			
		
		}


		function SafeHome()
		{
			$this->lead_id = SAFEHOME;
			$this->email = "sales@1800safehome.com.au, michael@1800safehome.com.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}



	class ASASecurity extends SecurityClass
	{

		function test()
		{			
			
			//-- check if the zip code is one that ASASecurity wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					ASASECURITY."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function ASASecurity()
		{
			$this->lead_id = ASASECURITY;
			$this->email = "alarmsinfo@bigpond.com";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			mail("code@irelocation.com","Security Lead - Copy - ASA Lead",$this->message,$this->headers);
		}
	
	}



	class Protect24 extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that PROTECT24 wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					PROTECT24."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function Protect24()
		{
			$this->lead_id = PROTECT24;
			$this->email = "linda@protect24.com.au";
			//$this->email = "rob@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}
	
	

	class PinPoint extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that PinPoint wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					PINPOINT."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function PinPoint()
		{
			$this->lead_id = PINPOINT;
			$this->email = "sales@pinpointsecurity.com.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}
	
	

	class SpecSecSys extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that Specialised Security Systems wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					SPECSECSYS."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function SpecSecSys()
		{
			$this->lead_id = SPECSECSYS;
			$this->email = "joel@sentry.com.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}
	
	

	class Securitex extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that Securitex wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					SECURITEX."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			//if Securitex one wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function Securitex()
		{
			$this->lead_id = SECURITEX;
			$this->email = "securitex247@bigpond.com";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}


	
	
	class National extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that National Security wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					NATIONAL."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			//if National Security one wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function National()
		{
			$this->lead_id = NATIONAL;
			$this->email = "caleb@nationalsecurity.net.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}


	
	
	class Signature extends SecurityClass
	{

		function test()
		{			
			
				return true;			
		
		}


		function Signature()
		{
			$this->lead_id = SIGNATURE;
			$this->email = "webrequestsyd@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
			
			//-- Determine the sq.m -- need to do this because the form has 'whacked' values 
			switch ( $sqr_footage ) {
				case "200":
					$square_meter = "2000+ sq. m";
					break;
			
				case "50":
					$square_meter = "500 - 2000 sq. m";
					break;
			
				case "30":
					$square_meter = "300 - 500 sq. m";
					break;
			
				case "15":
					$square_meter = "150 - 300 sq. m";
					break;
			
				case "0":
				default:
					$square_meter = "0 - 150 sq. m";
					break;
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Territory: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Meters: ".$square_meter."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			extract($this->thearray);
			// $zip = the origin zip of the lead
			// $quote_type = (duh) quote type
						
			//-- determine which email to send to based on whether it's a RES or COM lead and what the postal code is
			if ( $quote_type == "res" ) {
				$sig_email = "auth_rep@signaturesecurity.com.au";
			} else {
				
				//-- now, check for postal code (sigh)
				$zip_1_char = substr($zip,0,1);

				switch ( $zip_1_char ) {
					case "0": //-- 2xxx, 26xx
						$sig_email = "webrequestbri@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "1": //-- 2xxx, 26xx
						$sig_email = "webrequestsyd@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "2": //-- 2xxx, 26xx
						$sig_email = "webrequestsyd@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "3":
						$sig_email = "webrequestmel@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "4":
						$sig_email = "webrequestbri@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "5":
						$sig_email = "webrequestmel@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "6":
						$sig_email = "webrequestpth@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "7":
						$sig_email = "webrequestmel@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "8":
						$sig_email = "webrequestmel@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					case "9":
						$sig_email = "webrequestbri@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				
					default: // if something else comes through, we need to still send it somewhere
						$sig_email = "webrequestmel@signaturesecurity.com.au,auth_rep@signaturesecurity.com.au";
						break;
				}
				
			}

			
			# mail("rob@irelocation.com","SIGNATURE TYPE/ZIP","From the AuSecurityClasses.php file \n\n zip = $zip \n\n quote_type: $quote_type \n\n sig_email = $sig_email");

			$this->format();
			mail("$sig_email","Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Signature Security Group Home",$this->message,$this->headers);
		}
	
	}
	
	
//maa end
	//added 2/9/8 11:20am.
	class Chubb extends SecurityClass
	{
		function test()
		{			
			return true;				
		}

		function Chubb()
		{
			$this->lead_id = CHUBB;
			$this->email = "chs.sales@chubb.com.au";		
			//$this->email = "code@irelocation.com";				
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			#mail("davidhaveman@gmail.com","Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}
	
	class HarveyNorman extends SecurityClass
	{

		function test()
		{			
			
				return true;			
			
		}


		function HarveyNorman()
		{
			$this->lead_id = HARVEYNORMAN;
			$this->email = "peter@paul-tec.com.au";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - HarveyNorman Home",$this->message,$this->headers);
		}
	
	}
	


	class Safeguard extends SecurityClass
	{

		function test()
		{			
			//-- check if the zip code is one that Safe-Guard wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					SAFEGUARD."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function Safeguard()
		{
			$this->lead_id = SAFEGUARD;
			$this->email = "sales@safe-guard.com.au";
			#$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}



//----
	class TJMSecurity extends SecurityClass
	{

		function test()
		{			
			
			//-- check if the zip code is one that TJM Security wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					TJMSECURITY."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		
		}


		function TJMSecurity()
		{
			$this->lead_id = TJMSECURITY;
			$this->email = "tjmsecurity@bigpond.com";
			//$this->email = "code@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}




	

?>