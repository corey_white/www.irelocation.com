<?	
	define(DEGUB_TSC_EMAIL,"code@irelocation.com");
	define(APEX_PREFIX,"ap_field_");
	
	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{			
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}		



/* THIS IS A SAMPLE CLASS EXTENSION

	//-- make sure you change appropriately the class name (XXXProtect24XXX), the global var (ZZZPROTECT24ZZZ) and the email (XXXX@YYYY.ZZZZ)
	
	class XXXProtect24XXX extends SecurityClass
	{

		function test() //-- this tests to see if this company wants this lead, based on the zip code; if there is no zip code list for the company, simply return TRUE
		{			
			//-- check if the zip code is one that the company wants
			$sql = "select zip from security_zip_codes where lead_id = '".
					ZZZPROTECT24ZZZ."' and zip = '".$this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$they_wants_it = $rs->fetch_array();
			
			// -- if they wants it, return true
			if ($they_wants_it) {
				return true;	
			} else {				
				return false;
			}
		
		}


		function XXXProtect24XXX()
		{
			$this->lead_id = ZZZPROTECT24ZZZ;
			$this->email = "XXXX@YYYY.ZZZZ";
			//$this->email = "rob@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}

*/
	
	class ADTuk extends SecurityClass
	{

		function test() { //-- tests to see if this company wants this lead, based on the zip code; if there is no zip code list for the company, simply return TRUE
			return true;
		}


		function ADTuk()
		{
			$this->lead_id = ADTUK;
			$this->email = "pmiles@tycoint.com";
#			$this->email = "rob@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}
	


	class AbelAlarm extends SecurityClass
	{

		function test() //-- this tests to see if this company wants this lead, based on the zip code; if there is no zip code list for the company, simply return TRUE
		{			
			return true;	
		}


		function AbelAlarm()
		{
			$this->lead_id = ABELALARM;
			$this->email = "elaine.pacey@abelalarm.co.uk";
#			$this->email = "rob@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}



	class Defence extends SecurityClass
	{

		function test() //-- this tests to see if this company wants this lead, based on the zip code; if there is no zip code list for the company, simply return TRUE
		{			
			return true;	
		}


		function Defence()
		{
			$this->lead_id = DEFENCE;
			$this->email = "chris.gibson@defencesecurity.co.uk,rob@irelocation.com";
#			$this->email = "rob@irelocation.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - SafeHome Home",$this->message,$this->headers);
		}
	
	}

?>