<?	
	define(DEGUB_TSC_EMAIL,"code@irelocation.com");
	define(APEX_PREFIX,"ap_field_");
	
	
	
	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{			
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}		

//Maa

	class Aaaalarms extends SecurityClass
	{

		function test()//only Manatoba and ALberta until 1/31/2008
		{			
			if($this->thearray['state_code'] == "AB" || $this->thearray['state_code'] == "MB")
			{
				return true;			
			}
			else
				return false;
		}


		function Aaaalarms()
		{
			$this->lead_id = AAAALARMS;
			$this->email = "cindy.thompson@aaaalarms.ca";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Aaaalarms",$this->message,$this->headers);
		}
	
	}
//maa end	
//Maa

	class Searshome extends SecurityClass
	{

		function test()//test against exclusion zip code list
		{			
					
			$prefix = substr($this->thearray['zip'],0,3);
			$firstchar = substr($prefix,0,1);
				
			if ($firstchar == "M") return true;
				
			if ($firstchar == "L") {
			
				//-- Set up the list of bad prefixes
				$bad_prefix = array("L3V", "L0K", "L0L", "L0M", "L0N", "L0R", "L0S", "L2A", "L2E", "L2G", "L2H", "L2J", "L2M", "L2N", "L2P", "L2R", "L2S", "L2T", "L2V", "L2W", "L3K", "L3B", "L3C");
				
				if ( in_array($prefix,$bad_prefix) ) {
    				return false; //-- prefix not allowed
				} else {
   					return true; //-- prefix allowed
				}
			} else {
				return false; // Accept no other zipcodes
	
			}
				
			
		}


		function Searshome()
		{
			$this->lead_id = SEARSHOME;
			$this->email = "abaum@searshomealarm.ca, jdickson@searshomealarm.ca";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Sears Home",$this->message,$this->headers);
		}
	
	}
//maa end
	
	class Brinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()//only BC Leads as of 6/1/7
		{			
			if($this->thearray['state_code'] == "BC")
			{
				$own_rent = $this->thearray['own_rent'];
				$type = $this->thearray['quote_type'];
				
				if ($own_rent == "Own" || $type == "com" )
					return true;			
			}
			else
				return false;
		}
		
		/*	
			Residential
			 <option value="0">0 - 1,499 ft</option>
		  <option value="15">1,500 - 2,999 ft</option>
		  <option value="30">3,000 - 4,999 ft</option>
		  <option value="50">5,000 - 19,999 ft</option>		  
		  <option value="200">20,000+ ft</option>
		  
			Commercial
			<option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>
		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		  <option value="0">0 - 2,499 ft</option>
		  <option value="25">2,500 - 4,999 ft</option>
		  <option value="50">5,000 - 9,999 ft</option>

		  <option value="100">10,000 - 49,999 ft</option>		  
		  <option value="500">50,000+ ft</option>
		*/
		/*
		function sqrFootage($type,$sq)
		{

			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		*/
		function Brinks()
		{
			$this->url = "ftp.brinks.com";
			$this->lead_id = BRINKS;
		}	
			
		/* OLD */			
		/*
		function format()
		{
			$this->thearray["sqr_footage"] = str_replace(",","",$this->thearray["sqr_footage"]);
			$this->thearray = str_replace(array(",","\n","'")," ",$this->thearray);
			
			
			extract($this->thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$this->thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own")
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "Type:".(($quote_type=="com")?"Commercial":"Residential")." ";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type ";
			$newcomments .=	"Sqr. Footage: $sqr_footage ";
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
				
			
			if (strlen($comments) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			
			$this->lead_body = "$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,$phone2,$email,$newcomments,$renter,N733,".$this->thearray["quote_id"].",";			
		}
		*/
		
		/* NEW FORMAT */
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			$e21 = "";
			$e22 = "";
			$e23 = "";
			$e24 = "";
			$e25 = "";
			$e26 = "";
			$e27 = "";
			$e28 = "";
			$e29 = "";
			
			if ($this->thearray["source"] == "aff_p4p" || $this->thearray["source"] == "aff_rlc")
				$marketsource = "N751";
			else			
				$marketsource = "N733";			
			
			
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,$marketsource,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";		
			//mail("code@irelocation.com","Brinks #".$thearray["quote_id"],$this->lead_body);
		}
			
		/* END NEW FORMAT */	
			
		function send()
		{
			$result = true;
			$quote_id = $this->thearray["quote_id"];
			//Global Variables are turned off, so i have to put it to a scope where
			//Var.php can reach it. SESSION seems most reasonable..
			$_SESSION["text"] = $this->lead_body;	
							
			$login = "irelocation";
			$password = "Jun3bug!";
			$subdir = "In/";
			$ext = ".txt";			
			
			$fp = fopen('var://SESSION/text','r');
			$conn_id = ftp_connect($this->url);
				
			$login_result = ftp_login($conn_id, $login, $password);
			
			// check connection
			if ((!$conn_id) || (!$login_result)) 
			{				
				#mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				$result = false;
			}
			else
			{
				// upload the file
				$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
				
				// check upload status
				if (!$upload) 
				{
					#mail(DEGUB_TSC_EMAIL,"Brinks FTP Failure","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = false;
					recordLeadFailure($this->thearray,1519,
							"casecurity","irelocation.leads_security");
				} 
				else
				{
					mail("leads@brinks.com","iRelocation - $quote_id",$this->lead_body);
					#mail("code@irelocation.com","Brinks Lead - $quote_id",$this->lead_body);					
				
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					//mail(DEGUB_TSC_EMAIL,"Brinks FTP Success","FileText: $filetext<br/>QuoteId: $quote_id" ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");
					
					$result = true;
				}	
			}
			
			$this->processResponse($result);			
		}
		
		function processResponse($response)
		{			
			if ($response)
			{
				save_response($this->thearray["quote_id"],"FTP Accepted",BRINKS,1,"ctsc");				
			}
			else
			{
				save_response($this->thearray["quote_id"],"FTP Rejected",BRINKS,0,"ctsc");
			}
		}		
		
		#------------------------------------------------------------------------------------------------------------
		#
		#			BRINKS FORMAT
		#
		#------------------------------------------------------------------------------------------------------------
			
			
		/*
			Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
			
			Data files should be in plain text, comma-delimited format with one record per line.
			The delimiter character can be changed on a case-by-case basis. Given that commas 
			are used to delimit fields, and carriage returns are used to delimit records, care 
			must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
			returns are removed from data before being included in the file.
			
			Additional fields may be added to the specification upon agreement between Brinks and 
			the partner in question. The data file may not be jagged-that is, if a field is null, 
			it must still be included in the output file as an empty string. Each line must contain
			n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.
	
			The first line of the file should NOT contain field names. Field names are already
			known as long as the aforementioned format is followed.
	
			The file should contain no blank lines, and no lines with incomplete records.
	
			
			1.	First name (20 chars max)
			2.	Last name (20 chars max)
			3.	Street Number (20 chars max)
			4.	Street Name  (30 chars max, optional)
			5.	Additional Address (30 chars max, optional)
			6.	City (30 chars max)
			7.	State (2 char abbreviation)
			8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
			9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
			10.	Secondary Phone (optional)
			11.	E-mail address (40 chars max, optional)
			12.	Comment (255 chars max-may not contain carriage returns, optional)
			13.	Renter (1 char, 1 = true 0 = false)
	
			Sample File: brinks/sample.txt
			
		*/				
	}
	
	class BellHome extends TextEmail
	{	
		function BellHome()
		{
			$this->lead_id = BELLHOME;
			parent::TextEmail("securityleads@bellhomemonitoring.ca");
		}					
		
		function test() 
		{  					
			return true;
		}
		
		
		function send()
		{
			/*
			$this->format();
			if ($this->thearray['source'] == "aff_p4p"  || $this->thearray["source"] == "aff_rlc")							
				$subject = "OB - Security Lead";		
			else					
				$subject = "Security Lead";
				
		
			mail($this->,$subject,$this->message,$this->headers);
			*/
			//	mail("code@irelocation.com","Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
	}
	
	class FakeCompany extends TextEmail
	{
	
		function FakeCompany()
		{
			$this->lead_id = -1;
			parent::TextEmail(DEGUB_TSC_EMAIL);
		}	
		
		function send()
		{
			//parent::send();
			$this->formatHTML();
			
/*
			mail(DEGUB_TSC_EMAIL,"Top Security Lead - HTML",
				 $this->HTMLmessage,$this->HTMLheaders);
*/		
		}
		
		function test() { return true; }
	}
	
	class Voxcom extends TextEmail//1555
	{
		function Voxcom()
		{
			$this->lead_id = VOXCOM;
			parent::TextEmail("customer.support@voxcom.com,Patti.Mcdougall@protectron.com,martin.lamothe@protectron.com,steve.kular@voxcom.com,rebecca.nippard@voxcom.com,jesse.yuen@voxcom.com");
		}	
		
		
		function send()
		{			
			$this->format();
			if ($this->thearray['source'] == "aff_p4p"  || $this->thearray["source"] == "aff_rlc")
			{
				$this->message = str_replace("Source: irelocation.com",
											 "Source: irelo - call center",
											 $this->message);
			}	
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);			
		}
		
		function test() { 
			//-- Reliance Protectron (1555), formerly Voxcom, only wants leads that FastLink doesn't want.  Fast link doesn't want the below postal prefixes in Ontario, so I simply copied the Fast Link code and reversed the true/false conditions.
			
			if($this->thearray['state_code'] == "ON" ) {
				$prefix = substr($this->thearray['zip'],0,3);

				//-- Set up the list of bad prefixes
				$good_prefix = array("P8N", "P5A", "P5E", "P9A", "P3L", "P3P", "K6A", "P5N", "P9N", "P2N", "P3Y", "K8A", "K8B", "K8H", "P6A", "P6B", "P6C", "P8T", "P2B", "P3A", "P3B", "P3C", "P3E", "P3G", "P7C", "P7E", "P7J", "P7K", "P7L", "P7A", "P7B", "P7G", "P4N", "P4P", "P4R", "P3N", "P0T", "K6T", "K6V", "K7C", "K6H", "K6J", "K6K", "K4C", "K4P", "K6A", "K4M", "K4B", "K2K", "K2L", "K2M", "K2T", "K2V", "K2W", "K2H", "K1C", "K1E", "K1W", "K1A", "K1Y", "K1Z", "K2A", "K2B", "K1B", "K1G", "K1H", "K1X", "K1J", "K1K", "K4A", "K1L", "K1M", "K1N", "K2S", "K2C", "K2E", "K2G", "K2J", "K2R", "K1P", "K1R", "K2P", "K1S", "K1T", "K1V", "K8A", "K8B", "K7H", "K8H", "K7V", "K4K", "K4R", "K7A");
				
				if ( in_array($prefix,$good_prefix) ) {
    				return true; //-- prefix not allowed
				} else {
   					return false; //-- prefix allowed
				}

			} else {
				return true;	
			}
					
		}
	}
	
	class SafeTech extends TextEmail//1556
	{
		function SafeTech()
		{
			$this->lead_id = SAFETECH;
			parent::TextEmail("sean@safetechalarms.com,maria@safetechalarms.com");
		}	
		
		function test()//only 236K postal codes
		{
			$postal_code = $this->thearray['zip'];
			

			$sql = "select state from movingdirectory.safetech_areas where ".
					" postalcode = '$postal_code' and state != 'BC' limit 1   ";
			$rs = new mysql_recordset($sql);
			
			$safetech_zip = ($rs->fetch_array());
		
			$now_az = gmdate("Ymdhi",time()+5*60*60);	
			$midnight_az = '2007120100';
			if ($now_az >= $midnight_az)
				$province = ($this->thearray['state_code'] != "AB");			
			else
				$province = true;
			
			return ($safetech_zip && $province );
		}
		
		function send()
		{
			$this->format();
			if ($this->thearray['quote_type'] == "res")							
				$subject = "Residential ";		
			else					
				$subject = "Commercial ";
				
		
			mail($this->emailaddress,$subject."Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com",$subject."Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
			$this->autoResponder();
		}
		
		function autoResponder()
		{
			$text = "We have received your request for information; one of our advisors will contact you shortly.
	
We appreciate your business.

Regards

Safetech 
	
	";	
			$headers = "Content-Type: text/plain; \n".
					"From: \"Top Alarm Companies\" <autoresponder@irelocation.com>";
					
			//mail("code@irelocation.com","Safetech Alarms",$text,$headers);
			mail($this->myarray['email'],"Safetech Alarms",$text,$headers);
		}
	}
	
	class ApexDirect extends TextEmail//1557
	{
		var $data;
		
		//Shuts off leads at 5pm 10/31.

		
		function ApexDirect()
		{
			$this->lead_id = APEXDIRECT;
/*
#			$this->url = "https://www.aimpromote.com/remote/form_post.php";
			$this->url = "https://www.aimcrm.com/remote/form_post.php";
			parent::TextEmail("leads@apexdirect.ca");
			$this->data = array
			(		
				"ss_id" => 2321,
				"action" => "feedback_post_add",
				"ostat_id" => 1,
				"org_id" => 334,
				"c_id" => 662
				//"ap_field_11697" => "123456"
			);
*/
		}	
		
		function test() 
		{
			/*
			$own_rent = $this->thearray['own_rent'];
			$type = $this->thearray['quote_type'];
			echo "own_rent = ".$own_rent."<br/>";
			echo "type = ".$type."<br/>";
			
			$callcenter = ($this->thearray['source'] == "aff_rlc");
			
			if ($own_rent == "Own" || $type == "com" && !$callcenter)
				return true;
			return false;
			*/
			
			$postal_code = $this->thearray['zip'];
			$now_month = date("Ym");
			$type = $this->thearray['quote_type'];
			$own_rent = $this->thearray['own_rent'];

			if ( $type == "com" || $own_rent == "Rent" ) { //-- only want RES leads and no Renters
				echo "ApexDirect doesn't want Commercial leads OR Renters<br />";
				return false;
			} else {
				
				$sql = "select state from movingdirectory.apexdirect_zips where postalcode = '$postal_code'  limit 1   ";
				$rs = new mysql_recordset($sql);
				$apexdirect_zip = ($rs->fetch_array());
				
				//-- get total sent so far this month
				$sql1557 = " select count(*) as count from leads_security where lead_ids like '%1557%' and campaign = 'ca' and left(received,6) = '$now_month' ";
				echo "$sql1557<br />";
				$rs1557 = new mysql_recordset($sql1557);
				$rs1557->fetch_array();
				$monthly_count = $rs1557->myarray["count"];
				
				//-- get how many they can have
				$sql1557b = " select monthly_goal from movingdirectory.campaign where lead_id =1557 and month = '$now_month' ";
				echo "$sql1557b<br />";
				$rs1557b = new mysql_recordset($sql1557b);
				$rs1557b->fetch_array();
				$monthly_goal = $rs1557b->myarray["monthly_goal"];
				
				echo "monthly_count = $monthly_count<br />";
				echo "monthly_goal = $monthly_goal<br />";
				
				if ( $monthly_count <= $monthly_goal  ) {
					$ok_to_send_1557 = true;
				} else {
					$ok_to_send_1557 = false;
				}
				echo "ok_to_send_1557 = $ok_to_send_1557<br />";
			
				return ( $apexdirect_zip && $ok_to_send_1557 );
			}

		}
		
		function format()
		{
			parent::format();
		
			$this->data[APEX_PREFIX."11697"] = $this->thearray['quote_id'];
			$this->data[APEX_PREFIX."11698"] = $this->thearray['received'];
			$this->data[APEX_PREFIX."11699"] = $this->thearray['quote_type'];
			$this->data[APEX_PREFIX."11700"] = $this->thearray['name1'];
			$this->data[APEX_PREFIX."11701"] = $this->thearray['name2'];
			$this->data[APEX_PREFIX."11702"] = $this->thearray['phone1'];
			$this->data[APEX_PREFIX."11703"] = $this->thearray['phone2'];
			//$this->data[APEX_PREFIX."11772"] = $this->thearray['email'];
			$this->data[APEX_PREFIX."11394"] = $this->thearray['email'];
			$this->data[APEX_PREFIX."11704"] = $this->thearray['address'];
			$this->data[APEX_PREFIX."11774"] = $this->thearray['city'];
			$this->data[APEX_PREFIX."11705"] = $this->thearray['state_code'];
			$this->data[APEX_PREFIX."11706"] = $this->thearray['zip'];
			$this->data[APEX_PREFIX."11707"] = $this->thearray['current_needs'];
			$this->data[APEX_PREFIX."11708"] = $this->thearray['building_type'];
			$this->data[APEX_PREFIX."11709"] = $this->thearray['num_location'];
			$this->data[APEX_PREFIX."11769"] = $this->thearray['sqr_footage'];
			$this->data[APEX_PREFIX."11710"] = $this->thearray['own_rent'];
			$this->data[APEX_PREFIX."11711"] = $this->thearray['fire'];
			$this->data[APEX_PREFIX."11739"] = $this->thearray['basement'];
			$this->data[APEX_PREFIX."11740"] = $this->thearray['prewired'];
			$this->data[APEX_PREFIX."11741"] = $this->thearray['access'];
			$this->data[APEX_PREFIX."11771"] = $this->thearray['cctv'];
			$this->data[APEX_PREFIX."11773"] = $this->thearray['other_field'];
			$this->data[APEX_PREFIX."11770"] = $this->thearray['comments'];	
		}
		
		function send() { //-- ApexDirect doesn't want posting anymore, only the emails
/*
			$callcenter = ($this->thearray['source'] == "aff_rlc");
			$prefix = ($callcenter)?"Call Center - ":"";
*/
			
			//parent::send(); <- send the text email to leads@apexdirect.ca
			
/*
			$this->format();
			mail($this->emailaddress,$prefix."Security Lead",$this->message,$this->headers);
*/
			
			$this->formatHTML();
			
			mail("leadsapexdirect@gmail.com,joel@apexdirect.ca",$prefix."Top Security Lead",
					$this->HTMLmessage,$this->HTMLheaders);
					
/*
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $this->url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->data);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			
			echo "CURL RESPONSE = $response<br />";
				
			$this->processResponse($response);	
			
			return $response;
*/
		}
		
		function processResponse($response)
		{			
			//mail("code@irelocation.com","Apex Response Code",$response);
			if (is_numeric($response))
			{
				save_response($this->thearray["quote_id"],$response,
								APEXDIRECT,1,"ctsc");	
				echo "Apex Lead Accepted<br/>";			
			}
			else
			{
				//mail("code@irelocation.com","Apex Lead Rejected:",$response."\nQuote ID: ".$this->thearray['quote_id']);
				save_response($this->thearray["quote_id"],"Lead Rejected",
								APEXDIRECT,0,"ctsc");
				echo "Apex Lead Rejected<br/>";	
			}
		}		
	}

	class TextEmail extends SecurityClass
	{
		function TextEmail($email)
		{
			$this->emailaddress = $email;//;
		}
		
		function sqrFootage($type,$sq)
		{
			echo "sqrFootage($type,$sq)<br/>";
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return 2500;
				else if ($type == "com" && $sq == 50) return 20000;
				else if ($type == "res" && $sq == 0) return 1500;
				else if ($type == "res" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		
		function send()
		{
			$this->format();
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function formatHTML()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			echo "Extras: F: $fire A: $access C: $cctv P: $prewired B: $basement <br/>";
			
			echo "Fire:-$fire- <br/>";
			
			$extra_services = array();
			if ($fire == "1")
				$extra_services[] = "fire";
			if ($access  == "1")
				$extra_services[] = "access";
			if ($cctv  == "1")
				$extra_services[] = "cctv";
			if ($prewired  == "1")
				$extra_services[] = "house is prewired";
			if ($basement  == "1")
				$extra_services[] = "basement is finished";		
			
			if (count($extra_services) > 0)
				$extras = implode(",",$extra_services);
			echo "Extras: $extras <br/>";
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->HTMLheaders = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/html; charset=iso-8859-15";
						
			$this->HTMLmessage = 
			"<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
			"<tr><td colspan='2'><strong>Top Security Lead</strong></td></tr>".
			"<tr><td colspan='2'><hr width='250' align='left'/></td></tr>".
			"<tr><td width='200'>Quote ID: </td><td>".$quote_id."</td></tr>".
			"<tr><td width='200'>Received: </td><td>".$received."</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name."</td></tr>".
			"<tr><td width='200'>E-Mail Address: </td><td>".$email."</td></tr>".
			"<tr><td width='200'>Phone Number: </td><td>".$phone1."</td></tr>".
			"<tr><td width='200'>Alternate Phone: </td><td>".$phone2."</td></tr>".
			"<tr><td width='200'>Street Address: </td><td>".$address."</td></tr>".
			"<tr><td width='200'>City: </td><td>".$city.", ".$state_code."</td></tr>".
			"<tr><td width='200'>Postal Code: </td><td>".$zip."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Quote Type: </td><td>".$type."</td></tr>".
			"<tr><td width='200'>Building Type </td><td>".$building_type."</td></tr>".
			"<tr><td width='200'>Square Footage: </td><td>".$sqr_footage."</td></tr>".
			"<tr><td width='200'>Owners: </td><td>".$own_rent."</td></tr>".
			"<tr><td width='200'>Requested Services/Features: </td><td>".$extras."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td colspan='2' align='left'>Comments:</td></tr>".
			"<tr><td colspan='2' align='left'>".$comments."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Lead Source: </td><td>iRelocation</td></tr>".
			"</table>";	
		
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
	}
	
	class AlarmTrax extends SecurityClass
	{
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 83;
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}					
			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = array();
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: ".$data['building_type'];

			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
			
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<"."?xml version='1.0' standalone='yes'?".">
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<lead_type_id>".$this->lead_type_id."</lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".xmlentities($data['name1'])."</fname>
					<lname>".xmlentities($data['name2'])."</lname>
					<address1>".xmlentities($data['address'])."</address1>
					<address2></address2>
					<city>".xmlentities($data['city'])."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".xmlentities($data['comments'])."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".xmlentities($extra_text)."</lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			#mail("davidhaveman@gmail.com","CA AlarmTrax XML",$xml);
			return $xml;
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			//mail("code@irelocation.com","AlarmTrax XML",$xml);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			//mail("code@irelocation.com","AlarmTrax Content",$xmlcontent);
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>
			$rawresponse = $response;
			$response = substr($response,strpos($response,"<body>")+6);
			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
			if ($success == 0)
			{
				//new format?
				if (substr_count($response,"success=\"true\"") > 0)
				{
					$success = 1;
					$start = strpos($response,"<customer_id>")+13;
					$end = strpos($response,"</",$start);
					if ($end > $start && $start != -1)
						$responseId = substr($response,$start,$end);
				
					else
					{
						$success = 0;
						$responseId = "Success? - $response";
/*
						mail("code@irelocation.com","AlarmTrax Failure? - ".$this->lead_id,
						"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
						"\nLeadXML:".$this->xml);
*/
						recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
					}
					
					save_response($quote_id,$responseId,$this->lead_id,$success,"ctsc");	
				}
				else
				{
					$success = 0;
					$responseId = "failure - $response";
/*
					mail("code@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
					"\nLeadXML:".$this->xml);
*/
					recordLeadFailure($this->thearray,
						$this->lead_id,
						"security",
						"irelocation.leads_security");
					save_response($quote_id,$responseId,$this->lead_id,$success,"ctsc");	
				}
			}
			else
			{	
				save_response($quote_id,$result,$this->lead_id,$success,"ctsc");				
			}
			
			if ($success == 0)
			{
/*
				mail("code@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					"Lead #$quote_id Failed to Be Accepted: \nResult:$response\n".
					"LeadXML:".$this->xml);
*/
				recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
			}
			
			
		}		
	}

	class Gaylord extends AlarmTrax
	{
		function Gaylord()
		{
			parent::AlarmTrax(GAYLORD);
			$this->lead_type_id = "3";
		}	
		
		function test()
		{
			return ($this->thearray['source'] != "aff_p4p");
		}
	}
	
	
	
	
	class SafeMart extends SecurityClass
	{

		function test()
		{			
			return true;			
		}


		function SafeMart()
		{
			$this->lead_id = SAFEMART;
			$this->email = "david@safemart.com";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Aaaalarms",$this->message,$this->headers);
		}
	
	}




	class FastLink extends SecurityClass
	{

		function test()
		{			
			if($this->thearray['state_code'] == "ON" ) {
				$prefix = substr($this->thearray['zip'],0,3);

				//-- Set up the list of bad prefixes
				$bad_prefix = array("P8N", "P5A", "P5E", "P9A", "P3L", "P3P", "K6A", "P5N", "P9N", "P2N", "P3Y", "K8A", "K8B", "K8H", "P6A", "P6B", "P6C", "P8T", "P2B", "P3A", "P3B", "P3C", "P3E", "P3G", "P7C", "P7E", "P7J", "P7K", "P7L", "P7A", "P7B", "P7G", "P4N", "P4P", "P4R", "P3N", "P0T", "K6T", "K6V", "K7C", "K6H", "K6J", "K6K", "K4C", "K4P", "K6A", "K4M", "K4B", "K2K", "K2L", "K2M", "K2T", "K2V", "K2W", "K2H", "K1C", "K1E", "K1W", "K1A", "K1Y", "K1Z", "K2A", "K2B", "K1B", "K1G", "K1H", "K1X", "K1J", "K1K", "K4A", "K1L", "K1M", "K1N", "K2S", "K2C", "K2E", "K2G", "K2J", "K2R", "K1P", "K1R", "K2P", "K1S", "K1T", "K1V", "K8A", "K8B", "K7H", "K8H", "K7V", "K4K", "K4R", "K7A");
				
				if ( in_array($prefix,$bad_prefix) ) {
    				return false; //-- prefix not allowed
				} else {
   					return true; //-- prefix allowed
				}

			} else {
				return false;	
			}
					
		}


		function FastLink()
		{
			$this->lead_id = FASTLINK;
			$this->email = "amustafa@fastlinksecurity.ca";
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";	
		}
		
		function send()		
		{
			$this->format();
			mail($this->email,"CA Security Lead",$this->message,$this->headers);
			//mail("code@irelocation.com","Security Lead - Copy - Aaaalarms",$this->message,$this->headers);
		}
	
	}




//-- =========================================
	class oldFrontPoint extends SecurityClass
	{				

		function FrontPoint()
		{
#			$this->url = "http://fpfrontport.optimerics.com/1969-1/index.aspx"; //-- TEST URL
			$this->url = "http://frontport.frontpointsecurity.com/1969-1/index.aspx"; //-- PRODUCTION URL
			$this->lead_id = FRONTPOINT;
		}
	
		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()
		{
			return true;
/*
			$quote_type = strtolower($this->thearray["quote_type"]);
			$own_rent = strtolower($this->thearray["own_rent"]);
			$zip = $this->thearray["zip"];
			
			//-- if it's a RES RENT lead, they need to get it because APX doesn't want it
			if ( $quote_type == "res" && $own_rent == "rent" ) {
				echo "RES RENT Lead, giving to to FRONTPOINT cuz APX doesn't want these leads<br />";
				return true;
			} 
			
			//-- if we're still here, check to see if the zip is in either Protection One or APX zip lists, if so, then return false
			$sql = "select * from irelocation.security_zip_codes where zip = '".trim($zip)."' and ( lead_id = 1521 or lead_id = 1569 ) ";
			echo "Check to see if 1521 or 1569 wants the zip: $sql<br />";
			$rs = new mysql_recordset($sql);
			#$result = $rs->fetch_array();		
			$row_count = $rs->rowcount();
			
			if ( $row_count > 0 ) { //-- then 1521 or 1569 wants the zip (and not a RES RENT lead)
				echo "1521 or 1569 Wants this zip<br />";
				return false;
			} else {
				echo "1521 or 1569 does NOT want this zip<br />";
				return true;
			}

*/
		}
				

		function format()
		{

			$thearray = $this->thearray;
			
#			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C") {
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
				
				$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($lname,0,20);//same with contact name, or long lastname.
				$name1 = $fname;
				$name2 = $lname;
				
			} else {
				$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$name2 = substr($name2,0,20);//same with contact name, or long lastname.
			}
			
			
#			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 0;
				
			if ( $own_rent == "Own" ) {
				$owner_type = "Owner";
			} 
			if ( $own_rent == "Rent" ) {
				$owner_type = "Renter";
			} 
						
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$additional_services.= " ExtraServices:";
				if ($fire == 1)
					$additional_services  .= "Fire Protection ";
				if ($access == 1)
					$additional_services  .= "Access Control System ";				
				if ($cctv == 1)
					$additional_services  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$additional_services  .= xmlentities($other_field);
			}
			
			
/*				RESIDENTIAL
				house
				town house
				apartment
				condo
				mobile home
				
				COMMERCIAL
				industrial
				office
				retail
				restaurant
				home-based
				warehouse
				other
*/

		echo "BUILDING TYPE IS $building_type<br />";
		
		if ( $own_rent == "Own" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type)  ) {
				$p_type = "Residential - Homeowner";
			} 
			
			if ( eregi("industrial",$building_type) || eregi("office",$building_type) || eregi("retail",$building_type) || eregi("restaurant",$building_type) || eregi("home-based",$building_type) || eregi("other",$building_type)  ) {
				$p_type = "Commercial - Small Business";
			} 
			
		} elseif ( $own_rent == "Rent" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type) || eregi("apartment",$building_type)  ) {
				$p_type = "Residential - Renter";
			} 
			
		}
		echo "P TYPE = $p_type<br />";
		
		echo "SQUARE FOOTAGE is $sqr_footage<br />";
		
/*
		if ($quote_type == "res" && $sqr_footage == 0) $sqft = "0-1,499" ;
		if ($quote_type == "res" && $sqr_footage == 15) $sqft = "1,500-2,999" ;
		if ($quote_type == "res" && $sqr_footage == 30) $sqft = "3,000-4,999" ;
		if ($quote_type == "res" && $sqr_footage == 50) $sqft = "5,000-19,999" ;
		if ($quote_type == "res" && $sqr_footage == 200) $sqft = "20,000" ;

		if ($quote_type == "com" && $sqr_footage == 0) $sqft = "0 - 2,499" ;
		if ($quote_type == "com" && $sqr_footage == 25) $sqft = "2,500 - 4,999" ;
		if ($quote_type == "com" && $sqr_footage == 50) $sqft = "5,000 - 9,999" ;
		if ($quote_type == "com" && $sqr_footage == 100) $sqft = "10,000 - 49,999" ;
		if ($quote_type == "com" && $sqr_footage == 500) $sqft = "50,000" ;
		
		echo "NOW SQFT IS SET TO $sqft<br />";
*/

/*
		  RES
		  <option value="0">0 - 1,499 ft</option>
		  <option   value="15">1,500 - 2,999 ft</option>
		  <option   value="30">3,000 - 4,999 ft</option>
		  <option   value="50">5,000 - 19,999 ft</option>		  
		  <option   value="200">20,000+ ft</option>
		  
		  COM
		  <option  value="0">0 - 2,499 ft</option>
		  <option  value="25">2,500 - 4,999 ft</option>
		  <option  value="50">5,000 - 9,999 ft</option>
		  <option  value="100">10,000 - 49,999 ft</option>		  
		  <option  value="500">50,000+ ft</option>


IF LEAD IS RESIDENTIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Home' question in "DESC" Field as follows:  "Type of Home:  "[House; Apartment; Townhouse; Condo; Mobile Home]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-1,499; 1,500-2,999; 3,000-4,999; 5,000-19,999; 20,000+]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]
IF LEAD IS COMMERCIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Business' question in "DESC" Field as follows:  "Type of Business:  "[Office, ....]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-2,499;...]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]

	*	Add response to the 'How Many Locations' question in DESC Field:  "# of Locations:  "[1, ..., 10+]
	*	Add responses to the "Additional Services" questions in DESC Field:  "Additional Services:  " [Fire Protection, Access Control, Video Surveillance, Other: "  "]
	*	Ad response to the "ADditional Comments" text box:  "Additional comments:  "[  ]
	*	

*/

		
		if ( $quote_type == "res" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Home: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			
		} elseif ( $quote_type == "com" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Business: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			$newcomments .=	"\n# of Locations: $num_locations";
			$newcomments .=	"\nAdditional Services: $additional_services";

		}
		
		$newcomments .=	"\nAdditional comments: $comments";
		
		$newcomments = trim($newcomments);	
		

		 $this->lead_body = "FIRSTNAME=".urlencode($name1)."&".
			"LASTNAME=".urlencode($name2)."&".
			"ADDRESS=".urlencode($address)."&".
			"CITY=".urlencode($city)."&".
			"STATE=".urlencode($state_code)."&".
			"ZIP=".urlencode($zip)."&".
			"PHONE=".urlencode($phone1)."&".
			"ALTPHONE=".urlencode($phone2)."&".
			"EMAIL=".urlencode($email)."&".
			"REF=".$this->thearray["quote_id"]."&".
			"OWNERSHIP=".urlencode($owner_type)."&".
			"PREMISETYPE=".urlencode($p_type)."&".
			"DESC=".urlencode($newcomments);

		mail("code@irelocation.com","NEW FRONTPOINT CTSC Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->url."?".$this->lead_body);

				
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1); //-- they want it in POST cuz they can't accept GET  (SAY WHAT?  Must be stoopid .NET programming)
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","FRONTPOINT RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "SUCCESS":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											FRONTPOINT,1,"ctsc");
							echo "FRONTPOINT Lead - Success";
							break;
						case "FAILED":
						case "error":
						case "Bad Request":
							mail("code@irelocation.com","FRONTPOINT Bad Response - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"ctsc");
							echo "FRONTPOINT Lead - FAIL";
							break;
						default:
							mail("code@irelocation.com","FRONTPOINT Unknown - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"ctsc");
							echo "FRONTPOINT Lead - UNKNOWN";
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			mail("code@irelocation.com","FRONTPOINT Error Response","QuoteID: ".
				$this->myarray["quote_id"]."\n".$response);
					
		}
	}
//-- =========================================





//-- ========================================= NEW FRONT POINT CLASS
//-- ========================================= NEW FRONT POINT CLASS
//-- ========================================= NEW FRONT POINT CLASS
	class FrontPoint extends SecurityClass
	{				

		function FrontPoint()
		{
#			$this->url = "http://fpjanus.optimerics.com/post/index.aspx/3550577d-b812-4a16-a80b-a6ba39b4ddb4"; //-- TEST URL
			$this->url = "http://janus.frontpointsecurity.com/post/index.aspx/52160766-69d7-490b-9ac1-d6f4b30a7625"; //-- PRODUCTION URL
			$this->lead_id = FRONTPOINT;
		}
	
		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()
		{
			$prefix = substr($this->thearray['zip'],0,3);
			
			//-- Set up the list of bad prefixes - They don't want anything in Quebec, pulled these off the Internet as they don't have a list of them.
			$bad_prefix = array( "G0A", "G0C", "G0E", "G0G", "G0H", "G0J", "G0K", "G0L", "G0M", "G0N", "G0P", "G0R", "G0S", "G0T", "G0V", "G0W", "G0X", "G0Y", "G0Z", "G1A", "G1B", "G1C", "G1E", "G1G", "G1H", "G1J", "G1K", "G1L", "G1M", "G1N", "G1P", "G1R", "G1S", "G1T", "G1V", "G1W", "G1X", "G1Y", "G2A", "G2B", "G2C", "G2E", "G2G", "G2J", "G2K", "G2L", "G2M", "G2N", "G3A", "G3B", "G3E", "G3G", "G3H", "G3J", "G3K", "G3L", "G3M", "G3Z", "G4A", "G4R", "G4S", "G4T", "G4V", "G4W", "G4X", "G4Z", "G5A", "G5B", "G5C", "G5H", "G5J", "G5L", "G5M", "G5N", "G5R", "G5T", "G5V", "G5X", "G5Y", "G5Z", "G6A", "G6B", "G6C", "G6E", "G6G", "G6H", "G6J", "G6K", "G6L", "G6P", "G6R", "G6S", "G6T", "G6V", "G6W", "G6X", "G6Z", "G7A", "G7B", "G7G", "G7H", "G7J", "G7K", "G7N", "G7P", "G7S", "G7T", "G7X", "G7Y", "G7Z", "G8A", "G8B", "G8C", "G8E", "G8G", "G8H", "G8J", "G8K", "G8L", "G8M", "G8N", "G8P", "G8T", "G8V", "G8W", "G8Y", "G8Z", "G9A", "G9B", "G9C", "G9H", "G9N", "G9P", "G9R", "G9T", "G9X", "H0M", "H1A", "H1B", "H1C", "H1E", "H1G", "H1H", "H1J", "H1K", "H1L", "H1M", "H1N", "H1P", "H1R", "H1S", "H1T", "H1V", "H1W", "H1X", "H1Y", "H1Z", "H2A", "H2B", "H2C", "H2E", "H2G", "H2H", "H2J", "H2K", "H2L", "H2M", "H2N", "H2P", "H2R", "H2S", "H2T", "H2V", "H2W", "H2X", "H2Y", "H2Z", "H3A", "H3B", "H3C", "H3E", "H3G", "H3H", "H3J", "H3K", "H3L", "H3M", "H3N", "H3P", "H3R", "H3S", "H3T", "H3V", "H3W", "H3X", "H3Y", "H3Z", "H4A", "H4B", "H4C", "H4E", "H4G", "H4H", "H4J", "H4K", "H4L", "H4M", "H4N", "H4P", "H4R", "H4S", "H4T", "H4V", "H4W", "H4X", "H4Y", "H4Z", "H5A", "H5B", "H7A", "H7B", "H7C", "H7E", "H7G", "H7H", "H7J", "H7K", "H7L", "H7M", "H7N", "H7P", "H7R", "H7S", "H7T", "H7V", "H7W", "H7X", "H7Y", "H8N", "H8P", "H8R", "H8S", "H8T", "H8Y", "H8Z", "H9A", "H9B", "H9C", "H9E", "H9G", "H9H", "H9J", "H9K", "H9P", "H9R", "H9S", "H9W", "H9X", "J0A", "J0B", "J0C", "J0E", "J0G", "J0H", "J0J", "J0K", "J0L", "J0M", "J0N", "J0P", "J0R", "J0S", "J0T", "J0V", "J0W", "J0X", "J0Y", "J0Z", "J1A", "J1C", "J1E", "J1G", "J1H", "J1J", "J1K", "J1L", "J1M", "J1N", "J1R", "J1S", "J1T", "J1X", "J1Z", "J2A", "J2B", "J2C", "J2E", "J2G", "J2H", "J2J", "J2K", "J2L", "J2M", "J2N", "J2R", "J2S", "J2T", "J2W", "J2X", "J2Y", "J3A", "J3B", "J3E", "J3G", "J3H", "J3L", "J3M", "J3N", "J3P", "J3R", "J3T", "J3V", "J3X", "J3Y", "J3Z", "J4B", "J4G", "J4H", "J4J", "J4K", "J4L", "J4M", "J4N", "J4P", "J4R", "J4S", "J4T", "J4V", "J4W", "J4X", "J4Y", "J4Z", "J5A", "J5B", "J5C", "J5J", "J5K", "J5L", "J5M", "J5R", "J5T", "J5V", "J5W", "J5X", "J5Y", "J5Z", "J6A", "J6E", "J6J", "J6K", "J6N", "J6R", "J6S", "J6T", "J6V", "J6W", "J6X", "J6Y", "J6Z", "J7A", "J7B", "J7C", "J7E", "J7G", "J7H", "J7J", "J7K", "J7L", "J7M", "J7N", "J7P", "J7R", "J7T", "J7V", "J7W", "J7X", "J7Y", "J7Z", "J8A", "J8B", "J8C", "J8E", "J8G", "J8H", "J8L", "J8M", "J8N", "J8P", "J8R", "J8T", "J8V", "J8X", "J8Y", "J8Z", "J9A", "J9B", "J9E", "J9H", "J9J", "J9L", "J9P", "J9T", "J9V", "J9X", "J9Y", "J9Z", "K1A" );
			
			if ( in_array($prefix,$bad_prefix) ) {
				return false; //-- prefix not allowed
			} else {
				return true; //-- prefix allowed
			}

		}
				

		function format()
		{

			$thearray = $this->thearray;
			
#			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C") {
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
				
				$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($lname,0,20);//same with contact name, or long lastname.
				$name1 = $fname;
				$name2 = $lname;
				
			} else {
				$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$name2 = substr($name2,0,20);//same with contact name, or long lastname.
			}
			
			
#			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 0;
				
			if ( $own_rent == "Own" ) {
				$owner_type = "Owner";
			} 
			if ( $own_rent == "Rent" ) {
				$owner_type = "Renter";
			} 
						
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$additional_services.= " ExtraServices:";
				if ($fire == 1)
					$additional_services  .= "Fire Protection ";
				if ($access == 1)
					$additional_services  .= "Access Control System ";				
				if ($cctv == 1)
					$additional_services  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$additional_services  .= xmlentities($other_field);
			}
			
			
			//-- pre-wiring
			if ( $prewired == 1 ) {
				$has_prewired = "Yes";
			} else {
				$has_prewired = "No";
			}

		echo "BUILDING TYPE IS $building_type<br />";
		
		if ( $own_rent == "Own" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type)  ) {
				$p_type = "Residential";
			} 
			
			if ( eregi("industrial",$building_type) || eregi("office",$building_type) || eregi("retail",$building_type) || eregi("restaurant",$building_type) || eregi("home-based",$building_type) || eregi("other",$building_type)  ) {
				$p_type = "Commercial";
			} 
			
		} elseif ( $own_rent == "Rent" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type) || eregi("apartment",$building_type)  ) {
				$p_type = "Residential";
			} 
			
		}
		echo "P TYPE = $p_type<br />";
		
		echo "SQUARE FOOTAGE is $sqr_footage<br />";
		
/*
		if ($quote_type == "res" && $sqr_footage == 0) $sqft = "0-1,499" ;
		if ($quote_type == "res" && $sqr_footage == 15) $sqft = "1,500-2,999" ;
		if ($quote_type == "res" && $sqr_footage == 30) $sqft = "3,000-4,999" ;
		if ($quote_type == "res" && $sqr_footage == 50) $sqft = "5,000-19,999" ;
		if ($quote_type == "res" && $sqr_footage == 200) $sqft = "20,000" ;

		if ($quote_type == "com" && $sqr_footage == 0) $sqft = "0 - 2,499" ;
		if ($quote_type == "com" && $sqr_footage == 25) $sqft = "2,500 - 4,999" ;
		if ($quote_type == "com" && $sqr_footage == 50) $sqft = "5,000 - 9,999" ;
		if ($quote_type == "com" && $sqr_footage == 100) $sqft = "10,000 - 49,999" ;
		if ($quote_type == "com" && $sqr_footage == 500) $sqft = "50,000" ;
		
		echo "NOW SQFT IS SET TO $sqft<br />";
*/

/*
		  RES
		  <option value="0">0 - 1,499 ft</option>
		  <option   value="15">1,500 - 2,999 ft</option>
		  <option   value="30">3,000 - 4,999 ft</option>
		  <option   value="50">5,000 - 19,999 ft</option>		  
		  <option   value="200">20,000+ ft</option>
		  
		  COM
		  <option  value="0">0 - 2,499 ft</option>
		  <option  value="25">2,500 - 4,999 ft</option>
		  <option  value="50">5,000 - 9,999 ft</option>
		  <option  value="100">10,000 - 49,999 ft</option>		  
		  <option  value="500">50,000+ ft</option>


IF LEAD IS RESIDENTIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Home' question in "DESC" Field as follows:  "Type of Home:  "[House; Apartment; Townhouse; Condo; Mobile Home]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-1,499; 1,500-2,999; 3,000-4,999; 5,000-19,999; 20,000+]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]
IF LEAD IS COMMERCIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Business' question in "DESC" Field as follows:  "Type of Business:  "[Office, ....]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-2,499;...]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]

	*	Add response to the 'How Many Locations' question in DESC Field:  "# of Locations:  "[1, ..., 10+]
	*	Add responses to the "Additional Services" questions in DESC Field:  "Additional Services:  " [Fire Protection, Access Control, Video Surveillance, Other: "  "]
	*	Ad response to the "ADditional Comments" text box:  "Additional comments:  "[  ]
	*	

*/

		
		if ( $quote_type == "res" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Home: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			
		} elseif ( $quote_type == "com" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Business: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			$newcomments .=	"\n# of Locations: $num_locations";
			$newcomments .=	"\nAdditional Services: $additional_services";

		}
		
		$newcomments .=	"\nAdditional comments: $comments";
		
		$newcomments = trim($newcomments);	
		

		 //-- OLD
/*
		 $this->lead_body = "FIRSTNAME=".urlencode($name1)."&".
			"LASTNAME=".urlencode($name2)."&".
			"ADDRESS=".urlencode($address)."&".
			"CITY=".urlencode($city)."&".
			"STATE=".urlencode($state_code)."&".
			"ZIP=".urlencode($zip)."&".
			"PHONE=".urlencode($phone1)."&".
			"ALTPHONE=".urlencode($phone2)."&".
			"EMAIL=".urlencode($email)."&".
			"REF=".$this->thearray["quote_id"]."&".
			"OWNERSHIP=".urlencode($owner_type)."&".
			"PREMISETYPE=".urlencode($p_type)."&".
			"DESC=".urlencode($newcomments);
*/
		
		//-- NEW
		$this->lead_body = "REF=".$this->thearray["quote_id"]."&".
			"FIRSTNAME=".urlencode($name1)."&".
			"LASTNAME=".urlencode($name2)."&".
			"COMPANY=".urlencode($businessname)."&".
			"STREET1=".urlencode($address)."&".
			"CITY=".urlencode($city)."&".
			"STATE=".urlencode($state_code)."&".
			"ZIP=".urlencode($zip)."&".
			"COUNTRY=".urlencode("CA")."&".
			"EMAIL=".urlencode($email)."&".
			"PHONE=".urlencode($phone1)."&".
			"MOBILE=".urlencode($phone2)."&".
			"PREMISETYPE=".urlencode($p_type)."&".
			"OWNERSHIP=".urlencode($owner_type)."&".
			"INSTALLTYPE=".urlencode("NotSet")."&".
			"SYSTEMTYPE=".urlencode("NotSet")."&".
			"MONITORED=".urlencode("NotSet")."&".
			"PREWIRED=".urlencode($has_prewired)."&".
			"PREMISESIZE=".urlencode($sqr_footage)."&".
			"CAMPAIGN=".urlencode("ctsc")."&".
			"DESC=".urlencode($newcomments);

		mail("code@irelocation.com","NEW FRONTPOINT CTSC Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->url."?".$this->lead_body);

				
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1); //-- they want it in POST cuz they can't accept GET  (SAY WHAT?  Must be stoopid .NET programming :o)
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","FRONTPOINT RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "SUCCESS":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											FRONTPOINT,1,"ctsc");
							echo "FRONTPOINT Lead - Success";
							break;
						case "FAILED":
						case "error":
						case "Bad Request":
							mail("code@irelocation.com","FRONTPOINT Bad Response - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"ctsc");
							echo "FRONTPOINT Lead - FAIL";
							break;
						default:
							mail("code@irelocation.com","FRONTPOINT Unknown - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"ctsc");
							echo "FRONTPOINT Lead - UNKNOWN";
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			mail("code@irelocation.com","FRONTPOINT Error Response","QuoteID: ".
				$this->myarray["quote_id"]."\n".$response);
					
		}
	}
//-- =========================================





//-- ================================================ NEW BRINKS CLASS USING LEADCONDUIT
//-- 11/17/08 14:30 PM Brinks now wants the street number and name to be a single var instead of splitting up 

	class NewBrinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()//only BC Leads as of 6/1/7
		{			
			if($this->thearray['state_code'] == "BC")
			{
				$own_rent = $this->thearray['own_rent'];
				$type = $this->thearray['quote_type'];
				
				if ($own_rent == "Own" || $type == "com" )
					return true;			
			}
			else
				return false;
		}
		
		function NewBrinks()
		{
			$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
			$this->lead_id = BRINKS;
		}	
			
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C") {
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
				$nodeid = "000wxti4q";
				
			} else {
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
				$nodeid = "000w3xy4j";
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			
			if ($this->thearray["source"] == "aff_p4p" || $this->thearray["source"] == "aff_rlc")
				$marketsource = "N751";
			else			
				$marketsource = "N733";			
			
			$zip = ereg_replace(" ","",$zip); //-- now Brinks wants the spaces taken out of the postal codes - Today is 11/20/08. It is now 17:01 PM
			
/* OLD
		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetNumber=".urlencode($street_number)."&".
			"StreetName=".urlencode($street_name)."&".
			"City=".urlencode($city)."&".
			"State=".urlencode($state_code)."&".
			"ZipCode=".urlencode($zip)."&".
			"PrimaryPhone=".urlencode($phone1)."&".
			"EmailAddress=".urlencode($email)."&".
			"BusinessName=".urlencode($businessname)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);
*/

		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetAddress=".urlencode($address)."&".
			"City=".urlencode($city)."&".
			"State=".urlencode($state_code)."&".
			"ZipCode=".urlencode($zip)."&".
			"PrimaryPhone=".urlencode($phone1)."&".
			"EmailAddress=".urlencode($email)."&".
			"BusinessName=".urlencode($businessname)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);

#		mail("code@irelocation.com","NEW BRINKS CAS Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->lead_body);


/* OLD
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,$marketsource,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";		
			//mail("code@irelocation.com","Brinks #".$thearray["quote_id"],$this->lead_body);
*/
		}
			
			
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","BRINKS RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "success":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											BRINKS,1,"ctsc");
							echo "BRINKS Lead - Success";
							break;
						case "failure":
						case "error":
							mail("code@irelocation.com","BRINKS Response - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										BRINKS,0,"ctsc");
							break;
						default:
							mail("code@irelocation.com","BRINKS Unknown - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			mail("code@irelocation.com","BRINKS Error Response","QuoteID: ".
				$this->myarray["quote_id"]."\n".$response);
					
		}
		
	}
	
	
	
	

	class ApexMM extends SecurityClass
	{		
	
		function ApexMM()
		{
			$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
			$this->lead_id = APEXMM;
		}	
			
		
	
		function test() //-- they get all RESidential Canada postals
		{			
			if ( $this->thearray['quote_type'] == "res" ) {
				return true;			
			} else {
				return false;			
			}
			
		}
		
		function format()
		{

			extract($this->thearray);
			$area = substr($phone1,0,3);
			$pre =  substr($phone1,3,3);
			$suf =  substr($phone1,6);
			$phone_w_dashes = "$area-$pre-$suf";
			//they only get resi owners.
			$quote_type = "residential";				
			$own_rent = "YES";
				
			 $this->lead_body = "xxNodeId=03lcavhcl&".
				"City=".urlencode($city)."&".
				"Email=".urlencode($email)."&".
				"FirstName=".urlencode($name1)."&".
				"LastName=".urlencode($name2)."&".
				"Phone=".urlencode($phone1)."&".
				"phone1=".urlencode($phone_w_dashes)."&".
				"credit=GOOD&".
				"phone2=".urlencode($phone2)."&".
				"AFID=1985&".
				"State=".urlencode($state_code)."&".
				"PostalCode=".urlencode($zip)."&".
				"Address=".urlencode($address)."&".
				"ip=$remote_ip&".
				"IPAddress=66.96.131.11&".
				"ownhome=YES";
				
		}
			
			
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","APX RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1] . $response[2] . $response[3] . $response[4] . $response[5] . $response[6] . $response[7]);
					switch($response[0])
					{
						 case "success":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											APX,1,"ctsc");
							echo "APX Lead - Success";
							break;
						case "failure":
						case "error":
							mail("code@irelocation.com","APX Response - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										APX,0,"ctsc");
							break;
						default:
							mail("code@irelocation.com","APX Unknown - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			mail("code@irelocation.com","APX Error Response","QuoteID: ".
				$this->myarray["quote_id"]."\n".$response);
					
		}
		
	}
	

?>