<?php
/* 
************************FILE INFORMATION**********************
* File Name:  au_cron_security.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	5/1/08 11:08 AM - Rob - took out 1800SafeHome and added Signature Security Group
	
	5/5/08 11:52 AM - Rob - reworking the output code so that it's easier to see what's going on with this script
	
	7/30/08 14:28 PM - added more companies and changed the format of the array so that it was more readable
	
	11/7/08 - Rob - Reworked the Signature Security Group rotation code for a new company, ASM (1655)
**********************************************************************
*/

	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "AuSecurityClasses.php";//all company functions!! :P
	
	
	
	define(TIMEZONE_OFFSET,18000);
	$now = gmdate("YmdH", time() - TIMEZONE_OFFSET)."0000";
	$after_five = ($now > '20071031190000');


	$all_companies = array(
		TYCO => new Tyco(),
		SECURITEX => new Securitex(), 
		PROTECT24 => new Protect24(), 
		SPECSECSYS => new SpecSecSys(), 
		NATIONAL => new National(), 
		ASASECURITY => new ASASecurity(), 
		SAFEGUARD => new Safeguard(), 
		TJMSECURITY => new TJMSecurity(), 
		ASMMONITORING => new ASMMonitoring(),
		SAFECHOICE => new SafeChoice()
		);	
	

/*
$diff_companies = $all_companies;
foreach($diff_companies as $key => $value) {
	if($key == "1622" ) {
		unset($diff_companies[$key]);
	}
}
*/
#	$diff_companies = array_diff_assoc( $all_companies, array("1622") );

	echo "<pre>";
	echo "all_companies array: <br />";
	print_r($all_companies);
#	echo "diff_companies array: <br />";
#	print_r($diff_companies);
	echo "</pre>";
	
	$sql = "delete from irelocation.leads_security where name1 like 'Test%' or email = 'test@test.com' or name2 = 'Test' and campaign = 'aas' ;";
	$rs = new mysql_recordset($sql);
	
	//-- get all AUS sec companies
	$sql = "select ca.lead_id from movingdirectory.campaign as ca where month = '".date("Ym")."' and ca.site_id = 'aas' and ca.active;";
	
	echo "\n<br />Comapny SQL:  $sql<br /><br />\n\n";	
	

	$rs = new mysql_recordset($sql);
	$companies = array();
	
	echo "Setup:<br />\n";
	
	while($rs->fetch_array())	
	{		
		echo $rs->myarray["lead_id"]."<br>\n";
		$companies[] = $all_companies[$rs->myarray["lead_id"]];	
		
#		echo "<pre>all_companies lead_id: <br />";
#		print_r($all_companies[$rs->myarray["lead_id"]]);
		
	}	
	
/*
		echo "<pre>";
		echo "companies array:<br />";
		print_r($companies);
		echo "</pre>";
		echo "<hr>";

	echo "<br/>End Setup<Br/>\n";
*/
	


/*  THIS CODE WILL REMOVE A LED_ID FROM THE ARRAY
for($i=0;$i<count($companies);$i++){
	if ($companies[$i]->lead_id=="1622") {
		unset($companies[$i]);
	}
}

//-- now we need to reindex the array so it will cycle through the FOR count properly
array_multisort($companies);


echo "<pre>";
echo "NEW companies array:<br />";
print_r($companies);
echo "</pre>";
echo "<hr>";
*/

	
	$mytime = updateReceived("aas");//set the timestamp of new leads.
	
	$sql = "select *,REPLACE(REPLACE(comments,'\r',' '),'\n',' ') as ".
	" 'brinks_customer_comments' from irelocation.leads_security where ".
	" received = '".$mytime."' and email not like ".
	" '%randomkeystrokes@gmail.com%' and campaign = 'aas';";//get those leads.
	
	echo "<br />";
	echo "REPLACE SQL: $sql <br />";  //add the REPLACE(.. code for brinks they are dinosaurish	
	
	
	$rs = new mysql_recordset($sql);
	
	//iterate over each lead.
	$count = 0;

	while($rs->fetch_array())	
	{
		$lead_ids = array();
		$myarray = formatData($rs->myarray);		
		
		echo "QuoteId: ".$myarray["quote_id"]."<Br/>\n";
		echo "zip: ".$myarray["zip"]."<Br/>\n";
		
		
		
		
		$zip = $myarray["zip"];
		//-- assign companies to a temp var
		$companies_temp = $companies;

//-- ########  This Rotation code is for randomly choosing which Signature Security Group company will get the lead for a particular postal code.

//-- ########  The big issue with this is that we need to determine if more than one company wants the same zip code, instead of using a range of zips
//-- have to use SQL calls to make the determination - quickest, easiest way to do it


		switch ( $zip ) {

			case ($zip >= 3000 && $zip <4000): //-- ########################## 3000 - 3999 - fight between Safe Choice and Securitex
				
				//-- check to see if this is a zip that Safe Choice wants
				$sqlx = "select count(*) as count from security_zip_codes where zip = $zip and lead_id = 1705 ";
				$rsx = new mysql_recordset($sqlx);
				$rsx->fetch_array();
				$count = $rsx->myarray["count"];
				
				echo "ZIP IS 3000 - 3999<br />";
				
				if ( $count > 0 ) { //-- if Safe Choice also wants this zip, then we need to randomly choose one lead_id to be removed
					echo "SECURITEX AND SAFE CHOICE BOTH WANT THIS ZIP<br />";
					
					//-- now we need to randomly choose a company
					$guess = mt_rand(1,1000);
					echo "RANDOM NUMBER IS $guess<br />";
					
					if ( $guess & 1 ) { //-- odd, remove Securitex from list
						echo "Number is ODD<br />";
						
						for($i=0;$i<count($companies_temp);$i++){
							if ($companies_temp[$i]->lead_id=="1622") {
								unset($companies_temp[$i]);
							}
						}
						array_multisort($companies_temp);
						$companies_use = $companies_temp;
	
					} else { //-- even, remove Safe Choice from list
					
						echo "Number is EVEN<br />";
						
						for($i=0;$i<count($companies_temp);$i++){
							if ($companies_temp[$i]->lead_id=="1705") {
								unset($companies_temp[$i]);
							}
						}
						array_multisort($companies_temp);
						$companies_use = $companies_temp;
					}

				} 
			
				break;


			case ($zip >= 4000 && $zip <=4300): //-- ########################## 4000 - 4299 - fight between ASM and ASA
			
				//-- check to see if this is a zip that ASM wants
				$sqlx = "select count(*) as count from security_zip_codes where zip = $zip and lead_id = 1655 ";
				$rsx = new mysql_recordset($sqlx);
				$rsx->fetch_array();
				$count = $rsx->myarray["count"];

				echo "ZIP IS 4000 - 4300<br />";
				
				if ( $count > 0 ) { //-- if ASM also wants this zip, then we need to randomly choose one lead_id to be removed
					echo "ASA AND ASM BOTH WANT THIS ZIP<br />";
					
					//-- now we need to randomly choose a company
					$guess = mt_rand(1,1000);
					echo "RANDOM NUMBER IS $guess<br />";
					
					if ( $guess & 1 ) { //-- odd, remove ASA from list
					echo "Number is ODD<br />";
					
						for($i=0;$i<count($companies_temp);$i++){
							if ($companies_temp[$i]->lead_id=="1634") {
								unset($companies_temp[$i]);
							}
						}
						array_multisort($companies_temp);
						$companies_use = $companies_temp;
	
					} else { //-- even, remove ASM from list
						
						echo "Number is EVEN<br />";
						
						for($i=0;$i<count($companies_temp);$i++){
							if ($companies_temp[$i]->lead_id=="1655") {
								unset($companies_temp[$i]);
							}
						}
						array_multisort($companies_temp);
						$companies_use = $companies_temp;
	
					}

				} 
				
				break;
		
		
			default:
				//-- do nothing, leave the $companies array as is
				$companies_use = $companies_temp;
				break;
		}
		

		//-- need to put this in here becasue it used to be in the switch above and still needs to be assigned.
		$companies_use = $companies_temp;
		
		if ( $name1 != "tess" and $name2 != "ting" ) { //-- if this is TESS TING test lead, don't process further.
			
			for ($i = 0; $i < count($companies_use); $i++) {
				echo "Lead ID: ".$companies_use[$i]->lead_id."<br/>\n";
				
				if ($companies_use[$i]->assign($myarray)) { //assign the array, and test them.
					$companies_use[$i]->send();
					$lead_ids[] = $companies_use[$i]->lead_id;
					updateLeadIds(implode("|",$lead_ids) ,$myarray["quote_id"],
									"irelocation.leads_security");//save it
				}
			}
			
			sort($lead_ids);
			echo implode("|",$lead_ids)."<br/>";
			
			$count++;
			//updateLeadIds(implode("|",$lead_ids) ,$myarray["quote_id"],"irelocation.leads_security");//save it
			sendThankyouEmail($myarray,"tsc");

		} else {
			echo "SKIPPING TEST LEAD<br />";
		}
		
	}
	
	if ($count == 0) echo "<Br/>No Leads Processed.<br/>";
	else echo "<Br/>$count Leads Processed.<br/>";
		
	//updateCronStatus("security", "ended");
	//checkCronStatus("local","security");
	//checkCronStatus("atus","security");
#	phpinfo();
?>