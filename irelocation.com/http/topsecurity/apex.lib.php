<?
	define(URL,"https://www.aimpromote.com/remote/form_post.php");
	define(PREFIX,"ap_field_");
	
	include_once "../inc_mysql.php";
	
	function sendData($array)
	{
		$data = array
		(		
			"ss_id" => 2321,
			"action" => "feedback_post_add",
			"ostat_id" => 1,
			"org_id" => 334,
			"c_id" => 662
			//"ap_field_11697" => "123456"
		);
	
		doMapping($data,$array);
	
		$ch = curl_init();
			
		curl_setopt($ch, CURLOPT_URL, URL);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		
		return $response;
	}

	function doMapping(&$data,$array)
	{
		$data[PREFIX."11697"] = $array['quote_id'];
		$data[PREFIX."11698"] = $array['received'];
		$data[PREFIX."11699"] = $array['quote_type'];
		$data[PREFIX."11700"] = $array['name1'];
		$data[PREFIX."11701"] = $array['name2'];
		$data[PREFIX."11702"] = $array['phone1'];
		$data[PREFIX."11703"] = $array['phone2'];
		$data[PREFIX."11772"] = $array['email'];
		$data[PREFIX."11704"] = $array['address'];
		$data[PREFIX."11774"] = $array['city'];
		$data[PREFIX."11705"] = $array['state_code'];
		$data[PREFIX."11706"] = $array['zip'];
		$data[PREFIX."11707"] = $array['current_needs'];
		$data[PREFIX."11708"] = $array['building_type'];
		$data[PREFIX."11709"] = $array['num_location'];
		$data[PREFIX."11769"] = $array['sqr_footage'];
		$data[PREFIX."11710"] = $array['own_rent'];
		$data[PREFIX."11711"] = $array['fire'];
		$data[PREFIX."11739"] = $array['basement'];
		$data[PREFIX."11740"] = $array['prewired'];
		$data[PREFIX."11741"] = $array['access'];
		$data[PREFIX."11771"] = $array['cctv'];
		$data[PREFIX."11773"] = $array['other_field'];
		$data[PREFIX."11770"] = $array['comments'];	
	}

	if (!isset($_REQUEST['test']))
		exit();
		
	$sql = "select * from irelocation.leads_security where lead_ids like '%1557%' ".
			" and received like '200706%' limit 1";
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();
	
	$response = sendData($rs->myarray);
	echo $response;
	
/*

This is the info I found on it.
account id: 334
substatus: 2321 (initial communication"OPEN")
irelocate: ?apcid=662 or irelocate id: 662
usergroup: 942

quote_id[11697]
received[11698]
quote_type[11699]
name1[11700]
name2[11701]
phone1[11702]
phone2[11703]
email[11772]
address[11704]
city[11774]
state_code[11705]
zip[11706]
current_needs[11707]
building_type[11708]
num_location[11709]
sqr_footage[11769]
own_rent[11710]
fire[11711]
basement[11739]
prewired[11740]
access[11741]
cctv[11771]
other_field[11773]
comments[11770]


There are many lead delivery systems out there with which we can integrate to have your leads delivered directly into the AIMpromote system. As long as the company can deliver leads by HTTP post, we can receive their leads. The following is the information they need to successfully deliver leads directly into the AIMpromote system:

    * URL: https://www.aimpromote.com/remote/form_post.php
      This is the url that the field data will be posted to.
    * action = feedback_post_add
    * org_id = Your Organization Id Number
      Each AIMpromote account has a unique ID number. The lead delivery system needs to pass this value along so the lead data goes to the right organization. You can obtain your org_id by going to the account settings page and the number is posted in the account information title bar.
    * ostat_id = Lead Status ID
      give the lead a status by passing along the ostat_id
          o

                open = 1

          o

                closed-won = 2

          o

                Closed-Lost = 3

          o

                Non-Qual = 4

    * ss_id = Sub-Status ID
      If you want the leads to have a specific Active Internet Marketing | Search Engine Optimization (SEO), Advertising, Website Promotion - Home when they are delivered to your system, you need to pass the sub-status id. The ss_id is found in the settings tab, under the sub-status values link. The unique sub-status id is next to each sub-status in the list.
    * c_id = Campaign ID
      You can associate all the leads that are delivered by this post by first creating a campaign for it, and then passing along the campaign id. Each unique campaign id is found next to the campaign in the campaigns list.
    * userid = User ID
      If you want the leads to be assigned to a specific user when they are delivered, you must pass along a user id. Each unique user id is found in the view users list.
    * ug_id = User Group Id
      If you want the leads to be assigned to a user group rather than a particular user, you must pass along the user group id. This is not necessary if you have passed along a user id. The user group id is found in the view user groups list.
    * ap_field_xxxx = Field Id
      The values from their fields need to be mapped to your form fields. Each field in your system has a unique field id. for instance their field which may be titled "firstName" needs to be mapped to your field which has the unique id of ap_field_1234. You can get your field ids by going to account settings --> forms --> manage form fields.


*/
?>
