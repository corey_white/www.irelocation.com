<?php

	class Parent 
	{
		function Parent()
		{		}
		
		function test()
		{
			echo "Parent->test();<br/>";
		}	
	}
	
	class Child extends Parent
	{
		function Child()
		{
		
		}
		
		function test()
		{
			echo "Child->test();<br/>";
			parent::test();
		}	
	
	}
	
	$p = new Parent();
	$p->test();
	
	$c = new Child();
	$c->test();
	
?> 