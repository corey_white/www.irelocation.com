<?	
/* 
************************FILE INFORMATION**********************
* File Name:  SecurityClasses.php
**********************************************************************
* Description:  This is the TOPSECURITY SECURITYCLASS
**********************************************************************
* Creation Date:  unknown
**********************************************************************
* Modifications (Date/Who/Details):

2/18/10 09:12 AM - Rob - Updated the Source IDs for Broadview/Brinks

**********************************************************************

TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY
TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY
TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY    TOPSECURITY
*/

	define(DEGUB_TSC_EMAIL,"code@irelocation.com");
	include_once "pinnacle.php";
	include_once "SecurityClass.php";
	
	define(GAY_ABSOLUTE,"absolute");
	define(GAY_EVERSAFE,"eversafe");
	define(GAY_ROOT,"gaylord");
	
	class AlarmTrax extends SecurityClass
	{
	
		function setGaylordAffiliate($company)
		{
			$this->company = $company;
			
			if ($company == GAY_ABSOLUTE)
			{
				$this->source_id = 75;				
			}
			else if ($company == GAY_EVERSAFE)
			{
				$this->source_id = 34;	
			}
			else if ($company == GAY_ROOT)			
			{
				$this->source_id = 53;				
			}
			else
			{
				echo "INVALID GAYLORD AFFILIATE!!!";
				#mail("code@irelocation.com");
				exit();
			}
			$this->url = "https://secure.securitytrax.com/".$company."/leadpostxml.php";
		}
	
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			//75 - for Absolute
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 53;
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}
			else if ($this->lead_id == PINNACLE)
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";		
			}
			/*
			else if ($this->lead_id == PINNACLE)
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";		
			}
			*/


			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = array();
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: ".$data['building_type'];

			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
			
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<"."?xml version='1.0' standalone='yes'?".">
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<lead_type_id>".$this->lead_type_id."</lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".xmlentities($data['name1'])."</fname>
					<lname>".xmlentities($data['name2'])."</lname>
					<address1>".xmlentities($data['address'])."</address1>
					<address2></address2>
					<city>".xmlentities($data['city'])."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".xmlentities($data['comments'])."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".xmlentities($extra_text)."</lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			return $xml;
			mail("code@irelocation.com","TSC Gaylord XML",$xml);
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			//mail("code@irelocation.com","AlarmTrax XML",$xml);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>
			$rawresponse = $response;
			
			echo "<br>\nResponse:$response.<br>\n<br>\n";
			
			$response = substr($response,strpos($response,"<body>")+6);
			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
			if ($success == 0)
			{
				//new format?
				if (substr_count($response,"success=\"true\"") > 0)
				{
					$success = 1;
					$start = strpos($response,"<customer_id>")+13;
					$end = strpos($response,"</",$start);
					if ($end > $start && $start != -1)
						$responseId = substr($response,$start,$end);				
					else
					{
						$success = 0;
						$responseId = "Success? - $response";
						#mail("code@irelocation.com","AlarmTrax Failure? - ".$this->lead_id, "Lead #$quote_id Came Back with an odd response code: \nResult:$response". "\nLeadXML:".$this->xml);
						recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
					}
					
					save_response($quote_id,$responseId,$this->lead_id,$success,"tsc");	
				}
				else
				{
					$success = 0;
					$responseId = "failure - $response";
					#mail("code@irelocation.com","AlarmTrax Failure - ".$this->lead_id, "Lead #$quote_id Came Back with an odd response code: \nResult:$response". "\nLeadXML:".$this->xml);
					recordLeadFailure($this->thearray,
						$this->lead_id,
						"security",
						"irelocation.leads_security");
					save_response($quote_id,$responseId,$this->lead_id,$success,"tsc");	
				}
			}
			else
			{	
				save_response($quote_id,$result,$this->lead_id,$success,"tsc");				
			}
		}		
	}
	
	/*
	class SecurityClass
	{
		var $url;
		var $thearray;
		var $lead_body;
		var $lead_id;
		
		function SecurityClass()
		{
			$this->url = "";
			$this->thearray = array();
			$this->lead_body = "";
			$this->lead_id = "";
		}
				
		//default, if not overwridden this company will get all leads.
		function test()
		{
			return true;
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function active() { return true; }
		
		function getMessage()
		{
			return $this->lead_body;
		}			
	}	
	*/
	class ProtectAmerica extends SecurityClass
	{				
		function ProtectAmerica()
		{
			$this->url = "http://production.leadconduit.com/ap/PostLeadAction";
			$this->lead_id = PROTECT_AMERICA;
		}
	
		function format()
		{
			extract($this->thearray);
			if ($quote_type == "res")
				$quote_type = "residential";
			else
				$quote_type = "commercial";
				
			if ($own_rent == "own")
				$own_rent = "YES";
			else
				$own_rent = "NO";
				
			 $this->lead_body = "xxNodeId=2b43d7b&".
				"city_name=".urlencode($city)."&".
				"email_address=".urlencode($email)."&".
				"first_name=".urlencode($name1)."&".
				"last_name=".urlencode($name2)."&".
				"phone1=$phone1&".
				"phone2=$phone2&".
				"source=A01405&".
				"state_name=$state_code&".
				"street_name=".urlencode($address)."&".
				"userdef07=$own_rent&".
				"userdef74=GOOD&".
				"version=9&".
				"zipcode=$zip&";
			if ($source == "aff_cst" || $source == "aff_p4p" || strpos($source,"bpm") > 0)
				$this->lead_body .= "key3=callcenter-$quote_type";
			else
				"key3=$quote_type";
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			
			if (substr_count(strtolower($response),"the lead was submitted") == 0)
			{
				#mail("code@irelocation.com","PA Response","QuoteID: ". $this->myarray["quote_id"]."\n".$response);
				save_response($this->thearray["quote_id"], "Failure - $response", 
								PROTECT_AMERICA,0,"tsc");
				echo "ProtectAmerica Lead - Failure";
				recordLeadFailure($this->thearray,
							1520,
							"security",
							"irelocation.leads_security");
			}
			else 
			{
				
				save_response($this->thearray["quote_id"], "Lead Accepted", PROTECT_AMERICA,1,"tsc");
				echo "ProtectAmerica Lead - Success";
			}
		}
	}
	
	class APXSecurity extends SecurityClass
	{				
		function test()
		{
			//-- They only want RESidential OWNers
			$valid = (strtolower($this->thearray['own_rent']) == "own" && $this->thearray['quote_type'] == "res");		
			
			if($valid) {
				$sql = "select zip From security_zip_codes where lead_id = '". APX."' and zip = '".$this->thearray['zip']."' limit 1";
				#mail("code@irelocation.com","APX ZIP CHECK","Here is the sql for the zip code check for APX. \n\n $sql\n\n This email was sent from /Volumes/Documents/LiveSites/www.irelocation.com/topsecurity/SecurityClasses.php");
				$rs = new mysql_recordset($sql);
				$valid = $rs->fetch_array();
			}
			
			return $valid;		
		}
	
		function APXSecurity()
		{
			$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
			$this->lead_id = APX;
		}
	
		function format()
		{

			extract($this->thearray);
			$area = substr($phone1,0,3);
			$pre =  substr($phone1,3,3);
			$suf =  substr($phone1,6);
			$phone_w_dashes = "$area-$pre-$suf";
			//they only get resi owners.
			$quote_type = "residential";				
			$own_rent = "YES";
				
			 $this->lead_body = "xxNodeId=000ktsuc3&".
				"City=".urlencode($city)."&".
				"Email=".urlencode($email)."&".
				"FirstName=".urlencode($name1)."&".
				"LastName=".urlencode($name2)."&".
				"Phone=$phone1&".
				"phone1=$phone_w_dashes&".
				"credit=GOOD&".
				"phone2=$phone2&".
				"AFID=1975&".
				"State=$state_code&".
				"PostalCode=$zip&".
				"Address=".urlencode($address)."&".
				"ip=$remote_ip&".
				"IPAddress=66.96.131.11&".
				"ownhome=YES";
				
			/*
			if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
				$this->lead_body .= "key3=callcenter-$quote_type";
			else
				"key3=$quote_type";
			*/
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","APX RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "success":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											APX,1,"tsc");
							echo "APX Lead - Success";
							break;
						case "failure":
						case "error":
							#mail("code@irelocation.com","APX Response - ".$response[0], "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										APX,0,"tsc");
							break;
						default:
							#mail("code@irelocation.com","APX Unknown - ".$response[0], "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			#mail("code@irelocation.com","APX Error Response","QuoteID: ". $this->myarray["quote_id"]."\n".$response);
					
		}
	}
	
	//-- THIS IS THE OLD BRINKS CLASS - USE THE NEW ONE AT THE BOTTOM
	class Brinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()    //only owners as of 12/5/6 @ 8:45am; 12/17/09 13:42 PM - Brinks doesn't want COM leads anymore
		{			
#			return ((strtolower($this->thearray["own_rent"]) == "own") || ($this->thearray['quote_type'] == 'com'));
			return (strtolower($this->thearray["own_rent"]) == "own");
		}
				
		function Brinks()
		{
			$this->url = "ftp.brinks.com";
			$this->lead_id = BRINKS;
		}	
			
		
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 0;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			$e21 = "";
			$e22 = "";
			$e23 = "";
			$e24 = "";
			$e25 = "";
			$e26 = "";
			$e27 = "";
			$e28 = "";
			$e29 = "";
			
			
			if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
				$marketsource = "N751";
			else			
				$marketsource = "i90376";			
			
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,$marketsource,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";			
			
			echo "<Br/>Brinks LEAD: #".$this->thearray['quote_id'].": ".$this->lead_body."<Br/><Br/>";
			//mail("code@irelocation.com","Brinks #".$thearray["quote_id"],$this->lead_body);
//			return trim($lead_body);
		}
			
		/* END NEW FORMAT */	
			
		function send()
		{
			$result = true;
			$quote_id = $this->thearray["quote_id"];
			//Global Variables are turned off, so i have to put it to a scope where
			//Var.php can reach it. SESSION seems most reasonable..
			$_SESSION["text"] = $this->lead_body;	
							
			$login = "irelocation";
			$password = "Jun3bug!";
			$subdir = "In/";
			$ext = ".txt";			

			$fp = fopen('var://SESSION/text','r');
			$conn_id = ftp_connect($this->url);
				
			$login_result = ftp_login($conn_id, $login, $password);
			
			// check connection
			if ((!$conn_id) || (!$login_result)) 
			{				
				#mail(DEGUB_TSC_EMAIL,"Brinks FTP Connection Failure - TSC","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." ".$quote_id ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				$result = false;
			}
			else
			{
				// upload the file
				$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
				
				$update_response = print_r($upload,true);
				
				// check upload status
				if (!$upload) 
				{
					#mail(DEGUB_TSC_EMAIL,"Brinks FTP Upload Failure - TSC","$update_response \n\n Attempted to connect to ".$this->url." for user $login\n".date("YmdHis")." ".$quote_id ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = false;
				} 
				else
				{
					#mail("leads@brinks.com","iRelocation - $quote_id",$this->lead_body);
					#mail("code@irelocation.com","Brinks Lead - TSC - $quote_id",$this->lead_body . "\n\n update_response");
					
				
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = true;
				}	
				
			}
			
			$this->processResponse($result);			
		}
		
		function processResponse($response)
		{			
			if ($response)
			{
				save_response($this->thearray["quote_id"],"FTP Accepted",BRINKS,1,"tsc");				
			}
			else
			{
				save_response($this->thearray["quote_id"],"FTP Rejected",BRINKS,0,"tsc");
				recordLeadFailure($this->thearray,
							1519,
							"security",
							"irelocation.leads_security");
			}
		}		
		
		#------------------------------------------------------------------------------------------------------------
		#
		#			BRINKS FORMAT
		#
		#------------------------------------------------------------------------------------------------------------
			
			
		/*
			Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
			
			Data files should be in plain text, comma-delimited format with one record per line.
			The delimiter character can be changed on a case-by-case basis. Given that commas 
			are used to delimit fields, and carriage returns are used to delimit records, care 
			must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
			returns are removed from data before being included in the file.
			
			Additional fields may be added to the specification upon agreement between Brinks and 
			the partner in question. The data file may not be jagged-that is, if a field is null, 
			it must still be included in the output file as an empty string. Each line must contain
			n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.
	
			The first line of the file should NOT contain field names. Field names are already
			known as long as the aforementioned format is followed.
	
			The file should contain no blank lines, and no lines with incomplete records.
	
			
			1.	First name (20 chars max)
			2.	Last name (20 chars max)
			3.	Street Number (20 chars max)
			4.	Street Name  (30 chars max, optional)
			5.	Additional Address (30 chars max, optional)
			6.	City (30 chars max)
			7.	State (2 char abbreviation)
			8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
			9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
			10.	Secondary Phone (optional)
			11.	E-mail address (40 chars max, optional)
			12.	Comment (255 chars max-may not contain carriage returns, optional)
			13.	Renter (1 char, 1 = true 0 = false)
	
			Sample File: brinks/sample.txt
			
		*/				
	}
	
	//Change made: No Commercial Leads, effective 1pm 10/16/06
	/*
	class USAlarm extends SecurityClass
	{
		function USAlarm()
		{
			$this->url = "http://www.usalarm.com/leadspost/irelocation.php";
			$this->lead_id = USALARM;
		}	
			
			

		function test()
		{
			$zip = $this->thearray["zip"];
			$lead_content = "quote_id: ".$this->thearray['quote_id']."\nquote_type: ".$this->thearray["quote_type"]."\nown_rent: ".strtolower($this->thearray["own_rent"])."\nSent?";
			if ($this->thearray["quote_type"] == "res" && strtolower($this->thearray["own_rent"]) == "own")
			{
				//mail("DEGUB_TSC_EMAIL","USAlarm Test", $lead_content );
				
				$sql = "select * from irelocation.protectionone_zips where zip = '$zip' limit 1";
				echo "<br/>\n".$sql."<br/>\n";
				$rs = new mysql_recordset($sql);			
				$result = !($rs->fetch_array());
				$rs->close();
				if ($result)//protection one didnt get it, lets see if they want it...
				{
					$sql = "select * from irelocation.clearlink_zips where zip = '$zip' limit 1";
					echo "<br/>\n".$sql."<br/>\n";
					$rs = new mysql_recordset($sql);			
					$result = ($rs->fetch_array());
					$rs->close();
				}
				echo "Sent: ".($result)?"Yes":"No"."<br/>";
				return $result;
				//the opposite of Protection One.
			}
			else
			{
				return false;
				//mail("DEGUB_TSC_EMAIL","USAlarm Test", $lead_content );		
			}
		}
			
		
			http://www.usalarm.com/leadspost/post.php?kbid=1118&sub=test&FirstName=Bruce&
			LastName=Westenskow&Telephone=8019376964&TelephoneExt=6964&CellPhone=8015806096&
			Address=5202%20Douglas%20Corrigan%20Way&City=Salt%20Lake%20City&State=UT&
			Zip=84116&Email=bruce@clear-link.com&Owner=Y&CurAlarmSys=N&Residential=Y
		
			
		function format()
		{
			extract($this->thearray);		
			$own = (($own_rent == "own")?"Y":"N");		
			$current = (($needs!="upgrade")?"N":"Y");			//so it is new or replace
			
			$fields = "FirstName=".urlencode($name1)."&LastName=".urlencode($name2).
						"&Telephone=$phone1&State=$state_code&kbid=6450";
			$fields .= "&email=".urlencode($email)."&Landline=Y&Owner=".(($own_rent == "own")?"Y":"N").
						"&CurAlarmSys=$current&Residential=".(($quote_type="res")?"Y":"N");
						
			if (strlen($phone2) == 10)
				$fields .= "&CellPhone=$phone2";
				
			return $fields;
		}
	
		function send()
		{	
			$fields = $this->format();
			
			$ch=curl_init($this->url."?".$fields);				
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			echo "USalarm Response: ".$response;
			curl_close($ch);
			
			$this->processResponse($response);			
		}
		
		function processResponse($response)
		{			
			
			if ($response == "1")	
			{					
				echo "USalarm Response: Success";	
				save_response($this->thearray["quote_id"], "Valid", USALARM,1,"tsc");
			}
			else
			{							
				echo "USalarm Response: Failure";	
				save_response($this->thearray["quote_id"], "INVALID:\n".$response, USALARM,0,"tsc");			
				mail("dave@irelocation.com","US ALARM Test Lead","Quote Id: ".$this->thearray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
			}
		}		

	}
*/	
	

	class ProtectionOne extends SecurityClass
	{
		function ProtectionOne()
		{
			$this->url = "http://www.protectionone.com/ProspectUpdate/Pro1.asmx";
			$this->lead_id = PROTECTION_ONE;
		}
		
		function formatTextEmail()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->email_headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->email_message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->email_message .= "Alternate Phone: $phone2\n";
					
			$this->email_message .= "Street Address: $address\n".
						"City: $city\n".
						"State: $state_code\n".
						"Zip Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Type of Business: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
		function test()
		{
			$zip = $this->thearray["zip"];
			
			$com_zips = array("10014","10025","10530","10801","11215","11377","11550","11577","11725","11758","11801","14215","15136","19003","19020","19082","19115","19134","19136","19145","19403","19428","19462","20001","20019","20110","20148","20166","20171","20706","20707","20735","20772","20782","20784","20904","21146","21202","21207","21208","21217","21218","21230","21244","21401","21784","22204","22602","23111","23221","23320","23435","23513","23602","23805","23860","25401","27104","27301","27405","27406","27502","27516","27526","27545","27587","27610","27614","27616","28078","28269","28277","28306","28334","28601","29142","29229","29445","29485","29607","30004","30012","30022","30040","30043","30044","30045","30047","30062","30067","30084","30087","30088","30092","30093","30096","30097","30120","30122","30135","30157","30168","30236","30291","30294","30308","30314","30318","30328","30329","30338","30344","30345","30519","30566","32073","32210","32211","32224","32225","32226","32707","32713","32725","32809","32826","32828","32829","32901","32909","33004","33009","33013","33014","33015","33016","33019","33021","33024","33025","33027","33034","33067","33069","33127","33129","33155","33157","33169","33172","33177","33186","33193","33196","33309","33311","33317","33324","33326","33334","33351","33407","33410","33411","33415","33428","33437","33441","33458","33461","33510","33511","33569","33615","33619","33634","33713","33811","33881","33905","33907","33980","34202","34711","34743","34744","34748","34761","34771","37013","37040","37086","37138","37174","37207","37211","38016","40356","40511","41048","43026","43068","43137","43204","43215","43223","43229","43230","43232","44120","44646","44685","45103","45415","46220","46226","46237","46254","46260","46342","46783","47150","48036","48108","48114","48127","48174","48182","48190","48198","48212","48223","48227","48314","48335","48336","49006","49015","50703","53115","54016","54017","55033","55104","55117","55311","55331","55369","55409","55410","55426","55449","60007","60025","60048","60050","60068","60074","60089","60101","60103","60120","60137","60435","60466","60467","60473","60477","60517","60527","60540","60559","60564","60607","60613","60614","60617","60619","60622","60623","60626","60629","60630","60638","60641","60643","60805","61821","63025","63031","63136","63146","63147","63303","64014","64086","64111","64116","64131","67206","67220","73112","74145","75006","75023","75025","75032","75034","75041","75051","75061","75069","75071","75077","75080","75115","75137","75149","75217","75218","75219","75227","75229","75230","75234","75252","76002","76006","76011","76028","76034","76040","76063","76112","76133","76180","76226","76258","77002","77004","77006","77007","77008","77011","77015","77021","77034","77045","77063","77064","77066","77067","77070","77075","77081","77084","77092","77095","77098","77099","77338","77373","77375","77381","77386","77388","77396","77429","77447","77449","77459","77478","77489","77493","77494","77506","77520","77535","77546","77571","77706","77707","78130","78155","78163","78212","78213","78233","78244","78250","78641","78666","78752","78758","79936","79938","80012","80124","80125","80219","80601","81521","83709","84043","84097","84119","84401","84770","85009","85016","85017","85020","85029","85048","85051","85203","85206","85208","85248","85254","85258","85260","85296","85304","85705","85748","87401","89030","89031","89052","89101","89102","89109","89117","89130","89139","89145","90001","90016","90019","90026","90036","90041","90048","90064","90220","90221","90232","90255","90272","90277","90278","90302","90620","90621","90703","90706","90746","91011","91016","91307","91342","91344","91436","91601","91604","91710","91739","91740","91744","91761","91776","91910","91913","92008","92021","92026","92037","92064","92104","92105","92113","92128","92154","92201","92223","92234","92335","92394","92407","92503","92530","92562","92563","92584","92592","92646","92647","92688","92705","92707","92801","92804","92806","92833","92866","92880","92881","93307","93313","93534","93550","93552","93720","94044","94118","94122","94124","94303","94509","94538","94544","94550","94558","94565","94589","94605","94608","94806","94901","95008","95020","95037","95112","95125","95131","95136","95336","95361","95377","95616","95648","95758","95814","95816","95821","95834","95901","97007","97024","97123","97206","98003","98005","98012","98023","98028","98032","98071","98144","98188","98201","98296","98405","98661","99352");
			
			$own_rent = strtolower($this->thearray["own_rent"]);
			$quote_type = strtolower($this->thearray["quote_type"]);
			
			//-- if it's a residential rental, they don't want it
			if ($own_rent == "rent" && $quote_type == "res") {
				return false;
			}

			//-- if it's a res lead and a zip code in the com_zip array, they don't want it
			if ( $quote_type == "res" && in_array("$zip", $com_zips) ) {
				return false;
			} 
			
			//-- otherwise, check the zips in the table for them
			$sql = "select * from irelocation.security_zip_codes where zip = '".trim($zip)."' and lead_id = '".$this->lead_id."' ";
					
			echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			$result = $rs->fetch_array();			
			echo ($result)?"true":"false";
			
			return $result;
		}
	
		function format_fields()
		{		
			extract($this->thearray);
			$parameters = array();
			$parameters["Site_type"] = $quote_type;
			
			if ($quote_type == "res")
				$parameters["Name"] = "$name2, $name1 ";
			else
			{
				$parameters["Name"] = "$name1";//tack Name2 onto comments...
				$commercial = true;
			}		
			$parameters["Address1"] = $address;		
			$parameters["City"] = $city;
			$parameters["State"] = $state_code;
			$parameters["Zip"] = $zip;		
			$parameters["Phone1"] = $phone1;
			if (strlen($phone2) == 10)
				$parameters["Phone2"] = $phone2;			
			
			$parameters["Email"] = $email;
	
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type, ";
			$newcomments .=	"Sqr. Footage: $sqr_footage, ";
				
			if ($commercial)
			{
				$newcomments .=	"Number of Locations: $num_location, ";
				$newcomments .= "Contact Name: $name2";
			}			
			
			
			$newcomments .= "Cust Comments:".$comments;
			$parameters["Comments"] = $newcomments;
							
			return $parameters;	
		}
		
		function format()
		{
			/*
			echo "Sending to ProtectionOne<br/>";
			$parameters = $this->format_fields();
			
			$this->lead_body ="<"."?xml version=\"1.0\" encoding=\"utf-8\""."?".">
			<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
			<soap:Header>
			<AuthHeaders xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
			  <Username>pro1</Username>
			  <Password>pro1</Password>
			</AuthHeaders>
			</soap:Header>
			<soap:Body>
			<ProtectionOne xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\">
			  <Prospect>
				<Site_type>".$parameters["Site_type"]."</Site_type>
				<Name>". xmlentities($parameters["Name"])."</Name>
				<Address1>". xmlentities($parameters["Address1"])."</Address1>
				<Address2></Address2>
				<City>".xmlentities($parameters["City"])."</City>
				<State>".xmlentities($parameters["State"])."</State>
				<Zip>".$parameters["Zip"]."</Zip>
				<Phone1>".$parameters["Phone1"]."</Phone1>
				<Ext1></Ext1>
				<Phone2>".$parameters["Phone2"]."</Phone2>
				<Ext2></Ext2>
				<Email>".xmlentities($parameters["Email"])."</Email>
				<Comments>".xmlentities($parameters["Comments"])."</Comments>
				<Com_System_Type></Com_System_Type>	
				<promo_id>TSCOM</promo_id>
			  </Prospect>
			</ProtectionOne>
			</soap:Body>
			</soap:Envelope>";
			*/
		}
		
		function send()
		{
			/*
			$ch=curl_init($this->url);				
			$this_header = array("Host: www.protectionone.com", "Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($this->lead_body), "SOAPAction: \"http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne\"");
			curl_setopt($ch, CURLOPT_HEADER, 1);	
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->lead_body);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
			curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			$response=curl_exec($ch);		
			curl_close($ch);					
			
			echo "<textarea cols='60' rows='30'>$this->lead_body</textarea><Br/>\n";
			echo "<textarea cols='60' rows='30'>$response</textarea><Br/>\n";
			$this->processResponse($response);
			*/
			$this->formatTextEmail();
			mail("wichitatelesales@protectionone.com","iRelocation.com Security Lead",
					$this->email_message,$this->email_headers);
		}
		
		function processResponse($response)
		{
			$start = strpos($response,"<Prospect_No>")+strlen("<Prospect_No>");
			$end = strpos($response,"</Prospect_No>");
			if ($start > -1 && $end > $start)
			{		
				$id = substr($response,$start,($end-$start));
				echo "Protection One Lead - Success \n<br/>Response Id: $id<br/>";
				save_response($this->thearray["quote_id"],  $id, PROTECTION_ONE,1,"tsc");
			}
			else
			{			
				echo "Protection One Lead - Failure \n<br/>Quote Id: ".$this->thearray["quote_id"]."<br/>";
				save_response($this->thearray["quote_id"], "INVALID:\n".$response, PROTECTION_ONE,0,"tsc");			
				#mail(DEBUG_TSC_EMAIL,"Protection One Lead Failure","Quote Id: ".$this->thearray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
			}
		}		
	}
		
	
	
	class Pinnacle extends AlarmTrax
	{
		function Pinnacle()
		{
			parent::AlarmTrax(PINNACLE);
			$this->lead_type_id = "";
		}	
		
		function test()
		{
			if ($this->thearray['campaign'] == "pin") return true;
			else return pinnacle_test($this->thearray['state_code'],$this->thearray['zip']);
		}
	}
	
	class Gaylord extends AlarmTrax
	{
		function Gaylord()
		{
			parent::AlarmTrax(GAYLORD);
			$this->lead_type_id = "3";
			$this->company = "gaylord";
		}	
		
		function test()
		{
			return ($this->thearray['source'] != "aff_p4p");
		}
		/*
		function formatForEverSafe()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		*/
		function send()
		{					
			$eversafe_states = "NJ  PA  DE  MD  OH";
			$absolute_states = "FL  GA  WI VA";								
								
			//if() run code for mark alliata
			$state = $this->thearray["state_code"];	
			$quote_id = $this->thearray["quote_id"];
			$aff = false;
			
			if (substr_count($eversafe_states,$state) > 0)//send them the lead too!
			{				
				$rec = $this->thearray["received"];				
				$this->setGaylordAffiliate(GAY_EVERSAFE);
				parent::format();
				$aff = true;
			}							
			else if (substr_count($absolute_states,$state) > 0)
			{	
				$rec = $this->thearray["received"];				
				$this->setGaylordAffiliate(GAY_ABSOLUTE);	
				parent::format();
				$aff = true;
			}							
			else			
			{
				$this->setGaylordAffiliate(GAY_ROOT);		
				parent::format();
			}
			
			if ($aff)
			{
				//save it for our records.
				$sql = "insert into irelocation.gaylord_security_leads ".
						"(quote_id,received,company) ".
						" values ('".$quote_id."','".date("Ymdhis")."','".$this->company."') ";
				$rs = new mysql_recordset($sql);					
				$rs->close();						
			}
			
			parent::send();				
		}		
		/*
		function sendAbsolute($xml)
		{
			echo "sendAbsolute()<br/>";
			//$absolute_url = "https://secure.securitytrax.com/absolute/leadpostxml.php";
			$absolute_url = "https://secure.securitytrax.com/absolute/leadpost.php";
			$c = curl_init($absolute_url);
			
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER,false);
		    curl_setopt($c, CURLOPT_POST, 1);
		    curl_setopt($c, CURLOPT_POSTFIELDS, $xml);
		    curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
		    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		    $page = curl_exec($c);
		    curl_close($c);
			return $page;
		}
		
		function formatAbsolute()
		{
			echo "formatAbsolute()<br/>";
			$xml = '<'.'?xml version="1.0" encoding="ISO-8859-1" standalone="yes"?'.'>'.
					'<leads><lead>'.
						'<firstname>'.xmlentities($this->thearray['name1']).'</firstname>'.
						'<lastname>'.xmlentities($this->thearray['name2']).'</lastname>'.
						'<address>'.xmlentities($this->thearray['address']).'</address>'.
						'<city>'.xmlentities($this->thearray['city']).'</city>'.
						'<state>'.xmlentities($this->thearray['state_code']).'</state>'.
						'<zip>'.xmlentities($this->thearray['zip']).'</zip>'.
						'<email>'.xmlentities($this->thearray['email']).'</email>'.
						'<primaryphone>'.$this->thearray['phone1'].'</primaryphone>'.
						'<secondaryphone>'.$this->thearray['phone2'].'</secondaryphone>'.
						'<besttime>daytime</besttime>'.
						'<rentown>'.$this->thearray['own_rent'].'</rentown>'.
						'<comments>'.$this->thearray['comments'].'</comments>'.
					'</lead></leads>';
			//mail("code@irelocation.com","Absolute XML",$xml);
			return $xml;
		}
		*/
	}
	
	
	
	
	
	
	
	class TextEmail extends SecurityClass
	{
		function TextEmail($email)
		{
			$this->emailaddress = $email;//;
		}
		
		function sqrFootage($type,$sq)
		{
			echo "sqrFootage($type,$sq)<br/>";
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return 2500;
				else if ($type == "com" && $sq == 50) return 20000;
				else if ($type == "res" && $sq == 0) return 1500;
				else if ($type == "res" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		
		function send()
		{
			$this->format();
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function formatHTML()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			echo "Extras: F: $fire A: $access C: $cctv P: $prewired B: $basement <br/>";
			
			echo "Fire:-$fire- <br/>";
			
			$extra_services = array();
			if ($fire == "1")
				$extra_services[] = "fire";
			if ($access  == "1")
				$extra_services[] = "access";
			if ($cctv  == "1")
				$extra_services[] = "cctv";
			if ($prewired  == "1")
				$extra_services[] = "house is prewired";
			if ($basement  == "1")
				$extra_services[] = "basement is finished";		
			
			if (count($extra_services) > 0)
				$extras = implode(",",$extra_services);
			echo "Extras: $extras <br/>";
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->HTMLheaders = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/html; charset=iso-8859-15";
						
			$this->HTMLmessage = 
			"<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
			"<tr><td colspan='2'><strong>Top Security Lead</strong></td></tr>".
			"<tr><td colspan='2'><hr width='250' align='left'/></td></tr>".
			"<tr><td width='200'>Quote ID: </td><td>".$quote_id."</td></tr>".
			"<tr><td width='200'>Received: </td><td>".$received."</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name."</td></tr>".
			"<tr><td width='200'>E-Mail Address: </td><td>".$email."</td></tr>".
			"<tr><td width='200'>Phone Number: </td><td>".$phone1."</td></tr>".
			"<tr><td width='200'>Alternate Phone: </td><td>".$phone2."</td></tr>".
			"<tr><td width='200'>Street Address: </td><td>".$address."</td></tr>".
			"<tr><td width='200'>City: </td><td>".$city.", ".$state_code."</td></tr>".
			"<tr><td width='200'>Postal Code: </td><td>".$zip."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Quote Type: </td><td>".$type."</td></tr>".
			"<tr><td width='200'>Building Type </td><td>".$building_type."</td></tr>".
			"<tr><td width='200'>Square Footage: </td><td>".$sqr_footage."</td></tr>".
			"<tr><td width='200'>Owners: </td><td>".$own_rent."</td></tr>".
			"<tr><td width='200'>Requested Services/Features: </td><td>".$extras."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td colspan='2' align='left'>Comments:</td></tr>".
			"<tr><td colspan='2' align='left'>".$comments."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Lead Source: </td><td>iRelocation</td></tr>".
			"</table>";	
		
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"State: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Business Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
	}
	
	class Monitronics extends TextEmail
	{
		function Monitronics()
		{
			$this->lead_id = MONITRONICS;
			$this->emailaddress = "leads@monitronics.com";
		}
		
		function test()
		{		
			/*
			$start = "20070907080000";//(GMT - 7:00) 6:00am our time.
			define(TIMEZONE_OFFSET,18000);
			$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);
			
			$active = ($now > $start);
			*/
			/*
			
			echo "Monitronics Test:<br/>\n".
				 "Active: $active <br/>\n".
				 "Not a Pinnacle Lead: $not_pinnacle <Br/> ";
			//return ($active && $not_pinnacle);
			//it is now past 8am on 9/7/2007, so this variable will always be true.
			*/
			//as of 9/10/7
			//return true;
			//$not_pinnacle = !pinnacle_test($this->thearray['state_code'],$this->thearray['zip']);
			return true;

		}
		
		
		function send()
		{
			$this->format();
			if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
				$this->message.="\nLead Generated by Outbound Call Center";
			
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
		
		
	}
	
	
	
	class Sonitrol extends SecurityClass
	{
		function Sonitrol()
		{
			$this->lead_id = SONITROL;
		}
		
		function formatTextEmail()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			$type = "Commercial";
		
			$this->email_headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->email_message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: $name2, $name1\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->email_message .= "Alternate Phone: $phone2\n";
					
			$this->email_message .= "Street Address: $address\n".
						"City: $city\n".
						"State: $state_code\n".
						"Zip Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Type of Business: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
		function test()
		{
			
			$own_rent = strtolower($this->thearray["own_rent"]);
			$quote_type = strtolower($this->thearray["quote_type"]);
			$zip = $this->thearray["zip"];
			
			//-- if it's a residential rental, they don't want it
			if ($quote_type == "res") {
				return false;
			}

			//-- otherwise, check the zips in the table for them
			$sql = "select * from irelocation.security_zip_codes where zip = '".trim($zip)."' and lead_id = '".$this->lead_id."' ";
					
			echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			$result = $rs->fetch_array();			
			echo ($result)?"true":"false";
			
			return $result;
		}
	
		function format_fields()
		{		
			extract($this->thearray);
			$parameters = array();
			$parameters["Site_type"] = $quote_type;
			
			if ($quote_type == "res")
				$parameters["Name"] = "$name2, $name1 ";
			else
			{
				$parameters["Name"] = "$name1";//tack Name2 onto comments...
				$commercial = true;
			}		
			$parameters["Address1"] = $address;		
			$parameters["City"] = $city;
			$parameters["State"] = $state_code;
			$parameters["Zip"] = $zip;		
			$parameters["Phone1"] = $phone1;
			if (strlen($phone2) == 10)
				$parameters["Phone2"] = $phone2;			
			
			$parameters["Email"] = $email;
	
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type, ";
			$newcomments .=	"Sqr. Footage: $sqr_footage, ";
				
			if ($commercial)
			{
				$newcomments .=	"Number of Locations: $num_location, ";
				$newcomments .= "Contact Name: $name2";
			}			
			
			
			$newcomments .= "Cust Comments:".$comments;
			$parameters["Comments"] = $newcomments;
							
			return $parameters;	
		}
		
		function format()
		{
		}
		
		function send()
		{

			$this->formatTextEmail();
			mail("ajosef@sonitrol.com,thickey@sonitrol.com,bdupree@stanleyworks.com","iRelocation.com Security Lead",$this->email_message,$this->email_headers);
		}
		
		function processResponse($response)
		{
/*
			$start = strpos($response,"<Prospect_No>")+strlen("<Prospect_No>");
			$end = strpos($response,"</Prospect_No>");
			if ($start > -1 && $end > $start)
			{		
				$id = substr($response,$start,($end-$start));
				echo "Protection One Lead - Success \n<br/>Response Id: $id<br/>";
				save_response($this->thearray["quote_id"],  $id, PROTECTION_ONE,1,"tsc");
			}
			else
			{			
				echo "Protection One Lead - Failure \n<br/>Quote Id: ".$this->thearray["quote_id"]."<br/>";
				save_response($this->thearray["quote_id"], "INVALID:\n".$response, PROTECTION_ONE,0,"tsc");			
				mail(DEBUG_TSC_EMAIL,"Protection One Lead Failure","Quote Id: ".$this->thearray["quote_id"]." Response: $response","From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");			
			}
*/
		}		
	}
		
	
	
//-- ================================================ NEW BRINKS CLASS USING LEADCONDUIT
//-- 11/17/08 14:30 PM Brinks now wants the street number and name to be a single var instead of splitting up 

	class NewBrinks extends SecurityClass
	{				

		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test() 
		{			
			return ((strtolower($this->thearray["own_rent"]) == "own") && ($this->thearray['quote_type'] != 'com'));
		}
				

		function NewBrinks()
		{
			$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
			$this->lead_id = BRINKS;
		}
	
		function format()
		{

			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C") {
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
				
				$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($lname,0,20);//same with contact name, or long lastname.
				$name1 = $fname;
				$name2 = $lname;
				
				$nodeid = "000wxti4q";
				
			} else {
				$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$name2 = substr($name2,0,20);//same with contact name, or long lastname.
				$nodeid = "000w3xy4j";
			}
			
			
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 0;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			
				
			if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
				$marketsource = "N751";
			else			
				$marketsource = "i90376";			

			if ($sitetype == "C") {
				$marketsource = "i90399";
			}

/* OLD
		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetNumber=".urlencode($street_number)."&".
			"StreetName=".urlencode($street_name)."&".
			"City=".urlencode($city)."&".
			"State=".urlencode($state_code)."&".
			"ZipCode=".urlencode($zip)."&".
			"PrimaryPhone=".urlencode($phone1)."&".
			"EmailAddress=".urlencode($email)."&".
			"BusinessName=".urlencode($businessname)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);
*/

		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetAddress=".urlencode($address)."&".
			"City=".urlencode($city)."&".
			"State=".urlencode($state_code)."&".
			"ZipCode=".urlencode($zip)."&".
			"PrimaryPhone=".urlencode($phone1)."&".
			"EmailAddress=".urlencode($email)."&".
			"BusinessName=".urlencode($businessname)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);

#		mail("code@irelocation.com","NEW BRINKS TSC Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->lead_body);

				
/* old posting
			 $this->lead_body = "xxNodeId=000ktsuc3&".
				"City=".urlencode($city)."&".
				"Email=".urlencode($email)."&".
				"FirstName=".urlencode($name1)."&".
				"LastName=".urlencode($name2)."&".
				"Phone=$phone1&".
				"phone1=$phone_w_dashes&".
				"credit=GOOD&".
				"phone2=$phone2&".
				"AFID=1975&".
				"State=$state_code&".
				"PostalCode=$zip&".
				"Address=".urlencode($address)."&".
				"ip=$remote_ip&".
				"IPAddress=72.3.2226.111&".
				"ownhome=YES";
				
*/
			/*
			if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
				$this->lead_body .= "key3=callcenter-$quote_type";
			else
				"key3=$quote_type";
			*/
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","BRINKS RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "success":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											BRINKS,1,"tsc");
							echo "BRINKS Lead - Success";
							break;
						case "failure":
						case "error":
							#mail("code@irelocation.com","BRINKS Response - ".$response[0], "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										BRINKS,0,"tsc");
							break;
						default:
							#mail("code@irelocation.com","BRINKS Unknown - ".$response[0], "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			#mail("code@irelocation.com","BRINKS Error Response","QuoteID: ". $this->myarray["quote_id"]."\n".$response);
					
		}
	}



//-- =========================================
	class FrontPoint extends SecurityClass
	{				

		function FrontPoint()
		{
#			$this->url = "http://fpfrontport.optimerics.com/1969-1/index.aspx"; //-- TEST URL
#			$this->url = "http://frontport.frontpointsecurity.com/1969-1/index.aspx"; //-- PRODUCTION URL
			$this->url = "http://janus.frontpointsecurity.com/post/index.aspx/74c830c8-aadf-403a-ba46-d68df07bf1a6"; //-- NEW PRODUCTION URL 7/9/09 13:15 PM
			$this->lead_id = FRONTPOINT;
		}
	
		function buildingType($building)
		{
			
/*
			switch($building)
			{
				case "house": return "Residential - Homeowner";			
				case "town house": return "Residential - Homeowner";			
				case "apartment": return "Residential - Renter";
				case "condo": return "Residential - Homeowner";
				case "mobile home": return "Residential - Homeowner";
				case "office": return "Commercial - Small Business";
				default: return "";
			}
*/
			
			
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()
		{
			
			$quote_type = strtolower($this->thearray["quote_type"]);
			$own_rent = strtolower($this->thearray["own_rent"]);
			$zip = $this->thearray["zip"];
			
			//-- if it's a RES RENT lead, they need to get it because APX doesn't want it
			if ( $quote_type == "com" || $own_rent == "rent" ) { //-- ANY RENTer or COMmercial lead -- 7/6/09 17:26 PM
				echo "RES RENT Lead, giving to to FRONTPOINT cuz APX doesn't want these leads<br />";
				return true;
			} 
			
			//-- if we're still here, check to see if the zip is in APX zip list, if so, then return false
			$sql = "select * from irelocation.security_zip_codes where zip = '".trim($zip)."' and lead_id = 1569 ";
			echo "Check to see if 1569 wants the zip: $sql<br />";
			$rs = new mysql_recordset($sql);
			#$result = $rs->fetch_array();		
			$row_count = $rs->rowcount();
			
			if ( $row_count > 0 ) { //-- then 1569 wants the zip (and not a RES RENT lead)
				echo "1569 Wants this zip<br />";
				return false;
			} else {
				echo "1569 does NOT want this zip<br />";
				return true;
			}

		}
				

		function format()
		{

			$thearray = $this->thearray;
			
#			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C") {
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
				
				$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($lname,0,20);//same with contact name, or long lastname.
				$name1 = $fname;
				$name2 = $lname;
				
			} else {
				$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$name2 = substr($name2,0,20);//same with contact name, or long lastname.
			}
			
			
#			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 0;
				
			if ( $own_rent == "Own" ) {
				$owner_type = "Owner";
			} 
			if ( $own_rent == "Rent" ) {
				$owner_type = "Renter";
			} 
						
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$additional_services.= " ExtraServices:";
				if ($fire == 1)
					$additional_services  .= "Fire Protection ";
				if ($access == 1)
					$additional_services  .= "Access Control System ";				
				if ($cctv == 1)
					$additional_services  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$additional_services  .= xmlentities($other_field);
			}
			
			
/*				RESIDENTIAL
				house
				town house
				apartment
				condo
				mobile home
				
				COMMERCIAL
				industrial
				office
				retail
				restaurant
				home-based
				warehouse
				other
*/

		echo "BUILDING TYPE IS $building_type<br />";
		
		if ( $own_rent == "Own" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type)  ) {
#				$p_type = "Residential - Homeowner";
				$p_type = "Residential";
			} 
			
			if ( eregi("industrial",$building_type) || eregi("office",$building_type) || eregi("retail",$building_type) || eregi("restaurant",$building_type) || eregi("home-based",$building_type) || eregi("other",$building_type)  ) {
#				$p_type = "Commercial - Small Business";
				$p_type = "Commercial";
			} 
			
		} elseif ( $own_rent == "Rent" ) {
			if ( eregi("house",$building_type) || eregi("town house",$building_type) || eregi("condo",$building_type) || eregi("mobile home",$building_type) || eregi("apartment",$building_type)  ) {
#				$p_type = "Residential - Renter";
				$p_type = "Residential";
			} 
			
		}
		echo "P TYPE = $p_type<br />";
		
		echo "SQUARE FOOTAGE is $sqr_footage<br />";
		
/*
		if ($quote_type == "res" && $sqr_footage == 0) $sqft = "0-1,499" ;
		if ($quote_type == "res" && $sqr_footage == 15) $sqft = "1,500-2,999" ;
		if ($quote_type == "res" && $sqr_footage == 30) $sqft = "3,000-4,999" ;
		if ($quote_type == "res" && $sqr_footage == 50) $sqft = "5,000-19,999" ;
		if ($quote_type == "res" && $sqr_footage == 200) $sqft = "20,000" ;

		if ($quote_type == "com" && $sqr_footage == 0) $sqft = "0 - 2,499" ;
		if ($quote_type == "com" && $sqr_footage == 25) $sqft = "2,500 - 4,999" ;
		if ($quote_type == "com" && $sqr_footage == 50) $sqft = "5,000 - 9,999" ;
		if ($quote_type == "com" && $sqr_footage == 100) $sqft = "10,000 - 49,999" ;
		if ($quote_type == "com" && $sqr_footage == 500) $sqft = "50,000" ;
		
		echo "NOW SQFT IS SET TO $sqft<br />";
*/

/*
		  RES
		  <option value="0">0 - 1,499 ft</option>
		  <option   value="15">1,500 - 2,999 ft</option>
		  <option   value="30">3,000 - 4,999 ft</option>
		  <option   value="50">5,000 - 19,999 ft</option>		  
		  <option   value="200">20,000+ ft</option>
		  
		  COM
		  <option  value="0">0 - 2,499 ft</option>
		  <option  value="25">2,500 - 4,999 ft</option>
		  <option  value="50">5,000 - 9,999 ft</option>
		  <option  value="100">10,000 - 49,999 ft</option>		  
		  <option  value="500">50,000+ ft</option>


IF LEAD IS RESIDENTIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Home' question in "DESC" Field as follows:  "Type of Home:  "[House; Apartment; Townhouse; Condo; Mobile Home]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-1,499; 1,500-2,999; 3,000-4,999; 5,000-19,999; 20,000+]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]
IF LEAD IS COMMERCIAL, ADD RESPONSES TO THE QUERIES ASKED:
	*	Add response to 'Current Needs' question in "DESC" Field as follows:  "Current Needs:  "[New Security System; Upgrade Current Service; Replace Current Service]
	*	Add response to 'Type of Business' question in "DESC" Field as follows:  "Type of Business:  "[Office, ....]
	*	Add response to 'Square Footage' question in "DESC" Field as follows:  "Square Footage:  "[0-2,499;...]
	*	Add response to 'Do you own or rent' question in "DESC" Field as follows:  "Ownership Status: "[Own; Rent]

	*	Add response to the 'How Many Locations' question in DESC Field:  "# of Locations:  "[1, ..., 10+]
	*	Add responses to the "Additional Services" questions in DESC Field:  "Additional Services:  " [Fire Protection, Access Control, Video Surveillance, Other: "  "]
	*	Ad response to the "ADditional Comments" text box:  "Additional comments:  "[  ]
	*	

*/

		
		if ( $quote_type == "res" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Home: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			
		} elseif ( $quote_type == "com" ) {
			$newcomments .=	"\nCurrent Needs: $current_needs";
			$newcomments .=	"\nType of Business: $building_type";
			$newcomments .=	"\nSquare Footage: $sqr_footage";
			$newcomments .=	"\nOwnership Status: $own_rent";
			$newcomments .=	"\n# of Locations: $num_locations";
			$newcomments .=	"\nAdditional Services: $additional_services";

		}
		
		$newcomments .=	"\nAdditional comments: $comments";
		
		$newcomments = trim($newcomments);	
		

/*
		 $this->lead_body = "FIRSTNAME=".urlencode($name1)."&".
			"LASTNAME=".urlencode($name2)."&".
			"ADDRESS=".urlencode($address)."&".
			"CITY=".urlencode($city)."&".
			"STATE=".urlencode($state_code)."&".
			"ZIP=".urlencode($zip)."&".
			"PHONE=".urlencode($phone1)."&".
			"ALTPHONE=".urlencode($phone2)."&".
			"EMAIL=".urlencode($email)."&".
			"REF=".$this->thearray["quote_id"]."&".
			"OWNERSHIP=".urlencode($owner_type)."&".
			"PREMISETYPE=".urlencode($p_type)."&".
			"DESC=".urlencode($newcomments);
*/

			//-- THIS IS FOR THE NEW Posting URL that they're using
		 $this->lead_body = "FIRSTNAME=".urlencode($name1)."&".
			"LASTNAME=".urlencode($name2)."&".
			"STREET1=".urlencode($address)."&".
			"CITY=".urlencode($city)."&".
			"STATE=".urlencode($state_code)."&".
			"ZIP=".urlencode($zip)."&".
			"PHONE=".urlencode($phone1)."&".
			"MOBILE=".urlencode($phone2)."&".
			"EMAIL=".urlencode($email)."&".
			"REF=".$this->thearray["quote_id"]."&".
			"OWNERSHIP=".urlencode($owner_type)."&".
			"PROPERTYTYPE=".urlencode($p_type)."&".
			"CAMPAIGN=".urlencode("TopAlarm-US")."&".
			"DESC=".urlencode($newcomments);

		#mail("code@irelocation.com","NEW FRONTPOINT TSC Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->url."?".$this->lead_body);

				
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			curl_setopt($ch, CURLOPT_POST, 1); //-- they want it in POST cuz they can't accept GET  (SAY WHAT?  Must be stoopid .NET programming)
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
/* Original function that's not working
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strpos($response,"<resp") > 0)
			{
				$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
				//mail("code@irelocation.com","FRONTPOINT RESPONSE",$response);
				if (strpos($response,"\n") > 0)
				{
					$response = split("\n",$response);
					$response[1] = trim($response[1]);
					switch($response[0])
					{
						 case "SUCCESS":
						 case "queued":
							// save_response($quote_id, $result, $lead_id,$sucess,$site)
							save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
											FRONTPOINT,1,"tsc");
							echo "FRONTPOINT Lead - Success";
							break;
						case "FAILED":
						case "error":
						case "Bad Request":
							mail("code@irelocation.com","FRONTPOINT Bad Response - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"tsc");
							echo "FRONTPOINT Lead - FAIL";
							break;
						default:
							mail("code@irelocation.com","FRONTPOINT Unknown - ".$response[0],
								 "QuoteID: ".$this->myarray["quote_id"]."\n".$response);
							 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										FRONTPOINT,0,"tsc");
							echo "FRONTPOINT Lead - UNKNOWN";
							break;
					}		
					//its all good.	
					return;
				}
			}
			
			//are you still processing?? 
			//something went wrong if it gets here.			
			mail("code@irelocation.com","FRONTPOINT Error Response","QuoteID: ".
				$this->myarray["quote_id"]."\n".$response);
					
		}
*/

		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			echo $content."<br/>";
			
			if (strlen($response) > 0) {
				$response = trim(strip_tags(strtolower($response)));
				if(DEBUG_EMAILS)
					#mail(DEGUB_TSC_EMAIL,"FrontPoint RESPONSE","$response");
				
				//-- Frontpoint changed their response, so need new code to check the response - 2/24/10 13:35 PM
				if (substr_count(strtolower($response),"accepted") == 1) { //-- reports a success
					save_response($this->thearray["quote_id"],$response,FRONTPOINT,1,"tsc");
					
				} else {
					save_response($this->thearray["quote_id"],$response,FRONTPOINT,0,"tsc");
				}


/*				OLD CONDITION
				switch($response) {
					 case "accepted":
					 case "success":
					 case "SUCCESS":
						if(DEBUG_EMAILS)
							mail("code@irelocation.com","FRONTPOINT GOOD Response - ".$response,"QuoteID: ".$this->thearray["quote_id"]."\n".$response);
						save_response($this->thearray["quote_id"],$response,FRONTPOINT,1,"tsc");
						echo "FRONTPOINT Lead - Success";
						break;
					case "failed":
						if(DEBUG_EMAILS)
							mail("code@irelocation.com","FRONTPOINT Bad Response - ".$response[0],"QuoteID: ".$this->thearray["quote_id"]."\n".$response);
						 save_response($this->thearray["quote_id"],$response,FRONTPOINT,0,"tsc");
						echo "FRONTPOINT Lead - FAIL";
						break;
					default:
						if(DEBUG_EMAILS)
							mail("code@irelocation.com","FRONTPOINT Unknown Response - ".$response[0], "QuoteID: ".$this->thearray["quote_id"]."\n".$response);
						 save_response($this->thearray["quote_id"],$response, FRONTPOINT,0,"tsc");
						echo "FRONTPOINT Lead - UNKNOWN";
						break;
				}		
*/

				return;
			} else {
				
				//something went wrong if it gets here.			
				#mail(DEGUB_TSC_EMAIL,"FRONTPOINT NO Response","This email is sent when the strlen of the response is 0\n\nQuoteID: ". $this->thearray["quote_id"]."\nTheir Response: ".$response);
			}					
		}


	}
//-- =========================================





	class Ingrid extends SecurityClass
	{
		function Ingrid()
		{
			$this->lead_id = INGRID;
		}
		
		function formatTextEmail()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ( $quote_type == "res" ) {
				$type = "Residential";
			} else {
				$type = "Commercial";
			}
			
		
			$this->email_headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->email_message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: $name2, $name1\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->email_message .= "Alternate Phone: $phone2\n";
					
			$this->email_message .= "Street Address: $address\n".
						"City: $city\n".
						"State: $state_code\n".
						"Zip Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Type of Business: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
		function format_fields()
		{		
			extract($this->thearray);
			$parameters = array();
			$parameters["Site_type"] = $quote_type;
			
			if ($quote_type == "res")
				$parameters["Name"] = "$name2, $name1 ";
			else
			{
				$parameters["Name"] = "$name1";//tack Name2 onto comments...
				$commercial = true;
			}		
			$parameters["Address1"] = $address;		
			$parameters["City"] = $city;
			$parameters["State"] = $state_code;
			$parameters["Zip"] = $zip;		
			$parameters["Phone1"] = $phone1;
			if (strlen($phone2) == 10)
				$parameters["Phone2"] = $phone2;			
			
			$parameters["Email"] = $email;
	
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			
			$newcomments .=	"Building Type: $building_type, ";
			$newcomments .=	"Sqr. Footage: $sqr_footage, ";
				
			if ($commercial)
			{
				$newcomments .=	"Number of Locations: $num_location, ";
				$newcomments .= "Contact Name: $name2";
			}			
			
			
			$newcomments .= "Cust Comments:".$comments;
			$parameters["Comments"] = $newcomments;
							
			return $parameters;	
		}
		
		function format()
		{
		}

		function test()
		{
			
			$quote_type = strtolower($this->thearray["quote_type"]);
			$own_rent = strtolower($this->thearray["own_rent"]);
			$zip = $this->thearray["zip"];
			
			//-- if it's a commercial lead, they don't want it
			if ($quote_type == "com") {
				echo "COM Lead, Ingrid doesn't want it<br />";
				return false;
			} 
			
			//-- if it's a RES RENT lead, they need to get it because APX doesn't want it
			if ( $quote_type == "res" && $own_rent == "rent" ) {
				echo "RES RENT Lead, giving to to INGRID cuz APX doesn't want these leads<br />";
				return true;
			} 
			
			//-- if we're still here, check to see if the zip is in either Protection One or APX zip lists, if so, then return false
			$sql = "select * from irelocation.security_zip_codes where zip = '".trim($zip)."' and ( lead_id = 1521 or lead_id = 1569 ) ";
			echo "Check to see if 1521 or 1569 wants the zip: $sql<br />";
			$rs = new mysql_recordset($sql);
			#$result = $rs->fetch_array();		
			$row_count = $rs->rowcount();
			
			if ( $row_count > 0 ) { //-- then 1521 or 1569 wants the zip (and not a RES RENT lead)
				echo "1521 or 1569 Wants this zip<br />";
				return false;
			} else {
				echo "1521 or 1569 does NOT want this zip<br />";
				return true;
			}

		}
	
		
		function send()
		{
			extract($this->thearray);
			$quote_id = $this->thearray["quote_id"];
			echo "QUOTE ID is $quote_id<br />";
			
			$this->formatTextEmail();
			mail("dtruax@ingridhome.com,support@ingridhome.com,asisk@acttoday.com,mwright@acttoday.com,lsturgill@acttoday.com,dknapp@acttoday.com,leads@ingridhome.com","iRelocation.com Security Lead - $quote_id",$this->email_message,$this->email_headers);
			
		}
		
	}

?>