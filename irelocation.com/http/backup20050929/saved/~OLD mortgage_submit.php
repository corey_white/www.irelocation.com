<?
$DO_NOT_TRACK_PAGE_VIEW=true;
include_once("inc_session.php");

#
# Clean up fields
#
$sess_fname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($fname))));
$sess_lname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($lname))));
$sess_email=trim(str_replace(" ","",str_replace("\\","",str_replace("'","",strtolower($email)))));
$sess_phone=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone));
$sess_phone2=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone2));

$property_value=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$property_value));
$loan_amount=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$loan_amount));
$current_mortgage=trim(str_replace("'","",strtolower($current_mortgage)));
$comments=trim(str_replace("'","",strtolower($comments)));

#
# Check required fields
#
if($credit=='XX') $msg="Please select your estimated credit rating!";
if($loan_amount=='') $msg="Please enter your estimated loan amount!";
if($property_value=='') $msg="Please enter your estimated property value!";
if(strlen($sess_phone)<10 || strlen($sess_phone)>11) $msg="Please enter a valid phone number including the area code!";
if(substr_count($sess_email,'@')==0) $msg="Please enter a valid email address!";
if($sess_fname=='' || $sess_lname=='' || $sess_email=='' || $sess_phone=='') $msg="Please fill in all required fields!";

if($msg!='')
{
  include("mortgage.php");	
  exit;
}

#
# Get original campaign, referer, & keyword of this user
#
$rs=new mysql_recordset("select * from stats_pageviews where sid='$sid' and (campaign!='' or referer!='' or keyword!='')");
if($rs->fetch_array())
{
  $campaign=$rs->myarray["campaign"];
  $referer=$rs->myarray["referer"];
  $keyword=$rs->myarray["keyword"];
}

if($campaign!='')
{
  $campaign=$site_id."_".$campaign;
}
else
{
  $campaign=$site_id;
}

#
# Enter lead in database
#
$rs=new mysql_recordset("insert into leads_mortgage (received,fname,lname,email,phone,phone2,interest,property_value,loan_amount,credit,current_mortgage,comments,campaign,referer,keyword) values (NOW(),'$sess_fname','$sess_lname','$sess_email','$sess_phone','$sess_phone2','$interest','$property_value','$loan_amount','$credit','$current_mortgage','$comments','$campaign','$referer','$keyword')");

#
# Direct the user to the thank you page with a close button
#
$sess_mortgage_complete=true;
?>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>The iRelocation Network - Find A Mortgage - Thank You!</title>
</head>

<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#C0C0C0">
<?
if($sess_realtor_complete!=true && $sess_mover_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 324px; top: 317px" id="layer4">
	<table border="0" width="100%" id="table5" cellspacing="0" cellpadding="2">
		<tr>
			<td width="71%">
			<img border="0" src="images/find_mover.gif" width="90" height="12"></td>
		</tr>
		<tr>
			<td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Through 
			our network, you can save up to 50% on your move, from the nation&#39;s 
			leading moving companies! 1 Form, 4 Quotes!</font></td>
		</tr>
		<tr>
			<td width="71%">
			<p align="right"><font face="Tahoma" size="2">
			<a style="text-decoration: none" href="mover.php">
			<font color="#000000">start now &gt;</font></a></font></p>
			</td>
		</tr>
	</table>
</div>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 64px; top: 318px" id="layer3">
	<table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
		<tr>
			<td width="71%">
			<img border="0" src="images/find_realtor.gif" width="100" height="12"></td>
		</tr>
		<tr>
			<td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Get 
			CASH BACK on the sale of your home when you find 
			a real estate agent through our service!</font></td>
		</tr>
		<tr>
			<td width="71%" height="23">
			<p align="right"><font face="Tahoma" size="2">
			<a href="realtor.php" style="text-decoration: none">
			<font color="#000000">start now &gt;</font></a></font></p>
			</td>
		</tr>
	</table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
	<hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
	<hr>&nbsp;</div>
<?
}
elseif($sess_realtor_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 198px; top: 317px" id="layer3">
	<table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
		<tr>
			<td width="71%">
			<img border="0" src="images/find_realtor.gif" width="100" height="12"></td>
		</tr>
		<tr>
			<td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Get 
			CASH BACK on the sale of your home when you find 
			a real estate agent through our service!</font></td>
		</tr>
		<tr>
			<td width="71%" height="23">
			<p align="right"><font face="Tahoma" size="2">
			<a href="realtor.php" style="text-decoration: none">
			<font color="#000000">start now &gt;</font></a></font></p>
			</td>
		</tr>
	</table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
	<hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
	<hr>&nbsp;</div>
<?
}
elseif($sess_mover_complete!=true)
{
?>
<div style="position: absolute; width: 217px; height: 87px; z-index: 1; left: 198px; top: 317px" id="layer4">
	<table border="0" width="100%" id="table5" cellspacing="0" cellpadding="2">
		<tr>
			<td width="71%">
			<img border="0" src="images/find_mover.gif" width="90" height="12"></td>
		</tr>
		<tr>
			<td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Through 
			our network, you can save up to 50% on your move, from the nation&#39;s 
			leading moving companies! 1 Form, 4 Quotes!</font></td>
		</tr>
		<tr>
			<td width="71%">
			<p align="right"><font face="Tahoma" size="2">
			<a style="text-decoration: none" href="mover.php">
			<font color="#000000">start now &gt;</font></a></font></p>
			</td>
		</tr>
	</table>
</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 397px" id="layer7">
	<hr>&nbsp;</div>
<div style="position: absolute; width: 483px; height: 28px; z-index: 1; left: 60px; top: 299px" id="layer6">
	<hr>&nbsp;</div>
<?	
}
?>
<div style="position: absolute; width: 520px; height: 139px; z-index: 1; left: 41px; top: 144px" id="layer2">
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Your request 
	to find a mortgage has been received.&nbsp; You can expect a quick response within 
	the next 24 hours, during normal business days.&nbsp; This program was specifically 
	designed by The iRelocation Network and SIRVA Relocation to provide consumers 
	with access to the best real estate professionals nationwide while providing 
	them with HUGE rewards.</font></p>
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Additional information 
	will be emailed to you at:<b><br>
	<?echo $sess_email;?></b> <br>
	within the next 10 minutes, so please check your email!</font></p>
</div>
<div style="position: absolute; width: 162px; height: 43px; z-index: 1; left: 219px; top: 85px" id="layer1">
	<font face="Tahoma" size="6" color="#E03C41">Thank You!</font></div>
<div style="position: absolute; width: 129px; height: 29px; z-index: 1; left: 241px; top: 431px" id="layer5">
	<b><font face="Tahoma" color="#E03C41">
	<a href="#" onclick="window.close(); return false;"><font color="#E03C41">close 
	window</font></a></font></b></div>
<table border="0" width="600" cellspacing="0" cellpadding="0" height="500" bgcolor="#FFFFFF">
	<tr>
		<td height="53">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td><a target="_blank" href="http://www.irelocation.com">
				<img border="0" src="images/logo_small.gif" width="144" height="53"></a></td>
				<td>
				<img border="0" src="images/slogan.gif" width="341" height="13" align="right"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td background="images/bar.gif" height="18">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td>
		<table border="0" width="100%" id="table3" cellspacing="0" cellpadding="0" height="100%">
			<tr>
				<td width="25">&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td>&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="25">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="20">
		<table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0" height="20">
			<tr>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#BEC3BD">
				<p align="center">
				<img border="0" src="images/clear.gif" width="1" height="1"><b><font face="Tahoma" color="#5E5E5E" size="1">The 
				iRelocation Network � 2004 � Privacy Policy � Terms &amp; Conditions</font></b></p>
				</td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="1">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td height="3">
		<table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" height="3">
			<tr>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#CBBDB9">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</body>

</html>