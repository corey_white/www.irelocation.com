<?php
define(DOC_ROOT,"/var/www/vhosts/irelocation.com/httpdocs");
include_once("Rotater.php");
include_once("LogConfig.php");
		
 class Logger
 { 	
 	//full path to file...
 	var $filename; 
 	var $caller;
	var $levels;	
	var $config;
	var $log_dir = "";
	
 	function Logger($config="")
 	{
 		$this->caller = "";
 	
 		if (is_array($config))
 			$this->config = $config;
 		else 
 			$this->config = ($config = getDefaultLogger($config));
 		$this->log_dir = DOC_ROOT."/".$config["logging_dir"];
 		$this->filename = $this->log_dir.$config['filename'];
 		if (is_array($_SERVER))
 		{
 			if ($_SERVER['SCRIPT_NAME'] != "")
 				$this->caller = $_SERVER["REMOTE_ADDR"]."@".$_SERVER['SCRIPT_NAME']; 				
 		}

		if ($this->caller == "")
			$this->caller = "cron:".str_replace(DOC_ROOT,"",$_SERVER["argv"][0]); 		 		
 		$this->levels = array();
		$this->levels["TRACE"] = 0;
		$this->levels["DEBUG"] = 1;
		$this->levels["INFO"] = 2;
		$this->levels["WARN"] = 3;
		$this->levels["ERROR"] = 4;
		$this->levels["FATAL"] = 5;
 	}
 	
 	function ms()
	{
	    $m = explode(' ',microtime());
	    $m = (int)round($m[0]*1000,3);
	    if ($m < 10)
	    	$m = "00".$m;
	    else if ($m < 100)
	    	$m = "0".$m;	    
	    return $m;
	} 
	
 	function _append($text, $level)
 	{
 		if ($this->levels[$level] < $this->levels[$this->config["level"]])	
 			return;
		
		if ($this->config["rotater"]->test($this->filename))
			$this->_archive(); 	
		
 		$fh = fopen($this->filename,"a");  		
 		$text = "[ ".date($this->config["date_format"]).".".$this->ms()." ".$this->caller." ] ".$level.": ".$text."\n";
 		fwrite($fh,$text);
 		//echo $text;
 		fclose($fh);
			
 	}
 	
 	function _archive()
 	{
 		$dest = $this->log_dir.$this->config['filename'].$this->config["rotater"]->nameAddOn();
 		copy($this->filename,$dest);
 		unlink($this->filename);
 	}
 	
 	function debug($text){	$this->_append($text,"DEBUG"); 	}
 	function warn($text){	$this->_append($text,"WARN"); 	}
 	function error($text){	$this->_append($text,"ERROR"); 	}
 	function info($text) {	$this->_append($text,"INFO"); 	}
 	
 }
?>
