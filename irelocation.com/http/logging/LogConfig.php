<?php
/*
 * Created on Feb 6, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 function getDefaultLogger($filename="")
 {
 	if ($filename == "") $filename = "test.txt";	
 
 	$config = array();
 	//most commonly changed option...
 	$config["filename"] = $filename;
 	$config["date_format"] = "Y-m-d H:i:s";
 	$config["logging_dir"] = "logs/";
 	$config["level"] = "DEBUG";
 	$config["rotater"] = new DailyRotater();
 	return $config;
 }	
 
?>
