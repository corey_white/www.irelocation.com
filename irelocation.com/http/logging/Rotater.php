<?php
/*
 * Created on Feb 6, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 class Rotater
 {
 	function test($filename)
 	{
 		return false;
 	}

	function nameAddOn()
	{
		return "";	
	}
}

class FileSizeRotater extends Rotater
{
	var $maxsize =  0;
	
	function FileSizeRotater($maxsize="")
	{		
		//default to 10 MB
		if ($maxsize == "") $maxsize = 10000000;	
		$this->maxsize = $maxsize;		
	}
	
	function test($filename)
 	{
 		return (filesize($filename) > $this->maxsize);
 	}

	function nameAddOn()
	{
		return "";	
	}
}

class DailyRotater extends Rotater
{	
	var $time;
	var $format;
	
	function DailyRotater($format="")
	{
		if ($format == "") $format = "Ymd";
		$this->format = $format;
	}

	function test($filename)
	{
		if (file_exists($filename))	
		{
			$time = date($this->format,filemtime($filename));
			$this->time = $time;
	 		$now = date($this->format);
	 		if ($time != $now) 			
	 			return true;
		}
	}
	
	function nameAddOn()
	{
		return ".".$this->time;
	}
}
 
?>
