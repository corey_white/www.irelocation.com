<?
	include_once "../nusoap/nusoap.php";
	
	define("REPORTIDS_FILE","/tmp/reportids.txt");
	define("URLS_FILE","/tmp/url-response.txt");
	define("REPORTIDS_FILE","/tmp/reportids.txt");

	define("LICENSEKEY","B42D5434B8A38069");
	define("MCCID",94335);
	define("USERNAME","vsmith");
	define("CODE","m02b223");
	define("CONTENT_TYPE","text/xml; charset=utf-8");
	define("REPORT","https://ews11.marketing.ews.yahooapis.com:443/services/V2/BasicReportService");
	
	define("LOCATION","https://global.marketing.ews.yahooapis.com/".
						"services/V2/LocationService");
	
	$accounts = array("cs" => "7175746500", "tm" => "2043934210",
						"tm_2" => "1446559595", "tsc" => "1509327112");
	
	function getHeader()
	{
		return '<soap:Header>
	<wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">
		<wsse:UsernameToken soapenc:arrayType="xsd:string[2]" xsi:type="soapenc:Array">
			<wsse:Username xsi:type="xsd:string">'.USERNAME.'</wsse:Username>
			<wsse:Password xsi:type="xsd:string">'.CODE.'</wsse:Password>
		</wsse:UsernameToken>
	</wsse:Security>
	<license xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.LICENSEKEY.'</license>
	<masterAccountID xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.MCCID.'</masterAccountID>
</soap:Header>';
	}
	
	$soap = new soapclient(LOCATION."?wsdl");
	$soap->setHeaders(getHeader());		
	//print_r($soap);
	if (($err = $soap->getError()))
	{
		echo $err;
		echo $soapclient->response;
	}
	else
	{
		$proxy = $soap->getProxy();
		print_r($proxy);
	}
?>