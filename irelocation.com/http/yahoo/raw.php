<?
	/*
	Array
	(
		[0] => ('cs','159.42','77','overture','20070710')
		[1] => ('cs','123.09','61','overture','20070711')
		[2] => ('tm','21.57','14','overture','20070710')
		[3] => ('tm','23.06','13','overture','20070711')
		[4] => ('tm_2','4.34','11','overture','20070710')
		[5] => ('tm_2','4.43','7','overture','20070711')
		[6] => ('tsc','103.23','99','overture','20070710')
		[7] => ('tsc','163.94','143','overture','20070711')
	)
	*/
	
	$off = substr(date("O"),0,3).":00";
	
	define(GMT_OFFSET,$off);		
	define(ONE_DAY,24*60*60);
	define("PENDING_FLAG_FILE","/tmp/pending.txt");
	define("REPORTIDS_FILE","/tmp/reportids.txt");
	define("URLS_FILE","/tmp/url-response.txt");
	define("REPORTIDS_FILE","/tmp/reportids.txt");

	define("LICENSEKEY","B42D5434B8A38069");
	define("MCCID",94335);
	define("USERNAME","vsmith");
	define("CODE","m02b223");
	define("CONTENT_TYPE","text/xml; charset=utf-8");
	define("REPORT","https://ews11.marketing.ews.yahooapis.com:443/services/V2/BasicReportService");
	
	define("LOCATION","https://global.marketing.ews.yahooapis.com/".
						"services/V2/LocationService");
	
	$accounts = array("cs" => "7175746500", "tm" => "2043934210",
						"tm_2" => "1446559595", "tsc" => "1509327112");
	
	include "xml2Array.php";
	include "../inc_mysql.php";
	
	$command = $_REQUEST['command'];
	
	//mail("david@irelocation.com","Yahoo API Debug","$command was called at ".date("r"));
	
	switch($command)
	{
		case "request":
			if (isset($_REQUEST['one']))
			{
				$name = $_REQUEST['one'];
				$id = $accounts[$name];
				
				$xml = requestReport($name,$id);
				$response = send($xml);
				
				echo "<textarea>$response</textarea><br/>";
				
				$reportid = parseReportResponse($response);
				echo $reportid."<br/>";						
			}
			else
			{
				$reportids = array();
				foreach($accounts as $name => $id)
				{
					$xml = requestReport($name,$id);
					$response = send($xml);
					$reportid = parseReportResponse($response);
					echo $reportid."<br/>";
					$reportids[] = $reportid;
				}
				saveData(REPORTIDS_FILE,$reportids);
			}
			break;
		case "get":
			if (isset($_REQUEST['one']))
			{
				$url_response = send(getReportUrl($reportid));
				$url = parseUrlResponse($url_response);
				echo "<textarea>$url_response</textarea><br/>";
				echo $url;				
			}
			else
			{
				$reportids = loadData(REPORTIDS_FILE);
				echo "<br/>";
				print_r($reportids);
				echo "<br/>";
				$urls = array();
				$pending = false;
				foreach($reportids as $reportid)
				{			
					$url_response = send(getReportUrl($reportid));
					$url = parseUrlResponse($url_response);
					if ($url == "PENDING")
					{
						saveData(PENDING_FLAG_FILE,true);
						$pending = true;
						break;
					}
					$urls[] = $url;					
				}
				if (!$pending)
					saveData(URLS_FILE,$urls);
				else
					echo "PENDING TRY AGAIN!";
			}
			break;
		case "download":
			$urls = loadData(URLS_FILE);
			$data = array();
			foreach($urls as $url)
			{
				$report = getReport($url);	
				$result = parseReport($report);	
				foreach($result as $day)
					$data[] = $day;			
					
			}
			print_r($data);
			insertData($data);
			break;
		//testing only.
		case "load":
			$file = $_REQUEST['file'];
			$data = loadData($file);
			print_r($data);
			break;
		case "location":
			$response = sendLocation();		
			echo "<pre>".$response."</pre>";
			break;
		case "range":
			$start = time() - (ONE_DAY*$_REQUEST['start']);
			$end = time() - (ONE_DAY*$_REQUEST['end']);			
			$reportids = array();
			foreach($accounts as $name => $id)
			{
				$xml = requestRangeReport($name,$id,$start,$end);
				$response = send($xml);
				$reportid = parseReportResponse($response);
				echo $reportid."<br/>";
				$reportids[] = $reportid;
			}
			saveData(REPORTIDS_FILE,$reportids);
	}
	
	function loadData($file)
	{
		$fh = fopen($file,"r");
		$content = fread($fh,filesize($file));
		if (substr_count($content,"\n") > 0)
			return split("\n",$content);
		else
			return $content;
	}
	
	function saveData($file,$data)
	{
		$fh = fopen($file,"w");
		if (is_array($data))
			$d = implode("\n",$data);
		else $d = $data;
		{
			fwrite($fh,$d,strlen($d));
		}
		fclose($fh);
		echo "Data saved to $file <br/>";
	}
	
	function insertData($dataarray)
	{
		if (count($dataarray) == 1)
		{
			$sql = "insert into movingdirectory.costperclick ".
					" (site,cost,clicks,search_engine,ts) ".
					" values ".$dataarray[0];
		}
		else
		{			
			$sql = "insert into movingdirectory.costperclick ".
					" (site,cost,clicks,search_engine,ts) ".
					" values ";
			foreach($dataarray as $line)
			{
				if (strlen(trim($line)) > 0)
					$sql .= "$line,";
			}
			$sql = substr($sql,0,-1);
		}

		echo "<textarea>$sql</textarea><br/>";
		$rs = new mysql_recordset($sql);
		$rs->close();	
		return count($dataarray)." elements saved.";
	}
	
	function reformatDate($date)
	{
		list($month,$day,$year) = split("/",$date);
		if (strlen($year) == 2)
			$year = "20".$year;
		if (strlen($month) == 1)
			$month = "0".$month;
		if (strlen($day) == 1)
			$day = "0".$day;
		
		return $year.$month.$day;
	}
	
	function parseReport($content)
	{
		$lines = split("\n",$content);
		$title = "";
		$next = false;
		
		$import_data = array();
		
		foreach($lines as $line)
		{
			if (strlen(trim($line)) == 0)
				continue;
				
			if ($next)
			{
				$data = split(",",$line);
				$clicks = $data[4];
				$cost = $data[12];
				$date = reformatDate($data[0]);
				$import_data[] = "('$title','$cost','$clicks','overture','$date')";				
			}
			
			if (substr_count($line,"Report Name") == 1)
			{
				$title = trim(substr($line,strpos($line,",")+1));
				if (substr($title,-2) == "_2")
					$title = substr($title,0,-2);//kill extra flag.
			}
			if (substr_count($line,"Totals") == 1)
				$next = true;
		}
		
		return $import_data;
	}
	
	function parseReportResponse($content)
	{
		if (strpos($content,"HTTP/1.1 200 OK") == 0)//valid!
		{
			$intro = strpos($content,"<soap:");
			$xml = substr($content,$intro);
			$XML = new xml2Array();
			$xml_array = $XML->parse($xml);
				
			if (count($xml_array[0]['children']) == 2)
			{
				$body = $xml_array[0]['children'][1];
				$result = $body['children'][0]['children'][0];
				$reportid = $result['tagData'];
				return $reportid;
			}
			else
			{
				return "invalid child count: ".count($xml_array[0]['children']);
//				print_r($xml_array[0]);
			}
			
		}
		else
		{
			return "an error happened!";
		}		
	}
	
	function parseUrlResponse($content)
	{
		if (strpos($content,"HTTP/1.1 200 OK") == 0)//valid!
		{
			$intro = strpos($content,"<soap:");
			$xml = substr($content,$intro);
			$XML = new xml2Array();
			$xml_array = $XML->parse($xml);
				
			if (count($xml_array[0]['children']) == 2)
			{
				$body = $xml_array[0]['children'][1];
				$result = $body['children'][0]['children'][0];
				$url = $result['tagData'];
				return $url;
			}
			else
			{
				return "invalid child count: ".count($xml_array[0]['children']);
//				print_r($xml_array[0]);
			}
		}
		else
		{
			return "an error happened!";
		}		
	}
	

	function getReport($url)
	{
		$ch=curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response=curl_exec($ch);		
		curl_close($ch);					
		
		return $response;	
	}
	
	function sendLocation()
	{		
		$xml = locationService();
		$ch=curl_init(LOCATION);				
		$this_header = array("Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($xml));
		curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$response=curl_exec($ch);		
		curl_close($ch);					
		
		return $response;	
	}
	
	function send($xml)
	{		
		$ch=curl_init(REPORT);				
		$this_header = array("Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($xml));
		curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$response=curl_exec($ch);		
		curl_close($ch);					
		
		return $response;	
	}
	
	function getReportURL($reportid)
	{
		/*
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:namesp15="http://marketing.ews.yahooapis.com/V2" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
 <soap:Header>
  <wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">
   <wsse:UsernameToken soapenc:arrayType="xsd:string[2]" xsi:type="soapenc:Array">
    <wsse:Username xsi:type="xsd:string">'.USERNAME.'</wsse:Username>
    <wsse:Password xsi:type="xsd:string">'.CODE.'</wsse:Password>
   </wsse:UsernameToken>
  </wsse:Security>
  <license xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.LICENSEKEY.'</license>
  <masterAccountID xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.MCCID.'</masterAccountID>
 </soap:Header>
 <soap:Body>
  <getReportOutputUrl xmlns="http://marketing.ews.yahooapis.com/V2">
   <reportID xsi:type="xsd:int">'.$reportid.'</reportID>
   <fileFormat>
    <fileOutputType xsi:type="tns:FileOutputType">CSV</fileOutputType>
    <zipped xsi:type="xsd:boolean">false</zipped>
   </fileFormat>
  </getReportOutputUrl>
 </soap:Body>
</soap:Envelope>';
	*/
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:namesp15="http://marketing.ews.yahooapis.com/V2" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
'.getHeader().'
 <soap:Body>
  <getReportOutputUrl xmlns="http://marketing.ews.yahooapis.com/V2">
   <reportID xsi:type="xsd:int">'.$reportid.'</reportID>
   <fileFormat>
    <fileOutputType xsi:type="tns:FileOutputType">CSV</fileOutputType>
    <zipped xsi:type="xsd:boolean">false</zipped>
   </fileFormat>
  </getReportOutputUrl>
 </soap:Body>
</soap:Envelope>';
		return $xml;
	
	}
	
	function getHeader()
	{
		return '<soap:Header>
	<wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext">
		<wsse:UsernameToken soapenc:arrayType="xsd:string[2]" xsi:type="soapenc:Array">
			<wsse:Username xsi:type="xsd:string">'.USERNAME.'</wsse:Username>
			<wsse:Password xsi:type="xsd:string">'.CODE.'</wsse:Password>
		</wsse:UsernameToken>
	</wsse:Security>
	<license xmlns="http://marketing.ews.yahooapis.com/V1" xsi:type="xsd:string">'.LICENSEKEY.'</license>
	<masterAccountID xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.MCCID.'</masterAccountID>
</soap:Header>';
	}
	
	function formatDate($time)
	{		
		$result = date("Y-m-d\T00:00:00",$time).GMT_OFFSET;
		return $result;
		
	}	
	
	function locationService()
	{			
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:namesp1="http://marketing.ews.yahooapis.com/V2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.getHeader().'<soap:Body><getMasterAccountLocation xmlns="http://marketing.ews.yahooapis.com/V2" xsi:nil="true" /> </soap:Body></soap:Envelope>';
		
		return $xml;
	}
	
	function requestRangeReport($account,$accountid,$start,$end)
	{		
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:namesp1="http://marketing.ews.yahooapis.com/V2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.getHeader().'<soap:Body><addReportRequestForAccountID xmlns="http://marketing.ews.yahooapis.com/V2"><accountID xsi:type="xsd:string">'.$accountid.'</accountID><reportRequest>
<startDate xsi:type="tns:dateTime">'.formatDate($start).'</startDate>
<endDate xsi:type="tns:dateTime">'.formatDate($end).'</endDate>
<reportName xsi:type="xsd:string">'.$account.'</reportName>  <reportType xsi:type="tns:BasicReportType">AccountSummaryByDay</reportType></reportRequest> </addReportRequestForAccountID></soap:Body></soap:Envelope>';
		return $xml;
	}
	
	function requestReport($account,$accountid)
	{
		/*
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:namesp1="http://marketing.ews.yahooapis.com/V2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Header><wsse:Security xmlns:wsse="http://schemas.xmlsoap.org/ws/2002/04/secext"><wsse:UsernameToken soapenc:arrayType="xsd:string[2]" xsi:type="soapenc:Array"><wsse:Username xsi:type="xsd:string">'.USERNAME.'</wsse:Username><wsse:Password xsi:type="xsd:string">'.CODE.'</wsse:Password></wsse:UsernameToken>
  </wsse:Security><license xmlns="http://marketing.ews.yahooapis.com/V1" xsi:type="xsd:string">'.LICENSEKEY.'</license><masterAccountID xmlns="http://marketing.ews.yahooapis.com/V2" xsi:type="xsd:string">'.MCCID.'</masterAccountID></soap:Header> <soap:Body><addReportRequestForAccountID xmlns="http://marketing.ews.yahooapis.com/V2"><accountID xsi:type="xsd:string">'.$accountid.'</accountID><reportRequest>
<dateRange xsi:type="tns:DateRange">Yesterday</dateRange><reportName xsi:type="xsd:string">'.$account.'</reportName>  <reportType xsi:type="tns:BasicReportType">AccountSummaryByDay</reportType></reportRequest> </addReportRequestForAccountID></soap:Body></soap:Envelope>';
		*/
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<soap:Envelope xmlns:namesp1="http://marketing.ews.yahooapis.com/V2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" soap:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.getHeader().'<soap:Body><addReportRequestForAccountID xmlns="http://marketing.ews.yahooapis.com/V2"><accountID xsi:type="xsd:string">'.$accountid.'</accountID><reportRequest>
<dateRange xsi:type="tns:DateRange">Yesterday</dateRange><reportName xsi:type="xsd:string">'.$account.'</reportName>  <reportType xsi:type="tns:BasicReportType">AccountSummaryByDay</reportType></reportRequest> </addReportRequestForAccountID></soap:Body></soap:Envelope>';
		return $xml;
	}
	
?>