<?
/* 
************************FILE INFORMATION**********************
* File Name:  leads.php
**********************************************************************
* Description:  This file is for inputting leads into our local db
**********************************************************************
* Creation Date:  August 24, 2011 14:09 PM
**********************************************************************
* Modifications (Date/Who/Details):
**********************************************************************
*/

include "../inc_mysql.php";

function checkEmailValid($email) { 
	//-- Checks to see if there is an MX record for the domain and if we can make a socket connection to to SMTP port

	list($userName, $mailDomain) = split("@", $email);  //-- split email address into the username and domain. 
	
	if (!checkdnsrr($mailDomain, "MX")) { //-- this checks to see if there is an MX DNS record for the domain
		return false;
	}

	return true;
}


extract($_REQUEST);

/*
if ($_REQUEST['method'] == 'post') {
	extract($_POST);
	if ($rejectpage == "") {
		echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
		echo "<a href='leads.php?method=request'>use GET variables</a><Br/>\n";
	} else {
		header("Location: $rejectpage/?error=".urlencode($msg));
		exit();
	}
	//--mail("code@irelocation.com","Posted Auto Lead",print_r($_POST,true));
} else {
	extract($_REQUEST);
	if ($rejectpage == "") {
		echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
		echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
	} else {
		header("Location: $rejectpage/?error=".urlencode($msg));
		exit();
	}
	//--mail("code@irelocation.com","Requested Auto Lead",print_r($_REQUEST,true));
}

*/


$FirstName = trim(urldecode($FirstName));
$LastName = trim(urldecode($LastName));
$Email = trim(urldecode($Email));
$DayPhoneNumber = trim(urldecode($DayPhoneNumber));
$Address = trim(urldecode($Address));
$City = trim(urldecode($City));
$State = trim(urldecode($State));
$Zip = trim(urldecode($Zip));
$CreatedOn = trim(urldecode($CreatedON));
$IPAddress = trim(urldecode($IPAddress));
$LeadType = trim(urldecode($LeadType));
$ReferringDomain = trim(urldecode($ReferringDomain));
$MediaType = trim(urldecode($MediaType));

//-- clear any existing error messages
$msg = "";

//-- FIRST NAME
if (strlen($FirstName) == 0) {
	$msg .= "<strong>FirstName</strong>: A First Name is Required<Br/>\n";
} else {
	$FirstName = ucwords(strtolower($FirstName));
}

//-- LAST NAME
if (strlen($LastName) == 0) {
	$msg .= "<strong>LastName</strong>: A Last Name is Required<Br/>\n";
} else {
	$LastName = ucwords(strtolower($LastName));
}

//-- EMAIL
if (strlen($Email) == 0) {
	$msg .= "<strong>Email</strong>: An email address is Required<Br/>\n";
} 
/* This part isn't working with PHP5, need to figure out WTH why it's not working
else if( !checkEmailValid($Email) ) {
	$msg .= "<strong>Email</strong>: A valid email address is Required<Br/>\n";
}
*/

//-- PHONE
//-- remove dashes, period
$DayPhoneNumber = preg_replace('/[^0-9]/','',$DayPhoneNumber);

if (strlen($DayPhoneNumber)  != 10)
{
	$msg .= "<strong>DayPhoneNumber</strong>: A Phone Number is Required: 10 digits, only numbers<Br/>\n";
} else {
	//Phone Validation.	
	$p_area   = substr($DayPhoneNumber,0,3);
	$p_prefix = substr($DayPhoneNumber,3,3);	
	
	if ($p_area != 800 && $p_area != 866 && $p_area != 877 && $p_area != 888) {
		$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())
			$msg = "invalid phone: Please provide a valid phone number.";		
			#mail("george@irelocation.com","Bad NPA/NXX on Herbal Life lead","The following number had a area code and prefix that was not found in our database:\n\n$DayPhoneNumber");
	}
}

//-- CITY
if (strlen($City) == 0) {
	$msg .= "<strong>City</strong>: A City is Required<Br/>\n";
} else {
	$City = ucwords(strtolower($City));
}

//-- STATE
if (strlen($State) != 2) {
	$msg .= "<strong>State</strong>: A State is Required as a 2 letter code<Br/>\n";
}


//-- ALL THE OTHER CRUFT
if (strlen($Zip) == 0) {
		$msg .= "<strong>Zip</strong>: A Zip Code is required.<Br/>\n";
}

if (strlen($CreatedOn) == 0) {
		$msg .= "<strong>CreatedOn</strong>: CreatedOn is required.<Br/>\n";
}

if (strlen($IPAddress) == 0) {
		$msg .= "<strong>IPAddress</strong>: An IPAddress is required.<Br/>\n";
}

if (strlen($LeadType) == 0) {
		$msg .= "<strong>LeadType</strong>: A LeadType is required.<Br/>\n";
}

if (strlen($ReferringDomain) == 0) {
		$msg .= "<strong>ReferringDomain</strong>: A ReferringDomain is required.<Br/>\n";
}

if (strlen($MediaType) == 0) {
		$msg .= "<strong>MediaType</strong>: A MediaType is required.<Br/>\n";
}



//-- Check for dups
if (strlen($msg) == 0) {
	
	$check = "select * from irelocation.leads_herballife where (lower(Email) = '".strtolower($Email)."' or DayPhoneNumber = '$DayPhoneNumber') ";
	$rs = new mysql_recordset($check);
	
	if ($rs->fetch_array()) {
		$msg = "Duplicate: This lead has duplicated data.";
		$dupck = "yes";
	}
}

if (strlen($msg) == 0) {
#	define(TIMEZONE_OFFSET,18000);
	$now = gmdate("YmdHis", time() - TIMEZONE_OFFSET);

	//-- prevent sql injection attack
	$FirstName = mysql_real_escape_string($FirstName);
	$LastName = mysql_real_escape_string($LastName);
	$Email = mysql_real_escape_string($Email);
	$DayPhoneNumber = mysql_real_escape_string($DayPhoneNumber);
	$Address = mysql_real_escape_string($Address);
	$City = mysql_real_escape_string($City);
	$State = mysql_real_escape_string($State);
	$Zip = mysql_real_escape_string($Zip);
	$CreatedOn = mysql_real_escape_string($CreatedOn);
	$IPAddress = mysql_real_escape_string($IPAddress);
	$LeadType = mysql_real_escape_string($LeadType);
	$ReferringDomain = mysql_real_escape_string($ReferringDomain);
	$MediaType = mysql_real_escape_string($MediaType);
	
	$sql = "insert into irelocation.leads_herballife set received = '$now', FirstName = '$FirstName', LastName ='$LastName', Email = '$Email', DayPhoneNumber = '$DayPhoneNumber', Address = '$Address', City = '$City', State = '$State', Zip = '$Zip', CreatedOn = '$CreatedOn', IPAddress = '$IPAddress', LeadType = '$LeadType', ReferringDomain = '$ReferringDomain', MediaType = '$MediaType' ";



	$rs = new mysql_recordset($sql);		
	
	//-- Get Quote ID for LE Import
	$quote_id = $rs->last_insert_id();
	

	echo "Lead Accepted. Reference ID: ".$rs->last_insert_id();
	
	
} else {
	
	echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
	
	if ( $dupck == "yes" ) {
		echo "<Br/><strong>Duplicates:</strong>We Check for duplicate leads: if the following information is the same the lead is considered a duplicate.<Br/>";
		echo "<ul><li>Phone Number</li><li>Email Address</li></ul>";
	} 
}	
?>