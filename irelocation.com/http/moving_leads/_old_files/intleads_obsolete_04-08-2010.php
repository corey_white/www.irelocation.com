<?
	include "../inc_mysql.php";

	mail("david@irelocation.com","Ext Moving","");
	//default.
	$destination_country = "usa";
	$origin_country = "usa";
	$cat_id = 2;
	
	
	if ($_REQUEST['method'] == 'post')
	{
		extract($_POST);
		if ($rejectpage == "" && $thankyou == "")
		{
			echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
			echo "<a href='leads.php?method=request'>use REQUEST/GET variables</a><Br/>\n";
		}
	}
	else
	{
		extract($_REQUEST);
		if ($rejectpage == "" && $thankyou == "")
		{
			echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
			echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
		}
	}
	
	if ($destination_country != "usa")
	{
		$cat_id = 3;
	}
	
	$msg = "";
	if (strlen($fname) == 0)
	{
		$msg .= "<strong>fname</strong>: A First Name is Required<Br/>\n";
	}
	if (strlen($lname) == 0)
	{
		$msg .= "<strong>lname</strong>: A Last Name is Required<Br/>\n";
	}
	if (strlen($email) == 0)
	{
		$msg .= "<strong>email</strong>: An email address is Required<Br/>\n";
	}
	else if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))	
	{
		$msg .= "<strong>email</strong>: A valid email address is Required<Br/>\n";
	}
	
	if (strlen($phone)  != 10)
	{
		$msg .= "<strong>phone</strong>: A Phone Number is Required: 10 digits, only numbers<Br/>\n";
	}
	else
	{
		//Phone Validation.	
		$p_area   = substr($phone,0,3);
		$p_prefix = substr($phone,3,3);	
		
		$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())
			$msg = "invalid phone: Please provide a valid phone number.";		
	}
	
	if (strlen($origin_zip) == 5)
	{
		$sql = "select city,state from movingdirectory.zip_codes where zip = '$origin_zip' and length(city) > 3 limit 1";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$origin_city = $rs->myarray['city'];
			$origin_state = $rs->myarray['state'];
		}
		else
		{
			$sql = "select city,state from movingdirectory.zip_codes where zip = '$origin_zip' and limit 1";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
			{
				$origin_city = $rs->myarray['city'];
				$origin_state = $rs->myarray['state'];
			}
			else
				$msg .= "<strong>origin_zip</strong>: An Origin Zip Code or a valid City-State combination is required.<Br/>\n";
		}
	}
	else
	{
		if (strlen($origin_city) == 0)
		{
			$msg .= "<strong>origin_city</strong>: An Origin City is Required<Br/>\n";
		}
		if (strlen($origin_state) != 2)
		{
			$msg .= "<strong>origin_state</strong>: An Origin State is Required: 2 letter code<Br/>\n";
		}

		if (strlen($origin_state) == 2 && strlen($origin_city) > 2)//find zip code.
		{
			$sql = "select zip from movingdirectory.zip_codes where city like '$origin_city%' and state = '$origin_state' limit 1";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
				$origin_zip = $rs->myarray['zip'];
			else
				$msg .= "<strong>origin_zip</strong>: An Origin Zip Code or a valid City-State combination is required.<Br/>\n";			
		}
		else
			$msg .= "<strong>origin_zip</strong>: An Origin Zip Code or a valid City-State combination is required.<Br/>\n";			

	}

	if (strlen($destination_city) == 0)
	{
		$msg .= "<strong>destination_city</strong>: A Destination City is Required<Br/>\n";
	}
	if (strlen($destination_state) != 2 && $cat_id == 2)
	{
		$msg .= "<strong>destination_state</strong>: A Destination State is Required: 2 letter code (for US Leads only)<Br/>\n";
	}
	if (strlen($destination_zip) == 0 && $cat_id == 2)
	{
		$sql = "select * from zip_codes where city = '$destination_city' and state = '$destination_state'";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$destination_zip = $rs->myarray['zip'];
		}
		else
			$msg .= "<strong>destination_zip</strong>: A Destination Zip Code or a valid City-State combination (for US Leads only).<Br/>\n";
	}	
	
	if (strlen($est_move_date) != 10)
		$msg .= "<strong>est_move_date</strong>: An Estimated Move Date is required. YYYY-MM-DD <Br/>\n";		
	
	if (strlen($source) == 0)
	{
		$msg .= "<strong>source</strong>: Your source code is required.<Br/>\n";
	}

	if (!is_numeric($bedrooms))
	{		
		$msg .= "<strong>bedrooms</strong>: A Bedroom count is required (1-10).<Br/>\n";
	}
	
	if (strlen($type) == 0)
	{
		$msg .= "<strong>type</strong>: A Building Type is Required: house, condo, apartment, storage, other.<Br/>\n";
	}
	
	if ($contact != "phone" && $contact != "email")
	{
		$msg .= "<strong>contact</strong>: A Contact Method is Required. (phone/email)<Br/>\n";
	}

	//FEED INTO TOP MOVING FOR NOW...
	$source = "tm_$source";

	if (strlen($msg) == 0)
	{
		$two_days_ago = date("YmdHis",(time() - (2*24*60*60)));
		if ($destination_state != $origin_state)
		{
			$check = "select * from movingdirectory.quotes where (cat_id = 2 or cat_id = 3) and ".
					" origin_zip = '$origin_zip' and destination_city = '$destination_city' and ".
					" (email = '$email' or phone_home = '$phone') and received > '$two_days_ago' ";
			$rs = new mysql_recordset($check);
			if ($rs->fetch_array())
				$msg = "<strong>Duplicate</strong>: Please wait for the Auto Transporters to contact you.";
		}
		else
		{
			$check = "select * from movingdirectory.quotes_local where ".
					" origin_zip = '$origin_zip' and destination_zip = '$destination_zip' and ".
					" (email = '$email' or phone_home = '$phone') and received > '$two_days_ago' ";
			$rs = new mysql_recordset($check);
			if ($rs->fetch_array())
				$msg = "<strong>Duplicate</strong>: Please wait for the Auto Transporters to contact you.";
		}
	}
	
	$comments = "Type of Move: HouseHold Move\nType of Home: $type\nFurnished Rooms: $bedrooms bedrooms".
					"\n\nComments/Unique Items:  none.";
	
	if (strlen($msg) == 0)
	{
		if ($destination_state != $origin_state)
		{
			$sql = "insert into movingdirectory.quotes set cat_id = $cat_id, name = '$fname $lname', ".
					" email = '$email', phone_home = '$phone', est_move_date = '$est_move_date', ".
					" contact = '$contact', origin_city = '$origin_city', origin_state = '$origin_state', ".
					" origin_zip = '$origin_zip', destination_city = '$destination_city', ".
					" destination_state = '$destination_state', ".
					" source = '$source',  comments = '$comments', origin_country = '$origin_country', ".
					" destination_country = '$destination_country', ready_to_send = 1; ";
			$pre = "Q";
		}
		else
		{		
			$sql = "insert into movingdirectory.quotes_local set name = '$fname $lname', ".
					" email = '$email', phone_home = '$phone', est_move_date = '$est_move_date', ".
					" contact = '$contact', origin_city = '$origin_city', origin_state = '$origin_state', ".
					" origin_zip = '$origin_zip', destination_city = '$destination_city', ".
					" destination_state = '$destination_state', ".
					" destination_zip = '$destination_zip', source = '$source',  comments = '$comments'; ";
			$pre = "QL";					
		}		
		$rs = new mysql_recordset($sql);		
		
		if (isset($thankyou) && substr_count($thankyou,"http") > 0)
		{			
			header("Location: $thankyou?source=irelo&trackingid=".
			$pre.$rs->last_insert_id()."");
			exit();
		}
		else
		{
			echo "Lead Accepted: ".$pre.$rs->last_insert_id();
		}
		
	}
	else
	{
		if (isset($rejectpage))
		{
			header("Location: $rejectpage/?error=".urlencode($msg));
			exit();
		}
		else
		{
			echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
			
			echo "<br/><strong>Notes:</strong><Br/> Append a variable <strong>thankyou</strong> to have us redirect the users browser to your thankyou page.<br/> we will append tracking information for you to save:<Br/><Br/> &nbsp; &nbsp; &nbsp; [your_thank_you_page]?source=irelo&tracking_id=[unique_tracking_id]<br/>";
			echo "<br/>Append a variable <strong>rejectpage</strong> to have us redirect the user to a page if the lead fails submission<Br/>";
			echo "<Br/>By Default, all leads are US State to US State. Append <strong>destination_country=[country]</strong> to change this.<Br/>";
			echo "<Br/><strong>Duplicates:</strong>We Check for duplicate leads: if the following information is the same it is considered a duplicate.<Br/>";
			echo "<ul><li>(Phone Number or Email Address) AND</li><li>Origin Zip Code AND</li><li>Destination City AND</li>".
			"<li>Less than 2 Days old</li></ul>";
		}
	}	
?>