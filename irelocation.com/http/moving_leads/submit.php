<?
	include_once "../inc_mysql.php";
	
	session_start();
	define(EMAIL_REGEX,"^[_a-z0-9-]+(\.[\._a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
	define(NUMBER_REGEX,"[^0-9]");
	define(DUPE_DATE_RANGE,48*60*60);//two days

	function cleanText($in)
	{		
		$in = str_replace("'","\'",$in);
		return trim(ereg_replace("[^a-zA-Z '-\.]","",$in));
	}
		
	function injectionTest($in)
	{
		if (substr_count($in,"href=") > 0 || substr_count($in,"url=") > 0)
			return -1;
		else
		{
			$in = str_replace("'","\'",$in);
			$in = str_replace("insert","add",$in);
			$in = str_replace("update","change",$in);
			$in = str_replace("delete","remove",$in);
			$in = str_replace("create","make",$in);
			$in = str_replace("drop","release",$in);
			$in = str_replace("select","choose",$in);
		}
	}	


	function submit($postArray)
	{		
		if (!is_array($postArray))
		{
			return "nodata";			
		}
		$array_keys = array_keys($postArray);
		foreach($array_keys as $key)
		{
			$post_array[$key] = trim($postArray[$key]);
			$_SESSION[$key] = $postArray[$key];
		}		
		extract($postArray);
		
		$fname = cleanText($fname);
		$lname = cleanText($lname);
		$destination_city = cleanText($destination_city);				
		if (strlen($comments) > 0)
		{
			$comments = injectionTest($comments);
			if ($comments < 0)
				return "linkinjection";						
		}				
		
		if (!is_numeric($fname) && strlen($fname) > 2)
		{}//valid
		else
			return "fname";
		
		if (!is_numeric($lname) && strlen($lname) > 2)
		{}//valid
		else
			return "lname";
		
		if (eregi(EMAIL_REGEX, $email))
		{}//valid
		else
			return "email";
		
		if (strlen(ereg_replace(NUMBER_REGEX,"",$phone)) == 10)
		{
			$p_area   = substr($phone,0,3);
			$p_prefix = substr($phone,3,3);	
			
			$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array())
				return "phone_scrubbed";
			else
				$rs->close();
		}
		else
			return "phone";
			
		if (strlen(ereg_replace(NUMBER_REGEX,"",$origin_zip)) == 5)
		{
			$sql = "select * from movingdirectory.zip_codes where zip = '$origin_zip' ";
			$rs = new mysql_recordset($sql);
			
			if (!$rs->fetch_array())
				return "origin_zip_scrubbed";
			else
			{
				$origin_city = $rs->myarray['city'];
				$origin_state = $rs->myarray['state'];
				$rs->close();
			}
		}
		else
			return "origin_zip";
		
		if (strlen($destination_state) == 2)
		{}//valid
		else
			return "destination_state";

		if (strlen($destination_city) >= 3)
		{}//valid
		else
			return "destination_city";
		
		$check_city = $destination_city;
		do 
		{
			$sql = "select * from movingdirectory.zip_codes where state = '$destination_state' and city like '$check_city%'";
			$rs = new mysql_recordset($sql);
			$check_city = substr($check_city,strlen($check_city)-3);
		}
		while(!$rs->fetch_array() && strlen($check_city) > 0);

		if (strlen($rs->myarray['city']) > 0)//found an entry.
		{
			$destination_city = $rs->myarray['city'];
			$destination_zip = $rs->myarray['zip'];
			$rs->close();
		}
		else
			return "destination_scrubbed";

		if (strlen($move_day) == 2 && is_numeric($move_day))
		{}//valid
		else
			return "est_move_date";

		if (strlen($move_month) == 2 && is_numeric($move_month))
		{}//valid
		else
			return "est_move_date";

		if (strlen($move_year) == 4 && is_numeric($move_year))
		{}//valid
		else
			return "est_move_date";
		
		$est_move_date = $move_year."-".$move_month."-".$move_day;
		if (($est_move_date) > date("Y-m-d"))
		{}//valid
			else	 
			return "est_move_date";

		if (is_numeric($bedrooms) && strlen($bedrooms) > 0)//0,1,2,3,...
		{}//valid
		else	 
			return "bedrooms";
		
		if (strlen($type_home) > 0)//selected from drop down.
		{}//valid
		else	 
			return "type_home";

		if ($move_type == "household" || $move_type == "corporate")
		{}//valid
		else	 
			return "move_type";
	
		if ($contact == "phone" || $contact == "email")
		{}//valid
		else	 
			return "contact";

		$twodaysago = date("Ymdhis",time()-DUPE_DATE_RANGE);
		
		if (substr_count($source,"_") > 0)
		{
			$base_source = split("_",$source);
			$base_source = $base_source[0];	
		}
		else
			$base_source = $source;

		$sql = "select * from movingdirectory.quotes where source like '$base_source%' and (email = '$email' or phone_home = '$phone') ".
				" and origin_zip = '$origin_zip' and destination_city = '$destination_city' ".
				" and (received = '' or received > '$twodaysago') ";
		$local = str_replace("quotes","quotes_local",$sql);
		$long = $sql;

		$rs = new mysql_recordset($local);
		if ($rs->fetch_array())//duplicate in Vanlines table.
			return "duplicate";
		else
		{
			$rs->close();
			$rs = new mysql_recordset($long);
			if ($rs->fetch_array())//duplicate in main quotes table.
				return "duplicate";
		}		
		$rs->close();
		// if it gets to this point it is a valid quote.
		//build comments.
		
		/*
			Type of Move: Household Move
			Type of Home: house
			Furnished Rooms: 4 bedroom with attic,dining room,garage,kitchen,living room,office,play room
			Elevator: no
			Stairs: yes
			
			Comments/Unique Items: 
		*/
		$sql_comments = "Type of Move: $move_type move\n".
						"Type of Home: $type_home\n".
						"Furnished Rooms: $bedrooms bedrooms ".implode(",",$extra_rooms).
						"Elevator: ".(($elevator=="Y")?"yes":"no")."\n".
						"Stairs: ".(($stairs=="Y")?"yes":"no")."\n".
						"Comments/Unique Items: $comments";		
	
		$sql_insert = "insert into movingdirectory.quotes set comments = '$sql_comments', name = '".ucwords("$fname $lname")."',".
						" email = '$email', phone_home = '$phone', est_move_date = '$est_move_date', contact='$contact', ".
						" cat_id = 2, origin_city = '$origin_city', origin_state ='$origin_state', origin_zip = '$origin_zip',".
						" destination_city = '$destination_city', destination_state = '$destination_state', ready_to_send = '1';";
		$rs = new mysql_recordset($sql_insert);
		$rs->close();

		if (strlen($vyear) == 4 && strlen($vmake) > 0 && strlen($vmodel) > 0 && ($running == "R" || $running == "N") && strlen($vtype) > 0)
		{
			$sql_insert = "insert into marble.auto_quotes set fname = '$fname', lname = '$lname',".
						" email = '$email', phone = '$phone', est_move_date = '$move_month-$move_day-$move_year', contact='$contact', ".
						" origin_city = '$origin_city', origin_state ='$origin_state', origin_zip = '$origin_zip',".
						" destination_city = '$destination_city', destination_state = '$destination_state', destination_zip = '$destination_zip', ".
						" vmodel = '$vmodel', vmake = '$vmake', vyear='$vyear', running = '$running', vtype = '$vtype';";
			$rs = new marble($sql_insert);
			$rs->close();
		}
		else	
		{
			echo "year -$vyear- <br/> make = -$vmake- <br/> model = -$vmodel- <br/> running -$running- <Br/> type -$vtype- <Br/>";
		}
		return "valid";
	}

	echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	echo submit($_POST);

?>