<?php 

$QUOTE_ID = $last_insert_id;
$CAMPAIGN = "Moving";			//-- which campaign these leads are for
$POST_URL = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver";			//-- Posting URL

//-- Calculate No. Bedrooms and Poundage
switch ( $bedrooms ) {
    case "no bedroom":
        $number_bedrooms = "0";
        $poundage = "1500";
        break;

    case "one bedroom":
        $number_bedrooms = "1";
        $poundage = "4000";
        break;

    case "two bedrooms":
        $number_bedrooms = "2";
        $poundage = "6500";
        break;

    case "three bedrooms":
        $number_bedrooms = "3";
        $poundage = "9000";
        break;

    case "four bedrooms":
        $number_bedrooms = "4";
        $poundage = "12000";
        break;

    case "five bedrooms":
         $number_bedrooms = "5";
        $poundage = "Over 12000";
       break;

    case "six bedrooms":
         $number_bedrooms = "6";
        $poundage = "Over 12000";
       break;

    default:
        $number_bedrooms = "2";
        $poundage = "6500";
        break;
}

//-- fix the USA country issue
if ( $origin_country == "usa" ) {
    $origin_country = "US";
} 
if ( $destination_country == "usa" ) {
    $destination_country = "US";
} 

//-- Set IDs for Affiliates (added 1/7/11)
if ( preg_match("/atdepot/i", $source) ) { //-- IDs for ATDEPOT
	$VID = "1252";
	$AID = "2192";
} else {
	$VID = "1216";
	$AID = "2151";
}


//-- This is the GET VAR string - IMPORTANT:: The first 3 variables are NOT interchangable with other campaigns.  You need to change them for the campaign you are setting up.
$LEAD_BODY = 
"VID=$VID&".
"AID=$AID&".
"LID=370&".
"quote_id=$quote_id&".
"cat_id=$cat_id&".
"source=TMLQ:".urlencode($source)."&".
"referrer=".urlencode($referrer)."&".
"FirstName=".urlencode($fname)."&".
"LastName=".urlencode($lname)."&".
"email=".urlencode($email)."&".
"phone_home=".urlencode($phone)."&".
"contact=".urlencode($contact)."&".
"est_move_date=".urlencode($est_move_date)."&".
"number_bedrooms=".urlencode($number_bedrooms)."&".
"poundage=".urlencode($poundage)."&".
"origin_city=".urlencode($origin_city)."&".
"origin_state=".urlencode($origin_state)."&".
"origin_zip=".urlencode($origin_zip)."&".
"origin_country=".urlencode($origin_country)."&".
"destination_city=".urlencode($destination_city)."&".
"destination_state=".urlencode($destination_state)."&".
"destination_zip=".urlencode($destination_zip)."&".
"destination_country=".urlencode($destination_country)."&".
"scrub_notes=".urlencode($scrub_notes)."&".
"comments=".urlencode($moving_comments)."&".
"remote_ip=".urlencode($_SERVER['REMOTE_ADDR']);


function leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY) {	
	$ch=curl_init($POST_URL."?".$LEAD_BODY);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
	$response = curl_exec($ch);
	curl_close($ch);
	
	$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
	#echo "<br />-------------------------<br />LeadExec Repsonse <br />Url: ".$POST_URL."?".$LEAD_BODY." <br/>-------------------------<br/>Response: $response<br/>-------------------------<br />";
	
	if (substr_count(strtolower($response),"lead was accepted") == 1) { 
	
		#mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN","$content");

	} else {
		
		mail("rob@irelocation.com","LeadExec Repsonse Failure for TopMoving LeadQual Import - $CAMPAIGN","$content \n\n$response\n\nFrom page: www.irelocation.com/moving_leads/lead_exec_code_moving.php");
	
	}
}


leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY);
 ?>