<?

define (VERSION_ERROR,1999);

class MovingParser
{		
	function MovingParser()
	{
		$this->leadData = array();
		$this->table = "";
		$this->sql = "";
	}		
	
	function parseMovingDetails($data,&$lead)
	{
		
		foreach($data as $child)
		{
			if ($child['name'] != "ESTMOVEDATE")
			{
				if (DEBUG) echo TAB."MovingDetails->".strtolower($child['name'])." = ";
				if (DEBUG) echo $child['tagData']."<br/>";
				$lead->leadData[strtolower($child['name'])] = $child['tagData'];
			}
			else
			{
				$parts = $child['children'];
				foreach($parts as $part)
				{								
					$data['est_move_'.strtolower($part['name'])] = $part['tagData'];
					$lead->leadData['est_move_date'] = $data['est_move_year']."-".
													$data['est_move_month']."-".
													$data['est_move_day'];				
				}
				if (DEBUG) echo TAB."MovingDetails->est_move_date = ";												
				if (DEBUG) echo $lead->leadData['est_move_date']."<br/>";
			}
		}
	}
	
	function parseLocationInfo($locationArray,&$lead)
	{	
		if ($locationArray['name'] == "FROM")
			$pre = "origin_";
		else
			$pre = "destination_";
	
		foreach($locationArray['children'] as $kids)
		{
			if (DEBUG) echo TAB.TAB."Location->".$pre.strtolower($kids['name'])." = ";
			if (DEBUG) echo $kids['tagData']."<br/>";
			$lead->leadData[$pre.strtolower($kids['name'])] = $kids['tagData'];		
		}
	}	
	
	function parseMovingLead($leadArray)
	{	
		if (DEBUG) echo "MovingLead".$leadArray['name']."<br/>";
		$lead = new MovingLead();
		
		if ($leadArray['attrs']['TYPE'] != "moving")
		{
			echo "Moving Leads only!";
			return -1;
		}
			
		foreach($leadArray['children'] as $children)
		{				
			if (DEBUG) echo "Child Name: ".$children['name']."<br/>";
			switch($children['name'])
			{
				case "CONTACT":				
					foreach($children['children'] as $contactinfo)
					{
						if (DEBUG)
							echo TAB."Contact->".strtolower($contactinfo['name'])." = ".
								$contactinfo['tagData']."<br/>";
						$lead->leadData[strtolower($contactinfo['name'])] = $contactinfo['tagData'];				
					}
					break;
				case "MOVINGINFO":
					foreach($children['children'] as $location)				
					{
						if (DEBUG) echo TAB."MovingInfo->".$location['name']."<br/>";
						$this->parseLocationInfo($location,$lead);
					}
					break;
				case "MOVINGDETAILS":	
					$this->parseMovingDetails($children['children'],$lead);
					break;			
			}	
		}
			
		if ($lead->leadData['origin_country'] == "")
			$lead->leadData['origin_country'] = "US";
			
		if ($lead->leadData['destination_country'] == "")
			$lead->leadData['destination_country'] = "US";	
					
		if ($lead->leadData['destination_country'] != "US")
			$lead->leadData['cat_id'] = 3;
		else
			$lead->leadData['cat_id'] = 2;
			
	
		$lead->filterLeads();				
		
		return $lead;
	}
	
	function parseMovingXML($xmlarray)
	{	
		$leadObjects = array();
		if (DEBUG) echo "MovingXML: ".$xmlarray['name']."<br/>";
		
		foreach($xmlarray['children'] as $children)
		{
			$name = $children['name'];
			
			switch($name)				
			{
				case "VERSION":
					$version = $children['tagData'];
					break;			
				case "SOURCE":
					$source = $children['tagData'];
					break;
				case "LEAD":
					$leadObjects[] = $this->parseMovingLead($children);
					break;
				default:
					echo "Uknown:".$name."<br/>";
					break;
			}
		}
		
		if ($version != "1.0")
		{
			return VERSION_ERROR;				
		}
		
		foreach($leadObjects as $id => $lead)
		{
			$leadObjects[$id]->leadData['source'] = "aff_".$source;
			if ($leadObjects[$id]->errors == "")
			{
				unset($leadObjects[$id]->errors);
				$leadObjects[$id]->determineDestination();
				$leadObjects[$id]->getSql();
			}
		}
		
		return $leadObjects;	
	}
}

?>