<?
	include_once "../inc_mysql.php";
	//include "format.php";
	include ".lib.php";
	include "MovingLead.php";
	include "MovingParser.php";	
	include "xml2Array.php";
	
	header('Content-Type: text/xml');
	header('Content-Disposition: inline; filename=results.xml');
		
	$xml = trim($_POST['xml']);
	mail("david@irelocation.com","XML Moving Post",$xml);
	if (strlen(trim($xml)) == 0)
	{
		echo NODATA_RESPONSE;
		exit();
	}
	
	$objXML = new xml2Array();
	$arrOutput = $objXML->parse($xml);
	
	if ($arrOutput == XML_ERROR)
	{	
		echo XML_ERROR_RESPONSE;
		exit();
	}
	
 	$parser = new MovingParser();
	$parsed = $parser->parseMovingXML($arrOutput[0]); //print it out, or do whatever!
	if ($parsed == VERSION_ERROR)
	{
		echo VERSION_ERROR_RESPONSE;
		exit();
	}
	$leads = count($parsed);
	
	//echo "$leads parsed...<br/>";
	$valid = 0;
	
	if (count($parsed > 0))
	{
		$result = XML_HEADER."<result><responses>";
		
		foreach($parsed as $lead)
		{
			if($lead->testDuplicate())
				$lead->errors['duplicate'] = "this lead is a duplicate";

			if ($lead->errors == "")//nothing wrong!
			{				
				$sql = $lead->sql;
				$rs = new mysql_recordset($sql);
				$id = $rs->last_insert_id();
				$rs->close();
				$response = "<response>".
								"<success>".$lead->getPrefix().$id."</success>".
								"<email>".$lead->leadData['email']."</email>".
							"</response>";
				$result .= $response;
				$valid++;		
			}
			else//whoops!
			{
				$response = "<response><failure><reasons>";
				foreach($lead->errors as $f => $e)
				{
					$response .= "<reason>".
							"<field>$f</field>".
							"<description>$e</description>".
							"</reason>";
				}	
				$response .= "</reasons></failure>";
				$response .= "<email>".$lead->leadData['email']."</email>";
				$response .= "</response>";
				$result .= $response;
			}
		}
	}
 	$failed = $leads - $valid;
	$result .= "</responses>";
	$result .= "<summary>".
						"<processed>$leads</processed>".
						"<valid>$valid</valid>".
						"<failed>$failed</failed>".
				   "</summary>";
	$result .= "</result>";

	echo $result;
?>