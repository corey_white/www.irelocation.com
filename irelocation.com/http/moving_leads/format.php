<?



$XML = "<?xml version='1.0' encoding='iso-8859-1'?>
<leads>
	<version>1.0</version>
	<source>clearlink</source>
	<lead type='moving'>
		<contact>
			<fname>David</fname>
			<lname>Haveman</lname>
			<phone_home>4807857400</phone_home>
			<phone_work>4807638108</phone_work>
			<email>david@irelocation.com</email>
			<contact>email</contact>
		</contact>
		<movinginfo>
			<from>
				<city>Tempe</city>
				<state>AZ</state>
				<zip></zip>
				<country>US</country>
			</from>
			<to>
				<city>Wildwood</city>
				<state>NJ</state>
				<zip>08260</zip>
				<country>US</country>				
			</to>
		</movinginfo>
		<movingdetails>
			<type>house</type>
			<elevator>yes</elevator>
			<stairs>no</stairs>
			<bedrooms>4</bedrooms>
			<estmovedate>
				<month>10</month>
				<day>10</day>
				<year>2007</year>
			</estmovedate>
			<customer_comments>I'll like to move it, move it.</customer_comments>
		</movingdetails>
	</lead>
	<lead type='moving'>
		<contact>
			<fname>David</fname>
			<lname>Haveman</lname>
			<phone_home>4807857400</phone_home>
			<phone_work>4807638108</phone_work>
			<email>david@irelocation.com</email>
			<contact>email</contact>
		</contact>
		<movinginfo>
			<from>
				<city>Tempe</city>
				<state>AZ</state>
				<zip>85282</zip>
			</from>
			<to>
				<city>Wildwood</city>
				<state>NJ</state>
				<zip>08260</zip>
			</to>
		</movinginfo>
		<movingdetails>
			<type>house</type>
			<elevator>yes</elevator>
			<stairs>no</stairs>
			<bedrooms>4</bedrooms>
			<estmovedate>
				<month>10</month>
				<day>10</day>
				<year>2007</year>
			</estmovedate>
			<customer_comments>I like to move it, move it.</customer_comments>
		</movingdetails>
	</lead>
</leads>";

?>