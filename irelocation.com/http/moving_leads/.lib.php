<?

define(DEBUG,false);
define(TAB," &nbsp; &nbsp; ");

define(HOME_TYPES,"house,condo,office,appartment,storage");

define(DEFAULT_VANLINES_PERCENTAGE,20);
define(VANLINES,"movingdirectory.quotes_local");
define(TOPMOVING,"movingdirectory.quotes");
define(XML_HEADER,"<?xml version='1.0' encoding='iso-8859-1'?>");
define(NODATA_RESPONSE,
			XML_HEADER."<result><responses><result>".
			"<error>NO DATA GIVEN</error><summary><processed>0</processed>".
			"<valid>0</valid><failed>0</failed></summary></result>");

define(XML_ERROR_RESPONSE,XML_HEADER."<result><responses><result>".
		"<error>INVALID XML</error><summary><processed>0</processed>".
		"<valid>0</valid><failed>0</failed></summary></result>");

define(VERSION_ERROR_RESPONSE,XML_HEADER."<result><responses><result>".
		"<error>INVALID VERSION</error><summary><processed>0</processed>".
		"<valid>0</valid><failed>0</failed></summary></result>");

function validateEmail($email)
{
	$at = substr_count($email,"@");
	if ($at == 1)
	{
		list($u,$h) = split("@",$email,2);
		if (strlen($u) > 0 && strlen($h) > 5 && substr_count($h,".") > 0)
			return true;		
	}

	return false;
}
	

function validatePhone($p)
{
	$p = ereg_replace("[^0-9]","",$p);
	
	if (strlen($p) == 11 && substr($p,0,1) == 1)
		$p = substr($p,1);
	
	if (strlen($p) == 10)
	{
		$pa = substr($p,0,3);
		$pp = substr($p,3,3);
		
		$sql = "select country from movingdirectory.areacodes ".
				" where npa = $pa AND nxx = $pp ";
		
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
			return $p;
		else
			return "-1";
	}
	else
		return "-1";		
}

function validateLocation(&$c,&$s,&$z)
{
	$z = ereg_replace("[^0-9]","",$z);
	if (strlen($z) == 5)
	{
		$sql = " select city,state from movingdirectory.zip_codes ".
				" where zip = '".$z."' and length(city) > 3 ";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$c = $rs->myarray['city'];
			$s = $rs->myarray['state'];			
			return true;			
		}
		else//try city,state
		{ 
			$sql = " select city,state,zip from movingdirectory.zip_codes ".
				" where state = '".$s."' and city like '".$city."%' limit 1 ";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
			{
				$z = $rs->myarray['zip'];
				$c = $rs->myarray['city'];
				$s = $rs->myarray['state'];			
				return true;					
			}
			else//not found!
			{
				return false;
			}
		
		}
	}
	else//try city,state
	{ 
		$sql = " select city,state,zip from movingdirectory.zip_codes ".
			" where state = '".$s."' and city like '".$city."%' limit 1 ";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$z = $rs->myarray['zip'];
			$c = $rs->myarray['city'];
			$s = $rs->myarray['state'];			
			return true;					
		}
		else//not found!
		{
			return false;
		}
	
	}
}
?>