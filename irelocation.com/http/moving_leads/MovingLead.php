<?
class MovingLead
{
	var $leadData;
	var $sql;
	var $table;
	var $errors;
	
	function MovingLead($data="")
	{
		if ($data == "")
			$this->leadData = array();
		else
			$this->leadData = $data;
		$this->errors = array();
		$this->sql = "";
		$this->table = "";
	}	
	
	function getPrefix()
	{
		if ($this->table == VANLINES)
			return "QL";
		else
			return "Q";
	}
	
	function getSql($extra_sql="")
	{
		if ($this->table == VANLINES)
			unset($this->leadData['cat_id']);
	
		$sql = "insert into ".$this->table." set ";
		
		$keys = array_keys($this->leadData);
		foreach($keys as $k)
		{
			if (is_numeric($k))
				continue;
			
			$sql .= " $k = '".$this->leadData[$k]."', ";
		}

		$this->sql = substr($sql,0,-2);
		
		if ($extra_sql != "")
			$this->sql .= $extra_sql;
	}
		
	function getGhettoComments()
	{
		$comments = 
		"Type of Move: Household Move\n".
		"Type of Home: ".$this->leadData['type']."\n".
		"Furnished Rooms: ".$this->leadData['bedrooms']." bedrooms\n".
		"Elevator: ".$this->leadData['elevator']."\n".
		"Stairs: ".$this->leadData['stairs']."\n\n".	
		"Comments/Unique Items: ".$this->leadData['customer_comments']."";
		
		unset($this->leadData['customer_comments'],$this->leadData['stairs'],
			  $this->leadData['elevator'],$this->leadData['type'],
			  $this->leadData['bedrooms']);
			  		
		$this->leadData['comments'] = $comments;
	}	
		
	function testDuplicate()
	{
		$email = $this->leadData['email'];
		$phone_home = $this->leadData['phone_home'];
		$origin_zip = $this->leadData['origin_zip'];
		$destination_city = $this->leadData['destination_city'];
		
		$twodays = date("YmdHis",time()-24*60*60);
		
		$sql = "select * from ".$this->table." where (received = '' OR received = 0 OR ".
				" received > '".$twodays."') and ( email like '".$email."%' ".
				" or phone_home = '".$phone_home."' ) and origin_zip = '".$origin_zip."' ".
				" and destination_city = '".$destination_city."' ";
		if (DEBUG) echo "Dupe Code: $sql<br/>\n";			
		$rs = new mysql_recordset($sql);			
		return ($rs->fetch_array());
	}	
	
	function filterLeads()
	{		
		
		if (!validateEmail($this->leadData['email']))
			$this->errors['email'] = "Please enter a valid email address.";
	
		if (strlen($this->leadData['fname']) == 0)
			$this->errors['fname'] = "please enter a first name.";
		
		if (strlen($this->leadData['lname']) < 2)
			$this->errors['lname'] = "please enter a last name.";		
	
		$this->leadData['name'] = $this->leadData['fname']." ".$this->leadData['lname'];
		unset($this->leadData['fname'],$this->leadData['lname']);
	
		if (validatePhone($this->leadData['phone_work']) == "-1")
			$this->errors['phone_work'] = "Invalid Work Phone Number.";
			
		if (validatePhone($this->leadData['phone_home']) == "-1")
			$this->errors['phone_home'] = "Invalid Home Phone Number.";
		
		if(!validateLocation($this->leadData['origin_city'],
							$this->leadData['origin_state'],
							$this->leadData['origin_zip']))
			$this->errors['origin'] = "Invalid Origin Information";
		if ($this->leadData['cat_id'] == 2)
			if(!validateLocation($this->leadData['destination_city'],
							$this->leadData['destination_state'],
							$this->leadData['destination_zip']))
				$this->errors['origin'] = "Invalid Destination Information";
		
		$emd = str_replace("-","",$this->leadData['est_move_date']);
		$now = date("Ymd");
		if ($emd <= $now)
			$this->errors['est_move_date'] = "A Moving Date in the Future is required.";
		
		$this->leadData['bedrooms'] = ereg_replace("[^0-9]","",$this->leadData['bedrooms']);
		
		if (strlen($this->leadData['bedrooms']) == 0)
			$this->errors['bedrooms'] = "Please Specify the Number of Bedrooms!";
		
		if (substr_count(HOME_TYPES,$this->leadData['type']) != 1)
			$this->errors['type'] = "Please specify a building type: ".HOME_TYPES;
		
		if ($this->leadData['elevator'] == "")
			$this->leadData['elevator'] = "no";
			
		if ($this->leadData['stairs'] == "")
			$this->leadData['stairs'] = "no";
			
		if ($this->leadData['contact'] == "" ||
			($this->leadData['contact'] != "email" && $this->leadData['contact'] != "phone"))
			$this->leadData['contact'] = "email";
			
		
			
		
		$cc = $this->leadData['customer_comments'];
		if ($cc != "")
		{
			$cc = str_replace("'","\'",$cc);
			$cc = str_replace("insert","add",$cc);
			$cc = str_replace("update","change",$cc);
			$cc = str_replace("delete","remove",$cc);
			$cc = str_replace("select","choose",$cc);
			$cc = str_replace("create","make",$cc);
			$cc = str_replace("alter","change",$cc);
			
			$this->leadData['customer_comments'] = $cc;
		}
		
		$content = implode(" ",$this->leadData);	
		
		$bad = array("href=","<script","<?","onclick=","http://","https://");
		
		foreach($bad as $t)
			if (substr_count($content,$t) > 0)
				$this->errors['inject_'.$bad] = "Link/Code Injects will not be accepted - $t"	;						
		
		
			
		$this->determineDestination();					
			
	
		if(count($this->errors) == 0)
			$this->errors = "";	
			
		$this->getGhettoComments();			
	}
	
	function determineDestination()
	{
		$os = $this->leadData['origin_state'];
		$ds = $this->leadData['destination_state'];
		if ($os == $ds)
			$this->table = VANLINES;
		else
		{
			$sql = " select value from movingdirectory.irelo_variables ".
					" where name = 'vanlines_percentage' limit 1 ";
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
			{
				$perc = $rs->myarray['value'];		
			}
			else
			{
				mail("david@irelocation.com","VANLINES PERCENTAGE","not found!");
				$perc = DEFAULT_VANLINES_PERCENTAGE;
			}
			
			if (rand(0,100) < $perc)
				$this->table = VANLINES;
			else
				$this->table = TOPMOVING;
		}
	}
}
?>