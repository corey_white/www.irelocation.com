<?
	function add()
	{	
		$ch = curl_init($_POST['quote_page']);		
		if (!curl_exec($ch))
		{
			$error = "Your quote page does not work.";
			return $error;
		}
		
		$ch = curl_init($_POST['submit_page']);		
		if (!curl_exec($ch))
		{
			$error = "Your submission page does not work.";
			return $error;
		}
		
		$data_table = $_POST['data_table'];
		if (strlen($data_table) < 0 || substr_count($data_table,".") == 0)
		{
			$error = "Your data_table value is not correct.";
			return $error;
		}
		
		$equal_count = substr_count($_POST['post_fields'],"=");
		$parts = split("&",$_POST['post_fields']);
		if (count($parts) != $equal_count)
		{
			$error = "Your post fields are not formatted correctly.";
			return $error;
		}
		
		$sql = "select * from movingdirectory.site_fields where site_id = '".$_POST['site_id']."';";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$error = "That site code is taken.";
			return $error;
		}
		
		return "";		
	}
	
	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Add a site to Check</title>
</head>

<body>
	<form action="addsite.php" method="post">
			<table>
			<?
				if ($_POST['submit'] == "Add Site")
				{
					$result = add();
					if (strlen($result) > 0)
					{
						echo "<tr><td><font color='red'><strong>$result</strong></font></td></tr>";
					}
					else
					{
						//valid submission.
						//save it.
					
					}
				}
			
			?>
				<tr>
					<td>Quote Page:</td>
					<td><input type='text' name="quote_page" /></td>
				</tr>
				<tr>
					<td>Submission Page:</td>
					<td><input type='text' name="submit_page" /></td>
				</tr>
				<tr>
					<td>Database Table:</td>
					<td><input type='text' name="data_table" /></td>
				</tr>
				<tr>
					<td>Campaign:</td>
					<td>
					<select name="campaign">
						<option value="topauto">Top Auto</option>
						<option value="moving">Moving</option>
						<option value="auto-transport">Auto-Transport.us</option>
						<option value="security">Security</option>
						<option value="realestate">Real Estate</option>
					</select>
					</td>
				</tr>
				
				<tr>
					<td>Site ID:</td>
					<td><input type='text' name="site_id" /></td>
				</tr>
				<tr><td colspan="2" align="left">
					Post Fields:
				</td></tr>
				<Tr>
					<td colspan="2" align="left">
					<textarea name="post_fields" rows="25" cols="35"></textarea>					
					</td>
				</Tr>
				<tr><td colspan="2" align="center">
					<input type="submit" value="Add Site" name="submit" />
				</td></tr>
			</table>
			
	
	
	</form>
</body>
</html>
