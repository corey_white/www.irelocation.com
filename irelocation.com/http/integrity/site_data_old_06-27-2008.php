<?
	define(QUOTE_PAGE,0);
	define(SUBMIT_PAGE,1);
	define(POST_FIELDS,2);
	define(DATABASE_TABLE,3);
	define(CAMPAIGN,4);
	define(SITE_ID,5);
	define(LIVE,true);
	define(TEST_AUTO,true);
	define(MOVING_W_AUTO,"&type=Car&model=Rx8&make=Mazda&year=1999&condition=running");
	define(TEST_EMAIL_ADDRESS,"randomkeystrokes@gmail.com");
	define(PRIMARY_CELL,"code@irelocation.com");
	define(SECONDARY_CELL,"6024357508@vtext.com");

	function addSite($quote_page, $submit_page, $post_fields, $data_table, $campaign, $site_id)
	{
		$sql = "insert into movingdirectory.site_fields set quote_page = '$quote_page', submit_page = '$submit_page',".
				" post_fields = '$post_fields', data_table = '$data_table', campaign = '$campaign', site_id = '$site_id';";				
		$rs = new mysql_recordset($sql);		
	}
	
	function updateSite($id,$quote_page, $submit_page, $post_fields, $data_table, $campaign, $site_id)
	{
		$sql = "update movingdirectory.site_fields set quote_page = '$quote_page', submit_page = '$submit_page',".
				" post_fields = '$post_fields', data_table = '$data_table', campaign = '$campaign', site_id = '$site_id' ".
				" where id = $id; ";
		$rs = new mysql_recordset($sql);		
	}
	
	function getSomeSiteIDs($which)
	{
		if ($which == 1 || $which == 0)
		{
			$sql = "select distinct(site_id) 'site_id' from movingdirectory.site_fields ".
					" where active = 1 and id mod 2 = $which";
			echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			$results = array();
			while($rs->fetch_array())
				$results[] = $rs->myarray["site_id"];			
			
			return $results;
		}
		else return getSiteIDs();
	}

	function getSiteIDs()
	{
		$sql = "select distinct(site_id) 'site_id' from movingdirectory.site_fields where active = 1;";
		echo $sql."<br/>";
		$rs = new mysql_recordset($sql);
		$results = array();
		while($rs->fetch_array())
			$results[] = $rs->myarray["site_id"];			
		
		return $results;
	}
	
	function getFieldSQL($site)
	{
		$sql = "select * from movingdirectory.site_fields where site_id = '$site' and active";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			extract($rs->myarray);			
			$results = array();
			$results[QUOTE_PAGE] = $quote_page;
			$results[SUBMIT_PAGE] = $submit_page;
			$results[POST_FIELDS] = $post_fields;
			$results[DATABASE_TABLE] = $data_table;
			$results[CAMPAIGN] = $campaign;
			$results[SITE_ID] = $site;
		}	
		return $results;
	}
	
	/*
	
	Old Hardcoded way of doing it.
	
	function getFieldData($site)
	{
		$fields['tmc'] = array("http://topmovingcompanies.com/movingquote","http://topmovingcompanies.com/movingquote",
					"action=fin&name=Dave Lead&EMail=".TEST_EMAIL_ADDRESS."&areaday=480&regday=7857400&areaevening=480".
					"&regevening=7857400&cityfrom=Tempe&Statefrom=Arizona&zipfrom=85282&cityto=Wildwood&Stateto=NJ".
					"&zipto=08260&countryto=USA&Typemove=House&weight=no Bedroom&datemonth=01&dateday=31".
					"&dateyear=2050&comments=none&wants_auto=No&Submit=Get Quote!","movingdirectory.quotes",
					"moving","tmc");

		$fields['tm'] = array("http://topmoving.com/moving_quote.php","http://topmoving.com/moving_quote_submit.php",
					"cat_id=2&origin_country=usa&destination_country=usa&fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."".
					"&phone=4807857400&contact=email&origin_zip=85282&move_month=01&move_day=31&move_year=".(date("Y")+1).
					"&destination_city=Wildwood&destination_state=NJ&move_type=HouseHold Move&type_home=House".
					"&bedrooms=five bedrooms&moving_comments=testlead&submit=Submit Quote Request&source=dave",
					"movingdirectory.quotes","moving","tm");
	
		$fields['pm'] = array("http://www.promoving.com/household_mover_quote.php","http://www.promoving.com/moving_quote_submit.php",
					"cat_id=2&origin_country=usa&destination_country=usa&fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."".
					"&phone=4807857400&contact=email&origin_zip=85282&move_month=01&move_day=31&move_year=".(date("Y")+1).
					"&destination_city=Wildwood&destination_state=NJ&move_type=HouseHold Move&type_home=House".
					"&bedrooms=5 Bedroom&comments=testlead&submit=Request an Estimate ->&source=dave",
					"movingdirectory.quotes","moving","pm");
	
		$fields['me'] = array("http://www.movingease.com/household_mover_quote.php",
					"http://www.movingease.com/moving_quote_submit.php",
					"cat_id=2&origin_country=usa&destination_country=usa&fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."".
					"&phone=4807857400&contact=email&origin_zip=85282&move_month=01&move_day=31&move_year=".(date("Y")+1).
					"&destination_city=Wildwood&destination_state=NJ&move_type=HouseHold Move&type_home=House".
					"&bedrooms=5 Bedroom&comments=testlead&submit=Request an Estimate ->&source=dave",
					"movingdirectory.quotes","moving","me");
	
	
		$fields['mmc'] = array("http://www.movemycar.com/auto_transport_quote.php","http://www.movemycar.com/submit_quote.php",
					"fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&source=mmc_test&refer=www.test.com&keyword=test",
					"marble.auto_quotes","topauto","mmc");
					
		$fields['cs'] = array("http://www.carshipping.com/quote.php","http://www.carshipping.com/submit_quote.php",
					"customer_name=Dave Lead&customer_email=".TEST_EMAIL_ADDRESS."&customer_phone=4807857400&estimate_month=01".
					"&estimate_day=31&estimate_year=2050&from_zip=85282&to_city=Wildwood&to_state=NJ".
					"&vehicle_type=Car&vehicle_year=2005&vehicle_make=Mazda&vehicle_model=Rx8&condition=Running".
					"&moving=no&comments=none&submit=Submit Quote Request&source=cs_test&refer=www.test.com&keyword=test".
					"&quote_type=open","marble.auto_quotes","topauto","cs");			
					
		$fields['asc'] = array("http://autoshipping.com/quote_js.php?source=test",
					"http://autoshipping.com/quote_js.php?cmrefer=0&source=test",
					"firstname=Dave&lastname=Lead&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&origin_city=Tempe&origin_state=AZ".
					"&destination_city=Wildwood&destination_state=NJ&destination_zip=08260&contact=email".
					"&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no&comments=none".
					"&submit=Submit&submit_auto_quote=1","marble.auto_quotes","topauto","asc");
		
		
		$fields['tas'] = array("http://topautoshippers.com/","http://topautoshippers.com/submit_quote.php",
					"fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&source=mmc_test&refer=www.test.com&keyword=test",
					"marble.auto_quotes","topauto","tas");
		
		$fields['pas'] = array("http://proautoshipping.com/","http://proautoshipping.com/submit_quote.php",
					"fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&source=mmc_test&refer=www.test.com&keyword=test",
					"marble.auto_quotes","topauto","pas");
	
		$fields['csq'] = array("http://carshippingquote.com/","http://carshippingquote.com/submit_quote.php",
					"fname=Dave&lname=Lead&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&source=mmc_test&refer=www.test.com&keyword=test",
					"marble.auto_quotes","topauto","csq");
					
		
		$fields['atus'] = array("http://auto-transport.us/","http://auto-transport.us/submit_quote.php",
					"fname=Test&lname=Test&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&mysource=mmc_test&refer=www.test.com&keyword=test",
					"movingdirectory.quotes","auto-transport","atus");
		
		$fields['123'] = array("http://www.123Carmoving.com/","http://www.123Carmoving.com/submit_quote.php",
					"&fname=Test&lname=Test&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&move_month=01&move_day=31".
					"&move_year=2050&origin_zip=85282&destination_city=Wildwood&destination_state=NJ".
					"&contact=email&type=Car&year=2005&make=Mazda&model=Rx8&condition=running&moving=no".
					"&comments=none&submit=Submit Quote Request&source=mmc_test&refer=www.test.com&keyword=test",
					"movingdirectory.quotes","auto-transport","123");
		
		$fields['tsc_res'] = array("http://topsecuritycompanies.com/residential_security_quote.php",
					"http://topsecuritycompanies.com/submit.php",
					"form_location=local&quote_type=res&source=tsc_test&referrer=none&num_location=1".
					"&name1=Test&name2=Test&email=".TEST_EMAIL_ADDRESS."&phone1=4807857400&&phone2=4808932891".
					"&address=9830 S. 51st&city=Tempe&state_code=AZ&zip=85282&current_needs=new".
					"&sqr_footage=15&building_type=condo&own_rent=own&comments=none&submit_quote=submit",
					"irelocation.leads_security","security","tsc_res");
					
		$fields['tsc_com'] = array("http://topsecuritycompanies.com/business_security_quote.php",
					"http://topsecuritycompanies.com/submit.php",
					"form_location=local&quote_type=com&source=tsc_test&referrer=none&num_location=2".
					"&name1=Test Company&name2=Test Contact&email=".TEST_EMAIL_ADDRESS."&phone1=4807857400&&phone2=4808932891".
					"&address=9830 S. 51st&city=Tempe&state_code=AZ&zip=85282&current_needs=new".
					"&sqr_footage=0&building_type=retail&own_rent=own&comments=none&submit_quote=submit",
					"irelocation.leads_security","security","tsc_com");				
				
		$fields['irelo'] = array("http://www.irelocation.com/ppc2/","http://www.irelocation.com/ppc2/realtor_submit.php",
					"&fname=Test&lname=Test&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&phone2=4808932891".
					"&buying=yes&buying_state=AZ&buying_value=350&comments=none",
					"movecomp.leads_prd","realestate","irelo");
					
		$fields['irelo_wc'] = array("http://www.irelocation.com/ppc2/","http://www.irelocation.com/realtor_submit.php",
					"&fname=Test&lname=Test&email=".TEST_EMAIL_ADDRESS."&phone=4807857400&phone2=4808932891".
					"&buying=yes&buying_state=AZ&buying_value=350&comments=none",
					"movecomp.leads_prd","realestate","irelo_wc");	
	
		return $fields[$site];
	}	
	
	*/
	
	function testSite($site)
	{
		$check_email = $site.TEST_EMAIL_ADDRESS;
		//$site_data = getFieldData($site);
		$site_data = getFieldSQL($site);
		$site_data[POST_FIELDS] = str_replace(TEST_EMAIL_ADDRESS,
												$check_email,
													$site_data[POST_FIELDS]);
		echo "Checking for Email Address: $check_email<Br>\n";													
		$ch = curl_init($site_data[QUOTE_PAGE]);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$page = curl_exec($ch);
		curl_close($ch);
		
		//check how big the site is... might skip this one.
		if (substr_count($page,PARSE_ERROR) == 0)
		{
			echo "quote page works<Br/>\n";			
			echo "submitting lead to: <strong>".$site_data[SUBMIT_PAGE]."</strong><br/>\n";
			$ch = curl_init($site_data[SUBMIT_PAGE]);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
					
			if (TEST_AUTO && $site_data[CAMPAIGN] == 'moving' && $site != "tmc" && $site != "tmc_local" && $site != "genmoving" && substr_count($site,"local") == 0)
			{
				$test_auto = true;
				$site_data[POST_FIELDS] .= MOVING_W_AUTO;			
			}
										
			curl_setopt($ch, CURLOPT_POSTFIELDS, $site_data[POST_FIELDS]);
			
			$page = curl_exec($ch);			
			curl_close($ch);
			sleep(1);

			if (substr_count($page,PARSE_ERROR) == 0)//no errors.
			{
				echo "quote submission page works<Br/>\n";
				$table = $site_data[DATABASE_TABLE];
				
				
				if (substr_count($table,"marble") > 0)
				{
					$sql = "select * from $table where email = '$check_email';";
					echo $sql."<br/>";
					$rs = new mysql_recordset($sql);
				}
				else if (substr_count($table,"movecomp") > 0)
				{
					//movecompanion has differant fields.
					$sql = "select leadID as 'quote_id' From leads_prd where email = '$check_email';";
					$rs = new movecompanion($sql);
				}
				else
				{
					$sql = "select * from $table where email = '$check_email' ";
					if ($site_data[CAMPAIGN] == 'moving' && $table == "movingdirectory.quotes")
						$sql .= " and cat_id in (2,3) ";
					echo $sql."<br/>";
					$rs = new mysql_recordset($sql);
				}
				
				
				if ($rs->fetch_array())
				{
					echo "lead submitted.<Br/>\n";
					$q = $rs->myarray['quote_id'];
					$sql = " delete from $table where quote_id = $q or email = '$check_email'";
					if ($site_data[CAMPAIGN] == 'moving' && $table == "movingdirectory.quotes")
						$sql .= " and cat_id in (2,3) ";
					echo $sql."<br/>\n";
					if (substr_count($table,"marble") > 0)
						$rs = new mysql_recordset($sql);
					else if (substr_count($table,"movecomp") > 0)
					{
						//movecompanion has differant fields.
						$sql = "delete from $table where leadID = $q ";
						$rs = new movecompanion($sql);					
					}
					else
						$rs = new mysql_recordset($sql);
	
					echo "lead #$q was deleted.<Br/>\n";
					echo "291 Test Auto: ".(($test_auto)?"true":"false")."<br/>";
					if ($test_auto)
					{
						$sql2 = "select * from marble.auto_quotes where email = '$check_email';";
						
						$rs2 = new mysql_recordset($sql2);
						if ($rs2->fetch_array())
						{
							echo "auto lead submitted correctly.<Br/>\n";
							$q = $rs2->myarray['quote_id'];
							$sql2 = "delete from marble.auto_quotes where quote_id = $q; ";
							echo $sql2."<br/>\n";
							$rs = new mysql_recordset($sql2);
							echo "auto lead #$q deleted.<Br/>\n";
							$valid = true;
							echo "<font color='green'><strong>".ucwords($site)." works properly</strong></font><br/>\n";
						}
						else
						{
							echo "<strong><font color='red'>".
								"Auto Opt-In Lead Not Found".
								"</font></strong><Br/>\n";
							echo $page;
							if (LIVE)
								 mail(PRIMARY_CELL,"E",
								 		$site_data[SITE_ID]." is down",
										"From:".$site_data[SITE_ID]."@submit.com");
						}						
					}
					else
					{
					
						echo "<font color='green'><strong>".ucwords($site)." works properly</strong></font><br/>\n";
						//valid lead went in and just now got deleted.				
					}
				}		
				else
				{
					if ($site == "tmc" || $site == "tm" || $site == "pm" || $site == "me")//also check vanlines table...
					{
						$sql = "select * from movingdirectory.quotes_local where email = '$check_email';";
						$rs = new mysql_recordset($sql);
						if ($rs->fetch_array())
						{
							echo "lead submitted to quotes_local.<Br/>\n";
							$q = $rs->myarray['quote_id'];
							$sql = "delete from movingdirectory.quotes_local where quote_id = $q";							
							echo $sql."<br/>\n";
							echo "lead #$q was deleted.<Br/>\n";
							$rs = new mysql_recordset($sql);
							$valid = true;							
							echo "354 Test Auto: ".(($test_auto)?"true":"false")."<br/>";
							if ($test_auto)
							{
								$sql2 = "select * from marble.auto_quotes where email = '$check_email';";
								
								$rs2 = new mysql_recordset($sql2);
								if ($rs2->fetch_array())
								{
									echo "auto lead submitted correctly.<Br/>\n";
									$q = $rs2->myarray['quote_id'];
									$sql2 = "delete from marble.auto_quotes where quote_id = $q; ";
									echo $sql2."<br/>\n";
									$rs = new mysql_recordset($sql2);
									echo "auto lead #$q deleted.<Br/>\n";
									$valid = true;
									echo "<font color='green'><strong>".ucwords($site).
												" works properly</strong></font><br/>\n";
								}
								else
								{									
									$sql3 = "select * from movingdirectory.quotes where cat_id = 1 AND email = '$check_email';";
									$rs3 = new mysql_recordset($sql3);
									if ($rs3->fetch_array())
									{
										$q = $rs3->myarray['quote_id'];
										$sql3 = "delete from movingdirectory.quotes where quote_id = $q; ";
										echo $sql3."<br/>\n";
										$rs = new mysql_recordset($sql3);
										echo "auto lead #$q deleted.<Br/>\n";
										$valid = true;
										echo "<font color='green'><strong>".ucwords($site).
													" works properly</strong></font><br/>\n";
									}
									else
									{
										echo "<strong><font color='red'>Auto Opt-In Lead Not Found</font></strong><Br/>\n";
										echo $page;
										if (LIVE) mail(PRIMARY_CELL,"E",$site_data[SITE_ID]." is down",
												"From:".$site_data[SITE_ID]."@submit.com");
									}
								}
							}
							else
							{							
								echo "<font color='green'><strong>".ucwords($site)." works properly</strong></font><br/>\n";
								//valid lead went in and just now got deleted.				
							}
						}
						else
						{
							echo "<strong><font color='red'>Lead Not Found</font></strong><Br/>\n";
							echo $page;
							if (LIVE) mail(PRIMARY_CELL,"E",$site_data[SITE_ID]." is down",
								"From:".$site_data[SITE_ID]."@submit.com");
						}
					}
					else
					{
						echo "<strong><font color='red'>Lead Not Found</font></strong><Br/>\n";
						echo $page;
						if (LIVE) mail(PRIMARY_CELL,"E",$site_data[SITE_ID]." is down",
								"From:".$site_data[SITE_ID]."@submit.com");
					}
				}
			}
		}
		else
		{
			if(LIVE) mail(PRIMARY_CELL,"E",$site_data[SITE_ID]." is down",
						"From:".$site_data[SITE_ID]."@quote.com");
			echo "<strong><font color='red'>Error: quote page '".$site_data[QUOTE_PAGE]."' doesn't work!!</font></strong>";
		}
	}
	
?>