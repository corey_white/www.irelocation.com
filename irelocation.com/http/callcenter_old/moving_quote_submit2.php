<?
	include_once("settings.php");
	include("quote_class.php");
	include_once 'mysql.php';
	include "vanlines.php";

	function testDate($date)
	{		
		return ($date < date("Ymd",strtotime("+6 month")));	
	}

	function validatePhoneMore($phone)
	{
		$phone = substr($phone,3);
		$valid = true;
		
		for ($i = 0; $i < 10 && $valid; $i++)
		{
			$s = "".$i;
			if (substr_count($phone,$s) > 5)
			{
				$valid = false;
				break;
			}
		}
		return $valid;
	}

	function failInjection($type)
	{				
		header("Location: http://irelocation.com/callcenter/household_mover_quote.php/?msg=injection");
		exit();
	}
	
	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');

		$words = array("fuck"," ass","damn ","bitch", "shit ", " shit");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");		
	}		
	testInjection();//if it gets past this the input 'should' be ok.	

	//clean all fields
	$_POST["origin_zip"]=  ereg_replace("[^0-9]","",$_POST["origin_zip"]);
	$_POST["destination_city"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["destination_city"]))));
	$_POST["destination_state"]=eregi_replace('[[:punct:]]','',trim($_POST["destination_state"]));
	$_POST["fname"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["fname"]))));
	$_POST["fname"] = str_replace("'","",$_POST["fname"]);//no more apostrophes
	$_POST["lname"]=ucwords(strtolower(eregi_replace('[[:punct:]]','',trim($_POST["lname"]))));
	$_POST["lname"] = str_replace("'","",$_POST["lname"]);//no more apostrophes
	$_POST["email"]=strtolower(eregi_replace("'",'',trim($_POST["email"])));
	$_POST["phone"]= ereg_replace("[^0-9]","",$_POST["phone"]);

	$keys = array_keys($_POST);
	
	foreach($keys as $k)
	{
		$_POST[$k] = str_replace(array("<",">"),array("&lt;","&gt;"),trim($_POST[$k]));//kill all tags
		$_POST[$k] = str_replace(array("select","delete","insert","create","update","drop"),
								 array("choose","remove","add","make","change","fumble"),
								 $_POST[$k]);//kill all tags
		$_SESSION[$k] = $_POST[$k];
	}
		
	extract($_POST);


	$REDIRECT = "Location: /callcenter/household_mover_quote.php";	

	

	if (substr_count($_POST["comments"],"href") > 0 || substr_count($_POST["comments"],"[url]") > 0)
	{
		//mail("david@irelocation.com,mark@irelocation.com","PM PHP Check","LINK INJECTION:\n\n".print_r($_SESSION,true));
		$_SESSION["comments"] = "";
		header("$REDIRECT?msg=link_inject ");	
		exit();	
	}
	$_POST["comments"]=eregi_replace('[[:punct:]]','',trim($_POST["comments"]));				
	$_POST["comments"]=str_replace("'","",$_POST["comments"]);				
	
	if (strlen($fname) < 1)
	{
		header("$REDIRECT?msg=fname ");
		//mail("mark@irelocation.com","PM PHP Check","fname\n\n".print_r($_SESSION,true));
		exit();	
	}

	if (strlen($lname) < 2)
	{
		header("$REDIRECT?msg=lname ");
		//mail("mark@irelocation.com","PM PHP Check","lname\n\n".print_r($_SESSION,true));
		exit();	
	}
	
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email) )
	{
		header("$REDIRECT?msg=email ");
		//mail("mark@irelocation.com","PM PHP Check","email\n\n".print_r($_SESSION,true));
		exit();
	} 
	$long_domain = "\@[a-z0-9]\.[a-z]{2,4}";
	if (ereg($long_domain,$email) == 1)
	{
		header("$REDIRECT?msg=email ");
		//mail("mark@irelocation.com","PM PHP Check","email\n\n".print_r($_SESSION,true));
		exit();
	} 
	
	
	if (substr($phone,0,1) == 1)
		$phone = substr($phone,1);
		
	if (strlen($phone) != 10)
	{
		header("$REDIRECT?msg=phone ");
		//mail("mark@irelocation.com","PM PHP Check","phone\n\n".print_r($_SESSION,true));
		exit();
	}
	
	
	//Phone Validation.	
	$p_area   = substr($phone,0,3);
    $p_prefix = substr($phone,3,3);	
	
	$sql = "select country from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US' limit 1;";
	$rs = new movingdirectory($sql);
	
	if (!$rs->fetch_array())
	{
		header("$REDIRECT?msg=invalid_phone ");
		exit();
	}
	
	if (!validatePhoneMore($phone))
	{
		header("$REDIRECT?msg=invalid_phone ");	
		exit();
	}
	
	if (strlen($origin_zip) < 5 || !is_numeric($origin_zip))
	{
		header("$REDIRECT?msg=origin_zip ");
		//mail("mark@irelocation.com","PM PHP Check","origin_zip\n\n".print_r($_SESSION,true));
		exit();
	}
	
	$rs = new movingdirectory("select * from movingdirectory.zip_codes where zip = '$origin_zip' and length(city) > 3");
	if (!$rs->fetch_array())
	{
		header("$REDIRECT?msg=invalid_zip ");
		//mail("mark@irelocation.com","PM PHP Check","Origin Zip\n\n".print_r($_SESSION,true));
		exit();
	}
	else
	{
		$_SESSION["origin_state"] = $rs->myarray["state"];
		$_SESSION["origin_city"] = $rs->myarray["city"];
		$origin_state = $rs->myarray["state"];
		$origin_city = $rs->myarray["city"];		
	}
	
	$move_date = $move_year.$move_month.$move_day;
	
	$y = date("Y");
	$m = date("m");
	$d = date("d");
	$date = $y.$m.$d;
	
	if (intval($move_date) <= intval($date))
	{
		header("$REDIRECT?msg=month ");
		//mail("mark@irelocation.com","PM PHP Check","move_date\n\n".print_r($_SESSION,true));
		exit();
	}
	
	if (strlen($destination_city) < 3 || is_numeric($destination_city))
	{
		header("$REDIRECT?msg=destination_city ");
		//mail("mark@irelocation.com","PM PHP Check","dest city\n\n".print_r($_SESSION,true));
		exit();
	}
	else if (is_numeric($destination_city) && strlen($destination_city) == 5)
	{
		$sql = "select city,state from movingdirectory.zip_codes where zip = '$destination_city' limit 1";
		$rs = new movingdirectory($sql);
		if ($rs->fetch_array())
		{
			$destination_zip = $destination_city;
			$_SESSION["destination_city"] = $rs->myarray["city"];
			$destination_city = $rs->myarray["city"];
			$_SESSION["destination_state"] = $rs->myarray["state"];
			$destination_state = $rs->myarray["state"];
			
		}
		else
		{
			$_SESSION["destination_city"] == "";
			header("$REDIRECT?msg=invalid_zip ");
			//mail("mark@irelocation.com,david@irelocation.com","PM PHP Check","bad zip as destination city\n\n".print_r($_SESSION,true));
			exit();
		}
	}
	
	
	if ($destination_state == "XX")
	{
		$rs = new movingdirectory("select state from movingdirectory.zip_codes where city = '$destination_city' limit 1");
		if (!$rs->fetch_array())
		{
			header("$REDIRECT?msg=destination_state ");
			//mail("mark@irelocation.com","PM PHP Check","dest state\n\n".print_r($_SESSION,true));
			exit();
		}
		else
		{
			$destination_state = $rs->myarray["state"];
			$_POST["destination_state"] = $destination_state;
			$_SESSION["destination_state"] = $destination_state;
		}
	}
	else if (strlen($destination_zip) != 5)//if we have dest zip we're good.
	{
		$rs = new movingdirectory("select zip from movingdirectory.zip_codes where city = '$destination_city' ".
			"&& state = '$destination_state' limit 1");
		if (!$rs->fetch_array())
		{
			header("$REDIRECT?msg=destination_city_state ");			
			exit();
		}	
		else
			$destination_zip = $rs->myarray['zip'];
	}
	if (strlen($destination_zip) != 5)
	{
		$city = $_SESSION["destination_city"];
		$state = $_POST["destination_state"];
		$rs = new movingdirectory("select zip from movingdirectory.zip_codes where city like '$city%' and state = '$state' limit 1");
		if ($rs->fetch_array())
		{
			$_SESSION["destination_zip"] = $rs->myarray["zip"];
			$destination_zip = $rs->myarray["zip"];
		}
		else
		{
			$city = substr($city,0,-3);
			while (strlen($city) > 3)
			{			
				$rs = new movingdirectory("select zip from movingdirectory.zip_codes where city like '$city%' and state = '$state' limit 1");
				if ($rs->fetch_array())
				{
					$_SESSION["destination_zip"] = $rs->myarray["zip"];
					$destination_zip = $rs->myarray["zip"];
					break;
				}
				$city = substr($city,0,-3);
			}
		}
	}
	if ($bedrooms == "XX")
	{
		header("$REDIRECT?msg=bedrooms ");
		//mail("mark@irelocation.com","PM PHP Check","bedrooms\n\n".print_r($_SESSION,true));
		exit();
	}	
	
	if ($mysource == '')
	{
		$mysource = 'pm_callcenter';
		//mail("dave@irelocation.com","Blank Source from PM","Name:$fname $lname\nServer Data:".print_r($_SERVER,true));
	}
	
	$move_date = $move_year.'-'.$move_month.'-'.$move_day;
	$ts = time() - (24 * 3600);//subtract a day.
	$time = date("YmdHis",$ts);	
	
	//check quotes table first.	
	$table = "movingdirectory.quotes";
	$cat_id_test = "(cat_id = 2 or cat_id = 3) and ";
	
	
	$sql = "select * from $table where $cat_id_test (received = '' or received > '$time') ".
	"and (email = '$email' or phone_home = '$phone') and origin_zip = '$origin_zip' and ".
	"destination_city = '$destination_city' and source like 'pm%' limit 1;";	
	//mail("david@irelocation.com","TM Dupe SQL",$sql);
	$rs = new movingdirectory($sql);
	
	if($rs->fetch_array())
	{		
		header("$REDIRECT?msg=duplicate ");
		exit();
	}
	else
	{
		$sql = str_replace($cat_id_test,"",$sql);
		$sql = str_replace($table,"movingdirectory.quotes_local",$sql);
		//mail("david@irelocation.com","TM Dupe SQL",$sql);
		$rs = new movingdirectory($sql);
		//check in local table too.
		if($rs->fetch_array())
		{		
			header("$REDIRECT?msg=duplicate ");
			exit();
		}
	}
	

	$months = array(0,31,28,31,30,	31,30,31,31,	30,31,30,31);
	$days = $months[intval($move_month)];
	if ($move_year % 4 == 0 && intval($move_month) == 2) $days++;//leap year.
	if ($days < $move_day)
	{		
		header("$REDIRECT?msg=est_move_date ");
		exit();
	}

	
	if (substr_count($mysource,"boxdev") > 0)
		$mysource = "aff_boxdev";


if ($mysource == '')
{
	$mysource = 'pm_callcenter';
	//mail("dave@irelocation.com","Blank Source from PM","Name:$fname $lname;\nServer Data:".print_r($_SERVER,true));
}
//65% of the long distance leads go to TopMoving Companies

//$long_distance = ($origin_state != $destination_state);
$more = "CA,NY";
$less = "MI,PA,MN,IA,KS,NE,OK,LA";
//$offset = VANLINES_PERCENTAGE;
/*
if (substr_count($more,$origin_state) == 1)
	$offset -= 15;
else if (substr_count($less,$origin_state) == 1)
	$offset += 15;
	
$offset = min(max($offset,0),100);
*/
	$valid_vanlines = testDate($move_year.$move_month.$move_day);		
	$long_distance = ($origin_state != $destination_state);
	
	/*
	$offset = VANLINES_PERCENTAGE;
	$valid = true;
	*/
	if ($long_distance)
		$q=new moving_quote();
	else
	{
		if ($valid_vanlines)
			$q = new local_moving_quote();
		else
			fail(local_time);
	}	

$q->name=$fname." ".$lname;
$q->email=$email;
$q->phone_home=$phone;
$q->contact=$contact;

	$est_move_date = "$move_year-$move_month-$move_day";

$q->est_move_date= $est_move_date;
$q->origin_zip=$origin_zip;
$q->origin_country=$origin_country;
$q->destination_city=$destination_city;
$q->destination_state=$destination_state;
$q->destination_country=$destination_country;
$q->destination_zip = $destination_zip;
$q->cat_id=$cat_id;
$q->move_type=$move_type;
$q->type_home=$type_home;
$q->bedrooms=$bedrooms;
$q->attic=$attic;
$q->living=$living;
$q->basement=$basement;
$q->office=$office;
$q->den=$den;
$q->patio=$patio;
$q->dining=$dining;
$q->garage=$garage;
$q->play=$play;
$q->kitchen=$kitchen;
$q->shed=$shed;
$q->elevator=$elevator;
$q->stairs=$stairs;
$q->comments=$comments;
$q->source=$mysource;
$q->keyword=$mykeyword;


$result=$q->save_quote();
$quote_id = $q->quote_id;


	
	$yesterday = date("YmdHis",time()-24*60*60);
	$now = date("YmdHis");
	if (strlen($year) >= 2 && strlen($make) > 2 && strlen($model) > 2)
	{
		if (AUTO_LEADS_TO_ATUS)
		{
			$campaign = "'atus'";
			$source = "atus_pm";
		}
		else
		{
			$campaign = "'auto'";
			$source = "asc_pm";
		}
			
		$ip = $_SERVER['REMOTE_ADDR'];
		$sql = "select * From marble.auto_quotes where (email = '$email' or phone = '$phone') and origin_zip = $origin_zip and destination_zip = $destination_zip ".
		" and received > $yesterday limit 1";
		$rs = new movingdirectory($sql);
		if (!$rs->fetch_array())
		{		
			$sql = "insert into marble.auto_quotes set received = '$now', ".
					" source = '$source', remote_ip = '$ip', fname = '$fname', ".
					" lname = '$lname', email = '$email', ".
					" phone = '$phone', origin_city = '$origin_city',  ".
					" origin_state = '$origin_state', origin_zip = '$origin_zip',".
					" destination_city = '$destination_city', destination_zip = ".
					" '$destination_zip', destination_state = '$destination_state',".
					" est_move_date = '$move_month-$move_day-$move_year', ".
					" vyear = '$year', vmodel = '$model', vmake = '$make', ".
					" vtype = '$type', running = '$condition', contact = '$contact',".
					" campaign = $campaign";
			$rs = new movingdirectory($sql);
					//echo "<!--- $sql --->";
			
			$autoquote = true;
		}	
	}
	if ($storage == 1 && $estimatetime != "Select...")
	{
		$array = array();
		$array[name] = "$fname $lname";
		$array[comments] = $moving_comments;
		$array[est_move_date] = $est_move_date;
		$array[quote_id] = $quote_id;
		$array[phone_home] = $phone;
		$array[phone_work] = '';
		$array[origin_city] = $origin_city;
		$array[origin_state] = $origin_state;
		$array[origin_zip] = $origin_zip;
		$array[email] = $email;
		$array[storagetime] = $estimatetime;
		$array[source] = $mysource;
		include "storageleads.php";
		sendStorageLead($array);
	}
	if ($clean == 1 && $roomcleaning != "Select...")
	{
		$subject = "Carpet Cleaning Lead from www.ProMoving.com";
		$to = "irelocation@searscarpet.com, mark@irelocation.com";
		$headers = "From: noreply@promoving.com\r\n";
		$msg = "Lead from www.ProMoving.com\n";
		$msg .= "Name: $fname $lname\n";
		$msg .= "Phone: $phone\n";
		$msg .= "Email:  $email\n";
		$msg .= "Number of rooms to clean: $roomcleaning\n";
		$msg .= "City: $origin_city\n";
		$msg .= "State: $origin_state\n";
		$msg .= "Zip: $origin_zip\n";
		
		mail($to,$subject,$msg,$headers);
	
		
	}

if(isset($return_url))
{
	
  header("location: $return_url");
  exit;
}

if($result==true)
{
  if($realtor==1)
  {
  	$msg = "Thank you for submitting a moving quote!";
    include "realtor.php";
	exit;
  }
  else
  {
	$msg = "?name1=$fname&name2=$lname&email=$email&phone1=$phone&city=$destination_city&state_code=$destination_state";
	if ($autoquote)
		$msg .= "&auto";
		
	session_destroy();	
    header("location: thankyou.php".$msg);
  }
}
else
{
  $msg=$q->msg;
  $tempref=explode("promoving.com/",$_SERVER['HTTP_REFERER']);
  $refpage=explode("?",$tempref[1]);
  header("location: $refpage[0]?fname=$fname&lname=$lname&email=$email&phone=$phone&contact=$contact&move_year=$move_year&move_month=$move_month&move_day=$move_day&origin_zip=$origin_zip&destination_city=$destination_city&destination_state=$destination_state&destination_country=$destination_country&move_type=$move_type&type_home=$type_home&bedrooms=$bedrooms&attic=$attic&living=$living&basement=$basement&office=$office&den=$den&patio=$patio&dining=$dining&garage=$garage&play=$play&kitchen=$kitchen&shed=$shed&elevator=$elevator&stairs=$stairs&comments=$comments&msg=$msg");
}  

?>