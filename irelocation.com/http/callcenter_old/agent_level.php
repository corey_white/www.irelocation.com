<?
#
#  This file is used to read the users browsers version and tell
#  whether they support all CSS or not.
#

$http_user=strtolower($HTTP_USER_AGENT);
$agent_level="high";

if(substr_count($http_user,"mozilla/4")>0)
{
  $temp=explode("/",$http_user);
  $temp2=explode(" ",$temp[1]);
  $version=ereg_replace('[_a-zA-Z]','',$temp2[0]);
  if($version>4 && $version<5)
  {
    $agent_level="low";
  }
}

if(substr_count($http_user,"mozilla/1")>0)
{
  $agent_level="low";
}

if(substr_count($http_user,"mozilla/2")>0)
{
  $agent_level="low";
}

if(substr_count($http_user,"mozilla/3")>0)
{
  $agent_level="low";
}

if(substr_count($http_user,"webtv")>0)
{
  $agent_level="low";
}

?>
