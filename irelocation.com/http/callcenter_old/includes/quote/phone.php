<script>
	function handleKeyStroke(tf1,len,tf2,e)
	{		
		if(e.keyCode != 13)
		{
			var f2 = document.getElementById(tf2);
			if (tf1.value.length == len)
			{
				if (f2 != null)
					f2.focus();
				else
					populate_real();			
				return false;
			}
			return false;
		}
	}
	
	function populate_real()
	{
		var p1 = document.getElementById("p1");
		var p2 = document.getElementById("p2");
		var p3 = document.getElementById("p3");
		var real_phone = document.getElementById("real_phone");
		real_phone.value = p1.value + p2.value + p3.value;

		if (real_phone.value.length != 10)
			alert("Invalid Phone Format.");
	}
	
</script>
<form name="phone">
(<input type="text" id="p1" name="p1" maxlength="3" size="3" onKeyUp="handleKeyStroke(this,3,'p2',event);" />)
<input type="text" id="p2" name="p2" maxlength="3" size="3" onKeyUp="handleKeyStroke(this,3,'p3',event);"/> -
<input type="text" id="p3" name="p3" maxlength="4" size="4" onKeyUp="handleKeyStroke(this,4,null,event);"/>
<input type="hidden" name="phone" value="" id="real_phone" />
</form>