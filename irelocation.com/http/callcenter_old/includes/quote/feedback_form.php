<!--- Start residential_security_full_quote.php --->


<form name="feedback" method="post" action='submit_feedback.php' >
	<? if ($mysource == "" || $mysource == "tac")
	{
		if (strlen($_SESSION['source']) > 0)
			$mysource = $_SESSION['source'];
		if ($mysource == "")
			$mysource = $_REQUEST['source'];

		if (substr_count($mysource,"tac") == 0)
			$mysource = "tac_".$mysource;
	}
	?>
  <table border="0" cellspacing="1" width="520" background="<?php echo ON_BG; ?>">
   
   <tr align="left"> 
      <td colspan="4" class="mainsecuritytopic">
	  	<font size="+1">Rate your Security Company</font>
	  </td>
    </tr>
	 <tr align="left"> 
      <td colspan="4" class="mainsecuritytext">Thanks for using TopAlarmCompanies.com! Tell us how the security company you chose performed. did they meet your expectations? were you pleased with the services they provided? let us know!</td>
    </tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext">Your Name:</td>
	  <td width="35%" align="left">
	  	<input name="name" type="text" size="20" class="movingquotetext" maxlength="20"
			value="<?= $_SESSION['name'] ?>" />
		</td>
	  <td width="20%" align="right" class="mainsecuritytext">&nbsp;</td>
	  <td width="25%" align="left">&nbsp;</td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext">Email:</td>
	  <td width="35%" align="left">
	  	<input name="email" type="text" size="35" class="movingquotetext" maxlength="20" 
			value="<?= $_SESSION['email'] ?>"  />
		</td>
	  <td width="20%" colspan="2" align="right" class="mainsecuritylegal">
	  	* please enter the email you used to receive your quotes. This will not be public.
	  </td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext">Select your state:</td>
	  <td width="35%" align="left">
	  		<select name="state" class="movingquotetext" >
				<option value='xx'>- Select State -</option>
				<option value="OH">OH</option>
			</select>
		</td>
	  <td width="20%" align="right" class="mainsecuritytext">&nbsp;</td>
	  <td width="25%" align="left">&nbsp;</td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext">Who installed your System:</td>
	  <td width="35%" align="left">
	  	<select name="company" class="movingquotetext" >
			<option value='xx'>- Select Security Company -</option>
			<option value="1519">Brink's Home Security</option>
			<option value="1520">Protect America</option>
			<option value="1552">Gaylord ADT Dealer</option>
			<option value="1565">Monitronics</option>
		</select>
		</td>
	  <td width="20%" align="right" class="mainsecuritytext">&nbsp;</td>
	  <td width="25%" align="left">&nbsp;</td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext">Overall Rating:</td>
	  <td width="35%" align="left">
	  	<select name="rating" class="movingquotetext" >
			<option value="10">10 - Excellent</option>
			<option>9</option>
			<option>8</option>
			<option>7</option>
			<option>6</option>
			<option>5</option>
			<option>4</option>
			<option>3</option>
			<option>2</option>
			<option>1 - Poor</option>			
		</select>
		</td>
	  <td width="20%" align="right" class="mainsecuritytext">&nbsp;</td>
	  <td width="25%" align="left">&nbsp;</td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext" valign="top" colspan="4">
	  	&nbsp;
	  </td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext" valign="top" colspan="4">
	  	How did the security companies that contacted you compare?
	  </td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext" valign="top" colspan="4">
	  	<textarea name='comments' rows="4" cols="50" class="movingquotetext">
		<?= $_SESSION['comments'] ?></textarea>
	  </td>
	</tr>
	<tr align="left">  
	  <td width="20%" align="left" class="mainsecuritytext" valign="top" colspan="4">
	  	Additional Comments:
	  </td>
	</tr>
	<tr align="left">  
	  <td width="35%" align="left" colspan="4">
	  	<textarea name='comments' rows="5" cols="50" class="movingquotetext">
		<?= $_SESSION['comments'] ?></textarea>
		</td>
	</tr>
	
    <tr align="left"> 
      <td>&nbsp;</td>
      <td colspan="3"> <input type="submit" value="Submit" name="submit_quote" class="movingquotetext"></td>
    </tr>
  </table>
</form>
<!--- end residential_security_full_quote.php --->
