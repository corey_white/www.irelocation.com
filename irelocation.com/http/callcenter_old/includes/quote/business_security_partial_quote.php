<!-- Start business_security_partial_quote.php -->
<tr> 
		<td>&nbsp;</td>
      <td colspan="3" class="mainsecuritytopic" align="left">
	  	Select services below:
	</td>
    </tr>

    <tr align="left"> 
      <td class="mainsecuritytext"  align="left">Current Needs:</td>
      <td class="mainsecuritytext"  align="left"> 
	  <select size="1" name="current_needs" tabindex="1"  <?php if (substr_count($msg,"current_needs") > 0) echo " class='errortext' "; else echo " class='mainsecuritytext' "; ?>>
		  <option value="XX"> - Select - </option>
		  <option <?php if ($_SESSION["current_needs"] == "new") echo " selected "; ?> value="new">New Security System</option>
		  <option <?php if ($_SESSION["current_needs"] == "upgrade") echo " selected "; ?> value="upgrade">Upgrade Current Service</option>
		  <option <?php if ($_SESSION["current_needs"] == "replace") echo " selected "; ?> value="replace">Replace Current Service</option>
      </select>
	  </td>
	   <td class="mainsecuritytext"  align="right">Square Footage:</td>
      <td  align="left"> 
	  	<select size="1" name="sqr_footage" tabindex="1"  class="mainsecuritytext">
		  <option <?php if ($_SESSION["sqr_footage"] == "0") echo " selected "; ?> value="0">0 - 2,499 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "25") echo " selected "; ?> value="25">2,500 - 4,999 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "50") echo " selected "; ?> value="50">5,000 - 9,999 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "100") echo " selected "; ?> value="100">10,000 - 49,999 ft</option>		  
		  <option <?php if ($_SESSION["sqr_footage"] == "500") echo " selected "; ?> value="500">50,000+ ft</option>
        </select>
	  </td>
      
    </tr>
    <tr> 
      <td class="mainsecuritytext"  align="left">Type of Business:</td>
      <td  align="left"> 
	  	<select size="1" name="building_type" tabindex="1"  class="mainsecuritytext">		
		  <option value="XX"> - Select - </option>  
		  <option <?php if ($_SESSION["building_type"] == "office") echo " selected "; ?> value="office">Office</option>
		  <option <?php if ($_SESSION["building_type"] == "retail") echo " selected "; ?> value="retail">Retail</option>
		  <option <?php if ($_SESSION["building_type"] == "restaurant") echo " selected "; ?> value="restaurant">Restaurant</option>
		  <option <?php if ($_SESSION["building_type"] == "home-based") echo " selected "; ?> value="home-based">Home-Based</option>
		  <option <?php if ($_SESSION["building_type"] == "warehouse") echo " selected "; ?> value="warehouse">Warehouse</option>		  
		  <option <?php if ($_SESSION["building_type"] == "industrial") echo " selected "; ?> value="industrial">Industrial</option>
		  <option <?php if ($_SESSION["building_type"] == "other") echo " selected "; ?> value="other">Other</option>
        </select>
	  </td>
	  <td class="mainsecuritytext"  align="right">Location:</td>
	  <td class="mainsecuritytext"  align="left"> 
	 		<input type="radio" <?php if ($_SESSION["own_rent"] == "own") echo " checked "; ?>  name="own_rent" value="own" >Own
			<input type="radio" <?php if ($_SESSION["own_rent"] == "rent") echo " checked "; ?> name="own_rent" value="rent">Rent	 
	  </td>
	 
    </tr>
	<tr> 
      <td class="mainsecuritytext"  align="left">How many Locations:</td>
      <td colspan="3" align="left"> 
	  	<select size="1" name="num_location" tabindex="1"  class="mainsecuritytext">
		  <option <?php if ($_SESSION["num_location"] == "1") echo " selected "; ?> value="1">1</option>
		  <option <?php if ($_SESSION["num_location"] == "2") echo " selected "; ?> value="2">2</option>
		  <option <?php if ($_SESSION["num_location"] == "3-4") echo " selected "; ?> value="3-4">3-4</option>
		  <option <?php if ($_SESSION["num_location"] == "5-9") echo " selected "; ?> value="5-9">5-9</option>
		  <option <?php if ($_SESSION["num_location"] == "10+") echo " selected "; ?> value="10+">10+</option>
        </select>
	  </td>	 
    </tr>
	<tr> 
      	<td class="mainsecuritytext" valign="top"  align="left">Additional Services:</td>
		<td  colspan="3" >		  
			<table width="100%" class="mainsecuritytext">
				<tr align="left" >
					<td align="left" width="50%"><input type="checkbox" name="fire" <?php if ($_SESSION["fire"] == "1") echo " checked "; ?>  value='1' />Fire Protection</td>
					<td align="left" width="50%"><input type="checkbox" name="access" <?php if ($_SESSION["access"] == "1") echo " checked "; ?>  value='1' />Access Control</td>
				</tr>
				<tr align="left" height="20">
					<td  align="left"><input type="checkbox" name="cctv" <?php if ($_SESSION["cctv"] == "1") echo " checked "; ?>  value='1' />Video Surveillance (CCTV) System</td>
					<td align="left">Other<input type='text' class='mainsecuritytext' <?php if (strlen($_SESSION["other_field"]) > 0) echo " value='".$_SESSION["other_field"]."' "; ?> name='other_service' /></td>
				</tr>					
			</table>	    
		 </td>
    </tr>
    <tr> 
      <td valign="top" class="mainsecuritytext" align="left"> Please add any special 
        comments and/or special needs:</td>
      <td colspan="3" class="mainsecuritytext" align="left"> <textarea rows="7" name="comments" cols="60" class="mainsecuritytext"><?php if (strlen($_SESSION["comments"]) > 0) echo $comments; ?></textarea></td>
    </tr>
<!--- end business_security_partial_quote.php --->