<!--- Start residential_security_partial_quote.php --->
<tr> 
	<td>&nbsp;</td>
    <td colspan="3" class="mainsecuritytopic" align="left">
	  	Select services below:
	</td>
    </tr>

    <tr align="left"> 
      <td <?php if (substr_count($msg,"current_needs") > 0) echo " class='errortext' "; else echo " class='mainsecuritytext' "; ?>  align="left">Current Needs:</td>
      <td class="mainsecuritytext"  align="left"> 
	  <select size="1" name="current_needs" tabindex="1"  class="mainsecuritytext">
		  <option value="XX"> - Select - </option>
		  <option <?php if ($_SESSION["current_needs"] == "new") echo " selected "; ?> value="new">New Security System</option>
		  <option <?php if ($_SESSION["current_needs"] == "upgrade") echo " selected "; ?>  value="upgrade">Upgrade Current Service</option>
		  <option <?php if ($_SESSION["current_needs"] == "replace") echo " selected "; ?>  value="replace">Replace Current Service</option>
      </select>
	  </td>
	  <td class="mainsecuritytext"  align="right">Square Footage:</td>
      <td  align="left"> 
	  	<select size="1" name="sqr_footage" tabindex="1"  class="mainsecuritytext">		 
		  <option value="0">0 - 1,499 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "15") echo " selected "; ?>  value="15">1,500 - 2,999 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "30") echo " selected "; ?>  value="30">3,000 - 4,999 ft</option>
		  <option <?php if ($_SESSION["sqr_footage"] == "50") echo " selected "; ?>  value="50">5,000 - 19,999 ft</option>		  
		  <option <?php if ($_SESSION["sqr_footage"] == "200") echo " selected "; ?>  value="200">20,000+ ft</option>
        </select>
	  </td>
    </tr>
    <tr> 
      <td <?php if (substr_count($msg,"building_type") > 0) echo " class='errortext' "; else echo " class='mainsecuritytext' "; ?>  align="left">Type of Home:</td>
      <td  align="left"> 
		<select size="1" name="building_type" tabindex="1"  class="mainsecuritytext">
		 <option value="XX"> - Select - </option>
			<option <?php if ($_SESSION["building_type"] == "house") echo " selected "; ?>  value="house">House</option>
			<option <?php if ($_SESSION["building_type"] == "apartment") echo " selected "; ?>  value="apartment">Apartment</option>
			<option <?php if ($_SESSION["building_type"] == "town house") echo " selected "; ?>  value="town house">Townhouse</option>			
			<option <?php if ($_SESSION["building_type"] == "condo") echo " selected "; ?>  value="condo">Condo</option>
			<option <?php if ($_SESSION["building_type"] == "mobile home") echo " selected "; ?>  value="mobile home">Mobile Home</option>
        </select>
	  </td>
	  <td>&nbsp;</td>
	  <td class="mainsecuritytext"  align="left" colspan="1">Do you:
			<input type="radio" <?php if ($_SESSION["own_rent"] == "own") echo " checked "; ?>  name="own_rent" value="own" >Own
			<input type="radio" <?php if ($_SESSION["own_rent"] == "rent") echo " checked "; ?> name="own_rent" value="rent">Rent	  </td>
    </tr><tr><td colspan="3" class="mainsecuritytext" align="left">&nbsp;</td>
    </tr>
	<!--- End residential_security_partial_quote.php --->