<?php
	include 'support/settings.php';
	define(DUPLICATE,-1);
	define(RETURNEE,4400);
	define(NEWBIE,1337);
	define(OFF_BG,"#FFFFFF");
	define(ON_BG,"#E4E2CE");
	
		
	$msgs = array();								
	//common to both.
	$msgs["address"] = "Please enter your Street Address.";
	$msgs["email"] = "Please enter your Email Address.";
	$msgs["zip_state"] = "Error: You Zip Code and State do not match up.";
	$msgs["zip"] = "Please enter your Zip Code.";
	$msgs["state_code"] = "Please select your State.";
	$msgs["city"] = "Please enter your City.";
	$msgs["current_needs"] = "Please select your Security Needs.";
	$msgs["dirty"] = "Please do not include swear words in your quote form.";
	$msgs["building_type"] = "Please select your Building Type.";
	$msgs["invalid_phone"] = "Sorry, we could not validate your Primary Telephone Number.";
	$msgs["duplicate"] = "We see that you have already submitted a quote today.  Please wait for these companies to contact you.";
	$msgs["own_rent"] = "Please let us know if you own or rent";
	$msgs["injection"] = "Do not include live code in your quote submission. It will not be accepted, and this attempt has been logged.";
	

	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
	<head>
		<title>Call Center</title>
		<meta name="verify-v1" content="ThpISckmItL5BqxrSpyLct+Cba+uPZs1keVYe1TqljA=" /> 
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<?php if ($showform) { ?>
			<script src="/support/submitscript.js"  type="text/javascript"></script>
		<?php } ?>	
				
		
		<link href="/support/tac.css" rel="stylesheet" type="text/css" />
		<? include "trackinclude.php"; ?>
	

	</head>
	<body>
		