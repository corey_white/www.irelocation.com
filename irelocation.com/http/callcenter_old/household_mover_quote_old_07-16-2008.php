<?
	
	session_start();
	$rkeys = array_keys($_REQUEST);
	foreach($rkeys as $k)
	{
		$_SESSION[$k] = $_REQUEST[$k];
	}
	if($_REQUEST['name'] != "")
	{
		list($fname,$lname)=explode(" ",$_REQUEST['name'],2);
		$_SESSION['fname'] = $fname;
		$_SESSION['lname'] = $lname;
	}
	else if($name != "")
	{
		list($fname,$lname)=explode(" ",$name,2);
		$_SESSION['fname'] = $fname;
		$_SESSION['lname'] = $lname;
	}

	

	include("header.php");

	if (strlen($error_msg) == 0)
	{
		$error_msg = $_REQUEST["msg"];
	}
	
	$msgs["duplicate"] = "We see that you have allready submitted a quote today.  Please wait for these companies to contact you.";
	$msgs["link_inject"] = "Please do not include live links in your comments";
	$msgs["fname"] = "Please enter your First name";
	$msgs["lname"] = "Please enter your Last name";
	$msgs["phone"] = "Please enter your Phone Number";
	$msgs["email"] = "Please enter your Email Address";
	$msgs["month"] = "Please select your Moving Date.";
	$msgs["origin_zip"] = "Please enter your Starting Zip Code.";
	$msgs["destination_city"] = "Please enter your Destination City.";
	$msgs["destination_state"] = "Please select your State.";
	$msgs["bedrooms"] = "Please Specify the number of bedrooms in your home.";
	$msgs["invalid_zip"] = "The Starting Zip code you entered was not found.";
	$msgs["invalid_phone"] = "Sorry, we could not validate your Area Code or Prefix.";
	$msgs["destination_city_state"] = "Please enter the full City Name to where you would like to move.";
	$msg = $msgs[$error_msg];
	
?>


		<? // Change this to submit to moving_quote_submit.php to send auto leads to marble ?><style type="text/css">
<!--
body {
	background-color: #666666;
}
.style1 {
	font-size: 18px;
	font-weight: bold;
}
.style3 {font-size: 16px; font-weight: bold; }
-->
        </style>

<?php
	include 'content/nav.php';
?>
		<br />
		<form name="housequoteform" method="post" action="moving_quote_submit2.php" >
		<input type="hidden" name="js" value="1" />
		<table width="700" border="1" align="center" cellpadding="3" cellspacing="0" bordercolor="#999999" bgcolor="#CCCCCC">
          <tr>
            <td><table width="700" border="0" align="center" cellpadding="2" cellspacing="0" class="text_form2" id="table12">
				
							  <td colspan="2" align="center" class="mainsecuritytopic"><span class="style1">Moving Quote</span></td>
			  </tr>
<?php
if($msg!=""){
?>
	<td colspan="2" align="center"><font color="#FF0000"><?php echo $msg; ?></font></td>
<?php 
}
?>
			  <tr>
			  
			  </tr>
			  <tr>		
							<td colspan="2" align="left" class="mainsecuritytopic"><span class="style3">Contact Information</span></td>
			  </tr>
			  <tr>
								<td align="right" <? if (substr_count($error_msg,"fname") > 0) echo " class='errortext' ";?> >First Name:</td>
								<td>
								<input type="text" name="fname" size="16" class="field1" value="<? echo $_SESSION["fname"];?>"></td>
			  </tr>
							<tr>
								<td align="right" <? if (substr_count($error_msg,"lname") > 0) echo " class='errortext' ";?>>Last Name:</td>
								<td>
								<input type="text" name="lname" size="16" class="field1" value="<? echo $_SESSION["lname"];?>"></td>
							</tr>
							<tr>
								<td align="right" <? if (substr_count($error_msg,"email") > 0) echo " class='errortext' ";?>>E-Mail Address:</td>
								<td>
								<input type="text" name="email" size="24" class="field1" value="<? echo $_SESSION["email"];?>"></td>
							</tr>
							<tr>
								<td align="right" <? if (substr_count($error_msg,"phone") > 0) echo " class='errortext' ";?>>Phone Number:</td>
								<td>
								<input type="text" name="phone" size="16" class="field1" value="<? echo $_SESSION["phone"];?>"></td>
								<input type="hidden" name="contact" value="phone">
							</tr>
							<tr>
								<td align="right" <? if (substr_count($error_msg,"origin") > 0) echo " class='errortext' ";?>>Moving From:</td>
								<td class="text_dark">
								<span style="font-weight: 400">zip code
								<input type="text" name="origin_zip" size="6" class="field1" maxlength=5 value="<? echo $_SESSION["origin_zip"];?>">
								</span></td>
							</tr>
							<tr>
								<td align="right" <? if (substr_count($error_msg,"destination") > 0) echo " class='errortext' ";?> >Moving To:</td>
								<td class="text_dark">
								<span style="font-weight: 400">city
								<input type="text" name="destination_city" size="9" class="field1" maxlength=50 value="<? echo $_SESSION["destination_city"];?>">
								</span>
								<select size="1" name="destination_state"<?if($agent_level!='low'){echo ' class="field1"';}?>>
              					<option value="XX">- Please Select -</option>
              					<option value="AL"<? if ($_SESSION["destination_state"]=='AL'){echo " selected";}?>>Alabama</option>
              					<option value="AK"<? if ($_SESSION["destination_state"]=='AK'){echo " selected";}?>>Alaska</option>
              					<option value="AZ"<? if ($_SESSION["destination_state"]=='AZ'){echo " selected";}?>>Arizona</option>
              					<option value="AR"<? if ($_SESSION["destination_state"]=='AR'){echo " selected";}?>>Arkansas</option>
              					<option value="CA"<? if ($_SESSION["destination_state"]=='CA'){echo " selected";}?>>California</option>
              					<option value="CO"<? if ($_SESSION["destination_state"]=='CO'){echo " selected";}?>>Colorado</option>
              					<option value="CT"<? if ($_SESSION["destination_state"]=='CT'){echo " selected";}?>>Connecticut</option>
              					<option value="DC"<? if ($_SESSION["destination_state"]=='DC'){echo " selected";}?>>DC</option>
              					<option value="DE"<? if ($_SESSION["destination_state"]=='DE'){echo " selected";}?>>Delaware</option>
              					<option value="FL"<? if ($_SESSION["destination_state"]=='FL'){echo " selected";}?>>Florida</option>
              					<option value="GA"<? if ($_SESSION["destination_state"]=='GA'){echo " selected";}?>>Georgia</option>
              					<option value="HI"<? if ($_SESSION["destination_state"]=='HI'){echo " selected";}?>>Hawaii</option>
              					<option value="ID"<? if ($_SESSION["destination_state"]=='ID'){echo " selected";}?>>Idaho</option>
              					<option value="IL"<? if ($_SESSION["destination_state"]=='IL'){echo " selected";}?>>Illinois</option>
              					<option value="IN"<? if ($_SESSION["destination_state"]=='IN'){echo " selected";}?>>Indiana</option>
              					<option value="IA"<? if ($_SESSION["destination_state"]=='IA'){echo " selected";}?>>Iowa</option>
              					<option value="KS"<? if ($_SESSION["destination_state"]=='KS'){echo " selected";}?>>Kansas</option>
              					<option value="KY"<? if ($_SESSION["destination_state"]=='KY'){echo " selected";}?>>Kentucky</option>
              					<option value="LA"<? if ($_SESSION["destination_state"]=='LA'){echo " selected";}?>>Louisiana</option>
              					<option value="ME"<? if ($_SESSION["destination_state"]=='ME'){echo " selected";}?>>Maine</option>
              					<option value="MD"<? if ($_SESSION["destination_state"]=='MD'){echo " selected";}?>>Maryland</option>
              					<option value="MA"<? if ($_SESSION["destination_state"]=='MA'){echo " selected";}?>>Massachusetts</option>
              					<option value="MI"<? if ($_SESSION["destination_state"]=='MI'){echo " selected";}?>>Michigan</option>
              					<option value="MN"<? if ($_SESSION["destination_state"]=='MN'){echo " selected";}?>>Minnesota</option>
              					<option value="MS"<? if ($_SESSION["destination_state"]=='MS'){echo " selected";}?>>Mississippi</option>
              					<option value="MO"<? if ($_SESSION["destination_state"]=='MO'){echo " selected";}?>>Missouri</option>
              					<option value="MT"<? if ($_SESSION["destination_state"]=='MT'){echo " selected";}?>>Montana</option>
              					<option value="NE"<? if ($_SESSION["destination_state"]=='NE'){echo " selected";}?>>Nebraska</option>
              					<option value="NV"<? if ($_SESSION["destination_state"]=='NV'){echo " selected";}?>>Nevada</option>
              					<option value="NH"<? if ($_SESSION["destination_state"]=='NH'){echo " selected";}?>>New Hampshire</option>
              					<option value="NJ"<? if ($_SESSION["destination_state"]=='NJ'){echo " selected";}?>>New Jersey</option>
              					<option value="NM"<? if ($_SESSION["destination_state"]=='NM'){echo " selected";}?>>New Mexico</option>
              					<option value="NY"<? if ($_SESSION["destination_state"]=='NY'){echo " selected";}?>>New York</option>
              					<option value="NC"<? if ($_SESSION["destination_state"]=='NC'){echo " selected";}?>>North Carolina</option>
              					<option value="ND"<? if ($_SESSION["destination_state"]=='ND'){echo " selected";}?>>North Dakota</option>
              					<option value="OH"<? if ($_SESSION["destination_state"]=='OH'){echo " selected";}?>>Ohio</option>
              					<option value="OK"<? if ($_SESSION["destination_state"]=='OK'){echo " selected";}?>>Oklahoma</option>
              					<option value="OR"<? if ($_SESSION["destination_state"]=='OR'){echo " selected";}?>>Oregon</option>
              					<option value="PA"<? if ($_SESSION["destination_state"]=='PA'){echo " selected";}?>>Pennsylvania</option>
              					<option value="RI"<? if ($_SESSION["destination_state"]=='RI'){echo " selected";}?>>Rhode Island</option>
              					<option value="SC"<? if ($_SESSION["destination_state"]=='SC'){echo " selected";}?>>South Carolina</option>
              					<option value="SD"<? if ($_SESSION["destination_state"]=='SD'){echo " selected";}?>>South Dakota</option>
              					<option value="TN"<? if ($_SESSION["destination_state"]=='TN'){echo " selected";}?>>Tennessee</option>
              					<option value="TX"<? if ($_SESSION["destination_state"]=='TX'){echo " selected";}?>>Texas</option>
              					<option value="UT"<? if ($_SESSION["destination_state"]=='UT'){echo " selected";}?>>Utah</option>
              					<option value="VT"<? if ($_SESSION["destination_state"]=='VT'){echo " selected";}?>>Vermont</option>
              					<option value="VA"<? if ($_SESSION["destination_state"]=='VA'){echo " selected";}?>>Virginia</option>
              					<option value="WA"<? if ($_SESSION["destination_state"]=='WA'){echo " selected";}?>>Washington</option>
              					<option value="WV"<? if ($_SESSION["destination_state"]=='WV'){echo " selected";}?>>West Virginia</option>
              					<option value="WI"<? if ($_SESSION["destination_state"]=='WI'){echo " selected";}?>>Wisconsin</option>
              					<option value="WY"<? if ($_SESSION["destination_state"]=='WY'){echo " selected";}?>>Wyoming</option>
              					</select>
								</td>
				<tr>	
			  <tr>		
							<td colspan="2" align="left" class="mainsecuritytopic"><span class="style3">Moving Information</span></td>
			  </tr>
              <tr>
                <td width="185" align="right" <? if (substr_count($error_msg,"month") > 0) echo " class='errortext' ";?>>Approximate Moving Date:</td>
                <td><select size="1" name="move_month" class="field2">
                    <?for($i=1;$i<=12;$i++){ $mytime=mktime(0,0,0,$i,1,1);?>
                    <option value="<?echo date("m",$mytime);?>"<?if($month==date("m",$mytime)){echo " selected";}elseif(($month=='') && (date("m",$mytime)==date("m",time()+1296000))){echo " selected";}?>><?echo date("M",$mytime);?></option>
                    <?}?>
                  </select>
                    <select size="1" name="move_day" class="field2">
                      <?for($i=1;$i<=31;$i++){ $mytime=mktime(0,0,0,1,$i,1);?>
                      <option value="<?echo date("d",$mytime);?>"<?if($day==date("d",$mytime)){echo " selected";}elseif(($day=='') && (date("d",$mytime)==date("d",time()+1296000))){echo " selected";}?>><?echo date("j",$mytime);?></option>
                      <?}?>
                    </select>
                    <select size="1" name="move_year" class="field2">
                      <?for($i=date("Y");$i<=(date("Y")+1);$i++){ $mytime=mktime(0,0,0,1,1,$i);?>
                      <option value="<?echo date("Y",$mytime);?>"<?if($year==date("Y",$mytime)){echo " selected";}elseif(($year=='') && (date("Y",$mytime)==date("Y",time()+1296000))){echo " selected";}?>><?echo date("Y",$mytime);?></option>
                      <?}?>
                  </select></td>
              </tr>
              <?
					#
					# Again, the arrays is a bit much, but if we want to change it later it's really easy
					#
					?>
              <tr>
                <td width="185" align="right">Type of Home:</td>
                <td><select size="1" name="type_home" class="field2">
                    <?
								$homes = array("house","apartment","condo","storage","other");
								foreach($homes as $home)
								{ ?>
                    <option value="<? echo $home;?>"<? if($_SESSION["type_of_home"]==$home){echo " selected";}?>><? echo ucwords($home);?></option>
                    <? } ?>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="185" align="right" <? if (substr_count($error_msg,"bedrooms") > 0) echo " class='errortext' ";?> >Number of Bedrooms:</td>
                <td><select size="1" name="bedrooms" class="field2">
                    <option value="XX">-Select Bedroom-</option>
                    <? for($i=1;$i<=10;$i++){ ?>
                    <option value="<? echo $i; ?> bedroom" <? if($_SESSION["bedrooms"]==$i){echo " selected";}?>><? echo $i;?> Bedroom</option>
                    <?}?>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="185" align="right" valign="top">Furnished Rooms:</td>
                <td><table border="0" cellspacing="0" width="100%" cellpadding="0">
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="attic"<? if($_SESSION["attic"] == '1'){echo " checked";}?> />
                        attic </span></td>
                      <td class="text_dark"><span style="font-weight: 400">
                        <input type="checkbox" value="1" name="living"<? if($_SESSION["living"] == '1'){echo " checked";}?> />
                        living room </span></td>
                    </tr>
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="basement"<? if($_SESSION["basement"] == '1'){echo " checked";}?> />
                        basement&nbsp; </span></td>
                      <td class="text_dark"><span style="font-weight: 400">
                        <input type="checkbox" value="1" name="office"<? if($_SESSION["office"] == '1'){echo " checked";}?> />
                        office</span></td>
                    </tr>
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="den"<? if($_SESSION["den"] == '1'){echo " checked";}?> />
                        den </span></td>
                      <td class="text_dark"><span style="font-weight: 400">
                        <input type="checkbox" value="1" name="patio"<? if($_SESSION["patio"] == '1'){echo " checked";}?> />
                        patio</span></td>
                    </tr>
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="dining"<? if($_SESSION["dining"] == '1'){echo " checked";}?> />
                        dining room </span></td>
                      <td class="text_dark"><span style="font-weight: 400">
                        <input type="checkbox" value="1" name="play"<? if($_SESSION["play"] == '1'){echo " checked";}?> />
                        play room </span></td>
                    </tr>
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="garage"<? if($_SESSION["garage"] == '1'){echo " checked";}?> />
                        garage </span></td>
                      <td class="text_dark"><span style="font-weight: 400">
                        <input type="checkbox" value="1" name="shed"<? if($_SESSION["shed"] == '1'){echo " checked";}?> />
                        shed</span></td>
                    </tr>
                    <tr>
                      <td class="text_dark"><span style="font-weight: 400">&nbsp;
                            <input type="checkbox" value="1" name="kitchen"<? if($_SESSION["kitchen"] == '1'){echo " checked";}?> />
                        kitchen </span></td>
                      <td></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td width="185" align="right">Does your home have stairs?</td>
                <td><select size="1" name="stairs" class="field2">
                    <option value="no" >No</option>
                    <option value="yes"<? if($_SESSION["stairs"]=='yes'){echo " selected";} ?>>Yes</option>
                  </select>
                </td>
              </tr>
              <tr>
                <td width="185" align="right">Does your home have an elevator?</td>
                <td><select size="1" name="elevator" class="field2">
                    <option value="no" >No</option>
                    <option value="yes"<? if($_SESSION["elevator"]=='yes'){echo " selected";}?>>Yes</option>
                  </select>
                </td>
              </tr>
              
              <tr>
                <td width="185" align="right" valign="top">Comments:</td>
                <td><textarea rows="4" name="comments" cols="39" class="field2"><? echo $_SESSION["comments"]; ?></textarea></td>
              </tr>
              <tr>
                <td width="185" align="right" class="text_form1">&nbsp;</td>
                <td>&nbsp;</td>
              </tr>
              <!--
          <tr>
            <td width="185" align="right" class="text_form1"><span style="font-weight: 400">Would like us to help you <b>Find a Realtor</b>? </span></td>
            <td><select size="1" name="realtor" class="field1">
                <option value="0" >No</option>
                <option value="1" <? if ($_SESSION["realtor"] == "1") echo " selected "; ?> >Yes</option>
            </select></td>
          </tr>
		  
          <tr>
            <td width="185">&nbsp;</td>
            <td width="507">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2"><span style="font-weight: 400">Would you like to get an estimate for a <b>Self Storage Unit</b>? </font>
                <?php $storage = $_SESSION["storage"]; ?>
                <select size="1" name="storage" class="field1">
                  <option value="0">No</option>
                  <option value="1" <? if ($storage == 1) echo " selected "; ?>>Yes</option>
                </select>            </td>
          </tr>
          <tr>
            <td colspan="2"><span style="font-weight: 400">How long would you need this <strong>Storage Unit?</strong></font>
                <?php $estimatetime = $_SESSION["estimatetime"]; ?>
                <select size="1" name="estimatetime" class="field1">
                  <option value="Select..." <? if ($estimatetime == "Select...") echo " selected "; ?>>Select...</option>
                  <option value="1 Month" <? if ($estimatetime == "1 Month") echo " selected "; ?>>1 Month</option>
                  <option value="2 Months" <? if ($estimatetime == "2 Months") echo " selected "; ?>>2 Months</option>
                  <option value="3 Months" <? if ($estimatetime == "3 Months") echo " selected "; ?>>3 Months</option>
                  <option value="4 Months" <? if ($estimatetime == "4 Months") echo " selected "; ?>>4 Months</option>
                  <option value="5 Months" <? if ($estimatetime == "5 Months") echo " selected "; ?>>5 Months</option>
                  <option value="6 Months" <? if ($estimatetime == "6 Months") echo " selected "; ?>>6 Months</option>
                  <option value="More than 6 Months" <? if ($estimatetime == "More than 6 Months") echo " selected "; ?>>More than 6 Months</option>
                </select>            </td>
          </tr>
          
					<tr>
						<td width="14%">&nbsp;</td>
						<td colspan="2" class="text_form1"><table height="71" border="0" cellpadding="1" cellspacing="0">
						<tr>
                            <td align="center"><font face="Verdana" color="#4e4e4e" size="1"><img src="images/sears.gif" width="156" height="69" /></font></td>
                          </tr>
                          <tr>
                            <td width="383"><span style="font-weight: 400">Would you like to get a <strong>Sears Carpet</strong> Cleaning estimate? </span>
                                <?php $clean = $_SESSION["clean"]; ?>
                                <select size="1" name="clean" class="formfield">
                                  <option value="0">No</option>
                                  <option value="1" <? if ($clean == 1) echo " selected "; ?>>Yes</option>
                                </select>
                                <span style="font-weight: 400"><br />
                                  How many rooms do you need cleaned?</span>
                                <?php $roomcleaning = $_SESSION["roomcleaning"]; ?>
                                <select size="1" name="roomcleaning" class="formfield">
                                  <option value="Select..." <? if ($roomcleaning == "Select...") echo " selected "; ?>>Select...</option>
                                  <option value="3 Rooms" <? if ($roomcleaning == "3 Rooms") echo " selected "; ?>>3 Rooms</option>
                                  <option value="5 Rooms" <? if ($roomcleaning == "5 Rooms") echo " selected "; ?>>5 Rooms</option>
                                  <option value="7 Rooms" <? if ($estimatetime == "7 Rooms") echo " selected "; ?>>7 Rooms</option>
                                </select>
                                <br /></td>
                          </tr>
                          
                        </table></td>
					</tr>
					-->
              <tr>
                <td colspan="3" align="center"><input type="submit" value="Request an Estimate -&gt;" name="submit" class="button1" /></td>
              </tr>
            </table></td>
          </tr>
        </table>
		</form>
 <?
 include("templates/foot.php");
 ?>
