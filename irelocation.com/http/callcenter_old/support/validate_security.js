function validate(form)
{
  if (form.name1.value == "")
  {
	  if (form.quote_type.value == 'res')
    	alert("Please enter your first name");
	 else
		 alert("Please enter your Company's name");
    form.name1.focus()
    return false;
  }
  else if (form.name1.value.length < 2)
  {
	   if (form.quote_type == "res")
    	alert("Please enter your full first name");
	 else
		 alert("Please enter your Company's full name");
    form.name1.focus();
	 return false;
  }
  if (form.name2.value == "")
  {
   if (form.quote_type == "res")
    	alert("Please enter your last name");
	 else
		 alert("Please enter your Contact name");
    form.name2.focus();
    return false;
  }
  else if (form.name2.value.length < 2)
  {
	   if (form.quote_type == "res")
    	alert("Please enter your full last name");
	 else
		 alert("Please enter your Company's Contact name");
    form.name1.focus()
	 return false;
  }
  if (form.phone1.value == "")
  {
    alert("Please enter a phone number");
    form.phone1.focus()
    return false;
  }
  
  var GoodChars = "0123456789"
  var i = 0
  var TempPhone = ""
  var LastChar = ""
  var count = 0
  for (i =0; i <= form.phone1.value.length -1; i++) {
    if (GoodChars.indexOf(form.phone1.value.charAt(i)) != -1)
    {          
      if((TempPhone == "") && ((form.phone1.value.charAt(i) == "1") || (form.phone1.value.charAt(i) == "0")))
      {
      	//get rid of beginning 1 or 0
      }
      else
      {

        if(form.phone1.value.charAt(i)==LastChar)
        {
          count=count+1;
          if(count==10)
          {
            alert("Please enter a valid 10-digit phone number");
            form.phone1.focus();
            return false;
          }
        }        
        else
        {
          count=0;
        }

        if(LastChar=="")
        {
          count=1;
        }

        TempPhone+=form.phone1.value.charAt(i);
      }
      LastChar=form.phone1.value.charAt(i);      
    }
  }
  
  form.phone1.value=TempPhone  
  
  if(form.phone1.value.match(/^[ ]*[(]{0,1}[ ]*[0-9]{3,3}[ ]*[)]{0,1}[-]{0,1}[ ]*[0-9]{3,3}[ ]*[-]{0,1}[ ]*[0-9]{4,4}[ ]*$/)==null)
  {
    alert("Please enter a valid 10-digit phone number");
    form.phone1.focus();
    return false; 
  }
  
  var theStr = new String(form.email.value)
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
    {
      //pass
    }
    else
    {
      alert("Please enter a valid email address");
      form.email.focus();
      return false;
    }
  }
  else
  {
    alert("Please enter a valid email address");
    form.email.focus();
    return false;
  }
  
  if (form.address.value.length < 5)
  {
	  alert("Please enter a valid address");
	    form.address.focus();
    	return false;
  }
  
  if (form.zip.value == "")
  {
    alert("Please enter your zip code");
    form.zip.focus();
    return false;
  }
  else if (form.zip.value.length != 5)
  {
	  alert("Please enter a valid zip code");
	    form.zip.focus();
    	return false;
  }
  
 
  if (form.city.value == "")
  {
    alert("Please enter your city");
    form.city.focus();
    return false;
  }
  if (form.city.value.length < 3)
  {
    alert("Please enter the full city name.");
    form.city.focus();
    return false;
  }
  if (form.city.value.match(/^\d{1,}$/)!=null)
  {
    alert("Please enter a valid city");
    form.city.focus();
    return false;
  }
  if (form.state_code.value == "XX")
  {
    alert("Please select your state");
    form.state_code.focus();
    return false;
  }
  
  if (form.current_needs.value == "XX")
  {
	  alert("Please select your needs");
  	  form.current_needs.focus();
  	  return false;
  }
   if (form.building_type.value == "XX")
  {
	  alert("Please select your type of Building");
  	  form.building_type.focus();
  	  return false;
  }
}