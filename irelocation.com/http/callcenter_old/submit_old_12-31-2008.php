<?php
	session_start();
	
	include 'security_quotes.php';
	include 'includes/settings.php';
	//include 'retention.php';
	
	define(VALID_CHARS,"^([a-zA-Z \-\.']*)*$");
	define(VOWELS,"^(.*)?[aeiouyAEIOUY](.*)?$");
	define(INITIALS,"^[A-Za-z]\.( [A-Za-z]\.)?$");
	define(SHORTEST_TO_CHECK,3);
		
	function testName($name)
	{
		if (ereg(VALID_CHARS,$name))
		{
			if (strlen($name) > SHORTEST_TO_CHECK)
			{			
				if (ereg(VOWELS,$name)>0 || ereg(INITIALS,$name) > 0)
					return 1;				
				else
					return 0;
			}
			else return 1;
		}
		else return 0;		
	}
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		
		if ($_POST['quotetype'] == 'res') $quotepage = "residential_security_quotes.php";
		
		//split testing
		//if ($_POST['split'] == 'r') $quotepage = "index_r_split.php";
		//else if ($_POST['split'] == 'b') $quotepage = "index_b_split.php";
		//end split testing
		
		
		else $quotepage = "business_security_quotes.php";
		$output = strtolower(print_r($_POST,true));
		
		if ($type == "javascript")
		{
			if (substr_count($output,"<script") > 0)
				$content = substr($output,strpos($output,"<script"));						
			else
				$content = substr($output,strpos($output,"onload="));
		}
		else if ($type == "link")
			$content = substr($output,strpos($output,"href="));	
		else if  ($type == "serverside")
		{
			if (substr_count($output,"<?") > 0)
				$content = substr($output,strpos($output,"<?"));		
			else
				$content = substr($output,strpos($output,"<%"));		
		}
		else if ($type == "dirty")
		{
			header("Location: $quotepage?msg=dirty");
			exit();
		}
		$content = str_replace("'","&prime;",$content);
		$content = str_replace("\"","&quot;",$content);
		
		$sql = "insert into marble.injectors (site,ip,received,content) values ".
				"('usalarm_callenter','$ip','".date("YmdHis")."','$content'); ";
				
		$rs = new mysql_recordset($sql);
		
		
		header("Location: $quotepage?msg=injection");
		exit();
	}
	
	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");					
	}		
	testInjection();	

	$keys = array_keys($_POST);
	
	foreach($keys as $k)
	{
		$_POST[$k] = str_replace(array("<",">"),array("&lt;","&gt;"),trim($_POST[$k]));//kill all tags
		$_POST[$k] = str_replace(array("select","delete","insert","create","update","drop"),
								 array("choose","remove","add","make","change","fumble"),
								 $_POST[$k]);//kill all tags
		$_SESSION[$k] = $_POST[$k];
	}
	
	extract($_POST);	

	//mail("code@irelocation.com","Initially",$source." - ".$mysource);
	if (strlen(trim($source)) == 0)//from post.
	{	
		if (strlen(trim($mysource)) > 0)//try from session..
		{
			$source = $mysource;
			//mail("code@irelocation.com","Set From Session",$source." - ".$mysource);
		}
		else		//catch all. bad.
		{
			$source = SITE_BASE;
			//mail("code@irelocation.com","Catch All",$source." - ".$mysource);
		}
	}	

	while (substr_count($source,SITE_BASE) > 1)
		$source = str_replace(SITE_BASE."_".SITE_BASE."_",SITE_BASE."_",$source);
	
	//mail("code@irelocation.com","TAC",$msg);
	
	
	
// Setting up lead_ids for selection
	if (isset($brinks) && ($pa) && ($adt)) {
		$lead_ids = $brinks."|".$pa."|".$adt;
	} else if (isset($brinks) && ($pa)) {
		$lead_ids = $brinks."|".$pa;
	} else if (isset($brinks) && ($adt)) {
		$lead_ids = $brinks."|".$adt;
	} else if (isset($adt) && ($pa)) {
		$lead_ids = $adt."|".$pa;
	} else if (isset($adt)) {
		$lead_ids = $adt;
	} else if (isset($brinks)) {
		$lead_ids = $brinks;
	} else if (isset($pa)) {
		$lead_ids = $pa;
	}
	
	if ($quote_type == "res")
		$REDIRECT = "Location: /callcenter/residential_security_quotes.php";
	else
		$REDIRECT = "Location: /callcenter/business_security_quotes.php";
	
	$phone1 = ereg_replace("[^0-9]","",$phone1);

	$_SESSION["phone1"] = $phone1;
	
	$phone2 = str_replace("[^0-9]","",$phone2);
	$_SESSION["phone2"] = $phone2;
	
	$p_area   = substr($phone1,0,3);
    $p_prefix = substr($phone1,3,3);	
	$p_suffix = substr($phone1,6,4);
	
	if ($p_area != 800 && $p_area != 866 && $p_area != 877 && $p_area != 888)
	{
		$sql = "select * from movingdirectory.areacodes where npa = '$p_area' and nxx = '$p_prefix' and country = 'US';";
		//mail("code@irelocation.com","TAC","$p_area $p_prefix\n$sql");
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())
		{
			header("$REDIRECT?msg=invalid_phone ");
			mail("code@irelocation.com","TAC PHP Check","Invalid Phone Number\n\n".print_r($_SESSION,true));
			exit();
		}
	}	
	
	//clear out double+ spaces in the middle of the name.. 
	//screws up finding last name for commercial leads.
	
	$name1 = trim($name1);
	if (substr_count($name1,"  ") > 0)
		$name1 = ereg_replace("  "," ",$name1);
	$_SESSION['name1'] = $name1;
	if (substr_count($name2,"  ") > 0)
		$name2 = ereg_replace("  "," ",$name2);
	$_SESSION['name2'] = $name2;
	
	
	$name1 = trim($name1);
	if (strlen($name1) < 2)
	{
		header("$REDIRECT?msg=name1 ");
		exit();
	}
	else if (!testName($name1))
	{
		header("$REDIRECT?msg=name1 ");
		exit();
	}
	
	if (strlen($name2) < 3)
	{
		header("$REDIRECT?msg=name2 ");		
		exit();
	}
	
	if ($quote_type == "com" && substr_count($name2," ") == 0)
	{
		header("$REDIRECT?msg=name2 ");		
		exit();
	}
	
	
	if (!is_numeric($phone1) || strlen($phone1) < 10)
	{
		header("$REDIRECT?msg=phone1 ");		
		exit();
	}
	
	$email = trim($email);
	
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email) )
	{
		header("$REDIRECT?msg=email ");
		exit();
	} 	

	if (strlen($address) < 4 || substr_count($address," ") < 1 || substr_count($address,"@") > 0)
	{	
		header("$REDIRECT?msg=address ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}
	
	$city = trim($city);
	
	if ($city == "" || is_numeric($city) || strlen($city) < 3)
	{
		header("$REDIRECT?msg=city ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}
	
	if ($state_code == "XX")
	{
		header("$REDIRECT?msg=state_code ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}			

	if (!is_numeric($zip)  || strlen(trim($zip)) < 5)
	{
		header("$REDIRECT?msg=zip ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}

	$results = checkZip($zip,$state_code);
	
	if (!is_array($results))
	{
		header("$REDIRECT?msg=zip_state");		
		//mail("code@irelocation.com","TAC PHP Check","Invalid Zip\n\n".print_r($_SESSION,true));
		exit();
	}
	else
		$city = $results['city'];
	
	if($own_rent != "own" && $own_rent != "rent")
	{
		header("$REDIRECT?msg=own_rent ");
		exit();
	}

	if ($current_needs == "XX")
	{
		header("$REDIRECT?msg=current_needs ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}
	
	if ($building_type == "XX")
	{
		header("$REDIRECT?msg=building_type ");
		mail("code@irelocation.com","TAC PHP Check",print_r($_SESSION,true));
		exit();
	}
	
	if ($clean == 1 && $roomcleaning != "Select...")
	{
		$subject = "Carpet Cleaning Lead from www.TopAlarmCompanies.com";
		$to = "irelocation@searscarpet.com";
		$headers = "From: noreply@TopAlarmCompanies.com\r\n";
		$msg = "Lead from www.TopAlarmCompanies.com\n";
		$msg .= "Name: $name1 $name2\n";
		$msg .= "Phone: $phone\n";
		$msg .= "Email:  $email\n";
		$msg .= "Number of rooms to clean: $roomcleaning\n";
		$msg .= "Address: $address\n";
		$msg .= "City: $city\n";
		$msg .= "State: $state\n";
		$msg .= "Zip: $zip\n";
		
		mail($to,$subject,$msg,$headers);
	
		
	}
				
	$ts = time() - (24 * 3600);//subtract a day and a half.
	$time = date("YmdHis",$ts);

	$sql = "select * from irelocation.leads_security where ".
	" (phone1 = '$phone1' or email = '$email') and ".
	" (received = '' or received > '$time' or received is null) ".
	" and zip = '$zip' and city = '$city' and quote_type = '$quote_type' ";	
	
	//mail("code@irelocation.com","TAC Dupe Test",$sql);
	
	$rs = new mysql_recordset($sql);
	if ($rs->fetch_array())
	{
		header("$REDIRECT?msg=duplicate ");
		mail("code@irelocation.com","TAC PHP Check","Duplicate\n\n".print_r($_SESSION,true));
		exit();
	}
		
	

	
	if ($quote_type == "res")
	{
		$quote = new residential_quote();		
	}		
	else
	{		
		$quote = new business_quote();
	}
	
	if ($source == "usalarm_usalarm_overture")
		$source = "usalarm_overture";
	
	if (substr_count($source,"wc") > 0)
		$source = "aff_wc";
	else if (substr_count($source,"dm") > 0)
		$source = "aff_dm";
		
	$received = date("Ymdhis");
	
	$quote->source = $source;   
	$quote->received = $received;
	$quote->referrer = $referer; 	
	$quote->name1 = $name1;        
	$quote->name2 = $name2;        
	$quote->phone1 = $phone1;       
	$quote->phone2 = $phone2;       
	$quote->email = $email;        
	$quote->address = $address;      
	$quote->city = $city;         
	$quote->state_code = $state_code;   
	$quote->zip = $zip;          	
	$quote->current_needs = $current_needs; 
	$quote->building_type = $building_type; 
	$quote->num_location = $num_location;                           
	$quote->sqr_footage = $sqr_footage;  
	$quote->own_rent = $own_rent;     
	$quote->fire = $fire;         
	$quote->access = $access;       
	$quote->cctv = $cctv;         	
	$quote->other_field = $other_field;  
	$quote->comments = $comments;   
	$quote->campaign = $campaign;   
	$quote->lead_ids = $lead_ids;   

	$quote->scrub();
	$quote_id = $quote->save_quote();
	$_SESSION["quote_id"] = $quote_id;
	$_SESSION["quote_submitted"] == "true";
	
	//removeEntry($email,'tac');//don't need it if we got the quote.
		session_destroy();
		header("Location: /callcenter/thankyou.php");
	
	
	
	function checkZip($zip,$state)
	{
		$sql = "select * from movingdirectory.zip_codes where zip = '$zip' and state = '$state';";
		//mail("code@irelocation.com","tsc zip",$sql);
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$a = $rs->myarray;		
			$rs->close();
			return $a;
		}
		else
			return FALSE;
	}
	
	
?>