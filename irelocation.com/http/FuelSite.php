<?
	class FuelSite
	{
		var $url,$name;
		var $content;
		var $data;
		
		function FuelSite($url,$name)
		{
			$this->url = $url;
			$this->name = $name;
			$this->content = "";
			$this->data = array();
		}
		
		function getData()
		{
			$this->content = gethtmlsource($this->url);
			if (strlen($this->content) == 0)
			{
				mail("david@irelocation.com","Fuel Price Error: ".$this->name,"Price list not found:".
				$this->url);
				return false;
			}
			else return true;
		}
	
		function clean()
		{
		
		
		}
	
		function printPrices()
		{
			echo implode(",<Br/>\n",$this->data)."<br/>";
		}
	
		function process()
		{
			
		}
	}

	class FlyingJ extends FuelSite
	{
		function FlyingJ()
		{
			parent::FuelSite("http://serv2.flyingj.com/fuel/diesel_print_CF.cfm?state=US","Flying J");			
		}
		
		function clean()
		{
			$content = $this->content;
			$cut = strpos($content,"<TBODY>");
			$end = strpos($content,"</TBODY>",$cut);
				
			$content = substr($content,$cut,($end-$cut));
			$content = str_replace("TBODY","table",$content);
			$content = str_replace("<FONT face=Arial,Helvetica,Geneva,Swiss,".
									"SunSans-Regular size=2>&nbsp;","",$content);
			$this->content = $content;
		}
	
		function process()
		{	
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			echo "<!--- calling FJ process() --->\n";
			$lines = split("<TR>",$this->content);
			foreach($lines as $line)
			{
				$line = str_replace("  "," ",strip_tags($line));
				$parts = split("&nbsp;",$line);
				
				if (count($parts) == 12)
				{
					$new = array();
					$loc = trim($parts[2]);
					list($city,$add) = split("\n",$parts[2],2);
					list($state,$city) = split(", ",$city);
					$new[] = $state;
					$new[] = $city;
					$new[] = $add;
					if ($parts[3] == "N/A") $parts[3] = 0;
					if ($parts[5] == "N/A") $parts[5] = 0;
					
					if ($parts[3] != 0 && $parts[5] != 0)
						$new[] = min($parts[3],$parts[5]);
					else
					{
						$max = max($parts[3],$parts[5]);
						if (is_numeric($max))
							$new[] = $max;
					}
					if (count($new) == 4)
					{	
						$row[] = $new;
						//FL--Dade City -- I-75 Exit 285 & SR52 -- 2.819 
						$city = str_replace("'","",$new[1]);
						$loc = trim($new[2]);
						$price = trim($new[3]);
						$this->data[] =  "('Flying J','$new[0]','$city','$loc','$price')";						
					}
				}
			}		
			echo "<!--- prices found: ".count($this->data)." --->\n";
		}		
	}
	
	class Petro extends FuelSite
	{
		function Petro()
		{
			parent::FuelSite("http://www.petrotruckstops.com/fuel_search.sstg","Petro");
		}
		
		function process()
		{
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			echo "<!--- petro process(); --->\n";
			$data=explode('Date',str_replace(array("'","*","&nbsp;"),"",strip_tags($this->content)));
			$rows=explode("\n",$data[1]);
			
			foreach($rows as $myrow)			
			{  	
				$myrow=trim($myrow);
				if($myrow!='')
				{
					if ($myrow != "Updated")
						$row[]=$myrow;
				}
			}
			
			$i=0;
			
			while($row[$i]!='Averages:')
			{
				$state=$row[$i];
				$city=str_replace("'","",ucwords(strtolower($row[$i+1])));
				$address=str_replace("'","",$row[$i+3]);
				$price=$row[$i+4];
				if($state=='' || $city=='' || $price=='') break;
				
				$this->data[] =  "('Petro','$state','$city','$address','$price')";				
				$i=$i+6;
				$state="";
				$city="";
				$price="";
			}		
			echo "<!--- ".count($this->data). " petro records found. --->\n";
		}
	}
	
	class TravelCenters extends FuelSite
	{
		function TravelCenters()
		{
			parent::FuelSite("http://www.tatravelcenters.com/taweb/Content/DieselPricesPrint.aspx"
								,"Travel Centers");			
		}
		
		function clean()
		{
			$content = $this->content;
			$start = strpos($content,"<b>Retail</b></font></td>");
			$content = substr($content,$start+26);
			$this->content = $content;
		}
	
		function process()
		{	
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			
			echo "<!--- calling TravelCenters process() --->\n";
			$data=strip_tags($this->content);
	
			$lines = split("\n",$data);
			$rows = array();
			foreach($lines as $line)
			{
				if (strlen(trim($line)) > 0)
					$rows[] = trim($line);
			}		
			$count = count($rows);
			$i = 0;
			$state = "";
			
			//print_r($rows);
			
			while($i < $count)
			{
				if (substr_count($rows[$i],"&nbsp") == 0)
				{
					//all but first state.
					if ($city != "" && $price != "")
					{
						$city = str_replace("'","",$city);
						$loc = str_replace(array("'","&nbsp"),"",$loc);
						$this->data[] = "('TravelCenter','$state','$city','$loc','$price')";
						$price = "";
						$city = "";
						$loc = "";
					}
					if (substr($rows[$i],0,6) == "*Price")
						break;
					//new state
					$state = getStateCode($rows[$i]);
				}
				else if (substr_count($rows[$i],"ULSD") == 0 && substr_count($rows[$i],"DSL") == 0)				
				{
					if ($city != "" && $price != "")
					{
						$city = str_replace("'","",$city);
						$loc = str_replace(array("'","&nbsp"),"",$loc);
						$this->data[] = "('TravelCenter','$state','$city','$loc','$price')";
						$price = "";
						$city = "";
						$loc = "";
					}
				
					if (strpos($rows[$i],"I-") > 0)
					{
						list($city,$loc) = split("I-",$rows[$i],2);
						$loc = "I-".$loc;	
					}
					//echo "City: $city<Br/>\n";
				}
				else if(substr_count($rows[$i],"DSL") != 0)//desial
				{
					list($det,$price) = split("&nbsp",$rows[$i]);	
					$price = ereg_replace("[^0-9\.]","",$price);	
				}
				
				$i++;
			}	
			echo "<!--- prices found: ".count($this->data)." --->\n";
		}		
	}
	
	class Pilot extends FuelSite
	{
		function Pilot()
		{
			parent::FuelSite("http://www.pilotcorp.com/Locations/Travel_Centers/Complete_Pricing_List.aspx","Pilot");
			
		}
		
		function getData()
		{
			$this->content = gethtmlsource($this->url,'ddFuelTypes=Diesel Prices Only');
			if (strlen($this->content) == 0)
			{
				mail("david@irelocation.com","Fuel Price Error: ".$this->name,"Price list not found:".
				$this->url);
				return false;
			}
			else return true;
		}
	
		function clean()
		{
			$content = $this->content;
			$mark = "Friendly&nbsp;<nobr />Version</a></td>";
				$start = strpos($content,$mark);
			$end = strpos($content,"</table>",$start+500);
			$content = substr($content,$start+strlen($mark),$end-$start+strlen($mark));
			
			$start = strpos($content,"<tr class=\"gridHeader\">");
			$end = strpos($content,"</table>",$start);
			$content = "<table>".substr($content,$start,($end-$start))."</table>";
			$this->content = strip_tags($content);
		}
	
		function process()
		{
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			
			echo "<!--- calling Pilot process() --->\n";
			$lines = split("\n",$this->content);
			$rows = array();
			foreach($lines as $line)
			{
				if (strpos($line,"&nbsp;") == 0)
					continue; 
				if (strlen(trim($line)) > 0)
				{
					$start = strpos($line,"Center");		
					$end = strpos($line,"Prices");			
					$line = trim(substr($line,$start+6,$end-$start-6));		
					list($where,$price) = split("\\$",$line,2);
					list($cz,$loc) = split("&nbsp;",$where);
					
					$state = substr(trim($cz,","),-2);
					$city = substr(trim($cz,","),0,-2);
					
					$city = str_replace("'","",$city);
					$loc = str_replace("'","",$loc);
					
					$prices = split("\\$",$price);
					$this->data[] = "('Pilot','$state','$city','$loc','".array_pop($prices)."')";
				}
			}		
			echo "<!--- prices found: ".count($this->data)." --->\n";
		}	
	}
	
	class Loves extends FuelSite
	{
		function Loves()
		{
			parent::FuelSite("http://www.loves.com/DesktopModules/Loves.FuelPrices/FuelPrices_Print.aspx?ST=QWxs-jX0L4QmieS0%3d&CY=QWxs-jX0L4QmieS0%3d&SO=QWxs-jX0L4QmieS0%3d&SC=U3RhdGU%3d-Vgg9PejFAOU%3d&SD=QVND-hBYVDJoDyJc%3d","Loves");			
		}
		
		function clean()
		{
			$content=$this->content;
			
			$mark = "<table cellspacing=\"0\" rules=\"rows\" ".			
					"border=\"1\" id=\"gv_FuelPrices\" width=\"100%\">";
			$start = strpos($content,$mark);
			if ($start > 0)
			{
				$content = substr($content,$start+strlen($mark));
				$end = strpos($content,"</table>");
				if ($end > $start)
					$this->content = "<table>".substr($content,0,$end)."</table>";
					
			}
		}
		
		function process()
		{
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			
			echo "<!--- calling Loves process() --->\n";
			$rows=explode("\n",$this->content);
			foreach($rows as $myrow)
			{
				$myrow=trim($myrow);
				if($myrow!='')
					$row[]=$myrow;				
			} 	
			$i=0;
			$c = count($row);
			while($i < $c)
			{
				if (substr_count($row[$i],"Black10FP"))
				{
					$div = "</td><td class=\"Black10FP\">";
					$parts = split($div,trim($row[$i],$div));
					if (count($parts) == 5)
					{
						$city = $parts[1];
						$state = $parts[2];
						$state = getStateCode($state);
						$parts[3] = str_replace(array("&amp;","&nbsp;"),"",$parts[3]);
						$parts[4] = str_replace(array("&amp;","&nbsp;"),"",$parts[4]);
						if ($parts[3] == "" && $parts[4] == "")
						{
							$loc = "";
						}
						else if ($parts[3] != "" && $parts[4] != "")
						{
							$loc = $parts[3]." Exit ".$parts[4];		
						}
						else
							$loc = $parts[3];
					}
				}		
				else if (substr_count($row[$i],"_lbl_Price2") == 1)
				{
					$price = ereg_replace("[^0-9\.]","",strip_tags($row[$i]));
					if ($price != "\$0")
					{
						$city = str_replace("'","",$city);
						$loc = str_replace("'","",$loc);
						if ($state == "" || $city == "")
						{
						}
						else if ($price != 0)
							$this->data[] = "('Loves','$state','$city','$loc','$price')";
					}
					$city = $state = $loc = $price = "";
				}
				$i++;
			}
			echo "<!--- prices found: ".count($this->data)." --->\n";
		}		
	}
	
	class SpeedWay extends FuelSite
	{
		function SpeedWay()
		{
			parent::FuelSite("http://www.speedway.com/FindUs/StoreLocator/GasPriceSearch.aspx?State=Indiana","Speedway");
			
		}
		
		function clean()
		{
			$content=$this->content;
			$mark = "<div style=\"position:relative;right:0px;\">";
			$start = strpos($content,$mark);
			$mark = "<div id=\"Validators\" style=\"display:none;\">";
			$end = strpos($content,$mark);
			$content = substr($content,$start,($end-$start));
			$mark = "</tr><tr class=\"WhiteRow\">";
			$start = strpos($content,$mark);
			$this->content = substr($content,$start);
			return $this->content;
		}
	
		function process()
		{
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			
			$rows=explode("\n",$this->content);
			foreach($rows as $myrow)
			{
				$myrow=trim($myrow);
				if(strlen($myrow) >= 30 && substr_count($myrow,"imgStoreLogo") == 0)
					$row[]=$myrow;				
			}			
			$i=0;
			
			while($i < count($row))
			{
				if (substr_count($row[$i],"AddressColumn") == 1)
				{
					//echo "<br>\n";
					$parts = split("</div>",$row[$i++]);
					$loc = strip_tags($parts[0]);
					list($city,$state) = split(",",strip_tags($parts[1]));
					$zip = substr(strip_tags($parts[2]),0,5);
					//echo "$loc -- $city -- $state -- $zip <br>\n";
				}
				else if (substr_count($row[$i],"dgStores") == 0)
				{
					$mark = "id=\"truck_diesel\"";
					$start = strpos($row[$i],$mark);
					$mark = "id=\"kerosene\"";
					$end = strpos($row[$i],$mark);
					$td = strip_tags(substr($row[$i],$start,($end-$start)));
					if (strpos($td,"N/A") > 0)
						$i++;
					else
					{
						list($x,$price) = split(":",$td);
						$city = str_replace("'","",$city);
						$loc = str_replace("'","",$loc);
						$this->data[] = "('Speedway','$state','$city','$loc','$price')";
						$i++;
					}
				} 
				else
					$i++;
			}
		}
	}
	
	class SappBros extends FuelSite
	{
		function SappBros()
		{
			parent::FuelSite("http://www.sappbrostruckstops.com/corporate/fuel.php","Sapp Brothers");
		}
	
		function clean()
		{	
			$mark = "<tr  style=\"background-color:#CCCCCC\" >";
			$start = strpos($this->content,$mark);
			$content = substr($this->content,$start+strlen($mark));
			$end = strpos($content,"</table>");
			$this->content = substr($content,0,$end);			
		}
		
		function process()
		{
			if (!$this->getData())
				echo "Invalid Price List!";
				
			$this->clean();
			
			$lines = split("\n",$this->content);
			foreach($lines as $l)
				if (substr_count($l,"<td") > 0)				
					$rows[] = trim(strip_tags($l));
						
			$i = 0;
			while($i < count($rows))
			{
				$city = trim($rows[$i]);
				$price = ereg_replace("[^0-9\.]","",$rows[$i+1]);
				$i += 4;
				if (substr_count($city,"|") > 0)
					list($city,$alt) = split("\\|",$city);
				list($city,$state) = split(", ",$city,2);
				$this->data[] = "('Sapp Brothers','$state','$city','','$price')";
			}		
		}
	}
	
	
	
?>
