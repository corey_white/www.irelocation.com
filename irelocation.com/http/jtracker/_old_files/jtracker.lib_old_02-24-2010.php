<?	
/* 
************************FILE INFORMATION**********************
* File Name:  jtracker.lib.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	4/15/08 10:26 AM - Rob - doing a bunch of stuff to improve the error reporting to us (via code@ email) and outputting teh JTXML leadObj to see if we're actually sending the correct campaign type.
	==> Rewrote the getCampaignId so that it looked up the campaignID for JTracker from the lead_format table using the lead_id and campaign.
			Also changed the call for the above function in the populateLeadObject function
**********************************************************************
*/

	/*
		program flow:
			get a lead object,
			populate it
			send it to jtracker
			get response code
			parse it.
			save it.
	
	
	*/

	define('JT_LEAD_OBJECT_URL',"http://www.jtracker.com/WebServices/getLeadObject.wsdl");
	define('JT_POST_URL',"http://www.jtracker.com/WebServices/leadPosting.wsdl");
	define('JT_IRELO_SOURCE_ID',"f718ea9a978cc7f4da408087bb739c7a");
	define('JT_TEST_BROKER_ID',"5fac6b9dbf29e0ae4344ac88c1d4a968");
	define('JT_US',1);	
	define('JT_OPEN',1);
	define('JT_ENCLOSED',2);
	
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	
	//echo "<!--- JTracker.lib.php last modified: 6/14/2007 11:15am --->\n";
	//echo "<!--- JTracker.lib.php last touched: 3/3/08 15:22 PM --->\n";
	
	function recordLeadFailure($leaddata,$lead_id,$campaign,$table)
	{
		$quote_id = $leaddata['quote_id'];
		if ( $table == "marble.auto_quotes")
			$cat_id = 1;
		else if ($table == "irelocation.leads_security")
			$cat_id = 4;
		else
			$cat_id = $leaddata['cat_id'];
			
		$sql = "insert into `movingdirectory`.`failed_leads` 
	(`quote_id`, `cat_id`, `quote_table`, `campaign_name`, 
	`lead_id`,`failed`)
	values
	('$quote_id', '$cat_id', '$table', '$campaign', '$lead_id', 
	".date("YmdHis").");";
		//mail("code@irelocation.com","JTracker Failure SQL",$sql);
		$rs = new mysql_recordset($sql);
	}
	
	function getJtrackerEmail($lead_id,$campaign)
	{
		//echo "getJtrackerEmail($lead_id,$campaign)<br/>";
		$sql = " select lead_email 'email' from marble.rules where lead_id = $lead_id ".
				" and site_id = '".$campaign."' ";
		
		//echo "JtrackerEmail->sql = '".$sql."' <br/>";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$e = $rs->myarray['email'];
		$rs->close();
		return $e;
	}
	
	function postLead($leadObj)
	{		
		$postclient = new soapclient(JT_POST_URL, true);	
		if (($err = $postclient->getError()))
		{
			return "error";
		}
		else
		{
			$post_proxy = $postclient->getProxy();	
			$status = $post_proxy->postLead($leadObj);
			return $status;
		}		
	}
	
	//reset the object to use again
	function clearLeadObject(&$leadObj)
	{	
		$leadObj['brokerID'] = "";		
		$leadObj['leadSourceID'] = "";
		$leadObj['customer']['firstName'] = "";
		$leadObj['customer']['lastName'] = "";
		$leadObj['customer']['email'] = "";
		$leadObj['customer']['phone'] = "";
		$leadObj['customer']['phone2'] = "";			
		$leadObj['customer']['address']['city'] = "";
		$leadObj['customer']['address']['stateCode'] = "";
		$leadObj['customer']['address']['zipCode'] = "";
		$leadObj['customer']['address']['countryID'] = "";
		$leadObj['shippingInfo']['estimatedShipDate'] = "";														
		$leadObj['shippingInfo']['shipViaID'] = "";
		$leadObj['shippingInfo']['vehiclesRunFlag'] = "";
		$leadObj['shippingInfo']['comment'] = "";		
		$leadObj['pickupAddress']['city'] = "";
		$leadObj['pickupAddress']['zipCode'] = "";
		$leadObj['pickupAddress']['stateCode'] = "";
		$leadObj['pickupAddress']['countryID'] = "";
		$leadObj['dropoffAddress']['city'] = "";
		$leadObj['dropoffAddress']['stateCode'] = "";
		$leadObj['dropoffAddress']['zipCode'] = "";
		$leadObj['dropoffAddress']['countryID'] = "";
		$leadObj['vehicleList'][0]['year'] = "";
		$leadObj['vehicleList'][0]['make'] = "";
		$leadObj['vehicleList'][0]['model'] = "";
		$leadObj['vehicleList'][0]['typeID'] = "";
	}
	
	function populateLeadObject(&$leadObj,$mydata,$lead_id,$campaign)
	{
		//translate this to a (??).comp_name@jttmail.com address.		
		$email = getJtrackerEmail($lead_id,$campaign);
		//extract broker id from lead id
		$brokerId = getBrokerId($lead_id);
		
		//extract campaign/domain from email address.
#		$campaignId = getCampaignId($email); //-- OLD
		$campaignId = getCampaignId($lead_id,$campaign); //-- new and improved!
		echo "<strong>populateLeadObject campaignId</strong> = $campaignId<br />";
	
		if (strlen($mydata['fname']) == 0)
			list($mydata['fname'],$mydata['lname']) = split(" ",$mydata['name'],2);
	
		if ($mydata['vtype'] == "")
		{
			//ATUS Campaign leads.. databases are differant..
			//CLEAN THis Ish UP!!!
			$mydata['phone'] = $mydata['phone_home'];
			$mydata['vtype'] = $mydata['type'];
			$mydata['vmodel'] = $mydata['model'];
			$mydata['vmake'] = $mydata['make'];
			$mydata['vyear'] = $mydata['year'];
			//2007-05-01
			$mydata['jtracker_emd'] = $mydata['est_move_date'];
		}	
	
		$leadObj['brokerID'] = $brokerId;		
		$leadObj['leadSourceID'] = $campaignId;
		$leadObj['customer']['firstName'] = $mydata["fname"];
		$leadObj['customer']['lastName'] = $mydata["lname"];
		$leadObj['customer']['email'] = $mydata['email'];
		$leadObj['customer']['phone'] = $mydata['phone'];
		
		if ($mydata['phone_alt'] != "")
			$leadObj['customer']['phone2'] = $mydata['phone_alt'];
			
		$leadObj['customer']['address']['city'] = $mydata['origin_city'];
		$leadObj['customer']['address']['stateCode'] = $mydata['origin_state'];
		$leadObj['customer']['address']['zipCode'] = $mydata['origin_zip'];
		$leadObj['customer']['address']['countryID'] = JT_US;
		$leadObj['shippingInfo']['estimatedShipDate'] = $mydata['jtracker_emd'];
														//'2008-01-31';
		$leadObj['shippingInfo']['shipViaID'] = JT_OPEN;
		
		//-- Originally was testing to see if $mydata['running'] was 'r' but should have been testing for 'running'
		$leadObj['shippingInfo']['vehiclesRunFlag'] = ($mydata['running']=='running');
		
		
		if ($mydata['customer_comments'] != "")
			$leadObj['shippingInfo']['comment'] = $mydata['customer_comments'];
			
		$leadObj['pickupAddress']['city'] = $mydata['origin_city'];
		$leadObj['pickupAddress']['zipCode'] = $mydata['origin_zip'];
		$leadObj['pickupAddress']['stateCode'] = $mydata['origin_state'];
		$leadObj['pickupAddress']['countryID'] = JT_US;
		$leadObj['dropoffAddress']['city'] = $mydata['destination_city'];
		$leadObj['dropoffAddress']['stateCode'] = $mydata['destination_state'];
		$leadObj['dropoffAddress']['zipCode'] = $mydata['destination_zip'];
		$leadObj['dropoffAddress']['countryID'] = JT_US;
		$leadObj['vehicleList'][0]['year'] = $mydata['vyear'];
		$leadObj['vehicleList'][0]['make'] = $mydata['vmake'];
		$leadObj['vehicleList'][0]['model'] = $mydata['vmodel'];
		$leadObj['vehicleList'][0]['typeID'] = translateVehicleId($mydata['vtype']);
		
		// print_r ($leadObj) ;	//-- output the Array for testing
		
		//-- this is where we email the $leadObj to us so that we know what was created
		$po = print_r($leadObj,true);
/*
*/
		if(DEBUG_EMAILS)
			mail("code@irelocation.com","JTracker XML Lead y leadObj Array","lead_id is: " . $lead_id . "\n" . "email is: " . $email . "\n" . "campaign is: " . $campaign . "\n" . "broker is: " . $brokerId . "\n" . "campaign id: " . $campaignId . "\n".$po);		
		
		echo "lead_id is: " . $lead_id . "<br />" . "email is: " . $email . "<br />" . "campaign is: " . $campaign . "<br />" . "broker is: " . $brokerId . "<br />" . "campaign id: " . $campaignId . "<br />".$po;
	}
	
	
	//get the object from JTracker
	function getLeadObject()
	{				
		$accountclient = new soapclient(JT_LEAD_OBJECT_URL, true);	
		if (($err = $accountclient->getError()))
		{
			return "error";
		}
		else
		{
			$proxy = $accountclient->getProxy();	
			$leadObj = $proxy->getLeadObject();			
			return $leadObj;		
		}
	}	
	
	//deal with what they give us back.
	function processJTrackerResponse($status)
	{
		if ($status != 100)
		{
			return array(0,$status['faultcode']);			
			/*
			switch($status['faultcode'])
			{
				
				case 201:
					echo "Required data was missing";
					break;
				case 202:
					echo "Broker not setup for this lead source, ".
						 "please contact Jtracker.com";
					break;
				default:
					echo "Unkown Error: ".$status['faultstring']."<br/>".
							print_r($status,true).".";
					break;
			}
			*/
		}
		else
			return array(1,100);
	}
	/*
		1 Sedan Small 
		2 Sedan Large 
		3 Pickup Small 
		4 Pickup Extd. Cab 
		5 Pickup Crew Cab 
		6 SUV Small 
		7 SUV Mid-size 
		8 SUV Large 
		9 Van Mini 
		10 Van Full-size 
		11 Other 
		12 Boat 
		13 Sedan Midsize 
		14 Convertible 
		15 Pickup Full-size 
		16 Van Extd. Length 
		18 Van Pop-Top 
		19 Motorcycle 
		20 Dually 
		21 Coupe 		
	*/
	
	//our codes to theirs
	function translateVehicleId($vtype)
	{
		$vtype = strtolower($vtype);
		switch($vtype)
		{
			case "car";		
				return 1;
			case "sport utility":
				return 7;
			case "pickup":
			case "pick-up":
			case "pick-up truck":
				return 4;
			case "mini-van":
			case "van mini":
				return 9;
			case "oversize":
				return 8;
			case "van":
				return 10;
			case "convertible":
				return 14;
			case "motorcycle":
				return 19;
			case "coupe":
				return 21;
			case "sedan small":
				return 1;
			case "pickup small":
				return 3;
			case "rv":
			case "other":
			default:
				return 11;				
		}
	}


//campaign ids:
function getCampaignId($lead_id,$campaign) {
	
	//-- NEW CODE
	/* We need to pull the lead_body from the lead_format table (instead of the old way) based on the lead_id and campaign */
	
	$sql = "select lead_body from marble.lead_format where lead_id = '$lead_id' and site_id = '$campaign' ";
	echo "<br />";
	echo "getCampaignId SQL: $sql<br />";
	
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();
	$lead_body = $rs->myarray["lead_body"];
	
#mail("code@irelocation.com","lead_body","lead_body = $lead_body");
echo "<br />getCampaignId lead_body: $lead_body<br /><br />";
	
	return $lead_body;
}

/*
function getCampaignId($email)
{
	//-- THIS IS THE OLD FUNCTION
	
		#You have to use the email they are currently using for this campaign
		#you cannot group them by the sites they actually go to.. it doesn't 
		#work...
	
	list($campaign,$rest) = split("\.",$email,2);
	switch($campaign)
	{
		case "carshipping": return "f91c96e05f99001d39d1145fa6f4ab43";
		//Use autoshipping for all Top Auto Leads - auto
		case "autoshipping": return "39fb629f189a6cafb760aee661f108c5";
		case "1stmovingdirectory":
		case "1stmove": return "72b923cd21765d1cd94c7bcffe9b32fa";
		//Use auto-transport-us for all atus leads
		case "auto-transport-us": return "80ba1964eb26f42df6c900d68aa4e61e";
		case "movemycar": return "720be8b2d1ead450dd7c20e4d588a0b7";
		case "topautoshippers": return "3b4d2bb269b29ee6e1e35dd63f825e5f";

		default:	
			mail("code@irelocation.com","Unknown JTracker Campaign",$email);
			return "39fb629f189a6cafb760aee661f108c5";
			
	}
}
*/

function createJTrackerFormat($comp_id,$campaign,$leads,$month)
{
	$sql = "set @lead_id = (select lead_id from ".
			"movingdirectory.directleads where ".
			"comp_id = $comp_id limit 1);";
	$rs = new mysql_recordset($sql);
	if ($campaign == "atus")
		$sql = "insert into movingdirectory.lead_format set ";
	else 
		$sql = "insert into marble.lead_format set site_id = '$campaign', ";
	
	$sql .= " cat_id = 1, lead_type = 'jtxml', lead_body = '-1', ".
			"lead_id = @lead_id; ";
		
	$rs = new mysql_recordset($sql);
}		

function _saveId($label, $object, $jtid)
{		
	$sql = "insert into movingdirectory.jtracker (label,object,jtrackerid)
			values ('$label','$object','$id'); ";
	$rs = new mysql_recordset($sql);
	$rs->close();
}

function _getId($label,$object)
{
	$sql = " select jtrackerid from movingdirectory.jtracker where ".
			"label = '$label' and object = '$object'";
	
	$rs = new mysql_recordset($sql);
	
	if ($rs->fetch_array())
	{
		$id = $rs->myarray['jtrackerid'];
		$rs->close();
		return $id;
	}
	else
	{
		$rs->close();
		#mail("code@irelocation.com","JTracker $object id not found!","Label: $label");
		return -1;
	}
}

//broker ids:
function getBrokerId($lead_id)
{
	//these do not differ between campaigns.

	switch($lead_id)
	{	
		case 1572: return "a2ed73cba7b7e8e9dfb03328e4755f6b";
		case 1571: return "c74f86f208c466b66e5057a3c347cf78";
		case 1498: return "7480fd78fa008d144d9c48807b3ac127";
		case 1570: return "20586a3a739ead80e9f69493eb67ed01";
		case 1447: return "363d9b7a3f78df587d5439205fe36672";
		case 1382: return "f15480264a1dc6cb29938def81477d1a";
		case 1374: return "8e871a5344f8b7894d83d1468678a25a";
		case 1442: return "c55704728716728d5e1b6d3857b503a6";
		case 1406: return "a65979374013348e6ae91f46fa645fc8";
		case 1531: return "29015f00a9da788d78a5508b393b46f8";
		case 1503: return "f94c2bae8aaafd458f7f0603db2866d3";
		case 1505: return "48ff066ab205ad55235c0f9f0c2c2941";
		case 1514: return "f6c4fd691085251c24c95557d049ca01";
		case 1530: return "9dcddecd9f2c5115a5ae08c4eb9ea9f6";
		case 1535: return "13679a37fa40e7a4f55de8c0f28e7cee";
		case 1499: return "050d62e492f4a316e385619244eb3595";
		case 1469: return "acaff1358e18d252544eacf433076cb3";
		case 1544: return "9efe421f1c46681155273e0b1af46c2f";
		case 1543: return "a00e2ae8695081782d992aad1d5d0143";
		case 1542: return "c54ceee488b7c56aa5bb43207b36b0cc";
		case 1554: return "6d1d81ec27d8f59770ffe51071d911e8";
		case 1548: return "0eb3ea73ffabed8245d8d526d727eca8";
		case 1546: return "59068775455dbc875d47b26a09c4ea83";
		case 1559: return "0d339dccd78f65ccd03fe541aac2b445";		
		case 1562: return "c8e9bb3aadb43d0730e042f609251805";		
		case 1564: return "c7fb67368c25c29b9c10ca91b2d97488";
		case 1407: return "8cf0c85bf4169fb835bfcb16160fb3ca";
		case 1581: return "28189c4c61938ad0b89190b508222715";
		case 1587: return "71763b575383c89130ba12f3e9a0aecd";
		case 1586: return "95f344991aa0a3b4a258642ea00c3c1b";
		case 1591: return "0d6843d68aed72e9cc9caf759a2422bc";
		case 1593: return "c973d4a3dbba1ef55d1c6044968fc305";
		case 1598: return "13cbf9bef12f3e3681952425ce8288e5";
		case 1600: return "85c227789cd6559209ce208fd5dff10e";
		case 1601: return "b3d97746dbb45e92dc083db205e1fd14";
		case 1603: return "6b7727b566f6d41639f99bfd6fa171ff";
		case 1605: return "a4ebd6bbc93dcb7fe2450d0731e38ec0";
		case 1609: return "cd1adece1bf063e70f5ff37b135f7901";
		case 1610: return "b988295c268025b49dfb3df26171ddc3";
		case 1611: return "1143e83085fa2ca72b4caffedf08d75b";
		case 1612: return "dc4d53aa0d117d8b189b36d161af4e96";
		case 1436: return "4f6f30188f2de1a95c35fac7d89166e4";
		case 1614: return "737ce2852060b87b8ce8c66ecc1414a4";
		case 1613: return "dce60f18bdb5e85d06df9ebc14e86ce5";
		case 1615: return "f13863ed7d6b0224a0046d9646ecd02c";
		case 1620: return "d0709050622b7ee718c074cc56a0d80e";
		case 1623: return "a92560525c5361b120536648e60d7c04";
		case 1629: return "64e2f4f11ab5439b845391a6ced6118d";
		case 1642: return "d44aba297c0d3aa4ff9e0ba79ddc58fa";
		case 1644: return "e76e88ea74741e40c916450e72d95087";
		case 1646: return "da4d917f7f80013822b4f154c2dbda9e";
		case 1645: return "5a34fca1a961c85ef8011289029e549f";
		case 1650: return "b1c349de61e5ba64a64cd71e22638b07";
		case 1651: return "fa857190f356cd33139abe29d00e5ddc";
		case 1654: return "0cb8de684c142ba69955c3c646ba0a68";
		case 1657: return "b4c56f1edaae8e8ef3023769f2134e27";
		case 1689: return "0ea3cf1e1c3ced6f427846098a3613f9";
		case 1697: return "3a5f586c9f34c7a35e904f928671884d";
		case 1699: return "759373c495771bd057f1985cc7867a3e";
		case 1706: return "ac93a09161c8665184b9350bdedb8982";
		case 1707: return "e06e45334def5d7a504d8af6f90e4462";
		case 1709: return "a4293995cfbfa9ce60ce71ade2ff75f7";
		case 1711: return "b2e2bb4a5181029c567cc336f1def9f6";
		case 1414: return "903b12cac3a89bd1b3f81949e699d73f";
		case 1719: return "5c296da054f6a27bd20b0489376c1da1";
		case 1720: return "060714a3765fd7167eb5243f0a447651";
		case 1716: return "1ec3a5d181cf3a4c14341760790bc00a";
		case 1721: return "5ecf37bb121496261873c849edd657f0";
		case 1726: return "01b3f378798d72bf73c8050d76707e0a";
		case 1731: return "4c31e42a9dac82d8a249a0fe0d04764c";
		case 1732: return "2f0c8742296d08ccd1fa89967a0992f5";
		case 1736: return "a422fced429851af8d957e188dae41a7";
		case 1739: return "07cd1cda17ab189b04917c0a04360540";
		case 1742: return "34cde2eec220c2b82686d43f4e629509";
		case 1745: return "7d1c234136c12d26d6e4b5c34255c778";
		case 1746: return "1128a54246f8bb9714dce8c271c243ab";
		case 1747: return "9eb4c770a50d11a5d0570e859df1c579";
		case 1250: return "545e7ce66edc26beaa8681d5f122a9ff";
		case 1753: return "581ec7c0db8e0b655755491e069aa5cb";
		case 1755: return "36050dbf041e3c02c1de52b99c356d35";
		

		
		default:
			mail("code@irelocation.com","Unknown JTracker Broker",$lead_id);
			return "9dcddecd9f2c5115a5ae08c4eb9ea9f6";
	}
}
?>