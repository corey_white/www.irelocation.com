<?
	define('JT_LEAD_OBJECT_URL',"http://www.jtracker.com/WebServices/getLeadObject.wsdl");
	define('JT_POST_URL',"http://www.jtracker.com/WebServices/leadPosting.wsdl");
	define('JT_IRELO_SOURCE_ID',"f718ea9a978cc7f4da408087bb739c7a");
	define('JT_TEST_BROKER_ID',"5fac6b9dbf29e0ae4344ac88c1d4a968");
	define('JT_US',1);	
	define('JT_OPEN',1);
	define('JT_ENCLOSED',2);
	
	include "../nusoap/nusoap.php";
	
	function getLeadObject()
	{				
		$accountclient = new soapclient(JT_LEAD_OBJECT_URL, true);	
		echo "Client: ".$accountclient."<br/>";
		
		if (($err = $accountclient->getError()))
		{
			echo $err;
			exit();			
		}
		else
		{
			$proxy = $accountclient->getProxy();	
			$leadObj = $proxy->getLeadObject();	
			if (($err = $accountclient->getError()))
			{
				echo $err;
				exit();	
			}		
			return $leadObj;
		}
	}	
	
	 getLeadObject();
?>