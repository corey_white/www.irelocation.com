<?
/*
1stmovingdirectory.st2st@jttmail.com
1stmovingdirectory.btexpres@jttmail.com
1stmovingdirectory.tcar2@jttmail.com
1stmove.across@jttmail.com
movemycar.mred1@jttmail.com
autoshipping.caviness@jttmail.com
movemycar.mfarnham@jttmail.com
1stmove.bluestar@jttmail.com
autoshipping.stevecer@jttmail.com
autoshipping.FOOTBALL@jttmail.com
autoshipping.ttauto@jttmail.com
auto-transport-us.camirachi@jttmail.com
movemycar.sweitzer@jttmail.com
autoshipping.firstchance@jttmail.com
1stmove.aaall@jttmail.com
autoshipping.totalcar@jttmail.com
autoshipping.spauto@jttmail.com

Email Address
auto-transport-us.mfarnham@jttmail.com
topautoshippers.jayhawk2@jttmail.com

Email Address
auto-transport-us.firstchance@jttmail.com
auto-transport-us.sweitzer@jttmail.com
1stmove.across@jttmail.com
1stmove.stevecer@jttmail.com
1stmovingdirectory.FOOTBALL@jttmail.com
autoshipping.caviness@jttmail.com
autoshipping.aaall@jttmail.com
1stmove.tomlove@jttmail.com
*/

//campaign ids:
function getCampaignId($email)
{
	/*
		You have to use the email they are currently using for this campaign
		you cannot group them by the sites they actually go to.. it doesn't 
		work...
	*/	
	
	list($campaign,$rest) = split("\.",$email,2);
	switch($campaign)
	{
		case "autoshipping": return "39fb629f189a6cafb760aee661f108c5";
		case "1stmovingdirectory":
		case "1stmove": return "72b923cd21765d1cd94c7bcffe9b32fa";
		case "auto-transport-us": return "80ba1964eb26f42df6c900d68aa4e61e";
		case "movemycar": return "720be8b2d1ead450dd7c20e4d588a0b7";
		case "topautoshippers": return "3b4d2bb269b29ee6e1e35dd63f825e5f";
	}
}
//broker ids:
function getBrokerId($lead_id)
{
	switch($lead_id)
	{
		case 1447: return "363d9b7a3f78df587d5439205fe36672";
		case 1382: return "f15480264a1dc6cb29938def81477d1a";
		case 1374: return "8e871a5344f8b7894d83d1468678a25a";
		case 1442: return "c55704728716728d5e1b6d3857b503a6";
		case 1406: return "a65979374013348e6ae91f46fa645fc8";
		case 1531: return "29015f00a9da788d78a5508b393b46f8";
		case 1503: return "f94c2bae8aaafd458f7f0603db2866d3";
		case 1505: return "48ff066ab205ad55235c0f9f0c2c2941";
		case 1514: return "f6c4fd691085251c24c95557d049ca01";
		case 1530: return "9dcddecd9f2c5115a5ae08c4eb9ea9f6";
		case 1535: return "13679a37fa40e7a4f55de8c0f28e7cee";
		case 1499: return "050d62e492f4a316e385619244eb3595";
		case 1469: return "acaff1358e18d252544eacf433076cb3";
		case 1544: return "9efe421f1c46681155273e0b1af46c2f";
		case 1543: return "a00e2ae8695081782d992aad1d5d0143";
		case 1542: return "c54ceee488b7c56aa5bb43207b36b0cc";
		case 1554: return "6d1d81ec27d8f59770ffe51071d911e8";
		case 1548: return "0eb3ea73ffabed8245d8d526d727eca8";
		case 1546: return "59068775455dbc875d47b26a09c4ea83";
	}
}

echo getCampaignId("1stmove.tomlove@jttmail.com")."<br/>";
echo getCampaignId("autoshipping.ttauto@jttmail.com")."<br/>";
echo getCampaignId("1stmovingdirectory.btexpres@jttmail.com")."<br/>";

?>