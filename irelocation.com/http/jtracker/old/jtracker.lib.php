<?	
	/*
		program flow:
			get a lead object,
			populate it
			send it to jtracker
			get response code
			parse it.
			save it.
	
	
	*/
	


	define('JT_LEAD_OBJECT_URL',"http://www.jtracker.com/WebServices/getLeadObject.wsdl");
	define('JT_POST_URL',"http://www.jtracker.com/WebServices/leadPosting.wsdl");
	define('JT_IRELO_SOURCE_ID',"f718ea9a978cc7f4da408087bb739c7a");
	define('JT_TEST_BROKER_ID',"5fac6b9dbf29e0ae4344ac88c1d4a968");
	define('JT_US',1);	
	define('JT_OPEN',1);
	define('JT_ENCLOSED',2);
	
	include_once "/var/www/vhosts/irelocation.com/httpdocs/inc_mysql.php";
	
	//echo "<!--- JTracker.lib.php last modified: 6/14/2007 11:15am --->\n";
	
	function recordLeadFailure($leaddata,$lead_id,$campaign,$table)
	{
		$quote_id = $leaddata['quote_id'];
		if ( $table == "marble.auto_quotes")
			$cat_id = 1;
		else if ($table == "irelocation.leads_security")
			$cat_id = 4;
		else
			$cat_id = $leaddata['cat_id'];
			
		$sql = "insert into `movingdirectory`.`failed_leads` 
	(`quote_id`, `cat_id`, `quote_table`, `campaign_name`, 
	`lead_id`,`failed`)
	values
	('$quote_id', '$cat_id', '$table', '$campaign', '$lead_id', 
	".date("YmdHis").");";
		//mail("david@irelocation.com","JTracker Failure SQL",$sql);
		$rs = new mysql_recordset($sql);
	}
	
	function getJtrackerEmail($lead_id,$campaign)
	{
		//echo "getJtrackerEmail($lead_id,$campaign)<br/>";
		$sql = " select lead_email 'email' from marble.rules where lead_id = $lead_id ".
				" and site_id = '".$campaign."' ";
		
		//echo "JtrackerEmail->sql = '".$sql."' <br/>";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$e = $rs->myarray['email'];
		$rs->close();
		return $e;
	}
	
	function postLead($leadObj)
	{		
		$postclient = new soapclient(JT_POST_URL, true);	
		if (($err = $postclient->getError()))
		{
			return "error";
		}
		else
		{
			$post_proxy = $postclient->getProxy();	
			$status = $post_proxy->postLead($leadObj);
			return $status;
		}		
	}
	
	//reset the object to use again
	function clearLeadObject(&$leadObj)
	{	
		$leadObj['brokerID'] = "";		
		$leadObj['leadSourceID'] = "";
		$leadObj['customer']['firstName'] = "";
		$leadObj['customer']['lastName'] = "";
		$leadObj['customer']['email'] = "";
		$leadObj['customer']['phone'] = "";
		$leadObj['customer']['phone2'] = "";			
		$leadObj['customer']['address']['city'] = "";
		$leadObj['customer']['address']['stateCode'] = "";
		$leadObj['customer']['address']['zipCode'] = "";
		$leadObj['customer']['address']['countryID'] = "";
		$leadObj['shippingInfo']['estimatedShipDate'] = "";														
		$leadObj['shippingInfo']['shipViaID'] = "";
		$leadObj['shippingInfo']['vehiclesRunFlag'] = "";
		$leadObj['shippingInfo']['comment'] = "";		
		$leadObj['pickupAddress']['city'] = "";
		$leadObj['pickupAddress']['zipCode'] = "";
		$leadObj['pickupAddress']['stateCode'] = "";
		$leadObj['pickupAddress']['countryID'] = "";
		$leadObj['dropoffAddress']['city'] = "";
		$leadObj['dropoffAddress']['stateCode'] = "";
		$leadObj['dropoffAddress']['zipCode'] = "";
		$leadObj['dropoffAddress']['countryID'] = "";
		$leadObj['vehicleList'][0]['year'] = "";
		$leadObj['vehicleList'][0]['make'] = "";
		$leadObj['vehicleList'][0]['model'] = "";
		$leadObj['vehicleList'][0]['typeID'] = "";
	}
	
	function populateLeadObject(&$leadObj,$mydata,$lead_id,$campaign)
	{
		//translate this to a (??).comp_name@jttmail.com address.		
		$email = getJtrackerEmail($lead_id,$campaign);
		//extract broker id from lead id
		$brokerId = getBrokerId($lead_id);
		//extract campaign/domain from email address.
		$campaignId = getCampaignId($email);
	
		if (strlen($mydata['fname']) == 0)
			list($mydata['fname'],$mydata['lname']) = split(" ",$mydata['name'],2);
	
		if ($mydata['vtype'] == "")
		{
			//ATUS Campaign leads.. databases are differant..
			//CLEAN THis Ish UP!!!
			$mydata['phone'] = $mydata['phone_home'];
			$mydata['vtype'] = $mydata['type'];
			$mydata['vmodel'] = $mydata['model'];
			$mydata['vmake'] = $mydata['make'];
			$mydata['vyear'] = $mydata['year'];
			//2007-05-01
			$mydata['jtracker_emd'] = $mydata['est_move_date'];
		}	
	
		$leadObj['brokerID'] = $brokerId;		
		$leadObj['leadSourceID'] = $campaignId;
		$leadObj['customer']['firstName'] = $mydata["fname"];
		$leadObj['customer']['lastName'] = $mydata["lname"];
		$leadObj['customer']['email'] = $mydata['email'];
		$leadObj['customer']['phone'] = $mydata['phone'];
		
		if ($mydata['phone_alt'] != "")
			$leadObj['customer']['phone2'] = $mydata['phone_alt'];
			
		$leadObj['customer']['address']['city'] = $mydata['origin_city'];
		$leadObj['customer']['address']['stateCode'] = $mydata['origin_state'];
		$leadObj['customer']['address']['zipCode'] = $mydata['origin_zip'];
		$leadObj['customer']['address']['countryID'] = JT_US;
		$leadObj['shippingInfo']['estimatedShipDate'] = $mydata['jtracker_emd'];
														//'2008-01-31';
		$leadObj['shippingInfo']['shipViaID'] = JT_OPEN;
		$leadObj['shippingInfo']['vehiclesRunFlag'] = ($mydata['running']=='r');
		
		if ($mydata['customer_comments'] != "")
			$leadObj['shippingInfo']['comment'] = $mydata['customer_comments'];
			
		$leadObj['pickupAddress']['city'] = $mydata['origin_city'];
		$leadObj['pickupAddress']['zipCode'] = $mydata['origin_zip'];
		$leadObj['pickupAddress']['stateCode'] = $mydata['origin_state'];
		$leadObj['pickupAddress']['countryID'] = JT_US;
		$leadObj['dropoffAddress']['city'] = $mydata['destination_city'];
		$leadObj['dropoffAddress']['stateCode'] = $mydata['destination_state'];
		$leadObj['dropoffAddress']['zipCode'] = $mydata['destination_zip'];
		$leadObj['dropoffAddress']['countryID'] = JT_US;
		$leadObj['vehicleList'][0]['year'] = $mydata['vyear'];
		$leadObj['vehicleList'][0]['make'] = $mydata['vmake'];
		$leadObj['vehicleList'][0]['model'] = $mydata['vmodel'];
		$leadObj['vehicleList'][0]['typeID'] = translateVehicleId($mydata['vtype']);
			
	}
	
	
	//get the object from JTracker
	function getLeadObject()
	{				
		$accountclient = new soapclient(JT_LEAD_OBJECT_URL, true);	
		if (($err = $accountclient->getError()))
		{
			return "error";
		}
		else
		{
			$proxy = $accountclient->getProxy();	
			$leadObj = $proxy->getLeadObject();			
			return $leadObj;		
		}
	}	
	
	//deal with what they give us back.
	function processJTrackerResponse($status)
	{
		if ($status != 100)
		{
			return array(0,$status['faultcode']);			
			/*
			switch($status['faultcode'])
			{
				
				case 201:
					echo "Required data was missing";
					break;
				case 202:
					echo "Broker not setup for this lead source, ".
						 "please contact Jtracker.com";
					break;
				default:
					echo "Unkown Error: ".$status['faultstring']."<br/>".
							print_r($status,true).".";
					break;
			}
			*/
		}
		else
			return array(1,100);
	}
	/*
		1 Sedan Small 
		2 Sedan Large 
		3 Pickup Small 
		4 Pickup Extd. Cab 
		5 Pickup Crew Cab 
		6 SUV Small 
		7 SUV Mid-size 
		8 SUV Large 
		9 Van Mini 
		10 Van Full-size 
		11 Other 
		12 Boat 
		13 Sedan Midsize 
		14 Convertible 
		15 Pickup Full-size 
		16 Van Extd. Length 
		18 Van Pop-Top 
		19 Motorcycle 
		20 Dually 
		21 Coupe 		
	*/
	
	//our codes to theirs
	function translateVehicleId($vtype)
	{
		$vtype = strtolower($vtype);
		switch($vtype)
		{
			case "car";		
				return 1;
			case "sport utility":
				return 7;
			case "pickup":
			case "pick-up":
			case "pick-up truck":
				return 4;
			case "mini-van":
			case "van mini":
				return 9;
			case "oversize":
				return 8;
			case "van":
				return 10;
			case "convertible":
				return 14;
			case "motorcycle":
				return 19;
			case "coupe":
				return 21;
			case "sedan small":
				return 1;
			case "pickup small":
				return 3;
			case "rv":
			case "other":
			default:
				return 11;				
		}
	}
/*
1stmovingdirectory.st2st@jttmail.com
1stmovingdirectory.btexpres@jttmail.com
1stmovingdirectory.tcar2@jttmail.com
1stmove.across@jttmail.com
movemycar.mred1@jttmail.com
autoshipping.caviness@jttmail.com
movemycar.mfarnham@jttmail.com
1stmove.bluestar@jttmail.com
autoshipping.stevecer@jttmail.com
autoshipping.FOOTBALL@jttmail.com
autoshipping.ttauto@jttmail.com
auto-transport-us.camirachi@jttmail.com
movemycar.sweitzer@jttmail.com
autoshipping.firstchance@jttmail.com
1stmove.aaall@jttmail.com
autoshipping.totalcar@jttmail.com
autoshipping.spauto@jttmail.com

Email Address
auto-transport-us.mfarnham@jttmail.com
topautoshippers.jayhawk2@jttmail.com

Email Address
auto-transport-us.firstchance@jttmail.com
auto-transport-us.sweitzer@jttmail.com
1stmove.across@jttmail.com
1stmove.stevecer@jttmail.com
1stmovingdirectory.FOOTBALL@jttmail.com
autoshipping.caviness@jttmail.com
autoshipping.aaall@jttmail.com
1stmove.tomlove@jttmail.com
*/

//campaign ids:
function getCampaignId($email)
{
	/*
		You have to use the email they are currently using for this campaign
		you cannot group them by the sites they actually go to.. it doesn't 
		work...
	*/	
	
	list($campaign,$rest) = split("\.",$email,2);
	switch($campaign)
	{
		case "carshipping": return "f91c96e05f99001d39d1145fa6f4ab43";
		case "autoshipping": return "39fb629f189a6cafb760aee661f108c5";
		case "1stmovingdirectory":
		case "1stmove": return "72b923cd21765d1cd94c7bcffe9b32fa";
		case "auto-transport-us": return "80ba1964eb26f42df6c900d68aa4e61e";
		case "movemycar": return "720be8b2d1ead450dd7c20e4d588a0b7";
		case "topautoshippers": return "3b4d2bb269b29ee6e1e35dd63f825e5f";

		default:	
			mail("david@irelocation.com","Unknown JTracker Campaign",$email);
			return "39fb629f189a6cafb760aee661f108c5";
			
	}
}

function createJTrackerFormat($comp_id,$campaign,$leads,$month)
{
	$sql = "set @lead_id = (select lead_id from ".
			"movingdirectory.directleads where ".
			"comp_id = $comp_id limit 1);";
	$rs = new mysql_recordset($sql);
	if ($campaign == "atus")
		$sql = "insert into movingdirectory.lead_format set ";
	else 
		$sql = "insert into marble.lead_format set site_id = '$campaign', ";
	
	$sql .= " cat_id = 1, lead_type = 'jtxml', lead_body = '-1', ".
			"lead_id = @lead_id; ";
		
	$rs = new mysql_recordset($sql);
}		

function _saveId($label, $object, $jtid)
{		
	$sql = "insert into movingdirectory.jtracker (label,object,jtrackerid)
			values ('$label','$object','$id'); ";
	$rs = new mysql_recordset($sql);
	$rs->close();
}

function _getId($label,$object)
{
	$sql = " select jtrackerid from movingdirectory.jtracker where ".
			"label = '$label' and object = '$object'";
	
	$rs = new mysql_recordset($sql);
	
	if ($rs->fetch_array())
	{
		$id = $rs->myarray['jtrackerid'];
		$rs->close();
		return $id;
	}
	else
	{
		$rs->close();
		mail("david@irelocation.com","JTracker $object id not found!","Label: $label");
		return -1;
	}
}

//broker ids:
function getBrokerId($lead_id)
{
	//these do not differ between campaigns.

	switch($lead_id)
	{	
		case 1572: return "a2ed73cba7b7e8e9dfb03328e4755f6b";
		case 1571: return "c74f86f208c466b66e5057a3c347cf78";
		case 1498: return "7480fd78fa008d144d9c48807b3ac127";
		case 1570: return "20586a3a739ead80e9f69493eb67ed01";
		case 1447: return "363d9b7a3f78df587d5439205fe36672";
		case 1382: return "f15480264a1dc6cb29938def81477d1a";
		case 1374: return "8e871a5344f8b7894d83d1468678a25a";
		case 1442: return "c55704728716728d5e1b6d3857b503a6";
		case 1406: return "a65979374013348e6ae91f46fa645fc8";
		case 1531: return "29015f00a9da788d78a5508b393b46f8";
		case 1503: return "f94c2bae8aaafd458f7f0603db2866d3";
		case 1505: return "48ff066ab205ad55235c0f9f0c2c2941";
		case 1514: return "f6c4fd691085251c24c95557d049ca01";
		case 1530: return "9dcddecd9f2c5115a5ae08c4eb9ea9f6";
		case 1535: return "13679a37fa40e7a4f55de8c0f28e7cee";
		case 1499: return "050d62e492f4a316e385619244eb3595";
		case 1469: return "acaff1358e18d252544eacf433076cb3";
		case 1544: return "9efe421f1c46681155273e0b1af46c2f";
		case 1543: return "a00e2ae8695081782d992aad1d5d0143";
		case 1542: return "c54ceee488b7c56aa5bb43207b36b0cc";
		case 1554: return "6d1d81ec27d8f59770ffe51071d911e8";
		case 1548: return "0eb3ea73ffabed8245d8d526d727eca8";
		case 1546: return "59068775455dbc875d47b26a09c4ea83";
		case 1559: return "0d339dccd78f65ccd03fe541aac2b445";		
		case 1562: return "c8e9bb3aadb43d0730e042f609251805";		
		case 1564: return "c7fb67368c25c29b9c10ca91b2d97488";
		case 1407: return "8cf0c85bf4169fb835bfcb16160fb3ca";
		case 1581: return "28189c4c61938ad0b89190b508222715
";
		
		default:
			mail("david@irelocation.com","Unknown JTracker Broker",$lead_id);
			return "9dcddecd9f2c5115a5ae08c4eb9ea9f6";
	}
}
?>