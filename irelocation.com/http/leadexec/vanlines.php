<?php 

/*
This script will receive a lead from LeadExec and bounce it to Vanlines and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = $_GET['PhoneWork'];
$HomePhone = $_GET['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = $_GET['EstimatedMoveDate'];
$MoveSize =urlencode( $_GET['Bedrooms']);
$IPAddress =urlencode( $_GET['IPAddress']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize
IPAddress = $IPAddress";


//-- Build Vanlines Body
$Vanlines_body = "<VLData><Source><Company>www.irelocation.com</Company><LeadId>$leadId</LeadId><IpAddress>$IPAddress</IpAddress></Source><LeadDetail><ContactDetails><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><Email>$EmailAddress</Email><DayPhone>$WorkPhone</DayPhone><EvePhone>$HomePhone</EvePhone><BestTimeToCall></BestTimeToCall></ContactDetails><ServiceType>FullService</ServiceType><RequestDetails><MovingFrom><City>$FromCity</City><State>$FromState</State><Zip>$FromZip</Zip></MovingFrom><MovingTo><City>$ToCity</City><State>$ToState</State><Zip>$ToZip</Zip></MovingTo><TypeOfMove>$MoveSize</TypeOfMove><MovingDate>$MoveDate</MovingDate></RequestDetails><Comments>$Comments</Comments></LeadDetail></VLData>";

//-- send to Vanlines
$url = "http://www.vanlines.com/xml/receive_lead.asp"; //-- PRODUCTION
#$url = "http://www.vanlines.com/xml_test/test_lead.asp"; //-- TESTING

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($Vanlines_body));

$response = curl_exec($ch);
curl_close($ch);

#mail("rob@irelocation.com","VANLINES LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$Vanlines_body");

#echo $response;



?>