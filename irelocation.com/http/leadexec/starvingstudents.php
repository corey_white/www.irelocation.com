<?php

//-- This script will receive leads from Starving Students (SS), wash the data and then send it into LE via their affiliate account.

/*
Here is a sample GET string - to be sent to SS:

http://irelocation.com/leadexec/starvingstudents.php?movedate=05/31/2012&firstname=Tess&lastname=Ting&email=Tess.Ting%40gmail.com&movetype=LOCAL&fromcity=ALAMEDA&fromstate=CA&fromzip=94501&tocity=CONCORD&tostate=CA&tozip=94521&homephone=480-555-1234&bedrooms=3

*/

if(!$_GET){
    die("Please send your variables via GET");
}

//-- Grab and Sanitize the fields
$movedate = preg_replace('/[^0-9\/]/', '', $_GET['movedate']);
$firstname = trim( ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_GET['firstname']) ) ) );
$lastname = trim( ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_GET['lastname']) ) ) );
$email = strtolower( preg_replace('/[^A-Za-z0-9\\.@-_]/', '', $_GET['email']) );
$movetype = trim( ucwords( strtolower( preg_replace('/[^A-Za-z]/', '', $_GET['movetype']) ) ) );
$fromcity = ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_GET['fromcity']) ) );
$fromstate = strtoupper( preg_replace('/[^A-Za-z]/', '', $_GET['fromstate']) );
$fromzip = preg_replace('/[^0-9]/', '', $_GET['fromzip']);
$tocity = ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_GET['tocity']) ) );
$tostate = strtoupper( preg_replace('/[^A-Za-z]/', '', $_GET['tostate']) );
$tozip = preg_replace('/[^0-9]/', '', $_GET['tozip']);
$homephone = preg_replace('/[^0-9]/', '', $_GET['homephone']);
$homephone = substr($homephone,0,3) . "-" . substr($homephone,3,3) . "-" . substr($homephone,6,4)  ;
$bedrooms = preg_replace('/[^0-9]/', '', $_GET['bedrooms']);
$remoteip = "99.99.99.99";


echo "movedate = $movedate<br />";
echo "firstname = $firstname<br />";
echo "lastname = $lastname<br />";
echo "email = $email<br />";
echo "movetype = $movetype<br />";
echo "fromcity = $fromcity<br />";
echo "fromstate = $fromstate<br />";
echo "fromzip = $fromzip<br />";
echo "tocity = $tocity<br />";
echo "tostate = $tostate<br />";
echo "tozip = $tozip<br />";
echo "homephone = $homephone<br />";
echo "bedrooms = $bedrooms<br />";
echo "remoteip = $remoteip<br />";



//-- Calculate No. Bedrooms and Poundage
switch ( $bedrooms ) {
    case "1":
        $poundage = "4000";
        break;
    case "2":
        $poundage = "6500";
        break;
    case "3":
        $poundage = "9000";
        break;
    case "4":
        $poundage = "12000";
        break;
    case "5":
        $poundage = "Over 12000";
       break;
    case "6":
        $poundage = "Over 12000";
       break;
    case "7":
        $poundage = "Over 12000";
       break;
    case "8":
        $poundage = "Over 12000";
       break;
    case "9":
        $poundage = "Over 12000";
       break;
    case "10":
        $poundage = "Over 12000";
       break;
}

//-- fix the USA country issue
$origin_country = "US";
$destination_country = "US";

//-- local lead override to put into the new local moving vertical
if ( $movetype == "Local" ) {
    $cat_id = 22;
    $VID = 4442;
    $LID = 1672;
    $AID = 8501;
} else { //-- Long Distance
    $cat_id = 2;
    $VID = 4442;
    $LID = 370;
    $AID = 8500;
}



//-- This is the GET VAR string - IMPORTANT:: The first 3 variables are NOT interchangable with other campaigns.  You need to change them for the campaign you are setting up.
$LEAD_BODY = 
"VID=$VID&".
"AID=$AID&".
"LID=$LID&".
"quote_id=$quote_id&".
"cat_id=$cat_id&".
"source=starvingstudents&".
"FirstName=$firstname&".
"LastName=$lastname&".
"email=$email&".
"phone_home=$homephone&".
"contact=phone&".
"est_move_date=$movedate&".
"number_bedrooms=$bedrooms&".
"poundage=".urlencode($poundage)."&".
"origin_city=$fromcity&".
"origin_state=$fromstate&".
"origin_zip=$fromzip&".
"origin_country=$origin_country&".
"destination_city=$tocity&".
"destination_state=$tostate&".
"destination_zip=$tozip&".
"destination_country=$destination_country&".
"comments=&".
"remote_ip=$remoteip";


//-- send into LE
$POST_URL = "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver";			//-- Posting URL

$ch=curl_init($POST_URL."?".$LEAD_BODY);	
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
$response = curl_exec($ch);
curl_close($ch);

$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
echo "<br />-------------------------<br />LeadExec Repsonse <br />Url: ".$POST_URL."?".$LEAD_BODY." <br/>-------------------------<br/>Response: $response<br/>-------------------------<br />";

/*
if (substr_count(strtolower($response),"lead was accepted") == 1) { 

	#mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN","$content");
	#save_leadexec_response($QUOTE_ID, "Lead Accepted :: $response",1,"$CAMPAIGN");

} else {
	
	mail("rob@irelocation.com","Starving Student LeadExec Repsonse Failure - Cat_ID: $cat_id","$content \n\n$response");
	#save_leadexec_response($QUOTE_ID, "Failure - $response",0,"$CAMPAIGN");

}

*/

?>