<?php
/**
 * Forwards soap request for Security Networks
 */

if(!$_POST){
    die("Post your variables please");
}

$xml = '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <SaveLead xmlns="http://ws.securitynetworks.com/">
      <UserID>'.$_POST['UserID'].'</UserID>
      <Password>'.$_POST['Password'].'</Password>
      <lead>
        <LeadID>0</LeadID>
        <ErrorMessage>FAIL</ErrorMessage>
        <FirstName>'.$_POST['FirstName'].'</FirstName>
        <LastName>'.$_POST['LastName'].'</LastName>
        <StreetNumber></StreetNumber>
        <StreetName>'.$_POST['StreetAddress'].'</StreetName>
        <City>'.$_POST['City'].'</City>
        <State>'.$_POST['State'].'</State>
        <Zip>'.$_POST['ZipCode'].'</Zip>
        <Phone1>'.$_POST['Phone'].'</Phone1>
        <SSN></SSN>
        <DOB></DOB>
        <Email>'.$_POST['Email'].'</Email>
        <SpouseFirstName></SpouseFirstName>
        <SpouseLastName></SpouseLastName>
        <SpouseSSN></SpouseSSN>
        <SpouseDOB></SpouseDOB>
        <SpouseEmail></SpouseEmail>
        <StreetDirection></StreetDirection>
        <StreetType></StreetType>
        <Apt></Apt>
        <Phone2></Phone2>
        <Phone3></Phone3>
        <HomeOwnership>'.$_POST['OwnRent'].'</HomeOwnership>
        <CreditEstimate></CreditEstimate>
        <Comments>'.$_POST['Comments'].'</Comments>
      </lead>
    </SaveLead>
  </soap:Body>
</soap:Envelope>';

$lines = explode("\n", $xml);
foreach ($lines as $row => $line) {
    $lines[$row] = trim($line);
}

$xml = "" . implode('', $lines);

$header = array(
    "Content-Type: text/xml; charset=utf-8",
    "Accept: text/xml", 
    "Cache-Control: no-cache", 
    "Pragma: no-cache",
    "SOAPAction: \"http://ws.securitynetworks.com/SaveLead\"",
    "Content-length: ".strlen($xml)
);

try{
    // create a new cURL resource
    #$ch = curl_init("https://dev.affiliateservices.securitynetworks.net:1443/ws/Leads.asmx"); //-- Dev
    $ch = curl_init("https://affiliateservices.securitynetworks.net:444/ws/Leads.asmx"); //-- Production
    
    // set URL and other appropriate options
    
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    
    // grab URL and pass it to the browser
    $response = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo "curl error ";
        echo curl_errno($ch);
    }
    
    if(substr_count($response, "LeadID") == 0){ //Lead not accepted
        mail("jon@irelocation.com, rob@irelocation.com", "Security Networks Lead Rejected", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response."\n\n".curl_errno($ch));
    }
    
    // close cURL resource, and free up system resources
    curl_close($ch);
    
}
catch(Exception $e){
    mail("rob@irelocation.com,jon@irelocation.com", "Security Networks cURL Exception", "EXCEPTION:\n".print_r($e,true)."\n\nOPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
    print_r($e);
}

echo $response;