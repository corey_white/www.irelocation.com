<?php
/* 
************************FILE INFORMATION**********************
* File Name:  starvingstudentscsv.php
**********************************************************************
* Description:  This script will take a Starving Students CSV file, process it and post into their LeadExec account for bot Local and Domestic Long Distance Moving Leads.
**********************************************************************
* Creation Date:  5/29/12
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

$POST_URL = "https://secure.leadexec.net/LeadImport.asmx/LeadReceiver";			//-- Posting URL

//-- Connect to Db for zip lookups
define("MYSQL_HOST", "72.3.226.111");
define("MYSQL_USER", "irelo_remote");
define("MYSQL_PASS", "holf9ik9hyn2uf");
define("MYSQL_DB", "irelocation");
$link = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS);
mysql_select_db(MYSQL_DB);

$zip_error = 0;


if ( $_POST['process_file'] == "yes" ) {
	
	//-- Check that file exists
	if( !is_uploaded_file($_FILES['userfile']['tmp_name'] )  ) {
	echo "no file, action halted.";
	exit();
	}
	
	//-- Check that file has data
	if( $_FILES['userfile']['size'] == 0 ){
	echo "Nothing was received in your file.";
	exit();
	}
	
	
	$row = 1;
	if (($handle = fopen($_FILES['userfile']['tmp_name'], "r")) !== FALSE) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$num = count($data);
			if ( $num != 15 ) {
				exit("There are too many fields in this file.  Please fix and try uploading again.");
			} else {
				echo "<p> $num fields in line $row: <br /></p>\n";
			}
			
			
/*
			for ($c=0; $c < $num; $c++) {
				echo $data[$c] . "<br />\n";
			}
*/
			
			//-- Date
			$dateC = explode(" ",$data[0]);
			$movedate = preg_replace('/[^0-9\/]/', '', $dateC[0]);
			
			//-- Names
			$firstname = urlencode(trim( ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $data[1]) ) ) ) );
			$lastname = urlencode(trim( ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $data[2]) ) ) ) );
			if ( !$firstname ) {
				$firstname = $lastname;
			} 
			
			if ( !$lastname ) {
				$lastname = $firstname;
			} 
			
			//-- Email
			$email = strtolower( preg_replace('/[^A-Za-z0-9\\.@-_]/', '', $data[3]) );
			
			//-- type of move
			if ( strtolower($data[4]) == "local" ) {
				$cat_id = ss22;
				$VID = 4442;
				$LID = 1672;
				$AID = 8501;
			} else {
				$cat_id = ss2;
				$VID = 4442;
				$LID = 370;
				$AID = 8500;
			}
			

			//-- origin
			$origin_city = urlencode(ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $data[5]) ) ) );
			$origin_state = urlencode(strtoupper( preg_replace('/[^A-Za-z]/', '', $data[6]) ) );
			$origin_zip = urlencode(preg_replace('/[^0-9]/', '', $data[7]) );
			
			//-- Retreive city and state if either is missing
			if ( strtolower($origin_city) == "cwa" || strtolower($origin_state) == "cwa" ) {
				$sql = "select city,state from movingdirectory.zip_codes where zip = '$origin_zip' limit 1";
				$result = mysql_query($sql);
				$num_rows = mysql_num_rows($result);
				
				//-- If we can't find a zip, we can't pull the city and state, so we skip this at the end and not send to LeadExec
				if ( $num_rows == 0 ) {
						$zip_error = 1;			
				}
				
				$row = mysql_fetch_row($result);
				$origin_city = $row[0];
				$origin_state = $row[1];
			} 
			
			if ( strtolower($origin_address) == "cwa" ) {
				$origin_address = urlencode("Ask Client");
			} 
			
			//-- Destination
			$destination_city = urlencode(ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $data[10]) ) ) );
			$destination_state = urlencode(strtoupper( preg_replace('/[^A-Za-z]/', '', $data[11]) ) );
			$destination_zip = urlencode(preg_replace('/[^0-9]/', '', $data[12]) );
			
			//-- Retreive city and state if either is missing
			if ( strtolower($destination_city) == "cwa" || strtolower($destination_state) == "cwa" ) {
				$sql = "select city,state from movingdirectory.zip_codes where zip = '$destination_zip' limit 1";
				$result = mysql_query($sql);
				$num_rows = mysql_num_rows($result);
				
				//-- If we can't find a zip, we can't pull the city and state, so we skip this at the end and not send to LeadExec
				if ( $num_rows == 0 ) {
						$zip_error = 1;			
				}
				
				$row = mysql_fetch_row($result);
				$destination_city = $row[0];
				$destination_state = $row[1];
			} 

			if ( strtolower($destination_address) == "cwa" ) {
				$destination_address = urlencode("Ask Client");
			} 
			
			//-- phone
			$homephone = preg_replace('/[^0-9]/', '', $data[13]);
			$homephone = urlencode(substr($homephone,0,3) . "-" . substr($homephone,3,3) . "-" . substr($homephone,6,4)  );
			
			//-- number of bedrooms
			$bedrooms = preg_replace('/[^0-9]/', '', $data[14]);
			
			if ($bedrooms == 0) { $poundage = 1500; }
			if ($bedrooms == 1) { $poundage = 4000; }
			if ($bedrooms == 2) { $poundage = 6500; }
			if ($bedrooms == 3) { $poundage = 9000; }
			if ($bedrooms == 4) { $poundage = 12000; }
			if ($bedrooms >= 5) { $poundage = "Over 12000"; }
			
			//-- Fake the IP as they don't capture it
			$remoteip = "99.99.99.99";


			if ( $movedate != "movdt" ) {
/*
				echo "Move Date = $movedate<br />\n";
				echo "Name = $firstname $lastname<br />\n";
				echo "email = $email<br />\n";
				echo "cat_id = $cat_id<br />\n";
				echo "origin_city = $origin_city<br />\n";
				echo "origin_state = $origin_state<br />\n";
				echo "origin_zip = $origin_zip<br />\n";
				echo "destination_city = $destination_city<br />\n";
				echo "destination_state = $destination_state<br />\n";
				echo "destination_zip = $destination_zip<br />\n";
				echo "homephone = $homephone<br />\n";
				echo "bedrooms = $bedrooms<br />\n";
				echo "poundage = $poundage<br />\n";
*/
				
				echo "Processing:<br />$cat_id, $movedate, $firstname $lastname, $email; FROM: $origin_city, $origin_state, $origin_zip; TO: $destination_city, $destination_state, $destination_zip, $homephone, $bedrooms BR, $poundage pounds.<br />";
				
				echo "<br />";
				
				if ( $row > 1 && $zip_error == 0 ) {

/*
*/
					//-- Generate post
					$LEAD_BODY = 
					"VID=$VID&".
					"AID=$AID&".
					"LID=$LID&".
					"cat_id=$cat_id&".
					"source=starvingstudents&".
					"FirstName=$firstname&".
					"LastName=$lastname&".
					"email=$email&".
					"phone_home=$homephone&".
					"contact=phone&".
					"est_move_date=$movedate&".
					"number_bedrooms=$bedrooms&".
					"poundage=".urlencode($poundage)."&".
					"origin_city=$origin_city&".
					"origin_state=$origin_state&".
					"origin_zip=$origin_zip&".
					"destination_city=$destination_city&".
					"destination_state=$destination_state&".
					"destination_zip=$destination_zip&".
					"remote_ip=99.99.99.99";
					

					//-- Send to LE
					$ch=curl_init($POST_URL."?".$LEAD_BODY);	
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
					$response = curl_exec($ch);
					curl_close($ch);
					
					$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
					
					if ( substr_count(strtolower($response),"lead was accepted") == 1 ) {
						echo "Accepted by LeadExec<br />";
						echo "$response";
						echo "<br />";
					} else {
						mail("rob@irelocation.com","Starving Student LeadExec Repsonse Failure - Cat_ID: $cat_id","$content \n\n$response");
						echo "Error detected with Post<br />";
						echo "<pre>$content</pre>";
						echo "<br />";
					}

				} else {
					echo "There is an error in this line of data.  Skipping.<br />";
				}

			}

			echo "<hr />";
			
			$zip_error = 0;
			$row++;

		}
		fclose($handle);
	}



} else {
    
	echo "Starving Students CSV Upload
			<br /><br />
         <form enctype=\"multipart/form-data\" method=\"post\" action=\"$PHP_SELF\">
          <input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"10000000\">
          File to Upload:<br>
          <input type=\"file\" class=\"box\" name=\"userfile\">
          <br><br>
          <input type=\"hidden\" name=\"process_file\" value=\"yes\">
          <input type=\"submit\" class=\"box\" value=\"Upload file\">
        </form>";
        
}

?>