<?php
/*
This uses the API from LeadExec to pull the Delivery Account info for a client. This is faster than looking up each delivery account separately.
Running this at: http://www.irelocation.com/leadexec/getclientdata.php
*/

#error_reporting(E_ALL); //-- show runtime errors
error_reporting(0); //-- show no errors


require( "xmlLib.php" );

echo "<form name=\"input\" action=\"getclientdata.php\" method=\"get\">Client UID: <input type=\"text\" name=\"uid\"><input type=\"submit\" value=\"Submit\"></form><hr>";

//-- this part pulls the clients for a specific lead.

$ClientUID = $_GET[uid]; //-- a client

if ( !$ClientUID ) {
    exit();
} 

//-- get XML data from Clickpoint
#$xml = new XMLToArray( xmldata (string), ignorefields (array 1,2,3), replacefields(array OLD => NEW), show attributes?, convert to upper );
$xml = new XMLToArray( "https://api.leadexec.net/service.asmx/GetClientAccounts?Key=69EE07F8CF8C98117418A913187E017E7A208AB99D529C77&ClientUID=$ClientUID", array(), array( "ClientAccountType" => "_array_" ), true, false );

#print_r( $xml->getArray() );

$myarray = $xml->getArray();

#echo $myarray[ArrayOfClientAccountType][ClientAccountType][0][ClientAccountUID];

$CountArr = count($myarray[ArrayOfClientAccountType][ClientAccountType]);

echo "Client UID $ClientUID has $CountArr Delivery Accounts:<br>";



foreach($myarray[ArrayOfClientAccountType][ClientAccountType] as $key => $value){

	$acct_num =  intval($key)+1;
	
	# $myarray[ArrayOfClientAccountType][ClientAccountType][$key][XX]
	
	$automatedX = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][AutomationEnabled];
	if ( $automatedX == "true") {
		$automated = "Yes";
	} else {
		$automated = "<b>No</b>";
	}
	
	$accountDesc = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][Description];
	$dAcctID = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][ClientAccountUID];
	
	$leadPrice = number_format($myarray[ArrayOfClientAccountType][ClientAccountType][$key][Price],2);
	$hourly = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][HourMax];
	$daily = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][DayMax];
	$monthly = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][MonthMax];
	
	$delivMethod = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][DeliveryDetails][Name];
	$toAddress = $myarray[ArrayOfClientAccountType][ClientAccountType][$key][DeliveryDetails][tAddress];
	
	//-- Check for -1 in caps
	if ( $hourly == -1 ) {
		$hourly = 0;
	} 
	
	if ( $daily == -1 ) {
		$daily = 0;
	} 
	
	if ( $monthly == -1 ) {
		$monthly = 0;
	} 
	
	echo "--------------------------------------------------------------------------------<br />";
	echo "Delivery Account: $accountDesc<br/>";
	echo "Account Automated: $automated<br/>";
	echo "<br /><br />";
	
	echo "Delivery Account ID: $dAcctID<br />";
	echo "Lead Price: $leadPrice<br /><br />";
	
	echo "Lead Caps: <br />";
	echo "&nbsp;&nbsp;&nbsp;Hourly: $hourly<br />";
	echo "&nbsp;&nbsp;&nbsp;Daily: $daily<br />";
	echo "&nbsp;&nbsp;&nbsp;Monthly: $monthly<br />";
	
	echo "<br /><br />";
	
	echo "Delivery Method Details:<br />";
	echo "Delivery Used: $delivMethod<br />";
	
	if ( $toAddress ) {
		echo "Sending To: $toAddress<br />";
	} 

	#	echo "XX:<br />";



	// output everything:
	/*
		echo "<pre>";
		print_r($value, false);
		echo "</pre><br />";
	*/

}



?>
