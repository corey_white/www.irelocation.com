<?php
/**
 * Forwards request for granot
 * 
 * @since 2011-09-14 jolawski Make sure the moving date is in the future
 * @since 2011-09-14 jolawski Added emailer if "ccemail" is posted
 */

//SAMPLE POST
/*
$_POST = array(
    'servtypeid'    => '101',
    'leadno'        => '1234',
    'firstname'     => 'Joe',
    'lastname'      => 'Customer',
    'ocity'         => 'Phoenix',
    'ostate'        => 'AZ',
    'ozip'          => '85044',
    'dcity'         => 'Pittsburgh',
    'dstate'        => 'PA',
    'dzip'          => '15212',
    'weight'        => '1000',
    'volume'        => '1',
    'movesize'      => 'PartialHome',
    'notes'         => 'Test',
    'movedte'       => '12/12/2011',
    'email'         => 'test@test.com',
    'phone1'        => '4805551212',
    'phone2'        => '',
    'cell'          => '',
    'source'        => 'test',
    'mincount'      => '0',
    'maxcount'      => '0',
    'soldcount'     => '0',
    'moverref'      => 'noreply@granot.com',
    'redirurl'      => ''
);
*/

if(!$_POST){
    die("Post your variables please");
}

define("API_ID", "DFAB8417F966");
define("HTTP_URI", "https://gmove.granot.com/bin/wc.dll?lidgw~LEADSGWHTTP~");
define("AUTO_URI", "https://gmove.granot.com/bin/wc.dll?lidgw~LEADSGWHTTP~");

$auto=false;
switch($_POST['cat_id']){
    case '2': //long
        $servtypeid = 102;
        break;
    case '22': //local
        $servtypeid = 101;
        break;
    case 'auto': //auto
        $servtypeid = 103;
        $auto=true;
        break;
    case '3': //international
        $servtypeid = 104;
        break;
    default:
        die("Invalid cat_id");
}

$fields = array(
    'servtypeid'    => $servtypeid,
    'leadno'        => (isset($_POST['leadno']) ? $_POST['leadno'] : '' ),
    'firstname'     => (isset($_POST['firstname']) ? $_POST['firstname'] : '' ),
    'lastname'      => (isset($_POST['lastname']) ? $_POST['lastname'] : '' ),
    'ocity'         => (isset($_POST['ocity']) ? $_POST['ocity'] : '' ),
    'ostate'        => (isset($_POST['ostate']) ? $_POST['ostate'] : '' ),
    'ozip'          => (isset($_POST['ozip']) ? $_POST['ozip'] : '' ),
    'dcity'         => (isset($_POST['dcity']) ? $_POST['dcity'] : '' ),
    'dstate'        => (isset($_POST['dstate']) ? $_POST['dstate'] : '' ),
    'dzip'          => (isset($_POST['dzip']) ? $_POST['dzip'] : '' ),
    'notes'         => (isset($_POST['notes']) ? $_POST['notes'] : '' ),
    'movedte'       => (isset($_POST['movedte']) ? $_POST['movedte'] : '' ),
    'email'         => (isset($_POST['email']) ? $_POST['email'] : '' ),
    'phone1'        => (isset($_POST['phone1']) ? $_POST['phone1'] : '' ),
    'phone2'        => (isset($_POST['phone2']) ? $_POST['phone2'] : '' ),
    'cell'          => (isset($_POST['cell']) ? $_POST['cell'] : '' ),
    'source'        => (isset($_POST['source']) ? $_POST['source'] : '' ),
    'mincount'      => (isset($_POST['mincount']) ? $_POST['mincount'] : '' ),
    'maxcount'      => (isset($_POST['maxcount']) ? $_POST['maxcount'] : '' ),
    'soldcount'     => (isset($_POST['soldcount']) ? $_POST['soldcount'] : '' ),
    'moverref'      => (isset($_POST['moverref']) ? $_POST['moverref'] : (isset($_POST['MOVERREF']) ? $_POST['MOVERREF'] : '')),
    'redirurl'      => (isset($_POST['redirurl']) ? $_POST['redirurl'] : '')
);

if($servtypeid == 101 || $servtypeid == 102){ //Local & Long Distance
    $fields['weight'] = $_POST['weight'];
    $fields['volume'] = $_POST['volume'];
    $fields['movesize'] = $_POST['movesize'];
}
elseif($servtypeid == 103){ //Auto
    $fields['make0'] = $_POST['make0'];
    $fields['model0'] = $_POST['model0'];
    $fields['year0'] = $_POST['year0'];
    $fields['autotype0'] = $_POST['autotype0'];
    $fields['running0'] = $_POST['running0'];
}
elseif($servtypeid == 104){ //International
    $fields['ocountry'] = $_POST['ocountry'];
    $fields['dcountry'] = $_POST['dcountry'];
    $fields['weight'] = $_POST['weight'];
    $fields['movesize'] = $_POST['movesize'];
}

//@since 2011-09-14 jolawski Make sure the moving date is in the future
list($month, $day, $year) = explode('/', $fields['movedte']);
if(mktime(0,0,0,$month,$day,$year) < time()){
    $fields['movedte'] = date("m/d/Y", time()+(86400*7));
}

//@since 2011-09-14 jolawski Email a copy of the lead
if($_POST['ccemail'] != ''){

    if ( $servtypeid == 103 ) { //-- Auto lead
            $msg = "Quote ID: ".$_POST['leadno']."
    Received: ".date("m/d/Y")."
    Source: iRelocation
    First Name: ".$_POST['firstname']."
    Last Name: ".$_POST['lastname']."
    Email: ".$_POST['email']."
    Phone 1: ".$_POST['phone1']."
    Phone 2: ".$_POST['phone2']."
    Cell: ".$_POST['cell']."
    Estimated Move Date: ".$_POST['movedte']."
    Origin City: ".$_POST['ocity']."
    Origin State: ".$_POST['ostate']."
    Origin Zip: ".$_POST['ozip']."
    Destination City: ".$_POST['dcity']."
    Destination State: ".$_POST['dstate']."
    Destination Zip: ".$_POST['dzip']."
    Comments: ".$_POST['notes']."
    Make: ".$_POST['make0']."
    Model: ".$_POST['model0']."
    Year: ".$_POST['year0']."
    Type: ".$_POST['autotype0']."
    Running: ".$_POST['running0']."";
    
        
    mail($_POST['ccemail'], "Top Auto Leads", $msg, "From: topauto@irelocation.com");
    //mail("jon@irelocation.com, rob@irelocation.com", "COPY Top Auto Leads", $msg, "From: topauto@irelocation.com");
    
        
    } else { //-- Moving lead
            $msg = "Quote ID: ".$_POST['leadno']."
    Received: ".date("m/d/Y")."
    Source: iRelocation
    First Name: ".$_POST['firstname']."
    Last Name: ".$_POST['lastname']."
    Email: ".$_POST['email']."
    Phone 1: ".$_POST['phone1']."
    Phone 2: ".$_POST['phone2']."
    Cell: ".$_POST['cell']."
    Estimated Move Date: ".$_POST['movedte']."
    Origin City: ".$_POST['ocity']."
    Origin State: ".$_POST['ostate']."
    Origin Zip: ".$_POST['ozip']."
    Origin Country: ".$_POST['ocountry']."
    Destination City: ".$_POST['dcity']."
    Destination State: ".$_POST['dstate']."
    Destination Zip: ".$_POST['dzip']."
    Destination Country: ".$_POST['dcountry']."
    Comments: ".$_POST['notes']."
    Weight: ".$_POST['weight']."
    Move Size: ".$_POST['movesize']."";
    
        
    mail($_POST['ccemail'], "Top Moving Leads", $msg, "From: topmoving@irelocation.com");
    //mail("jon@irelocation.com, rob@irelocation.com", "COPY Top Moving Leads", $msg, "From: topmoving@irelocation.com");
    
    }
}


try{
    // create a new cURL resource
    $ch = curl_init();
    
    // set URL and other appropriate options
    $options = array(
        //CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HEADER => false,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_POSTFIELDS => $fields,
        CURLOPT_URL => ($auto ? AUTO_URI : HTTP_URI)."&API_ID=".API_ID
    );
    
    curl_setopt_array($ch, $options);
    
    // grab URL and pass it to the browser
    $response = curl_exec($ch);
    
    // close cURL resource, and free up system resources
    curl_close($ch);
    
    //Email test for auto leads
/*
    if($servtypeid == 103){
        mail("rob@irelocation.com,jon@irelocation.com", "Granot Auto Leads", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
    }
*/
    
    if(substr_count($response, ",OK") == 0){ //Lead not accepted
        mail("rob@irelocation.com,jon@irelocation.com", "Granot Lead Rejected", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
    }
    
}
catch(Exception $e){
    mail("rob@irelocation.com,jon@irelocation.com", "Granot cURL Exception", "EXCEPTION:\n".print_r($e,true)."\n\nOPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
    print_r($e);
}

echo $response;