<?php
/* 
************************FILE INFORMATION**********************
* File Name:  xmlLibTest.php
**********************************************************************
* Description:  Takes a LeadID from LeadExec and find the companies that received the lead, and pulls their Company Name, Phone and Email from Clickpoint servers.  Information is returned in an array.
**********************************************************************
* Creation Date:  8/8/11 - Rob Hanus
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

#error_reporting(E_ERROR | E_WARNING | E_PARSE); //-- show runtime errors
error_reporting(0); //-- show no errors


require( "xmlLib.php" );

//-- this part pulls the clients for a specific lead.

#$LeadUID = "11401148"; //-- this lead id should only pull 1 client
#$LeadUID = "11401163"; //-- this lead id should 8 clients
#$LeadUID = "11401880"; //-- this lead id should 5 clients
$LeadUID = "11401932"; //-- this lead id should 10 clients

//-- get XML data from Clickpoint
$xml = new XMLToArray( "https://api.leadexec.net/service.asmx/GetClientsForSend?Key=69EE07F8CF8C98117418A913187E017E7A208AB99D529C77&LeadUID=$LeadUID", array(), array( "ClientSendInfo" => "_array_" ), true, false );

$myarray = $xml->getArray();
/*
echo "<pre>";
print_r($myarray);
echo "</pre><br />";

*/

$CountArr = count($myarray[ArrayOfClientSendInfo][ClientSendInfo]);
echo "CountArr = $CountArr<br /><br />";

//-- Assign Companies ID into array
$company = array();
for ( $i = 0; $i < $CountArr; $i++ ) {
	$company[$i] = $myarray[ArrayOfClientSendInfo][ClientSendInfo][$i][ClientUID];
}


/*
//-- used for debugging
for ( $i = 0; $i < $CountArr; $i++ ) {
    echo "Company $i = $company[$i]<br />";
}
echo "<pre>";
print_r( $xml->getArray() );
echo "</pre><br />";
*/


//-- This part pulls the company name, email and phone from clickpoint based on client ID
$companyName = array();
$companyPhone = array();
$companyEmail = array();

foreach($company as $key => $value){
    $xml = new XMLToArray( "https://api.leadexec.net/service.asmx/GetClientData?Key=69EE07F8CF8C98117418A913187E017E7A208AB99D529C77&ClientUID=$value", array(), array( "ClientFieldType" => "_array_" ), true, false );
	$myarray = $xml->getArray();

	$companyName[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][0][FieldValue];
	$companyPhone[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][6][FieldValue]; //-- this is the Alternate Phone field
	$companyEmail[$key] = $myarray[ArrayOfClientFieldType][ClientFieldType][3][FieldValue];
}


for ( $i = 0; $i < $CountArr; $i++ ) {
	echo "Company $i Name = $companyName[$i] <br />";
	echo "Company $i Phone = $companyPhone[$i] <br />";
	echo "Company $i Email = $companyEmail[$i] <br />";
	echo "<br />";
}



/*
//-- used for debugging
echo "<pre>";
print_r( $xml->getArray() );
echo "</pre><br />";
*/

?>
