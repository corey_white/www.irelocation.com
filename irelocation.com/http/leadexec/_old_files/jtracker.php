<?php

/*
This script will receive a lead from LeadExec and bounce it to JTracker and output the reponse so it's picked up by LE.  The posting code is using what JTracker sent us directly.
*/

define('JT_LEAD_OBJECT_URL',"http://www.jtracker.com/WebServices/getLeadObject.wsdl");
define('JT_POST_URL',"http://www.jtracker.com/WebServices/leadPosting.wsdl");
define('JT_IRELO_SOURCE_ID',"f718ea9a978cc7f4da408087bb739c7a");
define('JT_TEST_BROKER_ID',"5fac6b9dbf29e0ae4344ac88c1d4a968");
define('JT_US',1);	
define('JT_OPEN',1);
define('JT_ENCLOSED',2);


$ClientBrokerID = urlencode($_GET['ClientBrokerID']);
$IreloBrokerID = urlencode($_GET['IreloBrokerID']);
$FirstName = urlencode($_GET['fname']);
$LastName = urlencode($_GET['lanme']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['origin_city']);
$State = urlencode($_GET['origin_state']);
$Zipcode = urlencode($_GET['origin_zip']);
$HomePhone = $_GET['phone'];
$EmailAddress = $_GET['email'];
$FromCity = urlencode($_GET['origin_city']);
$FromState = urlencode($_GET['origin_state']);
$FromZip = urlencode($_GET['origin_zip']);
$FromCountry = "";
$ToCity = urlencode($_GET['destination_city']);
$ToState = urlencode($_GET['destination_state']);
$ToZip = urlencode($_GET['destination_zip']);
$ToCountry = "";
$MoveDate = $_GET['est_move_date'];
$VehicleYear = $_GET['vyear'];
$VehicleMake = $_GET['vmake'];
$VehicleModel = $_GET['vmodel'];
$VehicleType = $_GET['vtype'];

$MSG_BODY = "Data from LE:\n\n
ClientBrokerID = $ClientBrokerID
IreloBrokerID = $IreloBrokerID
FirstName = $FirstName
LastName = $LastName
HomePhone = $HomePhone
EmailAddress = $EmailAddress
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
MoveDate = $MoveDate
VehicleYear = $VehicleYear
VehicleMake = $VehicleMake
VehicleModel = $VehicleModel
VehicleType = $VehicleType";


/**
 * Sample SOAP Client in PHP
 * 
 * The SOAP Web Service will publish the following APIs:
 *  postLead(JTrackerLead lead) : Takes in a JTrackerLead Object and attempts to post it
 *  getLeadObject() : Returns a valid JTrackerLead Object
 *  getAddressObject() : Returns a valid JTrackerAddress Object
 *  getCustomerObject() : Returns a valid JTrackerCustomer Object
 *  getShippingInfoObject() : Returns a valid JTrackerShippingInfo Object
 *  getVehicleObject() : Returns a valid JTrackerVehicle Object
 *  The get functions are there to make it easier on users of the APIs to build a valid JTrackerLead Object for submitting to the postLead function.
 * 
 */
// Client 1 is for getting valid objects for posting
$client1 = new soapclient(JT_LEAD_OBJECT_URL, true);	
 
// Client 2 is for posting leads
$client2 = new soapclient(JT_POST_URL, true);	
 
// Get Lead Object
$leadObj = $client1->getLeadObject();
 
// Populate Lead Object
$leadObj->brokerID = '$ClientBrokerID';
$leadObj->leadSourceID = '$IreloBrokerID';
$leadObj->customer->firstName = '$FirstName';
$leadObj->customer->lastName = '$LastName';
$leadObj->customer->email = '$EmailAddress';
$leadObj->customer->phone = '$HomePhone';
$leadObj->customer->address->city = '$City';
$leadObj->customer->address->stateCode = '$State';
$leadObj->customer->address->zipCode = '$Zipcode';
$leadObj->customer->address->countryID = 1;
$leadObj->shippingInfo->estimatedShipDate = '$MoveDate';
$leadObj->shippingInfo->shipViaID = 1;
$leadObj->shippingInfo->vehiclesRunFlag = true;
$leadObj->pickupAddress->city = '$FromCity';
$leadObj->pickupAddress->stateCode = $FromState;
$leadObj->pickupAddress->zipCode = '$FromZip';
$leadObj->pickupAddress->countryID = 1;
$leadObj->dropoffAddress->city = '$ToCity';
$leadObj->dropoffAddress->stateCode = $ToState;
$leadObj->dropoffAddress->zipCode = '$ToZip';
$leadObj->dropoffAddress->countryID = 1;
$leadObj->vehicleList[0]->year = '$VehicleYear';
$leadObj->vehicleList[0]->make = '$VehicleMake';
$leadObj->vehicleList[0]->model = '$VehicleModel';
$leadObj->vehicleList[0]->typeID = $VehicleType;
 
/*
// Add Second Vehicle
$vehicleObj = $client1->getVehicleObject();
$vehicleObj->year = '';
$vehicleObj->make = '';
$vehicleObj->model = '';
$vehicleObj->typeID = 1;
*/
 
#$leadObj->vehicleList[1] = $vehicleObj;
 
// Post Lead
$status = $client2->postLead($leadObj);
 
if (is_a($status, 'SoapFault')) {
	echo "An error occured";
	mail("rob@irelocation.com","JTRACKER LEADEXEC SEND ERROR","$MSG_BODY \n\n Response: $status");
} else {

// Available Status codes are as follows:
// 100 - Success
// 201 - Message Errors Found / Required Data Missing
// 203 - Invalid Lead Source
// 204 - Invalid Broker ID

	switch($status) {
		case 100:
			$msg =  "Everything went fine";
			break;
		case 201:
			$msg =  "Required data was missing";
			break;
		case 203:
			$msg =  "Invalid Lead Source, please contact Jtracker.com";
			break;
		case 204:
			$msg =  "Invalid Broker ID, please contact Jtracker.com";
			break;
	}
	
	mail("rob@irelocation.com","JTRACKER LEADEXEC SEND MSG","$MSG_BODY \n\n Response: $msg");
	echo "$msg";
}
?>

