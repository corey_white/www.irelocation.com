<?php
/*
 * Forwards soap request for Security Networks
 */

if(!$_POST){
    die("Post your variables please");
}

$FirstName = urlencode($_POST['FirstName']);
$LastName = urlencode($_POST['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_POST['OriginCity']);
$State = urlencode($_POST['OriginState']);
$Zipcode = urlencode($_POST['OriginZip']);
$WorkPhone = $_POST['PhoneWork'];
$HomePhone = $_POST['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_POST['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_POST['Comments']);
$leadId =urlencode( $_POST['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_POST['OriginCity']);
$FromState = urlencode($_POST['OriginState']);
$FromZip = urlencode($_POST['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_POST['DestinationCity']);
$ToState = urlencode($_POST['DestinationState']);
$ToZip = urlencode($_POST['DestinationZip']);
$ToCountry = "";
$MoveDate = $_POST['EstimatedMoveDate'];
$MoveSize =urlencode( $_POST['Bedrooms']);
$IPAddress =urlencode( $_POST['IPAddress']);
$PricePoint =urlencode( $_POST['PricePoint']);

//-- urldecode for email values
$AFirstName = urldecode($FirstName);
$ALastName = urldecode($LastName);
$AEmailAddress = urldecode($EmailAddress);
$AFromCity = urldecode($FromCity);
$AFromState = urldecode($FromState);
$AFromZip = urldecode($FromZip);
$AToCity = urldecode($ToCity);
$AToState = urldecode($ToState);
$AToZip = urldecode($ToZip);




function value_in($element_name, $xml, $content_only = true) {
    if ($xml == false) {
        return false;
    }
    $found = preg_match('#<'.$element_name.'(?:\s+[^>]+)?>(.*?)'.
            '</'.$element_name.'>#s', $xml, $matches);
    if ($found != false) {
        if ($content_only) {
            return $matches[1];  //ignore the enclosing tags
        } else {
            return $matches[0];  //return the full pattern match
        }
    }
    // No match found: return false.
    return false;
}

$MoveSizeX = urldecode($MoveSize);

/*
echo "Move Date: $MoveDate<br />";
echo "Move Size: $MoveSizeX<br />";
echo "Origin Zip: $FromZip<br />";
echo "Destination Zip: $ToZip<br />";
echo "<br />";
*/

$xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <soap:Body>
    <FullServiceZipToZipStringDate xmlns=\"http://tempuri.org/\">
      <date>$MoveDate</date>
      <moveSize>$MoveSizeX</moveSize>
      <originationZip>$FromZip</originationZip>
      <destinationZip>$ToZip</destinationZip>
      <domain>www.irelocation.com</domain>
    </FullServiceZipToZipStringDate>
  </soap:Body>
</soap:Envelope>";

#echo $xml;
#echo "<br />";

$lines = explode("\n", $xml);
foreach ($lines as $row => $line) {
    $lines[$row] = trim($line);
}

$xml = "" . implode('', $lines);

$header = array(
    "Content-Type: text/xml; charset=utf-8",
    "Accept: text/xml", 
    "Cache-Control: no-cache", 
    "Pragma: no-cache",
#    "SOAPAction: \"http://webservices.relocation.com/pricing.asmx\"",
    "Content-length: ".strlen($xml)
);

try{
    // create a new cURL resource
    #$ch = curl_init("zxzxzxz"); //-- Dev
    $ch = curl_init("http://webservices.relocation.com/pricing.asmx"); //-- Production
    
    // set URL and other appropriate options
    
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    
    // grab URL and pass it to the browser
    $response = curl_exec($ch);
    
    if (curl_errno($ch)) {
        echo "curl error ";
        echo curl_errno($ch);
    }
    
/*
    if(substr_count($response, "LeadID") == 0){ //Lead not accepted
        mail("rob@irelocation.com", "Ping Post Lead Rejected", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response."\n\n".curl_errno($ch));
    }
*/
    
    // close cURL resource, and free up system resources
    curl_close($ch);
    
}
catch(Exception $e){
    mail("rob@irelocation.com", "Security Networks cURL Exception", "EXCEPTION:\n".print_r($e,true)."\n\nOPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
    print_r($e);
}



$price = value_in('FullServiceZipToZipStringDateResult', $response);

if ( $PricePoint >= $price ) { //-- no joy
    echo "Too Low: $price";
    mail("anna@irelocation.com,rob@irelocation.com","Relocation Price Too Low","Price Too Low \n$AFirstName $ALastName, $AEmailAddress \nFrom: $AFromCity, $AFromState $AFromZip \nTo: $AToCity, $AToState $AToZip \nPrice we want: $PricePoint \nOffered Price: $price");
} else { //-- meets minimum pricing point for us
    
	$Vanlines_body = "<VLData><Source><Company>www.irelocation.com</Company><LeadId>$leadId</LeadId><IpAddress>$IPAddress</IpAddress></Source><LeadDetail><ContactDetails><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><Email>$EmailAddress</Email><DayPhone>$WorkPhone</DayPhone><EvePhone>$HomePhone</EvePhone><BestTimeToCall></BestTimeToCall></ContactDetails><ServiceType>FullService</ServiceType><RequestDetails><MovingFrom><City>$FromCity</City><State>$FromState</State><Zip>$FromZip</Zip></MovingFrom><MovingTo><City>$ToCity</City><State>$ToState</State><Zip>$ToZip</Zip></MovingTo><TypeOfMove>$MoveSize</TypeOfMove><MovingDate>$MoveDate</MovingDate></RequestDetails><Comments>$Comments</Comments></LeadDetail></VLData>";
	
	//-- send to Vanlines -- switched to production server May 14, 2012 15:58 - Rob
	$url = "http://www.vanlines.com/xml/receive_lead.asp"; //-- PRODUCTION 
	#$url = "http://www.vanlines.com/xml_test/test_lead.asp"; //-- TESTING
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	
	curl_setopt($ch, CURLOPT_POSTFIELDS, trim($Vanlines_body));
	
	$response = curl_exec($ch);
	curl_close($ch);

    echo "OK : $price : $response";

	#mail("anna@irelocation.com","Relocation Price Matched","Success! \nPrice we want: $PricePoint \nOffered Price: $price");
    mail("anna@irelocation.com,rob@irelocation.com","Relocation Price Matched","Success!  \n$AFirstName $ALastName, $AEmailAddress \nFrom: $AFromCity, $AFromState $AFromZip \nTo: $AToCity, $AToState $AToZip \nPrice we want: $PricePoint \nOffered Price: $price");

}

?>