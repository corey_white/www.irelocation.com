<?php
/**
 * Forwards request for granot
 */

//SAMPLE POST
/*
$_POST = array(
	'servtypeid' 	=> '101',
	'leadno' 		=> '1234',
	'firstname' 	=> 'Joe',
	'lastname' 		=> 'Customer',
	'ocity' 		=> 'Phoenix',
	'ostate' 		=> 'AZ',
	'ozip' 			=> '85044',
	'dcity' 		=> 'Pittsburgh',
	'dstate' 		=> 'PA',
	'dzip' 			=> '15212',
	'weight' 		=> '1000',
	'volume' 		=> '1',
	'movesize' 		=> 'PartialHome',
	'notes' 		=> 'Test',
	'movedte' 		=> '12/12/2011',
	'email' 		=> 'test@test.com',
	'phone1' 		=> '4805551212',
	'phone2' 		=> '',
	'cell' 			=> '',
	'source' 		=> 'test',
	'mincount' 		=> '0',
	'maxcount' 		=> '0',
	'soldcount' 	=> '0',
	'moverref' 		=> 'noreply@granot.com',
	'redirurl' 		=> ''
);
*/

if(!$_POST){
	die("Post your variables please");
}

define("API_ID", "DFAB8417F966");
define("HTTP_URI", "https://gmove.granot.com/bin/wc.dll?lidgw~LEADSGWHTTP~");
define("AUTO_URI", "https://gmove.granot.com/bin/wc.dll?lidgw~LEADSGWHTTP~");

$auto=false;
switch($_POST['cat_id']){
	case '2': //long
		$servtypeid = 102;
		break;
	case '22': //local
		$servtypeid = 101;
		break;
	case 'auto': //auto
		$servtypeid = 103;
		$auto=true;
		break;
	case 'YY': //international
		$servtypeid = 104;
		break;
	default:
		die("Invalid cat_id");
}

$fields = array(
	'servtypeid' 	=> $servtypeid,
	'leadno' 		=> $_POST['leadno'],
	'firstname' 	=> $_POST['firstname'],
	'lastname' 		=> $_POST['lastname'],
	'ocity' 		=> $_POST['ocity'],
	'ostate' 		=> $_POST['ostate'],
	'ozip' 			=> $_POST['ozip'],
	'dcity' 		=> $_POST['dcity'],
	'dstate' 		=> $_POST['dstate'],
	'dzip' 			=> $_POST['dzip'],
	'notes' 		=> $_POST['notes'],
	'movedte' 		=> $_POST['movedte'],
	'email' 		=> $_POST['email'],
	'phone1' 		=> $_POST['phone1'],
	'phone2' 		=> $_POST['phone2'],
	'cell' 			=> $_POST['cell'],
	'source' 		=> $_POST['source'],
	'mincount' 		=> $_POST['mincount'],
	'maxcount' 		=> $_POST['maxcount'],
	'soldcount' 	=> $_POST['soldcount'],
	'moverref' 		=> ($_POST['moverref'] ? $_POST['moverref'] : $_POST['MOVERREF']),
	'redirurl' 		=> $_POST['redirurl']
);

if($servtypeid == 101 || $servtypeid == 102){ //Local & Long Distance
	$fields['weight'] = $_POST['weight'];
	$fields['volume'] = $_POST['volume'];
	$fields['movesize'] = $_POST['movesize'];
}
elseif($servtypeid == 103){ //Auto
	$fields['make0'] = $_POST['make0'];
	$fields['model0'] = $_POST['model0'];
	$fields['year0'] = $_POST['year0'];
	$fields['autotype0'] = $_POST['autotype0'];
	$fields['running0'] = $_POST['running0'];
}
elseif($servtypeid == 104){ //International
	$fields['ocountry'] = $_POST['ocountry'];
	$fields['dcountry'] = $_POST['dcountry'];
}

try{
	// create a new cURL resource
	$ch = curl_init();
	
	// set URL and other appropriate options
	$options = array(
		//CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HEADER => false,
		CURLOPT_POST => true,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_POSTFIELDS => $fields,
		CURLOPT_URL => ($auto ? AUTO_URI : HTTP_URI)."&API_ID=".API_ID
	);
	
	curl_setopt_array($ch, $options);
	
	// grab URL and pass it to the browser
	$response = curl_exec($ch);
	
	// close cURL resource, and free up system resources
	curl_close($ch);
	
	//Email test for auto leads
	if($servtypeid == 103){
		mail("rob@irelocation.com,jon@irelocation.com", "Granot Auto Leads", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
	}
	
	if(substr_count($response, ",OK") == 0){ //Lead not accepted
		mail("rob@irelocation.com,jon@irelocation.com", "Granot Lead Rejected", "OPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
	}
	
}
catch(Exception $e){
	mail("rob@irelocation.com,jon@irelocation.com", "Granot cURL Exception", "EXCEPTION:\n".print_r($e,true)."\n\nOPTIONS:\n".print_r($options,true)."\n\nRESPONSE: \n".$response);
	print_r($e);
}

echo $response;