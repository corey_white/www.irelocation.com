<?php 

/*
This script will receive a lead from LeadExec and bounce it to Wheaton and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = urlencode($_GET['PhoneWork']);
$HomePhone = urlencode($_GET['PhoneHome']);
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = urlencode($_GET['EstimatedMoveDate']);
$MoveSize =urlencode( $_GET['Bedrooms']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";


//-- Build Wheaton Body
$wheaton_body = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><MoverLead version=\"1.0\" source=\"irelocation\"><UserContact><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><Address1>NA</Address1><Address2></Address2><City>$City</City><State>$State</State><Zipcode>$Zipcode</Zipcode><WorkPhone>$WorkPhone</WorkPhone><HomePhone>$HomePhone</HomePhone><CellPhone></CellPhone><EmailAddress>$EmailAddress</EmailAddress><BestTimeCall></BestTimeCall><ContactAtWork></ContactAtWork><Comments>$Comments</Comments><leadId>$leadId</leadId></UserContact><MoveDetails><MoveType>Full-Service</MoveType><FromCity>$FromCity</FromCity><FromState>$FromState</FromState><FromZip>$FromZip</FromZip><FromCountry></FromCountry><ToCity>$ToCity</ToCity><ToState>$ToState</ToState><ToZip>$ToZip</ToZip><ToCountry></ToCountry><MoveDate>$MoveDate</MoveDate><MoveSize>$MoveSize</MoveSize></MoveDetails></MoverLead>";

//-- send to Wheaton
$url = "http://access.wheatonworldwide.com/InternetLeads/external";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array("User-Agent: Mozilla/4.0", "Content-type: text/xml","REFERER: irelocation.com"));
#curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($wheaton_body));

$response = curl_exec($ch);
curl_close($ch);


mail("rob@irelocation.com","WHEATON LEADEXEC TEST","$MSG_BODY \n\n Response: $response \n\n$wheaton_body");

echo $response;


/* WHEATON FORMAT from old system
url:http://access.wheatonworldwide.com/InternetLeads/external
CURLOPT_HEADER:1
CURLOPT_HTTPHEADER:Content-type: text/xml,REFERER: irelocation.com,User-Agent: Mozilla/4.0
CURLOPT_USERAGENT: Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)
CURLOPT_TIMEOUT: 3000
CURLOPT_CUSTOMREQUEST: POST
validation: movewheaton
---------------
<?xml version="1.0" encoding="utf-8" ?><InternetLead version="1.0" source="irelocation"><Contact><LeadId>||quote_id||</LeadId><FirstName>||firstname||</FirstName><LastName>||lastname||</LastName><Address1>NA</Address1><Address2>NA</Address2><City>||xml_origin_city||</City><State>||origin_state||</State><Zipcode>||origin_zip||</Zipcode><EmailAddress>||email||</EmailAddress><ContactPreference>||contact||</ContactPreference><WorkPhone>||phone_work_dashes||</WorkPhone><HomePhone>||phone_dashes||</HomePhone><CellPhone>NA</CellPhone><BestTimeToCall>NA</BestTimeToCall><Comments>||xml_wheaton_comments||</Comments></Contact><MoveDetails><MoveType>Full-Service</MoveType><FromCity>||xml_origin_city||</FromCity><FromState>||origin_state||</FromState><FromZip>||origin_zip||</FromZip><FromCountry>||origin_country||</FromCountry><ToCity>||xml_destination_city||</ToCity><ToState>||destination_state||</ToState><ToZip>||destination_zip||</ToZip><ToCountry>||destination_country||</ToCountry><MoveDate>||emonth||/||eday||/||eyear||</MoveDate><MoveSize>||wheaton_furnished_rooms||</MoveSize></MoveDetails></InternetLead>


$FirstName
$LastName
$Address1
$Address2
$City
$State
$Zipcode
$WorkPhone
$HomePhone
$CellPhone
$EmailAddress
$BestTimeCall
$ContactAtWork
$Comments
$leadId
$MoveType
$FromCity
$FromState
$FromZip
$FromCountry
$ToCity
$ToState
$ToZip
$ToCountry
$MoveDate
$MoveSize

<?xml version="1.0" encoding="utf-8" ?><MoverLead version="1.0" source="irelocation"><UserContact><FirstName>[FirstName]</FirstName><LastName>[LastName]</LastName><Address1>NA</Address1><Address2></Address2><City>[OriginCity]</City><State>[OriginState]</State><Zipcode>[OriginZip]</Zipcode><WorkPhone>[PhoneWork]</WorkPhone><HomePhone>[PhoneHome]</HomePhone><CellPhone></CellPhone><EmailAddress>[Email]</EmailAddress><BestTimeCall></BestTimeCall><ContactAtWork></ContactAtWork><Comments>[Comments]</Comments><leadId>[QuoteID]</leadId></UserContact><MoveDetails><MoveType>Full-Service</MoveType><FromCity>[OriginCity]</FromCity><FromState>[OriginState]</FromState><FromZip>[OriginZip]</FromZip><FromCountry></FromCountry><ToCity>[DestinationCity]</ToCity><ToState>[DestinationState]</ToState><ToZip>[DestinationZip]</ToZip><ToCountry></ToCountry><MoveDate>[EstimatedMoveDate]</MoveDate><MoveSize>[Bedrooms]</MoveSize></MoveDetails></MoverLead>

*/


?>