<?php 

/*
This script will receive a lead from LeadExec and bounce it to SIRVA and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "Unknown";
$Address2 = "Unknown";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = $_GET['PhoneWork'];
$HomePhone = $_GET['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = $_GET['EstimatedMoveDate'];
$MoveSize =urlencode( $_GET['Bedrooms']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";


//-- Build SIRVA Body
$sirva_body = "xmlRequest=<EstimateRequest><ReferralCode>ASLAVLAGT</ReferralCode><PrimaryContact><Email>$EmailAddress</Email><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><PrimaryPhoneType>H</PrimaryPhoneType><PreferredContactTime>E</PreferredContactTime><HomePhone>$HomePhone</HomePhone><WorkPhone>$WorkPhone</WorkPhone><WorkPhoneExt></WorkPhoneExt><CellPhone></CellPhone><FaxPhone></FaxPhone></PrimaryContact><PickupAddress><Address1>$Address1</Address1><Address2></Address2><City>$City</City><State>$State</State><Zip>$Zipcode</Zip></PickupAddress><MoveDetails><PickupZip>$FromZip</PickupZip><DeliveryZip>$ToZip</DeliveryZip><MoveDate>$MoveDate</MoveDate><DwellingType>H3</DwellingType><AmountOfFurnishings>M</AmountOfFurnishings><PickupShuttle>N</PickupShuttle><DeliveryShuttle>N</DeliveryShuttle></MoveDetails><EstimateDetails><HasVehicles>N</HasVehicles><RequestedEstimateDate>1W</RequestedEstimateDate><RequestedEstimateTimeOfDay>E</RequestedEstimateTimeOfDay><SpecialtyItems></SpecialtyItems><Comments>$Comments</Comments></EstimateDetails><HTTPReferrer></HTTPReferrer><HTTPUserAgent></HTTPUserAgent><VendorId>IRELO</VendorId><OptInForSpecialOffers></OptInForSpecialOffers></EstimateRequest>";

//-- send to SIRVA
$url = "http://ballpark.imove.com/RAE/RequestAnEstimate.asmx/SendEstimateRequestXmlString";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: 100'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://ballpark.imove.com/RAE/SendEstimateRequestXml"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($sirva_body));

$response = curl_exec($ch);
curl_close($ch);


mail("rob@irelocation.com","SIRVA LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$sirva_body");

echo $response;



?>