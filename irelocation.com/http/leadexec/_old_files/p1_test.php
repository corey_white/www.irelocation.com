<?php 


/// Pull in the NuSOAP code
require_once('../nusoap/nusoap.php');

// Create the client instance
$client = new soapclient('http://www.protectionone.com/prospectupdateTEST/Pro1.asmx');


// Check for an error
$err = $client->getError();
if ($err) {
    // Display the error
    echo '<p><b>Constructor error: ' . $err . '</b></p>';
    // At this point, you know the call that follows will fail
} else {
	echo "no error";
}

#$validate = $client->call("ValidateEntity", $params, $options);

#$validate = $client->call('AuthHeaders', array('Username' => 'pro1'));
#$validate = $client->call('AuthHeaders', array('Password' => 'pro1'));

#$result = $client->call('Prospect', array('Name' => 'Test Lead'));

$authparams = array();
$authparams["Username"] = "pro1";
$authparams["Password"] = "pro1";

$result = $client->AuthHeaders($authparams);


$params = array();
$params["Name"] = "Test";

$result = $client->Pro1Soap($authparams);


print_r($result);

/*
<?xml version="1.0" encoding="utf-8"?>
<wsdl:definitions xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:tm="http://microsoft.com/wsdl/mime/textMatching/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/" xmlns:mime="http://schemas.xmlsoap.org/wsdl/mime/" xmlns:tns="http://tempuri.org/ProspectUpdateDMZ/Pro1" xmlns:s="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://schemas.xmlsoap.org/wsdl/soap12/" xmlns:http="http://schemas.xmlsoap.org/wsdl/http/" targetNamespace="http://tempuri.org/ProspectUpdateDMZ/Pro1" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">
  <wsdl:types>
    <s:schema elementFormDefault="qualified" targetNamespace="http://tempuri.org/ProspectUpdateDMZ/Pro1">
      <s:element name="ProtectionOne">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="Prospect" type="tns:Prospect" />
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:complexType name="Prospect">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Site_type" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Address1" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Address2" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="City" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="State" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Zip" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Phone1" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Ext1" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Phone2" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Ext2" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Email" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Comments" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Com_System_Type" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="promo_id" type="s:string" />
        </s:sequence>
      </s:complexType>
      <s:element name="ProtectionOneResponse">
        <s:complexType>
          <s:sequence>
            <s:element minOccurs="0" maxOccurs="1" name="ProtectionOneResult">
              <s:complexType>
                <s:sequence>
                  <s:element ref="s:schema" />
                  <s:any />
                </s:sequence>
              </s:complexType>
            </s:element>
          </s:sequence>
        </s:complexType>
      </s:element>
      <s:element name="AuthHeaders" type="tns:AuthHeaders" />
      <s:complexType name="AuthHeaders">
        <s:sequence>
          <s:element minOccurs="0" maxOccurs="1" name="Username" type="s:string" />
          <s:element minOccurs="0" maxOccurs="1" name="Password" type="s:string" />
        </s:sequence>
        <s:anyAttribute />
      </s:complexType>
    </s:schema>
  </wsdl:types>
  <wsdl:message name="ProtectionOneSoapIn">
    <wsdl:part name="parameters" element="tns:ProtectionOne" />
  </wsdl:message>
  <wsdl:message name="ProtectionOneSoapOut">
    <wsdl:part name="parameters" element="tns:ProtectionOneResponse" />
  </wsdl:message>
  <wsdl:message name="ProtectionOneAuthHeaders">
    <wsdl:part name="AuthHeaders" element="tns:AuthHeaders" />
  </wsdl:message>
  <wsdl:portType name="Pro1Soap">
    <wsdl:operation name="ProtectionOne">
      <wsdl:documentation xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/">Creates a prospect using the P1 Prospect  object</wsdl:documentation>
      <wsdl:input message="tns:ProtectionOneSoapIn" />
      <wsdl:output message="tns:ProtectionOneSoapOut" />
    </wsdl:operation>
  </wsdl:portType>
  <wsdl:binding name="Pro1Soap" type="tns:Pro1Soap">
    <soap:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="ProtectionOne">
      <soap:operation soapAction="http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne" style="document" />
      <wsdl:input>
        <soap:body use="literal" />
        <soap:header message="tns:ProtectionOneAuthHeaders" part="AuthHeaders" use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:binding name="Pro1Soap12" type="tns:Pro1Soap">
    <soap12:binding transport="http://schemas.xmlsoap.org/soap/http" />
    <wsdl:operation name="ProtectionOne">
      <soap12:operation soapAction="http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne" style="document" />
      <wsdl:input>
        <soap12:body use="literal" />
        <soap12:header message="tns:ProtectionOneAuthHeaders" part="AuthHeaders" use="literal" />
      </wsdl:input>
      <wsdl:output>
        <soap12:body use="literal" />
      </wsdl:output>
    </wsdl:operation>
  </wsdl:binding>
  <wsdl:service name="Pro1">
    <wsdl:port name="Pro1Soap" binding="tns:Pro1Soap">
      <soap:address location="http://p1interneta.pro1sdmz.local/prospectupdateTEST/Pro1.asmx" />
    </wsdl:port>
    <wsdl:port name="Pro1Soap12" binding="tns:Pro1Soap12">
      <soap12:address location="http://p1interneta.pro1sdmz.local/prospectupdateTEST/Pro1.asmx" />
    </wsdl:port>
  </wsdl:service>
</wsdl:definitions>
*/
?>