<?php 

/*
This script will receive a lead from LeadExec and bounce it to Protection1 and output the reponse so it's picked up by LE.
*/

$Site_type = urlencode($_GET['COM']);
$Name = urlencode($_GET['TEST LEAD']);
$Address1 = urlencode($_GET['1313 Mockingbird Lane']);
$City = urlencode($_GET['Chandler']);
$State = urlencode($_GET['AZ']);
$Zip = urlencode($_GET['85226']);
$Phone1 = urlencode($_GET['480-785-7400']);
$Email = urlencode($_GET['rob@irelocation.com']);
$Comments = urlencode($_GET['None']);
#$XXX = urlencode($_GET['XXX']);


/*
$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";
*/


//-- Build P1 Body
$p1_body = "xmlRequest=<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header><AuthHeaders xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\"><Username>pro1</Username><Password>pro1</Password></AuthHeaders></soap:Header><soap:Body><ProtectionOne xmlns=\"http://tempuri.org/ProspectUpdateDMZ/Pro1\"><Prospect><Site_type>$Site_type</Site_type><Name>$Name</Name><Address1>$Address1</Address1><Address2></Address2><City>$City</City><State>$State</State><Zip>$Zip</Zip><Phone1>$Phone1</Phone1><Ext1></Ext1><Phone2></Phone2><Ext2></Ext2><Email>$Email</Email><Comments>$Comments</Comments><Com_System_Type></Com_System_Type><promo_id>TSCOM</promo_id></Prospect></ProtectionOne></soap:Body></soap:Envelope>";


//-- send to P1
$url = "http://www.protectionone.com/prospectupdateTEST/Pro1.asmx";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/soap+xml; charset=utf-8', 'Content-length: 250'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($p1_body));

$response = curl_exec($ch);
curl_close($ch);


mail("rob@irelocation.com","Protection1 LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$p1_body");

echo $response;



?>