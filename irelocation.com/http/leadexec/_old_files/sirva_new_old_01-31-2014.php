<?php 

/*
This script will receive a lead from LeadExec and bounce it to SIRVA and output the reponse so it's picked up by LE.

January 25, 2013 15:43 - Rob - Had to remove the urlencode from the date and email, as it's coming from LE already encoded.
*/

$ReferralCode = urlencode($_GET['ReferralCode']);
#$Email = urlencode($_GET['Email']);
$Email = $_GET['Email'];
$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$PrimaryPhoneType = urlencode($_GET['PrimaryPhoneType']);
$PreferredContactTime = urlencode($_GET['PreferredContactTime']);
$HomePhone = $_GET['HomePhone'];
$WorkPhone = $_GET['WorkPhone'];
$Address1 = urlencode($_GET['Address1']);
$City = urlencode($_GET['City']);
$State = urlencode($_GET['State']);
$Zip = urlencode($_GET['Zip']);
$PickupZip = urlencode($_GET['Zip']);
$DeliveryZip = urlencode($_GET['DeliveryZip']);
#$MoveDate = urlencode($_GET['MoveDate']);
$MoveDate = $_GET['MoveDate'];
$DwellingType = urlencode($_GET['DwellingType']);
$AmountOfFurnishings = urlencode($_GET['AmountOfFurnishings']);
$PickupShuttle = urlencode($_GET['PickupShuttle']);
$DeliveryShuttle = urlencode($_GET['DeliveryShuttle']);
$HasVehicles = urlencode($_GET['HasVehicles']);
$RequestedEstimateDate = urlencode($_GET['RequestedEstimateDate']);
$RequestedEstimateTimeOfDay = urlencode($_GET['RequestedEstimateTimeOfDay']);
$VendorId = urlencode($_GET['VendorId']);
$OptInForSpecialOffers = urlencode($_GET['OptInForSpecialOffers']);
$ThirdPartyOptIn = urlencode($_GET['ThirdPartyOptIn']);
$ccemail = urlencode($_GET['ccemail']);


$MSG_BODY = "
FirstName = $FirstName
LastName = $LastName
PrimaryPhoneType = $PrimaryPhoneType
PreferredContactTime = $PreferredContactTime
HomePhone = $HomePhone
Email = $Email
Address1 = $Address1
City = $City
State = $State
Zip = $Zip
PickupZip = $PickupZip
DeliveryZip = $DeliveryZip
MoveDate = $MoveDate
DwellingType = $DwellingType
AmountOfFurnishings = $AmountOfFurnishings
PickupShuttle = $PickupShuttle
DeliveryShuttle = $DeliveryShuttle
HasVehicles = $HasVehicles
RequestedEstimateDate = $RequestedEstimateDate
RequestedEstimateTimeOfDay = $RequestedEstimateTimeOfDay
VendorId = $VendorId
OptInForSpecialOffers = $OptInForSpecialOffers
ThirdPartyOptIn = $ThirdPartyOptIn";

$MSG_BODY = urldecode($MSG_BODY);

//-- Build SIRVA Body
$sirva_body = "xmlRequest=<EstimateRequest><ReferralCode>$ReferralCode</ReferralCode><PrimaryContact><Email>$Email</Email><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><PrimaryPhoneType>$PrimaryPhoneType</PrimaryPhoneType><PreferredContactTime>$PreferredContactTime</PreferredContactTime><HomePhone>$HomePhone</HomePhone><WorkPhone>$WorkPhone</WorkPhone><WorkPhoneExt></WorkPhoneExt><CellPhone></CellPhone><FaxPhone></FaxPhone></PrimaryContact><PickupAddress><Address1>$Address1</Address1><City>$City</City><State>$State</State><Zip>$Zip</Zip></PickupAddress><MoveDetails><PickupZip>$PickupZip</PickupZip><DeliveryZip>$DeliveryZip</DeliveryZip><MoveDate>$MoveDate</MoveDate><DwellingType>$DwellingType</DwellingType><AmountOfFurnishings>$AmountOfFurnishings</AmountOfFurnishings><PickupShuttle>$PickupShuttle</PickupShuttle><DeliveryShuttle>$DeliveryShuttle</DeliveryShuttle></MoveDetails><EstimateDetails><HasVehicles>$HasVehicles</HasVehicles><RequestedEstimateDate>$RequestedEstimateDate</RequestedEstimateDate><RequestedEstimateTimeOfDay>$RequestedEstimateTimeOfDay</RequestedEstimateTimeOfDay><Comments></Comments></EstimateDetails><HTTPReferrer></HTTPReferrer><HTTPUserAgent></HTTPUserAgent><VendorId>$VendorId</VendorId><OptInForSpecialOffers>$OptInForSpecialOffers</OptInForSpecialOffers><ThirdPartyOptIn>$ThirdPartyOptIn</ThirdPartyOptIn><VendorDateTime></VendorDateTime><PromotionCode></PromotionCode><ReferrerFirstName></ReferrerFirstName><ReferrerLastName></ReferrerLastName><ReferrerEmail></ReferrerEmail><TCPAOptIn>N</TCPAOptIn></EstimateRequest>";

//-- send to SIRVA
$url = "http://ballparktest.allied.com/RAE/RequestAnEstimate.asmx/SendEstimateRequestXmlString"; //--test
#$url = "http://ballpark.allied.com/sirvaxmlfeed/requestanestimate.asmx/SendEstimateRequestXmlString"; //-- production

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: 100'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://ballpark.imove.com/RAE/SendEstimateRequestXml"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, trim($sirva_body));

$response = curl_exec($ch);
curl_close($ch);

//mail("rob@irelocation.com,tory@irelocation.com","SIRVA LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$sirva_body");

$pattern = '/\<MoveId\>\d+\<\/MoveId\>/';
if ( $ccemail && preg_match($pattern, $response) ) { //-- also send an email copy if there's a success response
#   mail("rob@irelocation.com","Top Moving Lead Email Copy","$MSG_BODY","From: TopMoving@irelocation.com");
    mail("$ccemail,rob@irelocation.com","Top Moving Lead Email Copy","$MSG_BODY","From: TopMoving@irelocation.com");
} 

echo $response;
?>