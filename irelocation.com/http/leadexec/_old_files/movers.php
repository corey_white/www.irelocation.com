<?php 

/*
This script will receive a lead from LeadExec and bounce it to Movers.com and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = $_GET['PhoneWork'];
$HomePhone = $_GET['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = $_GET['EstimatedMoveDate'];
$MoveSize =urlencode( $_GET['Bedrooms']);
$RemoteIP =urlencode( $_GET['remote_ip']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";


//-- Build Movers.com Body
$movers_body = "xmlData=<?xml version=\"1.0\" encoding=\"utf-8\" ?><moverscom><partnercode>MOVERS109-b1f7f64f2086fc9a</partnercode><personal_info><firstname>$FirstName</firstname><lastname>$LastName</lastname><homephone>$HomePhone</homephone><workphone>$WorkPhone</workphone><email>$EmailAddress</email><contactpreference>home</contactpreference><contacttime>anytime</contacttime></personal_info><move_info><fromcity>$FromCity</fromcity><fromstate>$FromState</fromstate><fromzip>$FromZip</fromzip><fromcountry>USA</fromcountry>  <tocity>$ToCity</tocity><tostate>$ToState</tostate><tozip>$ToZip</tozip><tocountry>USA</tocountry><movedate>$MoveDate</movedate><intlmovetype></intlmovetype><movesize>$MoveSize</movesize></move_info><additional_info><serviceid>LONGDISTANCE</serviceid><clientip></clientip><sessionid></sessionid><keyword></keyword><reference></reference><comment>$Comments</comment></additional_info><auto_info><type1></type1><make1></make1><model1></model1><year1></year1><runningcondition1></runningcondition1><type2></type2><make2></make2><model2></model2><year2></year2><runningcondition2></runningcondition2></auto_info></moverscom>";

//-- send to Movers.com
$url = "http://www.moversweb.com/webservices/pt-lead.asmx/InsertLead";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
#curl_setopt($ch, CURLOPT_HEADER, 0);
#curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
#curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($movers_body));

$response = curl_exec($ch);
curl_close($ch);


#mail("rob@irelocation.com","MOVERS.COM LEADEXEC SEND","$movers_body");

echo $response;



?>