<?php 

/*
This script will receive an international moving lead from LeadExec and bounce it to SIRVA and output the reponse so it's picked up by LE.
*/

$countries = array( 
	'Afghanistan'=>'AF',
	'Albania'=>'AL',
	'Algeria'=>'DZ',
	'American Samoa'=>'AS',
	'Andorra'=>'AD',
	'Angola'=>'AO',
	'Anguilla'=>'AI',
	'Antarctica'=>'AQ',
	'Antigua And Barbuda'=>'AG',
	'Argentina'=>'AR',
	'Armenia'=>'AM',
	'Aruba'=>'AW',
	'Australia'=>'AU',
	'Austria'=>'AT',
	'Azerbaijan'=>'AZ',
	'Bahamas'=>'BS',
	'Bahrain'=>'BH',
	'Bangladesh'=>'BD',
	'Barbados'=>'BB',
	'Belarus'=>'BY',
	'Belgium'=>'BE',
	'Belize'=>'BZ',
	'Benin'=>'BJ',
	'Bermuda'=>'BM',
	'Bhutan'=>'BT',
	'Bolivia'=>'BO',
	'Bosnia And Herzegovina'=>'BA',
	'Botswana'=>'BW',
	'Bouvet Island'=>'BV',
	'Brazil'=>'BR',
	'British Indian Ocean Territory'=>'IO',
	'Brunei'=>'BN',
	'Bulgaria'=>'BG',
	'Burkina Faso'=>'BF',
	'Burundi'=>'BI',
	'Cambodia'=>'KH',
	'Cameroon'=>'CM',
	'Canada'=>'CA',
	'Cape Verde'=>'CV',
	'Cayman Islands'=>'KY',
	'Central African Republic'=>'CF',
	'Chad'=>'TD',
	'Chile'=>'CL',
	'China'=>'CN',
	'Christmas Island'=>'CX',
	'Cocos (Keeling) Islands'=>'CC',
	'Columbia'=>'CO',
	'Comoros'=>'KM',
	'Congo'=>'CG',
	'Cook Islands'=>'CK',
	'Costa Rica'=>'CR',
	'Cote D\'Ivorie (Ivory Coast)'=>'CI',
	'Croatia (Hrvatska)'=>'HR',
	'Cuba'=>'CU',
	'Cyprus'=>'CY',
	'Czech Republic'=>'CZ',
	'Democratic Republic Of Congo (Zaire)'=>'CD',
	'Denmark'=>'DK',
	'Djibouti'=>'DJ',
	'Dominica'=>'DM',
	'Dominican Republic'=>'DO',
	'East Timor'=>'TP',
	'Ecuador'=>'EC',
	'Egypt'=>'EG',
	'El Salvador'=>'SV',
	'England'=>'UK',
	'Equatorial Guinea'=>'GQ',
	'Eritrea'=>'ER',
	'Estonia'=>'EE',
	'Ethiopia'=>'ET',
	'Falkland Islands (Malvinas)'=>'FK',
	'Faroe Islands'=>'FO',
	'Fiji'=>'FJ',
	'Finland'=>'FI',
	'France'=>'FR',
	'France, Metropolitan'=>'FX',
	'French Guinea'=>'GF',
	'French Polynesia'=>'PF',
	'French Southern Territories'=>'TF',
	'Gabon'=>'GA',
	'Gambia'=>'GM',
	'Georgia'=>'GE',
	'Germany'=>'DE',
	'Ghana'=>'GH',
	'Gibraltar'=>'GI',
	'Greece'=>'GR',
	'Greenland'=>'GL',
	'Grenada'=>'GD',
	'Guadeloupe'=>'GP',
	'Guam'=>'GU',
	'Guatemala'=>'GT',
	'Guinea'=>'GN',
	'Guinea-Bissau'=>'GW',
	'Guyana'=>'GY',
	'Haiti'=>'HT',
	'Heard And McDonald Islands'=>'HM',
	'Honduras'=>'HN',
	'Hong Kong'=>'HK',
	'Hungary'=>'HU',
	'Iceland'=>'IS',
	'India'=>'IN',
	'Indonesia'=>'ID',
	'Iran'=>'IR',
	'Iraq'=>'IQ',
	'Ireland'=>'IE',
	'Israel'=>'IL',
	'Italy'=>'IT',
	'Jamaica'=>'JM',
	'Japan'=>'JP',
	'Jordan'=>'JO',
	'Kazakhstan'=>'KZ',
	'Kenya'=>'KE',
	'Kiribati'=>'KI',
	'Kuwait'=>'KW',
	'Kyrgyzstan'=>'KG',
	'Laos'=>'LA',
	'Latvia'=>'LV',
	'Lebanon'=>'LB',
	'Lesotho'=>'LS',
	'Liberia'=>'LR',
	'Libya'=>'LY',
	'Liechtenstein'=>'LI',
	'Lithuania'=>'LT',
	'Luxembourg'=>'LU',
	'Macau'=>'MO',
	'Macedonia'=>'MK',
	'Madagascar'=>'MG',
	'Malawi'=>'MW',
	'Malaysia'=>'MY',
	'Maldives'=>'MV',
	'Mali'=>'ML',
	'Malta'=>'MT',
	'Marshall Islands'=>'MH',
	'Martinique'=>'MQ',
	'Mauritania'=>'MR',
	'Mauritius'=>'MU',
	'Mayotte'=>'YT',
	'Mexico'=>'MX',
	'Micronesia'=>'FM',
	'Moldova'=>'MD',
	'Monaco'=>'MC',
	'Mongolia'=>'MN',
	'Montserrat'=>'MS',
	'Morocco'=>'MA',
	'Mozambique'=>'MZ',
	'Myanmar (Burma)'=>'MM',
	'Namibia'=>'NA',
	'Nauru'=>'NR',
	'Nepal'=>'NP',
	'Netherlands'=>'NL',
	'Netherlands Antilles'=>'AN',
	'New Caledonia'=>'NC',
	'New Zealand'=>'NZ',
	'Nicaragua'=>'NI',
	'Niger'=>'NE',
	'Nigeria'=>'NG',
	'Niue'=>'NU',
	'Norfolk Island'=>'NF',
	'North Korea'=>'KP',
	'Northern Mariana Islands'=>'MP',
	'Norway'=>'NO',
	'Oman'=>'OM',
	'Pakistan'=>'PK',
	'Palau'=>'PW',
	'Panama'=>'PA',
	'Papua New Guinea'=>'PG',
	'Paraguay'=>'PY',
	'Peru'=>'PE',
	'Philippines'=>'PH',
	'Pitcairn'=>'PN',
	'Poland'=>'PL',
	'Portugal'=>'PT',
	'Puerto Rico'=>'PR',
	'Qatar'=>'QA',
	'Reunion'=>'RE',
	'Romania'=>'RO',
	'Russia'=>'RU',
	'Rwanda'=>'RW',
	'Saint Helena'=>'SH',
	'Saint Kitts And Nevis'=>'KN',
	'Saint Lucia'=>'LC',
	'Saint Pierre And Miquelon'=>'PM',
	'Saint Vincent And The Grenadines'=>'VC',
	'San Marino'=>'SM',
	'Sao Tome And Principe'=>'ST',
	'Saudi Arabia'=>'SA',
	'Senegal'=>'SN',
	'Seychelles'=>'SC',
	'Sierra Leone'=>'SL',
	'Singapore'=>'SG',
	'Slovak Republic'=>'SK',
	'Slovenia'=>'SI',
	'Solomon Islands'=>'SB',
	'Somalia'=>'SO',
	'South Africa'=>'ZA',
	'South Georgia And South Sandwich Islands'=>'GS',
	'South Korea'=>'KR',
	'Spain'=>'ES',
	'Sri Lanka'=>'LK',
	'Sudan'=>'SD',
	'Suriname'=>'SR',
	'Svalbard And Jan Mayen'=>'SJ',
	'Swaziland'=>'SZ',
	'Sweden'=>'SE',
	'Switzerland'=>'CH',
	'Syria'=>'SY',
	'Taiwan'=>'TW',
	'Tajikistan'=>'TJ',
	'Tanzania'=>'TZ',
	'Thailand'=>'TH',
	'Togo'=>'TG',
	'Tokelau'=>'TK',
	'Tonga'=>'TO',
	'Trinidad And Tobago'=>'TT',
	'Tunisia'=>'TN',
	'Turkey'=>'TR',
	'Turkmenistan'=>'TM',
	'Turks And Caicos Islands'=>'TC',
	'Tuvalu'=>'TV',
	'Uganda'=>'UG',
	'Ukraine'=>'UA',
	'United Arab Emirates'=>'AE',
	'United Kingdom'=>'UK',
	'United States Of America'=>'US',
	'United States Minor Outlying Islands'=>'UM',
	'Uruguay'=>'UY',
	'Uzbekistan'=>'UZ',
	'Vanuatu'=>'VU',
	'Vatican City (Holy See)'=>'VA',
	'Venezuela'=>'VE',
	'Vietnam'=>'VN',
	'Virgin Islands (British)'=>'VG',
	'Virgin Islands (US)'=>'VI',
	'Wallis And Futuna Islands'=>'WF',
	'Western Sahara'=>'EH',
	'Western Samoa'=>'WS',
	'Yemen'=>'YE',
	'Yugoslavia'=>'YU',
	'Zambia'=>'ZM',
	'Zimbabwe'=>'ZW'
); 

$Pco = $_GET['PickupCountryCode'];
$Dco = $_GET['DeliveryCountryCode'];


$PickupCountryCode = $countries["$Pco"];
$DeliveryCountryCode = $countries["$Dco"];

$ReferralCode = urlencode($_GET['ReferralCode']);
$Email = $_GET['Email'];
$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$PrimaryPhoneType = urlencode($_GET['PrimaryPhoneType']);
$PreferredContactTime = urlencode($_GET['PreferredContactTime']);
$HomePhone = $_GET['HomePhone'];
$WorkPhone = $_GET['WorkPhone'];

//-- PICKUP
$CountryCode = $PickupCountryCode;
$Address1 = urlencode($_GET['Address1']);
$City = urlencode($_GET['City']);
$State = urlencode($_GET['State']);
$Zip = urlencode($_GET['Zip']);

//-- MOVE DETAILS
$PickupCountryCode = $PickupCountryCode;
$DeliveryCountryCode = $DeliveryCountryCode;
$PickupZip = urlencode($_GET['Zip']);
$DeliveryZip = "12345";
#$DeliveryZip = urlencode($_GET['DeliveryZip']);
$MoveDate = $_GET['MoveDate'];
$DwellingType = urlencode($_GET['DwellingType']);
$AmountOfFurnishings = urlencode($_GET['AmountOfFurnishings']);
$PickupShuttle = urlencode($_GET['PickupShuttle']);
$DeliveryShuttle = urlencode($_GET['DeliveryShuttle']);

//-- ESTIMATE DETAILS
$HasVehicles = urlencode($_GET['HasVehicles']);
$RequestedEstimateDate = urlencode($_GET['RequestedEstimateDate']);
$RequestedEstimateTimeOfDay = urlencode($_GET['RequestedEstimateTimeOfDay']);
$VendorId = urlencode($_GET['VendorId']);
$OptInForSpecialOffers = urlencode($_GET['OptInForSpecialOffers']);
$ThirdPartyOptIn = urlencode($_GET['ThirdPartyOptIn']);



$MSG_BODY = "Data from LE:\n\nReferralCode = $ReferralCode
Email = $Email
FirstName = $FirstName
LastName = $LastName
PrimaryPhoneType = $PrimaryPhoneType
PreferredContactTime = $PreferredContactTime
HomePhone = $HomePhone

CountryCode = $CountryCode
Address1 = $Address1
City = $City
State = $State
Zip = $Zip

PickupCountryCode = $PickupCountryCode
DeliveryCountryCode = $DeliveryCountryCode
PickupZip = $PickupZip
DeliveryZip = $DeliveryZip
MoveDate = $MoveDate
DwellingType = $DwellingType
AmountOfFurnishings = $AmountOfFurnishings
PickupShuttle = $PickupShuttle
DeliveryShuttle = $DeliveryShuttle

HasVehicles = $HasVehicles
RequestedEstimateDate = $RequestedEstimateDate
RequestedEstimateTimeOfDay = $RequestedEstimateTimeOfDay
VendorId = $VendorId
OptInForSpecialOffers = $OptInForSpecialOffers
ThirdPartyOptIn = $ThirdPartyOptIn

PickupCountryCodex = $Pco
DeliveryCountryCodex = $Dco";



//-- Build SIRVA Body
$sirva_body = "xmlRequest=<InternationalEstimateRequest><ReferralCode>$ReferralCode</ReferralCode><PrimaryContact><Email>$Email</Email><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><PrimaryPhoneType>$PrimaryPhoneType</PrimaryPhoneType><PreferredContactTime>$PreferredContactTime</PreferredContactTime><HomePhone>$HomePhone</HomePhone><WorkPhone>$WorkPhone</WorkPhone><WorkPhoneExt></WorkPhoneExt><CellPhone></CellPhone><FaxPhone></FaxPhone></PrimaryContact><PickupAddress><CountryCode>$PickupCountryCode</CountryCode><Address1>$Address1</Address1><City>$City</City><State>$State</State><Zip>$Zip</Zip></PickupAddress><MoveDetails><PickupCountryCode>$PickupCountryCode</PickupCountryCode><DeliveryCountryCode>$DeliveryCountryCode</DeliveryCountryCode><PickupZip>$PickupZip</PickupZip><DeliveryZip>$DeliveryZip</DeliveryZip><MoveDate>$MoveDate</MoveDate><DwellingType>$DwellingType</DwellingType><AmountOfFurnishings>$AmountOfFurnishings</AmountOfFurnishings><PickupShuttle>$PickupShuttle</PickupShuttle><DeliveryShuttle>$DeliveryShuttle</DeliveryShuttle></MoveDetails><EstimateDetails><HasVehicles>$HasVehicles</HasVehicles><RequestedEstimateDate>$RequestedEstimateDate</RequestedEstimateDate><RequestedEstimateTimeOfDay>$RequestedEstimateTimeOfDay</RequestedEstimateTimeOfDay><Comments></Comments></EstimateDetails><HTTPReferrer></HTTPReferrer><HTTPUserAgent></HTTPUserAgent><VendorId>$VendorId</VendorId><OptInForSpecialOffers>$OptInForSpecialOffers</OptInForSpecialOffers><ThirdPartyOptIn>$ThirdPartyOptIn</ThirdPartyOptIn></InternationalEstimateRequest>";



//-- send to SIRVA
#$url = "http://ballparktest.allied.com/RAE/RequestAnEstimate.asmx/SendInternationalRequestXmlString"; //-- TEST URL
$url = "http://ballpark.allied.com/RAE/RequestAnEstimate.asmx/SendInternationalRequestXmlString"; //-- PRODUCTION URL

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: 100'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://ballpark.imove.com/RAE/SendEstimateRequestXml"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($sirva_body));

$response = curl_exec($ch);
curl_close($ch);


mail("rob@irelocation.com","SIRVA LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$sirva_body");

echo $response;

?>