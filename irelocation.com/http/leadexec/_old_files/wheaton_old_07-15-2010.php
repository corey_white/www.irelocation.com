<?php 

/*
This script will receive a lead from LeadExec and bounce it to Wheaton and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = $_GET['PhoneWork'];
$HomePhone = $_GET['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = $_GET['EstimatedMoveDate'];
$MoveSize =urlencode( $_GET['Bedrooms']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";


//-- Build Wheaton Body
$wheaton_body = "<?xml version=\"1.0\" encoding=\"utf-8\" ?><MoverLead version=\"1.0\" source=\"irelocation\"><UserContact><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><Address1>NA</Address1><Address2></Address2><City>$City</City><State>$State</State><Zipcode>$Zipcode</Zipcode><WorkPhone>$WorkPhone</WorkPhone><HomePhone>$HomePhone</HomePhone><CellPhone></CellPhone><EmailAddress>$EmailAddress</EmailAddress><BestTimeCall></BestTimeCall><ContactAtWork></ContactAtWork><Comments>$Comments</Comments><leadId>$leadId</leadId></UserContact><MoveDetails><MoveType>Full-Service</MoveType><FromCity>$FromCity</FromCity><FromState>$FromState</FromState><FromZip>$FromZip</FromZip><FromCountry></FromCountry><ToCity>$ToCity</ToCity><ToState>$ToState</ToState><ToZip>$ToZip</ToZip><ToCountry></ToCountry><MoveDate>$MoveDate</MoveDate><MoveSize>$MoveSize</MoveSize></MoveDetails></MoverLead>";

//-- send to Wheaton
$url = "http://access.wheatonworldwide.com/InternetLeads/external";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array("User-Agent: Mozilla/4.0", "Content-type: text/xml","REFERER: irelocation.com"));
#curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)';
curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($wheaton_body));

$response = curl_exec($ch);
curl_close($ch);


#mail("rob@irelocation.com","WHEATON LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$wheaton_body");

echo $response;



?>