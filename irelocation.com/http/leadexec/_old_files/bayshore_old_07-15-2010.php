<?php 

/*
This script will receive a lead from LeadExec and bounce it to Bayshore and output the reponse so it's picked up by LE.
*/

$FirstName = urlencode($_GET['FirstName']);
$LastName = urlencode($_GET['LastName']);
$Address1 = "NA";
$Address2 = "NA";
$City = urlencode($_GET['OriginCity']);
$State = urlencode($_GET['OriginState']);
$Zipcode = urlencode($_GET['OriginZip']);
$WorkPhone = $_GET['PhoneWork'];
$HomePhone = $_GET['PhoneHome'];
$CellPhone = "";
$EmailAddress = $_GET['Email'];
$BestTimeCall = "";
$ContactAtWork = "";
$Comments = urlencode($_GET['Comments']);
$leadId =urlencode( $_GET['QuoteID']);
$MoveType = urlencode("Full-Service");
$FromCity = urlencode($_GET['OriginCity']);
$FromState = urlencode($_GET['OriginState']);
$FromZip = urlencode($_GET['OriginZip']);
$FromCountry = "";
$ToCity = urlencode($_GET['DestinationCity']);
$ToState = urlencode($_GET['DestinationState']);
$ToZip = urlencode($_GET['DestinationZip']);
$ToCountry = "";
$MoveDate = $_GET['EstimatedMoveDate'];
$MoveSize =urlencode( $_GET['Bedrooms']);

$MSG_BODY = "Data from LE:\n\nFirstName = $FirstName
LastName = $LastName
Address1 = $Address1
Address2 = $Address2
City = $City
State = $State
Zipcode = $Zipcode
WorkPhone = $WorkPhone
HomePhone = $HomePhone
CellPhone = $CellPhone
EmailAddress = $EmailAddress
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork
Comments = $Comments
leadId = $leadId
MoveType = $MoveType
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveDate = $MoveDate
MoveSize = $MoveSize";


//-- Build Bayshore Body
$bayshore_body = "xmlRequest=<EstimateRequest><ReferralCode>IRELO01861</ReferralCode><PrimaryContact><Email>$EmailAddress</Email><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><PrimaryPhoneType>H</PrimaryPhoneType><PreferredContactTime>E</PreferredContactTime><HomePhone>$HomePhone</HomePhone><WorkPhone>$WorkPhone</WorkPhone><WorkPhoneExt></WorkPhoneExt><CellPhone></CellPhone><FaxPhone></FaxPhone></PrimaryContact><PickupAddress><Address1>Ask Customer</Address1><Address2></Address2><City>$FromCity</City><State>$FromState</State><Zip>$FromZip</Zip></PickupAddress><MoveDetails><PickupZip>$FromZip</PickupZip><DeliveryZip>$ToZip</DeliveryZip><MoveDate>$MoveDate</MoveDate><DwellingType>$MoveSize</DwellingType><AmountOfFurnishings>M</AmountOfFurnishings><PickupShuttle>N</PickupShuttle><DeliveryShuttle>N</DeliveryShuttle></MoveDetails><EstimateDetails><HasVehicles>N</HasVehicles><RequestedEstimateDate>3D</RequestedEstimateDate><RequestedEstimateTimeOfDay>E</RequestedEstimateTimeOfDay><SpecialtyItems></SpecialtyItems><Comments>$Comments</Comments></EstimateDetails></EstimateRequest>";

//-- send to Bayshore
$url = "http://ballpark.allied.com/RAE/RequestAnEstimate.asmx/SendEstimateRequestXmlString";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);

curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded', 'Content-length: 100'));
curl_setopt ($ch, CURLOPT_HTTPHEADER, array('SOAPAction: "http://ballpark.allied.com/RAE/SendEstimateRequestXml"'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

curl_setopt($ch, CURLOPT_POSTFIELDS, trim($bayshore_body));

$response = curl_exec($ch);
curl_close($ch);


mail("rob@irelocation.com","BAYSHORE LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$bayshore_body");

echo $response;



?>