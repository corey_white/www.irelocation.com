<?php 

/*
This script will receive a lead from LeadExec and bounce it to POM and output the reponse so it's picked up by LE.
*/

$FirstName = urldecode($_GET['FirstName']);
$LastName = urldecode($_GET['LastName']);
#$Address1 = urldecode($_GET['Address1']);
$Address1 = "123 Main St";
$City = urldecode($_GET['City']);
$State = urldecode($_GET['State']);
$Zip = urldecode($_GET['Zip']);

$Email = $_GET['Email'];
$WorkPhone = $_GET['WorkPhone'];
$HomePhone = $_GET['HomePhone'];

$FromCity = urldecode($_GET['FromCity']);
$FromState = urldecode($_GET['FromState']);
$FromZip = urldecode($_GET['FromZip']);
$FromCountry = urldecode($_GET['FromCountry']);

$ToCity = urldecode($_GET['ToCity']);
$ToState = urldecode($_GET['ToState']);
$ToZip = urldecode($_GET['ToZip']);
$ToCountry = urldecode($_GET['ToCountry']);

$MoveType = urldecode($_GET['MoveType']);
$MoveDate = urldecode($_GET['MoveDate']);
$MoveSize = urldecode($_GET['MoveSize']);

$BestTimeCall = urldecode($_GET['BestTimeCall']);
$ContactAtWork = urldecode($_GET['ContactAtWork']);


$MSG_BODY = "
FirstName = $FirstName
LastName = $LastName
Address1 = $Address1
City = $FromCity
State = $FromState
Zip = $FromZip
Email = $Email
WorkPhone = $WorkPhone
HomePhone = $HomePhone
FromCity = $FromCity
FromState = $FromState
FromZip = $FromZip
FromCountry = $FromCountry
ToCity = $ToCity
ToState = $ToState
ToZip = $ToZip
ToCountry = $ToCountry
MoveType = $MoveType
MoveDate = $MoveDate
MoveSize = $MoveSize
BestTimeCall = $BestTimeCall
ContactAtWork = $ContactAtWork";


$MSG_BODY = urldecode($MSG_BODY);

//-- Build POM Body
/*
$pom_body = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>
<MoverLead version=\"1.0\" source=\"movingcompanyleads.com\">
	<UserContact>
		<FirstName>$FirstName</FirstName>
		<LastName>$LastName</LastName>
		<Address1>$Address1</Address1>
		<Address2></Address2>
		<City>$FromCity</City>
		<State>$FromState</State>
		<Zipcode>$FromZip</Zipcode>
		<EmailAddress>$Email</EmailAddress>
		<WorkPhone>$WorkPhone</WorkPhone>
		<HomePhone>$HomePhone</HomePhone>
		<CellPhone></CellPhone>
		<BestTimeCall>$BestTimeCall</BestTimeCall>
		<ContactAtWork>$ContactAtWork</ContactAtWork>
		<Comments>Additional user requirements</Comments>
	</UserContact>
	<MoveDetails>
		<MoveType>$MoveType</MoveType>
		<FromCity>$FromCity</FromCity>
		<FromState>$FromState</FromState>
		<FromZip>$FromZip</FromZip>
		<FromCountry>$FromCountry</FromCountry>
		<ToCity>$ToCity</ToCity>
		<ToState>$ToState</ToState>
		<ToZip>$ToZip</ToZip>
		<ToCountry>$ToCountry</ToCountry>
		<MoveDate>$MoveDate</MoveDate>
		<MoveSize>$MoveSize</MoveSize>
	</MoveDetails>
</MoverLead>";
*/

#mail("rob@irelocation.com","POM Lead","$MSG_BODY\n\n\n\n$pom_body");

// --------------------------------------------------------------------------------


$url = "https://movegistics.com/lead_test1.php";  

$xml = '<?xml version=\"1.0\" encoding=\"utf-8\" ?><MoverLead version=\"1.0\" source=\"iRelocation\"><UserContact><FirstName>$FirstName</FirstName><LastName>$LastName</LastName><Address1>$Address1</Address1><Address2></Address2><City>$FromCity</City><State>$FromState</State><Zipcode>$FromZip</Zipcode><EmailAddress>$Email</EmailAddress><WorkPhone>$WorkPhone</WorkPhone><HomePhone>$HomePhone</HomePhone><CellPhone></CellPhone><BestTimeCall>$BestTimeCall</BestTimeCall><ContactAtWork>$ContactAtWork</ContactAtWork><Comments>Additional user requirements</Comments></UserContact><MoveDetails><MoveType>$MoveType</MoveType><FromCity>$FromCity</FromCity><FromState>$FromState</FromState><FromZip>$FromZip</FromZip><FromCountry>$FromCountry</FromCountry><ToCity>$ToCity</ToCity><ToState>$ToState</ToState><ToZip>$ToZip</ToZip><ToCountry>$ToCountry</ToCountry><MoveDate>$MoveDate</MoveDate><MoveSize>$MoveSize</MoveSize></MoveDetails></MoverLead>';  


$ch = curl_init();  
curl_setopt($ch, CURLOPT_URL,$url);  
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);  
curl_setopt($ch, CURLOPT_POST, 1);  

curl_setopt($ch, CURLOPT_POSTFIELDS,  
http_build_query(array('yourData' => $xml)));  

// receive server response ...  
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  

$response = curl_exec ($ch);  
curl_close ($ch);  

// --------------------------------------------------------------------------------


/* Just as a reference, this is what we had before:
//-- send to POM
$url = "https://pom.movegistics.com/index.php?r=site/page&view=lc"; //-- Production

//-- TEMPORARY PRODUCTION URL
#$url = ""; //-- TEST SERVER (apprently none at this time)


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HEADER, 0);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: text/xml', 'Content-length: 100'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, trim($pom_body));

$response = curl_exec($ch);
curl_close($ch);
*/

mail("rob@irelocation.com","POM LEADEXEC SEND","$MSG_BODY \n\n Response: $response \n\n$xml");

/*
$pattern = '/\<MoveId\>\d+\<\/MoveId\>/';
if ( $ccemail && preg_match($pattern, $response) ) { //-- also send an email copy if there's a success response
#   mail("rob@irelocation.com","Top Moving Lead Email Copy","$MSG_BODY","From: TopMoving@irelocation.com");
    mail("$ccemail,rob@irelocation.com","Top Moving Lead Email Copy","$MSG_BODY","From: TopMoving@irelocation.com");
} 
*/

echo $response;


?>