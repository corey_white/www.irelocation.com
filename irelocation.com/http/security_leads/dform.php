<?
	session_start();
	extract($_SESSION);	
	/*
	if (strlen($_SESSION['source']) == 0)
	{
		if ($_POST['username'] != "" && $_POST['password'] != "")
		{
			extract($_POST);
			if ($username == "pay4" && $password == "performance")
				$source = "p4p";
			else
			{
				header("Location: login.php?invalid");
				exit();
			}
		}
		else
		{
			header("Location: login.php?invalid");
			exit();
		}			
	}		
	*/
?>
<html>
	<head>
		<title>Security Leads</title>
		<link href="css.css" rel="stylesheet" type="text/css" />
		<script>
			function clearForm(theform)
			{
				var els = theform.elements;
				var i = 0;
				var count = els.length;
				
				for (i = 0; i < count; i = i + 1)
				{
					var field = els[i];
					if (field.type == "text" || field.type == "textarea")
						field.value = "";
					else if (field.type == "select-one")
						field.selectedIndex = 0;	
					else if (field.type == "checkbox")
						field.checked = false;
					else if (field.type == "button" ||
							 field.type == "hidden" ||
							 field.type == "submit" ||
							 field.type == "radio" )
						continue;
					else
						alert(field.type);														
				}
			
			}
		
		</script>
	</head>
	<body>
	<div align="center">
<table>
<tbody>							
	<tr bgcolor="#ffffff">
		<td colspan="3" align="center" bgcolor="#e4e2ce">
			<form name="quoteform" method="post" action="dform_submit.php">
				<input type='hidden' value="<?= $source ?>" name='source' />
				<input type='hidden' value="dform.php" name="form_page" />
			  <table background="#E4E2CE" border="0" cellspacing="1" width="520">
			   <tbody>
			   	<tr>
					<td colspan="4" class="mainsecuritytext">
						<strong><?= $form_title ?></strong>
					</td>
				</tr>
				<? if ($_REQUEST['msg'] != "") { ?>
				<tr align="left"> 
				  <td colspan="4" class="mainsecuritytext">
						<font color="#FF0000">
						<strong>Error! </strong>
						Please double check field: 
						<strong><?= $_REQUEST['msg'] ?>	</strong>						</font>				  </td>
				</tr>
				<? 
				} 
				else if ($_REQUEST['id'] != "") 
				{ 
										
				?>
				<tr align="left"> 
				  <td colspan="2" class="mainsecuritytopic" align="left">
						<font color="#009900">
							A Lead was added: S<?= $_REQUEST['id'] ?>
						</font>				  </td>
				  <td colspan="2" align="left">
				  	<input value="CLEAR FORM" class="movingquotetext" type="button"
		 onClick="javascript:clearForm(document.forms[0]);">		 		</td>
				</tr>
				<? } ?>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr align="left">  
					<td>&nbsp;</td>
			        <td colspan="3" class="mainsecuritytopic">
					  	Please Enter Contact Information </td>
			    </tr>
				<tr align="left">  
					<td class="mainsecuritytext" align="left" width="20%">
						First Name:					</td>
					
					<td align="left" width="35%">
						<input style="background-color: rgb(255, 255, 160);" name="name1" value="<?= $name1 ?>"  size="20" class="movingquotetext" maxlength="20"></td>
					<td class="mainsecuritytext" align="right" width="20%">Last Name:</td>
					<td align="left" width="25%"> <input style="background-color: rgb(255, 255, 160);" name="name2" value="<?= $name2 ?>" size="32" class="movingquotetext" maxlength="20" type="text"></td>
				</tr>
	<tr align="left">  
	  <td class="mainsecuritytext" align="left">Phone</td>
	  <td align="left"> <input style="background-color: rgb(255, 255, 160);" name="phone1" value="<?= $phone1 ?>"  size="20" class="movingquotetext" type="text"></td>

	  <td class="mainsecuritytext" align="right">Alternate Phone:</td>
	  <td align="left"> <input style="background-color: rgb(255, 255, 160);" name="phone2" value="<?= $phone2 ?>"  size="32" class="movingquotetext" type="text"></td>
	</tr>
		<tr align="left">
		<td class="mainsecuritytext" align="left">Zip Code:</td>
	 	<td align="left"> <input style="background-color: rgb(255, 255, 160);" name="zip" value="<?= $zip ?>"  size="8" class="mainsecuritytext" type="text"></td>
		<td class="mainsecuritytext" align="right">Address:</td>

		<td colspan="1" align="left"><input style="background-color: rgb(255, 255, 160);" name="address" value="<?= $address ?>"  size="32" class="movingquotetext" maxlength="50" type="text"></td>
	</tr>
	<tr align="left">		
		<td class="mainsecuritytext" align="left">City:</td>
				<td colspan="1" align="left"><input style="background-color: rgb(255, 255, 160);" name="city" value="<?= $city ?>"  size="20" class="movingquotetext" maxlength="30" type="text"></td>
		<td class="mainsecuritytext" align="right">State:</td>
		<td colspan="1" align="left">
		<?= "<!--- state_code: $state_code --->" ?>
					<select style="background-color: rgb(255, 255, 160);" size="1" name="state_code" class="mainsecuritytext">

                <option value="XX" selected="selected">- Select -</option>
				<optgroup label='--- United States ---'>
                <option value="US-AL" <?= ($state_code=="US-AL")?"selected ":" " ?>>Alabama</option> 
                <option value="US-AK" <?= ($state_code=="US-AK")?"selected ":" " ?>>Alaska</option>
                <option value="US-AZ" <?= ($state_code=="US-AZ")?"selected ":" " ?>>Arizona</option>
                <option value="US-AR" <?= ($state_code=="US-AR")?"selected ":" " ?>>Arkansas</option>
                <option value="US-CA" <?= ($state_code=="US-CA")?"selected ":" " ?>>California</option>
                <option value="US-CO" <?= ($state_code=="US-CO")?"selected ":" " ?>>Colorado</option>
                <option value="US-CT" <?= ($state_code=="US-CT")?"selected ":" " ?>>Connecticut</option>
                <option value="US-DC" <?= ($state_code=="US-DC")?"selected ":" " ?>>DC</option>
                <option value="US-DE" <?= ($state_code=="US-DE")?"selected ":" " ?>>Delaware</option>
                <option value="US-FL" <?= ($state_code=="US-FL")?"selected ":" " ?>>Florida</option>
                <option value="US-GA" <?= ($state_code=="US-GA")?"selected ":" " ?>>Georgia</option>
                <option value="US-HI" <?= ($state_code=="US-HI")?"selected ":" " ?>>Hawaii</option>
                <option value="US-ID" <?= ($state_code=="US-ID")?"selected ":" " ?>>Idaho</option>
                <option value="US-IL" <?= ($state_code=="US-IL")?"selected ":" " ?>>Illinois</option>
                <option value="US-IN" <?= ($state_code=="US-IN")?"selected ":" " ?>>Indiana</option>
                <option value="US-IA" <?= ($state_code=="US-IA")?"selected ":" " ?>>Iowa</option>
                <option value="US-KS" <?= ($state_code=="US-KS")?"selected ":" " ?>>Kansas</option>
                <option value="US-KY" <?= ($state_code=="US-KY")?"selected ":" " ?>>Kentucky</option>
                <option value="US-LA" <?= ($state_code=="US-LA")?"selected ":" " ?>>Louisiana</option>
                <option value="US-ME" <?= ($state_code=="US-ME")?"selected ":" " ?>>Maine</option>
                <option value="US-MD" <?= ($state_code=="US-MD")?"selected ":" " ?>>Maryland</option>
                <option value="US-MA" <?= ($state_code=="US-MA")?"selected ":" " ?>>Massachusetts</option>
                <option value="US-MI" <?= ($state_code=="US-MI")?"selected ":" " ?>>Michigan</option>
                <option value="US-MN" <?= ($state_code=="US-MN")?"selected ":" " ?>>Minnesota</option>
                <option value="US-MS" <?= ($state_code=="US-MS")?"selected ":" " ?>>Mississippi</option>
                <option value="US-MO" <?= ($state_code=="US-MO")?"selected ":" " ?>>Missouri</option>
                <option value="US-MT" <?= ($state_code=="US-MT")?"selected ":" " ?>>Montana</option>
                <option value="US-NE" <?= ($state_code=="US-NE")?"selected ":" " ?>>Nebraska</option>
                <option value="US-NV" <?= ($state_code=="US-NV")?"selected ":" " ?>>Nevada</option>
                <option value="US-NH" <?= ($state_code=="US-NH")?"selected ":" " ?>>New Hampshire</option>
                <option value="US-NJ" <?= ($state_code=="US-NJ")?"selected ":" " ?>>New Jersey</option>	 
	            <option value="US-NM" <?= ($state_code=="US-NM")?"selected ":" " ?>>New Mexico</option>
                <option value="US-NY" <?= ($state_code=="US-NY")?"selected ":" " ?>>New York</option>
                <option value="US-NC" <?= ($state_code=="US-NC")?"selected ":" " ?>>North Carolina</option>
                <option value="US-ND" <?= ($state_code=="US-ND")?"selected ":" " ?>>North Dakota</option>
                <option value="US-OH" <?= ($state_code=="US-OH")?"selected ":" " ?>>Ohio</option>
                <option value="US-OK" <?= ($state_code=="US-OK")?"selected ":" " ?>>Oklahoma</option>
                <option value="US-OR" <?= ($state_code=="US-OR")?"selected ":" " ?>>Oregon</option>
                <option value="US-PA" <?= ($state_code=="US-PA")?"selected ":" " ?>>Pennsylvania</option>
                <option value="US-RI" <?= ($state_code=="US-RI")?"selected ":" " ?>>Rhode Island</option>
                <option value="US-SC" <?= ($state_code=="US-SC")?"selected ":" " ?>>South Carolina</option>
                <option value="US-SD" <?= ($state_code=="US-SD")?"selected ":" " ?>>South Dakota</option>
                <option value="US-TN" <?= ($state_code=="US-TN")?"selected ":" " ?>>Tennessee</option>
                <option value="US-TX" <?= ($state_code=="US-TX")?"selected ":" " ?>>Texas</option>
                <option value="US-UT" <?= ($state_code=="US-UT")?"selected ":" " ?>>Utah</option>
                <option value="US-VT" <?= ($state_code=="US-VT")?"selected ":" " ?>>Vermont</option>
                <option value="US-VA" <?= ($state_code=="US-VA")?"selected ":" " ?>>Virginia</option>
                <option value="US-WA" <?= ($state_code=="US-WA")?"selected ":" " ?>>Washington</option>
                <option value="US-WV" <?= ($state_code=="US-WV")?"selected ":" " ?>>West Virginia</option>
                <option value="US-WI" <?= ($state_code=="US-WI")?"selected ":" " ?>>Wisconsin</option>
                <option value="US-WY" <?= ($state_code=="US-WY")?"selected ":" " ?>>Wyoming</option>
				</optgroup>
				<? /*
				<optgroup label="--- Canada ---">	
					<option value="CA-AB" <?= ($state_code=="CA-AB")?"selected ":" " ?>>Alberta  </option>
					<option value="CA-BC" <?= ($state_code=="CA-BC")?"selected ":" " ?>>British Columbia </option>
					<option value="CA-MB" <?= ($state_code=="CA-MB")?"selected ":" " ?>>Manitoba </option>
					<option value="CA-NB" <?= ($state_code=="CA-NB")?"selected ":" " ?>>New Brunswick </option>
					<option value="CA-NL" <?= ($state_code=="CA-NL")?"selected ":" " ?>>Newfoundland & Labrador </option>
					<option value="CA-NS" <?= ($state_code=="CA-NS")?"selected ":" " ?>>Nova Scotia </option>
					<option value="CA-NT" <?= ($state_code=="CA-NT")?"selected ":" " ?>>Northwest Territories </option>
					<option value="CA-NU" <?= ($state_code=="CA-NU")?"selected ":" " ?>>Nunavut </option>
					<option value="CA-ON" <?= ($state_code=="CA-ON")?"selected ":" " ?>>Ontario </option>
					<option value="CA-PE" <?= ($state_code=="CA-PE")?"selected ":" " ?>>Prince Edward Island </option>
					<option value="CA-QC" <?= ($state_code=="CA-QC")?"selected ":" " ?>>Quebec </option>
					<option value="CA-SK" <?= ($state_code=="CA-SK")?"selected ":" " ?>>Saskatchewan </option>
					<option value="CA-YT" <?= ($state_code=="CA-YT")?"selected ":" " ?>>Yukon </option>
				</optgroup>
				*/ ?>
              </select>		
		</td>
	</tr>
<tr> 
	<td>&nbsp;</td>
    <td colspan="3" class="mainsecuritytopic" align="left">
	  	Select Details below:	</td>
    </tr>
	
    <tr> 
      <td align="left" valign="top" class="mainsecuritytext">Is your home currently Monitored?</td>
      <td align="left" valign="top" class="mainsecuritytext"><select size="1" name="current_needs" tabindex="1" class="mainsecuritytext">		 
		    <option value='xx'>Select...</option>
		 	<option value='new' <?= ($current_needs=="new")?"selected ":" " ?>>No</option>
			<option value='upgrade' <?= ($current_needs=="upgrade")?"selected ":" " ?>>Yes</option>
        </select></td>
	  <td align="right" valign="top" class="mainsecuritytext">Do you:</td>
	  <td colspan="1" align="left" valign="top" class="mainsecuritytext">
	  	<input name="own_rent" value="own" <?= ($own_rent=="own")?"checked='checked' ":" " ?>
				 type="radio">Own
			<input name="own_rent" value="rent" <?= ($own_rent=="rent")?"checked='checked' ":" " ?>
				 type="radio">Rent	  </td>
    </tr>	
	<? /*
	<tr> 
      <td align="left" valign="top" class="mainsecuritytext">How large is your home or busines?</td>
      <td align="left" valign="top" class="mainsecuritytext"><select size="1" name="sqr_footage" tabindex="1" class="mainsecuritytext">		 
			<option value='xx'>Select...</option>
		 	<option value='20' <?= ($sqr_footage=="20")?"selected ":" " ?>>&lt; 5000 sq. ft.</option>
			<option value='500'<?= ($sqr_footage=="500")?"selected ":" " ?>>&gt; 5000 sq. ft.</option>
        </select></td>
	  <td align="right" valign="top" class="mainsecuritytext"></td>
	  <td colspan="1" align="left" valign="top" class="mainsecuritytext">
	  	</td>
    </tr>
	*/ ?>
    <tr> 
      <td class="mainsecuritytext" align="left" valign="top"> Please add any special 
        comments and/or special needs:</td>
      <td colspan="3" class="mainsecuritytext" align="left">
	  <textarea rows="7" name="comments" cols="60" class="mainsecuritytext">
	  <?= $comments ?>
	  </textarea></td>
    </tr>
	<? /*
	<tr> 
      <td align="left" valign="top" class="mainsecuritytext">Would you like to save money on your TV Service?</td>
      <td align="left" valign="top" class="mainsecuritytext"><input type='checkbox' name='tv'></td>
	  <td align="right" valign="top" class="mainsecuritytext"></td>
	  <td colspan="1" align="left" valign="top" class="mainsecuritytext">
	  	</td>
    </tr>
	*/ ?>
    <tr align="left"> 
      <td>&nbsp;</td>
      <td colspan="3">
	  	 <input value="SUBMIT LEAD" class="movingquotetext" type="submit">
		 &nbsp;  &nbsp;  &nbsp; 
		 <input value="CLEAR FORM" class="movingquotetext" type="button"
		 onClick="javascript:clearForm(document.forms[0]);">		 </td>
    </tr>
	
  </tbody></table>
</form>							</td>
							</tr>		
							<tr><td colspan='1' align="right" class="mainsecuritytext">
								<strong><a href='logout.php'>Log out</a></strong>
							</td></tr>					
						</tbody>
					</table>
				</div>