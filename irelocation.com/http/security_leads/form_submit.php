<?php
	session_start();
	include "../inc_mysql.php";		

	function validatePhoneMore($phone)
	{
		$phone = substr($phone,3);
		$valid = true;
		
		for ($i = 0; $i < 10 && $valid; $i++)
		{
			$s = "".$i;
			if (substr_count($phone,$s) > 5)
			{
				$valid = false;
				break;
			}
		}
		return $valid;
	}

	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$thispage = $_SERVER['SCRIPT_NAME'];
		$server = $_SERVER['SERVER_NAME'];

		$email = "IP: $ip\n\nUserAgent: $useragent\n\nPage: $thispage\n\nServer: $server\n\nTime: ".date("YmdHis")."\n\n";
		$email .= "Posted Data: ".strtolower(print_r($_POST,true));
		mail("code@irelocation.com,david@irelocation.com","***AFF FORM LINK INJECTION - ".$type,$email);
		if ($_POST['quotetype'] == 'res') $quotepage = "residential_security_quote.php";
		fail("Link Injection Detected.");
		exit();
	}
	
	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
	}		
	testInjection();	

	$keys = array_keys($_POST);
	
	foreach($keys as $k)
	{
		$_POST[$k] = str_replace(array("<",">"),array("&lt;","&gt;"),trim($_POST[$k]));//kill all tags
		$_POST[$k] = str_replace(array("select","delete","insert","create","update","drop"),
								 array("choose","remove","add","make","change","fumble"),
								 $_POST[$k]);//kill all tags
		$_SESSION[$k] = $_POST[$k];
	}
	
	extract($_POST);
	
	//mail("david@irelocation.com","Initially",$source." - ".$mysource);
	if (strlen(trim($source)) == 0)//from post.
	{	
		if (strlen(trim($mysource)) > 0)//try from session..
		{
			$source = $mysource;
			//mail("david@irelocation.com","Set From Session",$source." - ".$mysource);
		}
		else		//catch all. bad.
		{
			$source = SITE_BASE;
			//mail("david@irelocation.com","Catch All",$source." - ".$mysource);
		}
	}

	
	function fail($reason)
	{
		header("Location: form.php?msg=".urlencode($reason));		
		exit();
	}
	

	while (substr_count($source,SITE_BASE) > 1)
		$source = str_replace(SITE_BASE."_".SITE_BASE."_",SITE_BASE."_",$source);
	
	//mail("david@irelocation.com","TSC",$msg);
	
	
	
	$REDIRECT = "Location: form.php";
	
	$phone1 = ereg_replace("[^0-9]","",$phone1);

	$_SESSION["phone1"] = $phone1;
	
	$phone2 = ereg_replace("[^0-9]","",$phone2);
	$_SESSION["phone2"] = $phone2;
	
	if ($state_code != "XX")
		list($country,$state) = split("-",$state_code);
	/*
	$p_area   = substr($phone1,0,3);
    $p_prefix = substr($phone1,3,3);	
	$p_suffix = substr($phone1,6,4);
	
	
	
	if ($p_area != 800 && $p_area != 866 && $p_area != 877 && $p_area != 888)
	{
		$sql = "select * from movingdirectory.areacodes where ".
				"npa = '$p_area' and nxx = '$p_prefix';";
		$rs = new mysql_recordset($sql);		
		if (!$rs->fetch_array())
		{
			fail("phone");
		}
	}	
	*/
	if (!validatePhoneMore($phone1))
	{
		fail("phone");
	}
	
	if (strlen(trim($name1)) < 2)
	{
		fail("First Name");
	}
	
	if (strlen($name2) < 3)
	{
		fail("Last Name");
	}
	
	if (!is_numeric($phone1) || strlen($phone1) < 10)
	{
		fail("Phone Number");
	}
	
	$email = trim($email);
	
	if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email) )
	{
		fail("Email Address");
	} 	

	if (strlen($address) < 4 || substr_count($address," ") < 1 || substr_count($address,"@") > 0)
	{	
		fail("Physical Address");
	}
	
	$city = trim($city);
	
	if ($city == "" || is_numeric($city) || strlen($city) < 3)
	{
		fail("City");
	}

	
	if ($state == "XX")
	{
		fail("State/Province");		
	}			
	
	$zip = trim($zip);
	
	if (
		(is_numeric($zip)  && $country == "US" && strlen($zip) == 5) ||
		($country == "CA" && (strlen($zip) == 6 || strlen($zip) == 7))
	  )
	{
		$valid = true;				
	}
	else
	{
		$ca = ($country == "CA" && (strlen($zip) == 6 || strlen($zip) == 7));
		$us = (is_numeric($zip) && $country == "US" && strlen($zip) == 5);
		fail("Zip Code/Postal Code");	
	}

	if (!checkZip($country,$zip,$state))
	{
		fail("Location, zip and state do not match.");		
	}
	
	if($own_rent != "own" && $own_rent != "rent")
	{
		fail("Own/Rent");
	}
	

	if ($current_needs == "XX")
	{
		fail("Security Needs");
	}
	
	if ($building_type == "XX")
	{
		fail("Building Type");		
	}
				
	$ts = time() - (24 * 3600);//subtract a day
	$time = date("YmdHis",$ts);

	$sql = "select * from irelocation.leads_security where ".
	" (phone1 = '$phone1' or email = '$email') and (received = '' or received > '$time') ".
	"and zip = '$zip' and city = '$city' and address = '$address' limit 1";	
	

	$rs = new mysql_recordset($sql);
	if ($rs->fetch_array())
	{
		fail("This is a Duplicate Lead");
	}	
		
	function checkZip($country,$zip,$state)
	{
		if ($country == "US")
		{
			$sql = "select * from movingdirectory.zip_codes where".
					" zip = '$zip' and state = '$state';";
			
			$rs = new mysql_recordset($sql);
			return ($rs->fetch_array());
		}
		else
		{
			if (strlen($zip) == 7) $zip = str_replace(" ","",$zip);
				
			$sql = "select * from movingdirectory.zip_codes where".
					" postal_code = '$zip' and state = '$state';";
			
			$rs = new mysql_recordset($sql);
			return ($rs->fetch_array());		
		}
	}
	
	$ninetydays = date("YmdHis",time() - 90*24*60*60);
	
	$sql = " select * from irelocation.leads_security where ".
			"(email = '$email' or phone1 = '$phone1') and ".
			" zip = '$zip' and (received = '' OR received is null ".
			" OR received = '0' OR received > '$ninetydays') limit 1";
	$rs = new mysql_recordset($sql);
	if ($rs->fetch_array())
	{
		fail("This customer has already been submited.");
	}
	
	$comments = str_replace("'","\'",$comments);
	
	$phone2 = ereg_replace("[^0-9]","",$phone2);
	$remote_ip = $_SERVER['REMOTE_ADDR'];
	$sql = " insert into irelocation.leads_security set name1 = '$name1', ".
			" name2 = '$name2', phone1 = '$phone1', phone2 = '$phone2', ".
			" quote_type='res', source='aff_$source', email = '$email', ".
			" address = '$address', city = '$city', state_code = '$state',".
			" campaign = '$country', zip = '$zip', current_needs = '$current_needs', ".
			" building_type ='$building_type', num_location = 1, ".
			" sqr_footage = '$sqr_footage', own_rent = '$own_rent', fire = '$fire', ".
			" cctv = '$cctv', prewired = '$prewired', access = '$access', ".
			" other_field = '$other_field', comments = '$comments', ready_to_send = 1,".
			" remote_ip = '$remote_ip', received = '' ";
	$rs = new mysql_recordset($sql);
	$id = $rs->last_insert_id();
	//mail("david@irelocation.com","Security Aff Form",$sql);
	
	
	$_SESSION['name1'] = "";
	$_SESSION['name2'] = "";
	$_SESSION['phone1'] = "";
	$_SESSION['phone2'] = "";
	$_SESSION['email'] = "";
	$_SESSION['zip'] = "";
	$_SESSION['city'] = "";
	$_SESSION['state'] = "";
	$_SESSION['country'] = "";
	$_SESSION['address'] = "";
	$_SESSION['own_rent'] = "";
	$_SESSION['current_needs'] = "";
	$_SESSION['comments'] = "";
	
	header("Location: form.php?id=$id");
	exit();	
?>