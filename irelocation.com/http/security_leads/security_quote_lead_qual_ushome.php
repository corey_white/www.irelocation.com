<?php
	include_once "../inc_mysql.php";
	
class security_quote
{
	var $source;   
	var $referrer; 
	var $quote_type;
	
	var $name1;        
	var $name2;        
	var $phone1;       
	var $phone2;       
	var $email;        
	var $address;      
	var $city;         
	var $state_code;   
	var $zip;          
	var $country;
	var $current_needs; 
	var $building_type; 
	var $num_location;                           
	var $sqr_footage;  
	var $own_rent;     
	var $fire;         
	var $access;       
	var $cctv;         
	var $other_field;  
	var $scrub_notes;
	var $scrub_status;
	var $ipaddress;

	var $comments;     
	
	//call me after you set all my variables.
	function scrub()
	{
		//$this->name1=ucwords(strtolower(str_replace("'"," ",$this->name1)));// no apostrophe's
		//$this->name2=ucwords(strtolower(str_replace("'"," ",$this->name2)));
		//echo "<Br/>".$this->name1." ".$this->name2."<Br/>";
		
		if ($this->other_field = " - Other - ")
			$this->other_field = "";
		else
			$this->other_field = trim($this->other_field);
			
		$this->address=ucwords(strtolower(str_replace("'"," ",$this->address)));

		//Copied from the quote mailer.
		$this->other_field=strtolower(str_replace("'"," ",$this->other_field));		
		$this->ipaddress=strtolower(str_replace("'"," ",$this->ipaddress));		
		$this->comments=ucwords(strtolower(str_replace("'"," ",$this->comments)));		

		$this->scrub_notes=ucwords(strtolower(str_replace("'"," ",$this->scrub_notes)));		
		$this->scrub_status=ucwords(strtolower(str_replace("'"," ",$this->scrub_status)));		
		
		$this->name1=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$this->name1)));
		$this->name2=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$this->name2)));
		$this->email=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","", $this->email));
		
		$this->phone1=preg_replace("/([^0-9]|\\\)/i",'',$this->phone1);
		$this->phone2=preg_replace("/([^0-9]|\\\)/i",'',$this->phone2);
		
		$this->city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$this->city)));
		$this->state_code=strtoupper($this->state_code);
		
		$this->keyword=str_replace("'","",str_replace("+"," ",strtolower($this->keyword)));
		$this->source=str_replace("'","",strtolower( $this->source));
		
		if ( $this->sqr_footage == 1 ) {
			$this->sqr_footage = "0";
		} 
	}
	/*function verify_locale()
	{		
		$sql = "select zip from movingdirectory.zip_codes where city = '".$this->city."' and state='".$this->state_code."'; ";
		$rs = new mysql_recordset($sql);
		$found = false;
		while ($rs->fetch_array() && !$found)
		{
			$zip = $rs->myarray["zip"];
			$found = ($zip == $this->zip);
		}
		if (!$found)
		{
			//shrink the city...
			
		
		}
		return $found;		
	}*/
	
	function get_save_sql()
	{
		$ip = $_SERVER['REMOTE_ADDR'];
/* This SQL doesn't properly enter the campaign into the Db.
		$sql = "insert  into irelocation.leads_security ".
				"(received, source, referrer, quote_type, name1, name2, phone1, phone2, ".
				"email, address, city, state_code, zip, ".
				"current_needs, building_type, num_location, sqr_footage, own_rent, ".
				"fire, access, cctv, other_field, comments,campaign,remote_ip) ".
				"values ('','".$this->source."','".$this->referrer."','".$this->quote_type.
				"','".$this->name1."','".$this->name2."','".$this->phone1."','".$this->phone2.
				"','".$this->email."','".$this->address."','".$this->city."','".$this->state_code.
				"','".$this->zip."','".$this->current_needs."','".$this->building_type."','".
				$this->num_location."','".$this->sqr_footage."','".$this->own_rent."','".$this->fire.
				"','".$this->access."','".$this->cctv."','".$this->other_field."','".$this->comments.
				"','".$this->country."','$ip')";
*/
		
		$received = date("YmdHis");
		
		$sql = "insert  into irelocation.leads_security ".
				"(received, source, referrer, quote_type, name1, name2, phone1, phone2, ".
				"email, address, city, state_code, zip, ".
				"current_needs, building_type, num_location, sqr_footage, own_rent, ".
				"fire, access, cctv, other_field, comments,campaign,remote_ip,scrub_notes,scrub_status) ".
				"values ('$received','".$this->source."','".$this->referrer."','".$this->quote_type.
				"','".$this->name1."','".$this->name2."','".$this->phone1."','".$this->phone2.
				"','".$this->email."','".$this->address."','".$this->city."','".$this->state_code.
				"','".$this->zip."','".$this->current_needs."','".$this->building_type."','".
				$this->num_location."','".$this->sqr_footage."','".$this->own_rent."','".$this->fire.
				"','".$this->access."','".$this->cctv."','".$this->other_field."','".$this->comments.
				"','ushome','" . $this->ipaddress . "','".$this->scrub_notes."','".$this->scrub_status."')";
		return $sql;
	}
	
	function save_quote()
	{
		$sql = $this->get_save_sql();
		if ($this->source != "aff_robstest")
			mail("code@irelocation.com","Ext Sec Lead - ".$this->source,$sql);
		$rs = new mysql_recordset($sql);
		return $rs->last_insert_id();
	}

}



class residential_quote extends security_quote
{	   
	function residential_quote()
	{
		$this->quote_type='res';		
	}
}

class business_quote extends security_quote
{	   
	function business_quote()
	{
		$this->quote_type='com';		
	}
}
?>