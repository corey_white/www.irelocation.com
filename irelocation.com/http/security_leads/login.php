<?
	session_start();
	
	if (strlen($_SESSION['source']) > 0)
	{
		header("Location: act_login.php");
		exit();	
	}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Affiliate Login</title>
<link href="css.css" rel="stylesheet" type="text/css" />
	</head>

<body>
	<div align="center">
	<br/><br/><br/><br/>
	<form action="act_login.php" method="post">
		<table class='mainsecuritytopic' >
			<? if (isset($_REQUEST['invalid'])) { ?>
				<tr><Td colspan="2">
					<font color="#FF0000">
						<strong>
							Error, invalid login.
						</strong>
					</font>
				</Td></tr>
			<? } ?>
			<Tr class="off">
				<th colspan="2" align="right">Affiliate Security  Leads</th>
			</Tr>
			<tr><Td colspan="2">&nbsp;</Td></tr>
			<tr>
				<td align="left">Username</td>
				<td><input type='text' name="username" /></td>
			</tr>
			<tr>
				<td align="left">Password</td>
				<td><input type='password' name="password" /></td>
			</tr>
			<Tr>
				<td colspan="2" align="right"><input type='submit' value="Login" /></td>
			</Tr>
		</table>
	</form>
</div>
</body>
</html>
