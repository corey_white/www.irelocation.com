<?
//-- exclude ABCLeads
if ( eregi("abcleads",$source) ) {
    echo "Service Discontinued.";
    exit;
} 

//-- exclude yakov
if ( eregi("yakov",$source) ) {
    echo "Service Discontinued.";
    exit;
} 

//-- exclude leadto_realty
if ( eregi("leadto_realty",$source) ) {
    echo "Service Discontinued.";
    exit;
} 

//-- exclude coregmedia
if ( eregi("coregmedia",$source) ) {
    echo "Service Discontinued.";
    exit;
} 

	include "../inc_mysql.php";
	include "security_quote.php";

	/*
		TEST: 
		
http://irelocation.com/security_leads/leads.php?lead_type=res&email=randomkeystrokes@gmail.com&phone=4807857400&address=5010%20S.%20Birch&city=Tempe&state=AZ&needs=upgrade&source=davestest&own_rent=own&fname=Testy&lname=McTesterson&sqr_footage=15&building_type=condo&comments=hi
		
http://irelocation.com/security_leads/leads.php?lead_type=res&email=DZLDOG@gmail.com&phone=4805801234&address=5010%20S.%20Birch&city=Mesa&state=AZ&zip=85214&needs=upgrade&source=ltr&own_rent=own&fname=ROB&lname=HAN&sqr_footage=15&building_type=house&comments=This%20is%20a%20test
		
Schema: All text fields need to be urlencoded;

lead_type - res or com, for Residential or Commercial
email - 						must be a valid email address
phone - 					10 digits, all numeric
address - 				url encoded text
city - 						url encoded text
state - 						2 character standard abbreviation
needs - 					'new', 'upgrade', 'replace'
source - 					<YOUR IRELOCATION SOURCE CODE>
own_rent - 				'own', 'rent'
fname - 					url encoded text
lname - 					url encoded text
sqr_footage - 			url encoded text, max of 6 chars
building_type - 		url encoded - 'house','apartment','condo','mobile home','town house','office','retail','restaurant','home-based','warehouse','industrial','other'
comments - 				url encoded text, max of 250 chars
ipaddress - 				IP Address of the user
	
EXAMPLE:

http://irelocation.com/security_leads/leads.php?lead_type=res&email=rob%40irelocation.com&phone=4807857400&address=335+N.+Austin+Drive&city=Chandler&state=AZ&zip=85226&needs=new&source=test1234&own_rent=own&fname=Tess&lname=Ting&sqr_footage=15&building_type=house&comments=This+is+a+test.&ipaddress=111.222.333.444
	
	*/
	

	//tests for link injections or code injections.
	function failInjection($type,$data)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		$useragent = $_SERVER['HTTP_USER_AGENT'];
		$thispage = $_SERVER['SCRIPT_NAME'];
		$server = $_SERVER['SERVER_NAME'];

		$email = "IP: $ip\n\nUserAgent: $useragent\n\nPage: $thispage\n\nServer: $server\n\nTime: ".date("YmdHis")."\n\n";
		$email .= "Posted Data: ".strtolower(print_r($data,true));
		mail("code@irelocation.com,rob@irelocation.com","***FORM LINK INJECTION - ".$type,$email);
		echo "<strong>Link Injections</strong>: Do not let your visiters add live links, javascript or other malicious code to your forms. please prevent this from happening.<Br/>";
		exit();
	}
	
	//test for various bad things
	//ah yes, we call him "little bobby drop tables";
	function testInjection($array)
	{
		$output = strtolower(print_r($array,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript,$array);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link',$array);	
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside',$array);
	}		
	
	//accept post or request variables.
	if ($_REQUEST['method'] == 'post')
	{
		extract($_POST);	
		unset($_POST['thankyoupage']);
		testInjection($_POST);
		
		
		if ($rejectpage == "" && $thankyoupage == "")
		{
			echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
			echo "<a href='leads.php?method=request'>use REQUEST/GET variables</a><Br/>\n";
		}
		if (count($_POST) > 0 && substr_count($_POST['source'],"daves") == 0)
			mail("rob@irelocation.com","POSTED Security Lead",print_r($_POST,true));
	}
	else
	{
		extract($_REQUEST);		
		unset($_REQUEST['thankyoupage']);
		testInjection($_REQUEST);

		if ($rejectpage == "" && $thankyoupage == "")
		{
			echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
			echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
		}
		if (count($_REQUEST) > 0 && substr_count($_REQUEST['source'],"aff_") == 0)
			mail("rob@irelocation.com","REQUEST Security Lead",print_r($_REQUEST,true));
	}

	//these guys are lame, so we don't work with them anymore.
	if ($source == "rlc")
	{
		echo "Your account has been deactivated. <Br/>We are no longer accepting leads from your company.";
		exit();
	}
	
	$lead_type = strtolower($lead_type);
	$msg = "";
	
	//include type specific validation pages.
	if ($lead_type != "res" && $lead_type != "com")
	{
		$msg = "<strong>lead_type</strong>: Lead Type: Residential: res, ".
				"or Commercial: com<Br/>\n";
		//default includes.
		include "common.php";	
		include "res.php";
	}
	else
	{
		include "common.php";
		include $lead_type.".php";
	}

	//default number of homes a person has is 1..
	if (is_numeric($locations) && $locations != 0)
		$num_locations = $locations;
	else
		$num_locations = 1;//1


	//check for duplicates. more strict with affiliates.
	$source = "aff_$source";
	if (strlen($msg) == 0)
	{
		$ninetydays = date("YmdHis",time() - 90*24*60*60);		
		$select = "select * From irelocation.leads_security where quote_type = '$lead_type' and ".
				  " (email = '$email' or phone1 = '$phone') and zip = '$zip' and ".
				  " zip = '$zip' and (received = '' OR received is null ".
				  " OR received = '0' OR received > '$ninetydays') limit 1 ";
		$rs = new mysql_recordset($select);
		if ($rs->fetch_array())
		{
			$msg .= "<strong>Duplicate</strong>: This lead has been found ".
					"to be a duplicate.<Br/>\n";
		}
	}	
	
	/*
		Adding ServiceObjects Checking as of 2:55pm, 11/28		
	
	*/	
	if (strlen($msg) == 0)
	{
		$data = ($_REQUEST['method'] == 'post')?print_r($_POST,true):print_r($_REQUEST,true);
			
		mail("rob@irelocation.com","Affiliates Security Lead",$data);
		
		include "serviceobjects.php";
	
		$params['Name'] = $name1." ".$name2;
		$params['Address1'] = $address;
		$params['Address2'] = '';
		$params['City'] = $city;
		$params['State'] = $state;
		$params['Zip'] = $zip;
		$params['Country'] = 'USA';
		$params['Phone1'] = $phone;
		$params['Phone2'] = $phone2;
		$params['Email'] = $email;
		$params['IP'] = $ipaddress;
		$params['TestType'] = "normal1p";
		$params['LicenseKey'] = SO_IRELO_KEY;
		
		$result = validateLead($params);
		
		$original_score = $result['OverallCertainty'];
		
		//wireless phones arnt that bad.
		if ($result['WarningCode1'] == WIRELESS_CODE)
			$result['OverallCertainty'] += WIRELESS_COMP;
		else if ($result['WarningCode2'] == WIRELESS_CODE)
			$result['OverallCertainty'] += WIRELESS_COMP;
		
		$valid = ($result['OverallCertainty'] >= THRESHOLD)?"1":"-1";
		
		$scrub_id = $result['OverallCertainty'];
		$scrub_is_valid = $valid;
		if ($valid == "-1")
		{
			$msg .= "<strong>Invalid Quality</strong>: This lead has not met our quality ".
			"standards.<Br/>\n";			
			
			mail("rob@irelocation.com","Affiliates Security Lead Failed", "Original Score = $original_score\n\n" . 
					print_r($result,true)."\nSource: ".$data);
					
			if ( eregi("yakov",$source) ) {
				mail("rob@irelocation.com,yakov.business@gmail.com,michael.business@gmail.com","Affiliates Security Lead Failed ($source)", "Original Score = $original_score\n\n" .  
						print_r($result,true)."\nSource: ".$data);
				
			} 
		}
	}
	
	
	
	if (strlen($msg) == 0)
	{
		if ($lead_type == "res")
			$quote = new residential_quote();		
		else
			$quote = new business_quote();
		

		$quote->source = $source;   
	
		$quote->referrer = "";
		
		$quote->name1 = $name1;        
		$quote->name2 = $name2;        
		$quote->phone1 = $phone;       
		$quote->phone2 = "";       
		$quote->email = $email;        
		$quote->address = $address;      
		$quote->city = $city;         
		$quote->state_code = $state;  
		$quote->zip = $zip;          	
		$quote->current_needs = $needs;
		$quote->building_type = $building_type; 
		$quote->num_location = $num_locations;                           
		$quote->sqr_footage = $sqr_footage;  
		$quote->own_rent = $own_rent;     
		$quote->comments = $comments;   
		$quote->fire=0;
		$quote->other="";
		$quote->cctv=0;
		$quote->access=0;
		$quote->scrub();
		$quote->country = $country;
		$quote_id = $quote->save_quote();
		
		if ($scrub_id != "" && $quote_id != "")
		{
			$sql = "update leads_security set scrub_id = $scrub_id, scrub_is_valid = $scrub_is_valid ".
					" where quote_id = $quote_id ";
			$rs = new mysql_recordset($sql);
		}
		
		if ($thankyoupage == "")
		{
			echo "Lead Accepted: S".$quote_id."<br/>";
		}
		else
		{
			header("Location: ".$thankyoupage."?source=irelo&tracking_id=S".$quote_id);
			exit();
		}
	}
	else
	{	
		//something went wrong..
		
		if (isset($rejectpage))
		{
			//go to their failure page, only useful if they are sending the lead over on the fly.
			header("Location: $rejectpage/?error=".urlencode($msg));
			exit();
		}
		else
		{
			//show the default error page. not for consumers to see, for affiliates.
			echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
			echo "<strong>comments:</strong>: Append this to add customer comments to the lead.<br/>\n";
			echo "<br/><strong>Notes:</strong><Br/> Append a variable <strong>thankyou</strong> to have us redirect the users browser to your thankyou page.<br/> we will append tracking information for you to save:<Br/><Br/> &nbsp; &nbsp; &nbsp; [your_thank_you_page]?source=irelo&tracking_id=[unique_tracking_id]<br/>";
			echo "<br/>Append a variable <strong>rejectpage</strong> to have us redirect the user to a page if the lead fails submission<Br/>";
			echo "<Br/><font color='red'><strong>UPDATE:</strong></font><br/>Append the variable <strong>country</strong> as ca to specify Canada leads. Otherwise all leads will be seen as United States leads.<Br/>";
			echo "<Br/><strong>Duplicates:</strong>We Check for duplicate leads: if the following information is the same it is considered a duplicate.<Br/>";
			echo "<ul><li>(Phone Number or Email Address) AND</li><li>City + State</li>".
			"</ul>";
		}
	}	

?>