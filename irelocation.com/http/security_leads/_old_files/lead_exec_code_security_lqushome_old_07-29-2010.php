<?php 
//-- This code sends the lead data to the LeadExec software Db.

//-- Clean up phone2
if ( !is_numeric($phone2) || strlen($phone2) != 10 ) {
	$phone2 = "";
}


//-- Split Business and Personal names
if ($quote_type == "com") {
	list($fname,$lname) = split(" ",$name2);
	
	$businessname = $name1;	
	
	$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
	$lname = substr($lname,0,20);//same with contact name, or long lastname.
	$name1 = $fname;
	$name2 = $lname;

} else {
	$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
	$name2 = substr($name2,0,20);//same with contact name, or long lastname.
	
	$businessname = "";	

}


//-- This code will hack apart the source code to get just the source root
if ( preg_match("/clkbth/i", $source) ) {
   $source = str_replace("clkbth","clkbth_",$source);
} 
$source = str_replace("aff_","",$source);
$feh = explode("_",$source);
$source_root = $feh[0];

//-- set the marketing type
if ( $source_root == "clkbth" ) {
    $marketing_type = "email";
} else {
    $marketing_type = "cpc";
}


$CAMPAIGN = "ushome";			//-- which campaign these leads are for
$POST_URL = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver";			//-- Posting URL

$LEAD_BODY = 
"VID=1340&".
"AID=2326&".
"LID=371&".
"quote_id=$quote_id&".
"received=&".
"source=".urlencode($source)."&".
"referrer=".urlencode($referer)."&".
"quote_type=res&".
"firstname=".urlencode($name1)."&".
"lastname=".urlencode($name2)."&".
"business_name=".urlencode($businessname)."&".
"phone1=".urlencode($phone)."&".
"phone2=".urlencode($phone2)."&".
"email=".urlencode($email)."&".
"address=".urlencode($address)."&".
"city=".urlencode($city)."&".
"state_code=".urlencode($state)."&".
"zip=".urlencode($zip)."&".
"current_needs=".urlencode($needs)."&".
"building_type=".urlencode($building_type)."&".
"num_location=".urlencode($num_locations)."&".
"sqr_footage=".urlencode($sqr_footage)."&".
"own_rent=".urlencode($own_rent)."&".
"scrub_notes=".urlencode($scrub_notes)."&".
"comments=".urlencode($comments)."&".
"campaign=".urlencode($CAMPAIGN)."&".
"marketing_type=".urlencode($marketing_type)."&".
"source_root=".urlencode($source_root)."&".
"remote_ip=".urlencode($_SERVER['REMOTE_ADDR']);


function leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY) {	
	global $source;
	
	$ch=curl_init($POST_URL."?".$LEAD_BODY);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
	$response = curl_exec($ch);
	curl_close($ch);
	
	$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
	#echo "<br />-------------------------<br />LeadExec Repsonse <br />Url: ".$POST_URL."?".$LEAD_BODY." <br/>-------------------------<br/>Response: $response<br/>-------------------------<br />";
	
	if (substr_count(strtolower($response),"lead was accepted") == 1) { 
	
		mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN","$content");
		#save_leadexec_response($QUOTE_ID, "Lead Accepted :: $response",1,"$CAMPAIGN");

	} else {
		
		if ( $source != "alertbot" ) {
			mail("rob@irelocation.com","LeadExec Repsonse Failure - $CAMPAIGN","$content \n\n$response\n\nsource=$source");
		}

		#save_leadexec_response($QUOTE_ID, "Failure - $response",0,"$CAMPAIGN");
	
	}
}

//save the response in this table. mainy used for security leads.
function save_leadexec_response($quote_id, $result, $sucess, $site) {

	$result = addslashes($result);
	$success = addslashes($sucess);
	$site = addslashes($site);
	
	$sql = "insert into movingdirectory.lead_exec_responses_transcription (quote_id,result,success,site) values ($quote_id,'$result',$sucess,'$site')";
#mail("rob@irelocation.com","LEAD EXEC SQL","$sql");
	$rs = new mysql_recordset($sql);	
	
}		

leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY);
 ?>