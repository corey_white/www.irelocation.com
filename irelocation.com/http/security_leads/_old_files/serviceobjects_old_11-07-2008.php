<?
	define(THRESHOLD,75);
	define(ADDRESS_COMP,1.23);
	define(WIRELESS_COMP,10);
	define(WIRELESS_CODE,64);
	
	include_once "../inc_mysql.php";
	require_once('../nusoap/nusoap.php');
	
	function validateLead($params)
	{
		//Set Up Soap Client for LeadValidation
		$soapClient = new SoapClient( SO_WSDL_URL, "wsdl" );
		
		//Set up GetContactInfo's parameters
		
		
		//Call ValidateLead Operation
		$proxy = $soapClient->getProxy();
		$startTime = time() + microtime();
		$result = $proxy->ValidateLead($params);
		$queryTime = time() + microtime() - $startTime;
		$result = $result['ValidateLeadResult'];
		
		return $result;
	}
	
	
	define(SO_IRELO_KEY,"WS40-MPV1-FLT4");
	define(SO_WSDL_URL,"http://ws.serviceobjects.com/lv/leadvalidation.asmx?WSDL");
	
	function getParamsFromSecurityArray($rsarray)
	{
		//Setup Parameter Values
		$name = $rsarray['name1']." ".$rsarray['name2'];
		$city = $rsarray['city'];
		$state = $rsarray['state_code'];
		$zip = $rsarray['zip'];
		$phone = $rsarray['phone1'];
		$type = 'noip';		
		if ($rsarray['phone2'] != "")
		{
			$phone2 = $rsarray['phone2'];

		}
			
		$email = $rsarray['email'];
		//if ($rsarray['source'] != "aff_ysm")
		//	$ip = $rsarray['remote_ip'];
		//else
			$ip = "";
			
		$address = $rsarray['address'];
		
		$country = ($rsarray['campaign']=="us")?"USA":"CN";
		
		$key = SO_IRELO_KEY;
	
		$params['Name'] = $name;
		$params['Address1'] = $address;
		$params['Address2'] = '';
		$params['City'] = $city;
		$params['State'] = $state;
		$params['Zip'] = $zip;
		$params['Country'] = 'USA';
		$params['Phone1'] = $phone;
		$params['Phone2'] = $phone2;
		$params['Email'] = $email;
		$params['IP'] = $ip;
		$params['TestType'] = $type;
		$params['LicenseKey'] = $key;
		
		return $params;
	}
	
	function getParams($quote_id)
	{
		$sql = "select * from irelocation.leads_security where ".
				" quote_id = '".$quote_id."'";
		echo "SO.getParams:".$sql."<br/>";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		

		return getParamsFromSecurityArray($rs->myarray);	
	}
	
	function validateLeadQuoteId($quote_id)
	{
		$params = getParams($quote_id);
		$result = validateLead($params);
		return processResults($quote_id,$result);
	}
	
	function validateLeadQuality($rsarray)
	{
		$params = getParamsFromSecurityArray($rsarray);
		$result = validateLead($params);
		return processResults($rsarray['quote_id'],$result);
	}
	
	function processResults($quote_id,$result)
	{
		if ($result['WarningCode1'] == 64)
			$result['OverallCertainty'] += WIRELESS_COMP;
		else if ($result['WarningCode2'] == 64)
			$result['OverallCertainty'] += WIRELESS_COMP;
			
		$overall = $result['OverallCertainty'];
		

		
		$strresult = print_r($result,true);
		$strresult = ereg_replace("(\[|\])","",$strresult);
		$strresult = ereg_replace("  *"," ",$strresult);
		$strresult = ereg_replace("'","\'",$strresult);
		if (is_array($strresult))		
			echo "WHY IS IT AN ARRAY!!<br/>";
			
		$sql = "insert into movingdirectory.so_results ".
		"(quote_id, so_score, adjusted_score, rawata) values ".
		"('$quote_id', '".$result['OverallCertainty']."','".$overall."'".
		",'".$strresult."')";
		echo "SO Results: ".$sql."<br/>";
		
		echo "Overall: ". $result['OverallQuality']."<Br/>";
		$valid = ($result['OverallCertainty'] >= THRESHOLD)?"1":"-1";
		
		//$valid = ($result['OverallQuality']=="Accept")?"1":(($result['OverallQuality']=="Review")?"0":"-1");
		
		$sql = "update irelocation.leads_security set scrub_id = '".$result['OverallCertainty']."', ".
				" scrub_is_valid = '$valid' where quote_id = '".$quote_id."' ";
		echo "update sql: $sql<br/>";
		$rs = new mysql_recordset($sql);
		
		return ($overall > THRESHOLD);
	}
	
	if (strlen(($quote_id = $_REQUEST['test'])) != 0)
	{
		validateLeadQuoteId($quote_id);
	
	}
	
?>