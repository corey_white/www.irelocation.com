<?
	if (strlen($company) == 0)
	{
		$msg .= "<strong>company</strong>: A Company Name is Required<Br/>\n";
	}
	else
		$name1 = $company;
		
	if (strlen($contact) == 0)
	{
		$msg .= "<strong>contact</strong>: contact name is Required<Br/>\n";
	}
	else
		$name2 = $contact;
		
	if (strlen($sqr_footage == 0))
	{
		$msg .= "<strong>sqr_footage</strong>: a square footage rating is required:\n".
					"<ul><li>0: 0 - 2,499 ft</li>
		  <li>25: 2,500 - 4,999 ft</li>
		  <li>50: 5,000 - 9,999 ft</li>

		  <li>100: 10,000 - 49,999 ft</li>		  
		  <li>500: 50,000+ ft</li>"."</ul>\n";				
	}
	if (strlen($building_type) == 0)
	{
		$msg .= "<strong>building_type</strong>: a building type is required:".
		" <ul><li>office</li><li>retail</li><li>restaurant</li><li>home-based</li>".
		"<li>warehouse</li><li>industrial</li><li>other</li></ul>\n";
	}
?>