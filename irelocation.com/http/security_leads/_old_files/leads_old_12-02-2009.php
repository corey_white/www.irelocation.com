<?

//these guys are lame, so we don't work with them anymore.
if ($source == "rlc" || eregi("abcleads",$source) || eregi("yakov",$source) || eregi("leadto_realty",$source) || eregi("coregmedia",$source) || eregi("hsas",$source) ) {
	echo "Your account has been deactivated. <Br/>We are no longer accepting leads from your company.";
	exit();
}


include "../inc_mysql.php";
include "security_quote.php";



//tests for link injections or code injections.
function failInjection($type,$data)
{
	$ip = $_SERVER['REMOTE_ADDR'];
	$useragent = $_SERVER['HTTP_USER_AGENT'];
	$thispage = $_SERVER['SCRIPT_NAME'];
	$server = $_SERVER['SERVER_NAME'];

	$email = "IP: $ip\n\nUserAgent: $useragent\n\nPage: $thispage\n\nServer: $server\n\nTime: ".date("YmdHis")."\n\n";
	$email .= "Posted Data: ".strtolower(print_r($data,true));
	mail("code@irelocation.com","***FORM LINK INJECTION - ".$type,$email);
	echo "<strong>Link Injections</strong>: Do not let your visiters add live links, javascript or other malicious code to your forms. please prevent this from happening.<Br/>";
	exit();
}

//test for various bad things
//ah yes, we call him "little bobby drop tables";
function testInjection($array)
{
	$output = strtolower(print_r($array,true));
	if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
		failInjection(javascript,$array);
	if (substr_count($output,"href=") > 0)//link injection
		failInjection('link',$array);	
	if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
		failInjection('serverside',$array);
}		

//accept post or request variables.
if ($_REQUEST['method'] == 'post')
{
	extract($_POST);	
	unset($_POST['thankyoupage']);
	testInjection($_POST);
	
	
	if ($rejectpage == "" && $thankyoupage == "")
	{
		echo "<strong>Note:</strong> Using POST variables.<Br/>\n";
		echo "<a href='leads.php?method=request'>use REQUEST/GET variables</a><Br/>\n";
	}
	if (count($_POST) > 0 && substr_count($_POST['source'],"daves") == 0)
		mail("code@irelocation.com","POSTED Security Lead",print_r($_POST,true));
}
else
{
	extract($_REQUEST);		
	unset($_REQUEST['thankyoupage']);
	testInjection($_REQUEST);

	if ($rejectpage == "" && $thankyoupage == "")
	{
		echo "<strong>Note:</strong> Using REQUEST/GET variables.<Br/>\n";
		echo "<a href='leads.php?method=post'>use POST variables</a><Br/>\n";
	}
	if (count($_REQUEST) > 0 && substr_count($_REQUEST['source'],"aff_") == 0)
		mail("code@irelocation.com","REQUEST Security Lead",print_r($_REQUEST,true));
}

//these guys are lame, so we don't work with them anymore.
if ($source == "rlc")
{
	echo "Your account has been deactivated. <Br/>We are no longer accepting leads from your company.";
	exit();
}

$lead_type = strtolower($lead_type);
$msg = "";

//include type specific validation pages.
if ($lead_type != "res" && $lead_type != "com")
{
	$msg = "<strong>lead_type</strong>: Lead Type: Residential: res, ".
			"or Commercial: com<Br/>\n";
	//default includes.
	include "common.php";	
	include "res.php";
}
else
{
	include "common.php";
	include $lead_type.".php";
}

//default number of homes a person has is 1..
if (is_numeric($locations) && $locations != 0)
	$num_locations = $locations;
else
	$num_locations = 1;//1


//check for duplicates. more strict with affiliates.
$source = "aff_$source";
if (strlen($msg) == 0)
{
	$ninetydays = date("YmdHis",time() - 90*24*60*60);		
	$select = "select * From irelocation.leads_security where quote_type = '$lead_type' and ".
			  " (email = '$email' or phone1 = '$phone') and zip = '$zip' and ".
			  " zip = '$zip' and (received = '' OR received is null ".
			  " OR received = '0' OR received > '$ninetydays') limit 1 ";
	$rs = new mysql_recordset($select);
	if ($rs->fetch_array())
	{
		$msg .= "<strong>Duplicate</strong>: This lead has been found to be a duplicate.<Br/>\n";
	}
}	


//-- Adding ServiceObjects Checking as of 2:55pm, 11/28		
//-- updating this to push all SO fails to Lead Qual

if (strlen($msg) == 0)
{
	//-- ##################################################  CLICKBOOTH VALIDATION
	//-- If it's a clickbooth lead, do an extra scrub on the lead data
	
	if ( $own_rent == "own" ) { //-- if it's not a renter, then perform extra validation through Service Objects
		
		if (!$quote_type) {
			$quote_type = "res";
		}
		
		//-- create CSV line from data in case we need to send it or save it
		$recvdate = date("Ymdhis");
		
#		$csv_source = str_replace("clkbth","captured",$source);
		
		//-- Need to make sure there are no ' or , in the data 
		$source = ereg_replace(","," ",ereg_replace("'","",$source));
		$quote_type = ereg_replace(","," ",ereg_replace("'","",$quote_type));
		$name1 = ereg_replace(","," ",ereg_replace("'","",$name1));
		$name2 = ereg_replace(","," ",ereg_replace("'","",$name2));
		$phone1 = ereg_replace(","," ",ereg_replace("'","",$phone));
		$phone2 = ereg_replace(","," ",ereg_replace("'","",$phone2));
		$email = ereg_replace(","," ",ereg_replace("'","",$email));
		$address = ereg_replace(","," ",ereg_replace("'","",$address));
		$city = ereg_replace(","," ",ereg_replace("'","",$city));
		$state = ereg_replace(","," ",ereg_replace("'","",$state));
		$zip = ereg_replace(","," ",ereg_replace("'","",$zip));
		$current_needs = ereg_replace(","," ",ereg_replace("'","",$needs));
		$building_type = ereg_replace(","," ",ereg_replace("'","",$building_type));
		$num_location = ereg_replace(","," ",ereg_replace("'","",$num_location));
		$sqr_footage = ereg_replace(","," ",ereg_replace("'","",$sqr_footage));
		$own_rent = ereg_replace(","," ",ereg_replace("'","",$own_rent));


		$data_csv = "csvdata:$source,$quote_type,$name1,$name2,$phone1,$phone2,$email,$address,$city,$state,$zip,$current_needs,$building_type,$num_location,$sqr_footage,$own_rent,$remote_ip:endcsv";
		$data_csv_rob = "$source,$quote_type,$name1,$name2,$phone1,$phone2,$email,$address,$city,$state,$zip,$current_needs,$building_type,$num_location,$sqr_footage,$own_rent,$remote_ip";

		$email_data = "received: $recvdate";
		$email_data .= "\n";
		$email_data .= "source: $source";
		$email_data .= "\n";
		$email_data .= "lead_type: $quote_type";
		$email_data .= "\n";
		$email_data .= "fname: $name1";
		$email_data .= "\n";
		$email_data .= "lname: $name2";
		$email_data .= "\n";
		$email_data .= "phone: $phone1";
		$email_data .= "\n";
		$email_data .= "phone2: $phone2";
		$email_data .= "\n";
		$email_data .= "email: $email";
		$email_data .= "\n";
		$email_data .= "address: $address";
		$email_data .= "\n";
		$email_data .= "city: $city";
		$email_data .= "\n";
		$email_data .= "state: $state";
		$email_data .= "\n";
		$email_data .= "zip: $zip";
		$email_data .= "\n";
		$email_data .= "needs: $current_needs";
		$email_data .= "\n";
		$email_data .= "building_type: $building_type";
		$email_data .= "\n";
		$email_data .= "sqr_footage: $sqr_footage";
		$email_data .= "\n";
		$email_data .= "own_rent: $own_rent";
		$email_data .= "\n";
		$email_data .= "ipaddress: $ipaddress";
		$email_data .= "\n";
		
		
		include "serviceobjects.php";

		$params['Name'] = $name1." ".$name2;
		$params['Address1'] = $address;
		$params['Address2'] = '';
		$params['City'] = $city;
		$params['State'] = $state;
		$params['Zip'] = $zip;
		$params['Country'] = 'USA';
		$params['Phone1'] = $phone;
		$params['Phone2'] = $phone2;
		$params['Email'] = $email;
		$params['IP'] = $ipaddress;
		$params['TestType'] = "normal1p";
		$params['LicenseKey'] = SO_IRELO_KEY;
		
		$result = validateLead($params);
		
		$original_score = $result['OverallCertainty'];

		//wireless phones arnt that bad.
		if ($result['WarningCode1'] == WIRELESS_CODE)
			$result['OverallCertainty'] += WIRELESS_COMP;
		else if ($result['WarningCode2'] == WIRELESS_CODE)
			$result['OverallCertainty'] += WIRELESS_COMP;

		$valid = ($result['OverallCertainty'] >= THRESHOLD)?"1":"-1";
		
		$scrub_id = $result['OverallCertainty'];
		$scrub_is_valid = $valid;
		
		#mail("code@irelocation.com","BAD ClickBooth Security Lead from USAlarm ", "Original Score = $original_score\n\n$email_data Page: " .  $_SERVER['SCRIPT_FILENAME'] . "\n\n$data_csv \n\n");

		if ( $valid == -1 ) {
			$msg .= "<strong>Invalid Quality</strong>: This lead has been sent on for further validation.  If it validates, we will accept it into our database.<Br/>\n";			
			
			//-- This will be the email that goes to Lead Qual
			mail("code@irelocation.com, 869@leadqual.com","BAD ClickBooth Security Lead from Affiliate Import ", "Original Score = $original_score\n\n$email_data Page: " .  $_SERVER['SCRIPT_FILENAME'] . "\n\n$data_csv \n\n","FROM: reports@alarmsystemsnow.com");
			
			#mail("code@irelocation.com","BAD ClickBooth Security Lead from Affiliate Import ", "Original Score = $original_score\n\n$email_data Page: " .  $_SERVER['SCRIPT_FILENAME'] . "\n\n$data_csv \n\n","FROM: reports@alarmsystemsnow.com");
			
			//-- This is the email that goes to me and has the SO output in it.
			mail("code@irelocation.com","BAD ClickBooth Security Lead from Affiliate Import ---", "Original Score = $original_score\n\n$email_data Page: " .  $_SERVER['SCRIPT_FILENAME'] . "\n\n$data_csv_rob \n\nOriginal Score = $original_score\n\n" . print_r($result,true),"FROM: reports@alarmsystemsnow.com");

		} else {
			#mail("code@irelocation.com","GOOD ClickBooth Security Lead ", "Original Score = $original_score\n\n$email_data Page: " .  $_SERVER['SCRIPT_FILENAME'] . "\n\n$data_csv \n\n"); //-- THIS WOULD NEED TO BE UPDATED BEFORE REACTIVATED
		}
	} 
	//-- ##################################################  END CLICKBOOTH VALIDATION
}



if (strlen($msg) == 0)
{
	if ($lead_type == "res")
		$quote = new residential_quote();		
	else
		$quote = new business_quote();
	

	$quote->source = $source;   

	$quote->referrer = "";
	
	$quote->name1 = $name1;        
	$quote->name2 = $name2;        
	$quote->phone1 = $phone;       
	$quote->phone2 = $phone2;       
	$quote->email = $email;        
	$quote->address = $address;      
	$quote->city = $city;         
	$quote->state_code = $state;  
	$quote->zip = $zip;          	
	$quote->current_needs = $needs;
	$quote->building_type = $building_type; 
	$quote->num_location = $num_locations;                           
	$quote->sqr_footage = $sqr_footage;  
	$quote->own_rent = $own_rent;     
	$quote->comments = $comments;   
	$quote->fire=0;
	$quote->other="";
	$quote->cctv=0;
	$quote->access=0;
	$quote->scrub();
	$quote->country = $country;
	$quote_id = $quote->save_quote();
	
	if ($scrub_id != "" && $quote_id != "")
	{
		$sql = "update leads_security set scrub_id = $scrub_id, scrub_is_valid = $scrub_is_valid ".
				" where quote_id = $quote_id ";
		$rs = new mysql_recordset($sql);
	}
	
	if ($thankyoupage == "")
	{
		echo "Lead Accepted: S".$quote_id."<br/>";
	}
	else
	{
		header("Location: ".$thankyoupage."?source=irelo&tracking_id=S".$quote_id);
		exit();
	}
}
else
{	
	//something went wrong..
	
	if (isset($rejectpage))
	{
		//go to their failure page, only useful if they are sending the lead over on the fly.
		header("Location: $rejectpage/?error=".urlencode($msg));
		exit();
	}
	else
	{
		//show the default error page. not for consumers to see, for affiliates.
		echo "<strong>Lead Parsing Error:</strong><br>\n<hr/>\n".$msg;
		echo "<strong>comments:</strong>: Append this to add customer comments to the lead.<br/>\n";
		echo "<br/><strong>Notes:</strong><Br/> Append a variable <strong>thankyou</strong> to have us redirect the users browser to your thankyou page.<br/> we will append tracking information for you to save:<Br/><Br/> &nbsp; &nbsp; &nbsp; [your_thank_you_page]?source=irelo&tracking_id=[unique_tracking_id]<br/>";
		echo "<br/>Append a variable <strong>rejectpage</strong> to have us redirect the user to a page if the lead fails submission<Br/>";
		echo "<Br/><font color='red'><strong>UPDATE:</strong></font><br/>Append the variable <strong>country</strong> as ca to specify Canada leads. Otherwise all leads will be seen as United States leads.<Br/>";
		echo "<Br/><strong>Duplicates:</strong>We Check for duplicate leads: if the following information is the same it is considered a duplicate.<Br/>";
		echo "<ul><li>(Phone Number or Email Address) AND</li><li>City + State</li>".
		"</ul>";
	}
}	

?>