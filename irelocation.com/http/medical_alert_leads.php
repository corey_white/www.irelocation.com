<?
$pageTitle = "Medical Alert System Leads | iRelocation Network";
$metaDescription = "Medical alert system leads provided by iRelo are accurate and affordable. Join our prestigious team of medical alert systems companies now!";
$metaTags = "medical alert system leads, medical alert leads, medical alert companies, medical alert device leads";
include("irelocation/includes/header.php");
?>
<div class="int_content">  
    <h1>Medical Alert Leads</h1>
    <p>At iRelocation Network, we know that good leads should contain detailed, accurate information. Medical alert system leads from iRelo hit both of those marks precisely. We use organic traffic generated by our studiously crafted websites to provide quality medical alert leads to customers. Our highly targeted PPC campaigns generate a large volume of leads for our clients, and help you convert on sales.</p>
    <p>We have worked with some of the best medical alert systems companies in the business. When you become an iRelocation Network client, you will join our team of highly respected clients. As such, you will receive superior customer support - we will work with you to ensure your medical alert leads are of the quality you need to grow your business now!</p>
</div>

<div class="int_form">
    <?php include('irelocation/int_form.php');?>
</div>
</div>
<? include'irelocation/includes/footer.php'; ?>