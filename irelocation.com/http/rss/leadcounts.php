<?
	include_once "../inc_mysql.php";
	
	define(TODAY,date("Ymd"));
		
	$queries = array();
	$DATE = " where received like '".TODAY."%' and ";
	define(DATE,$DATE);
	$SELECT_SECURITY = " select count(*) 'count' from irelocation.leads_security ".$DATE;
							   
	$SELECT_AUTO = " select count(*) 'count' from movingdirectory.quotes ".$DATE.
					" cat_id = 1 and ";
	$SELECT_MARBLE = " select count(*) 'count' from marble.auto_quotes ".$DATE;
		  	
	
	$engines = array("google","msn","overture");
	
	$marble_sites = array("cs","asc","mmc","tas","cq","pas");
	$moving_sites = array("tm","me","pm");
	$auto_sites = array("atus","123cm");
	$security = array("tsc","tac");
	$canada = array("ctsc","ctac");
	
	$categories = array("marble" => $marble_sites, "moving" => $moving_sites,
						"atus" => $auto_sites, "security" => $security,
						"canada" => $canada);
	
	$queries['- Top Auto Leads -'] = "";
	
	foreach ($marble_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_MARBLE." source like '".
										$site."_".$engine."%'; ";	
	$queries['- Auto-Transport.us -'] = "";
	
	foreach ($auto_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_AUTO." source like '".
										$site."_".$engine."%'; ";	
	$queries['- Moving Sites -'] = "";
	
	foreach ($moving_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = moving($site,$engine);
	
	$queries['- Security Leads -'] = "";
	
	foreach ($security as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'us'; ";	

	$queries['- Canadian Security Leads -'] = "";
	
	foreach ($canada as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'ca'; ";
	
	
	function getCampaignTitle($category)
	{
		/*
		array("marble" => $marble_sites, "moving" => $moving_sites,
						"atus" => $auto_sites, "security" => $security,
						"canada" => $canada);
		*/
		switch($category)
		{
			case "marble": return "Top Auto Leads";
			case "atus": return "Auto-Transport.us Leads";
			case "moving": return "Moving Leads";
			case "security": return "US Security Leads";
			case "canada": return "CN Security Leads";
		}
	}
	
	function getTitle($code)
	{
		switch($code)
		{
			case "asc": return "AutoShipping.com";
			case "cs": return "CarShipping.com";
			case "cq": return "CarShippingQuote.com";
			case "mmc": return "MoveMyCar.com";
			case "atus": return "Auto-Transport.us";
			case "123cm": return "123CarMoving.com";
			case "pas": return "ProAutoShipping.com";
			case "tas": return "TopAutoShippers.com";
						
			case "tsc": return "TopSecurityCompanies.com";
			case "tac": return "TopAlarmCompanies.com";
			
			case "ctsc": return "Canada.TopSecurityCompanies.com";			
			case "ctac": return "Canada.TopAlarmCompanies.com";
			
			case "tm": return "TopMoving.com";
			case "pm": return "ProMoving.com";
			case "me": return "MovingEase.com";					
		}	
	}
	
	function getCost($site, $engine)
	{
		$sql = "select sum(cost) 'cost' from movingdirectory.costperclick where ".
				" ts like '".TODAY."%' and site like '".$site."%' and ".
				" search_engine = '".$engine."'; ";
		//echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$cost = $rs->myarray['cost'];
		$rs->close();
		return $cost;
	}
										
	function moving($site,$engine)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source like '".
				$site."_".$engine."%' and received like '".TODAY."%' and cat_id in".
				" (2,3) UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."_".$engine."%' and ".
				" received like '".TODAY."%'";	
	}
	
	function movingOrganic($site,$not)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source".
				" like '".$site."%' and received like '".TODAY."%' and cat_id in".
				" (2,3) $not UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."%' and ".
				" received like '".TODAY."%' $not";	
	}
	
	function getOrganicSql($site)
	{
		$not = "";
		$engines = array("google","msn","overture");
		foreach($engines as $e)
			$not .= " and source not like '".$site."_".$e."%' ";	
		
		switch($site)
		{
			case "tm":
			case "me":
			case "pm":
				$sql = movingOrganic($site,$not);
				break;
			case "asc":
			case "mmc":
			case "cs":
			case "cq":
			case "tas":
			case "pas":
				$sql = " select count(*) 'count' from marble.auto_quotes where ".
					   " received like '".TODAY."%' $not and source like ".
					   "'".$site."%' ";
				break;
			case "atus":
			case "123cm":
				$sql = " select count(*) 'count'  from movingdirectory.quotes where".
					   " received like '".TODAY."%' $not and cat_id = 1 and source".
					   " like '".$site."%' ";

				break;
			case "ctsc":
			case "tsc":
			case "tac":
			case "ctac":
				$sql = " select count(*) 'count'  from irelocation.leads_security ".
					   " where received like '".TODAY."%' $not and ".
					   "source like '".$site."%' ";
				break;
		}
		
		$count = 0;
		return $sql;
	}
	
	function getOrganicLeadCount($site)
	{
		$sql = getOrganicSql($site);
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$count += $rs->myarray['count'];
			
		return $count;
	}
	
	function printRSSHeader(
		$title="iRelocation Lead Counts",
		$link="http://www.irelocation.com/reporting/marble/login.php"
	)
	{
		header("Content-Type: text/xml");
		
		$date = date("D j M Y H:i:s T");
		
		echo "<?xml version=\"1.0\" ?>\n 
				<rss version=\"0.91\">
				<channel> 
				    <title>$title</title>
					<ttl>1</ttl>
					<pubDate>$date</pubDate>
				    <link>$link</link>\n";
		//Sun, 19 May 2002 15:21:36 GMT
	}
	
	function runCode($sql)
	{		
		$rs = new mysql_recordset($sql);
		$sum = 0;
		while($rs->fetch_array())
			$sum += $rs->myarray['count'];
		$rs->close();
		return $sum;
	}
	
	
	function getAffiliateSql($category)
	{
		$start = "select count(*) 'count' from ";
		switch($category)		
		{
			case "marble": 
				return $start."marble.auto_quotes ".DATE.
						" source like 'aff%'";
			case "atus": 
				return $start."movingdirectory.quotes ".DATE.
						" source like 'aff%' and cat_id = 1";
			case "moving": 
				return $start."movingdirectory.quotes ".DATE.
						"  source like 'aff%' and cat_id in (2,3) ".
						" UNION ALL ".
						$start."movingdirectory.quotes_local ".DATE.
						" source like 'aff%' ";						
			case "security": 
				return  $start."irelocation.leads_security.quotes ".DATE.
						" source like 'aff%' and campaign = 'us' ";
			case "canada": 
				return $start."irelocation.leads_security.quotes ".DATE.
						" source like 'aff%' and campaign = 'ca' ";
		}
	}
	
	
	function printItem($title,$link,$description)
	{
		echo "<item>\n<title>$title</title>\n";
		echo "<link>$link</link>\n";
		echo "<description>$description</description>\n</item>\n";		
	}
	
	

	
	
	if ($_REQUEST['site'] == "" && $_REQUEST['category'] == "")
	{	
		$sites = array_unique(array_keys($queries));
		echo "Click on a link to add it as an RSS Feed.<br/>";
		foreach($sites as $site)
		{
			if (substr($site,0,1) == "-")
			{			
				continue;
			}
			echo "<a href='http://www.irelocation.com/rss/leadcounts.php?site=$site'>$site - ".getTitle($site)."</a><br/>\n";
		
		}
		/*
		printRSSHeader();
		$sites = array_unique(array_keys($queries));
		foreach($sites as $site)
		{
			if (substr($site,0,1) == "-")
			{			
				continue;
			}
							
			//echo "-- Organic: ".getOrganicSql($site)."\n";
			$total_count = 0;
			$link = "http://www.irelocation.com/reporting/marble/login.php";		
			foreach($engines as $engine)
			{
				$sql = $queries[$site][$engine];
				$count = runCode($sql);
				$total_count += $count;
				$title = ucwords($site)."
				 - ".ucwords($engine).": $count";
				$desc = "lead counts from site: $site and engine: ".ucwords($engine);
				printItem($title,$link,$desc);						
			}
			
			$organic = getOrganicLeadCount($site);
			$total_count += $organic;
			
			$title = ucwords($site)." - Organic: $organic";
			$desc = "organic lead count from $site";
			printItem($title,$link,$desc);		
			
			$title = ucwords($site)." - Total: $total_count";
			$desc = "total lead count from $site";
			
			printItem($title,$link,$desc);										
		}
		*/
		
	}
	else if ($_REQUEST['site'] != "")
	{
		$site = $_REQUEST['site'];
		printRSSHeader(getTitle($site));
		//all data for one site.
		$total_count = 0;
		$link = "http://www.irelocation.com/reporting/marble/login.php";		
		foreach($engines as $engine)
		{
			$sql = $queries[$site][$engine];
			$count = runCode($sql);
			$total_count += $count;
			$title = ucwords($engine).": $count";
			$desc = "lead counts from site: $site and engine: ".ucwords($engine);
			printItem($title,$link,$desc);							
		}
		
		$organic = getOrganicLeadCount($site);
		$total_count += $organic;
		
		$title = "Organic: $organic";
		$desc = "organic lead count from $site";
		printItem($title,$link,$desc);			
		
		$title = "Total: $total_count";
		$desc = "total lead count from $site";
		
		printItem($title,$link,$desc);			
	}
	else if ($_REQUEST['category'] != "")
	{
		$category = $_REQUEST['category'];
		printRSSHeader( getCampaignTitle($category) );
		$sites = $categories[$category];		
		
		$leadcounts = array();
		
		foreach($sites as $site)
		{
			if (substr($site,0,1) == "-")
			{			
				continue;
			}
							
			//echo "-- Organic: ".getOrganicSql($site)."\n";
			$total_count = 0;
			$link = "http://www.irelocation.com/reporting/marble/login.php";		
			foreach($engines as $engine)
			{
				$sql = $queries[$site][$engine];
				$count = runCode($sql);
				$leadcounts[$engine] += $count;
				$leadcounts['total'] += $count;	
			}
			
			$organic = getOrganicLeadCount($site);
			
			$leadcounts['organic'] += $organic;			
			$leadcounts['total'] += $organic;
						
		}
		
		$asql = getAffiliateSql($category);
		$affcount = runCode($asql);
		
		$leadcounts['affiliates'] = $affcount;
		$leadcounts['total'] += $affcount;
		
		foreach($leadcounts as $engine => $count)
		{			
			if ($engine == "total")
				continue;//print last.
			$title = ucwords($engine)." leads: $count";
			printItem($title,$link,"test");											
		}
		
		$title = "Total leads: ".$leadcounts['total'];
		printItem($title,$link,"test");	
	}
	
	echo "</channel></rss>";
?>