<?
session_start();

// Setup Error messages

$error_msg = $_REQUEST["msg"];
$msgs["name"] = "Please enter your Full Name.";
$msgs["phone"] = "Please enter your Phone Number.";
$msgs["company"] = "Please enter your Company Name.";
$msgs["email"] = "Please enter your Email Address.";
$msgs["lead_type"] = "Please select your type of lead.";
$msgs["thankyou"] = "Thank you. We will contact you shortly.";
$msgs["verify"] = "Verification code is invalid.";
			
$msg = $msgs[$error_msg];

extract($_POST);


// is the form ready to check and send            
if ($send == "yes") { 
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$output = strtolower(print_r($_POST,true));
		
		if ($type == "javascript")
		{
			if (substr_count($output,"<script") > 0)
				$content = substr($output,strpos($output,"<script"));						
			else
				$content = substr($output,strpos($output,"onload="));
		}
		else if ($type == "link")
			$content = substr($output,strpos($output,"href="));	
		else if  ($type == "serverside")
		{
			if (substr_count($output,"<?") > 0)
				$content = substr($output,strpos($output,"<?"));		
			else
				$content = substr($output,strpos($output,"<%"));		
		}
		else if ($type == "dirty")
		{
			$content = "Dirty words.";
			header("Location: contact.php?msg=thankyou");
			exit;
		}	
		$content = str_replace("'","&prime;",$content);
		$content = str_replace("\"","&quot;",$content);
		
		$sql = "insert into marble.injectors (site,ip,received,content) values ".
				"('tm','$ip','".date("YmdHis")."','$content'); ";
				
		$rs = new mysql_recordset($sql);
		
		header("Location: contact.php?msg=thankyou");
		exit;
	}

	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");		
	}		
	testInjection();//if it gets past this the input 'should' be ok.
 
	// error checking
	if ($name == "") {
		header("Location: lead_form_test.php?msg=name&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($email == "") {
		header("Location: lead_form_test.php?msg=email&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($company == "") {
		header("Location: lead_form_test.php?msg=company&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($phone == "") {
		header("Location: lead_form_test.php?msg=phone&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($lead_type == "") {
		header("Location: lead_form_test.php?msg=lead_type&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($_SESSION['answer'] != $verify) {
        header("Location: lead_form_test.php?msg=verify&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
        
    } else {
		//passed error checking
 		// to email address
		$to = "katrina@irelocation.com, travis@irelocation.com, vsmith@irelocation.com, markb@irelocation.com";
		// The subject
		$subject = "iRelocation PPC Lead Form";
		// The message
		$message = "Name: ".$name."\n\n";
		$message .= "Company: ".$company."\n\n";
		$message .= "Email: ".$email."\n\n";
		$message .= "Phone: ".$phone."\n\n";
		$message .= "Lead Type: ".$lead_type."\n\n";
		$message .= "Comment: ".$comment;
		mail($to, $subject, $message, "From: $email");
		header("Location: thank_you.php");
		exit;
		
	}
	
} else {

//create numbers for human verification
$q1 = rand(0,10);    
$q2 = rand(0,10);
$_SESSION['answer'] = $q1 + $q2;

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="application/xhtml+xml; charset=utf-8" />
<link href="http://www.irelocation.com/irelocation/ppc-styles.css" rel="stylesheet" type="text/css" />
<title><?php echo $pageTitle; ?></title>
<meta name="description" content="<?php echo $metaDescription; ?>" />
<meta name="keywords" content="<?php echo $metaTags; ?>" />

<!--[if lt IE 7.]>
<script defer type="text/javascript" src="js/pngfix.js"></script>
<![endif]-->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-35525013-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>



<body>
 <!--wrapper-->
 <div id="wrapper-index">
 
 <!--header_index-->
 <div id="header">
 <div id="logo">
  <a href="#"><img src="irelocation/images/logo.png" height="60px" /></a>
 </div>



<div style="float:left; display:block; width:276px; margin:79px 0 0 525px;">
<img src="/irelocation/images/call-us.png" />
</div>
</div>

<div style="clear:both;"></div>

 <!--content_index-->
 <div id="content_index">
<img src="irelocation/images/porsche-main-image.png" width="920px" style="float:left; margin:22px 20px 0 20px"; />

 <div id="form">
   <div style="margin:0 0 5px -20px; padding:1px 0 1px 20px; width:255px; background-color:#c02a2a">
 <h2>GET A CUSTOM LEAD PROPOSAL</h2>
</div>
   <form method="post" action="lead_form_test.php">
  <input name="send" type="hidden" value="yes" />
    <table width="260px" border="0" cellpadding="2" cellspacing="0">
    <?
      	$errmsg='
      	<tr>
      	  <td align="center" colspan="2"><font color="red">'.$msg.'</font></td>
      	</tr>
		';
      	if($msg!="")
      	{
      		echo $errmsg;
      	}
    ?>
      <tr>
        <td>Name: <span class="orange">* </span></td>
        </tr>
        <tr>
        <td><label>
        <input name="name" type="text" id="name" value="<? echo $_REQUEST["name"]; ?>" style="border-color: #c7c7c7; background-color: #e8e8e8; color: #808082; width:230px; margin-bottom:7px;" />
        </label></td>
      </tr>
      <tr>
        <td>Company: <span class="orange">*</span></td>
        </tr>
        <tr>
        <td><input name="company" type="text" id="company" value="<? echo $_REQUEST["company"]; ?>" style="border-color: #c7c7c7; background-color: #e8e8e8; color: #808082; width:230px; margin-bottom:7px;" /></td>
      </tr>
      <tr>
        <td>Email: <span class="orange">*</span></td>
        </tr>
        <tr>
        <td><input name="email" type="text" id="email" value="<? echo $_REQUEST["email"]; ?>" style="border-color: #c7c7c7; background-color: #e8e8e8; color: #808082; width:230px; margin-bottom:7px;" /></td>
      </tr>
      <tr>
        <td>Phone: <span class="orange">*</span></td>
        </tr>
        <tr>
        <td><input name="phone" type="text" id="phone" value="<? echo $_REQUEST["phone"]; ?>" style="border-color: #c7c7c7; background-color: #e8e8e8; color: #808082; width:230px; margin-bottom:7px;" /></td>
      </tr>
      <tr>
        <td>Type of Leads:</td>
      </tr>
      <tr>
        <td><label>        
          <select name="lead_type" style="margin-bottom:5px;">
            <option value="Car Transport" <? if($lead_type=='Car Transport'){echo " selected";}?>>Auto Transport</option>
            <option value="Moving" <? if($lead_type=='Moving'){echo " selected";}?>>Moving</option>
            <option value="Home Security" <? if($lead_type=='Home Security'){echo " selected";}?>>Home Security</option>
            <option value="Home Senior Care" <? if($lead_type=='Home Senior Care'){echo " selected";}?>>Home Senior Care</option>
            <option value="Medical Alert" <? if($lead_type=='Medical Alert'){echo " selected";}?>>Medical Alert</option>
            <option value="Insurance" <? if($lead_type=='Insurance'){echo " selected";}?>>Insurance</option>
			<option value="Solar" <? if($lead_type=='Solar'){echo " selected";}?>>Solar</option>
            <option value="Other" <? if($lead_type=='Other'){echo " selected";}?>>Other</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td>Comments:</td>
      </tr>
      <tr>
        <td><label>
          <textarea name="comment" rows="8" style="height: 55px; width:230px; margin-bottom:5px;" ><? echo $_REQUEST["comment"]; ?></textarea>
        </label></td>
      </tr>
      <tr>
      <td><input name="image" type="image" src="irelocation/images/submit-button.png" style="margin-left:75px; margin-bottom:8px;"></td>
      </tr>
    </table>

  </form>
</div>
 
 <div class="icons">
<div class="icon-auto"><img src="irelocation/images/icons/car-transport-icon.png" /><br />
<span class="icon-title" style="margin-left:-1px;">Auto Transport</span></div>
 
<div class="icon-moving"><img src="irelocation/images/icons/moving-icon.png" /><br />
<span class="icon-title" style="margin-left:4px;">Moving</span></div>

<div class="icon-medical"><img src="irelocation/images/icons/medical-alert-icon.png" onmouseover="this.src='irelocation/images/icons/medical-alert-hover.png" /><br />
<span class="icon-title" style="margin-left:-8px;">Medical Alert</span></div>


<div class="icon-senior"><img src="irelocation/images/icons/senior-care-icon.png" /><br />
<span class="icon-title" style="margin-left:-8px;">Senior Care</span></div>

<div class="icon-security"><img src="irelocation/images/icons/security-icon.png" /><br />
<span class="icon-title" style="margin-left:-4px;">Security</span></div>
</div>
</div>
<div style="float:left; margin-top:8px; font-size:10px; margin-left:47px;">
<p>&copy; 2013 The iRelocation Network. All Rights Reserved.</p>
  </div>
  </div>
</body>
</html>
  <?php }  ?>



















