<?
	session_start();
	$_SESSION['sql'] = "";
	if ($_SESSION['username'] != "vsmith")
	{
		if ($_POST['username'] == "vsmith" && $_POST['password'] == "traffic")
			$_SESSION['username'] = "vsmith";
		else
		{
			header("Location: login.php");
			exit();
		}
	}

	include "../inc_mysql.php";
	if ($_REQUEST['term_id'] == "")
		$term_id = "";
	else
		$term_id = $_REQUEST['term_id'];

	if ($term_id != "")
	{
		$key = $_REQUEST['key'];
		$other = ($key=="from_term_id")?"to_term_id":"from_term_id";
		$sql = "select * from automoves.terminals where term_id = $term_id  ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$term = $rs->myarray;
	
		$sql = "select r.rate, r.rate_id, t.city, t.state, t.term_id from automoves.terminals as t ".
				"join automoves.rates as r on r.$other = t.term_id where ".
				" r.$key = $term_id order by t.city,t.state";
		$rs = new mysql_recordset($sql);
		$terminals = array();
		while($rs->fetch_array())
			$terminals[] = $rs->myarray;
	}
	else
	{
		$sql = "select * from automoves.terminals order by city, state ";
		$rs = new mysql_recordset($sql);
		$origins = array();
		while($rs->fetch_array())
			$origins[] = $rs->myarray;	
	}
	
	?>
	
	<script>
		
	
		function changeAmount(value)
		{			
			var i = 0;
			var end = document.forms.updates.elements.length;
			for (i = 0; i < end; i++)			
			{
				var el = document.forms.updates.elements[i];
				if (el.type == 'text')
				{
					if (el.name == 'custom_ammount') continue;
					var curr = parseInt(el.value);
					el.value = (curr + value);					
				}
			}			
		}
		
		function resetRates()
		{
			var i = 0;
			var end = document.forms.updates.elements.length;
			for (i = 0; i < end; i++)			
			{
				var el = document.forms.updates.elements[i];
				if (el.type == 'text')
				{
					if (el.name == 'custom_ammount') continue;
					var parts = el.name.split('_');
					var name = "base_"+parts[2];
					el.value = document.forms.updates[name].value;					
				}
			}			
		}
		
		function roundTo(value,precision)
		{
			return parseInt(value/precision)*precision;		
		}
		
		function changeByPercent(percent)
		{
			
			var i = 0;
			var end = document.forms.updates.elements.length;
			for (i = 0; i < end; i++)			
			{
				var el = document.forms.updates.elements[i];
				if (el.type == 'text')
				{
					if (el.name == 'custom_ammount') continue;
					var curr = parseInt(el.value);
					curr = (((percent/100.00) + 1.00) * curr);
					el.value = roundTo(curr,5);
				}
			}			
		}
	
	
	</script>
	
	
	<?
		
	
	if(is_array($origins))
	{	
		echo "<form name='from' action='from.php'>";
		echo "<input type='hidden' value='from_term_id' name='key' />";
		echo "<select name='term_id'>";
		foreach($origins as $o)
		{
			echo "<option value='".$o['term_id']."'>".$o['city'].", ".$o['state']."</option>\n";
		}
		echo "</select>";
		echo "<input type='submit' value='Select Origin' />";
		echo "</form>";
		echo "</td></tr>";
		echo "<form name='to' action='from.php'>";
		echo "<form name='to' action='from.php'>";
		echo "<input type='hidden' value='to_term_id' name='key' />";
		echo "<select name='term_id'>";
		foreach($origins as $o)
		{
			echo "<option value='".$o['term_id']."'>".$o['city'].", ".$o['state']."</option>\n";
		}
		echo "</select>";
		echo "<input type='submit' value='Select Destinations' />";
		
		echo "</form>";
		echo "<hr width='50%' align='left' />";
		
?>
	<form action='changeall.php' method='post'>
		<table>
		<tr>
			<td><strong>Adjust All Rates</strong></td>
			<td>
				X = <input type='text' name='X' value='5' size='4' maxlength='4' />
				&nbsp; &nbsp;
				Round to: 
				<select name='round'>
					<option>5</option>
					<option>10</option>
					<option>25</option>
				</select>	
				
			</td>
		</tr>
		<tr>
			<td valign='top'>
				<table>
					<tr>
						<td>
							<input type='radio' name='action' value=' - 50' >
						</td>
						<td>Drop All Rates by $50.00</td>
					</tr>
					<tr>
						<td><input type='radio' name='action' value=' - 5%' ></td>
						<td>Drop All Rates by 5%</td>
					</tr>					
					<tr>
						<td><input type='radio' name='action' value='+ 5% ' ></td>
						<td>Increase All Rates by 5%</td>
					</tr>
					<tr>
						<td><input type='radio' name='action' value=' + 50' ></td>
						<td>Increase All Rates by $50.00</td>
					</tr>
				</table>
			</td>
			<td valign='top'>
				<table>
					<tr>
						<td><input type='radio' name='action' value='- X' ></td>
						<td>Drop All Rates by $X.00</td>
					</tr>
					<tr>
						<td><input type='radio' name='action' value=' - X% ' ></td>
						<td>Drop All Rates by X%</td>
					</tr>					
					<tr>	
						<td><input type='radio' name='action' value=' + X% ' ></td>
						<td>Increase All Rates by X%</td>
					</tr>
					<tr>
						<td><input type='radio' name='action' value=' + X ' ></td>
						<td>Increase All Rates by $X.00</td>
					</tr>
				</table>
			</td>
		</tr>
		<? /*
		<tr>
			<td colspan="2">
				<input type='radio' name='action' checked="checked" value=' Back Up ' />
				Reset All Rates to their previous values *
			</td>
		</tr>
		*/ ?>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td align="left"> <input type='submit' value='Change All Rates' /></td>
			<td><? /* * <Strong>Note:</strong> This command can only go back one step. */?>&nbsp;</td>
		</tr>
		<tr><td colspan="2">&nbsp;</td></tr>
	</table>
	
	</form>
	
	<hr width='60%' align="left" />
	
	<form action="create_terminal.php" method="post">
		<table>
			<tr>
				<td>City:</td>
				<td><input type="text" name="city" /></td>
			</tr>
			<tr>
				<td>State:</td>
				<td><input type="text" name="state" maxlength="2" size="3" /></td>
			</tr>
			<tr>
				<td>Zip:</td>
				<td><input type="text" name="zip" maxlength="5" size="6"  /></td>
			</tr>	
			<tr>
				<td>Free Radius:</td>
				<td>
					<select name='free_radius'>
						<option>15</option>
						<option selected>30</option>
						<option>50</option>
						<option>75</option>
					</select> miles
				</td>
			</tr>	
			<tr>
				<td>$/Mile Extra: </td>
				<td>
					<select name='free_radius'>
						<option>1.00</option>	
						<option>1.50</option>
						<option selected>2.00</option>
						<option>2.50</option>
						<option>3.00</option>
					</select>
				</td>
			</tr>	
			<tr>
				<td>Mileage Cap:</td>
				<td>
					<select name='mile_cap'>
						<option>75</option>
						<option>100</option>
						<option selected>150</option>
						<option>200</option>
					</select> miles
				</td>
			</tr>	
			<tr>
				<td colspan="2" align="left">
					<input type='submit' value="Create Terminal" />
				</td>
			</tr>
		</table>
	
	</form>
<?
	}
	else
	{
		echo "<strong>Loads ";
		
		$verb = $_REQUEST['key'];
		if ($verb == "from_term_id")
		{
			echo "coming from ";
			$other = "from.php?key=to_term_id&term_id=$term_id";
		}
		else
		{
			echo "going to ";
			$other = "from.php?key=from_term_id&term_id=$term_id";
		}
?>	
		<?= $term['city'].", ".$term['state'] ?></strong>
		 <a href='$other'>Reverse Routes</a> 
		 <a href='from.php'>Choose Another Terminal</a> 
		</td></tr>
		<form name='updates' action='update_rates.php' method='post'>
		<input type='hidden' value='<?= $_REQUEST['key'] ?>' name='key' />
		<input type='hidden' name='term_id' value='<?= $term_id ?>' />
		<table>
		<tr><td colspan='9'> &nbsp;</td></tr>
		<tr><Td colspan='4'>
		Custom Changes: &nbsp; X = <input type='text' name='custom_ammount' size='4' maxlength='4' value='5'> 
		
		<input type='button' value=' -X% ' 
					onclick="changeByPercent(-1*(parseInt(custom_ammount.value)))"> 
		<input type='button' value=' -X$ ' 
					onclick="changeAmount(-1*parseInt(custom_ammount.value))"> 
		<input type='button' value=' +X$ ' 
					onclick="changeAmount(parseInt(custom_ammount.value))"> 
		<input type='button' value=' +X% ' 
					onclick="changeByPercent((parseInt(custom_ammount.value)))"> 
					
		</td>
		<td><input type="checkbox" value="copy" name="reverse" /> Copy in Reverse </td>
		<td colspan='4' align='right'>Controls: &nbsp; &nbsp; 
		
			<input type='button' value=' -50 ' 
					onclick="changeAmount(-50)"> 
			<input type='button' value=' -5% ' 
					onclick="changeByPercent(-5)"> 			
			<input type='button' value=' Reset ' 
					onclick="resetRates()"> 
			<input type='button' value=' +5% ' 
					onclick="changeByPercent(5)"> 
			<input type='button' value=' +50 ' 
					onclick="changeAmount(50)"> 
			</td>				
		</tr>
		<tr><td colspan='9'> <hr/></td></tr>
		<?
		$i = 0;
		$count = count($terminals);
		$per_row = floor($count/3);
		if ($count - ($per_row * 3) < 10)
			$per_row += 4;
		?>
		<tr><td colspan='9'>
		<table><tr>
		<?
		foreach($terminals as $t)
		{
			if ($i == 0 || ($i % $per_row) == 0)
			{
				if ($i > 0)
				{
					echo "</table></td>";
					echo "<td width='5'>&nbsp;</td>";
				}
				echo "<td valign='top'><table>";			
			}
		
			echo "<tr><td>".($i+1).") </td><td>".$t['city'].", ".$t['state']."</td>";
			echo "<td> ".$t['rate']." </td>";
			echo "<td><input type='text' name='rate_id_".$t['rate_id']."' size='4' ".
					"maxlength='5' value='".$t['rate']."'>";
			echo "<input type='hidden' name='base_".$t['rate_id']."' value='".$t['rate']."'>";
			echo "</td>";
			echo "</tr>\n";		
			$i++;
		}
		?>	
			
		</td></tr></table></td></tr>
		<tr>
			<td><input type='submit' value='Update'/></td>
			<td colspan='7'>&nbsp;</td>
			<td><input type='submit' value='Update'/></td>
		</tr>
		</table>
<?	}	?>