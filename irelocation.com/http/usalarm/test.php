<?php 

//-- test script

include_once '../inc_mysql.php';//Database!	


//-- NEW FUNCTION - need to remove lead_ids from the quotes BEFORE they are processed, so that the company counts reported are accurrate.
//-- Brinks - NO RES RENTERS
//-- APEX - NO RES RENTERS and NO CLICKBOOTH and ONLY CERTAIN ZIPS

/* flow:
1- select all current
2- loop thru each, explode the lead_ids, and check each lead_id against the quote info
3- remove the lead_ids that don't want the quote
4- implode the remaining leads
5- update the record

select quote_id, received, quote_type, own_rent, lead_ids, campaign FROM leads_security where ...

*/
echo "Begin<br />";

$sql = "select quote_id, received, source, quote_type, own_rent, lead_ids, campaign FROM leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 3 and left(received,8) = '20080911' ";

// and own_rent = 'rent' and quote_type = 'res' 

echo "$sql<br />";

$rs = new mysql_recordset($sql);

while( $rs->fetch_array() ) {
	 extract($rs->myarray);
	 echo "$quote_id<br />";
	 echo "$source<br />";
#	 echo "$received<br />";
	 echo "$quote_type<br />";
	 echo "$own_rent<br />";
	 echo "ORIG: $lead_ids<br />";
#	 echo "$campaign<br />";
	 
	 //-- validate Brink 1519
	if ( ereg( "1519", $lead_ids ) ) {
		#echo "Found 1519<br />";
		
		if ( ($quote_type == "res" && $own_rent == "rent" ) ) { //-- don't want residential renters
			$lead_ids = ereg_replace("1519","",$lead_ids);
		} 

	} 
	
	 //-- validate APEX 1569
	if ( ereg( "1569", $lead_ids ) ) {
		#echo "Found 1569<br />";

		if ( ($quote_type == "res" && $own_rent == "rent" ) || eregi("clkbth",$source) ) { //-- don't want residential renters or clickbooth
			$lead_ids = ereg_replace("1569","",$lead_ids);
		} 

	} 
	
	//-- remove double pipes ( || )
	$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
	
	//-- remove pipes as the first location
	if ( substr($lead_ids,0,1) == "|"  ) {
		echo "FIRST PIPE!<br />";
		$lead_ids = substr($lead_ids,1);
	} 
	
	echo "NEXT: $lead_ids<br />";
	
	//-- remove pipes as the last location
	if ( substr($lead_ids,-1,1) == "|"  ) {
		echo "LAST PIPE!<br />";
		$lead_ids = substr($lead_ids,0,-1);
	} 
	
	echo "END: $lead_ids<br />";

	
	echo "<hr />";
}


/*
$rs->fetch_array();
$YYY = $rs->myarray["ZZZ"];
*/


echo "End<br />";


?>