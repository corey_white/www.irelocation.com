<?	
/* 
************************FILE INFORMATION**********************
* File Name:  SecurityClasses.php
**********************************************************************
* Description:  This is the USALARM SECURITYCLASS
**********************************************************************
* Creation Date:  unknown
**********************************************************************

**********************************************************************

USALARM    USALARM    USALARM    USALARM    USALARM    USALARM   USALARM
USALARM    USALARM    USALARM    USALARM    USALARM    USALARM   USALARM
USALARM    USALARM    USALARM    USALARM    USALARM    USALARM   USALARM
*/

	define(DEGUB_TSC_EMAIL,"rob@irelocation.com,mark@irelocation.com");
	include_once "pinnacle.php";
	include_once "SecurityClass.php";
	
	define(GAY_ABSOLUTE,"absolute");
	define(GAY_EVERSAFE,"eversafe");
	define(GAY_ROOT,"gaylord");
	

	//base class for Gaylord Security
	class AlarmTrax extends SecurityClass
	{
	
		function setGaylordAffiliate($company)
		{
			$source = $this->thearray["source"];
			$company_count = $this->thearray["company_count"];
			$this->company = $company;
			
			if ($company == GAY_ABSOLUTE)
			{
				$this->source_id = 130;				
			}
			else if ($company == GAY_EVERSAFE)
			{
				$this->source_id = 78;	 //-- used to be 34
			}
			else if ($company == GAY_ROOT)			
			{
				$this->source_id = 84;//USalarm Companies Code				
			}
			else
			{
				echo "INVALID GAYLORD AFFILIATE!!!";
				mail("rob@irelocation.com");
				exit();
			}

			//-- set dynamic source for lead tracking - this is where we set the campaign_id for CPC leads
			switch($company_count) {
				case 1:
					$this->campaign_id = "4";
					break;
				case 2:
					$this->campaign_id = "5";
					break;
				case 3:	
					$this->campaign_id = "6"; 
					break;			
				case 4:	
					$this->campaign_id = "7"; 
					break;			
			}

			//-- remanipulate the company and source_id if it's a clickbooth lead override so the parent co gets the lead.
			if ( eregi("clkbth",$source) ) {
				
				//-- set dynamic source for lead tracking.
				switch($company_count) {
					case 1:
						$this->campaign_id = "8";
						break;
					case 2:
						$this->campaign_id = "9";
						break;
					case 3:	
						$this->campaign_id = "10"; 
						break;			
					case 4:	
						$this->campaign_id = "11"; 
						break;			
				}

				$company = GAY_ROOT;
				$this->source_id = 99;
				# $this->campaign_id = 2; //-- now dynamically set with the switch above
				echo "<br />THIS IS A CLICKBOOTH LEAD<br />";
			} 
			
			
			//-- OVERIDE the campaign_id, as neither Eversafe nor Absolute want to get these ids.
			if ($company == GAY_ABSOLUTE) {
				$this->campaign_id = "";
			} else if ($company == GAY_EVERSAFE) {
				$this->campaign_id = "";
			}

			
			$this->url = "https://secure.securitytrax.com/".$company."/leadpostxml.php";
		}
	
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			//75 - for Absolute
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 84; 
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}

			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 28;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = array();
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: ".$data['building_type'];

			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
			
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<"."?xml version='1.0' standalone='yes'?".">
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<campaign_id>".$this->campaign_id."</campaign_id>
					<lead_type_id>".$this->lead_type_id."</lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".xmlentities($data['name1'])."</fname>
					<lname>".xmlentities($data['name2'])."</lname>
					<address1>".xmlentities($data['address'])."</address1>
					<address2></address2>
					<city>".xmlentities($data['city'])."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".xmlentities($data['comments'])."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".xmlentities($extra_text." Note: Lead sent to $company_count companies.")." </lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			//mail("davidhaveman@gmail.com","Gaylord USAlarm XML",$xml);
			return $xml;
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			//mail("david@irelocation.com","AlarmTrax XML",$xml);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>
			$rawresponse = $response;
			
			echo "<br>\nResponse:$response.<br>\n<br>\n";
			
			$response = substr($response,strpos($response,"<body>")+6);
			$insert_response = trim(str_replace("version=\"1.0\"?>","",strip_tags($response)));

			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
			if ($success == 0)
			{
				//new format?
				if (substr_count($response,"success=\"true\"") > 0)
				{
					$success = 1;
					$start = strpos($response,"<customer_id>")+13;
					$end = strpos($response,"</",$start);
					if ($end > $start && $start != -1)
						$responseId = substr($response,$start,$end);				
					else
					{
						$success = 0;
						$responseId = "Success?";
						//mail("mark@irelocation.com","AlarmTrax Failure? - ".$this->lead_id,
						//"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
						//"\nLeadXML:".$this->xml);
						recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
					}
					
					save_response($quote_id,$insert_response,$this->lead_id,$success,"usalarm");	
					echo "Saving Success Response for $quote_id in Parent part a<br />";
				}
				else
				{
					$success = 0;
					$responseId = "failure";
					//mail("david@irelocation.com","AlarmTrax Failure - ".$this->lead_id,
					//"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
					//"\nLeadXML:".$this->xml);
					recordLeadFailure($this->thearray,
						$this->lead_id,
						"security",
						"irelocation.leads_security");
					save_response($quote_id,$insert_response,$this->lead_id,$success,"usalarm");	
					echo "Saving Fail Response for $quote_id in Parent<br />";
				}
			}
			else
			{	
				save_response($quote_id,$result,$this->lead_id,$success,"usalarm");			
				echo "Saving Success Response for $quote_id in Parent part b<br />";
			}
		}			
	}




	class Gaylord extends AlarmTrax
	{
		function Gaylord()
		{
			parent::AlarmTrax(GAYLORD);
			$this->lead_type_id = "3";
			$this->company = "gaylord";
		}	
		
		function test()
		{
			return ($this->thearray['source'] != "aff_p4p");
		}
	
		function send()
		{					
			$eversafe_states = "NJ  PA  DE  MD  OH";
			$absolute_states = "AL  AR  CT  DC  FL  GA  KY  LA  ME  ".
							   "MA  MS  NH  NY  RI  TN  VT  VA  WV";								
								
			//if() run code for mark alliata
			$state = $this->thearray["state_code"];	
			$quote_id = $this->thearray["quote_id"];
			$source = $this->thearray["source"];
			$aff = false;
			
			//-- if this is a clickbooth lead, override who it goes to
			if ( eregi("clkbth",$source) ) {

				$this->setGaylordAffiliate(GAY_ROOT);		
				parent::format();

			} else {
			
				if (substr_count($eversafe_states,$state) > 0) {
				
					$rec = $this->thearray["received"];				
					$this->setGaylordAffiliate(GAY_EVERSAFE);
					parent::format();
					$aff = true;
	
				} else if (substr_count($absolute_states,$state) > 0) {
				
					$rec = $this->thearray["received"];				
					$this->setGaylordAffiliate(GAY_ABSOLUTE);	
					parent::format();
					$aff = true;
	
				} else {
				
					$this->setGaylordAffiliate(GAY_ROOT);		
					parent::format();
	
				}
			
			}

			
			
			if ($aff)
			{
				//save it for our records.
				$sql = "insert into irelocation.gaylord_security_leads ".
						"(quote_id,received,company) ".
						" values ('".$quote_id."','".date("Ymdhis")."','".$this->company."') ";
				$rs = new mysql_recordset($sql);					
				$rs->close();						
			}
			
			parent::send();				
		}		
	}
	



	
	class ProtectAmerica extends SecurityClass
	{				
		function ProtectAmerica()
		{
			$this->url = "http://production.leadconduit.com/ap/PostLeadAction";
			$this->lead_id = PROTECT_AMERICA;
		}
	
		function test()
		{
			
			
			if ( strtolower($this->thearray['own_rent']) == "rent" && strtolower($this->thearray['quote_type']) == "res" ) { //-- only residential renters
				echo "ProtectAmerica 1520  WANTS RENTers<br />";
				$this->source_tag = "A01893";
				return true;
				
			} 
			
			//-- now check to see if the zip is in APX's (156( list, and if so, then Protect America does not want it.
			$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '" . $this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();

			if ( !$valid ) {
				echo "ProtectAmerica 1520 DOES WANT " . $this->thearray['zip'] . " (NOT FOUND IN APX ZIP LIST)<br />";
				
				echo "Source is " . $this->thearray['source'] . "<br />";
				
				if ( eregi("clkbth",$this->thearray['source'] ) ) {
					$this->source_tag = "A01962";
				} else {
					$this->source_tag = "A01963";
				}
				echo "Source_Tag now set to " . $this->source_tag . "<br />";
				
				return true;
			} else {
				echo "ProtectAmerica 1520 DOES NOT WANT " . $this->thearray['zip'] . " (FOUND IN APX ZIP TABLE)<br />";
				return false;
			}

		}
	
		function format()
		{
			extract($this->thearray);
			//set dynamic source for lead tracking.

/* NOT NEEDED
			switch($company_count)
			{
				case 1:
					$source_tag = "A01590";
					break;
				case 2:
					$source_tag = "A01591";
					break;
				case 3:	
					$source_tag = "A01592"; 
					break;			
			}
*/
			
			
/* Removed per Jeff butler on 8/1/08 14:18 PM - 
			if ($quote_type == "res")
				$quote_type = "residential";
			else
				$quote_type = "commercial";
*/
				
			//-- append the quote_type if it's a click booth 
			if ( eregi("clkbth",$source) ) {
				$quote_type .= " email ";
			} 
				
			if ($own_rent == "own")
				$own_rent = "YES";
			else
				$own_rent = "NO";
				
			 $this->lead_body = "xxNodeId=2b43d7b&".
				"city_name=".urlencode($city)."&".
				"email_address=".urlencode($email)."&".
				"first_name=".urlencode($name1)."&".
				"last_name=".urlencode($name2)."&".
				"phone1=$phone1&".
				"phone2=$phone2&".
				"source=" . $this->source_tag . "&".
				"state_name=$state_code&".
				"street_name=".urlencode($address)."&".
				"userdef07=$own_rent&".
				"userdef74=GOOD&".
				"version=9&".
				"zipcode=$zip&";
				$this->lead_body .= "key3=$quote_type";
			
		}
	
		function send()
		{
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			
			if (substr_count(strtolower($response),"the lead was submitted") == 0)
			{
				//mail("david@irelocation.com","PA Response","QuoteID: ".
				//$this->myarray["quote_id"]."\n".$response);
				save_response($this->thearray["quote_id"], "Failure - $response", 
								PROTECT_AMERICA,0,"usalarm");
				echo "ProtectAmerica Lead - Failure";
				recordLeadFailure($this->thearray,
							1520,
							"security",
							"irelocation.leads_security");
			}
			else 
			{
				
				save_response($this->thearray["quote_id"], "Lead Accepted", PROTECT_AMERICA,1,"usalarm");
				echo "ProtectAmerica Lead - Success";
			}
		}
	}



	
	class Brinks extends SecurityClass
	{		
	
		function buildingType($building)
		{
			switch($building)
			{
				case "town house": return "TH";			
				case "apartment": return "A";
				case "condo": return "C";
				case "mobile home": return "M";
				case "office": return "O";
				default: return "";
			}
		}
	

		function sqrFootage($type,$sq)
		{
	
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "C" && $sq == 0) return 2500;
				else if ($type == "C" && $sq == 50) return 20000;
				else if ($type == "R" && $sq == 0) return 1500;
				else if ($type == "R" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
			
		}
	
		function test()
		{			
/* old code, commented out on 5-19-2008 - Rob
			//return ((strtolower($this->thearray["own_rent"]) == "own") || ($this->thearray['quote_type'] == 'com'));
			//contract doesn't specify exclude resi-renters...
			return true;
*/
			//-- Added this to exclude rentals from Brinks - Mark said that this was the place.
			if ( strtolower($this->thearray["own_rent"]) == "rent" ) { //-- Brinks doesn't want any rentals
				$foo_temp = $this->thearray["own_rent"];
				//mail("rob@irelocation.com","Brinks CHECK","Checking lead for Brinks and found it to be a RENT \nvalue tested was $foo_temp");
				echo "Checking lead for Brinks and found it to be a RENT<br />";
				echo "value tested was $foo_temp<br />";
				return false;
			} else {
				$foo_temp = $this->thearray["own_rent"];
				//mail("rob@irelocation.com","Brinks CHECK","Checking lead for Brinks and found it to be a OWN \nvalue tested was $foo_temp");
				echo "Checking lead for Brinks and found it to be a OWN<br />";
				echo "value tested was $foo_temp<br />";
				return true;
			}

		}
				
		function Brinks()
		{
			$this->url = "ftp.brinks.com";
			$this->lead_id = BRINKS;
		}	
			
		
		
		function format()		
		{
			$thearray = $this->thearray;
			
			$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
			$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
			
			extract($thearray);
			
			$sitetype = (($quote_type=="com")?"C":"R");
			if ($sitetype == "C")
			{
				list($fname,$lname) = split(" ",$name2);
				$businessname = $name1;	
			}
			else
			{
				$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
				$lname = substr($name2,0,20);//same with contact name, or long lastname.
			}
			$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
			$city = substr($city,0,30);
			$email = substr($email,0,40);
			$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
			$street_number = substr($parts[0],0,20);
			$street_number = substr($street_number,0,19);		
			$street_name = substr($parts[1],0,20);
			if (strlen($phone2) < 10)
				$phone2 = "";
			if (strlen($phone1) < 10)
				$phone1 = "";
				
			if ($own_rent == "Own" || ($quote_type=="com"))
				$renter = 0;
			else
				$renter = 1;
						
			$newcomments = "";
			
			if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
			{
				$newcomments.= " ExtraServices:";
				if ($fire == 1)
					$newcomments  .= "Fire Protection ";
				if ($access == 1)
					$newcomments  .= "Access Control System ";				
				if ($cctv == 1)
					$newcomments  .= "CCTV System ";
				if (strlen($other_field) > 0)
					$newcomments  .= xmlentities($other_field);
				
				$newcomments .= " ";
			}
			$building = $this->buildingType($building_type);		
				
				
			if ($quote_type == "com" && $num_location > 1)
				$newcomments .=	"Num of Locations: $num_location ";
			$brinks_customer_comments = trim($comments);	
			
			if (strlen(trim($comments)) > 0)
			{			
				$length = 240 - strlen($newcomments);
				if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
					$newcomments .= "Cust Comments:".$brinks_customer_comments;
				else//trim their comments down
					$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
			}
			$e21 = "";
			$e22 = "";
			$e23 = "";
			$e24 = "";
			$e25 = "";
			$e26 = "";
			$e27 = "";
			$e28 = "";
			$e29 = "";
			
			/*
			  Please see new program ids for use in new
				campaign for usalarmcompanies.com. 
				Program Id Description Brinks Code 
				N755 Brinks Only USALRMCO1FTP 
				N756 Brinks plus One Company USALRMCO2FTP 
				N757 Brinks plus Two Companies USALRMCO3FTP
			 
			 */
			/*//-- OLD switch that has been superceded by the below
			switch($company_count)
			{
				case 1:
					$marketsource = "N755";
					break;
				case 2:
					$marketsource = "N756";
					break;				
				case 3:
				default:
					$marketsource = "N757";
					break;							
			}
			*/

			//-- this is the new switch code that will either assign the same codes as above, or if it's a clickbooth lead, will assign new codes.
			if ( eregi("clkbth",$source) ) { //-- if it's a click booth lead, we get one type of source_tag, otherwise we use the original:

				switch($company_count) {
					case 1:
						$marketsource = "N769";
						break;
					case 2:
						$marketsource = "N770";
						break;				
					case 3:
					default:
						$marketsource = "N771";
						break;							
				}		

			} else { //-- if NOT clickbooth, then assign the original codes
			
				switch($company_count) {
					case 1:
						$marketsource = "N755";
						break;
					case 2:
						$marketsource = "N756";
						break;				
					case 3:
					default:
						$marketsource = "N757";
						break;							
				}		
			}

			
			$this->lead_body ="$fname,$lname,$street_number,$street_name,,$city,$state_code,$zip,$phone1,".
			"$phone2,$email,$newcomments,$renter,$marketsource,".$thearray["quote_id"].
			",,$sitetype,$businessname,$building,$sqrfootage,$e21,$e22,$e23,$e24,$e25,$e26,$e27,$e28,$e29";			
			
			echo "<Br/>Brinks LEAD: #".$this->thearray['quote_id'].": ".$this->lead_body."<Br/><Br/>";
			//mail("david@irelocation.com","Brinks #".$thearray["quote_id"],$this->lead_body);
//			return trim($lead_body);
		}
			
		/* END NEW FORMAT */	
			
		function send()
		{
			$result = true;
			$quote_id = $this->thearray["quote_id"];
			//Global Variables are turned off, so i have to put it to a scope where
			//Var.php can reach it. SESSION seems most reasonable..
			$_SESSION["text"] = $this->lead_body;	
							
			$login = "irelocation";
			$password = "Jun3bug!";
			$subdir = "In/";
			$ext = ".txt";			

			$fp = fopen('var://SESSION/text','r');
			$conn_id = ftp_connect($this->url);
				
			$login_result = ftp_login($conn_id, $login, $password);
			
			// check connection
			if ((!$conn_id) || (!$login_result)) 
			{				
				mail("code@irelocation.com","Brinks FTP Failure - USALARM - Cannot Connect","Attempted to connect to ".$this->url." for user $login \n ".date("YmdHis")." ".$quote_id ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
				$result = false;
			}
			else
			{
				// upload the file
				$upload = ftp_fput($conn_id, $subdir.$quote_id.$ext,$fp, FTP_ASCII);
				
				$update_response = print_r($upload,true);
				
				// check upload status
				if (!$upload) 
				{
					mail("code@irelocation.com","Brinks FTP Failure - USALARM - Failed to Upload","$update_response \n\n Attempted to connect to ".$this->url." for user $login\n".date("YmdHis")." ".$quote_id ,"From: The iRelocation Network <no_reply@irelocation.com>\nContent-Type: text/html; charset=iso-8859-15");	    
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = false;
				} 
				else
				{
					mail("leads@brinks.com","iRelocation - $quote_id",$this->lead_body);
					#mail("code@irelocation.com","Brinks Lead Sent - USALARM - $quote_id",$this->lead_body . "\n\n $update_response");
					
				
					// close the FTP stream
					ftp_close($conn_id);
					fclose($fp);
					$result = true;

				}	//-- this bracket was ABOVE the close stream and setting result = true.  This MAY have been the reason why this wasn't reporting correctly.
			}
			
			$this->processResponse($result);			
		}
		
		function processResponse($response)
		{			
			if ($response)
			{
				save_response($this->thearray["quote_id"],"FTP Accepted",BRINKS,1,"usalarm");				
			}
			else
			{
				save_response($this->thearray["quote_id"],"FTP Rejected",BRINKS,0,"usalarm");
				recordLeadFailure($this->thearray,
							BRINKS,
							"security",
							"irelocation.leads_security");
			}
		}		
		
		#------------------------------------------------------------------------------------------------------------
		#
		#			BRINKS FORMAT
		#
		#------------------------------------------------------------------------------------------------------------
			
			
		/*
			Taken from "Incoming File Format Specification for iRelocation.doc" from Ray Brown @ Brinks.
			
			Data files should be in plain text, comma-delimited format with one record per line.
			The delimiter character can be changed on a case-by-case basis. Given that commas 
			are used to delimit fields, and carriage returns are used to delimit records, care 
			must be taken to ensure that all extraneous commas, quotes, double quotes and carriage 
			returns are removed from data before being included in the file.
			
			Additional fields may be added to the specification upon agreement between Brinks and 
			the partner in question. The data file may not be jagged-that is, if a field is null, 
			it must still be included in the output file as an empty string. Each line must contain
			n-1 delimiters, where n is the number of fields agreed upon between Brinks and the partner.
	
			The first line of the file should NOT contain field names. Field names are already
			known as long as the aforementioned format is followed.
	
			The file should contain no blank lines, and no lines with incomplete records.
	
			
			1.	First name (20 chars max)
			2.	Last name (20 chars max)
			3.	Street Number (20 chars max)
			4.	Street Name  (30 chars max, optional)
			5.	Additional Address (30 chars max, optional)
			6.	City (30 chars max)
			7.	State (2 char abbreviation)
			8.	ZIP/Postal Code (10 chars max, formatting unimportant, but MUST contain a valid ZIP code)
			9.	Primary Phone (formatting unimportant, but MUST contain a valid 10-digit phone number)
			10.	Secondary Phone (optional)
			11.	E-mail address (40 chars max, optional)
			12.	Comment (255 chars max-may not contain carriage returns, optional)
			13.	Renter (1 char, 1 = true 0 = false)
	
			Sample File: brinks/sample.txt
			
		*/				
	}





	//no one in the matrix campaign is using this one..
	class TextEmail extends SecurityClass
	{
		function TextEmail($email)
		{
			$this->emailaddress = $email;//;
		}
		
		function sqrFootage($type,$sq)
		{
			echo "sqrFootage($type,$sq)<br/>";
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return 2500;
				else if ($type == "com" && $sq == 50) return 20000;
				else if ($type == "res" && $sq == 0) return 1500;
				else if ($type == "res" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		
		function send()
		{
			$this->format();
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			/*
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id,
					$this->message,$this->headers);
			*/
		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function formatHTML()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			echo "Extras: F: $fire A: $access C: $cctv P: $prewired B: $basement <br/>";
			
			echo "Fire:-$fire- <br/>";
			
			$extra_services = array();
			if ($fire == "1")
				$extra_services[] = "fire";
			if ($access  == "1")
				$extra_services[] = "access";
			if ($cctv  == "1")
				$extra_services[] = "cctv";
			if ($prewired  == "1")
				$extra_services[] = "house is prewired";
			if ($basement  == "1")
				$extra_services[] = "basement is finished";		
			
			if (count($extra_services) > 0)
				$extras = implode(",",$extra_services);
			echo "Extras: $extras <br/>";
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->HTMLheaders = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/html; charset=iso-8859-15";
						
			$this->HTMLmessage = 
			"<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
			"<tr><td colspan='2'><strong>Top Security Lead</strong></td></tr>".
			"<tr><td colspan='2'><hr width='250' align='left'/></td></tr>".
			"<tr><td width='200'>Quote ID: </td><td>".$quote_id."</td></tr>".
			"<tr><td width='200'>Received: </td><td>".$received."</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name."</td></tr>".
			"<tr><td width='200'>E-Mail Address: </td><td>".$email."</td></tr>".
			"<tr><td width='200'>Phone Number: </td><td>".$phone1."</td></tr>".
			"<tr><td width='200'>Alternate Phone: </td><td>".$phone2."</td></tr>".
			"<tr><td width='200'>Street Address: </td><td>".$address."</td></tr>".
			"<tr><td width='200'>City: </td><td>".$city.", ".$state_code."</td></tr>".
			"<tr><td width='200'>Postal Code: </td><td>".$zip."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Quote Type: </td><td>".$type."</td></tr>".
			"<tr><td width='200'>Building Type </td><td>".$building_type."</td></tr>".
			"<tr><td width='200'>Square Footage: </td><td>".$sqr_footage."</td></tr>".
			"<tr><td width='200'>Owners: </td><td>".$own_rent."</td></tr>".
			"<tr><td width='200'>Requested Services/Features: </td><td>".$extras."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td colspan='2' align='left'>Comments:</td></tr>".
			"<tr><td colspan='2' align='left'>".$comments."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Lead Source: </td><td>iRelocation</td></tr>".
			"</table>";
			
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
	}
	
	
	
	
class APXSecurity extends SecurityClass
{				
	function test()
	{
	
		if ( $this->thearray['own_rent'] == "rent"  ) { //-- no renters
			echo "APX 1569 DOES NOT WANT (RENT)<br />";
			return false;
		} else {
			$sql = "select zip From security_zip_codes where lead_id = '" . $this->lead_id . "' and zip = '" . $this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();

			if ( $valid ) {
				echo "APX 1569 DOES WANT (FOUND ZIP)<br />";
				return true;
			} else {
				echo "APX 1569 DOES NOT WANT (NOT IN THEIR ZIP TABLE)<br />";
				return false;
			}

		}

	}

	function APXSecurity()
	{
		$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
		$this->lead_id = APX;
	}

	function format()
	{

		extract($this->thearray);
		$area = substr($phone1,0,3);
		$pre =  substr($phone1,3,3);
		$suf =  substr($phone1,6);
		$phone_w_dashes = "$area-$pre-$suf";
		//they only get resi owners.
		$quote_type = "residential";				
		$own_rent = "YES";

		//set dynamic source for lead tracking.
/*
		switch($company_count)
		{
			case 1:
				$source_tag = "1982";
				break;
			case 2:
				$source_tag = "1981";
				break;
			case 3:	
				$source_tag = "1980";
				break;			
		}
*/
		
			//-- this is the new switch code that will either assign the same codes as above, or if it's a clickbooth lead, will assign new codes.
			if ( eregi("clkbth",$source) ) { //-- if it's a click booth lead, we get one type of source_tag, otherwise we use the original:

				switch($company_count) {
					case 1:
						$source_tag = "1985";
						break;
					case 2:
						$source_tag = "1984";
						break;				
					case 3:
					default:
						$source_tag = "1983";
						break;							
				}		

			} else { //-- if NOT clickbooth, then assign the original codes
			
				switch($company_count) {
					case 1:
						$source_tag = "1982";
						break;
					case 2:
						$source_tag = "1981";
						break;				
					case 3:
					default:
						$source_tag = "1980";
						break;							
				}		
			}

			
		 $this->lead_body = "xxNodeId=000ktsuc3&".
			"City=".urlencode($city)."&".
			"Email=".urlencode($email)."&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"Phone=$phone1&".
			"phone1=$phone_w_dashes&".
			"credit=GOOD&".
			"phone2=$phone2&".
			"AFID=".urlencode($source_tag)."&".
			"State=$state_code&".
			"PostalCode=$zip&".
			"Address=".urlencode($address)."&".
			"ip=$remote_ip&".
			"IPAddress=66.96.131.11&".
			"ownhome=YES";
			
		/*
		if ($this->thearray['source'] == "aff_cst" || $this->thearray['source'] == "aff_p4p")
			$this->lead_body .= "key3=callcenter-$quote_type";
		else
			"key3=$quote_type";
		*/
		
/*
		mail("rob@irelocation.com","APX Lead Formatted","QuoteID: ".
			$this->thearray["quote_id"]."\n". $this->lead_body);
*/
		
	}

	function send()
	{
		$ch=curl_init($this->url."?".$this->lead_body);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		$response = curl_exec($ch);
		curl_close($ch);
		
		$this->processResponse($response);
	}
	
	function processResponse($response)
	{
		$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
		echo $content."<br/>";
		
		if (substr_count(strtolower($response),"success") == 1) { //-- reports a success
			save_response($this->thearray["quote_id"], $response, APX,1,"usalarm");
			
		} elseif (substr_count(strtolower($response),"failure") == 1) { //-- reports a failure
			save_response($this->thearray["quote_id"], $response, APX,0,"usalarm");
		
		} else { //-- can't determine what the reponse it, so send out an email
			mail("code@irelocation.com","UNDETERMINED APX RESPONSE",$response);
		}
		
/* OLD HOLDOVER FROM THE TSC CODE
		if (strpos($response,"<resp") > 0)
		{
			$response = trim(strip_tags(substr(strtolower($response),strpos($response,"<resp"))));
			
			echo "APX RESPONSE: $response<br />";
			mail("code@irelocation.com","APX RESPONSE",$response);
			
			if (strpos($response,"\n") > 0)
			{
				$response = split("\n",$response);
				$response[1] = trim($response[1]);
				switch($response[0])
				{
					 case "success":
					 case "queued":
						// save_response($quote_id, $result, $lead_id,$sucess,$site)
						save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
										APX,1,"tsc");
						echo "APX Lead - Success";
						break;
					case "failure":
					case "error":
						mail("code@irelocation.com","APX Response - ".$response[0],
							 "QuoteID: ".$this->thearray["quote_id"]."\n".$response);
						 save_response($this->thearray["quote_id"],$response[0]."-".$response[1], 
									APX,0,"tsc");
						break;
					default:
						mail("code@irelocation.com","APX Unknown - ".$response[0],
							 "QuoteID: ".$this->thearray["quote_id"]."\n".$response);
						break;
				}		
				//its all good.	
				return;
			}
		}
*/
		
		//are you still processing?? 
		//something went wrong if it gets here.			
		
				
	}
}






//-. ==== NEW BRINKS CLASS
//-- 11/17/08 14:15 PM Brinks now wants the street number and name to be a single var instead of splitting up 

class NewBrinks extends SecurityClass
{				
	function buildingType($building)
	{
		switch($building)
		{
			case "town house": return "TH";			
			case "apartment": return "A";
			case "condo": return "C";
			case "mobile home": return "M";
			case "office": return "O";
			default: return "";
		}
	}

	function sqrFootage($type,$sq)
	{

		if ($sq == 0 || $sq == 50)
		{
			if ($type == "C" && $sq == 0) return 2500;
			else if ($type == "C" && $sq == 50) return 20000;
			else if ($type == "R" && $sq == 0) return 1500;
			else if ($type == "R" && $sq == 50) return 10000;
		}
		else
			switch($sq)
			{
				case 15: return 3000;
				case 25: 
				case 30: return 5000;
				case 100: return 50000;
				case 200: return 20000;
				case 500: return "50000+";
			}
		
	}


	function test()
	{			
		if ( strtolower($this->thearray["own_rent"]) == "rent" ) { //-- Brinks doesn't want any rentals
			$foo_temp = $this->thearray["own_rent"];
			//mail("rob@irelocation.com","Brinks CHECK","Checking lead for Brinks and found it to be a RENT \nvalue tested was $foo_temp");
			echo "Checking lead for Brinks and found it to be a RENT<br />";
			echo "value tested was $foo_temp<br />";
			return false;
		} else {
			$foo_temp = $this->thearray["own_rent"];
			//mail("rob@irelocation.com","Brinks CHECK","Checking lead for Brinks and found it to be a OWN \nvalue tested was $foo_temp");
			echo "Checking lead for Brinks and found it to be a OWN<br />";
			echo "value tested was $foo_temp<br />";
			return true;
		}

	}
			
	function NewBrinks()
	{
		$this->url = "http://production.leadconduit.com/ap/v2/PostLeadAction";
		$this->lead_id = BRINKS;
	}	

	function format()
	{

		$thearray = $this->thearray;
		
		$thearray["sqr_footage"] = str_replace(",","",$thearray["sqr_footage"]);
		$thearray = str_replace(array(",","\n","'")," ",$thearray);
	
		extract($this->thearray);
		$sitetype = (($quote_type=="com")?"C":"R");
		if ($sitetype == "C")
		{
			list($fname,$lname) = split(" ",$name2);
			$businessname = $name1;	
		}
		else
		{
			$fname = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
			$lname = substr($name2,0,20);//same with contact name, or long lastname.
		}
		
		$sqrfootage = $this->sqrFootage($sitetype,$thearray["sqr_footage"]);
		$city = substr($city,0,30);
		$email = substr($email,0,40);
		$parts = split(" ",$address,2);//hopefully <[\d]*> [(w|n|e|s)] <[A-Z(a-z)*] [(st|dr|av|..)]>
		$street_number = substr($parts[0],0,20);
		$street_number = substr($street_number,0,19);		
		$street_name = substr($parts[1],0,20);
		if (strlen($phone2) < 10)
			$phone2 = "";
		if (strlen($phone1) < 10)
			$phone1 = "";
			
		if ($own_rent == "Own" || ($quote_type=="com"))
			$renter = 0;
		else
			$renter = 1;
					
		$newcomments = "";
		
		if ($fire == 1 || $access == 1 || $cctv == 1 || strlen($other_field) > 0)
		{
			$newcomments.= " ExtraServices:";
			if ($fire == 1)
				$newcomments  .= "Fire Protection ";
			if ($access == 1)
				$newcomments  .= "Access Control System ";				
			if ($cctv == 1)
				$newcomments  .= "CCTV System ";
			if (strlen($other_field) > 0)
				$newcomments  .= xmlentities($other_field);
			
			$newcomments .= " ";
		}
		$building = $this->buildingType($building_type);		
			
			
		if ($quote_type == "com" && $num_location > 1)
			$newcomments .=	"Num of Locations: $num_location ";
		$brinks_customer_comments = trim($comments);	
		
		if (strlen(trim($comments)) > 0)
		{			
			$length = 240 - strlen($newcomments);
			if ($length > strlen($brinks_customer_comments))//it will all fit, just add it
				$newcomments .= "Cust Comments:".$brinks_customer_comments;
			else//trim their comments down
				$newcomments .= "Cust Comments:".substr($brinks_customer_comments,0,$length-1);					
		}


		if ( eregi("clkbth",$source) ) { //-- if it's a click booth lead, we get one type of source_tag, otherwise we use the original:

			switch($company_count) {
				case 1:
					$marketsource = "N769";
					$nodeid = "000w3xy4i";
					break;
				case 2:
					$marketsource = "N770";
					$nodeid = "000w3xy4h";
					break;				
				case 3:
				default:
					$marketsource = "N771";
					$nodeid = "000w3xy4g";
					break;							
			}		

		} else { //-- if NOT clickbooth, then assign the original codes
		
			switch($company_count) {
				case 1:
					$marketsource = "N755";
					$nodeid = "000w3xy4i";
					break;
				case 2:
					$marketsource = "N756";
					$nodeid = "000w3xy4h";
					break;				
				case 3:
				default:
					$marketsource = "N757";
					$nodeid = "000w3xy4g";
					break;							
			}		
		}
			//-- NOTE: ProgramID is the new marketsource
			//-- TESTING - some of the values below will need to change before going live, it is set up for testing only
			//-- QUESTIONS:
				# do we need to split the address into StreetNumber and StreetName?
				# I'm going to be pushing an R for SiteType, this allows me to change it if necessary
			
/* OLD Where address is separated
		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetNumber=".urlencode($street_number)."&".
			"StreetName=".urlencode($street_name)."&".
			"City=".urlencode($city)."&".
			"State=$state_code&".
			"ZipCode=$zip&".
			"PrimaryPhone=$phone1&".
			"EmailAddress=".urlencode($email)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);
*/
			
		 $this->lead_body = "xxNodeId=$nodeid&".
			"FirstName=".urlencode($name1)."&".
			"LastName=".urlencode($name2)."&".
			"StreetAddress=".urlencode($address)."&".
			"City=".urlencode($city)."&".
			"State=$state_code&".
			"ZipCode=$zip&".
			"PrimaryPhone=$phone1&".
			"EmailAddress=".urlencode($email)."&".
			"ProgramID=$marketsource&".
			"SiteType=$sitetype&".
			"Comment=".urlencode($newcomments);
			
		
#		mail("rob@irelocation.com","NEW BRINKS USALARM Lead Formatted","QuoteID: ". $this->thearray["quote_id"]."\n". $this->lead_body);
		
	}

	function send()
	{
		$ch=curl_init($this->url."?".$this->lead_body);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		$response = curl_exec($ch);
		curl_close($ch);
		
		$this->processResponse($response);
	}
	
	function processResponse($response)
	{
		$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
		echo $content."<br/>";
		
		if (substr_count(strtolower($response),"success") == 1) { //-- reports a success
			save_response($this->thearray["quote_id"], $response, BRINKS,1,"usalarm");
			
		} elseif (substr_count(strtolower($response),"failure") == 1) { //-- reports a failure
			save_response($this->thearray["quote_id"], $response, BRINKS,0,"usalarm");
		
		} else { //-- can't determine what the reponse it, so send out an email
			mail("code@irelocation.com","UNDETERMINED BRINKS RESPONSE",$response);
		}		
				
	}
}







class SafeMart extends SecurityClass
{
	function SafeMart() 
	{
		$this->emailaddress = "david@safemart.com";
		$this->lead_id = SAFEMART;
	}

	function test()
	{
		echo "TESTING SAFEMART: ";
		echo $this->thearray['own_rent'];
		echo "<br />";
		if ( strtolower($this->thearray['own_rent']) == "rent" && strtolower($this->thearray['quote_type']) == "res" ) { //-- only residential renters
			echo "SafeMart 1653 ONLY WANTS RENTers<br />";
			return true;
		} else {
			echo "SafeMart 1653 DOESNT WANT OWNers<br />";
			return false;
		}

	}

	function format()
	{
		extract($this->thearray);
			
		//-- append the quote_type if it's a click booth 
		if ( eregi("clkbth",$source) ) {
			$lead_type .= " email ";
		} else {
			$lead_type .= " IC ";  //-- stands for Internet Campaign
		}
			
			
			$this->email_message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $lead_type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: $name2, $name1\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->email_message .= "Alternate Phone: $phone2\n";
					
			$this->email_message .= "Street Address: $address\n".
						"City: $city\n".
						"State: $state_code\n".
						"Zip Code: $zip\n".
						"Type of Business: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".			
						"Source: $source_tag\n" . 
						"Customer Comments: $comments";								
		
	}

	function send()
	{
		$this->format();
		mail($this->emailaddress,"Security Lead (usalarm)",$this->email_message,$this->headers);
	}
	
}



//-- =============================

	class Pinnacle extends SecurityClass
	{				
		function Pinnacle()
		{
			$this->url = "https://us2.five9.com/web2campaign/AddToList";
			$this->lead_id = PINNACLE;
		}
	
		function test() {
						
			//-- now check to see if the zip is in Pinnacle (1660) zip list
			$sql = "select zip From security_zip_codes where lead_id = '1660' and zip = '" . $this->thearray['zip']."' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();

			if ( !$valid ) {
				echo "Pinnacle 1660 DOES NOT WANT " . $this->thearray['zip'] . " (NOT FOUND IN Pinnacle ZIP LIST)<br />";
				
				return false;
			} else {
				echo "Pinnacle 1660 DOES WANT " . $this->thearray['zip'] . " (FOUND IN Pinnacle ZIP TABLE)<br />";
				return true;
			}

		}
	
		function format()
		{
			extract($this->thearray);

			 $this->lead_body = "F9domain=".rawurlencode("Pinnacle Alarm")."&".
				"F9list=".rawurlencode("Internet Leads IR")."&".
				"F9CallASAP=1&".
				"first_name=".rawurlencode($name1)."&".
				"last_name=".rawurlencode($name2)."&".
				"street=".rawurlencode($address)."&".
				"city=".rawurlencode($city)."&".
				"Email%20Address=".rawurlencode($email)."&".
				"state=$state_code&".
				"zip=$zip&".
				"number1=$phone1";
			
		}
	
		function send()
		{
			echo "<br /><br />SENDING PINNACLE LEAD :: " . $this->url . $this->lead_body . "<br /><br />";
			#mail("rob@irelocation.com","PINNACLE LEAD",$this->url . $this->lead_body);
			
			$ch=curl_init($this->url."?".$this->lead_body);	
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
			$response = curl_exec($ch);
			curl_close($ch);
			
			$this->processResponse($response);
		}
		
		function processResponse($response)
		{
			$content = "Url: ".$this->url."?".$this->lead_body." <br/>Response: $response<br/>";
			
			$response = eregi_replace("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 Transitional//EN\"><HTML><HEAD><title>","\n",$response);
			$response = eregi_replace("</title><META http-equiv=\"Content-Type\"   content=\"text/html\"></HEAD><BODY>","\n",$response);
			$response = eregi_replace("<h1>","\n",$response);
			$response = eregi_replace("</h1>","\n",$response);
			$response = eregi_replace("<TR>","\n",$response);
			$response = eregi_replace("</TR>","\n",$response);
			$response = eregi_replace("<TD>","\n",$response);
			$response = eregi_replace("</TD>","\n",$response);
			$response = eregi_replace("</TABLE></FORM></BODY></HTML>","\n",$response);
			$response = eregi_replace("<INPUT","\n",$response);
#			$response = eregi_replace("xxx","",$response);
			
			if ( eregi("name=\"F9errCode\" value=\"0\"",$response) ) { //-- 0 means No Errors

				save_response($this->thearray["quote_id"], "Lead Accepted - $response",PINNACLE,1,"usalarm");
				echo "Pinnacle Lead - Success";
				recordLeadFailure($this->thearray,PINNACLE,"security","irelocation.leads_security");
			
			} else {
				
				save_response($this->thearray["quote_id"], "Failure - $response", PINNACLE,0,"usalarm");
				echo "PINNACLE Lead - Failure";
			}
		}
	}




?>