<?php
/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P
 
 	function markAsSent($quote_id)
 	{
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'usalarm'";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes($quote_ids="*")
	{
		 $sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 ";
		 if ($quote_ids != "*")
		 {
			$sql .= " and quote_id in ($quote_ids)";		 	
		 }
	 	 $rs = new mysql_recordset($sql);
	 	 $quote_ids = array();
	 	 
	 	 while($rs->fetch_array())
	 	 	$quote_ids[] = $rs->myarray['quote_id'];
	 	 
	 	 if (count($quote_ids) > 0)
	 	 {
		 	 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'usalarm'";
		 	 echo "Update SQL: ".$sql."<br/>";
		 	 $rs = new mysql_recordset($sql);
			 	 
	 		 return $quote_ids;
	 	 }
	 	 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.
	$all_companies = array(GAYLORD => new Gaylord(),
							PROTECT_AMERICA => new ProtectAmerica(),
							BRINKS => new Brinks());
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}

	if ($_REQUEST['quote_ids'] == "")
	{
		exit();
	}
	
	$quote_ids = selectQuotes($_REQUEST['quote_ids']);
	
	if (count($quote_ids) == 0)
	{
		echo "No Quotes.";
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	
	
	//loop over each one.
	while($rs->fetch_array())
	{			
		$lead_ids = $rs->myarray['lead_ids'];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
				
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);
		
		$myarray = formatData($rs->myarray);
		
		$myarray['company_count'] = $company_count;
		
		//loop through each company that should get this lead.
		foreach($lead_ids as $lead_id)
		{
			$company = $active_companies[$lead_id];
			//assign it?
			if ($company->assign($myarray))//assign the array, and test them.
			{													
				$company->send();			
				echo $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
			}	
			else
			{
				echo "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
				echo "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
			}
		}	
		//mark lead when done.
		markAsSent($rs->myarray['quote_id']);
		
		//if we have too..
		if ($remove_from_brinks)
			removeFromBrinks($rs->myarray['quote_id'],implode("|",$lead_ids));
		
	}
?>

