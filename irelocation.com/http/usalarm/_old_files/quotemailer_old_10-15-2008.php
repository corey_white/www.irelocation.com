<?php
/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 
 9/10/08 - Rob - Adding functionality that "pre-washes" the quotes so that Brinks doesn't get RES Renters and APEX doesn't get RES Renters and no CLICKBOOTH leads.
 
 9/12/08 - Rob - Okay, we're now changing the above as NO company wants renters.  Changing the initial SQL to only pull own_rent = own.  Also adding a function to delete the lead_ids from any renter lead.
 
 10-9-2008 - 10-10-2008 - Rob - Adding ability for a client to get renter leads
 */
 
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P
 
 	function markAsSent($quote_id)
 	{
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'usalarm'";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes()
	{
		
		// This SQL set the Lead_ids if the field is blank for OWNERS
		$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569' where lead_ids is null and campaign = 'usalarm' and own_rent = 'own' ";
		echo "This SQL set the Lead_ids if the field is blank: <br />";
		$rs = new mysql_recordset($sql);
		
		// This SQL set the Lead_ids if the field is blank for RENTERS
		$sql = "update irelocation.leads_security set lead_ids = '1653' where lead_ids is null and campaign = 'usalarm' and own_rent = 'rent' ";
		echo "This SQL set the Lead_ids if the field is blank: <br />";
		$rs = new mysql_recordset($sql);
		
		//-- delete all lead_ids where own_rent = rent.
/*  DON'T WANT TO DELETE RENT ANYMORE
		$sql = "update irelocation.leads_security set lead_ids = '' where campaign = 'usalarm' and own_rent = 'rent' and ready_to_send = 1 ";
		echo "delete all lead_ids where own_rent = rent: $sql<br />";
		$rs = new mysql_recordset($sql);
*/

		
		//-- SPECIAL VALIDATION ============================================
		//-- this secton removes lead_ids for those companies that don't want a certain type of lead
#		$sql = "select quote_id, received, source, quote_type, own_rent, lead_ids, campaign from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- now want renters
		$sql = "select quote_id, received, source, quote_type, own_rent, lead_ids, campaign from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 ";
		
		echo " SPECIAL VALIDATION SELECT: $sql<br />";
		$rs = new mysql_recordset($sql);

		while( $rs->fetch_array() ) {
			 extract($rs->myarray);
	 echo "$quote_id<br />";
	 echo "$source<br />";
#	 echo "$received<br />";
	 echo "$quote_type<br />";
	 echo "$own_rent<br />";
	 echo "ORIG: $lead_ids<br />";
#	 echo "$campaign<br />";
	echo "<br />";


			 echo "<br />PROCESSING $quote_id<br />";
			
			 //-- validate APEX 1569 ================================== 1569
/*  APX (1569) THINKS THEY WANT CLICKBOOTH LEADS NOW  10/7/08 16:30 PM
			if ( ereg( "1569", $lead_ids ) ) {
				if ( eregi("clkbth",$source) ) { //-- don't want clickbooth
					$lead_ids = str_replace("1569","",$lead_ids);
				} 
			} 
			echo "LEAD IDS AFTER APEX SCRUB: $lead_ids<br />";
			
			//-- remove double pipes ( || )
			$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
			
			//-- remove pipes as the first location
			if ( substr($lead_ids,0,1) == "|"  ) {
				$lead_ids = substr($lead_ids,1);
			} 
			
			//-- remove pipes as the last location
			if ( substr($lead_ids,-1,1) == "|"  ) {
				$lead_ids = substr($lead_ids,0,-1);
			} 
			
*/
			// -- ======= END APEX 1569 VALIDATION ===================== end 1569
	
			$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
			echo "SPECIAL VALIDATION UPDATE SQL: $sql_update <br /><br />";
			$rs_update = new mysql_recordset($sql_update);
	
		}		
		//-- END SPECIAL VALIDATION ======================


		//-- continue normal processing...
		
		//-- NO Company wants to get renters
#	 $sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- OKAY TO PULL RENTERS AGAIN
		$sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'usalarm'";
			 echo "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.
/* 
	$all_companies = array(GAYLORD => new Gaylord(),
							PROTECT_AMERICA => new ProtectAmerica(),
							BRINKS => new Brinks());
*/
	
/* this NOT WORKING
	$all_companies = array(	GAYLORD => new Gaylord(),
											BRINKS => new Brinks(),
											APX => new APXSecurity(),
											SAFEMART => SafeMart()
											);
*/
	
	$all_companies = array(	GAYLORD => new Gaylord(),
											BRINKS => new Brinks(),
											APX => new APXSecurity(), 
											SAFEMART => new SafeMart()
											);
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	echo "Active Companies:<pre>";
	print_r($active_lead_ids);
	echo "</pre><br />";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}

	
	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		echo "No Quotes.";
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	echo "QUOTE SELECT: $sql<br />";
	
	//loop over each one.
	while($rs->fetch_array())
	{			
		$lead_ids = $rs->myarray['lead_ids'];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
				
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);
		
		$myarray = formatData($rs->myarray);
		
		$myarray['company_count'] = $company_count;
		
		//loop through each company that should get this lead.
		foreach($lead_ids as $lead_id) {
			$company = $active_companies[$lead_id];
			echo "Now processing $company<br />";

			//assign it?
			if ($company->assign($myarray)) { //assign the array, and test them.
				$q_id = $myarray['quote_id'];

				$company->send();			
				echo $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
				
			} else {
				$q_id = $myarray['quote_id'];
				//mail("rob@irelocation.com","Brinks Lead Bounce","This email was generated on line 118 of irelocation.com/usalarm/quotemailer.php \n\n Lead ID: $lead_id doesn't want quote_id: $q_id ");
				echo "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
				echo "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
			}
		}	
		//mark lead when done.
		markAsSent($rs->myarray['quote_id']);
		
		//if we have too..
		if ($remove_from_brinks)
			removeFromBrinks($rs->myarray['quote_id'],implode("|",$lead_ids));
		
	}
?>

