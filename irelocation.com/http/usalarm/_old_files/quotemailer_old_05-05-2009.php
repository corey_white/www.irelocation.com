<?php
/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 
 9/10/08 - Rob - Adding functionality that "pre-washes" the quotes so that Brinks doesn't get RES Renters and APEX doesn't get RES Renters and no CLICKBOOTH leads.
 
 9/12/08 - Rob - Okay, we're now changing the above as NO company wants renters.  Changing the initial SQL to only pull own_rent = own.  Also adding a function to delete the lead_ids from any renter lead.
 
 10-9-2008 - 10-10-2008 - Rob - Adding ability for a client to get renter leads
 
 5/4/09 - Rob - On 5-1, added massive amounts fo code for the way we're not giving out leads.  Lots of logic for determining who gets the lead.  Today, still working on fine-tuning that code.
 */
 
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P
	
	//-- first thing, update the received column for those leads that came in from 
	$mytime = date("YmdHis");
	$sql_time = "update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and campaign = 'usalarm' ";
	$body .= "UPDATING RECEIVED TIME: $sql_time<br/>";
	$rs_time=new mysql_recordset($sql_time);

 
	//-- UPDATE LEAD IDS AS NEEDED
	// This SQL set the Lead_ids if the field is blank for OWNERS
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1520|1660|1690' where (lead_ids is null || lead_ids = '' ) and campaign = 'usalarm' and own_rent = 'own' ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	// This SQL set the Lead_ids if the field is blank for RENTERS
	$sql = "update irelocation.leads_security set lead_ids = '1520' where (lead_ids is null || lead_ids = '' ) and campaign = 'usalarm' and own_rent = 'rent' ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	
	//-- SPECIAL VALIDATION ============================================
	//-- this secton removes lead_ids for those companies that don't want a certain type of lead
	
	//-- now want renters
	//-- 1/20/09 15:12 PM - added substrCount(lead_ids, '|') > 3 to this SQL so we only pull those leads that have not already been processed - this assumes there are more 
	//-- than 3 pipes ( | ) in the lead_ids field
	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and substrCount(lead_ids, '|') > 3 ";
	
	$body .= " SPECIAL VALIDATION SELECT: $sql<br />\n";
	$rs_getit = new mysql_recordset($sql_getit);

	while( $rs_getit->fetch_array() ) {
		 extract($rs_getit->myarray);
		 
		$body .= "LEAD INFO<br />\n";
		$body .= "$quote_id<br />\n";
		$body .= "$source<br />\n";
		$body .= "$quote_type<br />\n";
		$body .= "$own_rent<br />\n";
		$body .= "ORIG: $lead_ids<br />\n";
		$body .= "<br />\n";


		$body .= "<br />\nPROCESSING $quote_id<br />\n";
		
		/*
		If in 1569 zip list, then get rid of 1660 and 1520 - done
		
		if NOT in 1569 zip list
			get rid of 1569 and 1660
			give to 1520
		
		*/
		$body .= "<br />\nNOW RUNNING APX CRITERIA<br />\n";
		$body .= "NOW RUNNING APX CRITERIA<br />\n<br />\n";
		//-- check to see if Protect America wants the zip (they get all zips NOT in APX's list) ================================== 1520
		$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
		$rs = new mysql_recordset($sql);
		$valid = $rs->fetch_array();

		if ( $valid  && $own_rent != "rent"  ) { //-- Found in APX zip list, so Protect America doesn't want the zip (unless it's a renter)
			$body .= "Found Zip $zip in APX Zip List and is a OWNer, so Protect America Does Not Want it<br />\n";
			
/*
			$current_month = date("Ym");
			$body .= "Current Month set to $current_month<br />\n";
			
			//-- 1569 OR 1660 GETS THIS LEAD - REMOVE 1520 
			$lead_ids = str_replace("1520","",$lead_ids);
			$body .= "1569 OR 1660 GETS THIS LEAD - REMOVE 1520<br />\n";
			
			//-- GET LEADS PER DAY ALLOWED for 1660
			$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1660 and site_id = 'usalarm' and month = '$current_month' ";
			$rs_pin = new mysql_recordset($sql_pin);
			$rs_pin->fetch_array();
			$leads_per_day = $rs_pin->myarray["leads_per_day"];
			
			//-- SEE IF ZIP IN THEIR ZIP LIST for 1660
			$sql_pinzip = "select zip From security_zip_codes where lead_id = '1660' and zip = '$zip' limit 1 ";
			$rs_pinzip = new mysql_recordset($sql_pinzip);
			$valid_pinzip = $rs_pinzip->fetch_array();
			$body .= "sql_pinzip = $sql_pinzip<br />\n";
			
			//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660
			$today = date("Ymd");
			$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1660%' and ready_to_send = 3 ";
			$rs_pincount = new mysql_recordset($sql_pincount);
			$rs_pincount->fetch_array();
			$pincount = $rs_pincount->myarray["count"];

			$body .= "<br />\nleads_per_day for 1660 = $leads_per_day<br />\n";
			$body .= "Looking for zip in 1660 = $valid_pinzip<br />\n";
			$body .= "CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660 = $pincount<br />\n";

			if ( $valid_pinzip && ( $pincount <= $leads_per_day ) ) { //-- If zip in Pinnacle zip list and they haven't received all their leads for today, remove APX and PA (1520)
				$lead_ids = str_replace("1569","",$lead_ids);
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "zip in Pinnacle zip list and they haven't received all their leads for today, remove APX (1569) and PA (1520)<br />\n";
				$save_pinnacle = "yes";
				
			} else { //-- otherwise, remove Pinnacle
				$lead_ids = str_replace("1660","",$lead_ids);
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "Removing Pinnacle (1660) and PA (1520)<br />\n";
			}
*/
#				$lead_ids = str_replace("1660","",$lead_ids); //-- CAN'T remove Pinnacle here and now cuz we need them to be present for the Brinks srub below.
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "Removing PA (1520)<br />\n";

			
		} else {

			$body .= "Zip $zip Not Found in APX List or is a RENTER, so 1569 DOES NOT GET THIS LEAD<br />\n";

			//-- remove 1569 & 1660 from the equation
			$lead_ids = str_replace("1569","",$lead_ids);
#			$lead_ids = str_replace("1660","",$lead_ids);
			$body .= "Removing APX (1569) <br />\n";
#			$body .= "Removing APX (1569) and Pinnacle (1660)<br />\n";
		}

		$body .= "<br />\nLEAD IDS AFTER APX SCRUB: $lead_ids<br />\n";
		
		
		
		$body .= "<br />\nNOW RUNNING BRINKS CRITERIA<br />\n";
		$body .= "NOW RUNNING BRINKS CRITERIA<br />\n<br />\n";
		//-- Now, we need to do it again for Brinks (1519), FrontPoint (1690) and Pinnacle (1660).
		/* General Logic:
			1. Check to see if the zip is in Brinks zip list
				IF it is, take out 1660 and 1690 from the lead_ids list.
				ELSE if not
					check to see if 1660 and 1690 can still get leads for the day
						IF so, randomly pick one to get the lead, remove the other and 1519 from the lead_ids list
						ELSE if only one can get it, then give to to that one, removing the other and 1519 from the lead_ids list
						ELSEIF no one can get it, remove 1519, 1660 and 1690 from the lead_ids list
		*/
		//-- check to see if Brinks wants the zip ==================================  1519, 1660, 1690
		$sql = "select zip From security_zip_codes where lead_id = '1519' and zip = '$zip' limit 1";
		$rs = new mysql_recordset($sql);
		$valid = $rs->fetch_array();

		if ( $valid  && $own_rent != "rent"  ) { //-- Found in Brinks zip list, so Pinnacle and FrontPoint can't get the zip (unless it's a renter)
			$body .= "Found Zip $zip in Brinks Zip List and is an OWNer, so Pinnacle and FrontPoint can't get it<br />\n";
			
			//-- remove 1569 & 1660 from the equation
			if ( $save_pinnacle == "yes" ) { //-- don't remove Pinnacle (1660) if they were to get it from the APX scrub above
				$body .= "Performed a 'save' on Pinnacle, as they were supposed to get this from the APX scrub (IOW, not removing Pinnacle)<br />\n";
			} else {
				$lead_ids = str_replace("1660","",$lead_ids);
				$body .= "Removing Pinnacle (1660)<br />\n";
			}
			$lead_ids = str_replace("1690","",$lead_ids);
			$body .= "Removing FrontPoint (1690)<br />\n";

			
		} else {

			$body .= "Zip $zip Not Found in Brinks List or is a RENTER, so 1660 & 1690 CAN GET THIS LEAD<br />\n";
			$lead_ids = str_replace("1519","",$lead_ids);	
			$body .= "Removing Brinks (1519)<br />\n";

			$current_month = date("Ym");
			$body .= "Current Month set to $current_month<br />\n";
			
			//-- GET LEADS PER DAY ALLOWED for 1660
			$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1660 and site_id = 'usalarm' and month = '$current_month' ";
			$rs_pin = new mysql_recordset($sql_pin);
			$rs_pin->fetch_array();
			$leads_per_day1660 = $rs_pin->myarray["leads_per_day"];
			$body .= "sql_pin = $sql_pin<br />\n";

			//-- GET LEADS PER DAY ALLOWED for 1690
			$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1690 and site_id = 'usalarm' and month = '$current_month' ";
			$rs_pin = new mysql_recordset($sql_pin);
			$rs_pin->fetch_array();
			$leads_per_day1690 = $rs_pin->myarray["leads_per_day"];
			$body .= "sql_pin = $sql_pin<br />\n";
			
			$body .= "<br />\nleads_per_day1660 = $leads_per_day1660<br />\n";
			$body .= "leads_per_day1690 = $leads_per_day1690<br />\n<br />\n";
			
			//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660
			$today = date("Ymd");
			$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1660%' and ready_to_send = 3 ";
			$rs_pincount = new mysql_recordset($sql_pincount);
			$rs_pincount->fetch_array();
			$pincount1660 = $rs_pincount->myarray["count"];
			$body .= "sql_pincount = $sql_pincount<br />\n";

			//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1690
			$today = date("Ymd");
			$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1690%' and ready_to_send = 3 ";
			$rs_pincount = new mysql_recordset($sql_pincount);
			$rs_pincount->fetch_array();
			$pincount1690 = $rs_pincount->myarray["count"];
			$body .= "sql_pincount = $sql_pincount<br />\n";

			$body .= "<br />\npincount1660 = $pincount1660<br />\n";
			$body .= "pincount1690 = $pincount1690<br />\n<br />\n";
			
			//-- SEE IF ZIP IN THEIR ZIP LIST for 1660
			$sql_pinzip = "select zip From security_zip_codes where lead_id = '1660' and zip = '$zip' limit 1 ";
			$rs_pinzip = new mysql_recordset($sql_pinzip);
			$valid_pinzip = $rs_pinzip->fetch_array();
			$body .= "sql_pinzip 1660 = $sql_pinzip<br />\n";

			if ( $valid_pinzip ) { //-- is it in Pinnacle's Zip List?
				$pinnacle_zip = "yes";
			} else {
				$pinnacle_zip = "no";
			}
			$body .= "Is Zip in Pinnacle's Zip List: $pinnacle_zip<br />\n";

			if (  $leads_per_day1660 <= $pincount1660 || $pinnacle_zip == "no" ) {
				$okToGet1660 = "no";
			} else {
				$okToGet1660 = "yes";
			}
			
			$body .= "Pinnacle: Is $leads_per_day1660 <= $pincount1660<br />\n";
			$body .= "Pinnacle (1660) still needs leads for today AND this lead is in their zip list: $okToGet1660<br />\n";

			if ( $leads_per_day1690 <= $pincount1690 ) {
				$okToGet1690 = "no";
			} else {
				$okToGet1690 = "yes";
			}

			$body .= "FrontPoint: Is $leads_per_day1690 <= $pincount1690<br />\n";
			$body .= "FrontPoint (1690) still needs leads for today: $okToGet1690<br />\n";


			if ( $okToGet1660 == "yes" ) { //-- both are eligible, but give to Pinnacle as their zips don't come up often enough
/* if the zip is in Pinnacle's zip list, we need to give it to them (if they're not over their quota for the day), so bypassing this random selection
				$body .= "RANDOMLY choosing one.<br />\n";
				$rand = rand(0,100);
				$body .= "RANDOM NUMBER IS $rand<br />\n";
				if ($rand < 20) {
					$lead_ids = str_replace("1660","",$lead_ids);	
					$body .= "Removing Pinnacle (1660)<br />\n";
				} else {
					$lead_ids = str_replace("1690","",$lead_ids);
					$body .= "Removing FrontPoint (1690)<br />\n";
				}
*/
				$lead_ids = str_replace("1690","",$lead_ids);
				$body .= "Removing FrontPoint (1690)<br />\n";


/*
			} elseif ( $okToGet1660 == "yes" && $okToGet1690 == "no" ) { //-- remove Frontpoint
				$body .= "Removing FrontPoint (1690)<br />\n";
				$lead_ids = str_replace("1690","",$lead_ids);
*/
				
			} elseif ( $okToGet1690 == "yes" ) { //-- give to FrontPoint
				$body .= "Removing Pinnacle (1660)<br />\n";
				$lead_ids = str_replace("1660","",$lead_ids);//-- remove Pinnacle
				
			} else { //-- otherwise, remove both
				$body .= "REMOVING BOTH!! - Looks like both have already received their quota for the day!<br />\n";
				$lead_ids = str_replace("1660","",$lead_ids);
				$lead_ids = str_replace("1690","",$lead_ids);
				mail("rob@irelocation.com","MATRIX SHORT ON LEADS","You're getting this email because both Pinnacle and FrontPoint have reached their daily quota and there's no one to give the lead to!!!");
			}

		}
		

/*
		
		$body .= "LEAD IDS AFTER APX - PROTECT AMERICA SCRUB: $lead_ids<br />\n";
		$body .= "<br />\n";
		
		//-- NOW, WE NEED TO SEE WHO GETS THE LEAD BETWEEN WHO'S LEFT
		//-- it will either be 1569 & 1660 or 1520 & 1660
		
		
		 //-- SECOND SCRUB
		 //-- validate APEX 1569 ================================== 1569
		$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
		$rs = new mysql_recordset($sql);
		$valid = $rs->fetch_array();

		if ( !$valid || $own_rent == "rent" ) { //-- doesn't want the zip and is a renter
			$body .= "Zip $zip Not Found in APX Zip List (or is a RENTER)<br />\n";
			$lead_ids = str_replace("1569","",$lead_ids);
		} else {
			$body .= "Zip $zip FOUND in APX List (or is a RENTER)<br />\n";
			
			//-- If we get to this point, if we get here, then APX does want the lead, but now, we need to siphon off 30 of these per day to Pinnacle (1660)
			
			
		}
		
*/
		
		
		
		//-- THIS SECTION BELOW SCRUBS THE LEAD_IDS FIELD TO MAKE IT GOOD AGAIN
		
		//-- Finish scrubbing the lead_ids before reinserting them into the quote
		//-- remove quad pipes ( |||| ) - just in case this condition exists
		$lead_ids = ereg_replace("\|\|\|\|","\|",$lead_ids);
		$body .= "Pipe Scrub 1: $lead_ids<br />\n";
		
		//-- remove triple pipes ( ||| ) - just in case this condition exists
		$lead_ids = ereg_replace("\|\|\|","\|",$lead_ids);
		$body .= "Pipe Scrub 2: $lead_ids<br />\n";
		
		//-- remove double pipes ( || )
		$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
		$body .= "Pipe Scrub 3: $lead_ids<br />\n";
		
		$lead_ids = stripslashes($lead_ids);
		$body .= "LEAD IDS after stripslashes: $lead_ids<br />\n";

		//-- remove pipes as the first location
		if ( substr($lead_ids,0,1) == "|"  ) {
			$lead_ids = substr($lead_ids,1);
		} 
		$body .= "Pipe Scrub 4: $lead_ids<br />\n";
		
		//-- remove pipes as the last location
		if ( substr($lead_ids,-1,1) == "|"  ) {
			$lead_ids = substr($lead_ids,0,-1);
		} 
		$body .= "Pipe Scrub 5: $lead_ids<br />\n";

		$body .= "NEW LEAD IDS: $lead_ids<br />\n";
		

		//-- if there are less than 3 lead_ids, then add Brinks (1519) back into the mix, as we haven't sold all of the leads that they don't want yet.
		$brinks_arr = split("\|",$lead_ids);
		$brinks_test = count($brinks_arr);
		$body .= "brinks_test = $brinks_test<br />\n";
		
		if ( $brinks_test < 3 ) { 
			$lead_ids = $lead_ids . "|1519";
			$body .= "***RE-ADDING BRINKS AS WE HAVE NO ONE ELSE TO GIVE THE LEAD TO***<br />\n";
		} else {
			$body .= "There are more than 2 lead_ids, Not Re-adding Brinks<br />\n";
		}

		$body .= "<br />\nLEAD IDS AFTER BRINKS SCRUB: $lead_ids<br />\n";


		//-- save new lead_id list to record
		$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
		$body .= "SPECIAL VALIDATION UPDATE SQL: $sql_update <br />\n<br />\n";
		$rs_update = new mysql_recordset($sql_update);


	}		

	//-- END SPECIAL VALIDATION ======================

	$body .= "<br />\nLEAD IDS AFTER ALL SCRUBS AND SPECIAL VALIDATIONS: $lead_ids<br />\n";

	

 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'usalarm'";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes() {
		
		//-- continue normal processing...
		
		//-- NO Company wants to get renters
#	 $sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- OKAY TO PULL RENTERS AGAIN
		$sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'usalarm'";
			 $body .= "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.
	
	$all_companies = array(	GAYLORD => new Gaylord(),
											BRINKS => new NewBrinks(),
											APX => new APXSecurity(), 
											FRONTPOINT => new FrontPoint(), 
											PROTECT_AMERICA => new ProtectAmerica(),
											PINNACLE => new Pinnacle()
											);
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	$body .= "Active Companies:<pre>";
	$body .= print_r($active_lead_ids,true);
	$body .= "</pre><br />\n";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}


	
	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		$body .= "No Quotes.";
		echo $body;
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	$body .= "QUOTE SELECT: $sql<br />\n";
	
	//loop over each one.
	while($rs->fetch_array())
	{			
		$lead_ids = $rs->myarray['lead_ids'];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
				
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);
		
		$myarray = formatData($rs->myarray);
		
		$myarray['company_count'] = $company_count;
		
		//loop through each company that should get this lead.
		foreach($lead_ids as $lead_id) {
			$company = $active_companies[$lead_id];
			$body .= "Now processing $company<br />\n";

			//assign it?
			if ($company->assign($myarray)) { //assign the array, and test them.
				$q_id = $myarray['quote_id'];
				$body .= "Looking to send $q_id<br />\n";

				$company->send();			
				$body .= $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
				
			} else {
				$q_id = $myarray['quote_id'];
				//mail("rob@irelocation.com","Brinks Lead Bounce","This email was generated on line 118 of irelocation.com/usalarm/quotemailer.php \n\n Lead ID: $lead_id doesn't want quote_id: $q_id ");
				$body .= "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
				$body .= "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				

//-- Update the quote_id and take out the lead_id that doesn't want the lead	
$sql_rem = " update leads_security set lead_ids = replace(lead_ids,'$lead_id','') where quote_id = ".$myarray['quote_id'] ;
$body .= "Running: $sql_rem<br />\n";
$rs_rem = new mysql_recordset($sql_rem);

//-- Remove the occaisional double pipes ( || ) that are created	
$sql_dp = " update leads_security set lead_ids = replace(lead_ids,'||','|') where quote_id = ".$myarray['quote_id'] ;
$body .= "Running: $sql_dp<br />\n";
$rs_dp = new mysql_recordset($sql_dp);

//-- Trim pipes ( | )
$sql_trim = " update leads_security set lead_ids = trim(BOTH '|' FROM lead_ids ) where quote_id = " .$myarray['quote_id'] ;
$body .= "Running: $sql_trim<br />\n";
$rs_trim = new mysql_recordset($sql_trim);


			}
		}	
		//mark lead when done.
		markAsSent($rs->myarray['quote_id']);
		
		//if we have too..
		if ($remove_from_brinks)
			removeFromBrinks($rs->myarray['quote_id'],implode("|",$lead_ids));
		
	}
	
echo $body;
eregi_replace("<br />","",$body);
mail("rob@irelocation.com","USALARM QUOTEMAILER OUTPUT",$body);

?>

