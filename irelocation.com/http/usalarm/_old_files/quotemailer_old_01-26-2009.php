<?php
/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 
 9/10/08 - Rob - Adding functionality that "pre-washes" the quotes so that Brinks doesn't get RES Renters and APEX doesn't get RES Renters and no CLICKBOOTH leads.
 
 9/12/08 - Rob - Okay, we're now changing the above as NO company wants renters.  Changing the initial SQL to only pull own_rent = own.  Also adding a function to delete the lead_ids from any renter lead.
 
 10-9-2008 - 10-10-2008 - Rob - Adding ability for a client to get renter leads
 */
 
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P
	
	//-- first thing, update the received column for those leads that came in from 
	$mytime = date("YmdHis");
	$sql_time = "update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and campaign = 'usalarm' ";
	echo "UPDATING RECEIVED TIME: $sql_time<br/>";
	$rs_time=new mysql_recordset($sql_time);

 
	//-- UPDATE LEAD IDS AS NEEDED
	// This SQL set the Lead_ids if the field is blank for OWNERS
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1520|1660' where (lead_ids is null || lead_ids = '' ) and campaign = 'usalarm' and own_rent = 'own' ";
	echo "Ran $sql<br />";
	$rs = new mysql_recordset($sql);
	
	// This SQL set the Lead_ids if the field is blank for RENTERS
	$sql = "update irelocation.leads_security set lead_ids = '1520' where (lead_ids is null || lead_ids = '' ) and campaign = 'usalarm' and own_rent = 'rent' ";
	echo "Ran $sql<br />";
	$rs = new mysql_recordset($sql);
	
	
	//-- SPECIAL VALIDATION ============================================
	//-- this secton removes lead_ids for those companies that don't want a certain type of lead
	
	//-- now want renters
	//-- 1/20/09 15:12 PM - added substrCount(lead_ids, '|') > 3 to this SQL so we only pull those leads that have not already been processed - this assumes there are more 
	//-- than 3 pipes ( | ) in the lead_ids field
	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and substrCount(lead_ids, '|') > 3 ";
	
	echo " SPECIAL VALIDATION SELECT: $sql<br />";
	$rs_getit = new mysql_recordset($sql_getit);

	while( $rs_getit->fetch_array() ) {
		 extract($rs_getit->myarray);
		 
		echo "LEAD INFO<br />";
		echo "$quote_id<br />";
		echo "$source<br />";
		echo "$quote_type<br />";
		echo "$own_rent<br />";
		echo "ORIG: $lead_ids<br />";
		echo "<br />";


		echo "<br />PROCESSING $quote_id<br />";
		
		/*
		If in 1569 zip list, then get rid of 1660 and 1520 - done
		
		if NOT in 1569 zip list
			get rid of 1569 and 1660
			give to 1520
		
		*/
		
		//-- check to see if Protect America wants the zip (they get all zips NOT in APX's list) ================================== 1520
		$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
		$rs = new mysql_recordset($sql);
		$valid = $rs->fetch_array();

		if ( $valid  && $own_rent != "rent"  ) { //-- Found in APX zip list, so Protect America doesn't want the zip (unless it's a renter)
			echo "Found Zip $zip in APX Zip List or is a RENTER, so Protect America Does Not Want it<br />";
			
			//-- 1569 OR 1660 GETS THIS LEAD - REMOVE 1520 
			$lead_ids = str_replace("1520","",$lead_ids);
			
			//-- GET LEADS PER DAY ALLOWED for 1660
			$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1660 and site_id = 'usalarm' ";
			$rs_pin = new mysql_recordset($sql_pin);
			$rs_pin->fetch_array();
			$leads_per_day = $rs_pin->myarray["leads_per_day"];
			
			//-- SEE IF ZIP IN THEIR ZIP LIST for 1660
			$sql_pinzip = "select zip From security_zip_codes where lead_id = '1660' and zip = '$zip' limit 1 ";
			$rs_pinzip = new mysql_recordset($sql_pinzip);
			$valid_pinzip = $rs_pinzip->fetch_array();
			
			//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660
			$today = date("Ymd");
			$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1660%' and ready_to_send = 3 ";
			$rs_pincount = new mysql_recordset($sql_pincount);
			$rs_pincount->fetch_array();
			$pincount = $rs_pincount->myarray["count"];

			echo "<br />leads_per_day = $leads_per_day<br />";
			echo "valid_pinzip = $valid_pinzip<br />";
			echo "pincount = $pincount<br />";

			if ( $valid_pinzip && ( $pincount <= $leads_per_day ) ) { //-- If zip in Pinnacle zip list and they haven't received all their leads for today, remove APX and PA (1520)
				$lead_ids = str_replace("1569","",$lead_ids);
				$lead_ids = str_replace("1520","",$lead_ids);
			} else { //-- otherwise, remove Pinnacle
				$lead_ids = str_replace("1660","",$lead_ids);
				$lead_ids = str_replace("1520","",$lead_ids);
			}

			
		} else {

			echo "Zip $zip Not Found in APX List or is a RENTER, so 1569 & 1660 DOES NOT GET THIS LEAD<br />";

			//-- remove 1569 & 1660 from tHE equation
			$lead_ids = str_replace("1569","",$lead_ids);
			$lead_ids = str_replace("1660","",$lead_ids);
		}
		
		
		
		
/*
		
		echo "LEAD IDS AFTER APX - PROTECT AMERICA SCRUB: $lead_ids<br />";
		echo "<br />";
		
		//-- NOW, WE NEED TO SEE WHO GETS THE LEAD BETWEEN WHO'S LEFT
		//-- it will either be 1569 & 1660 or 1520 & 1660
		
		
		 //-- SECOND SCRUB
		 //-- validate APEX 1569 ================================== 1569
		$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
		$rs = new mysql_recordset($sql);
		$valid = $rs->fetch_array();

		if ( !$valid || $own_rent == "rent" ) { //-- doesn't want the zip and is a renter
			echo "Zip $zip Not Found in APX Zip List (or is a RENTER)<br />";
			$lead_ids = str_replace("1569","",$lead_ids);
		} else {
			echo "Zip $zip FOUND in APX List (or is a RENTER)<br />";
			
			//-- If we get to this point, if we get here, then APX does want the lead, but now, we need to siphon off 30 of these per day to Pinnacle (1660)
			
			
		}
		
*/
		
		echo "<br />LEAD IDS AFTER  SCRUB: $lead_ids<br />";
		
		
		//-- THIS SECTION BELOW SCRUBS THE LEAD_IDS FIELD TO MAKE IT GOOD AGAIN
		
		//-- Finish scrubbing the lead_ids before reinserting them into the quote
		//-- remove double pipes ( || )
		$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
		
		//-- remove pipes as the first location
		if ( substr($lead_ids,0,1) == "|"  ) {
			$lead_ids = substr($lead_ids,1);
		} 
		
		//-- remove pipes as the last location
		if ( substr($lead_ids,-1,1) == "|"  ) {
			$lead_ids = substr($lead_ids,0,-1);
		} 
		
		$lead_ids = stripslashes($lead_ids);
		echo "NEW LEAD IDS: $lead_ids<br />";

		//-- save new lead_id list to record
		$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
		echo "SPECIAL VALIDATION UPDATE SQL: $sql_update <br /><br />";
		$rs_update = new mysql_recordset($sql_update);

	}		

	//-- END SPECIAL VALIDATION ======================


	

 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'usalarm'";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes() {
		
		//-- continue normal processing...
		
		//-- NO Company wants to get renters
#	 $sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- OKAY TO PULL RENTERS AGAIN
		$sql = "select quote_id,lead_ids from leads_security where campaign = 'usalarm' and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'usalarm'";
			 echo "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.
	
	$all_companies = array(	GAYLORD => new Gaylord(),
											BRINKS => new NewBrinks(),
											APX => new APXSecurity(), 
											PROTECT_AMERICA => new ProtectAmerica(),
											PINNACLE => new Pinnacle()
											);
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	echo "Active Companies:<pre>";
	print_r($active_lead_ids);
	echo "</pre><br />";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}


	
	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		echo "No Quotes.";
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	echo "QUOTE SELECT: $sql<br />";
	
	//loop over each one.
	while($rs->fetch_array())
	{			
		$lead_ids = $rs->myarray['lead_ids'];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
				
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);
		
		$myarray = formatData($rs->myarray);
		
		$myarray['company_count'] = $company_count;
		
		//loop through each company that should get this lead.
		foreach($lead_ids as $lead_id) {
			$company = $active_companies[$lead_id];
			echo "Now processing $company<br />";

			//assign it?
			if ($company->assign($myarray)) { //assign the array, and test them.
				$q_id = $myarray['quote_id'];
				echo "Looking to send $q_id<br />";

				$company->send();			
				echo $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
				
			} else {
				$q_id = $myarray['quote_id'];
				//mail("rob@irelocation.com","Brinks Lead Bounce","This email was generated on line 118 of irelocation.com/usalarm/quotemailer.php \n\n Lead ID: $lead_id doesn't want quote_id: $q_id ");
				echo "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
				echo "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				

//-- Update the quote_id and take out the lead_id that doesn't want the lead	
$sql_rem = " update leads_security set lead_ids = replace(lead_ids,'$lead_id','') where quote_id = ".$myarray['quote_id'] ;
echo "Running: $sql_rem<br />";
$rs_rem = new mysql_recordset($sql_rem);

//-- Remove the occaisional double pipes ( || ) that are created	
$sql_dp = " update leads_security set lead_ids = replace(lead_ids,'||','|') where quote_id = ".$myarray['quote_id'] ;
echo "Running: $sql_dp<br />";
$rs_dp = new mysql_recordset($sql_dp);

//-- Trim pipes ( | )
$sql_trim = " update leads_security set lead_ids = trim(BOTH '|' FROM lead_ids ) where quote_id = " .$myarray['quote_id'] ;
echo "Running: $sql_trim<br />";
$rs_trim = new mysql_recordset($sql_trim);


			}
		}	
		//mark lead when done.
		markAsSent($rs->myarray['quote_id']);
		
		//if we have too..
		if ($remove_from_brinks)
			removeFromBrinks($rs->myarray['quote_id'],implode("|",$lead_ids));
		
	}
?>

