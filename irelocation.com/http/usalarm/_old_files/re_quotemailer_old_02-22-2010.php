<?php
/* 
************************FILE INFORMATION**********************
* File Name:  re_quotemailer.php
**********************************************************************
* Description:  This code will take a quote_id and resend the lead to the lead_ids in the lead_ids column
**********************************************************************
* Creation Date:  12/9/09
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: the purpose of this script is to simply resend the lead to the lead_ids that were supposed to get it, but somehow didn't.  This script needed to be created as I can't re-run the quotemailer.php code, as it may send the lead to different companies, because of how the current quotemailer now processes leads.

Push the quote ids into the script with quote_ids=1,2,3,4 in the URL

*/


	ob_start();
	
 	define(DEBUG_EMAILS,true);
 	
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P

	//-- let's deliniate when this output starts, as the Class output doesn't not buffer.
	$body .= "<br /><hr /><hr /><hr /><hr /><hr /><br />";

	
	 	
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		echo "loadCompanies SQL: $sql";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.

	$all_companies = array(	BRINKS => new NewBrinks(),
											GAYLORD => new Gaylord(),
											APX => new APXSecurity(), 
											PINNACLE => new Pinnacle(), 
											FRONTPOINT => new FrontPoint(), 
											PROTECT_AMERICA => new ProtectAmerica(),
											PROTECTION_ONE => new ProtectionOne()
											);
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	
	echo "Active Companies:<pre>";
	echo print_r($active_lead_ids,true);
	echo "</pre><br />\n";
	$body .= "Active Companies:<pre>";
	$body .= print_r($active_lead_ids,true);
	$body .= "</pre><br />\n";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}


#	$quote_ids = explode(",", $quoteids); //-- now pushed in via URL
	
	if ( !$quote_ids )
	{
		$body .= "No Quotes.";
		echo $body;
		exit();
	}
	
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in ($quote_ids)"; //-- pulling in quote_ids directly from the URL
	$rs = new mysql_recordset($sql);
	$body .= "QUOTE SELECT: $sql<br />\n";
	
	//loop over each one.
	while($rs->fetch_array()) {			
				
		$lead_ids = $rs->myarray['lead_ids'];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
		
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);

		if ( $company_count == 0 ) { //-- This condition should never happen, but it is. Need to send me info when this happens and prevent the quotemailer from crashing.
			$quoteid = $rs->myarray['quote_id'];
			
			mail("code@irelocation.com","EMPTY LEAD_IDS VAR IN USALARM","Quote ID = $quoteid");
			$body .= "<br />There was an issue with Quote ID $quoteid - seems it had no lead_ids in the array.<br />";
			$body .= "LEAD_IDS ARRAY IS CURRENTLY:<br />";
			$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
			$body .= "<br />";
			
		} else { //-- continue to process
			
			$myarray = formatData($rs->myarray);
			
			$myarray['company_count'] = $company_count;
			
			//loop through each company that should get this lead.
			$body .= "LEAD_IDS in \$lead_ids:<br />";
			$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
			$body .= "<br />";

			
			foreach($lead_ids as $lead_id) {
				$company = $active_companies[$lead_id];
				$body .= "Now processing $company<br />\n";
	
				//assign it?
				if ($company->assign($myarray)) { //assign the array, and test them.
					$q_id = $myarray['quote_id'];
					$body .= "RESENDING $q_id<br />\n";
	
					$company->send();			
					$body .= $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
					
				} else {
					echo "Unknown Error.  Seems we don't have a company to send to.";
	
	
				}
			}	

		}			
		
	}
	
echo $body;
$body = str_replace('<br />','',$body);

$content = ob_get_contents();
ob_end_clean();
echo $content;

$body = str_replace('<br />','\\n',$body);

if(DEBUG_EMAILS)
	mail("code@irelocation.com","USALARM RE_QUOTEMAILER OUTPUT",$content);

?>

