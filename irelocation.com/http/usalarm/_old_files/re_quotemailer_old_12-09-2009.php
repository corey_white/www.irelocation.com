<?php
/*
 * Created on Jan 11, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 
 9/5/08 - Rob - recoding so that this can be used to resend leads using just a link (like the others)
 */
 
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P
 
 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'usalarm'";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes($quote_ids) {
		 $sql = "select quote_id,lead_ids from leads_security where  quote_id in ($quote_ids)";		 	
		 echo "Pulling Leads: $sql<br />";
	 	 $rs = new mysql_recordset($sql);
	 	 $quote_ids = array();
	 	 
	 	 while($rs->fetch_array())
	 	 	$quote_ids[] = $rs->myarray['quote_id'];
	 	 
	 	 if (count($quote_ids) > 0)
	 	 {
		 	 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'usalarm'";
		 	 echo "Update SQL: ".$sql."<br/>";
		 	 $rs = new mysql_recordset($sql);
			 	 
	 		 return $quote_ids;
	 	 }
	 	 else return array();
	}
	
	function loadCompanies($lead_id) {
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm' and month = '$month' and active and (cancelled is null or cancelled > NOW()) and lead_id = $lead_id";
		echo "loadCompanies: $sql<br />";
		
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array()) {
		
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	

			echo " <br /><br />LEAD IDS FROM loadCompanies:<br /><pre>";
			print_r($lead_ids);
			echo "</pre><br />";

	 	}
	 	return $lead_ids;
	}
	
	//-- ==================================== START PROCESSING ==================================
	
	//load company classes based on the URL input
	switch ( $_REQUEST['lead_id'] ) {
	
		case "1519": //-- BRINKS
			$all_companies = array(	BRINKS => new Brinks() );
			$sole_lead_id = 1519;
			echo "Company is Brinks (1519)<br />";
			break;
	
	
		case "1552": //-- ADT
			$all_companies = array(	GAYLORD => new Gaylord() );
			$sole_lead_id = 1552;
			echo "Company is GAYLORD/ADT (1552)<br />";
			break;
	
	
		default: //-- lead_id not found, report and exit
			echo "Lead ID not found - check code<br />";
			exit;
			break;
	}
	
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies($sole_lead_id);
	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id) {		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}

	echo "<br /><br />ACTIVE COMPANIES:<pre>";
	print_r($active_companies);
	echo "</pre><br />";

	if ($_REQUEST['quote_id'] == "") {
		echo "No Quote IDs<br />";
		exit();
	} else {
		echo "Processing the following quote_id(s):<br />" . $_REQUEST['quote_id'] . "<br /><br />";
	}
	
	$quote_ids = selectQuotes($_REQUEST['quote_id']);
	
	if (count($quote_ids) == 0) {
		echo "Some or All of these Quotes were not found in the DB.";
		exit();
	}
	
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	echo "Select Quotes: $sql";
	$rs = new mysql_recordset($sql);
	
	
	//loop over each one.
	while($rs->fetch_array()) {			
		$lead_ids = $active_companies[$lead_id];
		$lead_ids = trim($lead_ids,"|");
		$lead_ids = split("\|",$lead_ids);
		
		echo "<br /><br />LEAD IDS REDUX:<pre>";
		print_r($lead_ids);
		echo "</pre><br />";

				
		//get the count after cleaning brinks.
		//count lead_ids
		$company_count = count($lead_ids);
		
		echo "company_count = $company_count<br />";
		
		$myarray = formatData($rs->myarray);
		
		$myarray['company_count'] = $company_count;

		//-- format for an email copy
#		extract($myarray);
		$email_msg = "
		quote_id = $quote_id 
		received = $received 
		source = $source 
		referrer = $referrer 
		quote_type = $quote_type 
		name1 = $name1 
		name2 = $name2 
		phone1 = $phone1 
		email = $email 
		address = $address 
		state_code = $state_code 
		zip = $zip 
		current_needs = $current_needs 
		building_type = $building_type 
		num_location = $num_location 
		num_location = $num_location 
		sqr_footage = $sqr_footage 
		own_rent = $own_rent 
		fire = $fire 
		basement = $basement 
		access = $access 
		cctv = $cctv 
		other_field = $other_field 
		(original) lead_ids = $lead_ids 
		comments = $comments 
		ready_to_send = $ready_to_send 
		in_check_queue = $in_check_queue 
		scrub_id = $scrub_id 
		campaign = $campaign 
		remote_ip = $remote_ip 
		company_count = $company_count 
		";
		
		$email_msg_display = nl2br($email_msg);
#		echo "MSG FOR EMAIL: <br />$email_msg_display<br />";	
	
	
		
		$lead_data = print_r($myarray,true);
		
/*
		echo "<br /><br />LEAD DATA:<pre>";
		echo "$lead_data";
		echo "</pre><br />";
*/
		
		echo "<br /><br />LEAD_IDS:<pre>";
		print_r($lead_ids);
		echo "</pre><br />";

		
		//loop through each company that should get this lead.
		foreach($lead_ids as $lead_id) {
			$company = $active_companies[$lead_id];
			echo "company = $company<br />";
			
			
			//assign it?
			if ($company->assign($myarray)) {  //assign the array, and test them.									
				$company->send();			
				echo $myarray['quote_id']." sent to ".$lead_id."<br/>\n";

			} else {

				echo "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
				echo "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
			}
/*
*/

		}	
		//mark lead when done.
#		markAsSent($rs->myarray['quote_id']);
		
	}

?>

