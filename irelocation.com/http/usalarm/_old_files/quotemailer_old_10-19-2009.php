<?php
/*
 * Created on Jan 11, 2008
 *
 
 9/10/08 - Rob - Adding functionality that "pre-washes" the quotes so that Brinks doesn't get RES Renters and APEX doesn't get RES Renters and no CLICKBOOTH leads.
 
 9/12/08 - Rob - Okay, we're now changing the above as NO company wants renters.  Changing the initial SQL to only pull own_rent = own.  Also adding a function to delete the lead_ids from any renter lead.
 
 10-9-2008 - 10-10-2008 - Rob - Adding ability for a client to get renter leads
 
 5/4/09 - Rob - On 5-1, added massive amounts fo code for the way we're not giving out leads.  Lots of logic for determining who gets the lead.  Today, still working on fine-tuning that code.
 
 7-14-2009 - updating quotemailer so it will also run the 'alarmsys' quotes from AlarmSystemsNow.com, which are identicle to USAlarm leads.
 
 7-21-2009 - Added code to delete test leads.  This existed in the Top Security quotemailer, but since this quotemailer runs twice as often as TSC, some clients were getting test leads sent to them.
 */
	ob_start();
	
 	define(DEBUG_EMAILS,true);
 	
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P

	//-- let's deliniate when this output starts, as the Class output doesn't not buffer.
	$body .= "<br /><hr /><hr /><hr /><hr /><hr /><br />";
	
	//-- Delete anything that smells like a test lead
	$sql = "delete from irelocation.leads_security where name1 like 'Test%' or (email = 'test@test.com' || email = 'testing@gmail.com') or name2 = 'Test' ;";
	$rs = new mysql_recordset($sql);
	

	//-- update the received column for those leads that came in from 
	$mytime = date("YmdHis");
	$sql_time = "update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and (campaign = 'usalarm' || campaign = 'alarmsys') ";
	$body .= "UPDATING RECEIVED TIME: $sql_time<br/>";
	$rs_time=new mysql_recordset($sql_time);

 
	//-- OVERRIDE LEADS_IDS for OWNERS - rewrite those lead_ids that are eligible to get OWN leads 8/17/09
	//-- REMOVED 1520 on 5/18/09 15:27 PM
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1660|1690' where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'own' and ready_to_send = 1  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- Check for leads that have a lead_ids of || and insert the lead_ids
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1660|1690' where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids = '||'  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- update and RTS = 2 so that they will run again.
	$sql = "update irelocation.leads_security set ready_to_send = 1 where ready_to_send = 2 and (campaign = 'usalarm' || campaign = 'alarmsys')  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	

	//-- OVERRIDE LEADS_IDS for RENTERS - rewrite those lead_ids that are eligible to get RENT leads 8/17/09
	//-- 8/17/09 11:18 AM - Added 1690
	//-- 9/17/09 11:34 AM - removed 1690
	$sql = "update irelocation.leads_security set lead_ids = '1520' where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'rent' and ready_to_send = 1  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	
	
	//-- Special Validation
		
#	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'own' and lead_ids is not null and ready_to_send = 1 and substrCount(lead_ids, '|') > 3 "; //-- OLD SQL
	
	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1  ";
	
	$body .= " SPECIAL VALIDATION SELECT: $sql_getit<br />\n";
	$rs_getit = new mysql_recordset($sql_getit);

	while( $rs_getit->fetch_array() ) {
		 extract($rs_getit->myarray);
		 
		$body .= "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br />\n";
		$body .= "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br />\n";
		$body .= "LEAD INFO<br />\n";
		$body .= "$quote_id<br />\n";
		$body .= "$source<br />\n";
		$body .= "$quote_type<br />\n";
		$body .= "$own_rent<br />\n";
		$body .= "ORIG: $lead_ids<br />\n";
		$body .= "<br />\n";


		if ( $own_rent == 'rent' ) { //-- Renters
			//-- SPECIAL VALIDATION FOR RENTERS ============================================ (new section created 8/17/09)
			//-- 9/17/09 11:35 AM - bypassed all of the 1690 code, as they don't want renters for now (though we may reinstitute later)
			//-- this section determines whether the renter lead goes to Protect America (1520) or FrontPoint (1690)
			
			//-- 1690 only wants to get 200 leads per month, so we need to check to see how many renter leads they've received so far this month, and if under 200, run a lottery between 1690 and 1520.
			
			echo "<h1>RENTERS</h1>";
			echo "<br /><br />RUNNING SPECIAL VALIDATION FOR RENTERS<br />";
			echo "Now running only 1520 for renters<br /><br />";
			
/*
			//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1690
			$this_month = date("Ym");
			$sql_fpcount = "select count(quote_id) as count from irelocation.leads_security where left(received,6) = '$this_month' and own_rent = 'rent' and lead_ids like '%1690%' and ready_to_send = 3 ";
			echo "sql_fpcount: $sql_fpcount<br />";
			$rs_fpcount = new mysql_recordset($sql_fpcount);
			$rs_fpcount->fetch_array();
			$fpcount = $rs_fpcount->myarray["count"];
			
			echo "FrontPoint has received $fpcount (fpcount) renter leads so far this month.<br />";
			
			if ( $fpcount < 200 ) { //-- run lottery
				
				$randomNumber = rand(0,1000);
				echo "Random Number Generated was $randomNumber (\$randomNumber)<br />";
				if ( $randomNumber <= 500 ) { //-- 50% chance that Frontpoint gets this lead.
					echo "Frontpoint won the lottery! Removing Protect America <br />";
					$lead_ids = str_replace("1520","",$lead_ids);
					$body .= "removed Protect America (1520) <br />\n";
				} else {
					echo "Frontpoint LOST the lottery<br />";
					$lead_ids = str_replace("1690","",$lead_ids);
					echo "Removing Frontpoint (1690).<br />";
				}
		
			} else { //-- send to 1520
				
				echo "Frontpoint over their limit for the month:<br />";
				$lead_ids = str_replace("1690","",$lead_ids);
				echo "Removing Frontpoint (1690).<br />";
		
			}

			//-- save new lead_id list to record
			$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
			$body .= "SPECIAL VALIDATION UPDATE SQL: $sql_update <br />\n<br />\n";
			$rs_update = new mysql_recordset($sql_update);

			//-- THIS SECTION BELOW SCRUBS THE LEAD_IDS FIELD TO MAKE IT GOOD AGAIN
			
			//-- Finish scrubbing the lead_ids before reinserting them into the quote
			//-- remove quad pipes ( |||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Rent 1: $lead_ids<br />\n";
			
			//-- remove triple pipes ( ||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Rent 2: $lead_ids<br />\n";
			
			//-- remove double pipes ( || )
			$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Rent 3: $lead_ids<br />\n";
			
			$lead_ids = stripslashes($lead_ids);
			$body .= "LEAD IDS after Rent stripslashes: $lead_ids<br />\n";
	
			//-- remove pipes as the first location
			if ( substr($lead_ids,0,1) == "|"  ) {
				$lead_ids = substr($lead_ids,1);
			} 
			$body .= "Pipe Scrub Rent 4: $lead_ids<br />\n";
			
			//-- remove pipes as the last location
			if ( substr($lead_ids,-1,1) == "|"  ) {
				$lead_ids = substr($lead_ids,0,-1);
			} 
			$body .= "Pipe Scrub Rent 5: $lead_ids<br />\n";
	
			$body .= "LEAD IDS AFTER RENT PIPE SCRUB: $lead_ids<br />\n";
*/
			
			
		} else { //-- OWNERS
			
			//-- SPECIAL VALIDATION FOR OWNERS ============================================
			//-- this section removes lead_ids for those companies that don't want a certain type of lead for OWNer leads

			echo "<h1>OWNERS</h1>";
			echo "<br /><br />RUNNING SPECIAL VALIDATION FOR OWNERS<br /><br />";
			

			$body .= "<br />\nPROCESSING $quote_id<br />\n";
			
			$body .= "<br />\nNOW RUNNING APX CRITERIA";
			$body .= "<br />\nNOW RUNNING APX CRITERIA<br />\n<br />\n";
	
			//-- check to see if APX wants the zip 
			$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();
	
			if ( $valid  && $own_rent != "rent"  ) { //-- Found in APX zip list, so Protect America doesn't want the zip (unless it's a renter)
				$body .= "Found Zip $zip in APX Zip List and is a OWNer, so Protect America Does Not Want it<br />\n";
				
				$current_month = date("Ym");
				$body .= "Current Month set to $current_month<br />\n";
				
				//-- 1569 OR 1660 GETS THIS LEAD - REMOVE 1520 
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "1569 OR 1660 GETS THIS LEAD - REMOVED 1520 (1520 doesn't get OWNer leads normally, only on an overflow situation when we don't have anyone else to give them too)<br />\n";
				
				//-- GET LEADS PER DAY ALLOWED for 1660
				$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1660 and site_id = 'usalarm' and month = '$current_month' ";
				$rs_pin = new mysql_recordset($sql_pin);
				$rs_pin->fetch_array();
				$leads_per_day = $rs_pin->myarray["leads_per_day"];
				
				//-- SEE IF ZIP IN THEIR ZIP LIST for 1660
				$sql_pinzip = "select zip From security_zip_codes where lead_id = '1660' and zip = '$zip' limit 1 ";
				$rs_pinzip = new mysql_recordset($sql_pinzip);
				$valid_pinzip = $rs_pinzip->fetch_array();
				$body .= "sql_pinzip = $sql_pinzip<br />\n";
				
				//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660
				$today = date("Ymd");
				$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1660%' and ready_to_send = 3 ";
				$rs_pincount = new mysql_recordset($sql_pincount);
				$rs_pincount->fetch_array();
				$pincount = $rs_pincount->myarray["count"];
				
				if ( $valid_pinzip ) {
					$f_zip = "yes";
				} else {
					$f_zip = "no";
				}
	
				$body .= "<br />\nleads_per_day for 1660 = $leads_per_day<br />\n";
				$body .= "Looking for zip in 1660 = $f_zip ($valid_pinzip)<br />\n";
				$body .= "CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660 = $pincount<br />\n";
	
				if ( $valid_pinzip && ( $pincount <= $leads_per_day ) ) { //-- If zip in Pinnacle zip list and they haven't received all their leads for today, remove APX 
					
					$randomNumber = rand(0,1000);
					if ( $randomNumber <= 300 ) { //-- 30% chance that Pinnacle can get this APX lead.
						echo "Pinnacle won the lottery!<br />";
						$lead_ids = str_replace("1569","",$lead_ids);
						$body .= "zip in Pinnacle zip list and they haven't received all their leads for today, removed APX (1569) <br />\n";
						$save_pinnacle = "yes";
					} else {
						echo "Pinnacle LOST the lottery<br />";
						echo "Removing no one.<br />";
					}
				} 
				
				//-- if zip is not in Pinnacle's zip list, we need to remove them
				if ( !$valid_pinzip ) { 
						echo "Removing Pinnacle becasue the zip is not in their list<br />";
						$lead_ids = str_replace("1660","",$lead_ids);
						$body .= "zip not in Pinnacle zip list, removed Pinnacle (1660) <br />\n";
						$save_pinnacle = "no";
					
				}
	
	
				
			} else {
	
				$body .= "Zip $zip NOT Found in APX List or is a RENTER, so 1569 DOES NOT GET THIS LEAD<br />\n";
	
				//-- remove 1569  from the equation
				$lead_ids = str_replace("1569","",$lead_ids);
				$body .= "Removing APX (1569) <br />\n";
	
				//-- We don't want Protect America (1520) to get any leads that aren't renters, so removing 1520
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "We don't want Protect America (1520) to get any leads that aren't renters, so removing 1520 (1520 doesn't get OWNer leads normally, only on an overflow situation when we don't have anyone else to give them too) - REMOVED 1520<br />\n";
				
			}
	
			$body .= "<br />\nLEAD IDS AFTER APX SCRUB: $lead_ids<br />\n";
			
			
			
			$body .= "<br />\nNOW RUNNING BRINKS CRITERIA";
			$body .= "<br />\nNOW RUNNING BRINKS CRITERIA<br />\n<br />\n";
	
			//-- Now, we need to do it again for Brinks (1519), FrontPoint (1690) and Pinnacle (1660).
	
			//-- check to see if Brinks wants the zip ==================================  1519, 1660, 1690
			$sql = "select zip From security_zip_codes where lead_id = '1519' and zip = '$zip' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();
	
			if ( $valid  && $own_rent != "rent"  ) { //-- Found in Brinks zip list, so Pinnacle and FrontPoint can't get the zip (unless it's a renter)
				$body .= "Found Zip $zip in Brinks Zip List and is an OWNer, so Pinnacle and FrontPoint can't get it<br />\n";
				
				//-- remove 1569 & 1660 from the equation
				if ( $save_pinnacle == "yes" ) { //-- don't remove Pinnacle (1660) if they were to get it from the APX scrub above
					$body .= "Performed a 'save' on Pinnacle, as they were supposed to get this from the APX scrub (IOW, not removing Pinnacle)<br />\n";
				} else {
					$lead_ids = str_replace("1660","",$lead_ids);
					$body .= "Removing Pinnacle (1660)<br />\n";
				}
				$lead_ids = str_replace("1690","",$lead_ids);
				$body .= "Removing FrontPoint (1690)<br />\n";
	
				
			} else {
	
				$body .= "Zip $zip Not Found in Brinks List or is a RENTER, so 1660 and/or 1690 CAN GET THIS LEAD<br />\n";
				$lead_ids = str_replace("1519","",$lead_ids);	
				$body .= "Removing Brinks (1519)<br />\n";
	
					$current_month = date("Ym");
					$body .= "Current Month set to $current_month<br />\n";
					
					//-- GET LEADS PER DAY ALLOWED for 1660
					$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1660 and site_id = 'usalarm' and month = '$current_month' ";
					$rs_pin = new mysql_recordset($sql_pin);
					$rs_pin->fetch_array();
					$leads_per_day1660 = $rs_pin->myarray["leads_per_day"];
					$body .= "sql_pin = $sql_pin<br />\n";
		
					//-- GET LEADS PER DAY ALLOWED for 1690
					$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1690 and site_id = 'usalarm' and month = '$current_month' ";
					$rs_pin = new mysql_recordset($sql_pin);
					$rs_pin->fetch_array();
					$leads_per_day1690 = $rs_pin->myarray["leads_per_day"];
					$body .= "sql_pin = $sql_pin<br />\n";
					
					$body .= "<br />\nleads_per_day1660 = $leads_per_day1660<br />\n";
					$body .= "leads_per_day1690 = $leads_per_day1690<br />\n<br />\n";
					
					//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1660
					$today = date("Ymd");
					$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and lead_ids like '%1660%' and ready_to_send = 3 ";
					$rs_pincount = new mysql_recordset($sql_pincount);
					$rs_pincount->fetch_array();
					$pincount1660 = $rs_pincount->myarray["count"];
					$body .= "sql_pincount = $sql_pincount<br />\n";
		
					//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1690
					$today = date("Ymd");
					$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and own_rent = 'own' and lead_ids like '%1690%' and ready_to_send = 3 ";
					$rs_pincount = new mysql_recordset($sql_pincount);
					$rs_pincount->fetch_array();
					$pincount1690 = $rs_pincount->myarray["count"];
					$body .= "sql_pincount = $sql_pincount<br />\n";
		
					$body .= "<br />\npincount1660 = $pincount1660<br />\n";
					$body .= "pincount1690 = $pincount1690<br />\n<br />\n";
					
					//-- SEE IF ZIP IN THEIR ZIP LIST for 1660
					$sql_pinzip = "select zip From security_zip_codes where lead_id = '1660' and zip = '$zip' limit 1 ";
					$rs_pinzip = new mysql_recordset($sql_pinzip);
					$valid_pinzip = $rs_pinzip->fetch_array();
					$body .= "sql_pinzip 1660 = $sql_pinzip<br />\n";
		
					if ( $valid_pinzip ) { //-- is it in Pinnacle's Zip List?
						$pinnacle_zip = "yes";
					} else {
						$pinnacle_zip = "no";
					}
					$body .= "Is Zip in Pinnacle's Zip List: $pinnacle_zip<br />\n";
		
					if (  $leads_per_day1660 <= $pincount1660 || $pinnacle_zip == "no" ) {
						$okToGet1660 = "no";
					} else {
						$okToGet1660 = "yes";
					}
					
					$body .= "Pinnacle: Is $leads_per_day1660 <= $pincount1660<br />\n";
					$body .= "Pinnacle (1660) still needs leads for today AND this lead is in their zip list: $okToGet1660<br />\n";
		
					if ( $leads_per_day1690 <= $pincount1690 ) {
						$okToGet1690 = "no";
					} else {
						$okToGet1690 = "yes";
					}
		
					$body .= "FrontPoint: Is $leads_per_day1690 <= $pincount1690<br />\n";
					$body .= "FrontPoint (1690) still needs leads for today: $okToGet1690<br />\n";
		
		
		
				if ( $save_pinnacle == "yes" ) { //-- IF Pinnacle was given the lead up above, then we automatically remove Frontpoint
					
					echo "save_pinnacle = $save_pinnacle<br />";
	
					if ( $okToGet1690 == "yes" ) {//-- FrontPoint can get it
						$body .= "Pinnacle got lead from APX, so it's okay for FrontPoint (1690) to get this lead, not removing anyone else<br />\n";
					} else { //-- oops, looks like FrontPoint is at its max for the day
						$lead_ids = str_replace("1690","",$lead_ids);
						$body .= "Pinnacle got lead from APX, but FrontPoint (1690) is maxed for the day, so removing them.<br />\n";
					}
	
				} else { //-- otherwise, they have to battle it out
					
		
					if ( $okToGet1660 == "yes" ) { //-- both are eligible, but give to Pinnacle as their zips don't come up often enough
		/* if the zip is in Pinnacle's zip list, we need to give it to them (if they're not over their quota for the day), so bypassing this random selection
						$body .= "RANDOMLY choosing one.<br />\n";
						$rand = rand(0,100);
						$body .= "RANDOM NUMBER IS $rand<br />\n";
						if ($rand < 20) {
							$lead_ids = str_replace("1660","",$lead_ids);	
							$body .= "Removing Pinnacle (1660)<br />\n";
						} else {
							$lead_ids = str_replace("1690","",$lead_ids);
							$body .= "Removing FrontPoint (1690)<br />\n";
						}
		*/
						$lead_ids = str_replace("1690","",$lead_ids);
						$body .= "Removing FrontPoint (1690)<br />\n";
		
		
		/*
					} elseif ( $okToGet1660 == "yes" && $okToGet1690 == "no" ) { //-- remove Frontpoint
						$body .= "Removing FrontPoint (1690)<br />\n";
						$lead_ids = str_replace("1690","",$lead_ids);
		*/
						
					} elseif ( $okToGet1690 == "yes" ) { //-- give to FrontPoint
						$body .= "Removing Pinnacle (1660)<br />\n";
						$lead_ids = str_replace("1660","",$lead_ids);//-- remove Pinnacle
						
					} else { //-- otherwise, remove both
						$body .= "REMOVING BOTH!! - Looks like both have already received their quota for the day!<br />\n";
						$lead_ids = str_replace("1660","",$lead_ids);
						$lead_ids = str_replace("1690","",$lead_ids);
						if(DEBUG_EMAILS)
							mail("rob@irelocation.com","MATRIX SHORT ON LEADS","You're getting this email because both Pinnacle and FrontPoint have reached their daily quota and there's no one to give the lead to!!!");
					}
				}
	
	
			}
			
	
			
			//-- THIS SECTION BELOW SCRUBS THE LEAD_IDS FIELD TO MAKE IT GOOD AGAIN
			
			//-- Finish scrubbing the lead_ids before reinserting them into the quote
			//-- remove quad pipes ( |||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 1: $lead_ids<br />\n";
			
			//-- remove triple pipes ( ||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 2: $lead_ids<br />\n";
			
			//-- remove double pipes ( || )
			$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 3: $lead_ids<br />\n";
			
			$lead_ids = stripslashes($lead_ids);
			$body .= "LEAD IDS after Own stripslashes: $lead_ids<br />\n";
	
			//-- remove pipes as the first location
			if ( substr($lead_ids,0,1) == "|"  ) {
				$lead_ids = substr($lead_ids,1);
			} 
			$body .= "Pipe Scrub Own 4: $lead_ids<br />\n";
			
			//-- remove pipes as the last location
			if ( substr($lead_ids,-1,1) == "|"  ) {
				$lead_ids = substr($lead_ids,0,-1);
			} 
			$body .= "Pipe Scrub Own 5: $lead_ids<br />\n";
	
			$body .= "LEAD IDS AFTER OWN PIPE SCRUB: $lead_ids<br />\n";
			
			
			//-- WE NEED TO CHECK IF THERE ARE TOO MANY LEAD_IDS
			$test1_arr = split("\|",$lead_ids);
			$test1_count = count($test1_arr);
			$body .= "Testing for too many leads";
			$body .= "test1_count = $test1_count<br />\n";
	
			if ( $test1_count > 3 ) { //-- there are too many, let's shuffle them and pick the first 3 
				$test1_arr = shuffle($test1_arr);
				$lead_ids = $test1_arr[0] . "|" . $test1_arr[1] . "|" . $test1_arr[2];
				$body .= "NEW LEAD ID ORDER IS $lead_ids<br />\n";
			} else {
				$body .= "Checked to see if there were too many leads and found none.<br />\n";
			}
			
			$body .= "<br />\nLEAD IDS AFTER CHECKING FOR TOO MANY LEADS: $lead_ids<br />\n";
			
			//-- NOW WE NEED TO CHECK IF THERE ARE TOO FEW LEAD_IDS
	
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			//-- if there are less than 3 lead_ids, then add some back into the mix, as we haven't sold all of the leads that they don't want yet.
			$brinks_arr = split("\|",$lead_ids);
			$brinks_test = count($brinks_arr);
			$body .= "Testing for too few leads.";
			$body .= "brinks_test = $brinks_test<br />\n";

			//-- SEE IF ZIP IN ZIP LIST for PROTECTION ONE 1521
			$sql_1521zip = "select zip From security_zip_codes where lead_id = '1521' and zip = '$zip' limit 1 ";
			$rs_1521zip = new mysql_recordset($sql_1521zip);
			$valid_1521zip = $rs_1521zip->fetch_array();
			$body .= "sql_1521zip SQL = $sql_1521zip<br />\n";
			$body .= "Found Zip for 1521: $valid_1521zip<br />\n";

			
			if ( $brinks_test < 2 ) { //-- if we only have 1 lead_id left, then 1660 and 1690 have gotten their daily quota and we can give it again to someone else
	
				//-- if 1690 is already in the list, add brinks, otherwise, add FrontPoint
				if ( ereg("1690",$lead_ids) ) { 
					if ( $valid_1521zip ) { //-- if Protection one can get this zip
						$lead_ids = $lead_ids . "|1520|1521";
						$body .= "*** FOUND ONLY ONE LEAD_ID - ADDING PROTECT AMERICA and PROTECTION ONE ***<br />\n";
					} else {
						$lead_ids = $lead_ids . "|1519|1520";
						$body .= "*** FOUND ONLY ONE LEAD_ID - ADDING BRINKS AND PROTECT AMERICA ***<br />\n";
					}
					
					
				} else {
					if ( $valid_1521zip ) { //-- if Protection one can get this zip
						$lead_ids = $lead_ids . "|1690|1521";
						$body .= "*** FOUND ONLY ONE LEAD_ID - ADDING FRONTPOINT AND PROTECTION ONE ***<br />\n";
					} else {
						$lead_ids = $lead_ids . "|1690|1520";
						$body .= "*** FOUND ONLY ONE LEAD_ID - ADDING FRONTPOINT AND PROTECT AMERICA ***<br />\n";
					}
					
				}
				
			} elseif ( $brinks_test < 3 ) { 
	
				//-- if 1690 is already in the list, add brinks, otherwise, add FrontPoint
				if ( ereg("1690",$lead_ids) ) { 
					if ( $valid_1521zip ) { //-- if Protection one can get this zip
						$lead_ids = $lead_ids . "|1521";
						$body .= "*** FOUND ONLY 2 LEAD_IDS - ADDING PROTECTION ONE ***<br />\n";
					} else {
						$lead_ids = $lead_ids . "|1520";
						$body .= "*** FOUND ONLY 2 LEAD_IDS - RE-ADDING PROTECT AMERICA ***<br />\n";
					}

				} else {
					$lead_ids = $lead_ids . "|1690";
					$body .= "*** FOUND ONLY 2 LEAD_IDS - ADDING FRONTPOINT ***<br />\n";
				}
	
				
			} else {
				$body .= "There are more than 2 lead_ids, Not Re-adding Any Lead_IDs<br />\n";
			}
	
			$body .= "<br />\nLEAD IDS AFTER CHECKING FOR TOO FEW LEADS: $lead_ids<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
	
	
			//-- save new lead_id list to record
			$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
			$body .= "SPECIAL VALIDATION UPDATE SQL: $sql_update <br />\n<br />\n";
			$rs_update = new mysql_recordset($sql_update);
	
	
		}		
	
		//-- END SPECIAL OWNER VALIDATION ======================
	
		$body .= "<br />\nLEAD IDS AFTER ALL SCRUBS AND SPECIAL VALIDATIONS: $lead_ids<br />\n";
	}
	

	

 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and (campaign = 'usalarm' || campaign = 'alarmsys') ";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes() {
		
		//-- continue normal processing...
		
		//-- NO Company wants to get renters
#	 $sql = "select quote_id,lead_ids from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- OKAY TO PULL RENTERS AGAIN
		$sql = "select quote_id,lead_ids from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and (campaign = 'usalarm' || campaign = 'alarmsys') ";
			 $body .= "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		echo "loadCompanies SQL: $sql";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.

	$all_companies = array(	BRINKS => new NewBrinks(),
											GAYLORD => new Gaylord(),
											APX => new APXSecurity(), 
											PINNACLE => new Pinnacle(), 
											FRONTPOINT => new FrontPoint(), 
											PROTECT_AMERICA => new ProtectAmerica(),
											PROTECTION_ONE => new ProtectionOne()
											);
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	echo "Active Companies:<pre>";
	echo print_r($active_lead_ids,true);
	echo "</pre><br />\n";
	$body .= "Active Companies:<pre>";
	$body .= print_r($active_lead_ids,true);
	$body .= "</pre><br />\n";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}


	
	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		$body .= "No Quotes.";
		echo $body;
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	$body .= "QUOTE SELECT: $sql<br />\n";
	
	//loop over each one.
	while($rs->fetch_array()) {			
	
		//-- Need to check for dups:
		//-- get yesterday's date, then check the Db to see if there is any leads that have the same PHONE and EMAIL with a READY_TO_SEND = 3 within the last 2 days.
		$twoDaysAgo = date("Ymd",strtotime("2 days ago"));
		$phone1 = $rs->myarray["phone1"];
		$zip = $rs->myarray["zip"];
		$source = $rs->myarray["source"];
		
		$dupcheck = "select count(*) as dupcount from leads_security where left(received,8) >= '$twoDaysAgo' and phone1 = '$phone1' and zip = '$zip' and source = '$source' and ready_to_send = 3  ";
		$rs_dup = new mysql_recordset($dupcheck);
		$rs_dup->fetch_array();
		#mail("rob@irelocation.com","DUP CHECK SQL","$dupcheck\n\nQuote ID should be " . $rs->myarray['quote_id']);
		
		if ( $rs_dup->myarray["dupcount"] > 0 ) { //-- uh oh, it's a dup, do not send and update it to RTS = 8
			
			$body .= "FOUND DUP!!!<br />";
			$quoteid = $rs->myarray['quote_id'];
			$rts8 = "update leads_security set ready_to_send = 8 where quote_id = '$quoteid'   ";
			$body .= "RTS DUP SQL: $rts8<br />";
			$rs_rts8 = new mysql_recordset($rts8);
			mail("rob@irelocation.com","FOUND USAlarm Dup - RTS SQL","Updated RTS=8 \n\nQuote ID should be $quoteid");
			
		} else { //-- found no dup, okay to process
			
			$lead_ids = $rs->myarray['lead_ids'];
			$lead_ids = trim($lead_ids,"|");
			$lead_ids = split("\|",$lead_ids);
			
			//-- Send leads to Anna for validation if a specific source id
			if ( eregi("yakov",$rs->myarray['source']) || eregi("hsas",$rs->myarray['source']) || eregi("coregmedia",$rs->myarray['source']) || eregi("abcleads",$rs->myarray['source'])  ) {
				
				$anna_csv = $rs->myarray['quote_id'] . ", " . $rs->myarray['received'] . ", " . $rs->myarray['source'] . ", " . $rs->myarray['name1'] . ", " . $rs->myarray['name2'] . ", " . $rs->myarray['phone1'] . ", " . $rs->myarray['email'] . ", " . $rs->myarray['own_rent'] . ", " . $rs->myarray['campaign'];
				
				$anna_msg = $rs->myarray['quote_id'] . "
" . $rs->myarray['received'] . "
" . $rs->myarray['source'] . "
" . $rs->myarray['name1'] . "
" . $rs->myarray['name2'] . "
" . $rs->myarray['phone1'] . "
" . $rs->myarray['email'] . "
" . $rs->myarray['own_rent'] . "
" . $rs->myarray['campaign'] . "
" . $rs->myarray['address'] . "
" . $rs->myarray['city'] . "
" . $rs->myarray['state_code'] . "
" . $rs->myarray['zip'];
				
				mail("rob@irelocation.com,anna@irelocation.com","Affiliate Lead Needing Validation", $anna_msg . "\n\n" . $anna_csv);
			} 
					
			//get the count after cleaning brinks.
			//count lead_ids
			$company_count = count($lead_ids);

			if ( $company_count == 0 ) { //-- This condition should never happen, but it is. Need to send me info when this happens and prevent the quotemailer from crashing.
				$quoteid = $rs->myarray['quote_id'];
				
				mail("rob@irelocation.com","EMPTY LEAD_IDS VAR IN USALARM","Quote ID = $quoteid");
				$body .= "<br />There was an issue with Quote ID $quoteid - seems it had no lead_ids in the array.<br />";
				$body .= "LEAD_IDS ARRAY IS CURRENTLY:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
				
			} else { //-- continue to process
				
				$myarray = formatData($rs->myarray);
				
				$myarray['company_count'] = $company_count;
				
				//loop through each company that should get this lead.
				$body .= "LEAD_IDS in \$lead_ids:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
	
				
				foreach($lead_ids as $lead_id) {
					$company = $active_companies[$lead_id];
					$body .= "Now processing $company<br />\n";
		
					//assign it?
					if ($company->assign($myarray)) { //assign the array, and test them.
						$q_id = $myarray['quote_id'];
						$body .= "Looking to send $q_id<br />\n";
		
						$company->send();			
						$body .= $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
						
					} else {
						$q_id = $myarray['quote_id'];
						//mail("rob@irelocation.com","Brinks Lead Bounce","This email was generated on line 118 of irelocation.com/usalarm/quotemailer.php \n\n Lead ID: $lead_id doesn't want quote_id: $q_id ");
						$body .= "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
						$body .= "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
		
						//-- Update the quote_id and take out the lead_id that doesn't want the lead	
						$sql_rem = " update leads_security set lead_ids = replace(lead_ids,'$lead_id','') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_rem<br />\n";
						$rs_rem = new mysql_recordset($sql_rem);
						
						//-- Remove the occaisional double pipes ( || ) that are created	
						$sql_dp = " update leads_security set lead_ids = replace(lead_ids,'||','|') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_dp<br />\n";
						$rs_dp = new mysql_recordset($sql_dp);
						
						//-- Trim pipes ( | )
						$sql_trim = " update leads_security set lead_ids = trim(BOTH '|' FROM lead_ids ) where quote_id = " .$myarray['quote_id'] ;
						$body .= "Running: $sql_trim<br />\n";
						$rs_trim = new mysql_recordset($sql_trim);
		
		
					}
				}	
				//mark lead when done.
				markAsSent($rs->myarray['quote_id']);
			
			}			
			
		}
		
	}
	
echo $body;
$body = str_replace('<br />','',$body);

$content = ob_get_contents();
ob_end_clean();
echo $content;

if(DEBUG_EMAILS)
	mail("rob@irelocation.com","USALARM QUOTEMAILER OUTPUT",$content);

?>

