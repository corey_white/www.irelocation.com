<?	
/* 
************************FILE INFORMATION**********************
* File Name:  SecurityClasses_ushome.php
**********************************************************************
* Description:  This is the USHOME SECURITYCLASS
**********************************************************************
* Creation Date:  1-11-20010
**********************************************************************
NOTES:

January 11, 2010 11:41:54 AM MST
This filel has been created mainly by culling out what wasn't needed from the original SecurityClasses.php file.  Currently, this is going to be used for ADT only, and thus, has only been set up for them.  If needed later, we can add more as they're needed.  I have stripped out anything that isn't needed, especially as it pertains to the Eversafe and Absolute companies, as they will not be getting these leads.

**********************************************************************

USHOME    USHOME    USHOME    USHOME    USHOME    USHOME   USHOME
USHOME    USHOME    USHOME    USHOME    USHOME    USHOME   USHOME
USHOME    USHOME    USHOME    USHOME    USHOME    USHOME   USHOME
*/

	define(DEGUB_TSC_EMAIL,"code@irelocation.com");
	include_once "SecurityClass.php";
	
	define(GAY_ROOT,"gaylord");
	

	//base class for Gaylord Security
	class AlarmTrax extends SecurityClass
	{
	
		function setGaylordAffiliate($company)
		{
			$source = $this->thearray["source"];
			$company_count = $this->thearray["company_count"];
			$this->company = $company;
			
			$this->source_id = 124; //USHOME Companies Code				<<<--- NEED NEW CODE?


			//-- set dynamic source for lead tracking - this is where we set the campaign_id for CPC leads
			switch($company_count) {
				case 1:
					$this->campaign_id = "4";
					break;
				case 2:
					$this->campaign_id = "5";
					break;
				case 3:	
					$this->campaign_id = "6"; 
					break;			
				case 4:	
					$this->campaign_id = "7"; 
					break;			
			}

			//-- remanipulate the company and source_id if it's a clickbooth lead override so the parent co gets the lead.
			if ( eregi("clkbth",$source) ) {
				
				//-- set dynamic source for lead tracking.
				switch($company_count) {
					case 1:
						$this->campaign_id = "8";
						break;
					case 2:
						$this->campaign_id = "9";
						break;
					case 3:	
						$this->campaign_id = "10"; 
						break;			
					case 4:	
						$this->campaign_id = "11"; 
						break;			
				}

				$company = GAY_ROOT;
				$this->source_id = 124;
				# $this->campaign_id = 2; //-- now dynamically set with the switch above
				echo "<br />THIS IS A CLICKBOOTH LEAD<br />";
			} 
			
			
			$this->url = "https://secure.securitytrax.com/".$company."/leadpostxml.php";
		}
	
		function AlarmTrax($lead_id=FAKE_COMPANY)
		{		
			$this->lead_id = $lead_id;
			
			if ($this->lead_id == GAYLORD)
			{
				$this->source_id = 124; 
				$this->url = "https://secure.securitytrax.com/gaylord/leadpostxml.php";
			}

			else /*if ($lead_id == FAKE_COMPANY)* for testing */
			{
				$this->source_id = 124;
				$this->url = "https://secure.securitytrax.com/pinnacle/leadpostxml.php";	
			}	
		}	
	
		//first field is the $this->myarray data array.
		function getAlarmTraxXML($data)
		{		
			$extras = array();
			if ($data['fire'])
				$extras[] = "fire protection";
	
			if ($data['cctv'])
				$extras[] = "Video Surveillance, CCTV System";
				
			if ($data['access'])
				$extras[] =  "access control";
	
			if (strlen($data['other']) > 0)
				$extras[] =  "other services: ".$data['other'];
			
			$extra_text = "Building Type: ".$data['building_type'];

			
			if (count($extras) > 0)
				$extra_text .= ", Customer Also Requested: ".implode(", ",$extras);
			
			if ($data['quote_type'] == 'com')
				$extra_text .= ", This is a Commercial Lead";
			else
				$extra_text .= ", This is a Residential Lead";
			
			
			$xml = "<"."?xml version='1.0' standalone='yes'?".">
			<leads>
				<lead>
					<source_id>".$this->source_id."</source_id>
					<campaign_id>".$this->campaign_id."</campaign_id>
					<lead_type_id>".$this->lead_type_id."</lead_type_id>
					<lead_purchase_price></lead_purchase_price>
					<fname>".xmlentities($data['name1'])."</fname>
					<lname>".xmlentities($data['name2'])."</lname>
					<address1>".xmlentities($data['address'])."</address1>
					<address2></address2>
					<city>".xmlentities($data['city'])."</city>
					<state>".$data['state_code']."</state>
					<zip>".$data['zip']."</zip>
					<phone1>".$data['phone1']."</phone1>
					<phone2>".$data['phone2']."</phone2>
					<phone3></phone3>
					<phone4></phone4>
					<email>".$data['email']."</email>
					<lead_comments>".xmlentities($data['comments'])."</lead_comments>
					<contact_time>afternoon</contact_time>
					<lead_source_info>online</lead_source_info>
					<lead_company_unique_id>".$data['quote_id']."</lead_company_unique_id>
					<lead_home_ownership>".$data['own_rent']."</lead_home_ownership>
					<lead_callcenter_rep></lead_callcenter_rep>
					<lead_callcenter_notes>".xmlentities($extra_text." Note: Lead sent to $company_count companies.")." </lead_callcenter_notes>
					<lead_promo_code></lead_promo_code>
					<lead_custom1></lead_custom1>
					<lead_custom2></lead_custom2>
					<lead_custom3></lead_custom3>
					<lead_custom4></lead_custom4>
					<spouse></spouse>
				</lead>
			</leads>";
			//mail(DEGUB_TSC_EMAIL,"Gaylord USHOME XML",$xml);
			return $xml;
		}

		function test()
		{
			return true;
		}
						
		function format()
		{
			$xml = $this->getAlarmTraxXML($this->thearray);
			//mail(DEGUB_TSC_EMAIL,"AlarmTrax XML",$xml);
			$this->xml = $xml;		
		}
	
		function send()
		{	
			$xmlcontent= $this->xml;
			
			$c = curl_init($this->url);
			curl_setopt($c, CURLOPT_POST, 1);
			curl_setopt($c, CURLOPT_POSTFIELDS, $xmlcontent);
			curl_setopt($c, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
			//curl_setopt($c, CURLOPT_CAINFO, "cacert.pem"); 
			//We can provide this certfile if needed (We use GeoTrust)
			curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
			//(if you are having problems with the https cert, you can disable checking)
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			$page = curl_exec($c);
			curl_close($c); 
			$this->processResponse($page);
		}
		
		function processResponse($response)
		{		
			//<body>Info: Attempting 1 leads.<br />Success: Added lead Fake Mctesterson.<br /><Br/>1 Leads Processed.<br/>
			$rawresponse = $response;
			
			echo "<br>\nResponse:$response.<br>\n<br>\n";
			
			$response = substr($response,strpos($response,"<body>")+6);
			$insert_response = trim(str_replace("version=\"1.0\"?;","",strip_tags($response)));

			list($attempt,$result,$rest) = split("<br />",$response,3);		
			
			$quote_id = $this->thearray['quote_id'];
			$success = (substr_count($result,"Success:") > 0)?1:0;
			if ($success == 0)
			{
				//new format?
				if (substr_count($response,"success=\"true\"") > 0)
				{
					$success = 1;
					$start = strpos($response,"<customer_id>")+13;
					$end = strpos($response,"</",$start);
					if ($end > $start && $start != -1)
						$responseId = substr($response,$start,$end);				
					else
					{
						$success = 0;
						$responseId = "Success?";
						//mail(DEGUB_TSC_EMAIL,"AlarmTrax Failure? - ".$this->lead_id,
						//"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
						//"\nLeadXML:".$this->xml);
						recordLeadFailure($this->thearray,
							$this->lead_id,
							"security",
							"irelocation.leads_security");
					}
					
					save_response($quote_id,$insert_response,$this->lead_id,$success,"ushome");	
					echo "Saving Success Response for $quote_id in Parent part a<br />";
				}
				else
				{
					$success = 0;
					$responseId = "failure";
					//mail(DEGUB_TSC_EMAIL,"AlarmTrax Failure - ".$this->lead_id,
					//"Lead #$quote_id Came Back with an odd response code: \nResult:$response".
					//"\nLeadXML:".$this->xml);
					recordLeadFailure($this->thearray,
						$this->lead_id,
						"security",
						"irelocation.leads_security");
					save_response($quote_id,$insert_response,$this->lead_id,$success,"ushome");	
					echo "Saving Fail Response for $quote_id in Parent<br />";
				}
			}
			else
			{	
				save_response($quote_id,$result,$this->lead_id,$success,"ushome");			
				echo "Saving Success Response for $quote_id in Parent part b<br />";
			}
		}			
	}




	class Gaylord extends AlarmTrax
	{
		function Gaylord()
		{
			parent::AlarmTrax(GAYLORD);
			$this->lead_type_id = "3";
			$this->company = "gaylord";
		}	
		
		function test()
		{
			return true;
		}
	
		function send()
		{					
								
			//if() run code for mark alliata
			$state = $this->thearray["state_code"];	
			$quote_id = $this->thearray["quote_id"];
			$source = $this->thearray["source"];
			$aff = false;
			
			$this->setGaylordAffiliate(GAY_ROOT);		
			parent::format();

			
			parent::send();				
		}		
	}
	






	//no one in the matrix campaign is using this one..
	class TextEmail extends SecurityClass
	{
		function TextEmail($email)
		{
			$this->emailaddress = $email;//;
		}
		
		function sqrFootage($type,$sq)
		{
			echo "sqrFootage($type,$sq)<br/>";
			if ($sq == 0 || $sq == 50)
			{
				if ($type == "com" && $sq == 0) return 2500;
				else if ($type == "com" && $sq == 50) return 20000;
				else if ($type == "res" && $sq == 0) return 1500;
				else if ($type == "res" && $sq == 50) return 10000;
			}
			else
				switch($sq)
				{
					case 15: return 3000;
					case 25: 
					case 30: return 5000;
					case 100: return 50000;
					case 200: return 20000;
					case 500: return "50000+";
				}
				
		}
		
		function send()
		{
			$this->format();
			mail($this->emailaddress,"Security Lead",$this->message,$this->headers);
			
			if(DEBUG_EMAILS)
				mail(DEGUB_TSC_EMAIL,"Security Lead - ".$this->lead_id, $this->message,$this->headers);

		}
		
		function assign($thearray)
		{			
			$this->thearray = $thearray;	
			$this->lead_body = "";		
			if ($this->test())
			{
				$this->format();
				return true;
			}
			else return false;
		}
		
		function formatHTML()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			echo "Extras: F: $fire A: $access C: $cctv P: $prewired B: $basement <br/>";
			
			echo "Fire:-$fire- <br/>";
			
			$extra_services = array();
			if ($fire == "1")
				$extra_services[] = "fire";
			if ($access  == "1")
				$extra_services[] = "access";
			if ($cctv  == "1")
				$extra_services[] = "cctv";
			if ($prewired  == "1")
				$extra_services[] = "house is prewired";
			if ($basement  == "1")
				$extra_services[] = "basement is finished";		
			
			if (count($extra_services) > 0)
				$extras = implode(",",$extra_services);
			echo "Extras: $extras <br/>";
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->HTMLheaders = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/html; charset=iso-8859-15";
						
			$this->HTMLmessage = 
			"<table border='0' width='100%' id='table1' cellspacing='0' cellpadding='0'>".
			"<tr><td colspan='2'><strong>Top Security Lead</strong></td></tr>".
			"<tr><td colspan='2'><hr width='250' align='left'/></td></tr>".
			"<tr><td width='200'>Quote ID: </td><td>".$quote_id."</td></tr>".
			"<tr><td width='200'>Received: </td><td>".$received."</td></tr>".
			"<tr><td width='200'>Customer Name: </td><td>".$name."</td></tr>".
			"<tr><td width='200'>E-Mail Address: </td><td>".$email."</td></tr>".
			"<tr><td width='200'>Phone Number: </td><td>".$phone1."</td></tr>".
			"<tr><td width='200'>Alternate Phone: </td><td>".$phone2."</td></tr>".
			"<tr><td width='200'>Street Address: </td><td>".$address."</td></tr>".
			"<tr><td width='200'>City: </td><td>".$city.", ".$state_code."</td></tr>".
			"<tr><td width='200'>Postal Code: </td><td>".$zip."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Quote Type: </td><td>".$type."</td></tr>".
			"<tr><td width='200'>Building Type </td><td>".$building_type."</td></tr>".
			"<tr><td width='200'>Square Footage: </td><td>".$sqr_footage."</td></tr>".
			"<tr><td width='200'>Owners: </td><td>".$own_rent."</td></tr>".
			"<tr><td width='200'>Requested Services/Features: </td><td>".$extras."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td colspan='2' align='left'>Comments:</td></tr>".
			"<tr><td colspan='2' align='left'>".$comments."</td></tr>".
			"<tr><td colspan='2'>&nbsp;</td></tr>".
			"<tr><td width='200'>Lead Source: </td><td>iRelocation</td></tr>".
			"</table>";
			
		}
		
		function format()
		{
			extract($this->thearray);
			//$sqrfootage = $sqr_footage;
			
			if ($quote_type == "res")
			{
				$name = $name1." ".$name2;		
				$type = "Residential";		
			}
			else
			{
				$name = $name2;
				$type = "Commercial";
			}
		
			$this->headers = "From: iRelocation.com <no_reply@irelocation.com>\n".
						"Reply-To: ".$name." <$email>\n".
						"Return-Path: '".$name."' <$email>\n".
						"Content-Type: text/plain; charset=iso-8859-15";
						
			$this->message = "Source: irelocation.com\n".
						"Received: $received\n".
						"Lead Type: $type\n".
						"Lead ID: $quote_id\n".
						
						"Customer: ".$name."\n".
						"Email: $email\n".
						"Phone: $phone1\n";
						
			if (strlen($phone2) > 0)
				$this->message .= "Alternate Phone: $phone2\n";
					
			$this->message .= "Street Address: $address\n".
						"City: $city\n".
						"Province: $state_code\n".
						"Postal Code: $zip\n".
						//"Needs: ||current_needs||\n".
						"Building Type: $building_type\n".
						"Square Footage: ".$sqr_footage."\n".
						"Own/Rent: ".$own_rent."\n".						
						"Customer Comments: $comments";								
		}
		
	}
	
	



?>