<?php
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer_ushome.php
**********************************************************************
* Description:  This is the quotemailer for the USHOME security alarm campaign
**********************************************************************
* Creation Date:  01-11-2010
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: the purpose of this script is to simply resend the lead to the lead_ids that were supposed to get it, but somehow didn't.  This script needed to be created as I can't re-run the quotemailer.php code, as it may send the lead to different companies, because of how the current quotemailer now processes leads.

Push the quote ids into the script with quote_ids=1,2,3,4 in the URL
>>>> DELETE THE ABOVE NOTES WHEN DONE WITH THEM

This quotemailer will send a lead only to those lead_ids that are identified for the lead in the Db (lead_ids field).  

This will be looking at the movingdirectory.campaign table for active USHOME campaigns.  Initially, there is only 1 company that will be getting these leads, but the quotemailer should be able to handle different companies if we decide to expand this and do the same for other companies.



*/

	ob_start();
	

 	define(DEBUG_EMAILS,true);
 	
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses_ushome.php";//all company functions!! :P

	//-- OVERRIDE LEADS_IDS for leads that came from USALARM_MMC
	//-- We're running a temp amount of leads and these need to be sent to Anna instead of being processed.
	$sql = "update irelocation.leads_security set ready_to_send = 8 where campaign = 'ushome' and source like '%ushome_mmc%' and referrer like '%ushome mmc%' and ready_to_send = 1 ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	

	//-- let's deliniate when this output starts, as the Class output doesn't not buffer.
	$body .= "<br /><hr /><hr /><hr /><hr /><hr /><br />";

	
	//-- Delete anything that smells like a test lead
	$sql = "delete from irelocation.leads_security where name1 like 'Test%' || email = 'test@test.com' || email = 'testing@gmail.com' || name2 = 'Test' ;";
	$rs = new mysql_recordset($sql);
	echo "Delete SQL: $sql <br /><br />";

	//-- update the received column for those leads that came in from 
	$mytime = date("YmdHis");
	$sql_time = "update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and email != 'testing@gmail.com' and campaign = 'ushome'  ";
	$body .= "UPDATING RECEIVED TIME: $sql_time<br/>";
	$rs_time=new mysql_recordset($sql_time);
	echo "Update Time SQL: $sql <br /><br />";


	
 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and campaign = 'ushome' ";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes() {
		
		//-- continue normal processing...

		$sql = "select quote_id,lead_ids from leads_security where campaign = 'ushome' and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and campaign = 'ushome' ";
			 $body .= "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'ushome'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		echo "loadCompanies SQL: $sql";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.

	$all_companies = array(	GAYLORD => new Gaylord()
											);

	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	
	echo "Active Companies:<pre>";
	echo print_r($active_lead_ids,true);
	echo "</pre><br />\n";
	$body .= "Active Companies:<pre>";
	$body .= print_r($active_lead_ids,true);
	$body .= "</pre><br />\n";

	
	//where we store the active company objects.
	$active_companies = array();
	
	
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}


	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		$body .= "No Quotes.";
		echo $body;
		exit();
	}
	
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	$body .= "QUOTE SELECT: $sql<br />\n";
	
	//loop over each one.
	while($rs->fetch_array()) {
	
		//-- Need to check for dups:
		//-- get yesterday's date, then check the Db to see if there is any leads that have the same PHONE and EMAIL with a READY_TO_SEND = 3 within the last 2 days.
		$twoDaysAgo = date("Ymd",strtotime("2 days ago"));
		$phone1 = $rs->myarray["phone1"];
		$zip = $rs->myarray["zip"];
		$source = $rs->myarray["source"];
		
		$dupcheck = "select count(*) as dupcount from leads_security where left(received,8) >= '$twoDaysAgo' and phone1 = '$phone1' and zip = '$zip' and source = '$source' and ready_to_send = 3  ";
		$rs_dup = new mysql_recordset($dupcheck);
		$rs_dup->fetch_array();
		#mail("code@irelocation.com","DUP CHECK SQL","$dupcheck\n\nQuote ID should be " . $rs->myarray['quote_id']);
		
		if ( $rs_dup->myarray["dupcount"] > 0 ) { //-- uh oh, it's a dup, do not send and update it to RTS = 8
			
			$body .= "FOUND DUP!!!<br />";
			$quoteid = $rs->myarray['quote_id'];
			$rts8 = "update leads_security set ready_to_send = 8 where quote_id = '$quoteid'   ";
			$body .= "RTS DUP SQL: $rts8<br />";
			$rs_rts8 = new mysql_recordset($rts8);
			mail("code@irelocation.com","FOUND USHOME Dup - RTS SQL","Updated RTS=8 \n\nQuote ID should be $quoteid");
			
		} else { //-- found no dup, okay to process
			
			$lead_ids = $rs->myarray['lead_ids'];
			$lead_ids = trim($lead_ids,"|");
			$lead_ids = split("\|",$lead_ids);
			
			//-- Send leads to Anna for validation if a specific source id
			if ( eregi("yakov",$rs->myarray['source']) || eregi("hsas",$rs->myarray['source']) || eregi("coregmedia",$rs->myarray['source']) || eregi("abcleads",$rs->myarray['source']) || eregi("prospectiv",$rs->myarray['source'])  ) {
				
				$anna_csv = $rs->myarray['quote_id'] . ", " . $rs->myarray['received'] . ", " . $rs->myarray['source'] . ", " . $rs->myarray['name1'] . ", " . $rs->myarray['name2'] . ", " . $rs->myarray['phone1'] . ", " . $rs->myarray['email'] . ", " . $rs->myarray['own_rent'] . ", " . $rs->myarray['campaign'];
				
				$anna_msg = $rs->myarray['quote_id'] . "
" . $rs->myarray['received'] . "
" . $rs->myarray['source'] . "
" . $rs->myarray['name1'] . "
" . $rs->myarray['name2'] . "
" . $rs->myarray['phone1'] . "
" . $rs->myarray['email'] . "
" . $rs->myarray['own_rent'] . "
" . $rs->myarray['campaign'] . "
" . $rs->myarray['address'] . "
" . $rs->myarray['city'] . "
" . $rs->myarray['state_code'] . "
" . $rs->myarray['zip'];
				
				mail("code@irelocation.com,anna@irelocation.com","Affiliate Lead Needing Validation - USHOME", $anna_msg . "\n\n" . $anna_csv);
			} 

			//get the count after cleaning brinks.
			//count lead_ids
			$company_count = count($lead_ids);

			if ( $company_count == 0 ) { //-- This condition should never happen, but it is. Need to send me info when this happens and prevent the quotemailer from crashing.
				$quoteid = $rs->myarray['quote_id'];
				
				mail("code@irelocation.com","EMPTY LEAD_IDS VAR IN USHOME","Quote ID = $quoteid");
				$body .= "<br />There was an issue with Quote ID $quoteid - seems it had no lead_ids in the array.<br />";
				$body .= "LEAD_IDS ARRAY IS CURRENTLY:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
				
			} else { //-- continue to process
				
				$myarray = formatData($rs->myarray);
				
				$myarray['company_count'] = $company_count;
				
				//loop through each company that should get this lead.
				$body .= "LEAD_IDS in \$lead_ids:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
	
				
				foreach($lead_ids as $lead_id) {
					$company = $active_companies[$lead_id];
					$body .= "Now processing $company<br />\n";
		
					//assign it?
					if ($company->assign($myarray)) { //assign the array, and test them.
						$q_id = $myarray['quote_id'];
						$body .= "Looking to send $q_id<br />\n";
		
						$company->send();			
						$body .= $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
						
					} else {
						$q_id = $myarray['quote_id'];
						$body .= "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
						$body .= "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
		
						//-- Update the quote_id and take out the lead_id that doesn't want the lead	
						$sql_rem = " update leads_security set lead_ids = replace(lead_ids,'$lead_id','') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_rem<br />\n";
						$rs_rem = new mysql_recordset($sql_rem);
						
						//-- Remove the occaisional double pipes ( || ) that are created	
						$sql_dp = " update leads_security set lead_ids = replace(lead_ids,'||','|') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_dp<br />\n";
						$rs_dp = new mysql_recordset($sql_dp);
						
						//-- Trim pipes ( | )
						$sql_trim = " update leads_security set lead_ids = trim(BOTH '|' FROM lead_ids ) where quote_id = " .$myarray['quote_id'] ;
						$body .= "Running: $sql_trim<br />\n";
						$rs_trim = new mysql_recordset($sql_trim);
		
		
					}
				}	
				//mark lead when done.
				markAsSent($rs->myarray['quote_id']);
			
			}			
			
		}
		
	}
	
echo $body;
$body = str_replace('<br />','',$body);

$content = ob_get_contents();
ob_end_clean();
echo $content;

$body = str_replace('<br />','\\n',$body);

if(DEBUG_EMAILS)
	mail("code@irelocation.com","USHOME RE_QUOTEMAILER OUTPUT",$content);

?>

