<?php
/*
 * Created on Jan 11, 2008
 *
 
 9/10/08 - Rob - Adding functionality that "pre-washes" the quotes so that Brinks doesn't get RES Renters and APEX doesn't get RES Renters and no CLICKBOOTH leads.
 
 9/12/08 - Rob - Okay, we're now changing the above as NO company wants renters.  Changing the initial SQL to only pull own_rent = own.  Also adding a function to delete the lead_ids from any renter lead.
 
 10-9-2008 - 10-10-2008 - Rob - Adding ability for a client to get renter leads
 
 5/4/09 - Rob - On 5-1, added massive amounts fo code for the way we're not giving out leads.  Lots of logic for determining who gets the lead.  Today, still working on fine-tuning that code.
 
 7-14-2009 - updating quotemailer so it will also run the 'alarmsys' quotes from AlarmSystemsNow.com, which are identicle to USAlarm leads.
 
 7-21-2009 - Added code to delete test leads.  This existed in the Top Security quotemailer, but since this quotemailer runs twice as often as TSC, some clients were getting test leads sent to them.
 
 5-11-2010 - Removed a lot of the commented code to make it cleaner and easier to read.  Added Smith and Wesson (1794) to share leads with FrontPoint for certain zips
 */
	ob_start();
	
 	define(DEBUG_EMAILS,true);
 	
	include "../cronlib/.functions.php";//Generic Functions.	
	include ".lib.php";//specific to Security Cron	
	include_once '../inc_mysql.php';//Database!	
	include "SecurityClasses.php";//all company functions!! :P

	//-- let's deliniate when this output starts, as the Class output doesn't not buffer.
	$body .= "<br /><hr /><hr /><hr /><hr /><hr /><br />";
	
	//-- Delete anything that smells like a test lead
	$sql = "delete from irelocation.leads_security where name1 like 'Test%' || email = 'test@test.com' || email = 'testing@gmail.com' || name2 = 'Test' || source like '%alertbot%' ";
	$rs = new mysql_recordset($sql);
	echo "Delete SQL: $sql <br /><br />";

	//-- update the received column for those leads that came in from 
	$mytime = date("YmdHis");
	$sql_time = "update irelocation.leads_security set received='".$mytime."' where (received='' or received='0') and LOWER(name1) != 'test' and email != 'testing@gmail.com' and (campaign = 'usalarm' || campaign = 'alarmsys') ";
	$body .= "UPDATING RECEIVED TIME: $sql_time<br/>";
	$rs_time=new mysql_recordset($sql_time);
	echo "Update Time SQL: $sql_time <br /><br />";
	
 
	//-- OVERRIDE LEADS_IDS for OWNERS - rewrite those lead_ids that are eligible to get OWN leads 8/17/09
	//-- REMOVED 1520 on 5/18/09 15:27 PM
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1690' where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'own' and ready_to_send = 1  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- Check for leads that have a lead_ids of || and insert the lead_ids
	$sql = "update irelocation.leads_security set lead_ids = '1519|1552|1569|1690' where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids = '||'  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- update and RTS = 2 so that they will run again.
/* 12/10/09 10:54 AM - Taking this out so I can see if there are any leads being caught like this.  Had an incident this morning where a lead was being sent over and over because it got stuck on RTS=2
	$sql = "update irelocation.leads_security set ready_to_send = 1 where ready_to_send = 2 and (campaign = 'usalarm' || campaign = 'alarmsys')  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
*/
	
	//-- update ELIVATE leads to RTS = 8 so that they won't process
	$sql = "update irelocation.leads_security set ready_to_send = 8, lead_ids = '' where ready_to_send = 1 and source like '%elivate%'  ";
	$body .= "<b>update ELIVATE leads to RTS = 8 so that they won't process:</b> $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- update PROSPECTIV leads to RTS = 8 so that they won't process
	$sql = "update irelocation.leads_security set ready_to_send = 8, lead_ids = '' where ready_to_send = 1 and source like '%prospectiv%'  ";
	$body .= "<b>update PROSPECTIV leads to RTS = 8 so that they won't process:</b> $sql<br />\n";
	$rs = new mysql_recordset($sql);
	

	//-- OVERRIDE LEADS_IDS for RENTERS - rewrite those lead_ids that are eligible to get RENT leads 8/17/09
	//-- 8/17/09 11:18 AM - Added 1690
	//-- 1/22/10 15:13 PM - removed 1520
	$sql = "update irelocation.leads_security set lead_ids = '1690' where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'rent' and ready_to_send = 1  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	//-- OVERRIDE LEADS_IDS for leads that came from alarmsys and redirected to usalarm
	$sql = "update irelocation.leads_security set lead_ids = '1552|1569|1690' where campaign = 'usalarm' and source like '%clkbth%' and own_rent = 'own' and ready_to_send = 1  ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
	
	
	//-- OVERRIDE LEADS_IDS for leads that came from USALARM_MMC
	//-- We're running a temp amount of leads and these need to be sent to Anna instead of being processed.
	$sql = "update irelocation.leads_security set ready_to_send = 8 where campaign = 'usalarm' and source like '%usalarm_mmc%' and referrer like '%usalarm v3%' and ready_to_send = 1 ";
	$body .= "Ran $sql<br />\n";
	$rs = new mysql_recordset($sql);
		

	
	//-- Special Validation
		
#	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and own_rent = 'own' and lead_ids is not null and ready_to_send = 1 and substrCount(lead_ids, '|') > 3 "; //-- OLD SQL
	
	$sql_getit = "select quote_id, received, source, quote_type, own_rent, zip, lead_ids, campaign from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1  ";
	
	$body .= " SPECIAL VALIDATION SELECT: $sql_getit<br />\n";
	$rs_getit = new mysql_recordset($sql_getit);

	while( $rs_getit->fetch_array() ) {
		 extract($rs_getit->myarray);
		 
		$body .= "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br />\n";
		$body .= "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-<br />\n";
		$body .= "LEAD INFO<br />\n";
		$body .= "$quote_id<br />\n";
		$body .= "$source<br />\n";
		$body .= "$quote_type<br />\n";
		$body .= "$own_rent<br />\n";
		$body .= "ORIG: $lead_ids<br />\n";
		$body .= "<br />\n";


		if ( $own_rent == 'rent' ) { //-- Renters
			//-- SPECIAL VALIDATION FOR RENTERS ============================================ (new section created 8/17/09)
			//-- 9/17/09 11:35 AM - bypassed all of the 1690 code, as they don't want renters for now (though we may reinstitute later)
			//-- this section determines whether the renter lead goes to Protect America (1520) or FrontPoint (1690)
			
			//-- 1690 only wants to get 200 leads per month, so we need to check to see how many renter leads they've received so far this month, and if under 200, run a lottery between 1690 and 1520.
			
			echo "<h1>RENTERS</h1>";
			echo "<br /><br />RUNNING SPECIAL VALIDATION FOR RENTERS<br />";
			
			
			
		} else { //-- OWNERS
			
			//-- SPECIAL VALIDATION FOR OWNERS ============================================
			//-- this section removes lead_ids for those companies that don't want a certain type of lead for OWNer leads

			echo "<h1>OWNERS</h1>";
			echo "<br /><br />RUNNING SPECIAL VALIDATION FOR OWNERS<br /><br />";
			

			$body .= "<br />\nPROCESSING $quote_id<br />\n";
			
			$body .= "<br />\nNOW RUNNING APX CRITERIA";
			$body .= "<br />\nNOW RUNNING APX CRITERIA<br />\n<br />\n";
	
			//-- check to see if APX wants the zip 
			$sql = "select zip From security_zip_codes where lead_id = '1569' and zip = '$zip' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();
	
			if ( $valid  && $own_rent != "rent"  ) { //-- Found in APX zip list, so Protect America doesn't want the zip (unless it's a renter)
				$body .= "Found Zip $zip in APX Zip List and is a OWNer, so Protect America Does Not Want it<br />\n";
				
				$current_month = date("Ym");
				$body .= "Current Month set to $current_month<br />\n";
				
				//-- 1569 GETS THIS LEAD - REMOVE 1520 
				$lead_ids = str_replace("1520","",$lead_ids);

				
			} else {
	
				$body .= "Zip $zip NOT Found in APX List or is a RENTER, so 1569 DOES NOT GET THIS LEAD<br />\n";
	
				//-- remove 1569  from the equation
				$lead_ids = str_replace("1569","",$lead_ids);
				$body .= "Removing APX (1569) <br />\n";
	
				//-- We don't want Protect America (1520) to get any leads that aren't renters, so removing 1520
				$lead_ids = str_replace("1520","",$lead_ids);
				$body .= "We don't want Protect America (1520) to get any leads that aren't renters, so removing 1520 (1520 doesn't get OWNer leads normally, only on an overflow situation when we don't have anyone else to give them too) - REMOVED 1520<br />\n";
				
			}
	
			$body .= "<br />\nLEAD IDS AFTER APX SCRUB: $lead_ids<br />\n";
			
			
			
			$body .= "<br />\nNOW RUNNING BRINKS CRITERIA";
			$body .= "<br />\nNOW RUNNING BRINKS CRITERIA<br />\n<br />\n";
	
			//-- Now, we need to do it again for Brinks (1519), FrontPoint (1690) and Pinnacle (1660).  Scratch that - no Pinnacle anymore
	
			//-- check to see if Brinks wants the zip ==================================  1519, 1690
			$sql = "select zip From security_zip_codes where lead_id = '1519' and zip = '$zip' limit 1";
			$rs = new mysql_recordset($sql);
			$valid = $rs->fetch_array();
	
			if ( $valid  && $own_rent != "rent"  ) { //-- Found in Brinks zip list, so  FrontPoint can't get the zip (unless it's a renter)
				$body .= "Found Zip $zip in Brinks Zip List and is an OWNer, so  FrontPoint can't get it<br />\n";
				
				//-- remove 1690 from the equation
				$lead_ids = str_replace("1690","",$lead_ids);
				$body .= "Removing FrontPoint (1690)<br />\n";
				
				
				//-- Need to check several companies to see who gets this lead.
				//-- Check each for a zip code match
				
				//-- check to see if Central Security Alarm wants the zip
				$body .= "Checking to see if Central Security Alarm (1797) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1797' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$csa_wants_zip = $rs->fetch_array();
				
				//-- check to see if Premium Security (1805) wants the zip
				$body .= "Checking to see if Premium Security (1805) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1805' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$Premium_wants_zip = $rs->fetch_array();
				
				//-- check to see if Best Security Portland (1807) wants the zip
				$body .= "Checking to see if Best Security Portland (1807) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1807' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$BestSecurity_wants_zip = $rs->fetch_array();
				
				//-- check to see if Defense First (1811) wants the zip
				$body .= "Checking to see if Defense First, Inc (1811) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1811' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$DefenseFirst_wants_zip = $rs->fetch_array();
				
				//-- check to see if Spot Security (1812) wants the zip
				$body .= "Checking to see if Spot Security (1812) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1812' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$SpotSecurity_wants_zip = $rs->fetch_array();
				
				//-- check to see if Santana Services (1804) wants the zip
				$body .= "Checking to see if Santana Services Inc (1804) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1804' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$Santana_wants_zip = $rs->fetch_array();
				
				//-- check to see if Protection One (1521) wants the zip
				$body .= "Checking to see if Protection One (1521) also wants this zip<br />\n";
				$sql = "select zip From security_zip_codes where lead_id = '1521' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$ProtectionOne_wants_zip = $rs->fetch_array();
				
				
				//-- select random number
				$rand = rand(0,1000);

				
				//-- Now we run over who really wants it.  I'm doing it this way because it's the quickest way to set it up; we're not going to be using this for too much longer and we only needs to run leads via this quotemailer for a very short time.  Just until I get the LE system set up.
				if ( $csa_wants_zip ) { //-- Replace Brinks with Central Security Alarm
					
					$body .= "Replacing Brinks (1519) with Central Security Alarm (1797)<br />\n";
					$lead_ids = str_replace("1519","1797",$lead_ids);
						
				} elseif ( $Premium_wants_zip ) { //-- Replace Brinks with Premium Security (1805) 
				
					$body .= "Replacing Brinks (1519) with Premium Security (1805) <br />\n";
					$lead_ids = str_replace("1519","1805",$lead_ids);
						
				} elseif ( $BestSecurity_wants_zip || $SpotSecurity_wants_zip ) { //-- Replace Brinks with Best Security Portland (1807) or Spot Security (1812) 
				
					if ( $rand > 500 && $BestSecurity_wants_zip ) { //-- give to Best Security Portland (1807) 
										
						$body .= "Replacing Brinks (1519) with Best Security Portland (1807) <br />\n";
						$lead_ids = str_replace("1519","1807",$lead_ids);
							
					} 
						
					if ( $rand <= 500 && $SpotSecurity_wants_zip ) { //-- give to Spot Security (1812) 
    					
    					$body .= "Replacing Brinks (1519) with Spot Security (1812) <br />\n";
						$lead_ids = str_replace("1519","1812",$lead_ids);
					} 							

				
				} elseif ( $DefenseFirst_wants_zip ) { //-- Replace Brinks with Defense First (1811) 
				
					$body .= "Replacing Brinks (1519) with Defense First (1811) <br />\n";
					$lead_ids = str_replace("1519","1811",$lead_ids);
						
				} elseif ( $Santana_wants_zip ) { //-- Replace Brinks with Santana Services (1804) 
				
					$body .= "Replacing Brinks (1519) with Santana Services (1804) <br />\n";
					$lead_ids = str_replace("1519","1804",$lead_ids);
						
				} elseif ( $ProtectionOne_wants_zip ) { //-- Replace Brinks with Protection One (1521) 
				
					$body .= "Replacing Brinks (1519) with Protection One (1521) <br />\n";
					$lead_ids = str_replace("1519","1521",$lead_ids);
						
				}
				
				$body .= "NONE of these smaller security companies wants this zip code ($zip) (or lost the lottery), so letting Brinks keep this lead.<br />\n";

				
			} else { //-- Give it to either FrontPoint (1690) or S&W (1794) or Central Security Alarm (1797)
	
				//-- REMOVE BRINKS (1519)
				$body .= "Zip $zip <b>Not Found</b> in Brinks List or is a RENTER, so 1690 CAN GET THIS LEAD<br />\n";
				$lead_ids = str_replace("1519","",$lead_ids);	
				$body .= "Removing Brinks (1519)<br />\n";
	
				//-- SET CURRENT MONTH
				$current_month = date("Ym");
				$body .= "Current Month set to $current_month<br />\n";
				
				//-- GET LEADS PER DAY ALLOWED for 1690
				$sql_pin = "select leads_per_day from movingdirectory.campaign where lead_id = 1690 and site_id = 'usalarm' and month = '$current_month' ";
				$rs_pin = new mysql_recordset($sql_pin);
				$rs_pin->fetch_array();
				$leads_per_day1690 = $rs_pin->myarray["leads_per_day"];
				$body .= "sql_pin = $sql_pin<br />\n";
				
				$body .= "leads_per_day1690 = $leads_per_day1690<br />\n<br />\n";

				//-- GET THE CURRENT NUMBER OF LEADS RECEIVED FOR TODAY for 1690
				$today = date("Ymd");
				$sql_pincount = "select count(quote_id) as count from irelocation.leads_security where left(received,8) = '$today' and own_rent = 'own' and lead_ids like '%1690%' and ready_to_send = 3 ";
				$rs_pincount = new mysql_recordset($sql_pincount);
				$rs_pincount->fetch_array();
				$pincount1690 = $rs_pincount->myarray["count"];
				$body .= "sql_pincount = $sql_pincount<br />\n";
	
				$body .= "pincount1690 = $pincount1690<br />\n<br />\n";
				
				//-- DETERMINE IF FRONTPOINT HAS MET THEIR DAILY CAP
				if ( $pincount1690 <= $leads_per_day1690 ) {
					$okToGet1690 = "yes";
				} else {
					$okToGet1690 = "no";
				}
	
				$body .= "FrontPoint: Leads Allowed vs Leads for today =  $leads_per_day1690 <= $pincount1690<br />\n";
				$body .= "FrontPoint (1690) still needs leads for today: $okToGet1690<br />\n";
				$body .= "<br />\n";
				$body .= "<br />\n";


				//-- DETERMINE IF ZIP IS IN S&W'S ZIP LIST AND IF SO, GIVE IT TO THEM INSTEAD OF FRONTPOINT
				$body .= "DETERMINE IF ZIP IS IN S&W'S ZIP LIST AND IF SO, GIVE IT TO THEM INSTEAD OF FRONTPOINT<br />\n";

				//-- check to see if S&W wants the zip andif so, replace 1690 with 1794
				$sql = "select zip From security_zip_codes where lead_id = '1794' and zip = '$zip' limit 1";
				$rs = new mysql_recordset($sql);
				$sw_wants_zip = $rs->fetch_array();
				
				//-- select random number
				$rand = rand(0,1000);

				if ( $sw_wants_zip && $rand < 501 ) { //-- 50% opportunity for the lead to go to S&W
					$body .= "S&W Wants this zip<br />\n";
					
					$body .= "Replacing FrontPoint (1690) with Smith and Wesson (1794)<br />\n";
					$lead_ids = str_replace("1690","1794",$lead_ids);//-- Replace FrontPoint with Smith and Wesson
				
				} 
				
				
				//-- IF FRONTPOINT HAS MET THEIR CAP, REMOVE THEM FROM THE LEADS LIST
				if ( $okToGet1690 != "yes" ) { //-- Remove FrontPoint
					$body .= "Removing FrontPoint (1690)<br />\n";
					$lead_ids = str_replace("1690","",$lead_ids);//-- remove FrontPoint
					
				}
				

	
			}
			
	
			
			//-- THIS SECTION BELOW SCRUBS THE LEAD_IDS FIELD TO MAKE IT GOOD AGAIN
			//-- Finish scrubbing the lead_ids before reinserting them into the quote
			//-- remove quad pipes ( |||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 1: $lead_ids<br />\n";
			
			//-- remove triple pipes ( ||| ) - just in case this condition exists
			$lead_ids = ereg_replace("\|\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 2: $lead_ids<br />\n";
			
			//-- remove double pipes ( || )
			$lead_ids = ereg_replace("\|\|","\|",$lead_ids);
			$body .= "Pipe Scrub Own 3: $lead_ids<br />\n";
			
			$lead_ids = stripslashes($lead_ids);
			$body .= "LEAD IDS after Own stripslashes: $lead_ids<br />\n";
	
			//-- remove pipes as the first location
			if ( substr($lead_ids,0,1) == "|"  ) {
				$lead_ids = substr($lead_ids,1);
			} 
			$body .= "Pipe Scrub Own 4: $lead_ids<br />\n";
			
			//-- remove pipes as the last location
			if ( substr($lead_ids,-1,1) == "|"  ) {
				$lead_ids = substr($lead_ids,0,-1);
			} 
			$body .= "Pipe Scrub Own 5: $lead_ids<br />\n";
	
			$body .= "LEAD IDS AFTER OWN PIPE SCRUB: $lead_ids<br />\n";
			
			
			//-- WE NEED TO CHECK IF THERE ARE TOO MANY LEAD_IDS
			$test1_arr = split("\|",$lead_ids);
			$test1_count = count($test1_arr);
			$body .= "Testing for too many leads";
			$body .= "test1_count = $test1_count<br />\n";
	
			if ( $test1_count > 3 ) { //-- there are too many, let's shuffle them and pick the first 3 
				$test1_arr = shuffle($test1_arr);
				$lead_ids = $test1_arr[0] . "|" . $test1_arr[1] . "|" . $test1_arr[2];
				$body .= "NEW LEAD ID ORDER IS $lead_ids<br />\n";
			} else {
				$body .= "Checked to see if there were too many leads and found none.<br />\n";
			}
			
			$body .= "<br />\nLEAD IDS AFTER CHECKING FOR TOO MANY LEADS: $lead_ids<br />\n";
			
			//-- NOW WE NEED TO CHECK IF THERE ARE TOO FEW LEAD_IDS
	
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
			//-- if there are less than 3 lead_ids, then add some back into the mix, as we haven't sold all of the leads that they don't want yet.
			$brinks_arr = split("\|",$lead_ids);
			$brinks_test = count($brinks_arr);
			$body .= "Testing for too few leads.";
			$body .= "brinks_test = $brinks_test<br />\n";

			
			if ( $brinks_test < 2 ) { //-- if we only have 1 lead_id left, then everyone else has received their daily quota and we can give it again to someone else
	
				//-- if 1690 is already in the list, add brinks, otherwise, add FrontPoint
				if ( ereg("1690",$lead_ids) ) { 

					if ( $campaign == "alarmsys" ) {
						$lead_ids = $lead_ids . "|1519";
						$body .= "*** FOUND ONLY ONE LEAD_ID - But it's an AlarmSys lead, so ONLY ADDING BRINKS ***<br />\n";
						mail("rob@irelocation.com","ALARM LEAD SHORT","'re short on leads because 1520 doesn't want AlarmSys leads \n\nsent from www.irelocation.com/usalarm/quotemailer.php");
					
					} else {
						$lead_ids = $lead_ids . "|1519|1520";
						$body .= "*** FOUND ONLY ONE LEAD_ID - ADDING BRINKS AND PROTECT AMERICA ***<br />\n";
					
					}

					
				} else {

					if ( $campaign == "alarmsys" ) {
						$lead_ids = $lead_ids . "|1690";
						$body .= "*** FOUND ONLY ONE LEAD_ID - But it's an AlarmSys lead, so ONLY ADDING FRONTPOINT ***<br />\n";
						mail("rob@irelocation.com","ALARM LEAD SHORT","'re short on leads because 1520 doesn't want AlarmSys leads \n\nsent from www.irelocation.com/usalarm/quotemailer.php");
					} else {
						$lead_ids = $lead_ids . "|1690|1520";
						$body .= "*** FOUND ONLY ONE LEAD_ID -  ADDING FRONTPOINT AND PROTECT AMERICA ***<br />\n";
					}
										
					
				}
				
			} elseif ( $brinks_test < 3 ) { 

				if ( $campaign != "alarmsys" ) {
   					$lead_ids = $lead_ids . "|1520";
					$body .= "*** FOUND ONLY 2 LEAD_IDS - RE-ADDING PROTECT AMERICA ***<br />\n";
				} else {
					$body .= "OOPS, we're short on leads because 1520 doesn't want AlarmSys leads<br />\n";
					mail("rob@irelocation.com","ALARM LEAD SHORT","'re short on leads because 1520 doesn't want AlarmSys leads \n\nsent from www.irelocation.com/usalarm/quotemailer.php");
				}

			} else {
				$body .= "There are more than 2 lead_ids, Not Re-adding Any Lead_IDs<br />\n";
			}
	
			$body .= "<br />\nLEAD IDS AFTER CHECKING FOR TOO FEW LEADS: $lead_ids<br />\n";
			$body .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />\n";
	
	
			//-- save new lead_id list to record
			$sql_update = "update irelocation.leads_security set lead_ids = '$lead_ids' where quote_id = $quote_id ";
			$body .= "SPECIAL VALIDATION UPDATE SQL: $sql_update <br />\n<br />\n";
			$rs_update = new mysql_recordset($sql_update);
	
	
		}		
	
		//-- END SPECIAL OWNER VALIDATION ======================
	
		$body .= "<br />\nLEAD IDS AFTER ALL SCRUBS AND SPECIAL VALIDATIONS: $lead_ids<br />\n";
	}
	

	

 	function markAsSent($quote_id) {
 		 $sql = "update irelocation.leads_security set ready_to_send = '3' where quote_id = '$quote_id' and (campaign = 'usalarm' || campaign = 'alarmsys') ";
	 	 $rs = new mysql_recordset($sql);
 	}
	 	
	function selectQuotes() {
		
		//-- continue normal processing...
		
		//-- NO Company wants to get renters
#	 $sql = "select quote_id,lead_ids from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1 and own_rent = 'own' ";
		
		//-- OKAY TO PULL RENTERS AGAIN
		$sql = "select quote_id,lead_ids from leads_security where (campaign = 'usalarm' || campaign = 'alarmsys') and lead_ids is not null and ready_to_send = 1 ";

		 $rs = new mysql_recordset($sql);
		 $quote_ids = array();
		 
		 while($rs->fetch_array())
			$quote_ids[] = $rs->myarray['quote_id'];
		 
		 if (count($quote_ids) > 0)
		 {
			 $sql = "update irelocation.leads_security set ready_to_send = 2 where quote_id in (".implode(",",$quote_ids).") and (campaign = 'usalarm' || campaign = 'alarmsys') ";
			 $body .= "Update SQL: ".$sql."<br/>";
			 $rs = new mysql_recordset($sql);
				 
			 return $quote_ids;
		 }
		 else return array();
	}
	
	function loadCompanies()
	{
		$month = date("Ym");
		$sql = "select lead_id from movingdirectory.campaign where site_id = 'usalarm'" .
				" and month = '$month' and active and (cancelled is null or cancelled > NOW())";
		echo "loadCompanies SQL: $sql";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
	 	{
	 	 	$lead_ids[] = $rs->myarray['lead_id'];	
	 	}
	 	return $lead_ids;
	}
	
	//load company classes.
	
	$all_companies = array(	BRINKS => new NewBrinks(),
											GAYLORD => new Gaylord(),
											APX => new APXSecurity(), 
											PINNACLE => new Pinnacle(), 
											FRONTPOINT => new FrontPoint(), 
											SMITHWESSON => new SmithWesson(), 
											CENTRAL => new Central(),
											PREMIUM => new Premium(),
											BESTSECURITY => new BestSecurity(),
											DEFENSEFIRST => new DefenseFirst(),
											SPOTSECURITY => new SpotSecurity(),
											SANTANA => new Santana(),
											PROTECTION_ONE => new ProtectionOne(),
											PROTECT_AMERICA => new ProtectAmerica()
											);
	
	//-- OLD: PROTECTION_ONE => new ProtectionOne()		
	# CENTRALSECURITYALARM => new CentralSecurityAlarm(), 
	
	//all active company lead_ids
	$active_lead_ids = loadCompanies();
	echo "<br />";
	echo "Active Companies:<pre>";
	echo print_r($active_lead_ids,true);
	echo "</pre><br />\n";
	$body .= "<br />\nActive Companies:<pre>";
	$body .= print_r($active_lead_ids,true);
	$body .= "</pre><br />\n";

	
	//where we store the active company objects.
	$active_companies = array();
	
	echo "<br /><br />Checking Lead IDs:<br />";
	//load all the active companies.
	foreach($active_lead_ids as $lead_id)
	{		
		echo "<pre>";
		print_r($all_companies[$lead_id]);
		echo "</pre>";
		$active_companies[$lead_id] = $all_companies[$lead_id];	
	}
	echo "<br /><br />";

	
	$quote_ids = selectQuotes();
	
	if (count($quote_ids) == 0)
	{
		$body .= "No Quotes.";
		echo $body;
		exit();
	}
	//select all those quotes.
	$sql = " select * from irelocation.leads_security where quote_id in (".implode(",",$quote_ids).")";
	$rs = new mysql_recordset($sql);
	$body .= "QUOTE SELECT: $sql<br />\n";
	
	//loop over each one.
	while($rs->fetch_array()) {			
	
		//-- Need to check for dups:
		//-- get yesterday's date, then check the Db to see if there is any leads that have the same PHONE and EMAIL with a READY_TO_SEND = 3 within the last 2 days.
		$twoDaysAgo = date("Ymd",strtotime("2 days ago"));
		$phone1 = $rs->myarray["phone1"];
		$zip = $rs->myarray["zip"];
		$source = $rs->myarray["source"];
		
		$dupcheck = "select count(*) as dupcount from leads_security where left(received,8) >= '$twoDaysAgo' and phone1 = '$phone1' and zip = '$zip' and source = '$source' and ready_to_send = 3  ";
		$rs_dup = new mysql_recordset($dupcheck);
		$rs_dup->fetch_array();
		#mail("code@irelocation.com","DUP CHECK SQL","$dupcheck\n\nQuote ID should be " . $rs->myarray['quote_id']);
		
		if ( $rs_dup->myarray["dupcount"] > 0 ) { //-- uh oh, it's a dup, do not send and update it to RTS = 8
			
			$body .= "FOUND DUP!!!<br />";
			$quoteid = $rs->myarray['quote_id'];
			$rts8 = "update leads_security set ready_to_send = 8 where quote_id = '$quoteid'   ";
			$body .= "RTS DUP SQL: $rts8<br />";
			$rs_rts8 = new mysql_recordset($rts8);
			#mail("code@irelocation.com","FOUND USAlarm Dup - RTS SQL","Updated RTS=8 \n\nQuote ID should be $quoteid");
			
		} else { //-- found no dup, okay to process
			
			$lead_ids = $rs->myarray['lead_ids'];
			$lead_ids = trim($lead_ids,"|");
			$lead_ids = split("\|",$lead_ids);
			
			//-- Send leads to Anna for validation if a specific source id
			if ( eregi("yakov",$rs->myarray['source']) || eregi("hsas",$rs->myarray['source']) || eregi("coregmedia",$rs->myarray['source']) || eregi("abcleads",$rs->myarray['source']) || eregi("prospectiv",$rs->myarray['source'])  || eregi("reply",$rs->myarray['source'])  ) {
				
				$anna_csv = $rs->myarray['quote_id'] . ", " . $rs->myarray['received'] . ", " . $rs->myarray['source'] . ", " . $rs->myarray['name1'] . ", " . $rs->myarray['name2'] . ", " . $rs->myarray['phone1'] . ", " . $rs->myarray['email'] . ", " . $rs->myarray['own_rent'] . ", " . $rs->myarray['campaign'];
				
				$anna_msg = $rs->myarray['quote_id'] . "
" . $rs->myarray['received'] . "
" . $rs->myarray['source'] . "
" . $rs->myarray['name1'] . "
" . $rs->myarray['name2'] . "
" . $rs->myarray['phone1'] . "
" . $rs->myarray['email'] . "
" . $rs->myarray['own_rent'] . "
" . $rs->myarray['campaign'] . "
" . $rs->myarray['address'] . "
" . $rs->myarray['city'] . "
" . $rs->myarray['state_code'] . "
" . $rs->myarray['zip'];
				
				mail("code@irelocation.com,anna@irelocation.com","Affiliate Lead Needing Validation - USALARM", $anna_msg . "\n\n" . $anna_csv);
			} 
					
			//get the count after cleaning brinks.
			//count lead_ids
			$company_count = count($lead_ids);

			if ( $company_count == 0 ) { //-- This condition should never happen, but it is. Need to send me info when this happens and prevent the quotemailer from crashing.
				$quoteid = $rs->myarray['quote_id'];
				
				mail("code@irelocation.com","EMPTY LEAD_IDS VAR IN USALARM","Quote ID = $quoteid");
				$body .= "<br />There was an issue with Quote ID $quoteid - seems it had no lead_ids in the array.<br />";
				$body .= "LEAD_IDS ARRAY IS CURRENTLY:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
				
			} else { //-- continue to process
				
				$myarray = formatData($rs->myarray);
				
				$myarray['company_count'] = $company_count;
				
				//loop through each company that should get this lead.
				$body .= "LEAD_IDS in \$lead_ids:<br />";
				$body .= "<pre>" . print_r($lead_ids,1) . "</pre>";
				$body .= "<br />";
	
				
				foreach($lead_ids as $lead_id) {
					$company = $active_companies[$lead_id];
					$body .= "Now processing $company<br />\n";
		
					//assign it?
					if ($company->assign($myarray)) { //assign the array, and test them.
						$q_id = $myarray['quote_id'];
						$body .= "Looking to send $q_id<br />\n";
		
						$company->send();			
						$body .= $myarray['quote_id']." sent to ".$lead_id."<br/>\n";
						
					} else {
						$q_id = $myarray['quote_id'];
						//mail("code@irelocation.com","Brinks Lead Bounce","This email was generated on line 118 of irelocation.com/usalarm/quotemailer.php \n\n Lead ID: $lead_id doesn't want quote_id: $q_id ");
						$body .= "<font color='#FF0000'>BAD:</font> Lead ID: ".$lead_id." doesn't want quote_id: ".$myarray['quote_id']."<br/>";
						$body .= "If this is happening, then something is wrong or a company changed what leads they want!!!<br/>";				
		
						//-- Update the quote_id and take out the lead_id that doesn't want the lead	
						$sql_rem = " update leads_security set lead_ids = replace(lead_ids,'$lead_id','') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_rem<br />\n";
						$rs_rem = new mysql_recordset($sql_rem);
						
						//-- Remove the occaisional double pipes ( || ) that are created	
						$sql_dp = " update leads_security set lead_ids = replace(lead_ids,'||','|') where quote_id = ".$myarray['quote_id'] ;
						$body .= "Running: $sql_dp<br />\n";
						$rs_dp = new mysql_recordset($sql_dp);
						
						//-- Trim pipes ( | )
						$sql_trim = " update leads_security set lead_ids = trim(BOTH '|' FROM lead_ids ) where quote_id = " .$myarray['quote_id'] ;
						$body .= "Running: $sql_trim<br />\n";
						$rs_trim = new mysql_recordset($sql_trim);
		
		
					}
				}	
				//mark lead when done.
				markAsSent($rs->myarray['quote_id']);
			
			}			
			
		}
		
	}
	
echo $body;
$body = str_replace('<br />','',$body);

$content = ob_get_contents();
ob_end_clean();
echo $content;

$body = str_replace('<br />','\\n',$body);

if(DEBUG_EMAILS)
	mail("code@irelocation.com","USALARM QUOTEMAILER OUTPUT",$content);

?>

