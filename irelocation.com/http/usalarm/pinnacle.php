<?
	function pinnacle_test($state,$zip)
	{
		
		$pinnacle_states = array();
		$pinnacle_states['AZ'] =	array(850, 852, 853);
		$pinnacle_states['CA'] = array(920, 921, 919, 920, 926, 917, 927, 928, 906, 908, 907, 902, 905, 903, 904, 900, 918, 911, 912, 914, 915, 916, 910, 913, 951, 940, 945, 943, 944, 953, 941, 946, 947, 948, 952, 957, 958);
		$pinnacle_states['CO'] = array(800, 802, 803);
		$pinnacle_states['FL'] = array(330, 331, 333, 337, 336);
		$pinnacle_states['IA'] = array(503);
		$pinnacle_states['IL'] = array(603,602, 606, 607, 608, 604, 605);
		$pinnacle_states['IN'] = array(464, 461, 462);
		$pinnacle_states['KS'] = array(661, 662, 672);
		$pinnacle_states['MI'] = array(482, 483);
		$pinnacle_states['MN'] = array(551, 554);
		$pinnacle_states['MO'] = array(641, 630, 631);
		$pinnacle_states['NC'] = array(276, 277, 282);
		$pinnacle_states['NE'] = array(681);
		$pinnacle_states['NM'] = array(871, 880);
		$pinnacle_states['OH'] = array(440, 441, 442, 443, 432);
		$pinnacle_states['OR'] = array(970, 972);
		$pinnacle_states['PA'] = array(190, 191);
		$pinnacle_states['TX'] = array(770, 775, 750, 752, 760, 761, 785);
		$pinnacle_states['UT'] = array(846, 840, 843, 844);
		$pinnacle_states['WA'] = array(980, 981, 984);
		$pinnacle_states['WI'] = array(532, 534);	
	
		$state = strtoupper($state);
		$zips = $pinnacle_states[$state];
		if (is_array($zips) && count($zips) > 0)
		{
			$prefix = substr($zip,0,3);
			$zips = implode(" ",$zips);
			
			//mail("david@irelocation.com","Pinnacle Test","$state $prefix\nZips:$zips");
			
			return (substr_count($zips,$prefix) > 0);		
		}
		else
			return false;	
	}
	
?>

