<script>
/*
Please refer to readme.html for full Instructions

Text[...]=[title,text]

Style[...]=[TitleColor,TextColor,TitleBgColor,TextBgColor,TitleBgImag,TextBgImag,TitleTextAlign,TextTextAlign, TitleFontFace, TextFontFace, TipPosition, StickyStyle, TitleFontSize, TextFontSize, Width, Height, BorderSize, PadTextArea, CoordinateX , CoordinateY, TransitionNumber, TransitionDuration, TransparencyLevel ,ShadowType, ShadowColor]
*/

var FiltersEnabled = 0 // if your not going to use transitions or filters in any of the tips set this to 0

Text[1]=["","<b>Washington"]
Text[2]=["","<b>Oregon"]
Text[3]=["","<b>California"]
Text[4]=["","<b>Alaska"]
Text[5]=["","<b>Idaho"]
Text[6]=["","<b>Nevada"]
Text[7]=["","<b>Utah"]
Text[8]=["","<b>Arizona"]
Text[9]=["","<b>Montana"]
Text[10]=["","<b>Wyoming"]
Text[11]=["","<b>Colorado"]
Text[12]=["","<b>New Mexico"]
Text[13]=["","<b>North Dakota"]
Text[14]=["","<b>South Dakota"]
Text[15]=["","<b>Nebraska"]
Text[16]=["","<b>Kansas"]
Text[17]=["","<b>Oklahoma"]
Text[18]=["","<b>Texas"]
Text[19]=["","<b>Minnesota"]
Text[20]=["","<b>Iowa"]
Text[21]=["","<b>Missouri"]
Text[22]=["","<b>Arkansas"]
Text[23]=["","<b>Louisiana"]
Text[24]=["","<b>Hawaii"]
Text[25]=["","<b>Wisconsin"]
Text[26]=["","<b>Illinois"]
Text[27]=["","<b>Mississippi"]
Text[28]=["","<b>Michigan"]
Text[29]=["","<b>Indiana"]
Text[30]=["","<b>Kentucky"]
Text[31]=["","<b>Tennessee"]
Text[32]=["","<b>Alabama"]
Text[33]=["","<b>Ohio"]
Text[34]=["","<b>Maine"]
Text[35]=["","<b>New Hampshire"]
Text[36]=["","<b>Vermont"]
Text[37]=["","<b>Massachusetts"]
Text[38]=["","<b>Rhode Island"]
Text[39]=["","<b>Connecticut"]
Text[40]=["","<b>New York"]
Text[41]=["","<b>Pennsylvania"]
Text[42]=["","<b>New Jersey"]
Text[43]=["","<b>West Virginia"]
Text[44]=["","<b>Maryland"]
Text[45]=["","<b>Delaware"]
Text[46]=["","<b>Virginia"]
Text[47]=["","<b>North Carolina"]
Text[48]=["","<b>South Carolina"]
Text[49]=["","<b>Georgia"]
Text[50]=["","<b>Florida"]

Text['AK1']=["","Anchorage"]



Text['delete']=["","Delete from Favorites"]

Style[12]=["#000000","#5A4437","#01476b","#edf1fe","","","","","","","","","","",100,"",2,2,10,10,"","","","",""]
Style[2]=["#000000","#5A4437","#CB0202","#edf1fe","","","","","","","","","","",100,"",2,2,10,10,"","","","",""]
applyCssFilter()
</script>
