<?session_start();
if(!isset($_SESSION['userid'])){
		header("Location: index.php");
	}?>
<html><head>
<!-- UTF-8 is the recommended encoding for your pages -->
    <meta http-equiv="content-type" content="text/xml; charset=utf-8" />
    <title>iRelocation Owner Admin</title>

<!-- Loading Theme file(s) -->
    <link rel="stylesheet" href="zpcal/themes/fancyblue.css" />

<!-- Loading Calendar JavaScript files -->
    <script type="text/javascript" src="zpcal/src/utils.js"></script>
    <script type="text/javascript" src="zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="zpcal/src/calendar-setup.js"></script>

<!-- Loading language definition file -->
    <script type="text/javascript" src="zpcal/lang/calendar-en.js"></script>

<link rel="stylesheet" href="../style.css" type="text/css">
<script language="JavaScript">
  function confirm_prompt(text) {
    if (confirm(text)) {
      window.location = 'index.php?action=del_all_mess';
    }
  }
</script>
</head>
<body>
		<table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0">
			<tr>
				<td width="84"><img border="0" src="../images/globe.gif" width="54" height="35"></td>
				<td nowrap><b><img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../index.php">Find a Vacation Home</a></b></td>
				<td class="menu" nowrap><b>
				<img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../about.php">About
				Us</a></b></td>
				<td class="menu" nowrap><b>
				<img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../members/index.php">List
				Your Property</a></b></td>
				<td class="menu" nowrap><b>
				<img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../faqs.php">FAQs</a></b></td>
				<td class="menu" nowrap><b>
				<img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../links.php">Travel
				Links</a></b></td>
				<td class="menu" nowrap><b>
				<img border="0" src="../images/up_right-arrow-white.gif"><a class="menulink" href="../contact.php">Contact
				Us</a></b></td>
			</tr>
		</table>
<h5><?echo $_SESSION['first'] ." ". $_SESSION['last'];?> Property Administration</h5>
<table border="1" width=700 id="table2" cellspacing="0" cellpadding="0" style="border:thin solid navy" align="center"><tr><td valign="top">
<table width=700 border=0><tr><td width=150 valign="top">
<?if($action != 'add' && $action != 'mod2'){
      	$sql = "SELECT * FROM listings l, members m WHERE l.oid = m.mid AND m.mid = '".$_SESSION['mid']."'";
      	$res = mysql_query($sql);
      	$numlisted = mysql_num_rows($res);
      	if($numlisted == 0){
		  	$sql = "SELECT * FROM members m WHERE m.mid = '".$_SESSION['mid']."'";
		  	$res = mysql_query($sql);
		  }
      	$row = mysql_fetch_array($res);
      	$numprop = $row['paidfor'];
      	$remaining = $numprop - $numlisted;

	if($remaining > 0){
	?>
<a class="bluelink" href="index.php?action=add">Add a Rental</a><br>
<?} else {
		echo "<a class=\"bluelink\" href=\"index.php?action=pay\">Add a Rental</a><br>";
}
if($_SESSION['current'] == 2){

?>
	<a class="bluelink" href="index.php?action=renew">Renew a Rental</a><br>
	<a class="bluelink" href="index.php?action=mod">Modify a Rental</a><br>
	<a class="bluelink" href="index.php?action=del">Delete a Rental</a><br>
	<a class="bluelink" href="index.php?action=book">Record Bookings</a><br>
	<a class="bluelink" href="index.php?action=pend">Confirm Bookings</a><br>
	<a class="bluelink" href="index.php?action=available">Make Available</a><br>

<?
}
	$sql = "SELECT * FROM messages WHERE to_id = '".$_SESSION['mid']."' AND deleted = 0 AND `read` = 0";
	//echo $sql;
  $res = mysql_query($sql);
	$num = mysql_num_rows($res);
	//if($num < 1){$num = 0;}
?>
<a class="bluelink" href="index.php?action=view_messages">View Messages</a> (<?=$num?> new)<br>
<a class="redlink" href="javascript:void(0);" onclick="confirm_prompt('Do you really want to delete ALL of your messages?'); return false;">Delete ALL Messages</a>&nbsp;&nbsp;&nbsp;

</td>
<td style="border-left:thin solid navy" width=1>&nbsp;</td>
<td valign="top">
      <?}?>

      <?if($action == 'login_action' || $action == ''){
      	$today = date('Y-m-d');
      	$sql = "SELECT * FROM listings l, members m WHERE l.oid = m.mid AND m.mid = '".$_SESSION['mid']."' AND l.expires >= '$today'";
      	$res = mysql_query($sql);
      	$numlisted = mysql_num_rows($res);
      	if($numlisted == 0){
		  	$sql = "SELECT * FROM members m WHERE m.mid = '".$_SESSION['mid']."'";
		  	$res = mysql_query($sql);
		  }
      	$row = mysql_fetch_array($res);
      	$numprop = $row['paidfor'];
      	if($numprop > 0 && $numlisted < $numprop){
		  	$sql = "SELECT * FROM listings l, members m WHERE l.oid = m.mid AND m.mid = '".$_SESSION['mid']."' AND l.expires <= '$today'";
      		$res = mysql_query($sql);
      		$row = mysql_fetch_array($res);
		  	$numexpired = mysql_num_rows($res);
		  }
      	$remaining = $numprop - $numlisted - $numexpired;

      	if($numlisted == 1){$singural1 = 'property';}else{$singural1 = 'properties';}
      	if($remaining == 1){$singural2 = 'property';}else{$singural2 = 'properties';}
      	if($numprop == 1){$singural3 = 'listing';}else{$singural3 = 'listings';}
      	?>
      <table width=500 border=0>
      <tr>
      <td valign="top" class="admin">
      Welcome to your control panel.  Here you can add, edit and delete properties.  You have paid for <?=$numprop?> property <?=$singural3?>.  Right now
      you have <?=$numlisted.' '.$singural1?> listed.  You are allowed to list <?=$remaining?> more <?=$singural2?> with us.

      </td>
      </tr>
      </table>
      <?}?>
</td><td valign="top">
