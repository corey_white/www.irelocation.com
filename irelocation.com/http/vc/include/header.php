<?session_start();
$row = mysql_fetch_row($res);
//echo $sql;
extract($row);
$keywords = "vacation rentals $city,vacation homes $city,vacation rental $city,vacation home $city,";
?>
<html>
<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title><?echo stripslashes($title).' ';?><?echo $city .' '.$fullstate.' '.$country.' '.$reg;?> - Rent Vacation Homes</title>
<meta name="keywords" content="<?=$keywords?><?=$attractions?>">
<meta name="description" content="<?=$description?>">
<link rel="stylesheet" href="style.css" type="text/css">
<link REL="shortcut icon" HREF="globe.png" TYPE="image/x-icon">
<script Language="JavaScript">
function isEmailAddr(email)
{
  var result = false;
  var theStr = new String(email);
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
	result = true;
  }
  return result;
}

function validRequired(formField,fieldLabel)
{
	var result = true;

	if (formField.value == "")
	{
		alert('Please enter a value for the "' + fieldLabel +'" field.');
		formField.focus();
		result = false;
	}

	return result;
}

function allDigits(str)
{
	return inValidCharSet(str,"0123456789");
}

function inValidCharSet(str,charset)
{
	var result = true;

	// Note: doesn't use regular expressions to avoid early Mac browser bugs
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}

	return result;
}

function validEmail(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

	if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) )
	{
		alert("Please enter a complete email address in the form: yourname@yourdomain.com");
		formField.focus();
		result = false;
	}

  return result;

}

function validNum(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		if (!allDigits(formField.value))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validInt(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var num = parseInt(formField.value,10);
 		if (isNaN(num))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validDate(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var elems = formField.value.split("/");

 		result = (elems.length == 3); // should be three components

 		if (result)
 		{
 			var month = parseInt(elems[0],10);
  			var day = parseInt(elems[1],10);
 			var year = parseInt(elems[2],10);
			result = allDigits(elems[0]) && (month > 0) && (month < 13) &&
					 allDigits(elems[1]) && (day > 0) && (day < 32) &&
					 allDigits(elems[2]) && ((elems[2].length == 2) || (elems[2].length == 4));
 		}

  		if (!result)
 		{
 			alert('Please enter a date in the format MM/DD/YYYY for the "' + fieldLabel +'" field.');
			formField.focus();
		}
	}

	return result;
}

function validateForm(theForm)
{
	// Customize these calls for your form

	// Start ------->
	if (!validRequired(theForm.zip,"Zip Code"))
		return false;

	if (!validEmail(theForm.email,"Email Address",true))
		return false;



	// <--------- End

	return true;
}

function validateForm2(theForm)
{
	// Customize these calls for your form

	// Start ------->
	//if (!validRequired(theForm.city,"City"))
	//	return false;
	if (!validRequired(theForm.state,"State"))
		return false;


	// <--------- End

	return true;
}

</script>
<script language="JavaScript1.2">
<!--
function refresh()
{
    window.location.reload( false );
}
//-->
</script>
<script type="text/javascript">
/***********************************************
* DHTML slideshow script-  � Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice must stay intact for legal use
* Visit http://www.dynamicdrive.com/ for full source code
***********************************************/

var photos=new Array()
var photoslink=new Array()
var which=1

//define images. You can have as many as you want:
<?php
	include 'config.php';
	$id = $_GET['id'];

// Note that !== did not exist until 4.0.0-RC2
$j = 1;
if ($handle = opendir($path2img.$id)) {

   /* This is the correct way to loop over the directory. */
   while (false !== ($file = readdir($handle))) {
	   $eximg = explode('g',$file);
	   if($eximg[0] != 'thim' && $file != '.' && $file != '..'){
       echo "photos[".$j."]=\"homes/$id/$file\"\n";
   	   $j++;
	   }
   }

   closedir($handle);
}
?>

//Specify whether images should be linked or not (1=linked)
var linkornot=0

//Set corresponding URLs for above images. Define ONLY if variable linkornot equals "1"
photoslink[0]=""
photoslink[1]=""
photoslink[2]=""

//do NOT edit pass this line

var preloadedimages=new Array()
for (i=0;i<photos.length;i++){
preloadedimages[i]=new Image()
preloadedimages[i].src=photos[i]
}


function applyeffect(){

}



function playeffect(){

}

function keeptrack(){
window.status="Image "+(which+1)+" of "+photos.length
}


function backward(){
if (which>1){
which--
applyeffect()
document.images.photoslider.src=photos[which]
playeffect()
keeptrack()
}
}

function forward(){
if (which<photos.length-1){
which++
applyeffect()
document.images.photoslider.src=photos[which]
playeffect()
keeptrack()
}
}

function transport(){
window.location=photoslink[which]
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

</script>
<?include 'main.js';?>
</head>

<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<DIV id="TipLayer" style="visibility:hidden;position:absolute;z-index:1000;top:0"></DIV>
<table border="0" width="759" id="table1" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0">
			<tr><?define("ARROW",'<img border=0 src="images/up_right-arrow-white.gif">');
                  define("TDMENU",'<td class="menu" nowrap>');?>
				<td width="84"><img border="0" src="images/globe.gif" width="54" height="35"></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="index.php">Find a Vacation Home</a></b></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="about.php">AboutUs</a></b></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="members/index.php">List Your Property</a></b></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="faqs.php">FAQs</a></b></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="links.php">Travel Links</a></b></td>
				<?=TDMENU.ARROW?><b><a class="menulink" href="contact.php">Contact Us</a></b></td>
			</tr>
		</table>
		<table border="0" width="100%"><tr><td align="right">

		<?$uri = explode('?',$_SERVER['REQUEST_URI']);
		  $uri = explode('/',$uri[0]);
		  if($uri[2] != 'listing.php'){
		?><img border="0" src="images/title.gif" height="30">
		</td><td background="images/search.jpg" style="background-repeat:no-repeat">

		<form action="search.php" method="GET" onsubmit="return validateForm2(this)" id="form2" name="form2">
		 <table border="0" width="100%" id="table7" cellspacing="0" cellpadding="0" class="bold_text">

		 <tr>
         <td rowspan=4><img src="images/spacer.gif" height=56 width=10></td>
		 <td colspan=6><img src="images/spacer.gif" height=4 width=355></td></tr>
		 <tr><input type="hidden" name="filter" value="citystate">
         <td>1. Search by City &amp; State</td>
		 <td colspan=3>2. Search by US Zip to a Distance of:</td></tr>
		 <tr><td><input type="text" name="city" size="10" class="field_search"><select size="1" name="state" class="field_search">
             <option disabled selected value="">State-&gt;</option>
             <option value="AL">AL</option>
             <option value="AK">AK</option>
             <option value="AZ">AZ</option>
             <option value="AR">AR</option>
             <option value="CA">CA</option>
             <option value="CO">CO</option>
             <option value="CT">CT</option>
             <option value="DC">DC</option>
             <option value="DE">DE</option>
             <option value="FL">FL</option>
             <option value="GA">GA</option>
             <option value="HI">HI</option>
             <option value="ID">ID</option>
             <option value="IL">IL</option>
             <option value="IN">IN</option>
             <option value="IA">IA</option>
             <option value="KS">KS</option>
             <option value="KY">KY</option>
             <option value="LA">LA</option>
             <option value="ME">ME</option>
             <option value="MD">MD</option>
             <option value="MA">MA</option>
             <option value="MI">MI</option>
             <option value="MN">MN</option>
             <option value="MS">MS</option>
             <option value="MO">MO</option>
             <option value="MT">MT</option>
             <option value="NE">NE</option>
             <option value="NV">NV</option>
             <option value="NH">NH</option>
             <option value="NJ">NJ</option>
             <option value="NM">NM</option>
             <option value="NY">NY</option>
             <option value="NC">NC</option>
             <option value="ND">ND</option>
             <option value="OH">OH</option>
             <option value="OK">OK</option>
             <option value="OR">OR</option>
             <option value="PA">PA</option>
             <option value="RI">RI</option>
             <option value="SC">SC</option>
             <option value="SD">SD</option>
             <option value="TN">TN</option>
             <option value="TX">TX</option>
             <option value="VI">VI</option>
             <option value="UT">UT</option>
             <option value="VT">VT</option>
             <option value="VA">VA</option>
             <option value="WA">WA</option>
             <option value="WV">WV</option>
             <option value="WI">WI</option>
             <option value="WY">WY</option>
             </select><input class="image"  border="0" src="images/button_go.gif" name="submit1" type="image" border=0>

		<td>             </form>
		<form action="search.php" method="GET" onsubmit="return validateForm(this)" id="form1" name="form1">
		<input type="text" name="zip" size="6" class="field_search">
        <select name="miles" class="field_search">
		<option value=5>5
		<option value=10>10
		<option value=25>25
		<option value=50>50
		</select> miles
		<input  class="image" border="0" src="images/button_go.gif" name="submit2" type="image" border=0></td>
        </tr><input type="hidden" name="filter" value="zip">
        </table>
    	</form>
    	<?}?>
   </td></tr></table>
		</p>
		<table border="0" width="100%" id="table7" cellspacing="1">
			<tr>
				<td valign="top">
