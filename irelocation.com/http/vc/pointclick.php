<?include 'include/header.php'?>

<?include 'config.php'?>
	<table width=100%>
	<tr>
	<td>
	<h1 align="center">Point &amp; Click</h1>
<img src="images/usmap.gif" border="0" usemap="#map" />
<?include 'style.js';?>
<map name="map">
<!-- #$-:Image Map file created by GIMP Imagemap Plugin -->
<!-- #$-:GIMP Imagemap Plugin by Maurits Rijk -->
<!-- #$-:Please do not edit lines starting with "#$" -->
<!-- #$VERSION:2.0 -->
<!-- #$AUTHOR:ruusvuu -->
<area onMouseOver="stm(Text[1],Style[12])" onMouseOut="htm()" shape="poly" coords="65,30,152,49,144,99,110,89,82,91,74,89,73,82,63,78,62,29,61,29" alt="Washington Vacation Rentals" href="pointclickcity.php?state=WA" />
<area onMouseOver="stm(Text[2],Style[12])" onMouseOut="htm()" shape="poly" coords="60,75,40,136,40,142,132,158,135,136,139,127,136,120,148,101,143,97,111,86,81,90,74,90,72,83,66,78,63,78" alt="Oregon Vacation Rentals" href="pointclickcity.php?state=OR" />
<area onMouseOver="stm(Text[3],Style[12])" onMouseOut="htm()" shape="poly" coords="40,143,30,165,36,219,54,264,98,306,134,308,138,290,144,289,138,270,81,198,94,152,42,143" alt="California" href="pointclickcity.php?state=CA" />
<area onMouseOver="stm(Text[4],Style[12])" onMouseOut="htm()" shape="poly" coords="111,333,158,355,162,419,216,461,205,469,156,435,135,425,76,455,36,462,80,439,56,404,71,388,64,360,79,356,78,344,113,334" alt="Alaska" href="pointclickcity.php?state=AK" />
<area onMouseOver="stm(Text[5],Style[12])" onMouseOut="htm()" shape="poly" coords="152,47,142,97,147,103,134,119,138,126,133,156,206,172,209,129,189,129,172,109,172,94,159,68,162,49,155,46" alt="Idaho" href="pointclickcity.php?state=ID" />
<area onMouseOver="stm(Text[6],Style[12])" onMouseOut="htm()" shape="poly" coords="96,152,82,196,137,269,143,254,148,257,166,165,95,151" alt="Nevada" href="pointclickcity.php?state=NV" />
<area onMouseOver="stm(Text[7],Style[12])" onMouseOut="htm()" shape="poly" coords="169,165,203,172,203,187,227,187,222,250,152,245,166,166" href="pointclickcity.php?state=UT" />
<area onMouseOver="stm(Text[8],Style[12])" onMouseOut="htm()" shape="poly" coords="153,245,149,257,144,256,140,269,145,288,135,292,132,310,179,339,210,340,220,251,153,245" href="pointclickcity.php?state=AZ" />
<area onMouseOver="stm(Text[9],Style[12])" onMouseOut="htm()" shape="poly" coords="165,51,160,68,176,95,171,109,177,109,187,128,208,128,210,124,298,129,300,63,167,49" href="pointclickcity.php?state=MO" />
<area onMouseOver="stm(Text[10],Style[12])" onMouseOut="htm()" shape="poly" coords="210,124,203,187,295,194,296,132,212,125" href="pointclickcity.php?state=WY" />
<area onMouseOver="stm(Text[11],Style[12])" onMouseOut="htm()" shape="poly" coords="227,188,221,249,317,253,317,192,227,187" href="pointclickcity.php?state=CO" />
<area onMouseOver="stm(Text[12],Style[12])" onMouseOut="htm()" shape="poly" coords="222,251,207,341,222,342,225,334,245,336,247,333,300,335,302,253,223,248" href="pointclickcity.php?state=NM" />
<area onMouseOver="stm(Text[13],Style[12])" onMouseOut="htm()" shape="poly" coords="299,62,296,113,389,117,391,114,379,65,297,64" href="pointclickcity.php?state=ND" />
<area onMouseOver="stm(Text[14],Style[12])" onMouseOut="htm()" shape="poly" coords="297,114,295,162,377,164,391,168,393,147,391,124,389,118,299,114" href="pointclickcity.php?state=SD" />
<area onMouseOver="stm(Text[15],Style[12])" onMouseOut="htm()" shape="poly" coords="296,162,294,192,317,193,318,208,411,212,391,169,369,163,297,161" href="pointclickcity.php?state=NE" />
<area onMouseOver="stm(Text[16],Style[12])" onMouseOut="htm()" shape="poly" coords="316,207,317,252,424,258,423,227,411,211,317,209" href="pointclickcity.php?state=KS" />
<area onMouseOver="stm(Text[17],Style[12])" onMouseOut="htm()" shape="poly" coords="303,253,302,259,343,263,346,300,389,309,429,310,425,259,303,253" href="pointclickcity.php?state=OK" />
<area onMouseOver="stm(Text[18],Style[12])" onMouseOut="htm()" shape="poly" coords="301,260,300,336,249,333,245,337,266,357,271,374,298,389,304,371,317,373,327,380,337,398,346,408,358,428,398,436,391,413,406,390,409,391,445,373,442,362,447,356,445,342,441,338,438,315,419,309,390,310,346,298,344,265,303,261" href="pointclickcity.php?state=TX" />
<area onMouseOver="stm(Text[19],Style[12])" onMouseOut="htm()" shape="poly" coords="381,66,392,120,395,157,460,150,459,147,439,136,437,119,444,110,443,100,467,75,450,76,429,67,422,72,409,65,381,66" href="pointclickcity.php?state=MN" />
<area onMouseOver="stm(Text[20],Style[12])" onMouseOut="htm()" shape="poly" coords="393,157,393,171,406,204,460,202,474,174,462,163,459,149,395,156" href="pointclickcity.php?state=IA" />
<area onMouseOver="stm(Text[21],Style[12])" onMouseOut="htm()" shape="poly" coords="406,204,422,229,427,265,488,262,484,270,492,271,498,250,482,233,480,222,474,222,458,201,406,201" href="pointclickcity.php?state=MO" />
<area onMouseOver="stm(Text[22],Style[12])" onMouseOut="htm()" shape="poly" coords="427,266,428,312,436,313,440,322,479,320,477,313,491,270,487,268,487,261,426,264" href="pointclickcity.php?state=AR" />
<area onMouseOver="stm(Text[23],Style[12])" onMouseOut="htm()" shape="poly" coords="438,321,439,338,445,344,448,359,442,364,444,370,464,378,471,373,492,382,506,377,511,382,518,378,510,370,513,361,509,361,501,348,475,351,481,331,478,321,436,323" href="pointclickcity.php?state=LA" />
<area onMouseOver="stm(Text[24],Style[12])" onMouseOut="htm()" shape="poly" coords="284,459,280,458,281,442,265,425,250,423,224,409,212,409,210,406,217,402,226,401,252,416,267,422,279,427,300,452,283,460" href="pointclickcity.php?state=HI" />
<area onMouseOver="stm(Text[25],Style[12])" onMouseOut="htm()" shape="poly" coords="445,101,444,111,437,119,437,135,457,146,467,169,504,164,501,149,508,119,500,128,493,109,460,102,457,98,443,101,442,101,443,102" href="pointclickcity.php?state=WI" />
<area onMouseOver="stm(Text[26],Style[12])" onMouseOut="htm()" shape="poly" coords="469,170,475,173,472,183,467,189,460,205,475,223,482,220,483,235,500,250,503,247,507,249,518,221,516,217,507,172,503,164,471,169" href="pointclickcity.php?state=IL" />
<area onMouseOver="stm(Text[27],Style[12])" onMouseOut="htm()" shape="poly" coords="486,286,477,313,481,330,476,351,501,350,506,361,510,355,525,357,515,283,486,287" href="pointclickcity.php?state=MS" />
<area onMouseOver="stm(Text[28],Style[12])" onMouseOut="htm()" shape="poly" coords="466,102,495,110,499,125,506,109,531,101,515,134,524,161,517,173,563,169,574,140,562,127,549,138,551,113,545,98,538,92,524,88,501,99,492,94,497,86,493,84,466,105,467,104,466,104" href="pointclickcity.php?state=MI" />
<area onMouseOver="stm(Text[29],Style[12])" onMouseOut="htm()" shape="poly" coords="510,175,518,219,513,240,531,235,535,228,536,233,551,213,546,172,510,174,511,174,509,175" href="pointclickcity.php?state=IN" />
<area onMouseOver="stm(Text[30],Style[12])" onMouseOut="htm()" shape="poly" coords="553,213,538,232,536,230,532,235,515,239,509,250,503,249,497,258,578,249,592,230,583,218,559,208,553,212" href="pointclickcity.php?state=KY" />
<area onMouseOver="stm(Text[31],Style[12])" onMouseOut="htm()" shape="poly" coords="497,259,488,286,573,275,575,269,602,244,496,259,496,258" href="pointclickcity.php?state=TN" />
<area onMouseOver="stm(Text[32],Style[12])" onMouseOut="htm()" shape="poly" coords="515,283,525,356,533,357,539,354,533,346,575,342,570,325,572,319,554,279,515,284" href="pointclickcity.php?state=AL" />
<area onMouseOver="stm(Text[33],Style[12])" onMouseOut="htm()" shape="poly" coords="545,172,552,212,559,208,585,220,605,192,599,158,598,160,591,160,587,167,571,173,563,167,547,172" href="pointclickcity.php?state=OH" />
<area onMouseOver="stm(Text[34],Style[12])" onMouseOut="htm()" shape="poly" coords="715,33,708,34,699,46,696,72,712,109,717,96,747,66,745,60,740,60,735,55,726,37,715,34,715,33" href="pointclickcity.php?state=ME" />
<area onMouseOver="stm(Text[35],Style[12])" onMouseOut="htm()" shape="poly" coords="697,73,694,76,691,104,695,121,712,112,696,75" href="pointclickcity.php?state=NH" />
<area onMouseOver="stm(Text[36],Style[12])" onMouseOut="htm()" shape="poly" coords="694,79,673,87,685,125,695,124,693,103,694,77,693,76" href="pointclickcity.php?state=VT" />
<area onMouseOver="stm(Text[37],Style[12])" onMouseOut="htm()" shape="poly" coords="687,122,686,137,713,127,718,134,731,131,735,126,730,121,721,125,715,119,715,111,713,111,706,119,688,125" href="pointclickcity.php?state=MA" />
<area onMouseOver="stm(Text[38],Style[12])" onMouseOut="htm()" shape="poly" coords="707,129,711,139,717,134,714,129,708,129" href="pointclickcity.php?state=RI" />
<area onMouseOver="stm(Text[39],Style[12])" onMouseOut="htm()" shape="poly" coords="688,134,689,151,695,143,710,138,707,129,688,136" href="pointclickcity.php?state=CT" />
<area onMouseOver="stm(Text[40],Style[12])" onMouseOut="htm()" shape="poly" coords="670,87,648,96,641,109,641,122,628,129,620,128,611,135,615,138,607,152,664,140,674,148,690,156,703,152,712,141,695,148,689,152,685,123,672,87" href="pointclickcity.php?state=NY" />
<area onMouseOver="stm(Text[41],Style[12])" onMouseOut="htm()" shape="poly" coords="606,150,599,156,609,192,667,177,678,169,673,160,674,157,670,152,672,149,663,139,607,154,605,149" href="pointclickcity.php?state=PA" />
<area onMouseOver="stm(Text[42],Style[12])" onMouseOut="htm()" shape="poly" coords="672,148,671,150,676,156,675,160,680,169,677,178,683,182,688,167,685,158,687,156,676,153" href="pointclickcity.php?state=NJ" />
<area onMouseOver="stm(Text[43],Style[12])" onMouseOut="htm()" shape="poly" coords="604,191,591,215,587,221,592,230,603,234,615,224,620,206,628,206,636,194,644,191,640,188,620,199,621,191,608,195,605,188" href="pointclickcity.php?state=WV" />
<area onMouseOver="stm(Text[44],Style[12])" onMouseOut="htm()" shape="poly" coords="623,188,625,194,643,185,649,190,657,192,658,202,667,204,663,192,676,205,683,199,683,196,676,196,668,178,623,188" href="pointclickcity.php?state=MD" />
<area onMouseOver="stm(Text[45],Style[12])" onMouseOut="htm()" shape="poly" coords="669,177,673,176,682,195,676,196,669,178" href="pointclickcity.php?state=DE" />
<area onMouseOver="stm(Text[46],Style[12])" onMouseOut="htm()" shape="poly" coords="579,245,594,229,602,234,620,224,625,208,630,208,640,191,656,193,655,202,673,208,676,226,639,238,578,248,580,246" href="pointclickcity.php?state=VA" />
<area onMouseOver="stm(Text[47],Style[12])" onMouseOut="htm()" shape="poly" coords="602,243,574,274,586,273,592,268,615,264,621,268,633,268,651,279,659,277,660,267,687,240,681,236,677,227,637,238,601,245,600,245,602,244" href="pointclickcity.php?state=NC" />
<area onMouseOver="stm(Text[48],Style[12])" onMouseOut="htm()" shape="poly" coords="585,276,625,313,644,294,649,277,633,265,620,270,613,265,594,267,588,276" href="pointclickcity.php?state=SC" />
<area onMouseOver="stm(Text[49],Style[12])" onMouseOut="htm()" shape="poly" coords="553,279,572,319,570,327,576,344,625,337,625,314,584,275,554,277" href="pointclickcity.php?state=GA" />
<area onMouseOver="stm(Text[50],Style[12])" onMouseOut="htm()" shape="poly" coords="535,346,541,352,557,352,568,355,572,360,585,356,586,352,590,352,610,368,616,385,650,423,656,419,659,392,624,338,535,346" href="pointclickcity.php?state=FL" />
</map><br>
<?$sql = "SELECT s.*, l.state FROM states s, listings l WHERE l.state = s.abbr GROUP BY s.fullstate ORDER BY s.fullstate ASC";
$res = mysql_query($sql)or die(mysql_error());
while($row = mysql_fetch_array($res)){
	extract($row);
	echo "<a href=\"pointclickcity.php?state=$abbr\">$fullstate</a> &nbsp; &nbsp; ";	
}
?>

	</td>
	</tr>
	</table>
</td>
</tr>
</table>
<?include 'include/footer.php'?>
