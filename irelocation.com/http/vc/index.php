<?session_start();?>
<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rent Vacation Homes</title>
<link rel="stylesheet" href="style.css" type="text/css">
<script Language="javascript">
function isEmailAddr(email)
{
  var result = false;
  var theStr = new String(email);
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
	result = true;
  }
  return result;
}

function validRequired(formField,fieldLabel)
{
	var result = true;

	if (formField.value == "")
	{
		alert('Please enter a value for the "' + fieldLabel +'" field.');
		formField.focus();
		result = false;
	}

	return result;
}

function allDigits(str)
{
	return inValidCharSet(str,"0123456789");
}

function inValidCharSet(str,charset)
{
	var result = true;

	// Note: doesn't use regular expressions to avoid early Mac browser bugs
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}

	return result;
}

function validEmail(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

	if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) )
	{
		alert("Please enter a complete email address in the form: yourname@yourdomain.com");
		formField.focus();
		result = false;
	}

  return result;

}

function validNum(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		if (!allDigits(formField.value))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validInt(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var num = parseInt(formField.value,10);
 		if (isNaN(num))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validDate(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var elems = formField.value.split("/");

 		result = (elems.length == 3); // should be three components

 		if (result)
 		{
 			var month = parseInt(elems[0],10);
  			var day = parseInt(elems[1],10);
 			var year = parseInt(elems[2],10);
			result = allDigits(elems[0]) && (month > 0) && (month < 13) &&
					 allDigits(elems[1]) && (day > 0) && (day < 32) &&
					 allDigits(elems[2]) && ((elems[2].length == 2) || (elems[2].length == 4));
 		}

  		if (!result)
 		{
 			alert('Please enter a date in the format MM/DD/YYYY for the "' + fieldLabel +'" field.');
			formField.focus();
		}
	}

	return result;
}

function validateForm(theForm)
{
	// Customize these calls for your form

	// Start ------->
	if (!validRequired(theForm.zip,"Zip Code"))
		return false;

	if (!validEmail(theForm.email,"Email Address",true))
		return false;



	// <--------- End

	return true;
}

function validateForm2(theForm)
{
	// Customize these calls for your form

	// Start ------->
	//if (!validRequired(theForm.city,"City"))
		//return false;
	if (!validRequired(theForm.state,"State"))
		return false;


	// <--------- End

	return true;
}

</script>
</head>

<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">
<div style="position:absolute;top:30px;left:540px;width:200px" align="right">
        <?if(!isset($_SESSION['mid'])){?>
		<form action="members/index.php?action=login_action" method="POST">
		<table>
	    <tr><td><input type="text" name="userid" size=9 value="Username"></td>
        <td><input type="password" name="passwd" size=9 value="******"></td>
		<td><input type="submit" value="Login"></td></tr></form>

		<tr><td align="right" colspan=3>
		                        <NOSCRIPT>
														<a target="new" href="members/register.php?action=register">
														</NOSCRIPT>
														<script>
														<!--
															document.write("<a href=\"javascript:void(0);\" onClick=\"window.open('members/register.php?action=register','','width=600,height=500,scrollbars=yes,toolbars=no');\">");
														-->
														</script>

		Register</a>&nbsp;&nbsp;&nbsp;</td></tr></table>
		<?} else {echo "You are logged in as ".$_SESSION['first'];}?>
</div>
<table border="0" width="759" id="table1" cellspacing="0" cellpadding="0">
<tr>
<td>
    <table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0">
    <tr>
    <td width="84"><img border="0" src="images/globe.gif" width="54" height="35"></td>
    <td><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="index.php">Find a Vacation Home</a></b></td>
    <td class="menu"><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="about.php">About Us</a></b></td>
	<td class="menu"><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="members/index.php">List Your Property</a></b></td>
    <td class="menu"><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="faqs.php">FAQs</a></b></td>
    <td class="menu"><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="links.php">Travel Links</a></b></td>
    <td class="menu"><b><img border="0" src="images/up_right-arrow-white.gif"><a class="menulink" href="contact.php">Contact Us</a></b></td>
    </tr>
    </table>
    <table border="0" width="100%" id="table3" cellspacing="0" cellpadding="0" height="274">
    <tr>
    <td background="images/rent-vacation-homes.gif" height="274">
    <div style="position: absolute; width: 236px; height: 135px; z-index: 1; left: 55px; top: 117px" id="SearchBox">
    <form action="search.php" method="GET" onsubmit="return validateForm2(this)" id="form2" name="form2">
         <table border="0" width="100%" id="table7" cellspacing="1" cellpadding="0" class="bold_text">
         <tr>
         <td colspan=3>1. Search by City &amp; State</td>
         </tr>
         <tr><input type="hidden" name="filter" value="citystate">
         <td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="city" size="10" class="field_search"></td>
             <td valign="top"> <select size="1" name="state" class="field_search">
             <option disabled selected value="">State-&gt;</option>
             <option value="AL">AL</option>
             <option value="AK">AK</option>
             <option value="AZ">AZ</option>
             <option value="AR">AR</option>
             <option value="CA">CA</option>
             <option value="CO">CO</option>
             <option value="CT">CT</option>
             <option value="DC">DC</option>
             <option value="DE">DE</option>
             <option value="FL">FL</option>
             <option value="GA">GA</option>
             <option value="HI">HI</option>
             <option value="ID">ID</option>
             <option value="IL">IL</option>
             <option value="IN">IN</option>
             <option value="IA">IA</option>
             <option value="KS">KS</option>
             <option value="KY">KY</option>
             <option value="LA">LA</option>
             <option value="ME">ME</option>
             <option value="MD">MD</option>
             <option value="MA">MA</option>
             <option value="MI">MI</option>
             <option value="MN">MN</option>
             <option value="MS">MS</option>
             <option value="MO">MO</option>
             <option value="MT">MT</option>
             <option value="NE">NE</option>
             <option value="NV">NV</option>
             <option value="NH">NH</option>
             <option value="NJ">NJ</option>
             <option value="NM">NM</option>
             <option value="NY">NY</option>
             <option value="NC">NC</option>
             <option value="ND">ND</option>
             <option value="OH">OH</option>
             <option value="OK">OK</option>
             <option value="OR">OR</option>
             <option value="PA">PA</option>
             <option value="RI">RI</option>
             <option value="SC">SC</option>
             <option value="SD">SD</option>
             <option value="TN">TN</option>
             <option value="TX">TX</option>
             <option value="VI">VI</option>
             <option value="UT">UT</option>
             <option value="VT">VT</option>
             <option value="VA">VA</option>
             <option value="WA">WA</option>
             <option value="WV">WV</option>
             <option value="WI">WI</option>
             <option value="WY">WY</option>
             </select></td>
		<td><input  class="image" border="0" src="images/button_go.gif" name="submit1" type="image"  border=0></td>
        </tr>
    </form>
    <form action="search.php" method="GET" onsubmit="return validateForm(this)" id="form1" name="form1">
        <tr>
        <td height="8"><img border="0" src="images/clear.gif" width="1" height="1"></td>
        </tr><input type="hidden" name="filter" value="zip">
        <tr>
        <td>2. Search by US Zip</td><td colspan=2>to a Distance of:</td>
        </tr>
        <tr>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" name="zip" size="10" class="field_search"></td>
        <td>&nbsp;&nbsp;<select name="miles" class="field_search">
		<option value=5>5
		<option value=10>10
		<option value=25>25
		<option value=50>50
		</select> miles</td>
        <td colspan=2><input class="image" border="0" src="images/button_go.gif" name="submit2" type="image"></td>
        </tr>
        </form>
        <tr>
        <td height="8"><img border="0" src="images/clear.gif" width="1" height="1"></td>
        </tr>
        <tr>
        <td colspan=2>3. <a class="white" href="pointclick.php">Point-n-Click</a></td>
        </tr>
         <tr>
        <td height="8" colspan=5 align="center"><img border="0" src="images/clear.gif" width="90%" height="1"></td>
        </tr>
        <tr>
        <td colspan=5>

		</td>
        </tr>
        </table>

    </div>
    <!-- <div style="position: absolute; width: 101px; height: 44px; z-index: 3; left: 178px; top: 229px" id="Click">
         <a href="pointclick.php"><img border=0 src="images/white_map.gif" width="82" height="46"></a>
	</div>
    <div style="position: absolute; width: 101px; height: 44px; z-index: 3; left: 72px; top: 233px" id="Msg">
    <a class="color_text" href="search.php">Find your home by clicking on our interactive maps!</a>
    </div> -->
    </td>
    </tr>
    </table>
    <table border="0" width="100%" id="table4" cellspacing="0" cellpadding="10" bgcolor="#E2E2E2">
    <tr>
    <td>
         <table border="0" width="100%" id="table5" cellspacing="1">
         <tr>
         <td width="36%" valign="top">
		 <div style="position: absolute; width: 100px; height: 100px; z-index: 2; left: 104px; top: 366px" id="Map"><img border="0" src="images/world.gif" width="166" height="95">
		 </div>
         <p align="center"><img border="0" src="images/title_more-destinations-worldwide.gif" width="222" height="17"></p>
         <p><b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=1">United States</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=2">Europe</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=3">Mexico</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=4">Caribbean</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=5">Canada</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=6">South America</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=7">Australia</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=8">Asia</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=9">Africa</a></b><br>
            <b><img border="0" src="images/up_right-arrow-grey.gif" width="14" height="8"><a class="menulink" href="search-intl.php?region=11">South Pacific</a></b><br>
         </td>
         <td width="2%"><img border="0" src="images/separator.gif" width="3" height="150"></td>
         <td class="regular_text" width="30%" valign="top">
             <img border="0" src="images/title_find-a-vacation-home-rental.gif" width="164" height="15"><br>
             Search thousands of vacation homes worldwide by using the
             search tools above, or by clicking on a link below. Then,
             compare vacation homes and contact the owner directly.<p>
             <img border="0" src="images/title_owners-property-managers.gif" width="162" height="17"><br>
             Join the fastest growing vacation home rental site on the
             Internet FREE for 30 days! Get started instantly online,
             or call us and a representative will gladly assist you.</p>
         </td>
         <td width="2%"><img border="0" src="images/separator.gif" width="3" height="150"></td>
         <td class="regular_text" width="30%" valign="top">
              <img border="0" src="images/title_why-choose-a-vacation-home.gif" width="171" height="17"><br>
              <b><font color="#01476B">1. Value: </font></b>One or two
              vacation homes cost less than 4 to 10 hotel rooms.<br>
              <br>
              <b><font color="#01476B">2. Privacy: </font></b>Vacation
              rentals offer you space and freedom so that you can truly
              enjoy your vacation. <br>
              <br>
              <b><font color="#01476B">3. Family: </font></b>Vacation
              homes accommodate families and groups much better than a
              resort, and the cost per person is often far less.
		 </td>
         </tr>
         </table>
     </td>
     </tr>
     </table>
     <p>&nbsp;</p>
     <table border="0" width="100%" id="table6" cellspacing="0" cellpadding="0" bgcolor="#01476B" height="3">
     <tr>
     <td><img border="0" src="images/clear.gif" width="1" height="1"></td>
     </tr>
     </table>
     <table border="0" width="100%" id="table6" cellspacing="0" cellpadding="0">
     <tr>
     <td class="light_text"><p align="center">Copyright 2005 The iRelocation Network.&nbsp;
                                All Rights Reserved.&nbsp; Privacy Policy&nbsp; Disclaimer</p>
     </td>
     </tr>
     </table>
</td>
</tr>
</table>
<p>&nbsp;</p>

</body>

</html>
