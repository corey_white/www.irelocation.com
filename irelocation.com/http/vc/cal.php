<head>
<style>
table,td    {border:thin solid #FFBE60;font-family:arial,sans-serif;font-size:12px;}
.thcell         {font-family:arial,sans-serif;font-size:12px;font-weight:bold;text-align:center;background-color:navy;color:white}
A.bluelink 		{text-decoration: none;font-family:arial,sans-serif;font-size:13px;font-weight: bold;color: #01476B;}
A.bluelink:hover {background-color: #FFBE60;}
</style>
</head>
<body>
<table>
<tr>
<?
if($_GET['m'] == ''){
	$m = date('m');
	$y = date('y');
}
else{
	$m = date('m',mktime(0,0,0,$_GET['m'],1,date('Y')));
	$y = date('Y',mktime(0,0,0,$_GET['m'],1,$_GET['y']));	
}
	$i = $m+11;
	$cols = 1;
    while($m <= $i){
	    echo "<td valign='top'>";
	    mk_drawCalendar($m,$y);
	    echo "</td>";
	    if($cols == 4){
			echo "</tr><tr>";
			$cols = 0;    
		}
	    $m++;
	    $cols++;
	}

###########################################################
## 	  DRAW CALENDAR
##
##    Draws out a calendar (in html) of the month/year
##    passed to it date passed in format mm-dd-yyyy
###########################################################
function mk_drawCalendar($m,$y)
{
	$hicontrast = $_GET['hicontrast'];
    if ((!$m) || (!$y))
    {
        $m = date("m",mktime());
        $y = date("Y",mktime());
    }

    /*== get what weekday the first is on ==*/
    $tmpd = getdate(mktime(0,0,0,$m,1,$y));
    $month = $tmpd["month"];
    $firstwday= $tmpd["wday"];

    $lastday = mk_getLastDayofMonth($m,$y);

?>

<table cellpadding=2 cellspacing=0 border=1>
<tr><td colspan=7>
    <table cellpadding=0 cellspacing=0 border=0 width="100%">
    <tr><th width="20"></th>
    <th><font size=2><?="$month $y"?></font></th>
    <th width="20"></th>
    </tr></table>
</td></tr>
<tr><td width=22 class="thcell">Su</td><td width=22 class="thcell">M</td>
    <td width=22 class="thcell">T </td><td width=22 class="thcell">W</td>
    <td width=22 class="thcell">Th</td><td width=22 class="thcell">F</td>
    <td width=22 class="thcell">Sa</td></tr>
<?  $d = 1;
    $wday = $firstwday;
    $firstweek = true;
    
    // Database connection
    //include 'config.php';
    mysql_connect('localhost', 'rwhitney', 'tah74#0');                                                                                                   
    mysql_select_db('vacationhomes');
    // Populate array of month days that will indicate if each day is booked or not
    $booked_days = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);    

    // Query database of any records that have booked days in this month
    //$sql = "SELECT * FROM calendar WHERE pid = '".$_GET['id']."' AND status = 'B'";
     // First day of month                                                                                                                     
    $month_start = "$y-$m-01";                                                                                                                
    //echo "Month Start: $month_start<br>";                                                                                                     
                                                                                                                                              
    // Last day of month                                                                                                                      
    $res = mysql_query("SELECT DATE_SUB(DATE_ADD('$month_start', INTERVAL 1 MONTH), INTERVAL 1 DAY)");                                        
    $row = mysql_fetch_row($res);                                                                                                             
    $month_end = $row[0];                                                                                                                     
    //echo "Month End: $month_end<br>";                                                                                                         
                                                                                                                                              
    // Query database of any records that have booked days in this month                                                                      
    $sql = "SELECT IF ( start BETWEEN '$month_start' AND '$month_end',start,'$month_start' ) as start,                                        
                   IF ( end   BETWEEN '$month_start' AND '$month_end',end,  '$month_end')    as end                                           
              FROM calendar                                                                                                                   
             WHERE start < '$month_end'                                                                                                       
               AND end >= '$month_start'                                                                                                      
               AND pid = '".$_GET['id']."' AND status = 'B'"; 
    $res = mysql_query($sql);
    while($row = mysql_fetch_array($res)) {  
        $s = $row["start"];
        $e = $row["end"];
        $sa = explode('-',$s);
        $ea = explode('-',$e);
        for ( $i=$sa[2]; $i<=$ea[2]; $i++ ) { $booked_days[$i-1] = 1; }
    }
    
    // Query database of any records that have booked days in this month                                                                      
    $sql = "SELECT IF ( start BETWEEN '$month_start' AND '$month_end',start,'$month_start' ) as start,                                        
                   IF ( end   BETWEEN '$month_start' AND '$month_end',end,  '$month_end')    as end                                           
              FROM calendar                                                                                                                   
             WHERE start < '$month_end'                                                                                                       
               AND end >= '$month_start'                                                                                                      
               AND pid = '".$_GET['id']."' AND status = 'P'"; 
    $res = mysql_query($sql);
    while($row = mysql_fetch_array($res)) {  
        $s = $row["start"];
        $e = $row["end"];
        $sa = explode('-',$s);
        $ea = explode('-',$e);
        for ( $i=$sa[2]; $i<=$ea[2]; $i++ ) { $booked_days[$i-1] = 2; }
    }

    /*== loop through all the days of the month ==*/
    while ( $d <= $lastday)
    {
        /*== set up blank days for first week ==*/
        if ($firstweek) {
            print "<tr>";
            for ($i=1; $i<=$firstwday; $i++) { print "<td><font size=2></font></td>"; }
            $firstweek = false;
        }

        /*== Sunday start week with <tr> ==*/
        if ($wday==0) { print "<tr>"; }

        /*== check for event ==*/
        if ($booked_days[$d-1] == 1 && $hicontrast == 1) {
            $bg = '#555555';$font = '<font color="#FFFFFF">';
    	} elseif ($booked_days[$d-1] == 1){
        	$bg = '#FFCCCC';$font = '<font color="#000000">';
	    } elseif ($booked_days[$d-1] == 2 && $hicontrast == 1){
            $bg = '#BBBBBB';$font = '<font color="#000000">';
        } elseif ($booked_days[$d-1] == 2){
            $bg = '#FFFFCC';$font = '<font color="#000000">';
        } elseif ($hicontrast == 1){
    		$bg = '#FFFFFF';$font = '<font color="#000000">';
	    } else { $bg = '#EDF1FE'; }

        print "<td class='tcell' align='center' bgcolor=$bg>";
        print $font . $d;
        print "</td>\n";

        /*== Saturday end week with </tr> ==*/
        if ($wday==6) { print "</tr>\n"; }

        $wday++;
        $wday = $wday % 7;
        $d++;
    }

/*== end drawCalendar function ==*/
echo "</table>";
}




/*== get the last day of the month ==*/
function mk_getLastDayofMonth($mon,$year)
{
    for ($tday=28; $tday <= 31; $tday++)
    {
        $tdate = getdate(mktime(0,0,0,$mon,$tday,$year));
        if ($tdate["mon"] != $mon)
        { break; }

    }
    $tday--;

    return $tday;
}

?>
</tr>
</table>
<div align="center">
<a class="bluelink" href="<?=$SCRIPT_NAME?>?m=1&y=<?=$y-1?>&id=<?=$_GET['id']?>&hicontrast=<?=$hicontrast?>"><?=$y-1;?></a> &nbsp; &nbsp; 
<?if($hicontrast != 1){?>
<a class="bluelink" href="<?=$_SERVER['REQUEST_URI']?>&hicontrast=1">View in High Contrast</a>
<?} else {?>
<a class="bluelink" href="<?=$_SERVER['REQUEST_URI']?>&hicontrast=0">View in Normal Colors</a>
<?}?>
 &nbsp; &nbsp; 
<a class="bluelink" href="<?=$SCRIPT_NAME?>?m=1&y=<?=$y+1?>&id=<?=$_GET['id']?>&hicontrast=<?=$hicontrast?>"><?=$y+1;?></a>
</div>