<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>Rent Vacation Homes</title>
<style>
A.white {text-decoration: none;font-family:arial,sans-serif;font-size:12px;font-weight: bold;color: #FFFFFF;}
A.menulink {padding-right:5px;text-decoration: none;font-family:arial,sans-serif;font-size:12px;font-weight: bold;color: #404040;}
A.menulink:hover {background-color: #FFBE60;}
A.smalllink {text-decoration: none;font-family:arial,sans-serif;font-size:11px;color: #404040;}
A.smalllink:hover {background-color: #FFBE60;}
A.bluelink {text-decoration: none;font-family:arial,sans-serif;font-size:13px;font-weight: bold;color: #01476B;}
A.bluelink:hover {background-color: #FFBE60;}
.regular_text {text-decoration: none;font-family:arial,sans-serif;font-size:11px;color: #202020;}
.large_text {text-decoration: none;font-family:arial,sans-serif;font-size:13px;color: #202020;}
.light_text {text-decoration: none;font-family:arial,sans-serif;font-size:10px;color: #474747;}
.bold_text {text-decoration: none;font-family:arial,sans-serif;font-size:13px;font-weight: bold;color: #EDF1FE;}
.color_text {text-decoration: none;font-family:arial,sans-serif;font-size:10px;color: #FFBE60;}
.field_search {font-family: Arial,sans-serif;font-size: 11px;font-weight: bold;border-style: solid;border-width: 1px;background-color: #EDF1FE;}
.field_filter {font-family: Arial,sans-serif;font-size: 12px;border-style: solid;border-width: 1px;}
h1			  {font-family:arial,sans-serif;font-size:24px;color:#01476B;background-color:#EDF1FE;border:thin solid navy;
			   font-stretch:extra-expanded;width:100%;text-align:center}
h2			  {font-family:arial,sans-serif;font-size:24px;color:#01476B}
h3			  {font-family:arial,sans-serif;font-size:20px;color:#01476B}
h4			  {font-family:arial,sans-serif;font-size:16px;color:#01476B}
h5			  {font-family:arial,sans-serif;font-size:12px;color:#01476B;background-color:#EDF1FE;border:thin solid navy;
			   font-stretch:extra-expanded;width:100%;text-align:center}
.details	{font-family:arial,sans-serif;font-size:10px;color:#01476B}
A.goldlink {color: #FFBE60;text-decoration: none;font-family:arial,sans-serif;font-size:13px;font-weight: bold;}
A.redlink {color: #cb0202;text-decoration: none;font-family:arial,sans-serif;font-size:13px;font-weight: bold;}
A.favlink {text-decoration: none;font-family:arial,sans-serif;font-size:10px;font-weight: bold;color: #01476B;}
A.delete {text-decoration: none;font-family:arial,sans-serif;font-size:10px;font-weight: bold;color: #01476B;}
A.favlink:hover {background-color: #FFBE60;}
A.admin {text-decoration: none;font-family:arial,sans-serif;font-size:12px;font-weight: bold;color: #01476B;}
A.admin:hover {background-color: #FFBE60;}
input.admin,textarea.admin,select.admin {border:thin solid navy;background-color:#EDF1FE}
input.image {border:none;}
input,textarea,select {border:thin solid navy;background-color:#EDF1FE}
.red {color:red}
body {text-decoration: none;font-family:arial,sans-serif;font-size:11px;color: #202020;background-color:#FFFFFF}
td {font-family:arial,verdana,sans-serif;font-size:12px;}
tr.bold {font-weight:bold}
td.admin {font-family:arial,sans-serif;font-size:12px;}
table.img {display:inline}
.gold     {background-color:#FFBE60}
.ltblue   {background-color:#EDF1FE}
a.unreadmessages  {background-color:#EDF1FE}
</style>
<script language="JavaScript">
  function confirm_prompt() {
    if (confirm('Do you really want to delete this user?')) {
      window.location = 'index.php?a=delopaction';
    }
  }
</script>
</head>

<body topmargin="0" leftmargin="0" marginwidth="0" marginheight="0">

<table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<table border="0" width="100%" id="table7" cellspacing="1">
			<tr>
				<td valign="top">
