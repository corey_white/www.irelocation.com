<?
    extract($_GET);
    $m = (!$m) ? date("m",mktime()) : "$m";
    $y = (!$y) ? date("Y",mktime()) : "$y";

    if ($REQUEST_METHOD == "POST")
    {
    print "<h2>Calendar Menu</h2>";
    print "... not really adding the data to a calendar, but you could  ...<br>";
        print "Event: $event<br>";
        print "Date: $eventdate<br>";
        exit();
    }
    
 mk_drawCalendar($m,$y);


//*********************************************************
// DRAW CALENDAR
//*********************************************************
/*
    Draws out a calendar (in html) of the month/year
    passed to it date passed in format mm-dd-yyyy
*/
function mk_drawCalendar($m,$y)
{
    if ((!$m) || (!$y))
    {
        $m = date("m",mktime());
        $y = date("Y",mktime());
    }

    /*== get what weekday the first is on ==*/
    $tmpd = getdate(mktime(0,0,0,$m,1,$y));
    $month = $tmpd["month"];
    $firstwday= $tmpd["wday"];

    $lastday = mk_getLastDayofMonth($m,$y);


    
    $background = '#F8EF94';
    $text = '#000000';
    $rowone = '#F8EF94';
    $rowtwo = '#F8EF94';
    $monthtext = '#000000';
    $bodytext = '#000000';
?>
<style>
a.h2            {font-family:arial,sans-serif;font-size:12px;color:<?print($bodytext)?>}
a               {font-family:arial,helvetica;font-size:12px;color:<?print($bodytext)?>;text-decoration:none}
.month          {font-family:arial,sans-serif;font-size:12px;font-weight:bold;background-color:<?print($month)?>;color:<?print($monthtext);?>}
.year           {font-family:arial,sans-serif;font-size:12px;font-weight:bold;background-color:<?print($month)?>;color:<?print($monthtext);?>}
a.month         {font-family:arial,sans-serif;font-size:12px;font-weight:bold;background-color:<?print($month)?>;color:<?print($monthtext);?>}
.white          {background-color:<?print($rowone)?>;color:<?print($text)?>;font-family:sans-serif;font-size:12px}
.yellow         {background-color:<?print($rowtwo)?>;color:<?print($text)?>;font-family:sans-serif;font-size:12px}
table,td        {border:thin solid gold;font-family:arial,sans-serif;font-size:12px;}
.thcell         {font-family:arial,sans-serif;font-size:12px;font-weight:bold;text-align:center;background-color:navy;color:white}
</style>
<table cellpadding=2 cellspacing=0 border=1>
<tr><td colspan=7>
    <table cellpadding=0 cellspacing=0 border=0 width="100%">
    <tr><th width="20"><a href="<?=$SCRIPT_NAME?>?m=<?=(($m-1)<1) ? 12 : $m-1 ?>&y=<?=(($m-1)<1) ? $y-1 : $y ?>">&lt;&lt;</a></th>
    <th><font size=2><?="$month $y"?></font></th>
    <th width="20"><a href="<?=$SCRIPT_NAME?>?m=<?=(($m+1)>12) ? 1 : $m+1 ?>&y=<?=(($m+1)>12) ? $y+1 : $y ?>">&gt;&gt;</a></th>
    </tr></table>
</td></tr>
<tr><td width=22 class="thcell">Su</td><td width=22 class="thcell">M</td>
    <td width=22 class="thcell">T </td><td width=22 class="thcell">W</td>
    <td width=22 class="thcell">Th</td><td width=22 class="thcell">F</td>
    <td width=22 class="thcell">Sa</td></tr>
<?  $d = 1;
    $wday = $firstwday;
    $firstweek = true;
    
    // Database connection
    include 'config.php';
    //mysql_connect('127.0.0.1', 'root', '');
    //mysql_select_db('richard');
    
    // Populate array of month days that will indicate if each day is booked or not
    $booked_days = array(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);    

    // Query database of any records that have booked days in this month
    $sql = "SELECT * FROM calendar WHERE pid = '".$_GET['id']."'";
    $res = mysql_query($sql);
    while($row = mysql_fetch_array($res)) {  
        $s = $row["start"];
        $e = $row["end"];
        $sa = explode('-',$s);
        $ea = explode('-',$e);
        for ( $i=$sa[2]; $i<=$ea[2]; $i++ ) { $booked_days[$i-1] = 1; }
    }

    /*== loop through all the days of the month ==*/
    while ( $d <= $lastday)
    {
        /*== set up blank days for first week ==*/
        if ($firstweek) {
            print "<tr>";
            for ($i=1; $i<=$firstwday; $i++) { print "<td><font size=2>&nbsp;</font></td>"; }
            $firstweek = false;
        }

        /*== Sunday start week with <tr> ==*/
        if ($wday==0) { print "<tr>"; }

        /*== check for event ==*/
        if ($booked_days[$d-1] == 1) {
            $bg = 'red';
        } else {
            $bg = 'white';
        }

        print "<td class='tcell' align='center' bgcolor=$bg>";
        print "$d";
        print "</td>\n";

        /*== Saturday end week with </tr> ==*/
        if ($wday==6) { print "</tr>\n"; }

        $wday++;
        $wday = $wday % 7;
        $d++;
    }

/*== end drawCalendar function ==*/
}




/*== get the last day of the month ==*/
function mk_getLastDayofMonth($mon,$year)
{
    for ($tday=28; $tday <= 31; $tday++)
    {
        $tdate = getdate(mktime(0,0,0,$mon,$tday,$year));
        if ($tdate["mon"] != $mon)
        { break; }

    }
    $tday--;

    return $tday;
}

?>

