<link rel="stylesheet" href="../style.css" type="text/css">
<?
switch($action){
	case 'register':register();break;
	case 'register_action':register_action();break;
}

function register(){?>
<script Language="JavaScript">
function isEmailAddr(email)
{
  var result = false;
  var theStr = new String(email);
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
	result = true;
  }
  return result;
}

function validRequired(formField,fieldLabel)
{
	var result = true;

	if (formField.value == "")
	{
		alert('Please enter a value for the "' + fieldLabel +'" field.');
		formField.focus();
		result = false;
	}

	return result;
}

function allDigits(str)
{
	return inValidCharSet(str,"0123456789");
}

function inValidCharSet(str,charset)
{
	var result = true;

	// Note: doesn't use regular expressions to avoid early Mac browser bugs
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}

	return result;
}

function validEmail(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

	if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) )
	{
		alert("Please enter a complete email address in the form: yourname@yourdomain.com");
		formField.focus();
		result = false;
	}

  return result;

}

function validNum(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		if (!allDigits(formField.value))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validInt(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var num = parseInt(formField.value,10);
 		if (isNaN(num))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validDate(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var elems = formField.value.split("/");

 		result = (elems.length == 3); // should be three components

 		if (result)
 		{
 			var month = parseInt(elems[0],10);
  			var day = parseInt(elems[1],10);
 			var year = parseInt(elems[2],10);
			result = allDigits(elems[0]) && (month > 0) && (month < 13) &&
					 allDigits(elems[1]) && (day > 0) && (day < 32) &&
					 allDigits(elems[2]) && ((elems[2].length == 2) || (elems[2].length == 4));
 		}

  		if (!result)
 		{
 			alert('Please enter a date in the format MM/DD/YYYY for the "' + fieldLabel +'" field.');
			formField.focus();
		}
	}

	return result;
}

function validateForm(theForm)
{
	// Customize these calls for your form

	// Start ------->
	if (!validRequired(theForm.first,"First Name"))
		//return false;
	if (!validRequired(theForm.last,"Last Name"))
		//return false;
	if (!validRequired(theForm.phone,"Phone"))
		//return false;
	if (!validEmail(theForm.email,"Email Address",true))
		return false;
	if (!validRequired(theForm.login,"Username"))
		//return false;
	if (!validRequired(theForm.passwd,"Password"))
		//return false;




	// <--------- End

	return true;
}



</script><b></b>
<?
  	define("AST","<span style=\"font-size:16px;color:red\">*</span>");
	echo "<table align=\"center\" style=\"border: thin solid navy\" border=0 width=600>";
	echo "<tr><td>";
	echo "<h1 align=\"center\"><img src=\"../images/title.gif\"><br>
		 Register as Rental Owner</h1><h3 align=\"center\">All fields marked with an asterisk (".AST.") are required</h3>";
	echo "<form enctype=\"multipart/form-data\" action=".$_SERVER['PHP_SELF']."?action=register_action method=POST onsubmit=\"return validateForm(this)\" id=\"form1\" name=\"form1\">";
	echo "<table align=\"center\">";
	echo "<tr class=\"details\"><td align=\"right\">".AST."First Name:</td><td><input type=\"text\" name=\"first\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">".AST."Last Name:</td><td><input type=\"text\" name=\"last\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">".AST."Phone:</td><td><input type=\"text\" name=\"phone\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">Cell:</td><td><input type=\"text\" name=\"cell\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">Fax:</td><td><input type=\"text\" name=\"fax\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">".AST."Email:</td><td><input type=\"text\" name=\"email\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td colspan=2>Payments Accepted:</td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">Visa:</td><td><input type=\"checkbox\" name=\"visa\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">MC:</td><td><input type=\"checkbox\" name=\"mc\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">Discover:</td><td><input type=\"checkbox\" name=\"disc\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">AMEX:</td><td><input type=\"checkbox\" name=\"amex\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">ECheck:</td><td><input type=\"checkbox\" name=\"echeck\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">Paypal:</td><td><input type=\"checkbox\" name=\"paypal\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">".AST."Username:</td><td><input type=\"text\" name=\"login\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td align=\"right\">".AST."Password:</td><td><input type=\"password\" name=\"passwd\" class=\"admin\"></td></tr>";
	echo "<tr class=\"details\"><td colspan=2 align=\"right\"><input type=\"submit\" class=\"admin\" value=\"Submit\"></td></tr>";
	echo "</table>";
}

function register_action(){
	include '../config.php';
	$sql = "INSERT INTO members SET ";
	$passwd = $_POST['passwd'];
	foreach($_POST as $p => $v){
		if($v == 'on'){
			$v = 1;
		}
		if($p != 'passwd'){
			$sql .= " $p = '$v',";
		}
	}
	$sql .= "passwd = '$passwd'";
	mysql_query($sql)or die(mysql_error());
	
	echo "<center><a href=\"index.php\">Click here to login</a>";
}
?>
