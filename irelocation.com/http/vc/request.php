<?session_start();?>
 <head>
<script Language="JavaScript">
function isEmailAddr(email)
{
  var result = false;
  var theStr = new String(email);
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
	result = true;
  }
  return result;
}

function validRequired(formField,fieldLabel)
{
	var result = true;

	if (formField.value == "")
	{
		alert('Please enter a value for the "' + fieldLabel +'" field.');
		formField.focus();
		result = false;
	}

	return result;
}

function allDigits(str)
{
	return inValidCharSet(str,"0123456789");
}

function inValidCharSet(str,charset)
{
	var result = true;

	// Note: doesn't use regular expressions to avoid early Mac browser bugs
	for (var i=0;i<str.length;i++)
		if (charset.indexOf(str.substr(i,1))<0)
		{
			result = false;
			break;
		}

	return result;
}

function validEmail(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

	if (result && ((formField.value.length < 3) || !isEmailAddr(formField.value)) )
	{
		alert("Please enter a complete email address in the form: yourname@yourdomain.com");
		formField.focus();
		result = false;
	}

  return result;

}

function validNum(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		if (!allDigits(formField.value))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validInt(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var num = parseInt(formField.value,10);
 		if (isNaN(num))
 		{
 			alert('Please enter a number for the "' + fieldLabel +'" field.');
			formField.focus();
			result = false;
		}
	}

	return result;
}


function validDate(formField,fieldLabel,required)
{
	var result = true;

	if (required && !validRequired(formField,fieldLabel))
		result = false;

 	if (result)
 	{
 		var elems = formField.value.split("/");

 		result = (elems.length == 3); // should be three components

 		if (result)
 		{
 			var month = parseInt(elems[0],10);
  			var day = parseInt(elems[1],10);
 			var year = parseInt(elems[2],10);
			result = allDigits(elems[0]) && (month > 0) && (month < 13) &&
					 allDigits(elems[1]) && (day > 0) && (day < 32) &&
					 allDigits(elems[2]) && ((elems[2].length == 2) || (elems[2].length == 4));
 		}

  		if (!result)
 		{
 			alert('Please enter a date in the format MM/DD/YYYY for the "' + fieldLabel +'" field.');
			formField.focus();
		}
	}

	return result;
}

function validateForm(theForm)
{
	// Customize these calls for your form

	// Start ------->


	if (!validEmail(theForm.email,"Email Address",true))
		return false;




	// <--------- End

	return true;
}



</script>
<!-- UTF-8 is the recommended encoding for your pages -->
    <meta http-equiv="content-type" content="text/xml; charset=utf-8" />
    <title>iRelocation Owner Admin</title>

<!-- Loading Theme file(s) -->
    <link rel="stylesheet" href="members/zpcal/themes/fancyblue.css" />

<!-- Loading Calendar JavaScript files -->
    <script type="text/javascript" src="members/zpcal/src/utils.js"></script>
    <script type="text/javascript" src="members/zpcal/src/calendar.js"></script>
    <script type="text/javascript" src="members/zpcal/src/calendar-setup.js"></script>

<!-- Loading language definition file -->
    <script type="text/javascript" src="members/zpcal/lang/calendar-en.js"></script>

<link rel="stylesheet" href="../style.css" type="text/css">
</head>
<?include 'include/xheader.php';
$subject_text = "[RVH-Visitor] ".$_SESSION['userid']." interested in your Property";
$message_text = "Hello!\n\nI am interested in renting your property.";
?>
	<table width=100%>
	<tr>
	<td>

	<h1 align="center">Request Rental</h1>
	<?
	include 'config.php';
	$sql = "SELECT l.id,l.oid,l.title,l.city,l.state,m.mid,m.email,m.first FROM listings l,members m WHERE l.id = '".$_GET['id']."' AND l.oid = m.mid";
	$row = mysql_fetch_array(mysql_query($sql));

//echo $sql;
	extract($row);
	//foreach($row as $r => $v){echo "<br>$r => $v";}
	if($_GET['action'] != 'send'){
	?>
	Send a private message to the owner of <?=$title?> to express your interest in renting his/her property.
	<form action="<?=$_SERVER['PHP_SELF']?>?action=send&id=<?=$id?>" Method="POST" onsubmit="return validateForm(this)" id="form1" name="form1">
		<Table>
		<tr><td>Email Address: </td><td colspan=2> <input type="text" name="email" size=60></td></tr>
		<tr><td>Subject: </td><td colspan=2> <input type="text" name="subject" value="<?=$subject_text?>" size=60></td></tr>
		<tr><td>Message: </td><td colspan=2><textarea cols=60 rows=6 name="message"><?=$message_text."\n\n"?><?=$title?></textarea></td></tr>
		<tr><td></td><td><?calstart();?></td><td><?calend();?></td></tr>
		<tr><td colspan=3 align="center"> <input type="submit" value="Send Message"></td></tr>
		</table>

	</form>
	<?}
	if($_GET['action'] == 'send'){
	include 'config.php';
	$sql = "SELECT l.id,l.oid,l.title,l.city,l.state,m.mid,m.email,m.first
    FROM listings l,members m WHERE l.id = '".$_GET['id']."' AND l.oid = m.mid";
    $row = mysql_fetch_array(mysql_query($sql));

    $owner = $row['mid'];
    extract($_REQUEST);
    $sql = "INSERT INTO calendar SET start = '$start', end = '$end', pid = '$id', status = 'P'";
    mysql_query($sql)or die('Could not set booking'. mysql_error());
	//foreach($_SESSION as $s => $v){echo "<br>$s == $v [1]";}
			$to = $row['first']." <" .$row['email'].">";
			$s = "[RVH-Member] ".$_SESSION['userid']." interested in rental";
			$m = "This is to notify you that you have a message waiting in your control panel at Rent Vacation Homes.\n";
			$m .= "Click the link to login:  http://rentvacationhomes.com/members/index.php \n";
			$m .= "Thank you for being an RVH customer.";
			$mh = "From: Rent Vacation Homes <rwhitney@irelocation.com>";

			mail($to,$s,$m,$mh)or die('Unable to send message at this time.');

			extract($_POST);
			$message = $message.'\n\nFrom: '.$start.' To: '.$end;
			$sql = "INSERT INTO messages SET from_email = '".$_POST['email']."',
											to_id = '$owner',
											subject = '$subject',
											message = '$message',
											date = '".date('Y-m-d h:i:s')."'";
			mysql_query($sql)or die('Could not store message'. mysql_error());
			echo "<br><br><center>Thank you.  Your message has been sent and your booking is now pending.  You should receive
			an email from the owner soon.<br><br>";
			echo "<a href=\"javascript:window.close();\">Close Window</a>";
		}
	?>
	</td>
	</tr>
	</table>
</td>
</tr>
</table>
<?


function calstart(){
?>
  <body>
	Start Date:
    <input type="text" id="calendar" name="start" />
    <button id="trigger">...</button><br>
    <script type="text/javascript">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 0,
        weekNumbers       : false,
        showOthers        : false,
        showsTime         : false,
        timeFormat        : "24",
        step              : 2,
        range             : [1900.01, 2999.12],
        electric          : false,
        singleClick       : true,
        inputField        : "calendar",
        button            : "trigger",
        ifFormat          : "%Y-%m-%d",
        daFormat          : "%Y/%m/%d",
        align             : "Br"
      });
    //]]></script>
<noscript>
<br/>
This page uses a <a href="http://www.zapatec.com/website/main/products/prod1/"> Javascript Calendar </a>, but
your browser does not support Javascript.
<br/>
Either enable Javascript in your Browser or upgrade to a newer version.
</noscript>

  </body>
</html>
<?
}
function calend(){
?>
  <body>
	End Date:
    <input type="text" id="calendar" name="end" />
    <button id="trigger">...</button><br>
    <script type="text/javascript">//<![CDATA[
      Zapatec.Calendar.setup({
        firstDay          : 0,
        weekNumbers       : false,
        showOthers        : false,
        showsTime         : false,
        timeFormat        : "24",
        step              : 2,
        range             : [1900.01, 2999.12],
        electric          : false,
        singleClick       : true,
        inputField        : "calendar",
        button            : "trigger",
        ifFormat          : "%Y-%m-%d",
        daFormat          : "%Y/%m/%d",
        align             : "Br"
      });
    //]]></script>
<noscript>
<br/>
This page uses a <a href="http://www.zapatec.com/website/main/products/prod1/"> Javascript Calendar </a>, but
your browser does not support Javascript.
<br/>
Either enable Javascript in your Browser or upgrade to a newer version.
</noscript>
<?}?>
  </body>
</html>

