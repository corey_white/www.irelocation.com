<?php
	include "../inc_mysql.php";
	
	define(WHEATON," lead_ids like '%1386%' ");
	define(SIRVA," lead_ids like '%1387%' ");
	define(BEKINS," lead_ids like '%1388%' ");	
	define(BEKINS_TM," mover_id = 89 ");
	define(UNIGROUP," mover_id != 89 and mover_id != '' ");
	define(PAUL_ARPIN," lead_ids like '%1427%'");
	define(ALBERT_MOVING," lead_ids like '%1517%'");
	define(WC,"Wise Click");
	define(TM," site like 'tm%' ");
	define(PM," (site like 'pm%' or site like 'me%') ");
	define(MMC," site like 'mmc%' ");
	
	
	$prices = array();	
	
	$numbers = array();
	
	$prices[WHEATON] = 3;
	$prices[SIRVA] = 2.5;
	$prices[BEKINS] = 3.0;
	$prices[BEKINS_TM] = 2.5;
	$prices[UNIGROUP] = 3.5;
	$prices[PAUL_ARPIN] = 2.75;	
	$prices[ALBERT_MOVING] = 4.00;
	
	$site[TM] = array(WHEATON,BEKINS_TM,UNIGROUP,SIRVA);
	$site[PM] = array(SIRVA,BEKINS,ALBERT_MOVING,PAUL_ARPIN);
	

	$month = date("m");
	$year = date("Y");
	
	foreach ($site[TM] as $comp)
	{
		//echo $prices[$comp];
		$count = gatherCounts(TM,$year,$month,$comp);
		$income = $count * $prices[$comp];
		echo "<!--- TM: $income --->\n";
		$numbers["topmoving"]["revenue"] += $income;		
	}
	$numbers["topmoving"]["cost"] = gatherAdCosts(TM,$year,$month);
	$numbers["topmoving"]["leads"] =  gatherCounts(TM,$year,$month,TM);
	
	$count = 0;
	foreach ($site[PM] as $comp)
	{		
		$count = gatherCounts(PM,$year,$month,$comp);
		$numbers["promoving"]["leads"] += $count;
		$income = $count * $prices[$comp];
		echo "<!--- PM: $income --->\n";
		$numbers["promoving"]["revenue"] += $income;		
	}
	$numbers["promoving"]["leads"] =  gatherCounts(PM,$year,$month,PM);
	$numbers["promoving"]["cost"] = gatherAdCosts(PM,$year,$month);
	$count = gatherCounts(MMC,$year,$month);	
	
	$numbers["movemycar"]["leads"] = $count;
	$numbers["movemycar"]["revenue"] = $count * 5;
	$numbers["movemycar"]["cost"] = gatherAdCosts(MMC,$year,$month);
	
	?>
	<style>
		.numbers
		{
			font-family:"Courier New", Courier, mono;
			color:#000000;			
			font-weight:normal;
		}
		
		.text
		{
			font-family:"Courier New", Courier, mono;
			color:#0000CC;
			font-weight:bold;
		
		}
	
	</style>
	<link href="irelocation.css" rel="stylesheet" type="text/css">
	<table width="50%">
		<tr>
			<td>&nbsp;</td><td colspan="3" class="mainmovingtopic">This data does not include leads for today.</td><td>&nbsp;</td>
		</tr>
		<tr>
			<th width="25%" class="mainmovingtopic">TopMoving.com</th>
			<td width="13%">&nbsp;</td>
			<th width="25%" class="mainmovingtopic">ProMoving.com</th>
			<td width="12%">&nbsp;</td>
			<th width="25%" class="mainmovingtopic">MoveMyCar.com</th>
		</tr>		
		<Tr>
			<?php 
			foreach ($numbers as $set)
			{
				echo "<td align='center' ><table border='1'>";
				
				echo "<tr><td class='text'>Leads:</td><td align='right'  class='numbers'> ".number_format($set["leads"],0)."</td></tr>";	
				echo "<tr><td class='text'>Cost:</td><td align='right'  class='numbers'> \$".number_format($set["cost"],2,".",",")."</td></tr>";
				echo "<tr><td class='text'>Income: </td><td align='right'  class='numbers'>\$".number_format($set["revenue"],2,".",",")."</td></tr>";	
				
				echo "<tr><td class='text'>C P L:</td><td align='right'  class='numbers'> \$".number_format(($set["cost"] / $set["leads"]),2)."</td></tr>";	
				echo "<tr><td class='text'>R P L:</td><td align='right'  class='numbers'> \$".number_format(($set["revenue"] / $set["leads"]),2)."</td></tr>";	
				
				echo "</table></td>";
				echo "<td>&nbsp;</td>";
			}
			?>
		</Tr>
		<tr>
			<Td colspan="6">&nbsp;</Td>
		</tr>
		<Tr>
			<td colspan="6" class="text">
				We have CPC data for yesterday from:
				<ul>
				<?php
					$cpc = gatherCurrentCPC();
					foreach($cpc as $k => $v)
					{
						echo "<li>".ucwords($k).": <span class='numbers'>".$v."</span></li>\n";
					}
				?>
				<br/>
				<li><font color="#FF0000">Note:</font>If you dont see a site in a particular<Br/> Engine's list, it has not been retrieved for the day.</li>
				</ul>
			</td>
		
		</Tr>
	</table>
	
	<?	
	
	function gatherCurrentCPC()
	{
		$ts = time() - (24 * 3600);//subtract a day.
 	 	$yesterday = date("Ymd",$ts);
		$sql = "select * From movingdirectory.costperclick where ts = '$yesterday' order by search_engine, site";
		$rs = new mysql_recordset($sql);
		$cpc = array();
		while($rs->fetch_array())
		{
			$cpc[$rs->myarray["search_engine"]][] = $rs->myarray["site"];		
		}
		
		$cpc["google"] = implode(",",$cpc["google"]);
		$cpc["msn"] = implode(",",$cpc["msn"]);
		$cpc["overture"] = implode(",",$cpc["overture"]);
		
		return $cpc;
	}
	
	
	function gatherAdCosts($site,$year, $month)
	{
		$sql = "select sum(cost) cost from movingdirectory.costperclick where ts like '$year$month%' and $site;";		
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$cost = $rs->myarray["cost"];
		if ($site == TM || $site == PM)
			$wiseclick = gatherCounts($site,$year,$month,WC) * 6;
		else
			$wiseclick = 0;
			
		return $cost + $wiseclick;
	}	
	
	function gatherCounts($site, $year, $month,$company="*")
	{
 	 	$today = date("Ymd",time());
	
		if ($site == TM)
		{
			switch($company)
			{
				case WC:
					$sql = "select count(*) as count from movingdirectory.quotes ".
							"where source like 'tm_wc' and lead_ids != '||||||' and ".
							"lead_ids != '' and received like '$year$month%' and received ".
							" not like '$today%';";
					break;			
				case TM:
					$sql = "select count(*) as count from movingdirectory.quotes ".
							"where source like 'tm%' and lead_ids != '||||||' and lead_ids != '' and ".
							" received like '$year$month%' and received not like '$today%'";
					break;
				case SIRVA:
					$sql = "select (count(*)) as count ".
							"From movingdirectory.quotes as q ".
							"join movingdirectory.movesirva as s ".
							"on s.quote_id = q.quote_id ".
							"where ".$company." and q.received like '$year$month%' ".
							"and q.source like 'tm%'  and q.received not like '$today%';";
					break;
				case WHEATON:
					$sql = "select (count(*)) as count From movingdirectory.quotes as q ".
							"join movingdirectory.movewheaton as w ".
							"on w.quote_id = q.quote_id ".
							"where ".$company." and q.received like '$year$month%' and q.source like 'tm%' ".
							"and w.record_key != -1  and q.received not like '$today%';";
					break;
				case BEKINS_TM:
					$sql = "select count(*)as count ".
							"from movingdirectory.quotes as q ".
							"join movingdirectory.movebekins as b ".
							"on q.quote_id = b.quote_id ".
							"where q.source like 'tm%' and q.received like '$year$month%'  ".
							"and ".$company." and b.record_key != -1  and q.received not like '$today%';";
					break;
				case UNIGROUP:
					$sql = "select count(*) as count From  ".
							"movingdirectory.quotes where ".$company." ".
							"and received like '$year$month%'  and received not like '$today%' and source like 'tm%';";
					break;
			}			
		}
		else if ($site == PM)
		{
			switch($company)
			{
				case WC:
					$sql = "select count(*) as count from movingdirectory.quotes ".
							"where source like 'pm_wc' and lead_ids != '||||||' and ".
							"lead_ids != '' and received like '$year$month%' and received ".
							" not like '$today%';";
					break;			
				case PM:
					$sql = "select count(*) as count from movingdirectory.quotes ".
							"where (source like 'pm%' or source like 'me%') and lead_ids != '||||||' and lead_ids != '' and  ".
							"received like '$year$month%'  and received not like '$today%';";
					break;
				case SIRVA:
					$sql = "select (count(*)) as count ".
							"From movingdirectory.quotes as q ".
							"join movingdirectory.movesirva as s ".
							"on s.quote_id = q.quote_id ".
							"where ".$company." and q.received like '$year$month%' ".
							"and (q.source like 'pm%' or q.source like 'me%')  and q.received not like '$today%';";
					break;				
				case ALBERT_MOVING:
						$sql = "select count(*)as count ".
							"from movingdirectory.quotes as q ".
							"join movingdirectory.movebekins as b ".
							"on q.quote_id = b.quote_id ".
							"where (q.source like 'pm%' or q.source like 'me%') and q.received like '$year$month%'  ".
							"and ".$company." and b.record_key != -1 and q.received not like '$today%';";
				case BEKINS:					
				case PAUL_ARPIN:
					$sql = "select (count(*)) as count From movingdirectory.quotes  ".
							"where ".$company." and received like '$year$month%' and  ".
							"(source like 'pm%' or source like 'me%')  and received not like '$today%';";
					break;
			}
		}
		else if ($site == MMC)
		{
			$sql = "select count(lead_ids) as count from movingdirectory.quotes  ".
					"where source like 'mmc%' and length(lead_ids) = 26 and received ". 
					"like '$year$month%'  and received not like '$today%';";
		}
		echo "<!--- SQL: gatherCounts('$site', '$year', '$month','$company'):\n = $sql --->\n\n";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];
	}

?>