<?php
	include "cron_functions.php";
	include "cpc.php";
	include_once "../inc_mysql.php";

	
	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];	
	
	echo "<!-- $month/$year --->";
?>
	<div align="center">
		<table width="85%">
			<tr>
				<td class="mainmovingtopic" align="left ">
					<? echo "Valid: ".getNumbers("valid",$month,$year); ?>
				</td>
				<td width="20">&nbsp;</td>
				<td class="mainmovingtopic" width="100">&nbsp;</td>
				<td class="mainmovingtext" width="40">&nbsp;</td>
				<td rowspan="5" width="50%">&nbsp; </td>
			</tr>
			<?php if ($month == date("m")) { ?>
			<tr>
				<td class="mainmovingtopic" align="left">
					<? echo "Limbo: ".getNumbers("not-proccessed",$month,$year); ?>
				</td>
				<td width="20">&nbsp;</td>
				<td width="20" class="mainmovingtopic">&nbsp;</td>
				<td class="mainmovingtext">&nbsp;</td>
			</tr>			
			<? } ?>
			<tr><td colspan="2">&nbsp;</td><td width="20" colspan="3" class="mainmovingtext">&nbsp;</td>
			<tr>
				<td class="mainmovingtopic" align="left" colspan="5">										
					<form action="<?php echo $_SERVER['PHP_SELF']; ?>?page_id=10" method="post">
						Campaign: 
						<select name="campaign">
							<option value="all">All</option>					
							<?php
								$data = getSources($month,$year);
								$sources = array_keys($data);
								foreach($sources as $source)
								{	
									echo "<option";
									if ($campaign == $source) echo " selected";
									echo " value='$source'> ".ucwords($source)." (".$data[$source].")</option>\n";							
								}
							?>					
						</select>
						<Select name="month">				
							<?php							
								$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");							
								for ($i = 1; $i < 13; $i++)
								{
									echo "<option value='";
									if ($i < 10)
										echo "0$i";
									else
										echo "$i";
									echo "' ";
									if (intval($month) == ($i)) 
										echo "selected ";
									echo ">".$months[$i-1]."</option>\n";
								}
							?>
						</Select>
						<select name="year">
							<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
						</select>
						<input type="submit" name="Filter" value="Go"/>
					</form>
				</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="mainmovingtopic" colspan="5" align="left">
					Leads
				</td>
			</tr>
			<tr>
				<td align="left" colspan="5">
					<?php displayLeads($campaign,$month,$year);//defaults to the current month, coming from TopSecurityCompanis.com, that are Residential leads from any campaign. ?>	
				</td>
			</tr>
		</table>
	</div>
	<Br/>
<?	


	function getTotalLeadRevenue($month,$year)
	{
		 $array = array(DOOR2DOOR,MOVEX,ABFUPACK);
		 foreach($array as $comp)
		 {
		 	$r = getRevenue($comp, $month,$year);
			//echo $r;
		 	$revenue += $r;
		 }
		 return $revenue;
	}
	
	function getNumbers($what="valid",$month="*",$year="*")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		switch($what)
		{
			case "valid":
				//-- valid Leads
				$sql = "select count(*) count from movingdirectory.quotes_local where lead_ids != '' and received like '$year$month%';";
				break;
			case "not-proccessed":
				//-- Yet to be Processed
				$sql = "select count(*) count from movingdirectory.quotes_local where lead_ids = '';";	
				break;
		}
		//echo $sql;
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		return $rs->myarray["count"];
	}
	
	function getSummaryInfo($month="*", $year="*")
	{
		$sql = "select count(*) ";
	
	}
	
	function printSummaryTable($month="*", $year="*")
	{		
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
	}
	
	function formatLeadTime($ts)
	{
		$year = substr($ts,0,4);
		$month = substr($ts,4,2);
		$day = substr($ts,6,2);
		$hour = substr($ts,8,2);
		if (intval($hour) > 12)
		{
			$pm = 1;
			$hour -= 12;		
		}
		else if (intval($hour) == 0)
			$hour = 12;
		$min = substr($ts,10,2);
		$sec = substr($ts,12,2);
		if (strlen($ts) > 8)
		{
			$time = intval($hour).":".$min.":".$sec;
			if ($pm == 1)
				$time .= " pm ";
			else
				$time .= " am ";				
			return "$time$month-$day-$year";
		}
		else if (strlen($ts) == 6)//YYYYMM
		{
			return "$month-$year";
		}
		else
			return "$month-$day-$year";

	}
	
	function displayLeads($campaign="all", $month="*", $year="*",  $site_code="smq")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		echo "<!--- in displayLeads($campaign,$month,$year) --->";
		//print_month_header($month,$year);
		if ($campaign == "google" || $campaign == "overture" || $campaign == "msn")
			print_cpc_month_header("smq",$campaign,$month,$year);
		else
			print_month_header($month,$year);
		print_it(getLocalLeads("all","all",$month,$year),$month, $year,"all",$campaign, $site_code);
		$sources = array("+tmc","tm%","pm%","me%");	
		sort($sources);
		foreach ($sources as $source)
			print_it(getLocalLeads($source,"all",$month,$year),$month, $year,$source, $campaign, $site_code);

		echo "</table>";
	}
	
	function getLocalLeads($lead_id, $campaign="all",  $month="*", $year="*", $site_code="smq")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "select substring(received,7,2) 'day', count(received) 'count' from movingdirectory.quotes_local ".
		"where received like '$year$month%'";
		if ($lead_id != "all")
			$sql .= " and source like '$lead_id' ";
		$sql .= " group by substring(received,1,8) WITH ROLLUP;";
		echo "<!--- LocalLeads: $sql --->";
		return $sql;
	}
	
	
	function print_it($sql, $month, $year, $lead_id, $campaign, $site_code)
	{				
		$sources = array(
			"all" => array("All Leads","all",0),			
			"+tmc" => array("TopMovingCompanies.com","+tmc",0),
			"tm%" => array("TopMoving.com","tm%",0),
			"pm%" => array("ProMoving.com","pm%",0),
			"me%" => array("MovingEase.com","me%",0)			
			);
			
		$comp_name = $sources[$lead_id][0];
		
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
		
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		while ($rs->fetch_array())
		{
			
			$link_base = "reporting.php?page_id=10&site=quotes_local";
			if ($source != "all")
				$link_base .= "&source=".urlencode($lead_id);
			
			$counter++;
			
			$day = $rs->myarray["day"];
								
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];
				echo "<td align='right'>";
				echo "<a href='".$link_base."&received=$year$month$day'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];				
				
				$total = "<a href='reporting.php?page_id=10&site=quotes_local&received=$year$month&source=".urlencode($lead_id)."'>$total</a>";
				
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "$total</td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."' >&nbsp;</td>";
					echo "<td align='right'> ";
					echo "$total</td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "$total</td>";
				}
				else
				{
					echo "<td align='right'> ";
					echo "$total</td>";
				}
			}			
			
			
		}				
		echo "</tr>\n";
	}
	

	function getABFLosses($month, $year)
	{
		//get all failures
		$sql = "select count(*) count from leads_verified as v join leads_selfmove as l on l.quote_id = v.quote_id where v.success = 0 and v.site = 'smq' and v.lead_id = '9004' and l.received like '$year$month%' ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();

		$count = $rs->myarray["count"];
		
		if ($month == "08" && $year == "2006")
			$count += 64;
		return $count;
	}
	
	function getSources($month,$year)
	{
		$sql = "select distinct source, count(source) count from irelocation.leads_selfmove where scrub_id != 0 and lead_ids != '' and received like '$year$month%' and source like 'smq_%' group by source;";
		$rs = new mysql_recordset($sql);
		$results = array();
		while ($rs->fetch_array())
		{
			$s = substr($rs->myarray["source"],4);
			
			$results[$s] = $rs->myarray["count"];
		}
		return $results;	
	}

?>
<script>
	function openPopup()
	{
		window.open( "http://irelocation.com/adwords/test.php?site=smq&reload=1", 'update' ,'width=250,height=250');
	}	
</script>