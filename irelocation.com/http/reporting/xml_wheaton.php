<?php
	include "../inc_mysql.php";
	
	$quote_id = $_REQUEST["quote"];
	if (strlen($quote_id) == 0)
	{
		echo "No Quote Id Given...";
		exit();
	}
		
	
	function getZip($city, $state)
	{
		$sql = "Select zip from movingdirectory.zip_codes where city like '$city%'  and state = '$state';";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return $rs->myarray["zip"];
		}
		else
		{
			//trim the city..
			if (strlen($city) > 3)
			{
				$city = substr($city,0,strlen($city) - 3);
				echo "<!--- $city,$state --->";
				return getZip($city,$state);
			}			
		}
	}
		
	
	
	$sql = "select * from movingdirectory.quotes where quote_id = '$quote_id';";
	$rs = new mysql_recordset($sql);
	if (!$rs->fetch_array())
	{
		echo "Invalid Quote id.";
		exit();
	}
	
	$keys = array_keys($rs->myarray);
	
	foreach ($keys as $key)
	{
		if (!is_numeric($key))
			echo $key." = ".$rs->myarray[$key]."<br/>";
	}
	
	extract($rs->myarray);
	
	
	$move_date = split("-",$est_move_date);//our format yyyy-mm-dd
	$est_move_date = $move_date[1]."/".$move_date[2]."/".$move_date[0];//theres mm/dd/yyyy
	
	$phone_home = str_replace("\.","",$phone_home);
	$phone_home = substr($phone_home,0,3)."-".substr($phone_home,3,3)."-".substr($phone_home,6,4);
	
	if (strlen($phone_work) > 0)
	{
		$phone_work = str_replace("\.","",$phone_work);
		$phone_work = substr($phone_work,0,3)."-".substr($phone_work,3,3)."-".substr($phone_work,6,4);
	}
	
	$xname=explode(" ",$rs->myarray["name"]);
    $xfirstname=$xname[0];
    $xlastname=substr($rs->myarray["name"],strlen($xfirstname)+1);
	
	$destination_zip = getZip($destination_city,$destination_state);
	
	if (strpos($name," ") === FALSE)//bad no last name.
	{
		echo "Error - No Last/First name.";
		exit();
	}
		
$format = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>".
	"<InternetLead version=\"1.0\" source=\"irelocation\">".
		"<Contact>".
			"<LeadId></LeadId>".
			"<FirstName>$xfirstname</FirstName>".
			"<LastName>$xlastname</LastName>".
			"<Address1></Address1>".
			"<Address2></Address2>".
			"<City>$origin_city</City>".
			"<State>$origin_state</State>".
			"<Zipcode>$origin_zip</Zipcode>".
			"<EmailAddress>$email</EmailAddress>".
			"<ContactPreference></ContactPreference>".
			"<WorkPhone>$phone_work</WorkPhone >".
			"<HomePhone>$phone_home</HomePhone>".
			"<CellPhone></CellPhone>".
			"<BestTimeToCall>Anytime</BestTimeToCall >".
			"<Comments>$comments</Comments>".
		"</Contact>".
		"<MoveDetails>".
			"<MoveType>Full-Service</MoveType>".
			"<FromCity>$origin_city</FromCity>".
			"<FromState>$origin_state</FromState>".
			"<FromZip>$origin_zip</FromZip>".
			"<FromCountry>$origin_country</FromCountry>".
			"<ToCity>$destination_city</ToCity>".
			"<ToState>$destination_state</ToState>".
			"<ToZip>$destination_zip</ToZip>".
			"<ToCountry>$destination_country</ToCountry>".
			"<MoveDate>$est_move_date</MoveDate>".
			"<MoveSize></MoveSize>".
		"</MoveDetails>".
	"</InternetLead>";

	//$format = str_replace(">","&gt;",$format);
	echo str_replace("<","&lt;",str_replace(">","&gt;",$format));

?>