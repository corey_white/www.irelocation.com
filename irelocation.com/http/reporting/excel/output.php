<?
	define(DELIM,",");
	define(DEBUG,false);
	define(ANY,0);
	define(ATUS,1);
	define(TAS,2);
	define(MARBLE,3);
	$campaigns = array("","Auto-Transport.us","TopAutoShippers","Top Auto Leads");
	$lead_id = $_REQUEST['lead_id'];
	
	if ($lead_id == "" || !is_numeric($lead_id))
	{
		echo "Error! invalid lead id given, usage: output.php?lead_id=[lead_id]";
		exit();
	}

	include "../../inc_mysql.php";
	if (DEBUG) echo "<!--- $lead_id --->";
	$results = checkForCampaigns($lead_id);
	
	if (DEBUG) echo "<!--- ".print_r($results,true)." --->";
	
	if ($results[ANY])
	{		
		$filename = getName($lead_id)." - ".date("F Y").".csv";
		if (!DEBUG)	header('Content-type: text/csv');
		if (!DEBUG) header('Content-Disposition: attachment; filename="'.$filename.'"');
	
		printHeaderRow();
		
		if ($results[ATUS]>0)
		{
			$data = runCode(getMonthlyCountSql($lead_id, ATUS));
			printRow($data,ATUS,$results[ATUS]);
		}
		
		if ($results[TAS]>0)
		{
			$data = runCode(getMonthlyCountSql($lead_id, TAS));
			printRow($data,TAS,$results[TAS]);
		}
		
		if ($results[MARBLE]>0)
		{
			$data = runCode(getMonthlyCountSql($lead_id, MARBLE));
			printRow($data,MARBLE,$results[MARBLE]);
		}
	}
	
	function getName($lead_id)
	{
		$sql = "select comp_name from movingdirectory.company as c join ".
				" movingdirectory.directleads as d on c.comp_id = d.comp_id ".
				" where d.lead_id = $lead_id limit 1";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray['comp_name'];
	}
	
	function printHeaderRow()
	{
		echo "Campaign".DELIM."Goal";
		
		$maxdays = date('t');
		for ($i = 1; $i <= $maxdays; $i++)
			echo DELIM.$i;
		echo DELIM."Total\n";
	}
	
	function printRow($results,$campaign,$goal)
	{
		switch($campaign)
		{	
			case ATUS: $title = "Auto-Transport.us";break;
			case TAS:  $title = "Top Auto Shippers";break;
			case MARBLE: $title = "Top Auto Leads";break;
		}
		
		echo $title.DELIM.$goal;
		$maxdays = date('t');
		$currday = 1;
		$total = 0;
		foreach($results as $day => $count)
		{
			$total += $count;
			echo DELIM;
			$day = intval($day);
			if ($day == $currday)
			{
				echo $count;				
			}
			else
			{
				for ($i = $currday; $i < $day; $i++)
					echo "0".DELIM;
				$currday = $day;
				echo $count;
			}
			$currday++;
		}
		
		if ($currday <= $maxdays)
		for ($i = $currday; $i <= $maxdays; $i++)
			echo DELIM."0";
		echo DELIM.$total."\n";
	}
	
	function runCode($sql)
	{
		$results = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$results[$rs->myarray['day']] = $rs->myarray['count'];
		$rs->close();
		return $results;
	}
	
	function getMonthlyCountSql($lead_id, $campaign)
	{
		$campaignnames = array("tas","auto");
		$sql = "select count(*) 'count', substring(received,7,2) 'day' from ";
		
		if ($campaign == ATUS)
			$sql .= " movingdirectory.quotes where cat_id = 1 ";
		else
			$sql .= " marble.auto_quotes where campaign = '".
					$campaignnames[$campaign-2]."' ";
		$sql .= " and lead_ids like '%".$lead_id."%' ".
				" and received like '".date("Ym")."%' ".
				" group by substring(received,7,2) ";
		if (DEBUG) echo "<!--- $sql -->";
		return $sql;
	}
	
	function checkForCampaigns($lead_id)
	{
		$marblesql = "select monthly_goal from marble.campaign where".
				" lead_id = '".$lead_id."' and month = '".date("Ym")."' and ".
				" active = 1 ";
		$atussql = str_replace("marble","movingdirectory",$marblesql);
		
		$tas = $marblesql." and site_id = 'tas' ";
		$marble = $marblesql." and site_id = 'auto' ";
		
		if (DEBUG) echo "<!--- $atussql --->";
		$rs = new mysql_recordset($atussql);
		$rs->fetch_array();
		$atus = $rs->myarray['monthly_goal'];
		if (DEBUG) echo "<!--- $atus --->";
		$rs->close();
		
		if (DEBUG) echo "<!--- $tas --->";
		$rs = new mysql_recordset($tas);
		$rs->fetch_array();
		$tas = $rs->myarray['monthly_goal'];
		if (DEBUG) echo "<!--- $tas --->";
		$rs->close();
		
		if (DEBUG) echo "<!--- $marble --->";
		$rs = new mysql_recordset($marble);
		$rs->fetch_array();
		$marble = $rs->myarray['monthly_goal'];
		if (DEBUG) echo "<!--- $marble --->";
		$rs->close();
		
		return array(($atus>0||$tas>0||$marble>0),$atus,$tas,$marble);	
	}

?>