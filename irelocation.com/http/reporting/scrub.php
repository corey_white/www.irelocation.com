<Select name="month">

<?php

	$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

	for ($i = 1; $i < 13; $i++)
	{
		echo "<option value='";
		if ($i < 10)
			echo "0$i";
		else
			echo "$i";
		echo "'>".$months[$i-1]."</option>\n";
	}
?>
</Select>

