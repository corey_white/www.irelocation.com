<?php
	include "../inc_mysql.php";
	
	extract($_REQUEST);

	
	define (VALID," and lead_ids != '' and lead_ids != '||||||' ");
	$sites = array(  "re" => "Real Estate",  "das" => "Dependable AutoShipping", "cs" => "CarShipping.com", "tm" => "TopMoving.com", "pm" => "ProMoving.com", "me" => "MovingEase.com", "mmc" => "MoveMyCar.com", "atus" => "AutoTransports.us");
	
	function formatTime($ts)
	{
		$year = substr($ts,0,4);
		$month = substr($ts,4,2);
		$day = substr($ts,6,2);
		$hour = substr($ts,8,2);
		if (intval($hour) > 12)
		{
			$pm = 1;
			$hour -= 12;		
		}
		else if (intval($hour) == 0)
			$hour = 12;
		$min = substr($ts,10,2);
		$sec = substr($ts,12,2);
		if (strlen($ts) > 8)
		{
			$time = intval($hour).":".$min.":".$sec;
			if ($pm == 1)
				$time .= " pm";
			else
				$time .= " am";	
			$time .= "<br/>";
			return "$time$month-$day-$year";
		}
		else if (strlen($ts) == 6)//YYYYMM
		{
			return "$month-$year";
		}
		else
			return "$month-$day-$year";

	}
	
	
	//...?page_id=5&ts=20060602&site=auto&campaign=all&lead_id=tm&month=06&year=2006
	function getIReloSourceString($referer="all", $campaign="all")
	{
		$source_string = " ";
		if ($campaign == "all")
		{
			if ($referer != "all")//no filter.
				$source_string = " and referer like '$referer%'";
		}
		else
		{
			if ($campaign == "org")//organic leads only...
			{
				if ($referer != "all")//then pm or tm
					$source_string = " and (referer like '$referer%search%' or referer = '$referer') ";
				else//all organics?? nasty.. 
					$source_string = " and (referer like '%search%' or referer = 'pm' or referer = 'tm') ";			
			}
			else
			{
				if ($referer != "all")//then pm or tm
					$source_string = " and referer like '$referer%$campaign%' and referer not like '%search%' ";
				else
					$source_string = " and referer like '%$campaign%' and referer not like '%search%' ";						
			}
		}
		return $source_string;
	}
	
	//same function as in all_leads.php.
	function getSourceString($site_code, $campaign="all")
	 {
	 	echo "<!--- $site_code $campaign --->";
	 	if ($campaign == "all")
		{
			if($site_code == "cs")//add csq source...
				return " and (source like 'cs%' or source like 'csq%') ";
			else
			{
				if ($site_code == "tm" || $site_code == "pm" || $site_code == "me")
					return " and source like '$site_code%' ".VALID;
				else
					return " and source like '$site_code%' ";
			}
		}
		else if ($campaign == "org")//organic
		{			
			echo "<!-- organic leads. -->";
			switch($site_code)
			{
				case "cs":
					return " and source like 'cs%' and source not like 'cs_overture%' and source not like 'cs_msn%' and source not like 'csq_google%' and source not like 'cs_superpages%' and source not like 'cs_google%' and source not like 'cs_tas%'  ";
				case "pm":					
					return " and source like 'pm%' and source not like 'pm_overture%' and source not like 'pm_msn%' and source not like 'pm_google%' and source not like 'pm_boxdev%' and source not like 'pm_silver%' and source not like 'pm_superpages%' and source not like 'pm_tmc%' and source not like 'pm_ask%' ".VALID;
				case "atus":					
					return " and source like 'atus%' and source not like 'atus_overture%' and source not like 'atus_msn%' and source not like 'atus_google%' and source not like 'atus_google%' and source not like 'atus_google%' ";
				case "mmc":					
					return " and source like 'mmc%' and source not like 'mmc_overture%' and source not like 'mmc_msn%' and source not like 'mmc_google%' and source not like 'mmc_google%' and source not like 'mmc_google%' and source not like 'mmc_pas%'  ";
				case "tm":					
					return " and source like 'tm%' and source not like 'tm_overture%' and source not like 'tm_msn%' and source not like 'tm_google%' and source not like 'tm_superpages%' and source not like 'tm_tmc%' and source not like 'tm_ask%' ".VALID;
				case "me":					
					return " and source like 'me%' and source not like 'me_overture%' and source not like 'me_msn%' and source not like 'me_google%' and source not like 'me_silver%' and source not like 'me_superpages%' and source not like 'me_ask%' ".VALID;					
			}
		}
		else
		{
			if ($site_code == "cs")
			{
				$result = "and (source like 'cs_".$campaign."%' or source like 'csq_".$campaign."%' ) ";			
			}
			else
			{				
				$result = " and source like '$site_code";
				$result .= "_";
				$result .= "$campaign%' ";
				if ($site_code == "tm" || $site_code == "pm" || $site_code == "me")
					$result.= VALID;
			}
			return $result;
		}	 	
		
	 }
	
	
	if ($site == "auto" || $site == "realtor")
	{
		if ($lead_id == -1)
			$lead_id = "all";
		$source_string = getIReloSourceString($lead_id, $campaign);
		$table = "irelocation.leads_$site";
		$lead_string = "";
		$filter = "";
	}
	else
	{
		if ($year == 2006)
			$table = "movingdirectory.quotes";
		else if ($year == 2005)
			$table = "movingdirectory.quotes_archive";
		else if ($year = 2004)
			$table = "movingdirectory.quotes_old";
	
		$source_string = getSourceString($site,$campaign);
		
		if ($cat_id == 3)
		{
			$filter = " and cat_id = 3 ".VALID;
		}
		else if ($cat_id == 2)
			$filter = " and cat_id = 2 ".VALID;
		else if (substr_count($site,"boxdev") > 0 || substr_count($site,"silver") > 0)
			$filter = VALID;		
		else 
			$filter = "";
			
		if ($lead_id == -1)
			$lead_string = "";
		else
			$lead_string = " and lead_ids like '%$lead_id%' ";
	}
	
	$sql = "select * from $table where received like '$ts%' $source_string $lead_string $filter ";
	if (strlen($orderby) > 0)
		$sql .= "order by $orderby";
	$rs = new mysql_recordset($sql);	
	
	
	
	function formatSQL($display_sql)
	{	
		$display_sql = str_replace("select *","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; *",$display_sql);
		$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
		$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
		$display_sql = ereg_replace("('[a-z0-9%_]*')","<font color=\"red\">\\1</font>",$display_sql);
		return "<font face='Courier New' size='2px'>".trim($display_sql).";</font><br/><br/>";
	}
	/*
	$display_sql = $sql;
	$display_sql = str_replace("select *","Select<Br/>&nbsp; &nbsp; *",$display_sql);
	$display_sql = str_replace("from","<br/>from<br/>&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("where","<br/>where<br/>&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("and","and<br/>&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("order by","<Br/>order by<br/>&nbsp; &nbsp; ",$display_sql);
	*/
	
	?>
	<div class="mainmovingtext" >
	<Strong class="mainmovingtopic" >SQL: </Strong><br/><?php  echo formatSQL($sql); ?><br/><Br/>
	<strong class="mainmovingtopic" >Received: <?php echo formatTime($ts); ?></strong><br/><Br/>
	<strong class="mainmovingtopic" >Source: <?php echo $sites[$site]; ?></strong><br/><Br/>
	<br/>
	*Note: If country is not displayed, the lead is local to the US.
	<br/>
	
	<br/>
	<?php
		//always put orderby last. much easier to strip off.	
		$filterlink = $_SERVER['QUERY_STRING'];
		//pull off old ordery by if it exists.
		$i = strpos($filterlink,"&orderby=");
		if ($i > 0)
			$filterlink = substr($filterlink,0,$i);
		if (strpos($_SERVER['QUERY_STRING'],"&orderby=") > 0 )
		{
			echo "<a class='mainmovingtopic' href='".$_SERVER['PHP_SELF']."?$filterlink'>Clear Ordering</a><br/>";
		}
	?>
	</div>
	<table border=1 class='mainmovingtext' ><tr >		
		<th class="mainmovingtopic"><a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=email"; ?>'>Customer</a></th>	
		<?php if ($table == "irelocation.leads_realtor") { ?>
			<th class="mainmovingtopic"><a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=buying_state"; ?>'>Buying Info</a></th>
			<th class="mainmovingtopic"> <a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=selling_state"; ?>'>Selling Info</a></th>			
		<? } else { ?>
			<th>Origin</th>
			<th>Destination</th>
		<? } ?>
		<th class="mainmovingtopic"><a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=received"; ?>'>Received</a></th>
		<th>Comments</th>
		<?php if ($table == "irelocation.leads_auto" || $table == "irelocation.leads_realtor") { ?>
			<th>Campaign</th>
			<th class="mainmovingtopic"><a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=referer"; ?>'>Referer</a></th>
		<? } else { ?>
			<th class="mainmovingtopic"><a href='<?php echo $_SERVER['PHP_SELF']."?".$filterlink."&orderby=source"; ?>'>Source</a></th>
		<? } ?>
		<th>Leads</th>
	</tr>
	<?php
	$count = 0;
	while($rs->fetch_array())
	{
		echo "<tr>";
		extract($rs->myarray);
		if (strlen($name) == 0)//if no name field try lname and fname.
		{
			$name = $fname." ".$lname;
		}
		
		echo "<td>$name<Br/>";
		unset($name);
		if (strlen($phone_home) == 10)
		{
			echo "(".substr($phone_home,0,3).") ".substr($phone_home,3,3)."-".substr($phone_home,6)."<br/>";
		}
		else if (strlen($phone) == 10)//irelocation.leads_* table...
		{
			echo "(".substr($phone,0,3).") ".substr($phone,3,3)."-".substr($phone,6)."<br/>";
		}
		else
		{
			if (strlen($phone_home) > 0)
				echo $phone_home."<br/>";	
		}
		
		echo $rs->myarray["email"];
		echo "</td>";
		if ($table != "irelocation.leads_realtor")
		{
			if(strlen($origin_city) > 0)
			{
				echo "<td> $origin_city, $origin_state $origin_zip";
			}
			else if (strlen($from_city) > 0)
			{
				echo "<td> $from_city, $from_state $from_zip";			
			}
			else echo "<td>&nbsp;</td>";
			
			if ($origin_country != $destination_country )
			{
				echo "<!--- id: ".$rs->myarray["quote_id"]."--->";
				echo "<Br/> $origin_country </td>";
			}
			else
				echo "</td>";
				
			if (strlen($destination_city) > 0)
			{
				echo "<td> $destination_city, $destination_state";
				if ($origin_country != $destination_country  )
					echo "<Br/> $destination_country </td>";
				else
					echo "</td>";		
			}
			else if (strlen($to_city) > 0)
			{
				echo "<td> $to_city, $to_state $to_zip</td>";
			}
			else echo "<td>&nbsp;</td>";
		}
		else//display realtor information
		{
			if ($buying == "yes")
			{
				echo "<td>$buying_state - $$buying_value</td>";
			}
			else
				echo "<td>&nbsp;</td>";
			if ($selling == "yes")
			{
				echo "<td>$selling_state - $$selling_value</td>";
			}
			else
				echo "<td>&nbsp;</td>";
		}
			
		echo "<td> ".formatTime($received)." </td>";
		echo "<td title='$comments'> Comments...</td>";
		
		if ($table == "irelocation.leads_auto" || $table == "irelocation.leads_realtor")
		{
			echo "<td> $campaign </td>";
			echo "<td> $referer </td>";
		}
		else
			echo "<td> $source </td>";
		echo "<td title='".str_replace('|',' ',$lead_ids)."'> Leads.. </td>";		
		echo "</tr>";
		$count++;
	}
	echo "</table><br/>";
	echo "<span class='mainmovingtopic'>$count</span><span class='mainmovingtext'> leads</span>";

?>
