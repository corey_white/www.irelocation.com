<?
	define(PIPE,"||");
	define(PIPE_LENGTH,2);
	define(maybe,(rand(0,100) > 50));
	include_once "../inc_mysql.php";
	include_once "countryfunction.php";
	include_once "save_response.php";
	
	function saveSirvaResult($response,$rsmyarray)
	{
		$tempmove=explode("<MoveId>",$response);
		$newmove=explode("</MoveId>",$tempmove[1]);
		
		$sql="insert into movesirva (received,move_id,quote_id,cat_id,source) values ('".$rsmyarray["received"].
		"','".$newmove[0]."','".$rsmyarray["quote_id"]."','".$rsmyarray["cat_id"]."','".$rsmyarray["source"]."')";
		$rs=new mysql_recordset($sql);
		
		return $newmove[0];
	}
	
	function saveMoveDirectResult($quote_id,$received,$success,$response,$cat_id,$source)
	{
		$sql = "insert into movingdirectory.movedirect set quote_id = $quote_id, ".
				" received = $received, success = $success, cat_id = $cat_id, ".
				" source = '$source', record_key = '$response'";				
		mail("david@irelocation.com","MoveDirect - ".(($success==1)?"Success":"Failure"),$sql);	
		$rs = new mysql_recordset($sql);
		$rs->close();			
	}
	
	
	class MovingQuoteData
	{
		var $myarray;
		
		function MovingQuoteData($thearray)
		{
			$this->myarray = $thearray;
			
			$this->myarray["fullname"] = $this->myarray["name"];
			list($this->myarray["firstname"],$this->myarray["lastname"]) = split(" ",$this->myarray["name"],2);
			
			$r_year=substr($this->myarray["received"],0,4);
			$this->myarray["ryear"] = $r_year;
			
			$r_month=substr($this->myarray["received"],4,2);			
			$this->myarray["rmonth"] = $r_month;
			
			$r_day=substr($this->myarray["received"],6,2);
			$this->myarray["rday"] = $r_day;
			
			$r_hour=substr($this->myarray["received"],8,2);
			$r_min=substr($this->myarray["received"],10,2);
			$this->myarray["received_standard"] = "$r_month/$r_day/$r_year $r_hour:$r_min";
			
			$temp_year=substr($this->myarray["est_move_date"],0,4);
			$temp_month=substr($this->myarray["est_move_date"],5,2);
			$temp_day=substr($this->myarray["est_move_date"],8,2);
		
			list($this->myarray["emonth"],$this->myarray["eday"],$this->myarray["eyear"]) = 
			explode("-",date("m-d-Y",mktime(0,0,0,$temp_month,$temp_day,$temp_year)));
			
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);
			$this->myarray["phone_dashes"] = "$p_area-$p_prefix-$p_suffix";
			
			$p2_area=substr($this->myarray["phone_work"],0,3);
			$p2_prefix=substr($this->myarray["phone_work"],3,3);
			$p2_suffix=substr($this->myarray["phone_work"],6,4);
			$this->myarray["phone_work_dashes"] = "$p2_area-$p2_prefix-$p2_suffix";
			
			$this->myarray["destination_zip"] = $this->getDestinationZip($this->myarray["destination_city"], $this->myarray["destination_state"]);
						
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Type of Move: ','',$mycomments);
			$mycomments=str_replace('Type of Home: ','',$mycomments);
			$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
			$mycomments=str_replace('Elevator: ','',$mycomments);
			$mycomments=str_replace('Stairs: ','',$mycomments);
			$mycomments=str_replace('Comments/Unique Items: ','',$mycomments);
			$this->myarray["wheaton_comments"] = $mycomments;
			$details=explode("\n",$mycomments);
			$this->myarray["type_of_move"] = trim($details[0]);
			$this->myarray["mycomments"] = $details[6];
			$this->myarray["moving_comments"] = "<tr><td width='200'>Type of Move: </td><td>$details[0]</td></tr>
			<tr><td width='200'>Type of Home: </td><td>$details[1]</td></tr>
			<tr><td width='200'>Furnished Rooms: </td><td>$details[2]</td></tr>
			<tr><td width='200'>Elevator: </td><td>$details[3]</td></tr>
			<tr><td width='200'>Stairs: </td><td>$details[4]</td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			<tr><td width='200'>Comments/Unique Items: </td><td>$details[6]</td></tr>";
			$this->myarray['nationwide_comments'] = str_replace('\n','',$this->myarray["comments"]);
		
		}
		
		function getSirvaCodes()
		{
			echo "Running Sirva Code<Br/>";
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Type of Move: ','',$mycomments);
			$mycomments=str_replace('Type of Home: ','',$mycomments);
			$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
			$mycomments=str_replace('Elevator: ','',$mycomments);
			$mycomments=str_replace('Stairs: ','',$mycomments);
			$mycomments=str_replace('Comments/Unique Items: ','',$mycomments);
			$this->details=explode("\n",$mycomments);
			
			$details[1]=strtolower($details[1]);
			$rooms=substr($details[2],0,4);
			$small_move = false;
			switch($rooms)
			{
				case "no b":
					$small_move = true;
					if($details[1]=='house') $dwelling="H1";
					elseif($details[1]=='apartment') $dwelling="A1";
					elseif($details[1]=='condo') $dwelling="A1";
					else $dwelling="S2";		  
					break;
				case "one ":
					$small_move = true;
					if($details[1]=='house') $dwelling="H1";
					elseif($details[1]=='apartment') $dwelling="A1";
					elseif($details[1]=='condo') $dwelling="A1";
					else $dwelling="S2";
					break;
				case "two ":
					if($details[1]=='house') $dwelling="H2";
					elseif($details[1]=='apartment') $dwelling="A2";
					elseif($details[1]=='condo') $dwelling="A2";
					else $dwelling="S2";
					break;
				case "thre":
					if($details[1]=='house') $dwelling="H3";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
				case "four":
					if($details[1]=='house') $dwelling="H4";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
				default:
					if($details[1]=='house') $dwelling="H4";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
			}
			
			$this->myarray["sirva_dwelling"] = $dwelling;			
			$this->myarray["sirva_fcountry"] = get_country_code($this->myarray["origin_country"]);
			$this->myarray["sirva_dcountry"] = get_country_code($this->myarray["destination_country"]);
			$source = strtolower($this->myarray["source"]);
			
			if ($this->myarray["cat_id"] == 3 && (substr_count($source,"pm") > 0 || substr_count($source,"me") > 0))
				$lead_code = "PROMVG";
			else if ($source == 'pm_silver')
				$lead_code = "IRELTST";
			else if (substr_count($source,"me") >0 || substr_count($source,"pe") >0 )
					$lead_code = "PROMVG";	
			else
					$lead_code = "IRELOPH";
			$this->myarray["sirva_lead_code"] = $lead_code;
			echo "Sirva Code Done<br/>";
		}
		
		function get($key,$url=0)
		{
			if (substr_count($key,"sirva_lead_code") > 0)
			{
				echo "Key: ".$key."<br/>";
				$code = true;
			}
				
			if (substr_count($key,"url_") > 0)
			{
				$key = substr($key,4);
				$url = 1;
			}
			if (substr_count($key,"xml_") > 0)
			{
				$key = substr($key,4);
				echo $key."<br/>";
				$url = 2;
			}
			
			if (strlen($this->myarray[$key]) > 0)
			{
				$v =  $this->myarray[$key];
				if ($code)
					echo "sirva lead code: ".$v;
				if ($url == 1)
					return urlencode($v);
				else if ($url == 2)
					return $this->xmlentities($v);
				else return $v;
			}
			else			
				return "keyword '$key' not found<br/>";
		}
		
		function xmlentities ( $string )
		{
		   return str_replace ( 
		   	array ( '&', '"', "'", '<', '>' ), 
			array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
			$string 
			);
		}

		
		function getDestinationZip($city,$state)
		{
			$destination_zip="";
			if((trim($city)!='') && ($state!=''))
			{
				$nomore=0;
				$sql="select zip from zip_codes where lower(city) like '%".strtolower(trim($city))."%' and state='".$state."'";
				$rs2=new mysql_recordset($sql);
				if($rs2->rowcount()>0)
				{
					$rs2->fetch_array();
					$destination_zip=$rs2->myarray["zip"];
				}
				else
				{
					$city_length=strlen(trim($city));
					$mycount=10;
	
					if($city_length<10)
						{$mycount=$city_length;}
	
					for($i=$mycount;$i>=3;$i--)
					{
						if($nomore==0)
						{
							for($c=0;$c<$city_length-$i+1;$c++)
							{
								if($nomore==0)
								{
									$city_like=substr(strtolower(trim($city)),$c,$i);
									$sql="select zip from zip_codes where lower(city) like '%$city_like%' and state='".$state."'";
									$rs2=new mysql_recordset($sql);
									if($rs2->rowcount()>0)
									{
											$rs2->fetch_array();
											$destination_zip=$rs2->myarray["zip"];
											$nomore=1;
									}
								}
							}
						}
					}
				}
				if($destination_zip=="")
				{
					if(intval($city)>0)
					{
						$destination_zip=intval($city);
						$sql="select city,state from zip_codes where zip='$destination_zip'";
						$rs2=new mysql_recordset($sql);
						$rs2->fetch_array();
					}
				}
				return $destination_zip;
			}
		}
		function findAllOccurences($Haystack, $needle, $limit=0)
		{
		  $Positions = array();
		  $currentOffset = 0;
		  $count=0;
		  while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
		  {
		   $Positions[] = $pos;
		   $offset = $pos + strlen($needle);
		   $count++;
		  }
		  return $Positions;
		}
		
		function merge($type)
		{
			//echo "Merging Data<br/>";
			$success = 1;
			$offset = 0;
			$variables = array();
			$results = $this->findAllOccurences($this->lead_body,PIPE);
			for ($i = 0; $i < count($results); $i +=  2)
			{				
				$variable = substr($this->lead_body,$results[$i],$results[$i+1]-$results[$i]+PIPE_LENGTH);				
				$variable = str_replace(PIPE,"",$variable);
				$variables[] = $variable;				
			}
			$variables = array_unique($variables);
			foreach ($variables as $v)
			{
				if ($type == "xml")
					$find_variable = "xml_$v";
				//else if ($type == "post")
				//	$find_variable = "url_$v";
				else
					$find_variable = $v;
				$find_variable = str_replace("xml_xml_","xml_",$find_variable);
				$find_variable = str_replace("url_url_","url_",$find_variable);
				
				if (substr_count($find_variable,"url_") > 0)
					echo $find_variable."<br/>";	
				$value = $this->get($find_variable);
				if (substr_count($value,"keyword") > 0)
					$success = 0;
									
				$this->lead_body = str_replace("||$v||",$value,$this->lead_body);
			}
			return $success;
		}
		
		function getMoveDirectCodes()
		{
			extract($this->myarray);
		
			$phone[0] = substr($phone_home,0,3);		
			$phone[1] = substr($phone_home,3,3);
			$phone[2] = substr($phone_home,6,3);
			$this->myarray["user_phone"] = "user_phone1=".$phone[0]."&user_phone2=".$phone[1]."&user_phone3=".$phone[2];
			
			$altphone[0] = substr($phone_work,0,3);
			$altphone[1] = substr($phone_work,3,3);
			$altphone[2] = substr($phone_work,6,3);
			$this->myarray["user_altphone"] = "user_altphone1=".$altphone[0]."&user_altphone2=".
												$altphone[1]."&user_altphone3=".$altphone[2];
			
			$f = strpos($comments,"Furnished Rooms:");
			$b = strpos($comments,"bed",$f);
		
			$rooms = substr($comments,$f,$b-$f);
			$rooms = str_replace("Furnished Rooms","House",$rooms);
			$rooms = str_replace("Other","House",$rooms);
			$rooms = str_replace("Office","Condo",$rooms);
			$rooms = str_replace("no","1",$rooms);
			
			$rooms = str_replace(array("one","two","three","four","five","six"),array(1,2,3,4,5,6),$rooms);
			
			list($type,$count) = split(":",$rooms);
	
			if (strlen($type) == 0)
				$type = "House";
			
			if (strlen($count) == 0 || !is_numeric($count))
				$count = 3;
			
			$types["House"] = 540;
			$types["Apartment"] = 546;
			$types["Condo"] = 552;
			
			$this->myarray["dwelling_id"] = ($types[$type]+$count-1);					
		}
		
		function process($lead_array)
		{
			$this->lead_body = $lead_array["lead_body"];
			
			if ($lead_array["lead_id"] == 1387)
				$this->getSirvaCodes();			
			else if ($lead_array["lead_id"] == 1527)			
				$this->getMoveDirectCodes();
			
			
			$success = $this->merge($lead_array["format_type"]);
			if ($success == 1)
			{
				//lead_id,lead_body,format_type,email
				if ($lead_array["format_type"] == "email")
				{
					
					list ($subject,$body) = split("\n",$this->lead_body,2);
					list($headers,$body) = split("---------------",$body,2);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = implode("\n",$body_parts);
					
					//mail($lead_array["email"].",david@irelocation.com",$subject,trim($body),trim($headers));
					mail("david@irelocation.com",$subject,trim($body),trim($headers));
				}
				else if ($lead_array["format_type"] == "xml")// curl POST
				{													 
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
					//$ch=curl_init("http://www.irelocation.com/reporting/receive.php");
					$ch=curl_init(trim($url));
					
					foreach($variables as $v)
					{
						if (strlen(trim($v)) > 0)
						{
							list($k,$va) = split(":",$v,2);							
							
							if ($k == "validation")
								$validation = $va;
							else
							{								
								echo "curl_setopt(\$ch,$k,$va);<br/>";
								if ($k == "CURLOPT_HTTPHEADER")
								{
									if (substr_count(trim($va),",") > 0)
										$va = split(",",$va);
									curl_setopt($ch,CURLOPT_HTTPHEADER,$va);
									echo "<pre>".print_r($va,true)."</pre>";
								}
								else
								{
									curl_setopt($ch, trim($k), trim($va));
								}
							}
						}
					}
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, trim($body));
					
					$response = curl_exec($ch);
					curl_close($ch);
					
					validateResponse($response,$validation, $this->myarray);
					
					mail("david@irelocation.com","XML Moving Lead Lead",
						"XML:".$body."\nQuote_id:".$this->myarray["quote_id"]."\nResponse:".$response);
					
					/*
					echo "URL: <input type='text' name='url' value='$url' /><br/>";
					echo "Valid Response Test: <input type='text' name='validation' value='$validation' /><br/>";
					echo "<textarea cols='50' rows='30'>$body</textarea><br/>";
					echo "Response: <input type='text' name='results' value='$results' /><br/>";
					*/
					//
				}
				else if ($lead_array["format_type"] == "post")//faturl
				{
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
					/*
					$ch=curl_init($url."?".$body);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$results = curl_exec($ch);
					curl_close($ch);
					*/
					
					validateResponse($response,$validation, $this->myarray);
					
					mail("david@irelocation.com","Fat Url Moving Post","URL:".$url."?".$body."\n"."Response:".$results);					
				}
			}
			else
			{
				mail("david@irelocation.com","Failed to Format Moving Lead",print_r($this->myarray,true)."\n\n".$this->lead_body);
			}
		}
	}
	
	function validateResponse($response,$company, $rsmyarray)
	{
		if ($company == "movebekins")
		{
			saveResponse($rsmyarray["quote_id"],$response,"movebekins");
			if(substr_count(strtolower($data),"transfer failed")>0)
			{
				mail("code@irelocation.com,david@irelocation.com",
					"Bekins Post Failure",$date."\n\n"."quote_id: ".$rsmyarray["quote_id"]);
			}
		}
		else if ($company == "movewheaton")
		{
			saveResponse($rsmyarray["quote_id"],$response,"movewheaton");
	
			if (strpos(strtolower($response),"lead accepted") === FALSE)//give both of us errors..
			{	
				mail("dave@irelocation.com,code@irelocation.com","Wheaton XML Error", 
					$wheaton_xml."<br/><Br/>Response: $response" , 
					"From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$email."");
			}
		
		}
		else if ($company == "movedirect")
		{		
			if (substr_count($response,"Created:") > 0)
			{
				$success = 1;
				$response = str_replace("Created:","",$response);
			}
			else
				$success = 0;	
			
			saveMoveDirectResult($rsmyarray["quote_id"],$rsmyarray["received"],$success,
						$response,$rsmyarray["cat_id"],$rsmyarray["source"]);
		}
		else if ($company == "movesirva")
		{					
			$sirva_result = saveSirvaResult($response,$rsmyarray)
			if($sirva_result == "-1")
			{
				mail("code@irelocation.com","Sirva Error Message", "$response\n\nquote_id: ".$rsmyarray["quote_id"]
					, "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rsmyarray["email"]."");
			}
		}
	
	}
	
	
	$sql = "select * From movingdirectory.quotes where email = 'david@irelocation.com' and cat_id = 2 limit 1;";
	
	//$sql = "select * from movingdirectory.quotes where quote_id > 1644840 and lead_ids like '%1518%';";
	echo $sql."<br/>";
	$rs = new mysql_recordset($sql);
	while ($rs->fetch_array())
	{
		//echo $rs->myarray["quote_id"];
		$data = new MovingQuoteData($rs->myarray);
		$sql = "select 
	(select email from movingdirectory.rules where lead_id = lf.lead_id) as 'email',
	lead_id,
	format_type,
	if(
		lead_body < 0,
		(select lead_body from movingdirectory.lead_format where lead_id = lf.lead_body),
		lf.lead_body
	  ) as 'lead_body'
	from 
	movingdirectory.lead_format as lf
where cat_id = 2 and lead_id = 1386
group by lead_body;";
		echo $sql."<br/>";
		$rs2 = new mysql_recordset($sql);
		$all_variables = array();
		
		while ($rs2->fetch_array())
		{							
			$lead_body = $rs2->myarray["lead_body"];
			$format_type = $rs2->myarray["format_type"];
			$data->process($rs2->myarray);			
		}	
	}
	
	
	
	
?>