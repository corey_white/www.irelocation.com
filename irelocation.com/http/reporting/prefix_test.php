<?php
/* 
Description: This function takes an array of prefixes that are not allowed and tests to see of the prefix is in the array. 
This allows for a subsequent test sequence on numerous prefixes
*/

function checkAllowedPrefix($prefix,$bad_prefix) {
	
	//-- Check to see if prefix is in the array of non-allowed prefixes
	if ( in_array($prefix,$bad_prefix) ) {
		return false; //-- prefix not allowed
	} else {
		return true; //-- prefix allowed
	}
	
}

//-- Set up the list of bad prefixes
$bad_prefix = array("111","222","333","444","555","666","777","888","999","000");

//-- Prefix that we're testing
$prefix = "618";
echo "Prefix is $prefix<br />";


//-- send data to function for testing
if ( checkAllowedPrefix($prefix,$bad_prefix) ) {
    echo "prefix allowed<br />";
} else {
    echo "prefix not allowed<br />";
}

echo "<br />";
echo "Just for spit and giggles, here is the list of bad prefixes (prefix above should not be listed here if deemed allowed):<br />";
print_r($bad_prefix);

?>