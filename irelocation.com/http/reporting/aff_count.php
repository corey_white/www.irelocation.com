<?php 

//-- Set for register_globals off
function auth($username,$password) {
  //-- global $_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']; //-- not needed in PHP5

  if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Leads"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Authorization Required.';
    exit;
  } else if (isset($_SERVER['PHP_AUTH_USER'])) {
      if (($_SERVER['PHP_AUTH_USER'] != $username) || ($_SERVER['PHP_AUTH_PW'] != $password)) {
      header('WWW-Authenticate: Basic realm="Leads"');
      header('HTTP/1.0 401 Unauthorized');
      echo 'Authorization Required.';
      exit;
      }
  }
}

auth("irelo","nosoupforyou");

include "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYY-MM-DD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

//-- If there is no start and end date, then set to first day of month and current day

if ( !$startDate ) {
    $startDate = date("m/d/Y", strtotime(date('m').'/01/'.date('Y').' 00:00:00'));
} 

if ( !$endDate ) {
    $endDate = date('m/d/Y');
} 

echo "<form action=\"aff_count.php\" method=\"get\">";
echo "<table cellspacing=\"2\" cellpadding=\"2\" align=\"center\" bgcolor=\"#fefff5\"  style=\"border-color: black;  border-width: 1px;  border-style: solid; border-collapse: collapse;\">";

echo "<tr><td colspan=\"3\" style=\"border-color: black;  border-width: 1px;  border-style: solid; border-collapse: collapse;\">Start Date: <input name=\"startDate\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"$startDate\"> End Date: <input name=\"endDate\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"$endDate\"> Source Filter: <input name=\"sourceFilter\" type=\"text\" size=\"20\" value=\"$sourceFilter\"> <input name=\"Go\" type=\"submit\" value=\"Go\"></td>";

echo "</table>";

echo "<input name=\"sort\" type=\"hidden\" value=\"$sort\">";
echo "</form>";

//-- Set some vars to 0
$dateUpBorder = 0;
$dateDownBorder = 0;
$sourceUpBorder = 0;
$sourceDownBorder = 0;
$countUpBorder = 0;
$countDownBorder = 0;
$campaignUpBorder = 0;
$campaignDownBorder = 0;
$total = 0;

if ( $sourceFilter ) {
	$filterBy = "and source like '%$sourceFilter%'";
} else {
	$filterBy = "";
}



if ( $startDate && $endDate ) {
    
    $startDateYF = dateYearFirst($startDate);
    $endDateYF = dateYearFirst($endDate);
    
	switch ( $sort ) {
		case "dateUp":
			$orderBy = "order by received ASC";
			$dateUpBorder = 1;
			break;
		case "dateDown":
			$orderBy = "order by received DESC";
			$dateDownBorder = 1;
			break;
		case "sourceUp":
			$orderBy = "order by source ASC";
			$sourceUpBorder = 1;
			break;
		case "sourceDown":
			$orderBy = "order by source DESC";
			$sourceDownBorder = 1;
			break;
		case "countUp":
			$orderBy = "order by count ASC";
			$countUpBorder = 1;
			break;
		case "countDown":
			$orderBy = "order by count DESC";
			$countDownBorder = 1;
			break;
		case "campaignUp":
			$orderBy = "order by campaign ASC";
			$campaignUpBorder = 1;
			break;
		case "campaignDown":
			$orderBy = "order by campaign DESC";
			$campaignDownBorder = 1;
			break;
		default:
			$orderBy = "order by received,source";
			break;
	}
	
	$sql = "select received, source, count(source) as count, campaign from leads_security where own_rent = 'own' and  left(received,8) >= '$startDateYF' and left(received,8) <= '$endDateYF' and source not like '%clkbth%' $filterBy group by source $orderBy";
	$rs = new mysql_recordset($sql);

} 

echo "<table cellspacing=\"2\" cellpadding=\"2\" align=\"center\" bgcolor=\"#fefff5\"  style=\"border-color: black;  border-width: 1px;  border-style: solid; border-collapse: collapse;\">";
echo "<tr><td colspan=\"3\" style=\"border-color: black;  border-width: 1px;  border-style: solid; border-collapse: collapse;\"><h3>Non-Clickbooth Affiliate Counts for $startDate to $endDate (<u>no renters</u>):</h3></td>";
echo "<tr>";

//-- Clear All line
echo "<tr><td colspan=\"3\" style=\"border-color: black; border-width: 1px; border-style: solid; border-collapse: collapse; text-align: right;\"><a href=\"aff_count.php\">Clear All</a></td></tr>";


/*
echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse; width:150px;\"><b>Date Code</b> ";
echo "<a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=dateUp&sourceFilter=$sourceFilter\"><img src=\"images/arrow_up.png\" border=\"$dateUpBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo " <a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=dateDown&sourceFilter=$sourceFilter\"><img src=\"images/arrow_down.png\" border=\"$dateDownBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo "</td>";
*/


echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse; width:35.0px;\"><b>Source Code</b> ";
echo "<a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=sourceUp&sourceFilter=$sourceFilter\"><img src=\"images/arrow_up.png\" border=\"$sourceUpBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo " <a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=sourceDown&sourceFilter=$sourceFilter\"><img src=\"images/arrow_down.png\" border=\"$sourceDownBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo "</td>";


echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse; width:100px;\"><b>Count</b> ";
echo "<a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=countUp&sourceFilter=$sourceFilter\"><img src=\"images/arrow_up.png\" border=\"$countUpBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo " <a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=countDown&sourceFilter=$sourceFilter\"><img src=\"images/arrow_down.png\" border=\"$countDownBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo "</td>";


echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse; width:150px;\"><b>Campaign</b> ";
echo "<a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=campaignUp&sourceFilter=$sourceFilter\"><img src=\"images/arrow_up.png\" border=\"$campaignUpBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo " <a href=\"aff_count.php?startDate=$startDate&endDate=$endDate&sort=campaignDown&sourceFilter=$sourceFilter\"><img src=\"images/arrow_down.png\" border=\"$campaignDownBorder\" width=\"15\" height=\"15\" border=\"0\" align=\"bottom\"></a>";
echo "</td>";

echo"</tr>";

while ($rs->fetch_array()) {
	
	$sourceLimit = 50;
	if ( strlen($rs->myarray["source"]) > $sourceLimit )  {
		$sourcex = substr($rs->myarray["source"],0,$sourceLimit) . "...";
	} else {
		$sourcex = $rs->myarray["source"];
	}

	$footemp = $rs->myarray["received"];
	$received =  substr($footemp,4,2) . "-" . substr($footemp,6,2) . "-" . substr($footemp,0,4) . " " . substr($footemp,8,2) . ":" . substr($footemp,10,2) . ":" . substr($footemp,12,2);
	
	$total += $rs->myarray["count"];
	
	echo "<tr>";
	#echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\">$received</td>";
	echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\">$sourcex</td>";
	echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\">" . $rs->myarray["count"] . "</td>";
	echo "<td style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\">" . $rs->myarray["campaign"] . "</td>";
	echo"</tr>";
}

//-- Total line
echo "<tr><td>&nbsp;</td><td style=\"border-color: black; border-top-width: 2px; border-top-style: thick; border-collapse: collapse; text-align: right;\">Total: $total</td><td>&nbsp;</td></tr>";

//-- Clear All line
echo "<tr><td colspan=\"3\" style=\"border-color: black; border-width: 1px; border-style: solid; border-collapse: collapse; text-align: right;\"><a href=\"aff_count.php\">Clear All</a></td></tr>";


echo "</table>";


?>