<?php
	include "../inc_mysql.php";
	
	if (isset($_POST["month"]))
	{
		$month = $_POST["month"];
		$year = $_POST["year"];	
	}
	else
	{
		$year = date("Y");
		$month = date("m");
	}
	
	function getSources($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "select distinct(campaign) campaign ,count(campaign) count from movecomp.leads_prd where length(campaign) > 0 and leadDate like '$year-$month-%' group by campaign;";
		$rs = new movecompanion($sql);
		$results = array();
		while ($rs->fetch_array())
		{
			$results[] = array($rs->myarray['campaign'],$rs->myarray['count']);		
		}
		return $results;
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
	
	function print_realtor($title, $month,$year,$source="")
	{
		if ($source != "")
		{
			$campaign = " and campaign = '$source' ";
			$source = "&campaign=$source";
		}
		$total_sql = "select count(*) total from movecomp.leads_prd where leadDate like '$year-$month%' $campaign;";

		$rs = new movecompanion($total_sql);
		$rs->fetch_array();
		$total = $rs->myarray["total"];
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		$last = -1;
		$sql = "select substring(leadDate,9,2) 'day', count(leadDate) 'count'  from movecomp.leads_prd where leadDate like '$year-$month%' $campaign group by substring(leadDate,9,2);";
		
		$link_base = "realtor_leads.php?year=$year&month=$month$source";
		
		$rs = new movecompanion($sql);
		
		echo "<tr>\n<td align='left'><Strong>$title</strong></td>";
		
		while ($rs->fetch_array())
		{			
			$counter++;			
			$day = $rs->myarray["day"];

			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				echo "<a href='$link_base&day=$day'>";
				echo "$count";
				echo "</a>";
				echo "</td>";				
			}			
		}				
		if ($day == $ndays)
		{
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else if ((intval($ndays) - intval($last)) > 0)
		{
			echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else if ($day < $nday)
		{
			$delta = $ndays - $day;
			echo "<td colspan='$delta'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else
		{
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		echo "</tr>\n";
	}	
	?>
		<html>
		<head>
		<title>iRelocation Reporting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="irelocation.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<div align="center">
			<form method="post" action="<?= $_SESSION['PHP_SELF'] ?>?page_id=11">
			<table width="80%">
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td>&nbsp;</td>
					<td class="mainmovingtopic" colspan="2">iRelocation Realtor Leads</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">
							<select name="month"  class="mainmovingtopic">
								<option value="08" <? if ($_POST["month"] == "08") echo "selected ";?>>August</option>
								<option value="09" <? if ($_POST["month"] == "09" || $month == '09') echo "selected ";?>>September</option>
								<option value="10" <? if ($_POST["month"] == "10") echo "selected ";?>>October</option>
							</select>						
						</td>
					</tr>										
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">						
							<select name="year" class="mainmovingtopic" >
								<option value="2006"<? if ($_POST["year"] == "2006") echo "selected ";?>>2006</option>
								<option value="2007"<? if ($_POST["year"] == "2007") echo "selected ";?>>2007</option>
							</select>						
						</td>
					</tr>										
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2"><input type="submit" value="Show Leads" class="mainmovingtopic" /></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td width="25">&nbsp;</td>
						<td width="*">
						<?					
							print_month_header($month,$year);		
							$sources = getSources($month,$year);			
											
							print_realtor("All Leads",$month,$year);
							foreach($sources as $source)
							{								
								print_realtor(ucwords($source[0])."  (".$source[1].")",$month,$year,$source[0]);
							}
							echo "</table>";	
						?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td><td align="left" ><a href="logout.php" class="mainmovingtext">&lt;Logout&gt;</a></td>						
					</tr>
					<tr class="mainmovingtext">
						<td>&nbsp;</td><td>you will be logged out after 20 minutes of inactivity.</td>
					</tr>
			</table>		
			</form>
		</div>
	<?php echo  " <!--- ".$_SESSION["time"]." --->";?>
	

