<?
	include_once "../inc_mysql.php";
	include "CompanyClasses.php";
	
	#
# Clean up entries
#
	$mytime = date("YmdHis");
	$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array())
	{
		$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
		$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
		$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
		$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
		$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
		$neworigin_state=strtoupper($rs->myarray["origin_state"]);
		$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
		$newdestination_state=strtoupper($rs->myarray["destination_state"]);
		$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
		$newsource=str_replace("'","",strtolower($rs->myarray["source"]));
		$rs2=new mysql_recordset("update movingdirectory.quotes set name='$newname',email='$newemail',".
		"phone_home='$newphone_home',".
		"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
		"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
		"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'");
	}	

	$mytime = substr($mytime,0,10);//YYYYMMDDHH

	$sql = "select * from movingdirectory.quotes where ready_to_send = 1 AND not in_check_queue ".
			" AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";
	
	$rs = new mysql_recordset($sql);
	$count = 0;
	while ($rs->fetch_array())
	{				
		$count++;
		if ($rs->myarray["cat_id"] == 1)
		{
			$data = new CarQuoteData($rs->myarray);
			echo "Processing Auto-Quote<br/>\n";
		}
		else if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
		{
			$data = new MovingQuoteData($rs->myarray);
			echo "Processing Moving-Quote<br/>\n";
		}
			
		$comp_sql = getCompanySQL($rs->myarray["source"],$rs->myarray["cat_id"]);
		echo "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
		$leads_rs = new mysql_recordset($comp_sql);
		$lead_count = 0;
		while ($leads_rs->fetch_array())
		{							
			$lead_count++;
			$data->process($leads_rs->myarray);			
		}	
		$data->finishLead();//update ready_to_send = 2.
		
		
		echo "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
	}
	echo $count." leads processed.";
?>