<?
/* 
************************FILE INFORMATION**********************
* File Name:  viewleads_multiple_moving.php
**********************************************************************
* Description:  This is an adaptation on the securityviewleads.php script that will pull all leads for a date range.
**********************************************************************
* Creation Date:  6-5-2009
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

session_start();
include_once('../../class.phpmailer.php'); //-- new class added

$admin = ($_SESSION['user'] == "uberadmin");
$tsv_output = "";

if ($_SESSION['user'] != "uberadmin")
{
	header("Location: login.php");
	exit();	
}

$current_date = date("m-d-Y",strtotime("now"));


function formatData($myarray)
{
	switch($myarray["current_needs"])
	{
		case "new":	$myarray["current_needs"] = "New System";	break;
		case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
		case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
	}

	if ($myarray["quote_type"] == "com")
	{
		switch($myarray["sqr_footage"])
		{				
			case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
			case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
			case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
			case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
			case "0": 
			default:
				$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
		}
	}
	else
	{
		switch($myarray["sqr_footage"])
		{
			case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
			case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
			case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
			case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
			case "0": 
			default:
				$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
		}		
	}
	
	$myarray["received"] = getReceived($myarray["received"]);
	
	return $myarray;
}

function getMapLink($myarray)
{
	return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
}		

function formatPhone($phone)
{	
	if (strlen($phone) == 10)
	{
		$p_area=substr($phone,0,3);
		$p_prefix=substr($phone,3,3);
		$p_suffix=substr($phone,6,4);

		return "($p_area) $p_prefix - $p_suffix";
	}
	else
		return $phone;
}

function getReceived($received)
{
	$r_year=substr($received,0,4);
	$r_month=substr($received,4,2);
	$r_day=substr($received,6,2);
	$r_hour=substr($received,8,2);
	$r_min=substr($received,10,2);
	if (strlen($received) == 14)
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	else
		return " -- ";
}	

function buildTSC($array,$font_counter)
{
	global $body, $tsv_output;
	
	if ($font_counter % 2 == 0)//even
		$style = "on";
	else
		$style = "off";
	
	$array = formatData($array);
	extract($array);
	
	$customerInfo = ucwords($name)."<br/>".$email."<Br/>".$phone_home;
		
	$info = "Est. Move Date: " . $est_move_date."<br/>". "Origin: " . $origin_city.", " . $origin_state." ". $origin_zip . "Destination: " . $destination_city.", " . $destination_state." ". $destination_zip. "<br/>".$comment;
	
	$body .= "<tr class='$style'>";
	$body .= "<td align='center'>$quote_id</td>";
	$body .= "<td align='left' >$customerInfo</td>";			
	$body .= "<td align='left' >$received</td>";			
	$body .= "<td align='left' >$info</td>";
	$body .= "<td align='left' >$source</td>";			
	$body .= "</tr>";
	
	$tsv_output .= "$quote_id";
	$tsv_output .= "	";
	$tsv_output .= "$first_name $last_name";
	$tsv_output .= "	";
	$tsv_output .= "$email";
	$tsv_output .= "	";
	$tsv_output .= "$phone";
	$tsv_output .= "	";
	$tsv_output .= "$received";
	$tsv_output .= "	";
	$tsv_output .= "$info";
	$tsv_output .= "	";
	$tsv_output .= "$source";
	$tsv_output .= "<br />\r\n";
}

// change date format to match db, from MM/DD/YYYY to YYYY-MM-DD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Y-m-d',strtotime("$dateinput"));
    return $newDate;
}

// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}


//-------- BEGIN 
include "../../inc_mysql.php";
include "skin_top.php";



$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$lead_id = $_REQUEST['lead_id'];

$sql_start_date = ereg_replace("-","",dateYearFirst($start_date));
$sql_end_date = ereg_replace("-","",dateYearFirst($end_date));

echo "	
<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>
<form name=\"gid\" action=\"viewleads_multiple_moving.php\" method=\"get\">
	<table cellpadding=\"2\">
		<tr>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Start Date</span></td>
			<td><input type=\"Text\" name=\"start_date\" value=\"$start_date\"><a href=\"javascript:show_calendar4('document.gid.start_date', document.gid.start_date.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a></td>
		</tr>
		<tr>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">End Date</span></td>
			<td><input type=\"Text\" name=\"end_date\" value=\"$end_date\"><a href=\"javascript:show_calendar4('document.gid.end_date', document.gid.end_date.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a></td>
		</tr>
		<tr>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Lead ID</span></td>
			<td><input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\"></td>
		</tr>
		<tr>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Email to send to</span></td>
			<td><input name=\"email\" type=\"text\" value=\"$email\" size=\"30\"><br /><small>(Don't put an email in here unless you want it emailed to that address.  Separate multiple emails with a comma)</small></td>
		</tr>
		<tr>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Format to send:</span></td>
			<td><span style=\"color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">
			<input name=\"email_format\" type=\"radio\" value=\"html\" checked> HTML &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name=\"email_format\" type=\"radio\" value=\"tsv\"> TSV Email &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name=\"email_format\" type=\"radio\" value=\"tsv_file\">TSV Attachment</span></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input value=\"Go\" type=\"submit\"></td>
		</tr>
	</table>
	</form>";

/*
echo "start_date = $start_date<br />";
echo "end_date = $end_date<br />";
echo "lead_id = $lead_id<br />";
*/


if ( $start_date && $end_date && $lead_id ) { //-- if we have a start_date, end_date and lead_id, then process


	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);

	
	if (strlen($lead_id) > 0)
	{
		if (isset($_REQUEST['quote_type']))
			$append = " and q.quote_type = '".$_REQUEST['quote_type']."' ";
	
		$lead_sql = "select * from movingdirectory.quotes as q where ".
					"lead_ids like '%$lead_id%' and left(received,8) >= '$sql_start_date' and left(received,8) <= '$sql_end_date' ";
	
		if (isset($orderby))
		{
			$lead_sql .= " order by $orderby";		
		}
		
		$company_info_sql = "select * from movingdirectory.company as co join ".
							" movingdirectory.campaign as ca on ca.comp_id = co.comp_id ".
							" where ca.lead_id = $lead_id ";
		
		
		$company = new mysql_recordset($company_info_sql);
		$company->fetch_array();
		$comp_name = $company->myarray['comp_name'];
	}
/*
	else
	{
		$lead_sql = "Select * from movingdirectory.quotes as q where source like 'merch_$engine' and left(received,8) >= '$sql_start_date' and left(received,8) <= '$sql_end_date' ";
		$comp_name = ucwords($_SESSION['user']);						
	}
*/
	
		echo "\n\n<!-- $lead_sql -->\n\n";
		echo "\n\n<!-- $company_info_sql -->\n\n";
	
	
/*
	if (isset($_REQUEST['site']))
	{
		$sqlFilter .= " and q.source like '".$_REQUEST['site'];
		if (isset($_REQUEST['engine']))
			$sqlFilter .= "_".$_REQUEST['engine']."%' ";
		else
			$sqlFilter .= "%' ";
	}
	else
	{
		if (isset($_REQUEST['engine']))
			$sqlFilter .= " and q.source like '%".$_REQUEST['engine']."%' and q.source not like '%search%' ";		
	}
*/
	
	$lead_sql  .= $sqlFilter;
							
	
	$leads = new mysql_recordset($lead_sql);
	
	$body .= "<table border=\"0\" width=\"100%\" cellspacing=0 cellpadding=1>
		<Tr><td>&nbsp;</td></tr>
		<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">Company: $comp_name </font></b>
		</td>
		</tr>";
		
		if (strlen($lead_id) > 0) { 
		
		$body .= "<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">Phone: ";
				
				$phone_num = ereg_replace("[^0-9]","",$company->myarray['phone_num']);
				if (strlen($phone_num) > 10)
				{	
					$ext =  "Ext. ".substr($phone_num,10);
					$phone_num = substr($phone_num,1,10);
				}
				if (strlen($phone_num) == 10)
				{
					$a = substr($phone_num,0,3);
					$b = substr($phone_num,3,3);
					$c = substr($phone_num,6);
					$body .= "($a) $b - $c $ext ";
				}
				
										
			$body .= "</font></b>
		</td>
		</tr>
		<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">View Lead Format</font></b>
		</td>
		</tr>";			
		
		} 
		$self_link = $_SERVER['PHP_SELF'];
		
		$body .= "<tr><td>&nbsp;</td></tr>
		<tr><td align='left'>
	
		<table border=\"1\" width=\"100%\" cellspacing='0' cellpadding='3'>
		<tr class=\"toprow\">";
		
			$body .= "<th><A href=\"$self_link?$filterlink&orderby=quote_id\" class='black'>Quote Id</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=email\" class='black'>Customer Info</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=received\" class='black'>Time Stamp</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=sqr_footage\" class='black'>Info</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=source\" class='black'>Campaign</a></th>";
			
		$body .= "</tr>";
		
		//-- TSV HEADER
		$tsv_output = "Quote ID";
		$tsv_output .= "	";
		$tsv_output .= "Name";
		$tsv_output .= "	";
		$tsv_output .= "Email";
		$tsv_output .= "	";
		$tsv_output .= "Phone";
		$tsv_output .= "	";
		$tsv_output .= "Date Received";
		$tsv_output .= "	";
		$tsv_output .= "Info";
		$tsv_output .= "	";
		$tsv_output .= "Source";
		$tsv_output .= "<br />\r\n";

									
		 
		$font_counter = 0;
		$lead_count = 0;
		while($leads->fetch_array())
		{
			$body .= buildTSC($leads->myarray,$font_counter);		
			$font_counter ++;
			$lead_count++;
		}						
		
$body .= "	</table>
	<div align=\"left\">
		<font face='verdana' size=2>
			<strong>$lead_count</strong> Leads.
		</font>
	</div>
";	


} 


//-- if there was an email entered into the form, email the leads to that email address(es)
if ( $email && $email_format != "tsv_file") {
	$SUBJECT = "Leads for $comp_name ( " . dateYearLast($sql_start_date) . " - " . dateYearLast($sql_end_date) . ")";
		
	$mail    = new PHPMailer();
	
	$mail->From     = "noreply@iRelocation.com";
	$mail->FromName = "iRelocation";
	
	$mail->Subject = $SUBJECT;
	
	$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test
	
	if ( $email_format == "tsv" ) {
		$mail->MsgHTML("<pre>" . $tsv_output . "</pre>");
	} else {
		$mail->MsgHTML($body);
	} 
	
	
	$mail->AddAddress("$email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail";
	} else {
	  echo "<span style=\"color: red; font-weight: bold;\">Mail Sent ($email_format)</span>";
	}
	


} elseif ( $email_format == "tsv_file" && $tsv_output ) {

  // create file and email
  	$file_dir = "movingviewleads_temp";
  	$randomFoo = md5(time()); 
  	$filename = date("Ymd",strtotime("now")) . "_" . $randomFoo . ".txt" ;
  	
  	$file = $file_dir . "/" . $filename;
  	
  	echo "\n\n<!-- file = $file -->\n\n";
  	
	$fileptr=fopen($file,"w"); // open the file
	
	$tsv_output = eregi_replace("<br />","",$tsv_output);
	
	fputs($fileptr, $tsv_output);
	
	fclose($fileptr); // closes the file at $fileptr.
	
	$fileatt = $file; // Path to the file                  
	$fileatt_type = "application/octet-stream"; // File Type 
	$fileatt_name = $file; // Filename that will be used for the file as the attachment
	$email_from = "no_reply@irelocation.com"; // Who the email is from 
	$email_subject = "TSV File $current_date"; // The Subject of the email 
	$email_txt = ""; // Message that the email has in it
	$email_to = $email; // Who the email is to
	
	$headers = "From: ".$email_from;
	$file = fopen($fileatt,'rb'); 
	$data = fread($file,filesize($fileatt)); 
	fclose($file);
	
	$semi_rand = md5(time()); 
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
	    
	$headers .= "\nMIME-Version: 1.0\n" . 
	            "Content-Type: multipart/mixed;\n" . 
	            " boundary=\"{$mime_boundary}\"";
	$email_message .= "This is a multi-part message in MIME format.\n\n" . 
	                "--{$mime_boundary}\n" . 
	                "Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
	               "Content-Transfer-Encoding: 7bit\n\n" . 
	$email_message . "\n\n";
	$data = chunk_split(base64_encode($data));
	$email_message .= "--{$mime_boundary}\n" . 
	                  "Content-Type: {$fileatt_type};\n" . 
	                  " name=\"{$fileatt_name}\"\n" . 
	                  //"Content-Disposition: attachment;\n" . 
	                  //" filename=\"{$fileatt_name}\"\n" . 
	                  "Content-Transfer-Encoding: base64\n\n" . 
	                 $data . "\n\n" . 
	                  "--{$mime_boundary}--\n";
	                  
	$ok = @mail($email_to, $email_subject, $email_message, $headers);
	
	if($ok) { 
	echo "<span style=\"color: red; font-weight: bold;\">Mail Sent (TSV File)</span><br>"; 
	} else { 
	echo "Oops... an error was detected.  File not sent.<br>"; 
	}    
}

if ( $tsv ) {
	echo "<pre>";
	echo $tsv_output;
	echo "</pre>";
} else {
	echo "\n\n<!-- body start -->\n\n";
	echo $body;
	echo "\n\n<!-- body end -->\n\n";
} 



include "skin_bottom.php"; ?>
