<? 
	session_start();
	
	$timeCheck = ((date("YmdHis") - $_SESSION['time']) < (4*60*60));
	
	if ($_SESSION['user'] == 'autotransporter' && strlen($_SESSION['lead_id']) > 0 && $timeCheck)
	{
		header("Location: marbleindividualgrid.php");
		$_SESSION['time'] = date("YmdHis");
		exit();
	}
	else if ($_SESSION['user'] == 'uberadmin' && $timeCheck)
	{
		header("Location: marblereport.php"); 
		$_SESSION['time'] = date("YmdHis");
		exit();
	}
	else
		header("Location: login.php"); 
?>