<?
	session_start();
	include "graphs/atus.php";
	define(CPC_DEBUG,true);
	
	$site = ($_POST['site']=="")?"all":$_POST['site'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	
	$graph = new AtusGraph($timeframe,$engine,$site);
	
	echo "<!--- ".$_SESSION['lead_id']." --->";
	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		if (strlen($_SESSION['user']) > 0)
		header("Location: atusindividualgrid.php");
		exit();
	}
	else
		$_SESSION['time'] = date("YmdHis");
	
	include "skin_top.php";

	if (strlen($_POST['ts']) != 6)
		$this_month = date("Ym");
	else
		$this_month = $_POST['ts'];


	$sql = "Select co.comp_name,ca.* from movingdirectory.campaign as ca join ".
			"movingdirectory.company as co on co.comp_id = ca.comp_id where ca.site_id = 'auto' ".
			" and ca.month = '$this_month'";			
	$rs = new mysql_recordset($sql );
	while ($rs->fetch_array())
	{
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}
		
	$today = date("d");
	$days_in_month = date('t');
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	

	$sqlFilter = "";
	echo "<!--- ".print_r($_POST,true)." --->";
	if ($_POST['filter'] == 'Filter')
	{
		if ($_POST['engine'] != "all")
			$sqlFilter .= "and q.source like '%".$_POST['engine']."%' ";
		if ($_POST['site'] != "all") 
			$sqlFilter .= "and source like '".$_POST['site']."%' ";
	}
	echo "<!--- SQL Filter: $sqlFilter --->";
	
	$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received and received like '$this_month%' $sqlFilter and cat_id = 1;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];
	
	$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received and received like '$this_month$today%' $sqlFilter and cat_id = 1;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	$default_order_by = "comp_name,day";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				movingdirectory.campaign as c
				left join 
				movingdirectory.quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'atus'
				AND q.received like '$timeframe%'
				$sqlFilter
				and c.month = '$this_month'			
				and cat_id = 1						
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				$default_order_by;";

	echo "<!--- $sql --->";
	$rs = new mysql_recordset($sql);
	
	
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					movingdirectory.quotes as q
				where 
					q.received like '$timeframe%'					
					$sqlFilter
					and cat_id = 1
				group by
					substring(received,7,2)";
	
	$total_rs = new mysql_recordset($total_sql);
	
	include "cpc_graph.php";
	
	
	?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
		<?	$graph->showGraph(); ?>
		</td></tr>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border='0' width="100%" cellspacing=0 cellpadding=2>
				<tr>
				<td width='40%'>
			<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
		</td>
		<td rowspan="3" width="20%">&nbsp;</td>
		<td rowspan="3" valign="top">
			<b><font face="Verdana" size="2">
			<?
				list($revenue,$cpl,$cost) = $graph->getData($timeframe);
				
				echo "Total Cost: \$".$cost."<br/>\n";
				if (substr_count($revenue,"hit") == 0) 
					echo "Total Revenue: \$"; 
				echo $revenue."<Br/>\n"; 
				echo "Cost Per Lead: \$".$cpl."<br/>\n";
			?>
			</font></b>
		</td>
		</tr>
		<tr><td width='40%'>
			<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
		</td></tr>
		<tr><td width='40%'>
			<b><font face="Verdana" size="2">	
	
			<form action="_atusgrid.php" method="post" name="sitefilter">	
			Period
			<?= printPeriodDropDown("ts",$_POST['ts']); ?><br/>
											
						<input type='hidden' name="filter" value="Filter" />
					Site:
					<select name="site">
 <option value="atus" <? if ($_POST['site'] == 'atus') echo "selected"; ?> >Auto-Transport.us</option>
	<option value="123cm" <? if ($_POST['site'] == '123cm') echo "selected"; ?> >123CarMoving.com</option>
	<option value="all" <? if ($_POST['site'] == 'all') echo "selected"; ?> >All</option>
					</select>
					<br/>
					Search Engines
					<select name='engine' >
						
						<option value='all'>All</option>
					<? 
						$engines = array('google','overture');
						foreach($engines as $engine)
						{
							echo "<option value='$engine'";
							if ($_POST['engine'] == $engine) echo " selected ";
							echo ">".ucwords($engine)."</option>\n";														
						}
					?>										
					</select><Br/>
					<br/>
					<input type='submit' name="filter" value="Filter" />
				</form>
			</font>
			
			</tr></table>
		</td></tr>
		<Tr><td>

<?	
	
	$sql = "select count(*) count from movingdirectory.quotes q where received like '$this_month%' $sqlFilter ";
	echo "<!--- $sql --->";
	$check_for_any = new mysql_recordset($sql);
	$check_for_any->fetch_array();
	if ($check_for_any->myarray["count"] == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	$days = date('t');
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left' class='white'>Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count <= $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
	}
	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	while($rs->fetch_array())
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
				
		
	
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days)//not a full month of data yet.
			{
				echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{
		
			echo "<tr class='$style'><td class='$style' >".($font_counter-1)."</td>";
			
			$link = "<a href='atusformat.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='atusviewleads.php?ts=$timeframe$day$append'>$count</a>";
		else
			$link = "<A class='black'  href='atusviewleads.php?ts=$timeframe$day&lead_id=".$lead_id."$append'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='atusviewleads.php?ts=$timeframe$append'>$total</a>";
	else
		$link = "<A class='black' href='atusviewleads.php?ts=$timeframe&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr></table>";

	include "skin_bottom.php";

?>