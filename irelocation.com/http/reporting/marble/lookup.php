<?
	session_start();

	include "skin_top.php";
	$key = $_REQUEST['key'];
?>
	<div class='filterfont'>
	
	Enter a Companie's ID# or name to Find out <br/>all i know about that company.<br/><br/>
	
	<form action="lookup.php" method="get">
		Company Name/Number:<input type="text" name="key" value="<?= $key ?>" />
		<br/>	
		<input type="submit" name="s" value="Lookup" />
	</form>
<?
	function testCampaignForm($comp_id)
	{
		$errors = array("company not created","lead id not created","format not created","email address not created");
		
		$campaign = $_REQUEST['campaign'];
		
		if ($_REQUEST['error'] != "")
			$error = "Error: ".$errors[$_REQUEST['error']];
		else if (isset($_REQUEST['valid']))
			$error = "This company is ready to start.";
		else if (isset($_REQUEST['added']))
			$error = "This company has been added.";
		?>
		<form action="marbleadd.php" method="post">
			<input type="hidden" name="comp_id" value="<?= $comp_id ?>" />
		<tr>
			<td colspan='2'>			
				<?= $error ?>
			</td>
		</tr>		
		<? if (isset($_REQUEST['valid'])) { ?>
		<tr>
			<td>Campaign:</td>
			<td><?= $campaign ?>
				<input type="hidden" name="campaign" value="<?= $campaign ?>" />
			</td>
		</tr>
		<tr>
			<td>Leads: </td>
			<td>
				<select name='month'>
				<? for ($i = 500; $i < 9000; $i += 500)
					echo "<option>$i</option>\n";
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>Month: </td>
			<td><select name='month'>
					<option><?= date("Ym") ?></option>
					<option><?= date("Ym",time()+30*24*60*60) ?></option>
					<option><?= date("Ym",time()+60*24*60*60) ?></option>				
				</select>
			</td>
		</tr>
		<tr>
			<td colspan='2'>			
				<input type="submit" value="Create Campaign" />
				&nbsp; &nbsp; &nbsp;
				<input type="button" value="Cancel" 
					onclick="window.location='lookup.php?selected=<?= $comp_id ?>';" />
			</td>
		</tr>
		<? } else { ?>
		<tr>
			<td>Campaign:</td>
			<td>
				 <select name='campaign'>
					<option <?= (($campaign=="atus")?"selected ":" ") ?>>atus</option>
					<option <?= (($campaign=="tas")?"selected ":" ") ?>>tas</option>
					<option <?= (($campaign=="auto")?"selected ":" ") ?>>auto</option>
				</select>			
			</td>
		</tr>
		<tr>
			<td colspan='2'>			
				<input type="submit" value="Check" />
			</td>
		</tr>
		<? } ?>
				
		</form>
		
		<?	
	}

	function showResults($comp_id)
	{
		$month = date("Ym");
		$sql = "select 					
	mca_atus.month,
	c.comp_name,
	c.comp_id,
	d.lead_id,
	
	d.cat_id,
	c.contact_name as 'Contact Name',
	c.contact_email as 'Contact Email',
	c.phone_num,

	mlf_atus.format_id as 'ATUS Lead Format ID#',
	mr_atus.lead_email as 'ATUS Lead Email',
	mca_atus.monthly_goal as 'ATUS Leads Monthly Goal',
	
	mlf_auto.format_id as 'Top Auto Leads Format ID#',
	mr_auto.lead_email as 'Top Auto Leads Email',
	mca_auto.monthly_goal as 'Top Auto Leads Monthly Goal'
	
	
from 
	movingdirectory.company as c
	left join movingdirectory.directleads as d on 
		c.comp_id = d.comp_id
	left join marble.companies as mc on mc.comp_id = c.comp_id

	left join marble.rules as mr_auto on mr_auto.lead_id = d.lead_id 
		and mr_auto.site_id = 'auto'
	left join marble.rules as mr_atus on mr_atus.lead_id = d.lead_id 
		and mr_atus.site_id = 'atus'
	
	left join marble.lead_format as mlf_atus on mlf_atus.lead_id = d.lead_id
	and mlf_atus.site_id = 'atus'
	left join marble.lead_format as mlf_auto on mlf_auto.lead_id = d.lead_id
	and mlf_auto.site_id = 'auto'

	left join marble.campaign as mca_auto on mca_auto.lead_id = d.lead_id 
	and mca_auto.month = '$month' and mca_auto.site_id = 'auto'
	left join marble.campaign as mca_atus on mca_atus.lead_id = d.lead_id 
	and mca_atus.month = '$month' and mca_atus.site_id = 'atus'

where 
	c.comp_id = $comp_id
limit 1;



						
						";			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();	
		$data = $rs->myarray;
		
		echo "<!--- ".print_r($data,true)." --->";
		
		if ($data != "" && count($data) > 0)
		{
			
			echo "<table class='filterfont'>";		
			
			echo "<tr><td colspan='2'><a style='color:#0000FF;' href='../excel/output.php?lead_id=".$data['lead_id']."'>Downloadable Report for ".date("F Y")."</a></td></tr>";
			foreach($data as $k => $v)
			{
				if (!is_numeric($k))
				{
					echo "<tr><td align='left'>";
					if (substr_count($k,"ATUS") > 0)
						echo "<a style='color:#0000FF;' href='alteratus.php?comp_id=".$data['comp_id']."'>";
					else if (substr_count($k,"Top Auto Leads") > 0)
						echo "<a style='color:#0000FF;' href='alterauto.php?comp_id=".$data['comp_id']."'>";
					else if (substr_count($k,"Top Auto Shippers") > 0)
						echo "<a style='color:#0000FF;' href='altertas.php?comp_id=".$data['comp_id']."'>";

					echo ucwords(str_replace("_"," ",$k));
					if (substr_count($k,"ATUS") > 0 || substr_count($k,"Top Auto") > 0
						|| substr_count($k,"Top Auto Shippers") > 0)
						echo "</a>";
					echo "</td><td align='left'>$v</td></tr>";			
				}
			}
			echo "<tr><td colspan='2'>&nbsp;</td></tr>";
			
			testCampaignForm($comp_id);
			
			echo "<tr><td colspan='2'>&nbsp;</td></tr>";
			echo "</table>";	
		}
		else
		{
			echo "No Data Found!";
		}
		echo "<br/><br/><a style='color:#0000FF;' href='lookup.php'>Go Back</a>";
	}

	if ($selected == "")
	{
		if ($key != "")// a company wasn't selected yet, but a key was givem so display
		//companies we think they want.
		{
			$sql = "select * from movingdirectory.".
						"company where comp_";
			if (is_numeric($key))
			{
				$sql .= "id = $key ";			
			}
			else
			{
				$sql .= "name like '".$key."%' ";								
			}		
			$rs = new mysql_recordset($sql);
			echo "Results:<br/><br/>";
			if ($rs->rowcount() == 1)
			{
				$rs->fetch_array();
				showResults($rs->myarray['comp_id']);
			}
			else
				while($rs->fetch_array())
				{
					extract($rs->myarray);
					echo "<a style='color:#0000FF;' href='lookup.php?selected=$comp_id' >";
					echo "$comp_id $comp_name";		
					echo "</a><br/>";
				}	
		}
	}
	else// a company was selected. display all info about it.
		showResults($selected);		
	
	echo "</div>";
	include "skin_bottom.php";
?>