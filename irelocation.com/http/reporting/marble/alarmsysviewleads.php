<?
	session_start();
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] == "autotransporter")
	{		
		header("Location: marblegrid.php");
		exit();	
	}
	else if ($_SESSION['user'] == "uberadmin")
	{
		if(strlen($_REQUEST['lead_id']) >= 0)
			$lead_id = $_REQUEST['lead_id'];
		else
		{
			header("Location: marblegrid.php");
			exit();	
		}
	}
	else
	{
		header("Location: login.php");
		exit();	
	}
	
	if (strlen($_REQUEST['ts']) < 6)
	{
		header("Location: alarmsysgrid.php");
		exit();		
	}
	
	
	$ts = $_REQUEST['ts'];

	include "../../inc_mysql.php";
	
	
	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);


	
	function formatData($myarray)
	{
		switch($myarray["current_needs"])
		{
			case "new":	$myarray["current_needs"] = "New System";	break;
			case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
			case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
		}

		if ($myarray["quote_type"] == "com")
		{
			switch($myarray["sqr_footage"])
			{				
				case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
				case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
				case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
			}
		}
		else
		{
			switch($myarray["sqr_footage"])
			{
				case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
				case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
				case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
				case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
				case "0": 
				default:
					$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
			}		
		}
		
		$myarray["maplink"] = getMapLink($myarray);
		$myarray["own_rent"] = ucwords($myarray["own_rent"]);
		$myarray["building_type"] = ucwords($myarray["building_type"]);
		$myarray["other_field"] = ucwords($myarray["other_field"]);
		$myarray["received"] = getReceived($myarray["received"]);
		
		return $myarray;
	}

	function getMapLink($myarray)
	{
		return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
	}		
	
	function formatPhone($phone)
	{	
		if (strlen($phone) == 10)
		{
			$p_area=substr($phone,0,3);
			$p_prefix=substr($phone,3,3);
			$p_suffix=substr($phone,6,4);
	
			return "($p_area) $p_prefix - $p_suffix";
		}
		else
			return $phone;
	}
	
	function getReceived($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		if (strlen($received) == 14)
			return "$r_month/$r_day/$r_year $r_hour:$r_min";
		else
			return " -- ";
	}	
	
	function buildTSC($array,$font_counter)
	{
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		$array = formatData($array);
		extract($array);
		
		if ($quote_type == "res")
			$customerInfo = ucwords($name1." ".$name2)."<br/>".$email."<Br/>".formatPhone($phone1);
		else
			$customerInfo = ucwords($name1."<Br/>".$name2)."<br/>".$email."<Br/>".$phone1;
			
		$location = "<a href='$maplink' class='black' target='_blank' >$address <br/> $city, $state_code $zip</a>";
		if ($quote_type == "res")
			$quote_type = "Residential";
		else
			$quote_type = "Commercial";
		$info = $current_needs."<br/>".$building_type."<br/>".$sqr_footage;
		
		echo "<tr class='$style'>";
		echo "<td align='center'>$quote_id</td>";
		echo "<td align='left' >$customerInfo</td>";			
		echo "<td align='left' >$quote_type</td>";
		echo "<td align='left' >$received</td>";			
		echo "<td align='left' >$location</td>";			
		echo "<td align='left' >$info</td>";
		echo "<td align='left' >$source</td>";			
		echo "</tr>";
	}
	
	
	
	if (strlen($lead_id) > 0)
	{
		if (isset($_REQUEST['quote_type']))
			$append = " and q.quote_type = '".$_REQUEST['quote_type']."' ";

		$lead_sql = "select * from irelocation.leads_security as q where ".
					"lead_ids like '%$lead_id%' and received like '$ts%' ";

		if (isset($orderby))
		{
			$lead_sql .= " order by $orderby";		
		}

		$company_info_sql = "select * from movingdirectory.company as co join ".
							" movingdirectory.campaign as ca on ca.comp_id = co.comp_id ".
							" where ca.lead_id = $lead_id and ca.site_id = 'tsc'";
		
		$company = new mysql_recordset($company_info_sql);
		$company->fetch_array();
		$comp_name = $company->myarray['comp_name'];
	}
	else
	{
		$lead_sql = "Select * from irelocation.leads_security as q where source like 'tsc_$engine' and received like '$ts%' ";
		$comp_name = ucwords($_SESSION['user']);						
	}
	
	if (isset($_REQUEST['site']))
	{
		$sqlFilter .= " and q.source like '".$_REQUEST['site'];
		if (isset($_REQUEST['engine']))
			$sqlFilter .= "_".$_REQUEST['engine']."%' ";
		else
			$sqlFilter .= "%' ";
	}
	else
	{
		if (isset($_REQUEST['engine']))
			$sqlFilter .= " and q.source like '%".$_REQUEST['engine']."%' and q.source not like '%search%' ";		
	}
	
	$lead_sql  .= $sqlFilter;
	$lead_sql  .= " and campaign = 'alarmsys' ";

echo "\n\n<!-- lead_sql = $lead_sql -->\n\n";
	
	$leads = new mysql_recordset($lead_sql);
	include "skin_top.php";
?>	

						<table border="0" width="100%" cellspacing=0 cellpadding=1>
							<Tr><td>&nbsp;</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Company: 
								<?= $comp_name  ?></font></b>
							</td>
							</tr>
							<? if (strlen($lead_id) > 0) { ?>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Phone: 
								<?	
									$phone_num = ereg_replace("[^0-9]","",$company->myarray['phone_num']);
									if (strlen($phone_num) > 10)
									{	
										$ext =  "Ext. ".substr($phone_num,10);
										$phone_num = substr($phone_num,1,10);
									}
									if (strlen($phone_num) == 10)
									{
										$a = substr($phone_num,0,3);
										$b = substr($phone_num,3,3);
										$c = substr($phone_num,6);
										echo "($a) $b - $c $ext ";
									}
									
								?>								
								</font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">View Lead Format</font></b>
							</td>
							</tr>			
							<? }  ?>				
							<tr><td>&nbsp;</td></tr>
							<tr><td align='left'>

							<table border="1" width="100%" cellspacing='0' cellpadding='3'>
	<tr class="toprow">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=quote_id"; ?>' class='black'>Quote Id</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='black'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=quote_type"; ?>' class='black'>Quote Type</a></th		
		><th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=received"; ?>' class='black'>Time Stamp</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=state_code"; ?>' class='black'>Location</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=sqr_footage"; ?>' class='black'>Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=source"; ?>' class='black'>Campaign</a></th>
		
	</tr>
								
								<? 
								$font_counter = 0;
								$lead_count = 0;
								while($leads->fetch_array())
								{
									buildTSC($leads->myarray,$font_counter);		
									$font_counter ++;
									$lead_count++;
								}						
								?>
							</table>
							<div align="left">
								<font face='verdana' size=2>
									<strong><?= $lead_count ?></strong> Leads.
								</font>
							</div>
<? include "skin_bottom.php"; ?>
