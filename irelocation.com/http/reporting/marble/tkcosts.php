<?
	session_start();
	
	include "skin_top.php";
	
	include "data/tracking.php";
	
	echo "<!--- ".print_r($_POST,true)." --->";
	
	$website = ($_POST['website']!="")?$_POST['website']:1;
	$engine = ($_POST['engine']!="")?$_POST['engine']:1;
	
	$sortby = ($_POST['sortby']=="")?"cpa":$_POST['sortby'];
	
	$timeframe = ($_POST['year']!="")?
						$_POST['year'].$_POST['month'].$_POST['day']:
						date("Ymd",time()-24*60*60);
	
	$t = new Tracking($website,$timeframe,$sortby);


	$_SESSION['ajax_table_sql'] = $t->getTestSql($timeframe);
	echo "\n<!--- ".$_SESSION['ajax_table_sql']." --->\n";
	include "table.php";
?>
	<style>
			table.sortable thead 
			{
				background-color:#eee;
				color:#666666;
				font-weight: bold;
				font-family: Verdana;
				font-size:12px;
				cursor: default;
			}
			
			tr
			{
				font-family: Verdana;
				font-size:11px;
				border:solid;
				border-color:#333333;
				
			}
			
			tr.odd
			{
				font-family: Verdana;
				font-size:11px;
				border:solid;
				border-color:#333333;
				background-color:#CCCCCC;			
			}
			
			</style>
	<script type="text/javascript" src="sorttable.js"></script>		
	<script language="javascript" src="../../complicated/ajax.js"></script>
	<script>
		function loadTable()
		{
			rawCall("table.php","callback_table");
		}
		
		function callback_table()
		{
			document.getElementById('tableoutput').innerHTML = AJAX_BUFFER;
		}
	</script>
	<form name='filterform' action="tkcosts.php" method="post">
		<input type='hidden' name="sortby" value="<?= $sortby ?>" />
		<select name='website'>
			<option value="1">CarShipping.com</option>
		</select> - 
		<select name="engine">
			<option value="1">Google</option>
		</select> - 
		<input type="hidden" name="year" value="2007"/>
		<select name="month"><option value="05">May</option></select>
		<select name="day">
		<?
			for ($i = 1; $i < date("d"); $i++)
			{
				$d = ($i<10)?"0$i":$i;
				echo "<option value='$d' ";
				if ($day == $i) echo "selected ";
				echo ">$d</option>";
			}
		?>
		</select>
		
		<input type="submit" value="Update" />
	</form>
	<Br/>
	<? if ($timeframe == date("Ymd")) 
			echo "<strong>Note:</strong> Data for today ".
				" has a 15 minute lag to account for users ".
				" currently on the site.<Br/>"; 
	?>
	<Br/>
	<strong>Date:</strong> <?= $timeframe ?><Br/>
	<strong>Total Clicks:</strong><?= $t->totalClicks ?><br/>
	<strong>Total Conversions:</strong><?= $t->conversions ?><br/>
	<strong>Conversion Rate:</strong><?= number_format($t->conversionRate,2,".",",") ?>%<br/>
	<Br/><Br/>
	<? echo printTable(); ?>
	<? include "skin_bottom.php"; ?>