<?
	define(CPC_DEBUG,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	define(SITES,"smq");
	define(CAMPAIGN,'smq');
	define(SITE_ID,'smq');
	define(SUBMIT_PAGE,"smqgrid.php");
			
	define(SAM,1681);//all leads
	define(SELFMOVESUSA,1560);//all leads
	define(PODS,1628);//all leads
	define(ALBERTMOVING,1643);//all leads
	define(WEHAUL,1647);//all leads
	define(MOVINGSTAFFERS,1683);//all leads
	define(TESTLEAD,9999);//all leads
	define(TESTLEAD2,8888);//all leads


	define(LEAD_SOURCE,"standard");
	define(CAMPAIGN_TYPE,"adshare");
	define(SHOW_CPC_GRAPH,true);
	
	/*
	OPERATIONAL NOTE:
	If you add or delete any companies from the above define statements, you need to change the ARRAY in the getTotalLeadRevenue function in graphs/security.php as well (really bad system - need to update this at some point)
	
	*/
	
	include "securitygrid.php";

?>f