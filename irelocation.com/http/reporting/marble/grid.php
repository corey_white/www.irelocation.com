<?
	/*
		campaign topsecurity, auto-transport.us, moving, etc.		
		
		filters:
			site (tsc,cs,tm,etc...)
			engine (google,msn,yahoo,affiliates)
			timeframe (july,june,august) in yyyymm format.
	*/
	include_once "../../inc_mysql.php";
	include_once "generic.php";
	include "skin_top.php";
?>
<script>
	function checkCombo(site,engine)
	{
		var form = document.forms['sitefilter'];
		var site_value = site.options[site.selectedIndex].value;
		var engine_value = engine.options[engine.selectedIndex].value;		
	
		if (site_value == "aff" && (engine_value == "google" || engine_value == "msn" 
									|| engine_value == "overture" || engine_value == "google_local"))
			alert("If you select 'affiliates', you must select one of the affiliates.");
			
		else if (engine_value != "organic" && engine_value != "google" && engine_value != "msn" && engine_value != "all"
				&& engine_value != "overture" && engine_value != "google_local")
		{
			site.selectedIndex = 1;
			form.submit();
		}
		else
			form.submit();
	}

</script>

<?
	$campaign = $_REQUEST['campaign'];
	
	$month = ($_POST['ts']!="")?$_POST['ts']:date("Ym");
	$engine = ($_POST['engine']!="")?$_POST['engine']:"all";
	$site = ($_POST['site']!="")?$_POST['site']:"all";
	
	$a = getCampaignDetails($campaign,$month);
	
	
	
	
	
	$costs = _getCostByDay($a,$month,$engine,$site);
	$counts = _getCountByDay($a,$month,$engine,$site);
	$totalcount =  getTotalCount($counts);
	$totalcost = getTotalCost($costs);
	$revenue =  _getRevenue($a,$month,$engine,$site,$totalcount);
	$todays_leads= $counts[date("d")]['count'];
	if ($totalcount != 0)
		$cpl = $totalcost/($totalcount - $todays_leads);
	else
		$cpl = 0.00;
	
	if ($cpl < .75*$a['target_price'])
		$font = 'green';
	else if ($cpl < $a['target_price'])
		$font = '#FFCC33';
	else
		$font = '#FF0000';
	
	$totalcost = number_format($totalcost,2,".",",");
	$revenue = number_format($revenue,2,".",",");
	$totalcount = number_format($totalcount,0,".",",");
	$cpl = "<font color='$font'>".number_format($cpl,2,".",",")."</font>";
	?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>

	<Tr><td>&nbsp;</td></tr>
	<tr><td>
	<?	showGraph($costs,$counts);	?>
	</td></tr>
	<Tr><td>&nbsp;</td></tr>
	<tr><td>
		<table border='0' width="100%" cellspacing=0 cellpadding=2>
			<tr>
			<td width='40%'>
		<b><font face="Verdana" size="2">Total Leads: <?= $totalcount ?></font></b> 									
	</td>
	<td rowspan="3" width="20%">&nbsp;</td>
	<td rowspan="3" valign="top">
		<b><font face="Verdana" size="2">
		Total Cost: $<?= $totalcost ?><br/>
		Total Revenue: $<?= $revenue ?><br/>
		Cost Per Lead: $<?= $cpl ?><br/>		
		</font>
		</b>
	</td>
	</tr>
	<tr><td width='40%'>
		<b><font face="Verdana" size="2">Total Leads today: <?= $todays_leads ?></font></b>
		
	</td></tr>
	<tr><td width='40%'>
		<b><font face="Verdana" size="2">	

		<form action="grid.php?campaign=<?= $campaign ?>" method="post" name="sitefilter">	
			<? 	printFilter($a,$timeframe,$engine,$site);	?>
			</form>
		</font>
		</b>
		</tr></table>
	</td></tr>
	<Tr><td>
	
	<?
	
	
	
	
	
	
	
	
	
	//echo "\n<!--- \n";
	//	print_r($a);
	//echo " --->\n";
	
	/*
	echo "\n<!--- \n";
	
	print_r($counts);
	
	print_r($costs);
	
	echo "\n--->\n";
	*/
	
	$comp = _getCompanies($a,$month,$engine,$site);
	
	if (count($counts) == 0)
	{
		echo "No Data";
		exit();
	
	}	
	_printGrid($comp,$counts);
	
	
	
	function _getCostByDay($details,$month,$engine="all",$site="all")
	{		
		if($details['moving'])
		{//percentages table...
			$sql = " select substr(timeframe,7,2) 'day',sum(cost) 'cost' ".
					" from movingdirectory.percentages ".
			        " where substr(timeframe,1,6) = '$month' and ".
					" campaign = '".$details['moving-type']."' ";
			if ($site != "all")
			{
				if ($site == "aff")
				{
					if ($engine == "all")
						$sql .= " and engine = 'affiliates' ";
					else
					{						
						$site = $engine;
						$engine = "affiliates";								

						$sql .= " and engine = '$engine' and site = '$site' ";					
					}
				}
				else
				{
					if ($engine == "all")
						$sql .= " and site = '$site' and engine != 'organic'";
					else
						$sql .= " and site = '$site' and engine = '$engine'";															
				}				
			}	
			
			if ($month == date("Ym"))
				$sql .= " and timeframe != '".date("Ymd")."' group by timeframe ";			
			$affcosts = "";
		}
		else
		{
			$sql = "select substr(ts,7,2) 'day', sum(cost) 'cost' from ".
					"movingdirectory.costperclick where substr(ts,1,6) = '$month' ";
			if ($site == "aff")
			{
				$affcosts = getAffiliateCostByDay($details['campaign'],$month,$engine);
				//mail("david@irelocation.com","AFF COST ARRAY",print_r($affcosts,true));
				$sql = "";
			}
			else if ($site != "all")
			{
				$sql .= " and site = '$site' ";
				if ($engine != "all")
					$sql .= " and search_engine = '$engine' ";	
			} 
			else 
			{
				$sites = split(",",$details['sites']);
				$sites = "'".implode("','",$sites)."'";
				$sql .= " and site in ($sites) ";
				if ($engine != "all")
					$sql .= " and search_engine = '$engine' ";				
				else
					$affcosts = getAffiliateCostByDay($details['campaign'],$month);
				//mail("david@irelocation.com","AFF COST ARRAY",print_r($affcosts,true));
			}			
			
			if ($sql != "" && $month == date("Ym"))
				$sql .= " and ts != '".date("Ymd")."' group by ts";
		}
		
		if ($sql != "") 
		{						
			echo "\n<!--- Costs By Day Sql : $sql --->\n";			
			$rs = new mysql_recordset($sql);		
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
			$rs->close();
			echo "\n<!--- cost by day ran --->\n";
			if (is_array($affcosts))
			{
				foreach($affcosts as $day=>$cost)
					$data[$day]['cost'] += $cost;			
			}
		}
		else
		{
			if (is_array($affcosts))
			{
				foreach($affcosts as $day=>$cost)
					$data[$day]['cost'] = $cost;			
			}		
		}
		
		return $data;			
	}	
	
	
	
	function _getCountByDay($details,$month,$engine,$site)
	{		
		$sql = "select substr(received,7,2) 'day', count(*) 'count' ".
				" from ".$details['table']." as q where ".
				" substr(received,1,6) = '$month' ".$details['filter'];
			
		$sql .= _getFilter($engine,$site);
		
		$sql .= " group by substr(received,1,8)";
		
		echo "\n<!--- Counts By Day Sql : $sql --->\n";			
		
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
			$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
		$rs->close();
		
		return $data;
	}

	
	
	function printFilter($details,$timeframe="x",$sel_engine="all",$sel_site="all")
	{
		if ($timeframe=="x") $timeframe = $this->timeframe;
		$table = $details['table'];		
		
		$sites = split(",",$details['sites']);
		$affiliates = $details['afflist'];
		$engines = array("google","msn","overture","citysearch");
		$organics = array();
											
		
		echo "<table>";
		
		echo "<tr><td class='filterfont'>Month:</td><td>";
		printPeriodDropDown('ts',$_POST['ts']);
		echo "</td></tr><tr><td class='filterfont'>Sites:</td><td>";
		echo "<select name='site'>\n";
		echo "<option value='all'".test("all",$sel_site).">All</option>\n";
		if (count($affiliates) > 0)
			echo "<option value='aff'".test('aff',$sel_site).">affiliates</option>\n";
		foreach($sites as $site)
			echo "<option value='$site'".test($site,$sel_site).">$site</option>\n";			
		echo "</select>";
		echo "</td></tr><tr><td class='filterfont'>Sources:</td><td>";
		echo "<select name='engine'>\n";
			echo "<option value='all'".test("all",$sel_engine).">All</option>\n";	
			if (count($organics) > 0)
				echo "<option value='organic'".test("organic",$sel_engine).">organic</option>\n";
			echo "<optgroup label='Pay per Click'>\n";
			foreach($engines as $engine)
				echo "<option value='$engine'".test($engine,$sel_engine).">$engine</option>\n";
			echo "</optgroup>\n";
			echo "<optgroup label='Affiliates'>\n";
			
			foreach($affiliates as $aff)
				echo "<option value='".$aff[1]."'".test($aff[1],$sel_engine).">".
						$aff[0]."</option>\n";
				
			echo "</optgroup>\n";	
		echo "</select>\n";	
		echo "</td></tr>";
		echo "<tr><td class='filterfont'>Submit:</td><td><input type='button' ".
			 "onClick='javascript:checkCombo(document.sitefilter.site,document.sitefilter.engine);' ".
			 " value='Submit' ></td></tr></table>";
	}
	
	function test($selected,$value)
	{
		if ($selected == $value) return " selected ";
		else return " ";
	}	

	

	//Generic, doesn't care what data it gets...
	function showGraph($costs,$counts)
	{				
		$max_cost = 0;
		if (count($costs) == 0)
			return;
		
		foreach($costs as $day => $values)
		{
			if ($counts[$day]["count"] > 0)			
			$max_cost = max($max_cost, ($values["cost"]/$counts[$day]["count"]));			
		}
		$max_cost = ceil($max_cost);
	
		echo "<!--- MAX: $max_cost --->";
		?>
		<table border="1" cellpadding="0" cellspacing="0"><Tr><td>
		<table cellpadding="0" cellspacing="0" border="1">
		<TR height="20"><td width='25'  valign="bottom">$<?= ($max_cost+1) ?></td><td rowspan="<?= intval($max_cost+2) ?>" valign="bottom">
		<?
		
		
		echo "<Table cellspacing='0' cellpadding='0' border='0'><tr valign='bottom'>";
		foreach($costs as $day => $values)
		{
		
			$counts[$day]["cost"] = $values["cost"];
			
			if ($counts[$day]["count"] > 0)			
			$counts[$day]["cpl"] = number_format(($values["cost"]/$counts[$day]["count"]),1,".",",");
			else
			$counts[$day]["cpl"] = number_format($values["cost"],1,".",",");
			
			echo "<td align='center'>";
			echo "<img src='bar.php?width=25&height=".($counts[$day]["cpl"]*22)."&color=0,255,0' ";
			echo " title='\$".$counts[$day]["cpl"]."0' />";
			echo "<br/>".intval($day)."</td>";
		}
		echo "</tr></table>";
		?>
		</td></TR>
		<?
		for($x = ($max_cost); $x > 0; $x--)
		echo "<TR height='20'><td valign='bottom'>\$$x</td></tr>";
		?>
		<TR height="20"><td valign="bottom">&nbsp;</td></tr>
		</table>
		</td></Tr></table> <?
	}
	
	function _getCompanies($details,$month,$engine,$site)
	{
		if (substr($details['campaign'],0,4) == "auto")
		{
			$db = "marble";		
			$comp = "$db.companies";
		}
		else
		{
			$db = "movingdirectory";	
			$comp = "$db.company";
		}
		$site_id = $details['campaign_code'];
		$table = $details['table'];
		$filter = $details['filter']._getFilter($engine,$site);
			
		$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				$db.campaign as c
				left join 
				$table as q
				on locate(c.lead_id,q.lead_ids) > 0
				join $comp as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = '$site_id'
				AND q.received like '$month%'				
				and c.month = '$month'			
				$filter					
			group by
				c.lead_id,
				substring(received,7,2)
		";
		echo "\n<!--- Long Company Sql: \n$sql --->\n";				
		$rs = new mysql_recordset($sql);
		while ($rs->fetch_array())
			$companies[] = $rs->myarray;
		
		return $companies;
		
	}

	/*
		companies - results from _getCompanies($details,$month,$engine,$site)
		counts - results from _getCountByDay($a,$month,$engine,$site)	
	*/
	function _printGrid($companies,$counts)
	{
		echo "<table border='1'>";
		//START OF HEADER ROW
		echo "<tr class='header' ><td> &nbsp; </td>".
				"<th align='left' class='white' >Company</th>".
				"<th class='white'>Goal</th>";
		
		$days = date('t');
		for($i = 1; $i <= $days; $i++)
			echo "<th class='white'>$i</th>";
						
		echo "<th class='white'>Total</th></tr>";
			
		$font_counter = 1;
		
		$last_company = 0;
		$total = 0;	
		$day_count = 1;
		
		
		
		echo "<tr class='off'><td class='$style' > - </td>";
		echo "<th align='left' style='color:#000000' >Site Total</th>";	
		echo "<th class='off'> - - </th>";
		
		
		
		$totalkeys = array_keys($counts);
		
		echo "\n<!--- ".print_r($counts,true)." --->\n";
		
		foreach($totalkeys as $day)
		{
			$count = $counts[$day]['count'];
			
			$day =  intval($day);
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
		
			if (intval($day) == $day_count)//
				echo "<th class='off'>$count</th>";
			$total += $count;		
			$day_count++;
		}
		
		if ($day_count <= $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
		}
		
		echo "<th class='off' align='right' >$total</th></tr>";		
		//END OF HEADER ROW(S)
		
		echo "\n<!--- ".print_r($counts,true)." --->\n";
		
		foreach($companies as $company)
		{
			$append = "";
			if ($_POST['site'] != "all")
				$append = "&site=".$_POST['site'];
			if ($_POST['engine'] != "all")
				$append = "&engine=".$_POST['engine'];
								
			extract($company);
			
			if ($font_counter % 2 == 0)//even
				$style = "on";
			else
				$style = "off";
			
			if ($last_company != $comp_name && $last_company != "")//new company
			{
				if ($day_count < $days)//not a full month of data yet.
				{
					echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
				}
				echo "<td  align='right'>$total</td></tr>";
				$last_company = "";
			}
			
			if ($last_company == "")
			{
			
				echo "<tr class='$style'><td class='$style' >".($font_counter)."</td>";
				
				
					
				//$link = "<a href='movingformat.php?lead_id=".$lead_id.
				//"' class='black'>$comp_name</a>";				
				$link = $comp_name;
				
				echo "<th align='left'>";
				if (!$active)
					echo "<font color='red'>INACTIVE </font>";
				echo "$link</th>";	
				echo "<th class='$style'>$goal</th>";
				$last_company = $comp_name;		
				$total = "";	
				$day_count = 0;
				$font_counter ++;
			}
		
			$day_count++;
			
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
						
			$link = $count;
			if (intval($day) == $day_count)//
				echo "<th>$link</th>";
			$total += $count;
		}
		
		if ($day_count < $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
		}
			
		$link = "$total";
		echo "<td  align='right'>$link</td></tr>";
		
		echo "</table>";
	}
?>