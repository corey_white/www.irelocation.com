<?
	session_start();
	define(COMPS_PER_LEAD,7);
	define(CPC_DEBUG,true);
	
	include "../../inc_mysql.php";
	include "skin_top.php";
	include "graphs/tas.php";
	
	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$site = ($_POST['site']=="")?"all":$_POST['site'];

	$tasGraph = new TasGraph($timeframe,$engine,$site);
	
	echo "<form action='tastest.php' method='post'>";
	
	$tasGraph->printFilter($timeframe,$engine,$site);
	echo "</form>";

	print_r($_POST);
	extract($_POST);

	function getSqlFilter($timeframe,$engine,$site)
	{
		$sql = " received like '".$timeframe."%' ";
		$this_month = date("Ym");
		if ($timeframe == $this_month)
		{
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		}
	
		$sql .= " and campaign = 'tas' ";			
		
		if ($site != "all")
			$sql .= " and source like '$site%' ";		
					
		if ($engine != "all" && $engine != "organic")
			$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
		else if ($engine == "organic")
		{
			$sql .= " and ((length(source) < 4) OR (source like '%_search_%') ".
							"OR (source like '%_email')) ";
		}
		
		return $sql;
	}

	function getCountsByDay($timeframe="x",$engine="x",$site="x")
	{
		if ($timeframe=="x") $timeframe = $this->timeframe;
		if ($engine=="x") $engine = $this->engine;
		if ($site=="x") $site = $this->site;
		
		$sql = "select count(*) count, substring(received,7,2) 'day' from marble.auto_quotes q ".
			"where ".getSqlFilter($timeframe,$engine,$site);
		
		$sql .=  " group by substring(received,7,2) ";
		
		if(CPC_DEBUG)
			echo "\n<!--- $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
		{
			$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
		}
		return $data;	
	}

	function getCount($timeframe="x",$engine="x",$site="x")
	{
		if ($timeframe=="x") $timeframe = $this->timeframe;
		if ($engine=="x") $engine = $this->engine;
		if ($site=="x") $site = $this->site;
	
		$sql = "select count(*) count from marble.auto_quotes q where ".
				getSqlFilter($timeframe,$engine,$site);
						
		echo "\n<!--- $sql --->\n";	
					
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	}
	
	echo getCount($timeframe,$engine,$site)."<br/>\n";
	$da = getCountsByDay($timeframe,$engine,$site);
	
	foreach($da as $day => $count)
	{
		echo $day." ".$count['count']."<br/>\n";
	}

?>