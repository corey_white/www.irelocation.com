<?
/* 
************************FILE INFORMATION**********************
* File Name:  movingviewleads.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  Unkown
**********************************************************************
* Modifications (Date/Who/Details):
3/7/08 12:52 PM - added column for quote id
**********************************************************************
*/

	session_start();
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] == "autotransporter")
	{		
		$lead_id = $_SESSION['lead_id'];		
	}
	else if ($_SESSION['user'] == "uberadmin")
	{
		if(strlen($_REQUEST['lead_id']) >= 0)
			$lead_id = $_REQUEST['lead_id'];
		else
		{
			header("Location: marblegrid.php");
			exit();	
		}
	}
	else if ($_SESSION['user'] == 'wiseclick')
	{
		$lead_id = "";		
		$engine = "wc";		
		$_REQUEST['engine'] = $engine;
	}
	else
	{
		header("Location: login.php");
		exit();	
	}
	
	if (strlen($_REQUEST['ts']) < 6)
	{
		header("Location: marblegrid.php");
		exit();		
	}
	
	
	$ts = $_REQUEST['ts'];

	include "../../inc_mysql.php";
	
	
	
	if (strlen($lead_id) > 0)
	{
		if ($lead_id == 1516)//vanlines
		{
			if (isset($_REQUEST['local']))
				$append = " and q.destination_state = q.origin_state";
			else if (isset($_REQUEST['long']))
				$append = " and q.destination_state != q.origin_state";
			$table = "quotes_local";
			$lead_sql = "select * from movingdirectory.quotes_local as q where success = 1".
						" and received like '$ts%' $append";
			$company_info_sql = "select 'Van Lines' as comp_name ";
		}
		else
		{
			$lead_sql = "select * from movingdirectory.quotes as q where ".
						"lead_ids like '%$lead_id%' and received like '$ts%' ";
	
			$company_info_sql = "select * from movingdirectory.company as co join ".
								" movingdirectory.campaign as ca on ca.comp_id = co.comp_id ".
								" where ca.lead_id = $lead_id and ca.site_id = 'pm'";
		}
		
		$company = new mysql_recordset($company_info_sql);
		$company->fetch_array();
		$comp_name = $company->myarray['comp_name'];
	}
	else
	{
		$lead_sql = "Select * from movingdirectory.quotes as q where source like 'aff_$engine' and received like '$ts%' ";
		$comp_name = ucwords($_SESSION['user']);						
	}
	
	if (isset($_REQUEST['site']))
	{
		$sqlFilter .= " and q.source like '".$_REQUEST['site'];
		if (isset($_REQUEST['engine']))
			$sqlFilter .= "_".$_REQUEST['engine']."%' ";
		else
			$sqlFilter .= "%' ";
	}
	else
	{
		if (isset($_REQUEST['engine']))
			$sqlFilter .= " and q.source like '%".$_REQUEST['engine']."%' and q.source not like '%search%' ";		
	}
	
	$lead_sql  .= $sqlFilter;
							
	
	$leads = new mysql_recordset($lead_sql);
?>	
<link rel='stylesheet' type='text/css' href='marble.css' />
<script language="javascript">
	function checkAll(form)
	{
		var elcount = form.elements.length;
		var counter = 0;
		for(var i = 0; i < elcount; i++)
		{
			if (form.elements[i].type == 'checkbox')
				form.elements[i].checked = true;
		}
	}

</script>
<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<a href='movingreport.php' class="white">.:Moving Leads</a>
			</td>
		</tr>
		<tr>
			<td>
				<form action="marbleresend.php" method="post" name="resend">
					<input type='hidden' name="lead_id" value="<?= $lead_id ?>" />
					<table border="0" width="100%" cellspacing=0 cellpadding=3>
					<tr>
						<td  bgcolor='navy' width="130" valign="top">&nbsp;							
							<br/><br/><br/><br/><br/>
							<? 	include "sidelinks.php";	?>
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=1>
							<Tr><td>&nbsp;</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Company: 
								<?= $comp_name  ?></font></b>
							</td>
							</tr>
							<? if (strlen($lead_id) > 0) { ?>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Phone: 
								<?	
									$phone_num = ereg_replace("[^0-9]","",$company->myarray['phone_num']);
									if (strlen($phone_num) > 10)
									{	
										$ext =  "Ext. ".substr($phone_num,10);
										$phone_num = substr($phone_num,1,10);
									}
									if (strlen($phone_num) == 10)
									{
										$a = substr($phone_num,0,3);
										$b = substr($phone_num,3,3);
										$c = substr($phone_num,6);
										echo "($a) $b - $c $ext ";
									}
									
								?>								
								</font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">View Lead Format</font></b>
							</td>
							</tr>			
							<? }  ?>				
							<tr><td>&nbsp;</td></tr>
							<tr><td align='left'>
							<? if (strlen($lead_id) > 0) { ?>
							<div align="right">
								<input type='reset' name="clear" value="Un Check All" /> 
								<input type='button' name="clear" value="Check All" onClick="checkAll(resend);" /> 
								<input type='submit' name="send" value="Resend" />
								
							</div>
							<? } ?>
							<table border="1" width="100%" cellspacing='0' cellpadding='3'>
								<tr class="toprow">
									<th>Quote ID</th>
									<th>Name</th>
									<th>Contact</th>
									<th>Est Move Date</th>
									<th>Origin</th>
									<th>Destination</th>
									<th>House Info</th>
									<? if (strlen($lead_id) > 0) { ?>
									<th>Resend</th>
									<? } ?>
								</tr>		
								
								<? 
								$font_counter = 0;
								$lead_count = 0;
								while($leads->fetch_array())
								{
									if ($font_counter % 2 == 0)//even
										$style = "on";
									else
										$style = "off";
									extract($leads->myarray);
									
									list($details,$ccomments) = split("Comments/Unique Items",$comments);
									$details = str_replace("\n","<br/>",$details);
									echo "<tr class='$style'>\n";
									echo "<td class='$style'>$quote_id</td>";
									echo "<td class='$style'>".ucwords("$name")."</td>";
									echo "<td>";
									if (strlen($phone) == 10)
									{
										$a = substr($phone,0,3);
										$b = substr($phone,3,3);
										$c = substr($phone,6);
										echo "($a) $b - $c $ext ";
									}
									else
										echo $phone;
									echo "<Br/>";
									echo "<a class='black' href='mailto:$email'>$email</a></td>";
									echo "<td>$est_move_date</td>";
									$link = "<a class='black' href='http://maps.google.com/?q=$origin_city, ".
											"$origin_state $origin_zip'>".
											"$origin_city, $origin_state $origin_zip</a";
									echo "<td>$link</td>";
									$link = "<a class='black' href='http://maps.google.com/?q=$destination_city, ".
											"$destination_state $destination_zip'>".									
											"$destination_city, $destination_state $destination_zip</a";											
									echo "<td>$link</td>";					
									echo "<td>$details</td>";
									if (strlen($lead_id) > 0)
										echo "<td><input type='checkbox' name='quote_id[]' value='$quote_id'/></td>";
									echo "</tr>\n";				
									$font_counter ++;
									$lead_count++;
								}						
								?>
							</table>
							<? if (strlen($lead_id) > 0) { ?>
							<div align="right">
								<input type='reset' name="clear" value="Un Check All" /> 
								<input type='button' name="clear" value="Check All" onClick="checkAll(resend);" /> 
								<input type='submit' name="send" value="Resend" />
								
							</div>
							</form>
							<? } ?>
							<div align="left">
								<font face='verdana' size=2>
									<strong><?= $lead_count ?></strong> Leads.
								</font>
							</div>
</td></tr></table></td></tr></table></td></tr></table>