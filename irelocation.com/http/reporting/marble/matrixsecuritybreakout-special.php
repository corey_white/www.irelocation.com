<?
	session_start();
	define(BRINKS,1519);//0
	define(PROTECT_AMERICA,1520);	//2			
	define(GAYLORD,1552);
	define(APX,1569);
	define(CPC_DEBUG,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	
	define(CAMPAIGN,"usalarm");
	$this_page = "matrixsecuritybreakout.php";
	
	include "../../inc_mysql.php";
	
	echo "<h1>THIS IS A SPECIAL REPORT FOR 11-01 to 11-20 ONLY</h1>";
	
	function getLeadCounts($lead_id, $ts)
	{
		//totally rework this method...
		
		return $results;		
	}
	function getCompanyName($lead_id)
	{
		switch($lead_id)
		{
			case APX: return "APX Home Security";
			case BRINKS: return "Brink's";
			case PROTECT_AMERICA: return "Protect America";
			case PROTECTION_ONE:return "Protection One";
			case USALARM: return "US Alarm";
			case SITEBASE: return "TopSecurityCompanies.com";
			case PINNACLE: return "Pinnacle Security";
			case GAYLORD: return "Gaylord Security";
			case APEXDIRECT: return "Apex Direct";
			case SAFETECH: return "Safe-Tech";
			case VOXCOM: return "Voxcom";			
			case BELLHOME: return "Bell Home Monitoring";	
			case MONITRONICS: return "Monitronics";
		}	
	}
	
	function getRevenueData($timeframe)
	{
		$sql = "select 
					ca.lead_id, 
					cpl.extra, 
					cost, 
					count(*) 'count',
					cost*count(*) 'sub_total'
				from 
					movingdirectory.campaign as ca 
					join
					irelocation.leads_security q
					on 
					locate(ca.lead_id,q.lead_ids) > 0 and q.campaign = ca.site_id
					join 
					marble.cost_per_lead cpl 
					on 
					cpl.lead_id = ca.lead_id and cpl.extra*4 = length(replace(q.lead_ids,'|',''))
					join irelocation.leads_verified as v 
					on q.quote_id = v.quote_id and v.lead_id = ca.lead_id and v.success = 1
				where 
					ca.month = '$timeframe' 
					and ca.site_id = '".CAMPAIGN."' 
					and q.received like '200811%' and q.received < '20081121'  ";
		if ($timeframe == date("Ym"))
			$sql .= " and q.received not like '".date("Ymd")."%' ";
			
		$sql .= " and q.ready_to_send = 3 group by
						ca.lead_id,cpl.extra;";
		
		echo "\n\n<!-- getRevenueData: $sql -->\n\n";
		
		$rows = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array()) $rows[] = $rs->myarray;
		$rs->close();
		
		return $rows;
	}

	function getCount($timeframe,$quote_type="all",$engine="all") { //-- gets the total count for all leads in this campaign?
	
		$sql = "select count(*) count from irelocation.leads_security q where received like '$timeframe%' and campaign = '".CAMPAIGN."' ";
		
		if ($quote_type != "all" && $quote_type != "")
			$sql .= " and quote_type = '$quote_type' ";											
		
		if ($site != "all")
			$sql .= " and q.source like '$site%' ";
		
		if ($engine != "all")
			$sql .= " and q.source like '%$engine%' ";
		
		if(CPC_DEBUG)
			echo "\n<!--- count $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	}

	function getCompanyArray($ts,$campaign)
	{
		$sql = "select lead_id from movingdirectory.campaign where ".
				" month='$ts' and site_id = '$campaign'";
		
			
		$rs = new mysql_recordset($sql);				

		while($rs->fetch_array())
			$companies[] = $rs->myarray['lead_id'];
		return $companies;
	}

	function getTotalCost($ts)
	{				
		$sites = "'usalarm'";
		$aff = "usalarm.security";
			
		$sql = "select sum(cost) cost from movingdirectory.costperclick where site in ($sites) and ts like '$ts%'";
		$rs = new mysql_recordset($sql);				
		$rs->fetch_array();
		$cost = $rs->myarray['cost'] + getAffiliateCost($aff,$ts);				
		return $cost;
	}


	if (strlen($_REQUEST['ts']) != 6)
		$ts = date("Ym");
	else
		$ts = $_REQUEST['ts'];
	
	$array = getCompanyArray($ts,CAMPAIGN);
	
	include "skin_top.php";
	
	if (date("d") == 1 && $ts == date("Ym"))
	{
		echo "No data yet. :( ";
		exit();
	}
	?>	
	
	<table width='500'> 
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3"><font size="+1"><u><strong>USALARM Leads</strong></u></font></td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3">If in the current month, these numbers do not include today.  Also this takes in account if the companies rejected a lead for some reason.</td></tr>	
		<tr><td colspan="3">&nbsp;</td></tr>		
		<tr>
			<td colspan="3">
			<?
				$months = array(date("Ym",strtotime(" -2 Month")),date("Ym",strtotime(" -1 Month")),date("Ym",time()));
			
			?>				
				<form name="months" action="<?= $this_page ?>" method="get">
				Month: 
				<select name="ts">
				<? foreach($months as $m) { 
					echo "<option value='".$m."' ".(($m==$ts)?" selected ":"")." >$m</option>";
				} ?>
				</select>
				 &nbsp; &nbsp; <input type="submit" value="Update Month" /></form>
			</td>
		</tr>
		<? 
		
	//$graph = new SecurityGraph($ts);
	
	$grand_total_moneys = 0;
	$revenue_data = getRevenueData($ts);
	$last_company = "";
	$company_total = 0;
	echo "<tr><td colspan='2'>";
	foreach($revenue_data as $row)
	{
		extract($row);
		
		$company = getCompanyName($lead_id);
		if ($company != $last_company)
		{
			if ($company_total != 0)
			{
				echo "<Strong>$last_company</strong> Total: <Strong>$company_total</strong><br/><br/>";
				
			}	
			echo "<strong>$company:</strong><Br/>";
			$last_company = $company;
			$company_total = 0;
		}
		if ($extra != 1)
			echo " + ".($extra-1)." Others = ";
		else
			echo "Exclusive = ";
		echo "$cost X $count = $sub_total.<br/> ";
		
		$grand_total_moneys  += $sub_total;
		$company_total += $sub_total;	
	}	
		echo "<Strong>$last_company</strong> Total: <Strong>$company_total</strong><br/><br/>";
	$total_cost = getTotalCost($ts);
	$profit = $grand_total_moneys - $total_cost;
	
	$total_leads = getCount($ts,"all","all");
	$cpl = number_format(($total_cost / $total_leads),2,".",",");
	echo "</td></tr>";
	?>
<tr><td colspan="2">&nbsp;</td><td align="center"><hr width="70%"></td></tr>
<tr><td colspan='2'>&nbsp;</td></tr>
<tr>
	<td>&nbsp;</td>
	<td align="right" colspan="2">
		 <table>
		 	<tr>
				<td align="right"><strong>Grand Total:</strong></td>
				<td align="right"><strong>$<?= number_format(($grand_total_moneys),2,".",",") ?></strong> </td>
			</tr>
			<tr>
				<td align="right"><strong>Cost ($<?= $cpl." x ".$total_leads." leads" ?>) : </strong></td>
				<td align="right"><strong>$<?= number_format(($total_cost),2,".",",") ?></strong> </td>
			</tr>
			<tr>
				<td align="right"><strong>Gross Profit:</strong></td>
				<td align="right"><strong>$<?= number_format(($profit),2,".",",") ?></strong> </td>
			</tr>
		 </table>
		 
	</td>
</tr>
</table>
<? include "skin_bottom.php"; ?>
