<?
	session_start();	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] == "autotransporter")
	{		
		$lead_id = $_SESSION['lead_id'];		
		$_SESSION['time'] = date("YmdHis");
		
	}
	else if ($_SESSION['user'] == "uberadmin")
	{
		$_SESSION['time'] = date("YmdHis");
		
		if(strlen($_REQUEST['lead_id']) >= 0)
			$lead_id = $_REQUEST['lead_id'];
		else
		{
			header("Location: marblereport.php");
			exit();	
		}
	}
	else
	{
		header("Location: login.php");
		exit();	
	}
	include "../../inc_mysql.php";

	$sites["asc"] = array("google","overture","ask","google_local");
	$sites["0"] = "";
	$sites["cs"] = array("google","msn","overture");
	$sites["cs_tas"] = array("google","overture");
	$sites["csq"] = array("google");
	$sites["1"] = "";
	$sites["mmc"] = array("google","msn","overture");
	$sites["mmc_pas"] = array("google","overture");
	$sites["2"] = "";
	$sites["atus"] = array("google","overture");
	$sites["atus_123"] = array("overture");
	$sites["3"] = "";
	$sites["tm"] = array("google","msn","overture","wc","ask");
	$sites["5"] = "";
	$sites["pm"] = array("google","msn","overture","wc","ask");
	$sites["6"] = "";
	$sites["me"] = array("google","overture");
	$sites["4"] = "";
	$sites["tsc"] = array("google","msn","overture","ask");	
	$sites["5"] = "";
	$sites["tac"] = array("google");	
	
	function getTable($site)
	{
		if ($site == "tsc" || $site == "tac")
			return "irelocation.leads_security";
		else if ($site == "atus" || $site == "pm" || $site == "me" || $site == "tm")
			return "movingdirectory.quotes";
		else
			return "marble.auto_quotes";
	}
	
	$site_codes = array_keys($sites);

	function formatTime($received)
	{
		$r_year=substr($received,0,4);
		$r_month=substr($received,4,2);
		$r_day=substr($received,6,2);
		$r_hour=substr($received,8,2);
		$r_min=substr($received,10,2);
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	}
	
	function differance($received)
	{
		$ry=substr($received,0,4);
		$rm=substr($received,4,2);
		$rd=substr($received,6,2);
			
		$ny = date("Y");
		$nm = date("m");
		$nd = date("d");
		$nh = date("H");
		$nmin = date("i");
		$start_date=gregoriantojd($rm, $rd,$ry);
		$end_date=gregoriantojd($nm, $nd,$ny);
		$differance = $end_date - $start_date;
		if ($differance == 0)//lead came in today
		{
			$r_hour=substr($received,8,2);
			$r_min=substr($received,10,2);
			$min_received = ($r_hour*60)+$r_min;
			$min_now =  ($nh*60)+$nmin;
			
			$differance = $min_now - $min_received;
			$hours = floor($differance/60);			
							
			$minutes = ($differance%60);
			if ($minutes < 10)
				$minutes = "0".$minutes;
			
			//2 Servers are on differant time zones :(
			if ($hours < 0)
				$hours += 1;
							
			if ($hours == 0)
			{
				$value = "";
				if (intval($minutes) == 0)
					$value = "seconds ago";
				else
					$value = intval($minutes)." minutes ago";
				return "<font color='green'>$value</font>";
			}
			else
			{
				$value = $hours.":".$minutes." hours ago";
				if ($hours < 3)
					return "<font color='orange'>$value</font>";
				else
					return "<font color='red'>$value</font>";
				
			}
		}	
		else
			return "<font color='red'>$differance days ago</font>";
	}
	
	include "skin_top.php";

	echo "<table width='80%'><tr><td valign='top' align='center'>";
		echo "<table>\n";
	$autos = true;
	foreach ($site_codes as $site)
	{
		if (is_numeric($site))
		{
			echo "<tr><td colspan='3'><hr/></td></tr>\n";
			continue;
		}
		if ($site == "tm")
		{
			if ($autos)
			{
				echo "</table>\n</td><td  valign='top'><table>";
				$autos = false;
			}
		}
				
		$table = getTable($site);
		
		$engines = $sites[$site];
		
		$engine_tracking = "source like '".$site."_".implode("' or source like '".$site."_",$engines)."'";
		$source_tag = " source like '$site%' AND (".$engine_tracking.") ";
		$sql = "select distinct(source) source,max(received) received from $table where $source_tag group by source;";
			
		
		if ($table != "marble.auto_quotes")
			$rs = new mysql_recordset($sql);		
		else
			$rs = new mysql_recordset($sql);	
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			echo "<!--- SITE: $site $source => ".formatTime($received)." ( ".differance($received)." ) --->";	
			echo "<tr><td align='left'><font face='Verdana' size='2'>$source</font></td>\n<td align='left'><font face='Verdana' size='2'>"." ( ".differance($received)." )"."</font></td>\n<td align='left'><font face='Verdana' size='2'>".formatTime($received)."</font></td></tr>\n";
		}
		
	}
	echo "</table>\n</td></tr></table>";
?>