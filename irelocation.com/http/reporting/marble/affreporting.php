<?
	session_start();
	include_once "../../inc_mysql.php";
	
	define(AUTO,"auto");
	define(MOVING,"moving");
	define(SECURITY,"security");


	include "skin_top.php";

	/**
	 * I also added in the case so you can pass in the category
	 * via request parameters and filter. used after affupdateform.php
	 * also stored selected category in the $RAW_CATEGORY variable, to make
	 * it easier to pre-select the value in the drop down.
	 * 
	 * @author david haveman
	 * @created 2/26/2008 7:15pm
	 */

	$RAW_CATEGORY = "";
	$category = "";
	
	$ts = (($_POST['ts']=="")?date("Ym"):$_POST['ts']);
	if ($_POST['category']!="" && $_POST['category']!="*")
	{
		$category = " and lead_type = '".$_POST['category']."' ";
		$RAW_CATEGORY = $_POST['category'];	
	}
	else if ($_REQUEST['category'] != "" && $_REQUEST['category']!="*")
	{
		$category = " and lead_type = '".$_REQUEST['category']."' ";
		$RAW_CATEGORY = $_REQUEST['category'];	
	}
	
	echo "<!--- $category --->";
	
	$sql = " select * from movingdirectory.leadbased_affiliates where month = '$ts' ".
			" $category order by lead_type,ppl,comp_name ";

	echo "\n\n<!-- get affiliates? :: $sql -->\n\n";
	
	$rs = new mysql_recordset($sql);
	$aff = array();
	while($rs->fetch_array())
	{
		$aff[] = array($rs->myarray['comp_name'],$rs->myarray['source'],
							$rs->myarray['lead_type'],$rs->myarray['ppl'],
							$rs->myarray['affiliateid'],$rs->myarray['localppl']);
	}
	$rs->close();
	
	echo "\n\n<!-- ";
	print_r($aff);	
	echo "-->\n\n";

	echo "<br/><font size='+1'><A href='affaddform.php' class='black'>Add a New Company</a></font><br/><br/>";
	?>
	<style>
		td 
		{
			font-family:Arial;
			font-size: 12px;
		}
	</style>

	<Script>
		function validateDelete(which)
		{
			if (confirm("Are you sure?"))
				window.location = "affdelete.php?affiliateid="+which;
		}
	
	</Script>
	<form action="affreporting.php" method="post">
	<table>
		<tr>
			<td colspan="6">
				Month: <?= printPeriodDropDown("ts",$_POST['ts']); ?> 
			</td>
		</tr>
		<tr>
			<td colspan="6">
			Category: <select name="category">
				<option value="*">All</option>
				<option <?= ($$RAW_CATEGORY=="auto")?"selected":"" ?> value="auto">Auto</option>
				<option <?= ($RAW_CATEGORY=="moving")?"selected":"" ?> value="moving">Moving</option>
				<option <?= ($RAW_CATEGORY=="security")?"selected":"" ?> value="security">Security</option>
			</select>
			</td>
		</tr>
		<tr>
			<td colspan="6">		
				<input type='submit' value='Update'/>
			</td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<? if (date("d")+5 > date("t")) { ?>
		<tr>
			<Td colspan='6' align="left">
			Click <a href='affcopy.php' style="text-decoration:none; color:#0000FF; ">here</a> to copy this months affiliates to next month.
			</Td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<? } ?>
		<tr>
			<Td colspan='6' align="left">
			<strong>T</strong>his page is to be used as a simple summary of all affiliates that are sending us leads for each month.  If a company is missing use the above link to add them, or ask Dave or Mark to do so.  If a company is not included in this reporting, it will not be factored into the costs of the respective grid pages.
			</Td>
		</tr>
	
		<tr>
			<Td colspan='6' align="left">
			Currently local and long distance moving leads for most companies are paid the same, 
			once i get values for local leads, this will change.
			</Td>
		</tr>
		<tr><td colspan="6">&nbsp;</td></tr>
		<Tr align="left">
			<td width="150">Company</td><td width="100"> Type </td><Td width="120"> Count </Td>
			<Td width="120"> $/Lead </Td><td width="100"> Total </td><td width="100"> Last Lead </td><td>&nbsp;</td>
		</Tr>
	<?
	$total_bill = 0;
	
	foreach($aff as $comp)
	{
		$lead_time = getLastLeadTime($ts,$comp[1],$comp[2]);
		if ($lead_time != 0)
		{
			$year = substr($lead_time,0,4);
			$m = substr($lead_time,4,2);
			$d = substr($lead_time,6,2);
			$lead_time = $m."/".$d."/".$year;
		}
		else
			$lead_time = "--";
		
		
		
		$counts = getLeadCount($ts,$comp[1],$comp[2]);
		$leads = $counts[0];
		if (count($counts) == 2 && $counts[1] != 0)//local as well
		{
			$bill = ($leads>0)?"\$".number_format(($comp[3]*$leads+$counts[1]*$comp[5]),2,".",","):"N/A";	
			$total_bill += ($comp[3]*$leads+$counts[1]*$comp[5]);
			$leads = "$leads ( +".$counts[1]." locals)";
			$ppl = "\$".$comp[3]." ( Lcl \$".$comp[5]." ) ";
		}	
		else
		{		
			$total_bill += ($comp[3]*$leads);
			$bill = ($leads>0)?"\$".number_format(($comp[3]*$leads),2,".",","):"N/A";	
			$ppl = "\$".$comp[3];
		}
		echo "\n<tr align='left'><td><strong>".updateLink($comp[0],$comp[4])."</strong></td><td>".ucwords($comp[2])."</td><td>"
				.$leads."</td><td>".$ppl."</td><td>$bill</td><td>$lead_time</td><td><font size='-1'>".
				"<A href='#' class='black' onclick='validateDelete(".$comp[4].")'>delete</a>";
		echo "</font></td></tr>\n";		
	}
	
	echo "<tr><td>&nbsp;</td><td colspan='4'><hr/></td><td>&nbsp;</td></tr>\n";
	echo "<tr><Td colspan='2'>&nbsp;</td><td align='right' colspan='2'>Total Due:</td>".
		 "<td>\$".number_format($total_bill,2,".",",")."</td><td>&nbsp;</td></tr>\n";
	 echo "</table>	</form>";
	
	/**
	 * method to grab a link to the update affiliates page.
	 * 
	 * PARAMS:
	 * @name the name of the company to display
	 * @the id of the company for this month.
	 * 
	 * @author david haveman
	 * @created 2/26/2008 7:15pm.
	 */
	
	function updateLink($name,$affid)
	{
		return  "<a href='affupdateform.php?affid=".$affid."' " .
				"style='text-decoration: none; color: rgb(0, 0, 255);'" .
				" title='Click to Update Values'>" .
				$name."</a>";
	}
	
	function getLastLeadTime($ts,$tag,$type)
	{
		if ($type == AUTO)
		{	
			$sql = "select max(received) 'count' from marble.auto_quotes where ".
					"received like '$ts%' and source like '%$tag%' ";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count  = $rs->myarray['count'];
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id = '1'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count += $rs->myarray['count'];
			return $count;
		}
		else if ($type == SECURITY)
		{
			$sql = "select max(received) count from irelocation.leads_security ".
					" where received like '$ts%' and source like '%$tag%'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count = $rs->myarray['count'];
			if ($count == 0) return 0;
			else return $count;
		}
		else if ($type == MOVING)
		{
			$local = 0;
			$count = 0;
			
			$sql = "select max(received) count from movingdirectory.quotes_local ".
					" where received like '$ts%' and source like '%$tag%' and origin_state ".
					" = destination_state";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$local = $rs->myarray['count'];
			
			$sql = "select max(received) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id in (2,3) ".
					" and origin_state = destination_state";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$local += $rs->myarray['count'];
			
			$sql = "select max(received) count from movingdirectory.quotes_local ".
					"where received like '$ts%' and source like '%$tag%'  and origin_state != ".
					" destination_state";

			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$count  = $rs->myarray['count'];
			
			$sql = "select max(received) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id in (2,3) ".
					" and origin_state != destination_state";

								
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$count += $rs->myarray['count'];
			if ($count > $local)
			return $count;
		}
	}
	
	function getLeadCount($ts,$tag,$type)
	{
		if ($type == AUTO)
		{	
			$sql = "select count(*) count from marble.auto_quotes where ".
					"received like '$ts%' and source like '%$tag%' ";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count  = $rs->myarray['count'];
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id = '1'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count += $rs->myarray['count'];
			return array($count);
		}
		else if ($type == SECURITY)
		{
			$sql = "select count(*) count from irelocation.leads_security ".
					" where received like '$ts%' and source like '%$tag%'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count = $rs->myarray['count'];
			if ($count == 0) return array(0);
			else return array($count);
		}
		else if ($type == MOVING)
		{
			$local = 0;
			$count = 0;
			
			$sql = "select count(*) count from movingdirectory.quotes_local ".
					" where received like '$ts%' and source like '%$tag%' and origin_state ".
					" = destination_state";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$local = $rs->myarray['count'];
			
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id in (2,3) ".
					" and origin_state = destination_state";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$local += $rs->myarray['count'];
			
			$sql = "select count(*) count from movingdirectory.quotes_local ".
					"where received like '$ts%' and source like '%$tag%'  and origin_state != ".
					" destination_state";

			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$count  = $rs->myarray['count'];
			
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id in (2,3) ".
					" and origin_state != destination_state";

								
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			echo "\n<!--- $sql => ".$rs->myarray['count']."--->\n";
			$count += $rs->myarray['count'];
			
			return array($count,$local);
		}
	}

	function getCategory($cat_id)
	{
		if (AUTO == $cat_id) return "Auto Leads";
		if (MOVING == $cat_id) return "Moving Leads";
		if (SECURITY == $cat_id) return "Security Leads";
		return "Unknown($cat_id) Leads";
	}

	include "skin_bottom.php";
?>