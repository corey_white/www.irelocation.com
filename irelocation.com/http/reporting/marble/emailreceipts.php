<?
	session_start();

	function ssplit()
	{
		$numargs = func_num_args();
		$arg = func_get_args();
		if ($numargs == 0) return "";
		if ($numargs == 1) return $arg[0];
		else if ($numargs == 2)
		{
			$a = substr($arg[0],0,$arg[1]);
			$b = substr($arg[0],$arg[1]);
			return array($a,$b);
		}		
		else
		{
			$results = array();
			for($i=1; $i < $numargs; $i++)
			{
				$results[$i-1] = substr($arg[0],0,$arg[$i]);
				$arg[0] = substr($arg[0],$arg[$i]);			
			}
			$results[] = $arg[0];
			
			return $results;
		}
	}
	

	define(AUTO,1);
	define(MOVING,2);
	define(INT,3);
	define(SECURITY,4);
	
	include_once "../../inc_mysql.php";
	include "skin_top.php";

	
	if ($_REQUEST['ts'] != "")
		$ts = $_REQUEST['ts'];
	else
		$ts = date("Ym");
	list($year,$month) = ssplit($ts,4);
	$time = mktime(1,1,1,$month,1,$year);
	$prev = date("Ym",strtotime("-1 month",$time));
	$next = date("Ym",strtotime("+1 month",$time));
	$nl = "<a class='black' title='click to see the next month' href='emailreceipts.php?ts=$next'>$next</a>";
	$pl = "<a class='black' title='click to see the previous month' href='emailreceipts.php?ts=$prev'>$prev</a>";
	
	
	$sql = "
		select min(received) 'received', '1' as 'cat_id' from  movingdirectory.email_receipts where cat_id = 1 and received like '$ts%'
UNION ALL
select min(received), '2' as 'cat_id' from movingdirectory.email_receipts where cat_id = 2 and received like '$ts%'
UNION ALL
select min(received), '3' as 'cat_id' from movingdirectory. email_receipts where cat_id = 3 and received like '$ts%'
UNION ALL
select min(received), '4' as 'cat_id' from movingdirectory.email_receipts where cat_id = 4 and received like '$ts%'";

	$rs = new mysql_recordset($sql);
	while($rs->fetch_array())
		$data[$rs->myarray['cat_id']] = $rs->myarray['received'];
	$rs->close();	
	
	
	for($i = AUTO; $i <= SECURITY; $i++)
	{
		$receipts[$i] = getReceiptCount($i,$ts);
		$totalleads[$i] = getLeadCount($i,$data[$i]);
		if ($totalleads[$i] > 0)
			$percentage[$i] = $receipts[$i]/$totalleads[$i]*100;
		else
			$percentage[$i] = 0;
		$percentage[$i] = number_format($percentage[$i],2,".",",");
	}
	echo "<table class='filterfont'>";
	echo "<tr><td colspan='3' align='left'>";
	echo "Data for $pl <strong>$ts</strong> $nl";
	echo "</td></tr><tr><td colspan='3'>&nbsp;</td></tr>";
	echo "<tr><td>Category</td><td>Percent</td><td>Total Leads</td></tr>";
	echo "<tr><td>Security</td><td align='right'>".$percentage[SECURITY];
	echo "</td><td align='right'>".$totalleads[SECURITY]."</td></tr>";
	
	echo "<tr><td>Auto</td><td align='right'>".$percentage[AUTO];
	echo "</td><td align='right'>".$totalleads[AUTO]."</td></tr>";
	
	echo "<tr><td>Moving</td><td align='right'>".$percentage[MOVING];
	echo "</td><td align='right'>".$totalleads[MOVING]."</td></tr>";
	
	echo "<tr><td>International</td><td align='right'>".$percentage[INT];
	echo "</td><td align='right'>".$totalleads[INT]."</td></tr>";
	echo "</table>";
	
	function getLeadCount($cat_id, $minreceived, $campaign="all")
	{
		if ($minreceived == "")
			return 0;
			
		$month = substr($minreceived,0,6);
		echo "<!--- $month $cat_id --->\n";
		if ($cat_id == AUTO)
		{
			$sql[] = "select count(*) 'count' from marble.auto_quotes ".
					" where processed >= $minreceived and processed like ".
					" '$month%' ";	
			if ($month < 200708)
				$sql[] = "select count(*) 'count' from movingdirectory.quotes ".
						" where received >= $minreceived and received like ".
						" '$month%' and cat_id = 1 ";			
		}	
		else if ($cat_id == SECURITY)
		{
			$sql[] = "select count(*) 'count' from irelocation.leads_security ".
					" where received >= $minreceived and received like ".
					" '$month%' ";
		}
		else if ($cat_id == INT)
		{
			$sql[] = "select count(*) 'count' from movingdirectory.quotes ".
					" where received >= $minreceived and received like ".
					" '$month%' and cat_id = 3 ";
		}
		else if ($cat_id == MOVING)
		{
			$sql[] = "select count(*) 'count' from movingdirectory.quotes ".
					" where received >= $minreceived and received like ".
					" '$month%' and cat_id = 2 ";
			if ($month >= 200708)
			$sql[] = "select count(*) 'count' from movingdirectory.quotes_local ".
					" where received >= $minreceived and received like ".
					" '$month%'";
		}
		
		$total = 0;
		if (count ($sql) > 0)		
			foreach($sql as $s)
			{
				echo "\n<!--- $s --->\n";
				$rs = new mysql_recordset($s);
				$rs->fetch_array();
				$total += $rs->myarray['count'];
				$rs->close();
			}
			
		return $total;
	}
	
	function getReceiptCount($cat_id,$ts,$campaign="all")
	{
		if ($campaign == "all")
			$sql = " select count(distinct(quote_id)) 'count' from ".
					" movingdirectory.email_receipts where received like ".
					" '$ts%' and cat_id = $cat_id ";
		else
		{
			//auto site, 	
			$sql = "select count(*) 'count' from movingdirectory.email_receipts r ".
					" join marble.auto_quotes as q on q.quote_d = r.quote_id ".
					" where received like '$ts%' and q.campaign = '$campaign' ".
					" group by q.quote_id ";
		}
		
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		
		return $rs->myarray['count'];
	}

	include "skin_bottom.php";
?>