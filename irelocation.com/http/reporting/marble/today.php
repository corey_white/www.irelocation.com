<?
	session_start();
	
	include "skin_top.php";

	include_once "../../inc_mysql.php";
	
	if($_REQUEST['ts'] == "")
		define(YESTERDAY, date("Ymd"));
	else
		define(YESTERDAY, $_REQUEST['ts']);
		
	$queries = array();
	$DATE = " where received like '".YESTERDAY."%' and ";
	
	$SELECT_SECURITY = " select count(*) 'count' from irelocation.leads_security ".$DATE;
							   
	$SELECT_AUTO = " select count(*) 'count' from movingdirectory.quotes ".$DATE.
					" cat_id = 1 and ";
	$SELECT_MARBLE = " select count(*) 'count' from marble.auto_quotes ".$DATE;
		  	
	
	$engines = array("google","msn","overture");
	
	$marble_sites = array("cs","asc","mmc","tas","cq","pas");
	$moving_sites = array("tm","me","pm");
	$auto_sites = array("atus","123cm");
	$security = array("tsc","tac");
	$canada = array("ctsc","ctac");
	
	$queries['- Top Auto Leads -'] = "";
	
	foreach ($marble_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_MARBLE." source like '".
										$site."_".$engine."%'; ";	
	$queries['- Auto-Transport.us -'] = "";
	

	foreach ($auto_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_MARBLE." source like '".
												$site."_".$engine."%'; ";	
												
	$queries['- Moving Sites -'] = "";
	
	foreach ($moving_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = moving($site,$engine);
	
	$queries['- Security Leads -'] = "";
	
	foreach ($security as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'us'; ";	

	$queries['- Canadian Security Leads -'] = "";
	
	foreach ($canada as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'ca'; ";
	
	function andNotAffiliates($type,$month)
	{
		$aff = _getAffiliates($type,$month);
		$sql = "";
		foreach ($aff as $a)
		{
			$sql .= " and source != 'aff_$a' ";
		}
	}
	
	function _getAffiliates($type,$month)
	{
		$sql = "select distinct(source) 'source' from movingdirectory.".
				"leadbased_affiliates where	month = '".substr($month,0,6).
				"' and lead_type = '$type' order by source ";
		$data = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$data[] = $rs->myarray['source'];
		
		return $data;
	}	
									
	function moving($site,$engine)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source like '".
				$site."_".$engine."%' and received like '".YESTERDAY."%' and cat_id in".
				" (2,3) UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."_".$engine."%' and ".
				" received like '".YESTERDAY."%'";	
	}
	
	function movingOrganic($site,$not)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source".
				" like '".$site."%' and received like '".YESTERDAY."%' and cat_id in".
				" (2,3) $not UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."%' and ".
				" received like '".YESTERDAY."%' $not";	
	}
	
	function getOrganicSql($site)
	{
		$not = "";
		$engines = array("google","msn","overture");
		foreach($engines as $e)
			$not .= " and source not like '".$site."_".$e."%' ";	
		
		switch($site)
		{
			case "tm":
			case "me":
			case "pm":
				$sql = movingOrganic($site,$not);
				break;
			case "asc":
			case "mmc":
			case "cs":
			case "cq":
			case "tas":
			case "pas":
				$sql = " select count(*) 'count' from marble.auto_quotes where ".
					   " received like '".YESTERDAY."%' $not and source like ".
					   "'".$site."%' ";
				break;
			case "atus":
			case "123cm":
				$sql = " select count(*) 'count'  from movingdirectory.quotes where".
					   " received like '".YESTERDAY."%' $not and cat_id = 1 and source".
					   " like '".$site."%' ";

				break;
			case "ctsc":
			case "tsc":
			case "tac":
			case "ctac":
				$sql = " select count(*) 'count'  from irelocation.leads_security ".
					   " where received like '".YESTERDAY."%' $not and ".
					   "source like '".$site."%' ";
				break;
		}
		
		$count = 0;
		return $sql;
	}
	
	function getOrganicLeadCount($site)
	{
		$sql = getOrganicSql($site);
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$count += $rs->myarray['count'];
			
		return $count;
	}
	
	function runCode($sql)
	{
		if (substr_count($sql,"UNION") > 0)
			echo "<!--- $sql --->";
		
		$rs = new mysql_recordset($sql);
		$sum = 0;
		while($rs->fetch_array())
			$sum += $rs->myarray['count'];
		$rs->close();
		return $sum;
	}
	/*
	function twodecimals($float)
	{
		return number_format($float,2,".",",");
	}
	*/
	
	$sites = array_unique(array_keys($queries));
	
	?>
	<style>
		.text
		{
			font-face:Arial;
			font-size:12px;			
		}
	</style>	
	<div class="text">
	<form action="queries.php" method="get">		
		<table width="600"  class='filterfont'>
			<tr>
				<td align="left">
				Enter Date/Month: 
				<input type="text" name="ts" value="<?= YESTERDAY ?>" />
				<input type="submit" value="Submit" />
				</td>
			</tr>
			<tr><td align="left">
				Format: [4-digit-year][2-digit-month][2-digit-day]
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
			  <td>
		<font color="#0000FF">*Note*</font>These numbers do not take in account which lead campaign each lead went too. Because of this for example the numbers on Top Auto Leads for carshipping.com overture leads will not be the same as the numbers here. Cost is not known yet.</td>
			</tr>
			<tr><td>&nbsp;</td></tr>		
		</table>
	</form>
	
	<?
	
	
	echo "<table class='filterfont' width='600'><tr><td>Category</td><td>Site</td>".
		    "<td align='center'>Organic</td>".
			"<td align='center'>Google</td>".
			"<td align='center'>MSN</td>".
			"<td align='center'>Overture</td>".
			"<td align='center'>Total</td></tr>".
			"<tr><td colspan='12'><hr/></td></tr>";
			
	$first = true;
	
	foreach($sites as $site)
	{
		if (substr($site,0,1) == "-")
		{
			$newsite = "<strong>".ucwords(trim($site,"-"))."</strong>";
			continue;
		}
									
		
		$total_count = 0;
		
		if ($newsite != "")
		{
			if (!$first)
				echo "<tr><td colspan='12'>&nbsp;</td></tr>";
			else
				$first = false;	
			
			echo "<tr><td>$newsite</td>";
			$newsite = "";
		}
		else
			echo "<tr><td>&nbsp;</td>";
			
		echo "<td>$site</td>";
		
		$organic = getOrganicLeadCount($site);
		
		$total_count += $organic;
		
		echo "<td align='center'>$organic</td>";
		
		foreach($engines as $engine)
		{			
			$sql = $queries[$site][$engine];
			$count = runCode($sql);
			$total_count += $count;
			
			
			
			if ($count != 0)
				echo "<td align='center'>$count</td>";
			else
				echo "<td align='center'> - </td>";
		}
		
		if ($total_count != 0)
		{
			//$tcpc = number_format($total_cost/$total_count,2,".",",");
			echo "<td align='center'>$total_count</td>";
		}
		else
			echo "<td align='center'> - </td>";
		
		
		echo "</tr>\n";
							
	}
	echo "</table></div>";
	
	
	echo "<!--- \n\n";
	foreach($sites as $site)
	{
		if (substr($site,0,1) == "-")
		{
			echo "-- - - - - - - - - - - - - - - - - - -\n-- ".ucwords(trim($site,"-")).
				"\n-- - - - - - - - - - - - - - - - - -\n\n";
			continue;
		}
		
		echo "-- Site: $site\n";				
		echo "-- Organic: ".getOrganicSql($site)."\n";
		foreach($engines as $engine)
		{
			echo $queries[$site][$engine]."\n";			
		}
	}
	echo "\n --->";
	
		
	
	
include "skin_bottom.php";
	
?>