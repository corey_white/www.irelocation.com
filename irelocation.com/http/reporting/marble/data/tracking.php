<?
define(CARSHIPPING,1);

class Tracking
{
	var $websiteid;
	var $timeframe;
	
	var $remoteips;
	var $keywords;
	var $totalClicks;
	var $conversions;
	var $sortby;
	var $data;
	
	function Tracking($site=CARSHIPPING,$timeframe='x',$sortby="keyword")
	{
		if ($timeframe=='x')$timeframe = date("Ym");
		$this->websiteid = $site;
		$this->timeframe = $timeframe;
		$this->sortby = $sortby;	
		$this->keywords = array();
		$this->data = array();		
		$this->totalClicks = 0;
		$this->conversions = 0;			
	}
		
	function getTestSql($date="x")
	{
		if ($date == "x" ) $date = date("Ymd",time()-24*60*60);
		$this->data = array();
		
		$sql = "select 
			k.keyword,
			c.keywordid,
			count(clickid) 'clicks',
			(totalspend/count(clickid)) 'avgcpc',
			totalspend,
			sum(if(quoteid=0,0,1)) 'conversions',
			sum(if(quoteid=0,0,1))/count(clickid)*100 'convperc',
			count(clickid)/impressions*100 'ctr',
			avgpos,
			impressions,
			totalspend/sum(if(quoteid=0,0,1)) 'cpa'
		from 
			tk_clicks as c 
			left join 
			tk_spendings as s 
			on s.keywordid = c.keywordid 
			join
			tk_keywords as k
			on s.keywordid = k.keywordid
		where 
			received like '".$date."%'
			and s.date = '".$date."'
		group by
			c.keywordid
		order by ".$this->sortby;
		return $sql;
	}
		
	function queryWithCosts($date="x")
	{
		if ($date == "x" ) $date = date("Ymd",time()-24*60*60);
		$this->data = array();
		
		$sql = "select 
			k.keyword,
			c.keywordid,
			count(clickid) 'clicks',
			(totalspend/count(clickid)) 'avgcpc',
			totalspend,
			sum(if(quoteid=0,0,1)) 'conversions',
			sum(if(quoteid=0,0,1))/count(clickid)*100 'convperc',
			count(clickid)/impressions*100 'ctr',
			avgpos,
			impressions,
			totalspend/sum(if(quoteid=0,0,1)) 'cpa'
		from 
			tk_clicks as c 
			left join 
			tk_spendings as s 
			on s.keywordid = c.keywordid 
			join
			tk_keywords as k
			on s.keywordid = k.keywordid
		where 
			received like '$date%'
			and substring(received,1,8) = s.date
		group by
			c.keywordid
		order by ".$this->sortby;
		/*
		$sql = "select 
					k.keyword,
					c.keywordid,
					count(clickid) 'clicks',
					if (totalspend=0,'--',(totalspend/count(clickid))) 'avgcpc',
					totalspend,
					if(sum(if(quoteid=0,0,1))=0,'--',sum(if(quoteid=0,0,1))) 'conversions',
					if (sum(if(quoteid=0,0,1))=0,'--',sum(if(quoteid=0,0,1))/count(clickid)*100) 'convperc',
					count(clickid)/impressions*100 'ctr',
					avgpos,
					impressions,
					if (sum(if(quoteid=0,0,1))=0,'--',totalspend/sum(if(quoteid=0,0,1))) 'cpa'
				from 
					tk_clicks as c 
					left join 
					tk_spendings as s 
					on s.keywordid = c.keywordid 
					join
					tk_keywords as k
					on s.keywordid = k.keywordid
				where 
					received like '".$date."%'
					and substring(received,1,8) = s.date
				group by
					c.keywordid
				order by ".$this->sortby;
		*/
		$rs = new mysql_recordset($sql);
		$this->totalClicks = 0;
		$this->conversions = 0;	
		$this->totalCost = 0;
		$this->clickThroughRate = 0;
		$this->impressions = 0;
		
		while($rs->fetch_array())
		{	
			$this->data[] = $rs->myarray;
			extract($rs->myarray);
			$this->totalClicks += $clicks;
			$this->conversions += $conversions;
			$this->totalCost += $totalspend;
			$this->impressions += $impressions;
		}
		
		$this->conversionRate = ($this->conversions/$this->totalClicks)*100;
		$this->clickThroughRate = ($this->impressions/$this->totalClicks)*100;
		
		$rs->close();
	}
		
	function query()
	{
		$this->keywords = array();
		$this->remoteips = array();
		$this->totalClicks = 0;
		$this->conversions = 0;		
		$this->data = array();
		
		if ($this->timeframe == date("Ymd"))
			$today = " and received < '".date("YmdHi",time()-15*60)."' ";
		else
			$today = "";
		
		$sql = "select 
					k.keywordid,
					k.keyword,
					count(*) 'clicks',
					sum(if(quoteid>0,1,0)) 'conversions',
					(sum(if(quoteid>0,1,0))/count(*)*100) 'convperc'
				from 
					irelocation.tk_clicks as cl
					join
					irelocation.tk_adgroups as a 
					on a.adgroupid = cl.adgroupid
					join irelocation.tk_campaigns as ca 
					on ca.campaignid = a.campaignid
					join irelocation.tk_keywords as k 
					on cl.keywordid = k.keywordid
				where
					websiteid = ".$this->websiteid."
					and received like '".$this->timeframe."%'
					$today
				group by 
					keyword
				order by ".$this->sortby;
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
		{	
			$this->data[] = $rs->myarray;
			extract($rs->myarray);
			$this->keywords[$keyword]["clicks"]++;
			
			$this->remoteips[$remoteip] ++ ;
			
			$this->totalClicks += $clicks;
			
			
			$this->conversions += $conversions;
			
		}	
		$this->conversionRate = ($this->conversions/$this->totalClicks)*100;
		
	}	

}
	


?>