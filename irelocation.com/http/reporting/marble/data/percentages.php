<?
	class Percentages
	{
		var $timeframe;
	
		function Percentages($timeframe="x")
		{
			if ($timeframe == "x") $this->timeframe = date("Ym");
			else
				$this->timeframe = $timeframe;
		}
	
		function savePercentageCache($data,$timeframe="x")
		{
			if ($timeframe == "x") $timeframe = $this->timeframe;
			$insert = "insert into movingdirectory.irelo_variables (name,value) values";
			foreach ($data as $d => $v)
				$insert .= "('moving_perc_".$timeframe.$d."','$v'), ";
				
			$insert = substr($insert,0,-2);
			$rs = new mysql_recordset($insert);
		}
		
		function getPercentages($timeframe="x")
		{
			if ($timeframe == "x") $timeframe = $this->timeframe;
			if (checkForCache($timeframe))
				return loadCache($timeframe);
			else
			{
				$cpc = getCPCPercentage($timeframe);
				savePercentageCache($cpc,$timeframe);
				return $cpc;
			}
		}
		
		
		function loadCache($timeframe="x")
		{
			if ($timeframe == "x") $timeframe = $this->timeframe;
			$data = array();
			$select = "select * from movingdirectory.irelo_variables ".
					" where name like 'moving_perc_$timeframe%' ";
			$rs = new mysql_recordset($select);
			
			while ($rs->fetch_array())
			{
				$day = ereg_replace("[^0-9]","",$rs->myarray["name"]);				
				$data[substr($day,-2)] = $rs->myarray['value'];
			}
			
			return $data;
		}
		
		function checkForCache($timeframe="x")
		{
			if ($timeframe=="x")
			{
				$timeframe = $this->timeframe;
				$this_month = true;
				$count = date("d");
			}
			else 
			{
				$this_month = false;
				$year = substr($timeframe,0,4);
				$month = substr($timeframe,2,2);
				$day = substr($timeframe,4,2);
				$count = date("j",mktime(12,30,30,$month,$day,$year));
			}
			
			$select = "select count(*) count from movingdirectory.irelo_variables ".
					" where name like 'moving_perc_$timeframe%' ";
			$rs = new mysql_recordset($select);
			
			if ($rs->fetch_array())
				return ($rs->myarray['count'] == $count);			
			else
				return false;
		}
		
		function getCPCPercentage($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$engine = "all";
			$site = "all";
			
			$tm = array();
			$vl  = array();
			$sum = array();
			$percentages = array();
			
			$select_tm = "select substring(received,7,2) 'day', count(*) 'count' ".
			" From movingdirectory.quotes where cat_id = 2 and received like '$timeframe%' ";
			if ($site != "all")
				$selet_tm .= " and source like '$site%' ";
			if ($engine != "all") 
				$select_tm .= " and source like '%$engine%'	and source not like '%search%' ";
			
			$select_tm .= " group by substring(received,7,2);";
			
			
			$select_vl = "select substring(received,7,2) 'day', count(*) 'count' ".
			" From movingdirectory.quotes_local where received like '$timeframe%' ";
			if ($site != "all")
				$select_vl .= " and source like '$site%' ";
			if ($engine != "all") 
				$select_vl .= " and source like '%$engine%'	and source not like '%search%' ";
			
			$select_vl .= " group by substring(received,7,2);";
			
			$rs = new mysql_recordset($select_tm);
			while($rs->fetch_array())
			{
				$tm[$rs->myarray['day']] = $rs->myarray['count'];
				$sum[$rs->myarray['day']] = $rs->myarray['count'];
			}
			$rs->close();
			
			$rs = new mysql_recordset($select_vl);
			while($rs->fetch_array())
			{
				$day = $rs->myarray['day'];
				$vl[$day] = $rs->myarray['count'];
				$sum[$day] += $rs->myarray['count'];
				
				//TOPMOVING PERENTAGE
				$percentages[$day] = number_format(($tm[$day] / $sum[$day]),2,".",",");
			}
			$rs->close();
			
			return $percentages;
		}
	
	
	}


?>