<?
	class Vanlines
	{
		var $timeframe;
		var $site;
		var $engine;		
	
		function Vanlines($timeframe,$site,$engine)
		{
			$this->timeframe = $timeframe;
			$this->site = $site;
			$this->engine = $engine;			
		}
	
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe == "x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select if(origin_state=destination_state,'long','local') 'type', ".
					"count(*) count from movingdirectory.quotes_local where received ".
					" like '$timeframe%'";
			if ($site != "all")
				$sql .= " and source like '$site%' ";
			
			if ($engine != "all")
				$sql .= " and source like '%$engine' and source not like '%search%' ";
			$sql .= " group by if(origin_state=destination_state,'long','local')  ";
			
			echo "<!--- $sql --->\n";
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['type']] = $rs->myarray['count'];
			}

			return $data;
		}	
		
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from movingdirectory.quotes_local q where received ".
					" like '".$timeframe."%' ";					
			
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_".$engine."%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%".$engine.
							"%' and source not like '%search%' ";					
			}
			
			$sql .=  " group by substring(received,7,2) ";			
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
			}
			
			return $data;		
		}		
	}
?>