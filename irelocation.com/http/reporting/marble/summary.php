<?
	session_start();
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		header("Location: login.php");
		exit();
	}
	else
	{
		$_SESSION['time'] = date("YmdHis");
	}
	
	include "../../inc_mysql.php";	
	
	

	include "skin_top.php";
	if ($_REQUEST['ts'] == 0 || strlen($_REQUEST['ts']) < 4)
		$ts = date("Ymd",time()-3600*25);
	else 
		$ts = $_REQUEST['ts'];

	$campaigns['Top Auto'] = array("select count(*) 'count' from marble.auto_quotes where received like '$ts%' and campaign = 'auto'","'mmc','asc','cs','csq','pas','asc-local'","auto");
	$campaigns['Auto-Transport'] = array("select count(*) 'count' from marble.auto_quotes where received like '$ts%' and campaign = 'atus' ","'atus','123cm'","auto-atus");
	$campaigns['T&P Moving'] = array("select count(*)  'count' from movingdirectory.quotes where received like '$ts%' and cat_id = 2 ","'tm','pm','me','tm-local'","moving");
	$campaigns['Vanlines Moving'] = array("select count(*) 'count' from movingdirectory.quotes_local where received like '$ts%'","","vanlines");
	$campaigns['Security'] = array("select count(*) 'count' from irelocation.leads_security where received like '$ts%' and campaign = 'us'" ,"'tsc','tac','tsc-local'","security");
	
	$campaigns['CN-Security'] = array("select count(*) 'count' from irelocation.leads_security where received like '$ts%' and campaign = 'ca'" ,"'ctsc','ctac'","ca.security");
	
	$campaigns['AU-Security'] = array("select count(*) 'count' from irelocation.leads_security where received like '$ts%' and campaign = 'aas'" ,"'au','atac'","au.security");
	
	function getMovingCosts($campaign,$ts)
	{
		if ($campaign == "moving") $campaign = "top";
		else if ($campaign == "vanlines") $campaign = "van";
		
		$sql = " select sum(cost) 'cost' from movingdirectory.percentages where ".
				" campaign = '".$campaign."' and timeframe = '".$ts."'";
		
		echo "\n<!-- $campaign sql : $sql --->\n";			
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
			return $rs->myarray['cost'];				
		else
			return 0;
	}
	
	function getAffiliateSpending($campaign,$ts)
	{
		$cost = getAffiliateCost($campaign,$ts);		
		return $cost;		
	}

?>	
<table border="0" width="100%" cellspacing=0 cellpadding=2>
	<Tr><td>&nbsp;</td></tr>
	
	<Tr><td>&nbsp;</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Today is the: <?= date("jS \o\f F"); ?></font></b>
	</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Below is the data for yesterday the <?= date("jS \o\f F",time()-24*3600); ?></font></b><br/>
	</td></tr>
	<Tr><td>&nbsp;</td></tr>
	<? foreach($campaigns as $title => $data) 
	{ 
		echo "<!--- $data[0] --->";
		
		$rs = new mysql_recordset($data[0]);					
		if (!$rs->fetch_array())
			continue;
			
		$count = $rs->myarray['count'];
		if ($data[2] == "moving" || $data[2] == "vanlines")
		{
			$cost = getMovingCosts($data[2],$ts);			
			echo "\n<!--- ".$data[2]." = $cost --->\n";
		}
		else
		{
			$sql = "select sum(cost) 'cost' from movingdirectory.costperclick ".
				   " where site in  (".$data[1].") and ts like '$ts%'; ";
			echo "<!--- $sql --->";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$cost = $rs->myarray['cost'];
		}
	?>
	<Tr><td>
		<strong><?= $title ?></strong>
		<ul>
			<li><?= $count ?> Leads.</li>
			<? if ($cost > 0) { ?>
			<li>$<?= number_format($cost,2,".",","); ?> Spent on Advertising.</li>
			<? } ?>
			<? $affspend = getAffiliateSpending($data[2],$ts);
			if ($affspend > 0)
				echo "<li>$".number_format($affspend,2,".",",")." Spent on Affiliates</li>\n";
			
			if ($affspend > 0 || $cost > 0) 
			{
				$total = $affspend+$cost;
				echo "<li>$".number_format($total,2,".",",")." Spent total</li>\n";
			}
			if ($cost > 0 && $count > 0)
			{
				$cpl = $cost/$count;
				echo "<li>$".number_format($cpl,2,".",",")." Blended Cost Per Lead</li>\n";
			}
			?>

		</ul>
	</td></tr>
	<? } ?>
</table>	
						
	
	

</td></tr></table></td></tr></table></td></tr></table>