<?


	function getColor($name)
	{				
		switch($name)
		{
			case "tm":
			case "asc":
			case "google":
				$a = array(0,0,255);
				break;
			case "pm":
			case "cs":
			case "overture":
				$a = array(255,0,0);
				break;
			case "me":
			case "mmc":
			case "msn":
				$a = array(0,255,0);
				break;
			case "ask":
				$a = array(255,255,0);
				break;
			default:
				$a = array(238,233,233);
				break;
		}		
		return $a;
	}

	function percentToAngle($perc)
	{
		if ($perc >= 1 && $perc <= 100)
			return $perc/100*360;
		else if ($perc > 0)
			return $perc*360;
		else
			return $perc;		
	}

	//in (array(array(<name>,<amount>),*),total)
	function setAngles($data,$total)
	{
		$newdata = array();
		foreach($data as $entry)
		{
			$entry[2] = percentToAngle(($entry[1]/$total));
			$entry[3] = getColor($entry[0]);
			$newdata[] = $entry;
			
		}
		return $newdata;
	}

	

	function getPieChart($data)
	{
		$total = 0;
		
		foreach($data as $d)		
			$total += $d[1];
		
		$data = setAngles($data,$total);
		
	
		$Randomized = count($data);
		
		$imgx='200';
		$imgy='200';//Set Image Size. ImageX,ImageY
		
		$cx = '100';
		$cy ='75'; //Set Pie Postition. CenterX,CenterY
		
		$sx = '200';
		$sy='150';
		$sz ='20';// Set Size-dimensions. SizeX,SizeY,SizeZ
			
		//convert to angles.
		for($i=0;$i<=$Randomized;$i++)
		{
		   $angle[$i] = $data[$i][2];
		   $angle_sum[$i] = array_sum($angle);
		};
		
		$im  = imagecreate ($imgx,$imgy);	
		$background = imagecolorallocate($im, 255, 255, 255);
		//Random colors.
		for($i=0;$i<=$Randomized;$i++)
		{	 
		   list($r,$g,$b) = $data[$i][3];
		   
		   $colors[$i] = imagecolorallocate($im,$r,$g,$b);
		   $colord[$i] = imagecolorallocate($im,($r/2),($g/2),($b/2));
		}
		//3D effect.
		for($z=1;$z<=$sz;$z++)
		{
			for($i=0;$i<=$Randomized;$i++)
			{
				imagefilledarc($im,$cx,($cy+$sz)-$z,$sx,$sy,$angle_sum[$i-1]-90,$angle_sum[$i]-90,$colord[$i],IMG_ARC_PIE);
			};
		};
		
		//Top pie.
		for($i=0;$i<=$Randomized;$i++)
		{
			imagefilledarc($im,$cx,$cy,$sx,$sy,$angle_sum[$i-1]-90 ,$angle_sum[$i]-90, $colors[$i], IMG_ARC_PIE);
		};
		
		
		//Output.
		header('Content-type: image/png');
		imagepng($im);
		imagedestroy($im);
	}
	
	$data[] = array("google",150);
	$data[] = array("overture",100);
	$data[] = array("msn",80);
	$data[] = array("ask",40);
	getPieChart($data);
?>