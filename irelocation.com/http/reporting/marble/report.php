<?
	include_once "skin_top.php";
	include_once "marblehelper.php";
	include_once "generic.php";
	include_once "marbletrends.php";
	define(DEVIATION,5);
	$campaign = $_REQUEST['campaign'];
	
	function _getSortLink($name,$index)
	{
		$l = "<a href='report.php?campaign=".$_REQUEST['campaign'];
		$l .= "&sort=$index";
		if ($_REQUEST['sort'] == $index && !isset($_REQUEST['asc']))
			$l .= "&asc";
		$l .= "'>$name</a>";
		return $l;	
	}
	
	function getSortBy($campaign)
	{	
		$goal_based = ($campaign == "moving" || substr_count($campaign,"auto") > 0);
		
	
		$sort = $_REQUEST['sort'];
		$dir = (isset($_REQUEST['asc']))?" asc":" desc";
		if ($sort != "")
		{
			if (is_numeric($sort) && $sort > -1 || $sort > 7)
			{
				if ($goal_based)
					$sorts = array("co.comp_name","c.lead_id","Lead_Count",
					       "Actual_Sent","Est_Sent","needed_per_day",
						   "deviation","leads_needed");
				else
					$sorts = array("co.comp_name","c.lead_id","Lead_Count",
									"Actual_Sent","leads_needed");
				$sort = $sorts[$sort];
			}
		}
		else	
			$sort = "co.comp_name";
			
		return " order by c.active desc, $sort $dir";
	}
	
	
	if ($campaign=="") $campaign = "auto";

	$month = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	$a = getCampaignDetails($campaign,$month);
	
	function _getAllCompanies($a,$month)
	{
		if (substr($a['campaign'],0,4) == "auto")
		{
			$db = "marble";		
			$comp = "$db.companies";
		}
		else
		{
			$db = "movingdirectory";	
			$comp = "$db.company";
		}
		
		$sql = "Select co.comp_name,ca.* from $db.campaign as ca join $comp as co ".
				"on co.comp_id = ca.comp_id where ca.site_id = '".$a['campaign_code']."' and ".
				" ca.month = '$month'; ";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql );
		while ($rs->fetch_array())
		{
			$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
			$companies[$rs->myarray["lead_id"]] = $rs->myarray;
		}
		
		return $companies;
	}
	
	function _getCompanySql($a,$month)
	{
		if (substr($a['campaign'],0,4) == "auto")
		{
			$db = "marble";		
			$comp = "$db.companies";
		}
		else
		{
			$db = "movingdirectory";	
			$comp = "$db.company";
		}
		$site_id = $a['campaign_code'];
		$table = $a['table'];
		$filter = $a['filter'];
		
		if (strlen($month) == 6 && $month != date("Ym"))
		{
			$today = getNDays(substr($month,4),substr($month,0,4));
			echo "<!--- today: $today --->";
			$this_month = $month;
			$full_days = $today;
			$today_sql = $today;
			$show_old_data = true;
			$days_in_month = $today;
			$days_left = 1;			
		}
		else
		{
			//$today_sql = " and received not like '".date("Ymd")."%' ";
			$today_sql =  max(1,date("d")-1) + getTrends();
			$today = date("d");
			$full_days = $today-1;
			if ($full_days == 0)
				$full_days = 1;
			$this_month = $month;
			$show_old_data = false;
			$days_in_month = date('t');
			$days_left = $days_in_month - $today + 1;
		}
		
		$sql = "select 
				co.comp_name,
				c.lead_id,
				c.temp_goal,
				(c.monthly_goal+c.temp_goal) 'Lead_Count',
				count(q.quote_id) 'Actual_Sent',
				ceiling((c.monthly_goal+c.temp_goal) / $days_in_month ) * $today_sql 'Est_Sent',	
				ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ ".($days_left)." ) 'needed_per_day',	
				count(q.quote_id) - (ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ $days_in_month ) * $today_sql) 'deviation',
				c.monthly_goal - count(q.quote_id) 'leads_needed',
				c.active
			from
				$db.campaign as c
				left join 
				$table as q
				on locate(c.lead_id,q.lead_ids) > 0
				join $comp as co
				on	
				c.comp_id = co.comp_id
			where 				
				c.site_id = '$site_id'
				AND q.received like '$this_month%'
				and c.month = '$this_month' 
				$filter
			group by
				c.lead_id 
			".getSortBy($a['campaign']);
		echo "\n<!--- $sql --->\n";
		return $sql;	
	}
	
	function _totals($a,$month)
	{
		$total_count = "select count(*) count from ".$a['table']." as q where ".
					"received like '$month%' ".$a['filter'];
		$rs = new mysql_recordset($total_count);
		$rs->fetch_array();
		$total_leads = $rs->myarray["count"];
		$rs->close();
	
		if ($month == date("Ym"))
		{
			$total_count = "select count(*) count from ".$a['table']." as q where ".
							"received like '$month".date("d")."%' ".$a['filter'];
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads_today = $rs->myarray["count"];
			$rs->close();
		}
		else	
			$total_leads_today = 0;
			
		return array($total_leads,$total_leads_today);			
	}
	
	function _lastWeek($a,$month)
	{
		$msg = "";
		if ($month == date("Ym"))
		{
			$lwtime = time()-7*24*60*60;
			$lastweek = date("YmdHis",$lwtime);
			$lastweekday  = date("Ymd",$lwtime);
			$sql = "select count(*) 'count' from ".$a['table']." as q where ".
				  " received like '$lastweekday%' and received < '$lastweek' ".
				   $a['filter'];
			echo "<!--- $sql --->\n";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$leads = $rs->myarray['count'];
			$msg = "We received $leads leads this time last ".date("l",$lwtime).".";
		}
		return $msg;
	}

	list($total_leads,$total_leads_today) = _totals($a,$month);
	
	
?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>		
	<Tr>
		<td>
		<form action="report.php?campaign=<?= $campaign ?>" method="post">
			<?= printPeriodDropDown("ts",$month); ?>
			<input type='submit' name="go" value="Go" />
		</form>
		</td>
	</Tr>
	<Tr><td>&nbsp;</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Today is the: <?= date("jS \o\f F"); ?></font></b>
	</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
		
	</td></tr>
	<? if ($month == date("Ym")) { ?>
	<tr><td>
		<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>			
	</td></tr>
	<tr><td><b><font face="Verdana" size="2"><?= _lastWeek($a,$month) ?></font></b></td></tr>
	<? } ?>	
	<Tr><td>
	
	<?
		$count = 1;	
		$font_counter = 1;	
		$companies = _getAllCompanies($a,$month);
		$sql = _getCompanySql($a,$month);
		$rs = new mysql_recordset($sql);
		if ($rs->rowcount() == 0)
		{
			echo "<b><font face='Verdana' size='2'>";
			echo "No Companies have been setup on this Campaign yet.";
			echo "</font></b>";
			exit();
		}
	?>
	
	<div align="right">
	<font face="Verdana" size="2">
	<font color="#FF0000">Greater than 5% differance</font>
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <Br/>
	<font color="#006600">Less than 5% differance</font>
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <Br/>
	</font>
	</div>
	<table border='1'>
	<tr class='header white' ><td>&nbsp;</td>
			<th align='left' ><?= _getSortLink("Company",0) ?></th>
			<th><?= _getSortLink("Lead ID",1) ?></th>
			<th>Monthly Goal</th>
			<th>Sent</th>			
	    <? if ($a['table'] != "irelocation.leads_security") { ?>
			<th>Est. Sent</th>
			<th>Deviation</th>
		<? if (!$show_old_data) { ?>
			<th>Needed Per Day</th>
		<? } 
		
		}
		?>
			<th>Leads Needed</th>
		</tr>
	<?
	
	
	
	while($rs->fetch_array())
	{		
		extract($rs->myarray);
		
		$companies[$lead_id] = 0;
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count++."</td><td>";
		if (!$active)
			echo "<font color='red'>INACTIVE </font>";
		echo "<a class='black' title='Click to view lead format' href='marbleformat.php?lead_id=$lead_id'>";
		echo "$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		
		if ($temp_goal > 0)
			echo "<font color='#006600'>+$temp_goal</font> ";
		else if ($temp_goal < 0)
			echo "<font color='#FF0000'>$temp_goal</font> ";
		
		echo $Lead_Count."</td><td align='right'>";
		echo $Actual_Sent."</td>";
		
		
		//est sent
		if ($a['table'] != "irelocation.leads_security")
		{
			echo "<td align='right'>";
			if ($month == date("Ym"))
			{
				echo (($active)?floor($Est_Sent):"--").
						"</td><td align='right'>";
			}
			else
			{
				echo (($active)?floor($Lead_Count):"--").
						"</td><td align='right'>";		
			}
			
			//deviation
			if ($active)
				echo colorCode($Actual_Sent,floor($Est_Sent),DEVIATION);
			else
				echo " -- ";
			echo "</td>";
			
			if (!$show_old_data)
			{
				echo "<td align='right'>";		
				echo (($active)?$needed_per_day:"--")."</td>";
			}		
		}
		
			

		echo "<td align='right'><strong>".(($active)?$leads_needed:"--")."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}
	
	
	
	foreach($companies as $comp)
	{		
		if ($comp == 0)
			continue;
		else
			extract($comp);
			
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count."</td><td>";
		$count++;
		if (!$active)
			echo "<font color='red'>INACTIVE </font>";
		echo "<a class='black' title='Click to view lead format' href='marbleformat.php?lead_id=$lead_id'>";
		echo "$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		echo "$monthly_goal</td><td align='right'>";
		echo "0</td><td align='right'>";
		echo "-</td><td align='right'>";
		
		$deviation = $monthly_goal;
		if ($deviation > 0)
			$deviation = "<font color='#000000'><strong>$deviation</strong></font>";
		else if ($deviation < 0)
			$deviation = "<font color='#FF0000'>$deviation</font>";
		else 
			$deviation = "<font color='#00FF00'>$deviation</font>";
			
		echo $deviation."</td>";
		
		if (!$show_old_data)
		{
			echo "<td align='right'>-</td>";
		}
		echo "<td align='right'><strong>".$monthly_goal."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}
	
	
	?>