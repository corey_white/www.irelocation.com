<?
	session_start();
	
	include "../../inc_mysql.php";
	
	
	/*
		This script copies all campaign entries for Atus from this month
		to next month that haven't been cancelled. all companies by default
		are set to active, and the temp_goal field is cleared out.	
	*/
	
	$sql = "
			insert into movingdirectory.campaign
			(lead_id,comp_id,monthly_goal,leads_per_day,
			site_id,sort_order,needed,active,month,temp_goal)
			
			select 
			lead_id,comp_id,monthly_goal,leads_per_day, 
			'atus' as 'site_id',sort_order,monthly_goal as 'needed',1 as 'active',
			".date("Ym",strtotime("+1 month"))." as 'month',0 as 'temp_goal'
			from 
			movingdirectory.campaign where site_id = 'atus' and ".
			" month = '".date("Ym")."' ".
			" and cancelled is null ";
		
	$rs = new mysql_recordset($sql);
	
	header("Location: atusreport.php?copied");
	exit();
?>