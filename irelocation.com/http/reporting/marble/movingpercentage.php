<?	
	session_start();
	
	define(CPC_DEBUG,true);
	
	include "skin_top.php";
	
	//-- if cap form posted, process
	if ( $auto_cap_submit ) {

		if ( !is_numeric($auto_cap_override) ) {
			echo "<p style=\"font-family: Verdana; font-size: 14px; color: red;\" align=\"center\">You're entry is not a number, it was not entered.</p>";
			echo "<p style=\"font-family: Verdana; font-size: 14px; color: red;\" align=\"center\">(decimal place is okay, commas are not)</p>";
		} else {
			$insert = "update movingdirectory.irelo_variables set value = '$auto_cap_override' where name = 'topauto_revenue_cap' ";
			$insert_rs = new mysql_recordset($insert);
		}
	} 
	
	if ( $atus_cap_submit ) {

		if ( !is_numeric($atus_cap_override) ) {
			echo "<p style=\"font-family: Verdana; font-size: 14px; color: red;\" align=\"center\">You're entry is not a number, it was not entered.</p>";
			echo "<p style=\"font-family: Verdana; font-size: 14px; color: red;\" align=\"center\">(decimal place is okay, commas are not)</p>";
		} else {
			$insert = "update movingdirectory.irelo_variables set value = '$atus_cap_override' where name = 'atus_revenue_cap' ";
			$insert_rs = new mysql_recordset($insert);
		}
	} 
	
	$array = get("vanlines_percentage,moving_to_atus,topmoving_cap,atus_percentage");
	extract($array);
	if ($vanlines_percentage >= 0)
	{
		?>
		<script>

			function validate(form)
			{
				var value = form.new_percent.value;
				if (isNaN(value) || (value > 100 || value < 0))
				{
					alert("Please enter a number between 0 and 100");			
					form.new_percent.focus();
				}
				else
					form.submit();
			}
		</script>
		<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<? if (isset($_REQUEST['bad']) || isset($_REQUEST['good'])) 
		{ 
			$msgs['range'] = "<font color='#FF0000'><strong>Invalid Percentage Range.</strong></font>";
			$msgs['password'] = "<font color='#FF0000'><strong>Invalid Password.</strong></font>";
			$msgs['changed'] = "<font color='#00FF00'><strong>Percentage Updated.</strong></font>";
			$msgs['nochange'] = "<font color='#0000FF'><strong>Percentage Left unchanged.</strong></font>";
			
			$msgs['atuschanged'] = "<font color='#00FF00'><strong>Auto Opt-In Lead Destination Changed.</strong></font>";
			$msgs['atusnochange'] = "<font color='#0000FF'><strong>Auto Opt-In Lead Destination Left unchanged.</strong></font>";

			$msgs['capchanged'] = "<font color='#00FF00'><strong>Mover Daily Cap Changed.</strong></font>";
			$msgs['capnochange'] = "<font color='#0000FF'><strong>Mover Daily Cap Left unchanged.</strong></font>";

			echo "<Tr><td>&nbsp;</td></tr>";
			echo "<tr><td><font face='Verdana' size='2'>";
			if (isset($_REQUEST['good']))
				echo $msgs[$_REQUEST['good']];
			else
				echo $msgs[$_REQUEST['bad']];
			echo "</font></td></tr>";
			echo "<Tr><td>&nbsp;</td></tr>";
		}
		?>
		<? /*
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<font face="Verdana" size="2">
			<?
				$percent = $vanlines_percentage;
				echo $percent."% of moving leads will go to Vanlines.com";
			?>
			</font>
		</td></tr>
		<Tr><td>
		<font face="Verdana" size="2">
		<form name="updatepercentage" action="moving_update.php" method="post">
			<input type="hidden" name="action" value="vanlines" />
			<input type="hidden" name="current" value="<?= $percent ?>" />
			Enter a new value: <input type="text" value="<?= $percent ?>" name="new_percent" size="4" maxlength="3" />%
			<br/>
			<input type="button" name="go" value="Update"  onClick="validate(document.updatepercentage);"/>
		</form>
		</font>
		</td></tr>
		<Tr><td align="left"><hr width="40%" align="left"/></td></Tr>
		*/ ?>
		<tr><td>
			
		</td></tr>
		<Tr><td>
		<font face="Verdana" size="2">
		<form name="updatepercentage" action="moving_update.php" method="post">
			<input type="hidden" name="action" value="atus_percentage" />
			<input type="hidden" name="current" value="<?= $atus_percentage ?>" />
			<input type="text" value="<?= $atus_percentage ?>" name="new_atus_percentage" 
				size="3" maxlength="3" />% 
			 of leads from Top Auto campaign will be fed into Atus
			<br/><br/>
			<input type="submit" name="go" value="Update" />
		</form>
		</font>
		</td></tr>
		
		<Tr><td align="left"><hr width="40%" align="left"/></td></Tr>
		
		<tr><td>
			<font face="Verdana" size="2">
			<form action="moving_update.php" method="post">
				<input type="hidden" name="action" value="autooptin" />
				<input type="hidden" name="current" value="<?= $moving_to_atus ?>" />
				Auto Opt-in leads on Moving Forms will go to:<br/>
				  &nbsp; &nbsp; <input type="radio" name="atus" value="1" <? if($moving_to_atus=="1")echo "checked"; ?> /> Auto-Transport.us<Br/>
				  &nbsp; &nbsp; <input type="radio" name="atus" value="0" <? if($moving_to_atus=="0")echo "checked"; ?> /> Top Auto Leads
				<br/>
				<input type="submit" value="Update" />
			</form>
			</font>
		</td></tr>
		<Tr><td align="left"><hr width="40%" align="left"/></td></Tr>
		
		<Tr><td>&nbsp;</td></tr>
		<? /*
		<Tr><td><font face="Verdana" size="2">
				A program runs every 20 minutes against this value.<br/>
				If the movers have gotten more than this number of leads<Br/>
				 in a given day, the Program will send all further leads<Br/> to Vanlines.com</font></td></Tr>
		<Tr><td><font face="Verdana" size="2">
				A Subset of this program runs at Midnight and sets<Br/>
				the <a href='#' title="Sirva,Wheaton,Bekins" style="text-decoration:none; color:#0000FF;" >TopMovers</a> Cap to the number of leads it <br/>
				needs per day to finish out the month with 2500 leads.
				</font></td></Tr>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<font face="Verdana" size="2">
			<form action="moving_update.php" method="post">
				<input type="hidden" name="action" value="topmoving_cap" />
				<input type="hidden" name="current" value="<?= $topmoving_cap ?>" />
				Change the Maximum # of Leads that <Br/>the <a href='#' title="Sirva,Wheaton,Bekins" style="text-decoration:none; color:#0000FF;" >TopMovers</a> will get per day:
				<input type="text" name="new_mover_cap" value="<?= $topmoving_cap ?>" size="4" maxlength="3" />
				<br/>
				<input type="submit" value="Update" />
			</form>
			</font>
		</td></tr>
		<Tr><td>
			<font face="Verdana" size="2">
				<A href='http://www.irelocation.com/vanlines/cron.php' target='_blank' style="text-decoration:none; color:#0000FF;">
					Run Mover Cap Cron now.
				</A>
			</font>
		</td></tr>				
		<Tr><td>&nbsp;</td></tr>
		*/ ?>
		<Tr><td>
			<? 
				$topauto = get("topauto_revenue_cap"); 
				$atus = get("atus_revenue_cap"); 
			?>			
			<font face="Verdana" size="2">
				Top Auto Revenue Cap: $<?= $topauto ?> 
				<A href='recalc.php?site=auto' style="text-decoration:none; color:#0000FF;">
					re-calc
				</A><br/>
				Atus Revenue Cap: $<?= $atus ?>
				<A href='recalc.php?site=atus' style="text-decoration:none; color:#0000FF;">
					re-calc
				</A><br/>
			</font>
		</td></tr>				
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td><span style="font-family: Verdana; font-size: 12px;">Manually Override the Top Auto Revenue Cap:</span>
			<form action="movingpercentage.php" method="post">
				<input name="auto_cap_override" type="text" size="15">
				<input name="auto_cap_submit" type="submit" value="override">
			</form>
						
			</td>
		</tr>
		<tr>
			<td><span style="font-family: Verdana; font-size: 12px;">Manually Override the Atus Revenue Cap:</span>
			<form action="movingpercentage.php" method="post">
				<input name="atus_cap_override" type="text" size="15">
				<input name="atus_cap_submit" type="submit" value="override">
			</form>
						
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>

		<?
	}		
	else
	{
		echo "<strong>ERROR VANLINES PERCENTAGE NOT FOUND!";		
	}
	include "skin_bottom.php";

?>