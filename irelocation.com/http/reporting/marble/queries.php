<?
	session_start();
	
	include $_SERVER['DOCUMENT_ROOT']."/reporting/marble/skin_top.php";

	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	
	if($_REQUEST['ts'] == "")
		define(YESTERDAY, date("Ymd",time()-24*60*60));
	else
		define(YESTERDAY, $_REQUEST['ts']);
		
	$queries = array();
	$DATE = " where received like '".YESTERDAY."%' and ";
	
	$SELECT_SECURITY = " select count(*) 'count' from irelocation.leads_security ".$DATE;
							   
	$SELECT_AUTO = " select count(*) 'count' from movingdirectory.quotes ".$DATE.
					" cat_id = 1 and ";
	$SELECT_MARBLE = " select count(*) 'count' from marble.auto_quotes ".$DATE;
		  	
	
	$engines = array("google","msn","overture");
	
	$marble_sites = array("cs","asc","mmc","tas","cq","pas","usat");
	$moving_sites = array("tm","me","pm");
	$auto_sites = array("atus","123cm");
	$security = array("tsc","tac");
	$canada = array("ctsc","ctac");
	$australia = array("atsc","atac");
	$securitymatrix = array("usalarm");
	
	$queries['- Top Auto Leads -'] = "";
	
	foreach ($marble_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_MARBLE." source like '".
										$site."_".$engine."%'; ";	
	$queries['- Auto-Transport.us -'] = "";
	

	foreach ($auto_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_MARBLE." source like '".
												$site."_".$engine."%'; ";	
												
	$queries['- Moving Sites -'] = "";
	
	foreach ($moving_sites as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = moving($site,$engine);
	
	$queries['- Security Leads -'] = "";
	
	foreach ($security as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'us'; ";	

	$queries['- Canadian Security Leads -'] = "";
	
	foreach ($canada as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'ca'; ";
										
	$queries['- Australian Security Leads -'] = "";
	
	foreach ($australia as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%' and campaign = 'aas'; ";
	
	$queries['- Security Matrix Leads -'] = "";
	
	foreach ($securitymatrix as $site)
		foreach($engines as $engine)
			$queries[$site][$engine] = $SELECT_SECURITY." source like '".
										$site."_".$engine."%'; ";
	
	
	function andNotAffiliates($type,$month)
	{
		$aff = _getAffiliates($type,$month);
		$sql = "";
		foreach ($aff as $a)
		{
			$sql .= " and source != 'aff_$a' ";
		}
	}
	
	function _getAffiliates($type,$month)
	{
		$sql = "select distinct(source) 'source' from movingdirectory.".
				"leadbased_affiliates where	month = '".substr($month,0,6).
				"' and lead_type = '$type' order by source ";
		$data = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$data[] = $rs->myarray['source'];
		
		return $data;
	}	
	
	function getCost($site, $engine)
	{
		$sql = "select sum(cost) 'cost' from movingdirectory.costperclick where ".
				" ts like '".YESTERDAY."%' and site like '".$site."%' and ".
				" search_engine = '".$engine."'; ";
		//echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$cost = $rs->myarray['cost'];
		$rs->close();
		return $cost;
	}
										
	function moving($site,$engine)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source like '".
				$site."_".$engine."%' and received like '".YESTERDAY."%' and cat_id in".
				" (2,3) UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."_".$engine."%' and ".
				" received like '".YESTERDAY."%'";	
	}
	
	function movingOrganic($site,$not)
	{
		return "select count(*) 'count' from movingdirectory.quotes where source".
				" like '".$site."%' and received like '".YESTERDAY."%' and cat_id in".
				" (2,3) $not UNION ALL select count(*)  'count' from movingdirectory.".
				"quotes_local where source like '".$site."%' and ".
				" received like '".YESTERDAY."%' $not";	
	}
	
	function getOrganicSql($site)
	{
		$not = "";
		$engines = array("google","msn","overture");
		foreach($engines as $e)
			$not .= " and source not like '".$site."_".$e."%' ";	
		
		switch($site)
		{
			case "tm":
			case "me":
			case "pm":
				$sql = movingOrganic($site,$not);
				break;
			case "asc":
			case "mmc":
			case "cs":
			case "cq":
			case "usat":
			case "tas":
			case "pas":
				$sql = " select count(*) 'count' from marble.auto_quotes where ".
					   " received like '".YESTERDAY."%' $not and source like ".
					   "'".$site."%' ";
				break;
			case "atus":
			case "123cm":
				$sql = " select count(*) 'count'  from movingdirectory.quotes where".
					   " received like '".YESTERDAY."%' $not and cat_id = 1 and source".
					   " like '".$site."%' ";

				break;
			case "ctsc":
			case "tsc":
			case "usalarm":
			case "atac":
			case "atsc":
			case "tac":
			case "ctac":
				$sql = " select count(*) 'count'  from irelocation.leads_security ".
					   " where received like '".YESTERDAY."%' $not and ".
					   "source like '".$site."%' ";
				break;
		}
		
		$count = 0;
		return $sql;
	}
	
	function getOrganicLeadCount($site)
	{
		$sql = getOrganicSql($site);
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$count += $rs->myarray['count'];
			
		return $count;
	}
	
	function runCode($sql)
	{
		if (substr_count($sql,"UNION") > 0)
			echo "<!--- $sql --->";
		
		$rs = new mysql_recordset($sql);
		$sum = 0;
		while($rs->fetch_array())
			$sum += $rs->myarray['count'];
		$rs->close();
		return $sum;
	}
	/*
	function twodecimals($float)
	{
		return number_format($float,2,".",",");
	}
	*/
	function color($site,$cpl)
	{
		$BAD = "#FF0000";
		$DANGER = "#FF6600";
		$color = "#006633";
		switch($site)
		{
			case "tm":
			case "me":
			case "pm":
				if ($cpl > 13) $color = $BAD;
				else if ($cpl > 10) $color = $DANGER;
				break;
			case "atus":
			case "123cm":
				if ($cpl > 7) $color = $BAD;
				else if ($cpl > 6) $color = $DANGER;
				break;
			case "tsc":
			case "tac":
				if ($cpl > 30) $color = $BAD;
				else if ($cpl > 25) $color = $DANGER;
				break;
			case "ctac":
			case "ctsc":
				if ($cpl > 40) $color = $BAD;
				else if ($cpl > 30) $color = $DANGER;
				break;
			case "atac":
			case "atsc":
				if ($cpl > 40) $color = $BAD;
				else if ($cpl > 30) $color = $DANGER;
				break;
			case "usalarm":
				if ($cpl > 50) $color = $BAD;
				else if ($cpl > 45) $color = $DANGER;
				break;
			case "asc":
			case "mmc":
			case "cs":
			case "usat":
			case "cq":
			case "tas":
			case "pas":
				if ($cpl > 6.25) $color = $BAD;
				else if ($cpl > 5.5) $color = $DANGER;
				break;
		}		
		
		if ($color != $BAD)
			return "<font color='$color'>".twodecimals($cpl)."</font>";
		else
			return "<font color='$color'><strong>".twodecimals($cpl)."</strong></font>";
	}
	
	$sites = array_unique(array_keys($queries));
	
	?>
	<style>
		.text
		{
			font-face:Arial;
			font-size:12px;			
		}
	</style>	
	<div class="text">
	<form action="queries.php" method="get">		
		<table width="600">
			<tr>
				<td align="left">
				Enter Date/Month: 
				<input type="text" name="ts" value="<?= YESTERDAY ?>" />
				<input type="submit" value="Submit" />
				</td>
			</tr>
			<tr><td align="left">
				Format: [4-digit-year][2-digit-month][2-digit-day]
			</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr><td>
		<font color="#0000FF">*Note*</font>These numbers do not take in account which lead campaign each lead went too. Because of this for example the numbers on Top Auto Leads for carshipping.com overture leads will not be the same as the numbers here. the cost-per-lead will also differ for the same reason.
		</td></tr>
			<tr><td>&nbsp;</td></tr>		
		</table>
	</form>
	
	<?
	
	
	echo "<table width='600'><tr><td>Category</td><td>Site</td>".
		    "<td colspan='2' align='center'>Organic/Free</td>".
			"<td colspan='2' align='center'>Google</td>".
			"<td colspan='2' align='center'>MSN</td>".
			"<td colspan='2' align='center'>Overture</td>".
			"<td colspan='2' align='center'>Total</td></tr>".
			"<tr><td colspan='12'><hr/></td></tr>";
			
	$first = true;
	
	foreach($sites as $site)
	{
		if (substr($site,0,1) == "-")
		{
			$newsite = "<strong>".ucwords(trim($site,"-"))."</strong>";
			continue;
		}
									
		$total_cost = 0;
		$total_count = 0;
		
		if ($newsite != "")
		{
			if (!$first)
				echo "<tr><td colspan='12'>&nbsp;</td></tr>";
			else
				$first = false;	
			
			echo "<tr><td>$newsite</td>";
			$newsite = "";
		}
		else
			echo "<tr><td>&nbsp;</td>";
			
		echo "<td>$site</td>";
		
		$organic = getOrganicLeadCount($site);
		
		$total_count += $organic;
		
		echo "<td colspan='2' align='center'>$organic</td>";
		
		foreach($engines as $engine)
		{			
			$sql = $queries[$site][$engine];
			$count = runCode($sql);
			$total_count += $count;
			$cost = getCost($site, $engine);
			$total_cost += $cost;
			if ($count != 0)
				$cpc = number_format($cost / $count,2,".",",");
			else
				$cpc = 0;
			if ($count != 0)
				echo "<td>$count</td><td>\$".color($site,$cpc)."</td>";
			else if ($cost != 0)
			{
				echo "<td>0</td><td>\$".color($site,$cpc)."</td>";
			}
			else
				echo "<td colspan='2' align='center'> - </td>";
		}
		
		if ($total_count != 0)
		{
			$tcpc = number_format($total_cost/$total_count,2,".",",");
			echo "<td>$total_count</td><td>\$".color($site,$tcpc)."</td>";
		}
		else if ($total_cost != 0)
		{
			$tcpc = number_format($total_cost,2,".",",");
			echo "<td>0</td><td>\$".color($site,$tcpc)."</td>";
		}
		else
			echo "<td colspan='2' align='center'> - </td>";
		
		
		echo "</tr>\n";
							
	}
	echo "</table></div>";
	
	
	echo "<!--- \n\n";
	foreach($sites as $site)
	{
		if (substr($site,0,1) == "-")
		{
			echo "-- - - - - - - - - - - - - - - - - - -\n-- ".ucwords(trim($site,"-")).
				"\n-- - - - - - - - - - - - - - - - - -\n\n";
			continue;
		}
		
		echo "-- Site: $site\n";				
		echo "-- Organic: ".getOrganicSql($site)."\n";
		foreach($engines as $engine)
		{
			echo $queries[$site][$engine]."\n";
			echo "-- \$: ".getCost($site,$engine)."\n\n";
		}
	}
	echo "\n --->";
	
		
	
	
include "skin_bottom.php";
	
?>