<?
	session_start();
	include "skin_top.php";
	include "data/tracking.php";
	
	echo "<!--- ".print_r($_POST,true)." --->";
	
	$website = ($_POST['website']!="")?$_POST['website']:1;
	$engine = ($_POST['engine']!="")?$_POST['engine']:1;
	
	$sortby = ($_POST['sortby']=="")?"cpa":$_POST['sortby'];
	
	$timeframe = ($_POST['year']!="")?
						$_POST['year'].$_POST['month'].$_POST['day']:
						date("Ymd",time()-24*60*60);
	
	$t = new Tracking($website,$timeframe,$sortby);


	$t->queryWithCosts($timeframe);

	function printSortLink($thisone,$current)
	{
		$current = ereg_replace("(\+| |asc|desc)","",$current);
		
		$onclick = "onclick=\"javascript:applySort('".$thisone."',";
		echo "<!--- $thisone $current --->";
		if ($thisone == $current)//currently desc
		{
			$onclick .= "'asc');\" ";
			$title = " title='click to sort ascendingly (1..9)' ";
			$char = "^";
		}
		else
		{
			$onclick .= "'desc');\" ";
			$title = " title='click to sort descendingly (9..1)' ";
			$char = "v";	
		}
		
		echo "<A style='color:#0000FF; text-decoration:none;' ".
			"href='#' $onclick  $title ><strong>$char</strong></A>";

	}
	
?>
	<script language="javascript">
		function applySort(invalue,direction)
		{
			document.forms.filterform.sortby.value = invalue + " "+ direction;
			document.forms.filterform.submit();
		}
	
	</script>

	<style>
		th
		{
			font:Verdana;
			font-size:14px;
		}	
		td
		{
			font:Verdana;
			font-size:13px;
		}	
	</style>
	<form name='filterform' action="tkcosts.php" method="post">
		<input type='hidden' name="sortby" value="<?= $sortby ?>" />
		<select name='website'>
			<option value="1">CarShipping.com</option>
		</select> - 
		<select name="engine">
			<option value="1">Google</option>
		</select> - 
		<input type="hidden" name="year" value="2007"/>
		<select name="month"><option value="05">May</option></select>
		<select name="day">
		<?
			for ($i = 1; $i < date("d"); $i++)
			{
				$d = ($i<10)?"0$i":$i;
				echo "<option value='$d'>$d</option>";
			}
		?>
		</select>
		
		<input type="submit" value="Update" />
	</form>
	<Br/>
	<? if ($timeframe == date("Ymd")) 
			echo "<strong>Note:</strong> Data for today ".
				" has a 15 minute lag to account for users ".
				" currently on the site.<Br/>"; 
	?>
	<Br/>
	<strong>Date:</strong> <?= $timeframe ?><Br/>
	<strong>Total Clicks:</strong><?= $t->totalClicks ?><br/>
	<strong>Total Conversions:</strong><?= $t->conversions ?><br/>
	<strong>Conversion Rate:</strong><?= number_format($t->conversionRate,2,".",",") ?>%<br/>
	<Br/><Br/>
	<table>
		<tr>
			<th><? printSortLink("keyword",$t->sortby); ?> Keyword </th>
			<th align="left"><? printSortLink("clicks",$t->sortby); ?> Clicks</th>			
			<th align="left"><? printSortLink("totalspend",$t->sortby); ?> Total Spend</th>
			<th align="left"><? printSortLink("avgcpc",$t->sortby); ?> AVG Cpc</th>			
			<th><? printSortLink("conversions",$t->sortby); ?> Conversions</th>			
			<th><? printSortLink("convperc",$t->sortby); ?> Conv %</th>
			<th align="left"><? printSortLink("ctr",$t->sortby); ?> Click Through Rate</th>
			
			<th align="left"><? printSortLink("avgpos",$t->sortby); ?> Avg Position</th>
			<th align="left"><? printSortLink("impressions",$t->sortby); ?> Impressions</th>
			<th align="left"><? printSortLink("cpa",$t->sortby); ?> CPA</th>
		</tr>	
	<?
	foreach($t->data as $data)
	{	
		$cl = $data['clicks'];
		$cv = ($data['conversions']==0)?"--":$data['conversions'];
		
		$cp = $data['convperc'];
		$cp = ($cp==0)?"--":number_format($cp,2,".",",")."%";
		
		$ctr = $data["ctr"];
		$ctr = ($ctr==0)?"--":number_format($ctr,2,".",",")."%";
		
		$keyword = $data['keyword'];
		$kid = $data['keywordid'];
		$keyword = "<a href='tkkeyword.php?kid=$kid&keyword=".urlencode($keyword)."' style='color:#0000FF; text-decoration:none;' >".$keyword."</a>";
		$ts = $data['totalspend'];
		$avgcpc = $data['avgcpc'];
		
		$cpa = $data['cpa'];
		$cpa = ($cpa==0)?"--":"$".number_format($data['cpa'],2,".",",");
		
		?>
			<tr>
				<td><?= $keyword ?></td>
				<td><?= $cl ?></td>
				<td><?= $ts ?></td>
				<td><?= $avgcpc ?></td>
				<td><?= $cv ?></td>
				<td><?= $cp ?></td>
				<td><?= $ctr ?></td>
				<td><?= $data['avgpos'] ?></td>
				<td><?= $data['impressions'] ?></td>
				<td><?= $cpa ?></td>
			</tr>
		<?
	}
	echo "</table>";
	include "skin_bottom.php";
?>