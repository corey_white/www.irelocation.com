<? 
	/*
		basic template header page.
	*/

	include_once "../../inc_mysql.php";
	include "marblehelper.php";
	$categories = array("security","moving","atus","marble","storage",
						"summary","aff","tas","tk","international","vanlines","ca","merch","callctr","transcribe");
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*" && $month  == "*")
			return date('t');
		else
			return date('t',mktime(0,0,0,substr($_POST['ts'],4),15,substr($_POST['ts'],0,4)));				
	}
	
	$a = split("/",$_SERVER['SCRIPT_FILENAME']);
	$a = array_pop($a);
	foreach($categories as $cat)
	{
		if (substr_count($a,$cat) > 0)
		{

			if ($cat == "marble")
				$rw = "top auto ";
			else if ($cat == "tas")
				$rw = "top auto shippers ";
			else if ($cat == "merch")
				$rw = "merchant cc ";
			else if ($cat == "transcribe")
				$rw = "transcription ";
			else if ($cat == "callctr")
				$rw = "call center ";
			else if ($cat == "tk")
				$rw = "tracking ";
			else if ($cat == "ca")
			{
				$rw = "Canada ";
			}
			else
				$rw = "$cat ";
		
			$pagetitle = ucwords(str_replace($cat,$rw,$a));
			$pagetitle = str_replace(".php","",$pagetitle);
			$pagetitle = str_replace("Viewleads","View Leads",$pagetitle);
		}
	}
	
	if ($a == "defsecuritygrid.php")
		$pagetitle = "USHomeSecuritySystems.com Grid";
	else if ($a == "defsecurityreport.php")
		$pagetitle = "USHomeSecuritySystems.com Report";
	else if ($a == "merchbreakout.php" || $a == "merchgrid.php" || $a == "viewleads_multiple_merchant.php")
		$pagetitle = "Merchant Report";
	else if ($a == "callcenterbreakout.php" || $a == "callcentergrid.php" || $a == "viewleads_multiple_callcenter.php")
		$pagetitle = "Call Center Report";
	else if ($a == "transcribebreakout.php" || $a == "transcribegrid.php" || $a == "viewleads_multiple_transcription.php")
		$pagetitle = "Transcription Report";
	
	?>
<html>
<head>
	<title>iRelocation Reporting - <?= $pagetitle ?></title>
	<link rel='stylesheet' type='text/css' href='marble.css' />
	<style>
		.filterfont
		{
			font-family:Verdana;
			font-size:12px;
			font-weight:bold;
		}
	</style>
	<script>
		var counter = 151;
		var timer = null;
		var qs = '<?= $_SERVER['QUERY_STRING']; ?>';
		function updateCounter()
		{
			timer = setTimeout("updateCounter()",1000);
			counter -= 1;
			if (counter == 0)
			{
				var sURL = unescape(window.location.pathname);
				if (qs != '') sURL += "?"+qs;
				window.location.replace( sURL );
			}
			//updateHeader();			
		}
		/*
		function updateHeader()
		{
			//var div = document.getElementById('timer');
			//var msg = "You will be automatically logged out in ";
			var minutes = parseInt(counter / 60);
			if (minutes == 0) minutes = "0";
				
			var seconds = (counter % 60);
			if (seconds == 0)
				seconds = "00";
			else if (seconds < 10)
				seconds = "0"+seconds;
			
			div.innerHTML = msg + minutes + ":"+seconds + " minutes. ";
		}
		*/
	</script>
</head>
<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF" >
<table border="0" width="100%" cellspacing=0 cellpadding=0>
	<tr bgcolor='navy'>
		<td align=left>
			<font face='verdana' size=2 color='white'><b>&nbsp;.:
			<?= $pagetitle ?></b></font>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" width="100%" cellspacing=0 cellpadding=10>
				<tr>
					<td  bgcolor='navy' width="130" valign="top" nowrap="nowrap">
						<br />
							<? 	include "sidelinks.php";	?>
						<br />
						<!-- 
<br /><br /><br /><br /><br /><br />
						<br /><br /><br /><br /><br /><br /><br />
 -->
					</td>
					<td width="20">&nbsp;</td>
					<td valign="top" width='*'>						