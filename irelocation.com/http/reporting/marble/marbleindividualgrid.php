<? 
	session_start();
	//echo "\n<!--- ".print_r($_SESSION,true)." --->\n";
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] == "autotransporter")
	{		
		$lead_id = $_SESSION['lead_id'];		
		$_SESSION['time'] = date("YmdHis");
	}
	else if ($_SESSION['user'] == "uberadmin")
	{
		if(strlen($_REQUEST['lead_id']) >= 0)
			$lead_id = $_REQUEST['lead_id'];
		else
		{
			header("Location: marblegrid.php");
			exit();	
		}
	}
	else
	{
		header("Location: login.php");
		exit();	
	}

	include "../../inc_mysql.php";
	echo "<!--- ".$lead_id." ".$_SESSION['lead_id']." --->";
	function getSortLink($current,$field_name,$label)
	{
		if ($current != $field_name)
			return "<a href='marblegrid.php?order=$field_name' title='Click to Sort Accending'>".ucwords($label)."</a>";
		else
			return "<a href='marblegrid.php' title='Click to Clear Sorting'>".ucwords($label)."</a>";
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	
	$ts = date("Ym");
	
	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join marble.companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'auto'
				and c.month = '$ts'
				AND q.received like '$ts%'									
				and c.lead_id = $lead_id
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				day;";

	echo "<!--- $sql --->";
	$rs = new mysql_recordset($sql);
?>	
<link rel='stylesheet' type='text/css' href='marble.css' />
<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<font face='verdana' size=2 color='white'><b>
				<? 
					if ($admin)
					{
						$goto = "marblereport.php";
					}
					else
					{
						$goto = "marbleindividualgrid.php";						
					}
				?>
				<a href='<?= $goto ?>' class='white'>&nbsp;.:
				Top Auto Leads</a></b></font>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing=0 cellpadding=10>
					<tr valign='top'>
						<td  bgcolor='navy' width="130" valign="top">
							<br/><br/><br/>
								<?
									$goto = "marbleformat.php";
									if (!isset($_SESSION['lead_id']))
										$goto .= "?lead_id=".$_REQUEST['lead_id'];								
								?>
								<a class='white' href="<?= $goto ?>"> - Lead Format</a>
								<Br/>
								<a class='white' href="marblelogout.php"> - Log Out</a>
							<br/><br/><br/><br/><br/><br/><br/><br/><br/>
							
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>&nbsp;</td></tr>							
							<Tr valign='top'><td><br/><br/><br/>

	
	
<?	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	$days = date('t');
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	
	
	while($rs->fetch_array())
	{
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company == "")
		{		
			echo "<tr class='$style'><td class='$style' >$font_counter</td>";
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			$link = "<a href='";

			$goto = "marbleformat.php";
			if (!isset($_SESSION['lead_id']))
				$goto .= "?lead_id=".$_REQUEST['lead_id'];								

			$link  .= $goto;
			$link  .= "' class='black'>$comp_name</a>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = 0;	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black' href='marbleviewleads.php?ts=$ts$day'>$count</a>";
		else
			$link = "<A class='black' href='marbleviewleads.php?ts=$ts$day&lead_id=".$_REQUEST['lead_id']."'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
					
		$total += $count;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black' href='marbleviewleads.php?ts=$ts'>$total</a>";
	else
		$link = "<A class='black' href='marbleviewleads.php?ts=$ts&lead_id=".$_REQUEST['lead_id']."'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr>";
	$last_company = "";
	echo "</table>";

?>
</td></tr></table></td></tr></table></td></tr></table>