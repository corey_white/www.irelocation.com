Quote ID	Name	Email	Phone	Quote Type	Date Received	Address	City	State Code	Zip	Comments	Source
66512	Nancy Dimaranan	ndimaranan@hotmail.com	4167244485	Residential	04/01/2008 23:48	19 Amiens Road	Scarborough	ON	M1E 3S5	New System House Up to 1,499 ft	ctac_google
66563	Jennifer Ogeer-gyles	jog373@sasktel.net	3063419350	Residential	04/02/2008 09:32	82 Champlin Crescent	Saskatoon	SK	S7H 4T3	New System House 1,500 - 2,999 ft	ctac_google
66604	Jose Diaz	josdiaz@deloitte.ca	4164649467	Residential	04/02/2008 13:32	136 Dooley Cres	Ajax	ON	L1T 4J6	New System House 1,500 - 2,999 ft	ctac_google
66674	Reg Brockway	dbrockway@cogeco.ca	9055227479	Residential	04/02/2008 23:48	19 Century Street	Hamilton	ON	L8L 1V7	New System Apartment Up to 1,499 ft	ctac_google
66720	Luc Robillard	lucrobillard@videotron.ca	5142954748	Residential	04/03/2008 08:16	5697 Bourbonniere	Montreal	QC	H1X 2N5	New System Condo Up to 1,499 ft	ctac_google
66742	Carol Maclean	maclean.carol@sympatico.ca	5192510762	Residential	04/03/2008 11:48	908 Dawson Rd.	Windsor Ontario	ON	N8Y 4A4	Upgrade Current System House 1,500 - 2,999 ft	ctsc_overture
66785	Chris Seguin	seg662000@yahoo.ca	7058559276	Residential	04/03/2008 17:48	81 Main.street.w. Box317	Chelmsford	ON	P0M 1L0	New System House Up to 1,499 ft	ctac_google
66798	Brenda Nolin	bnolin@sarbit.com	2049442455	Residential	04/03/2008 20:16	Box 145 Grp 374 Rr 3	Winnipeg	MB	R3C 2E7	Replace Current System House Up to 1,499 ft	ctac_google
66807	Water  Inc Mansoor Syed	water110inc@primus.ca	4166951200	Commercial	04/03/2008 21:16	290 The West Mall Unit 5	Etobicoke	ON	M9C 1C6	New System Retail Up to 2,499 ft	ctac_google
66909	Manyan Deng Deng	manyang.deng@sscocanada.org	4162407726	Residential	04/04/2008 15:48	2103 & 2105 Weston Rd	Toronto	ON	M9N 1X7	Replace Current System House 3,000 - 4,999 ft	ctac_google
66917	Chanelle Moschenbacher	chanellem@rogers.com	9052402466	Residential	04/04/2008 16:32	570 Beatrice St. E #11	Oshawa	ON	L1K 2G2	New System Town House Up to 1,499 ft	ctac_google
66960	Jean Hamm	jjgjhamm@dccnet.com	6045997662	Residential	04/04/2008 23:48	1505 Rousseau Cres	Regina	SK	S4X 4L7	New System House 1,500 - 2,999 ft	ctac_ask
67002	Basil Almukhtar	almukhtarbasil@yahoo.com	4167258673	Residential	04/05/2008 08:48	71 Coxwell	Toronto	ON	M4L 3B1	New System House Up to 1,499 ft	ctac_google
67011	Sima Weig	simaweig22@hotmail.com	6474017398	Residential	04/05/2008 09:48	153 Oakhurst Dr	Barry	ON	L4J 8H6	New System House Up to 1,499 ft	ctac_msn
67128	Joan Deshaw	jdeshaw@hotmail.com	5193544600	Residential	04/06/2008 07:48	40 Woodland Ave	Chatham	ON	N7L 2S6	New System House 1,500 - 2,999 ft	ctac_google
67139	Mark Mccormack	mmccormack@prudential.ns.ca	9028308710	Residential	04/06/2008 09:16	12 Beechwood Terr.	Halifax	NS	B3M 2C2	New System House 1,500 - 2,999 ft	ctac_google
67187	Rob Bryce	donnadbryce@aol.com	5196375220	Residential	04/06/2008 17:16	1773 Tigerlily Rd	London	ON	N6K 0A2	New System House 1,500 - 2,999 ft	ctac_google
67229	Norm Frost	nkfrost@mts.net	2049320662	Residential	04/06/2008 23:01	27 N/a	Wpg	MB	R3T 4K3	New System House 1,500 - 2,999 ft	ctac_google
67271	Moms Best Gourmet Angela Pettigrew	momsbestgourmet@bellnet.ca	9056968889	Commercial	04/07/2008 08:01	6435 Kestrel Rd.	Mississauga	ON	L5T 1Z8	Upgrade Current System Other 10,000 - 49,999 ft	ctac_google
67280	Sarah Gray	andy_sarah@rogers.com	9054250415	Residential	04/07/2008 09:16	5 Jordan Court	Brooklin	ON	L1M 0A4	New System House 3,000 - 4,999 ft	ctac_google
67292	Rob Don	rdonko@cogeco.ca	9054840482	Residential	04/07/2008 10:48	4014 Donnic Drive	Burlington	ON	L7M 0A7	New System Town House 1,500 - 2,999 ft	ctac_google
67322	Munawar Niazi	niazi5@hotmail.com	5199667187	Residential	04/07/2008 13:48	2384 Askin Ave	Windsor	ON	N9E 4Y2	New System House 1,500 - 2,999 ft	ctac_google
67332	Lou Daey	jldaeninck@gmail.com	2042554923	Residential	04/07/2008 15:01	15 Harmony Cove	Wpg	MB	R2M 4X9	New System House Up to 1,499 ft	ctac_google
67389	High Powered Ross Baylin	rbaylin1661@rogers.com	6138254778	Commercial	04/07/2008 22:01	9 Cliffbrake Grove	Ottawa	ON	K2R 1A2	New System Retail Up to 2,499 ft	ctac_google
67412	Nirmala Bahall	n_bahall@yahoo.ca	4169027864	Residential	04/08/2008 00:32	77 St Clair East	Toronto	ON	M4T 1M5	New System Town House Up to 1,499 ft	ctac_google
67453	Marlene Pearce	marlene.p@bypeterandpauls.com	9053264318	Residential	04/08/2008 09:32	24 Hawkedon Crescent	Etobicoke	ON	M9W 3L7	New System House Up to 1,499 ft	ctac_google
67486	The Drip Curtis Ventura	curt_019@hotmail.com	6138647848	Commercial	04/08/2008 13:32	12 Rideau River Lane	Ottawa	ON	K1S 0X1	New System Restaurant Up to 2,499 ft	ctac_google
67505	Les Stroud Productio Parres Allen	parres@lesstroudonline.com	7057892988	Commercial	04/08/2008 15:32	680 Muskoka Rd. 3 N	Huntsville	ON	P1H 1C9	New System Home-based Up to 2,499 ft	ctac_google
67541	Guy Laporte	guylaporte@rogers.com	6138357574	Residential	04/08/2008 20:48	5905 Buckland Rd	Vars	ON	K0A 3H0	Upgrade Current System House 1,500 - 2,999 ft	ctac_google
67549	Madhu Dixit	msd@physics.carleton.ca	6138677770	Residential	04/08/2008 21:32	45 Vanhurst Place	Ottawa	ON	K1V 9Z7	New System House 1,500 - 2,999 ft	ctac_google
67592	Brian Shelfoon	jennifer270@sympatico.ca	9059821142	Residential	04/09/2008 07:01	1632 King St	Prince Albert	ON	L9L 1C2	Upgrade Current System House 1,500 - 2,999 ft	ctac_google
67609	Gail Styles	gstyle230@acn.net	7055274651	Residential	04/09/2008 09:48	1039 Glen Bogie Cres	Midland	ON	L4R 4S6	New System House Up to 1,499 ft	ctsc_overture
67765	Leading Edge Motor Angela Jesus	angela@lemc.ca	9058563303	Commercial	04/10/2008 10:16	95 Whitmore Road	Woodbridge	ON	L4L 6E2	Replace Current System Office 5,000 - 9,999 ft	ctac_google
67769	Sandeep Chandna	s_chandna@email.com	4164710708	Residential	04/10/2008 10:48	3152 Stepping Ston Court	Mississauga	ON	L5B 4H4	Upgrade Current System House 1,500 - 2,999 ft	ctac_google
67925	Uv Signs  Graphics Carlos Anon	uv.signs@hotmail.com	4162221010	Commercial	04/11/2008 12:32	50 Weybright Court #19	Toronto	ON	M1S 5A8	Replace Current System Industrial Up to 2,499 ft	ctac_msn
67957	Jason Proc	jsun124@hotmail.com	4164440203	Residential	04/11/2008 17:32	43 Valley Woods Rd #99	Toronto	ON	M3A 2R5	Replace Current System House 1,500 - 2,999 ft	ctac_google
67962	Rita Brady	ritabrady@hotmail.com	4167022750	Residential	04/11/2008 18:16	2 Clay Brick Crt	Brampton	ON	L6V 4M7	New System House 1,500 - 2,999 ft	ctac_google
68189	Demetre Kosmas	jkosmas92@rogers.com	6472675059	Residential	04/13/2008 12:32	174 Fairglen Ave	North York	ON	M2H 2K2	New System House Up to 1,499 ft	ctac_overture
68219	Iftikhar Ahmed	iftikhar42_03@hotmail.com	5142711508	Residential	04/13/2008 17:48	850 Cremaize West #11,12,14	Montreal	QC	H3N 1A3	New System Apartment Up to 1,499 ft	ctac_overture
68313	Geoff Del Grande	geoff@accessfitness.ca	5199380970	Residential	04/14/2008 08:48	123 Broadway Unit C Orangeville	Orangeville	ON	L9W 1K2	New System Condo Up to 1,499 ft	ctac_google
68344	Beth Weinberger	beth.weinberger@gmail.com	6138680830	Residential	04/14/2008 12:32	1 Roselawn Crt.	Nepean	ON	K2G 4J3	New System Town House 1,500 - 2,999 ft	ctac_google
68347	Philip Palmer	bighead_221@hotmail.com	6137590110	Residential	04/14/2008 13:16	44 Rideaucrest Drive	Ottawa	ON	K2G 6A3	New System House 1,500 - 2,999 ft	ctac_overture
68356	Storystream Don Tjart	don@storystream.com	6478998793	Commercial	04/14/2008 14:01	2936 Dundas Street West	Toronto	ON	M6P 1Y8	New System Office Up to 2,499 ft	ctac_google
68363	Angie Gauthier	ms_angie_lynn@hotmail.com	6137619202	Residential	04/14/2008 14:48	1077 Merivale Rd. Apt 12	Ottawa	ON	K1Z 6A9	New System Apartment Up to 1,499 ft	ctac_msn
68380	Kurt Mahon	kurtmahon@yahoo.ca	5193451515	Residential	04/14/2008 17:01	R.r# 1	Dublin	ON	N0K 1E0	New System House 3,000 - 4,999 ft	ctac_google
68389	Ed Crosby	pastorev@cogeco.ca	5197966146	Residential	04/14/2008 17:16	858 Moy Ave.	Windsor	ON	N9A 2N8	New System House 1,500 - 2,999 ft	ctac_google
68457	Niki Zigomanis	nikizigomanis@msn.com	9054791783	Residential	04/15/2008 05:48	88 Reidmount Avenue	Agincourt	ON	M1S 1B7	New System House Up to 1,499 ft	ctac_msn
68479	Dana Lust	lust@cogeco.ca	9058734170	Residential	04/15/2008 09:01	31 Mcnally St	Georgetown	ON	L7G 6E2	New System House 1,500 - 2,999 ft	ctac_google
68509	Scott Cummings	scott.cummings@aircanada.ca	9056764478	Residential	04/15/2008 13:48	4614 Beech Grove Rd	Caledon	ON	L7K 0M5	New System Town House 1,500 - 2,999 ft	ctac_google
68533	Peter Dragovic	pdragovic@yahoo.com	5199999999	Residential	04/15/2008 17:01	749 9th Concession	Lakeshore	ON	N0R 1K0	New System House 3,000 - 4,999 ft	ctac_overture
68577	Colleen Sault	eaglewind2@hotmail.com	9057687333	Residential	04/15/2008 22:32	3410 Mississauga Road	Hagersville	ON	N0A 1H0	New System House 1,500 - 2,999 ft	ctac_overture
68655	Mike Tran	mtran@hdgh.org	5198192805	Residential	04/16/2008 11:32	1790 Randolph	Windsor	ON	N9B 2W1	New System House 3,000 - 4,999 ft	ctac_google
68664	Angela Boin	lboin@pppoe.ca	9057277338	Residential	04/16/2008 12:48	8 Wainwright Avenue	Richmond Hill	ON	L4C 5R5	Replace Current System House 3,000 - 4,999 ft	ctac_overture
68665	Donghyon Lim	luckylim01@yahoo.com	4164317861	Residential	04/16/2008 13:01	38 Leecentre Suit 1907,leecenter Dr Scarborough	Toronto	ON	M1H 3J7	New System House 3,000 - 4,999 ft	ctac_google
68670	Sarah Thomas	sarah@watchnetdvr.com	4168320199	Residential	04/16/2008 13:32	18 Madas Ave	Richmondhill	ON	L4E 5E4	New System House 3,000 - 4,999 ft	ctac_google
68727	Anil Bhardwaj	bhardwaj1@ntlworld.com	4164270288	Residential	04/16/2008 22:32	341 Kirkvalley Cresent	Aurora	ON	L4G 7S1	New System House 1,500 - 2,999 ft	ctac_google
68728	Maria Nagel	maris.makris@rbe.sk.ca	3065852663	Residential	04/16/2008 22:48	11 Heritage Pl	Regina	SK	S4S 2Z7	New System House 1,500 - 2,999 ft	ctac_google
68773	Aaron Mccoll	aaronmccoll@gmail.com	9053790044	Residential	04/17/2008 07:48	178 Grosvenor Ave N	Hamilton	ON	L8L 7S8	New System House Up to 1,499 ft	ctac_google
68778	Kelly Araujo	kellyjaraujo@hotmail.com	2042922722	Residential	04/17/2008 08:48	39 Al Thompson Drive	Winnipeg	MB	R3W 0A3	New System House Up to 1,499 ft	ctac_msn
68815	The Millwork House Doug Campbell	doug@millwork-house.com	9055649571	Commercial	04/17/2008 13:32	6290 Danville Rd	Mississauga	ON	L5T 2H7	Replace Current System Industrial 10,000 - 49,999 ft	ctac_google
68834	Robert Boisclair	bobguitar@amtelecom.net	5194265947	Residential	04/17/2008 15:16	12-9 Norfolk St. South	Simcoe	ON	N3Y 2V8	New System Apartment Up to 1,499 ft	ctac_google
68835	Jan Stevenson	bertnjan@sympatico.ca	6136233415	Residential	04/17/2008 15:32	5803 Loggers Way	Ottawa - Arnprior	ON	K7S 3G7	New System House Up to 1,499 ft	ctac_google
68845	Michael Sideris	msideris@cogeco.ca	5192538126	Residential	04/17/2008 16:48	1167 Dougall	Windsor	ON	N9A 4S1	New System House 1,500 - 2,999 ft	ctac_google
68918	Ron Or Yvette Desjardins	rcartage@magma.ca	6137312459	Residential	04/18/2008 06:01	3830 Russell Rd.	Ottawa	ON	K1G 3N2	New System House 1,500 - 2,999 ft	ctac_google
68931	 Ontario Ltd Dave Gethings	davecheryl@rogers.com	7055278402	Commercial	04/18/2008 07:32	243 Ruby St	Midland	ON	L4R 2L3	Upgrade Current System Home-based Up to 2,499 ft	ctac_google
69296	David Greenwood	david.greenwood@sympatico.ca	9056288107	Residential	04/20/2008 18:32	11 Pirie Drive	Dundas	ON	L9H 6Z6	New System House 1,500 - 2,999 ft	ctac_google
69375	Brent Lang	jibre@rogers.com	5198290391	Residential	04/21/2008 07:32	60 Grey Oak	Guelph	ON	N1L 1R2	New System House 1,500 - 2,999 ft	ctac_google
69395	Missy Chamberlain	honeymis@cogeco.ca	6137719815	Residential	04/21/2008 10:16	3122 Shannonville Road	Plainfield	ON	K0K 2V0	New System House Up to 1,499 ft	ctac_google
69405	Paul Lagahit	pslagahit_04@rogers.com	9055569722	Residential	04/21/2008 12:01	56 Palomino Place	Whitby	ON	L1R 2V5	Upgrade Current System House 1,500 - 2,999 ft	ctac_google
69437	Almost Naked Inc Dan Sutcliffe	wunderdan@aol.com	4167923488	Commercial	04/21/2008 16:16	471 Queens St. West	Toronto	ON	M5V 2B1	New System Retail Up to 2,499 ft	ctac_google
69487	Percival Francisco	pfrancisco@smarttech.com	6138368423	Residential	04/21/2008 21:48	37 Ballymore Avenue	Ottawa	ON	K1T 3Z7	Upgrade Current System House 1,500 - 2,999 ft	ctac_google
69560	David Nolan	dnolan@wolfsonbell.com	4169100859	Residential	04/22/2008 10:48	22f Mistwell Cr	Oakville	ON	L6L 0A2	New System House 3,000 - 4,999 ft	ctac_google
69570	Lucy Faria	faria02@hotmail.com	4163192012	Residential	04/22/2008 11:48	11 Harper Rd	Brampton	ON	L6W 2W3	New System House Up to 1,499 ft	ctac_google
69590	Ida Daddese	idadc@yahoo.com	9058328964	Residential	04/22/2008 14:16	14 Welton St.	Maple	ON	L6A 3Y3	Upgrade Current System House 3,000 - 4,999 ft	ctac_google
69602	Tissa De Silva	tdesilva@ibigroup.com	4167215416	Residential	04/22/2008 15:32	8 Rosedale Heights Drive	Thornhill	ON	L4J 8A2	New System House 3,000 - 4,999 ft	ctac_google
69636	Aj Varghese	avarghesej@gmail.com	9055671157	Residential	04/22/2008 19:16	1845 Willow Way	Mississauga	ON	L5M 4Y5	New System House 1,500 - 2,999 ft	ctac_google
69638	Karl Rodwell	karbon3@sympatico.ca	5193484053	Residential	04/22/2008 19:48	94 Napier St.	Mitchell	ON	N0K 1N0	New System House 3,000 - 4,999 ft	ctac_google
69752	Alain Canizares	alain.canizares@gmail.com	4163654006	Residential	04/23/2008 13:01	8 Abraham Crt	Ajax	ON	L1Z 0A9	New System House 1,500 - 2,999 ft	ctac
69948	Aaa Aaa	azar.hojabr@gmail.com	4164197585	Residential	04/24/2008 15:16	484 Worthington Ave.	Richmondhill	ON	L4E 0E3	New System Town House 1,500 - 2,999 ft	ctac_google
69982	Cuong Huynh	huynh75@sympatico.ca	9057129452	Residential	04/24/2008 19:48	5401 Marblewood Dr.	Mississauga	ON	L5V 2L1	New System House Up to 1,499 ft	ctac_google
70217	Steve Tsao	steventsao@hotmail.com	4168167280	Residential	04/26/2008 08:32	80 Lytton Blvd	Richmond Hill	ON	L4B 3H4	New System House 1,500 - 2,999 ft	ctac_google
70314	Carrie Kish	skinneythepooh@porchlight.ca	5194572830	Residential	04/26/2008 22:01	1447 Huron	London	ON	N5V 2E6	New System House 1,500 - 2,999 ft	ctac_google
70399	Maribeth Knezev	maribeth.k@netzero.net	4164657296	Residential	04/27/2008 13:48	266 Riverdale Ave	Toronto	ON	M4K 1C6	New System House 1,500 - 2,999 ft	ctac_google
70437	Xiaoling Huang	kevxiao@hotmail.com	6472826109	Residential	04/27/2008 19:16	109 Longwater Chase	Markham	ON	L3R 4A9	New System Town House Up to 1,499 ft	ctac_google
70442	Deborah Simonds	debbie.simonds@persona.ca	7056944620	Residential	04/27/2008 20:01	56 Second Ave Box 339	Coniston	ON	P0M 1M0	New System House 1,500 - 2,999 ft	ctac_google
70535	Aslan Inc Carrie Gray	carrie@aslanleather.com	4163060462	Commercial	04/28/2008 09:32	Po Box 102 Stn B	Toronto	ON	M5T 2T3	New System Other Up to 2,499 ft	ctac_google
70542	Kevin Allan	kevincrallan@gmail.com	9055994144	Residential	04/28/2008 10:01	136 Springhurst Ave, # 8	Toronto	ON	M6K 1C1	New System Condo Up to 1,499 ft	ctac_google
70557	Jensen Tire Andrew Hill	jensentireah@bellnet.ca	9053389919	Commercial	04/28/2008 11:01	928 Winston Churchill Blvd	Oakville	ON	L6J 7X5	Replace Current System Warehouse 10,000 - 49,999 ft	ctac_google
70578	James Page	james.page@cibc.com	4169805144	Residential	04/28/2008 12:32	2922 Termini Terrace	Mississauga	ON	L5M 5S4	New System House Up to 1,499 ft	ctac_google
70581	Tracy Thomson	tracythomson@rogers.com	4168694544	Residential	04/28/2008 12:48	2457 Mainroyal St	Mississauga	ON	L5L 1E1	New System House Up to 1,499 ft	ctac_google
70601	Dana Liang	dliang@rogers.com	6472813262	Residential	04/28/2008 14:32	14 Balding Ct.	Toronto	ON	M2P 1Y7	Upgrade Current System House 3,000 - 4,999 ft	ctac_google
70603	Armario Monica Swaneck	swaneck@rogers.com	9057616004	Commercial	04/28/2008 14:32	255 Bass Pro Mills Drive	Vaughn	ON	L4K 0A2	New System Retail Up to 2,499 ft	ctac_google
70620	James Lane	jamesfromgta@gmail.com	4166770565	Residential	04/28/2008 15:32	14 Talwood Dr.	Toronto	ON	M3B 2P3	New System House Up to 1,499 ft	ctac_google
70680	Raj Paramarajah	arrul89@yahoo.ca	5196572473	Residential	04/28/2008 22:16	800 Wonderland Rd S	London	ON	N6K 4L8	New System House 1,500 - 2,999 ft	ctac_google
70799	Domistyle Inc Vanessa Lee	vanessa@domistylegifts.com	4168068409	Commercial	04/29/2008 14:48	688 Richmond St. W	Toronto	ON	M6J 1C5	New System Warehouse Up to 2,499 ft	ctac_google
70801	Jacques Berard	jberardn408@rogers.com	6139563310	Residential	04/29/2008 15:01	127 Dalhousie Street	Ottawa	ON	K1N 7C2	New System Town House Up to 1,499 ft	ctac_google
70824	Kyle Pearce	kylepearce@sympatico.ca	5198180078	Residential	04/29/2008 17:16	843 Southwood Drive	Belle River	ON	N0R 1A0	New System House 1,500 - 2,999 ft	ctac_google
70990	Maribeth Knezev	maribeth.k@netzero.net	4169999999	Residential	04/30/2008 17:01	266  Riverdale  Ave	Toronto	ON	M4K 1C6	New System House Up to 1,499 ft	ctac_google
70997	Sherri Lange	kodaisl@rogers.com	4165675115	Residential	04/30/2008 17:48	22 Avonmore Square	Toronto	ON	M1E 1C9	New System House 1,500 - 2,999 ft	ctac_google
