<?
/* 
************************FILE INFORMATION**********************
* File Name:  securitybreakout.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/12/08 - Rob - corrected the name for Chubb
	
	8/27/08 - Rob - Added UK stuff
**********************************************************************
*/


	session_start();
	
	define(CPC_DEBUG,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	
	$pricesPerLead = array();
	
	
	
	if (!defined(CAMPAIGN))
	{
		if ($_REQUEST['test'] == "")
		{
			define(CAMPAIGN,"us");
			define(SITE_ID,'tsc');
			define(APX,1569);
			define(BRINKS,1519);//0
			define(PINNACLE,1523);//6
			define(PROTECTION_ONE,1521);//3
			define(PROTECT_AMERICA,1520);	//2
			define(USALARM,1522);//5
			define(GAYLORD,1552);
			define(MONITRONICS,1565);
			define(SONITROL,1635);
			define(INGRID,1656);
			define(FRONTPOINT,1690);
		}
		else
		{
			define(CAMPAIGN,"ca");
			define(SITE_ID,'cas');
			define(GAYLORD,1552);
			define(VOXCOM,1555);
			define(SAFETECH,1556);
			define(APEXDIRECT,1557);
			define(BRINKS,1519);
		}
	}
	
	
	if (CAMPAIGN == "us")
		$this_page = "securitybreakout.php";
	else if (CAMPAIGN == "ca")
		$this_page = "casecuritybreakout.php";
	else if (CAMPAIGN == "aas")
		$this_page = "ausecuritybreakout.php";
	else if (CAMPAIGN == "uk")
		$this_page = "uksecuritybreakout.php";
	
	include "../../inc_mysql.php";
	
	function getLeadCounts($lead_id, $ts)
	{
		if (CAMPAIGN == "us")
		{
			
			if ($lead_id == MONITRONICS)
			{
				$sql = "select quote_type, count(distinct(s.quote_id)) 'count' ".
						" from irelocation.leads_security as s where".
						" s.received like '$ts%' and lead_ids like '%".MONITRONICS."%'".
						" and s.campaign = '".CAMPAIGN."' ";
							
				if (date("Ym") == $ts)//if now is during this month.
					$sql .= " and s.received not like '$year$month".date("d")."%'";
				$sql .= " group by quote_type ";
				$results = array();
				
				echo "\n\n<!-- MONITRONICS LEAD COUNT = $sql -->\n\n";
			
				$rs = new mysql_recordset($sql);		
				
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]]  = $rs->myarray["count"];
		
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]]  = $rs->myarray["count"];
				$rs->close();
			
			}
			if ($lead_id == GAYLORD)//add in leads that went to eversafe.
			{
				
				$results = array();
				$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
						"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
						" and lead_ids like '%".$lead_id."%' ";
				
				if (date("Ym") == $ts)//if now is during this month.
						$sql .= " and s.received not like '".date("Ymd")."%'";
				$sql .= " group by quote_type ";	
				
				echo "\n\n<!-- GAYLORD LEAD COUNT = $sql -->\n\n";
				
				$rs = new mysql_recordset($sql);	
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
				
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
				$rs->close();				
				/*
				echo "<!-- b4 Gaylord add in ".print_r($results,true)." --->";
				$sql = "select quote_type, count(*) 'count' from gaylord_security_leads as v ".
						" join irelocation.leads_security as s on s.quote_id = v.quote_id ".
						" where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ";
				if (date("Ym") == $ts)//if now is during this month.
					$sql .= " and s.received not like '".date("Ymd")."%'";
				$sql .= " group by quote_type ";					
				echo "<!-- Gaylord add in: ".$sql." --->";	
				$rs = new mysql_recordset($sql);	
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] += $rs->myarray["count"];
				
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] += $rs->myarray["count"];
				$rs->close();
				*/
				echo "<!-- after Gaylord add in  ".print_r($results,true)." --->";
			}
			else if ($lead_id == PROTECTION_ONE)			
			{
				$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
					"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
					" and lead_ids like '%".$lead_id."%' ";
			
				if (date("Ym") == $ts)//if now is during this month.
						$sql .= " and s.received not like '".date("Ymd")."%'";
				$sql .= " group by quote_type ";	
				
				echo "\n\n<!-- PROTECTION_ONE LEAD COUNT = $sql -->\n\n";
				
				$results = array();
				
				$rs = new mysql_recordset($sql);		
				
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]]  = $rs->myarray["count"];
		
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]]  = $rs->myarray["count"];
				$rs->close();
			}
			else
			{						
				$results = array();
				$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
						"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
						" and lead_ids like '%".$lead_id."%' ";
				
				if (date("Ym") == $ts)//if now is during this month.
						$sql .= " and s.received not like '".date("Ymd")."%'";
				$sql .= " group by quote_type ";	
				
				echo "\n\n<!-- LEAD COUNT FOR $lead_id = $sql -->\n\n";
				
				$rs = new mysql_recordset($sql);	
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
				
				$rs->fetch_array();
				$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
				$rs->close();
			}
		} 
		else if (CAMPAIGN == "ca")
		{						
			$results = array();
			$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
					"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
					" and lead_ids like '%".$lead_id."%' ";
			
			if (date("Ym") == $ts)//if now is during this month.
					$sql .= " and s.received not like '".date("Ymd")."%'";
			$sql .= " group by quote_type ";	
			echo "<!--- canada: $sql --->\n";
			$rs = new mysql_recordset($sql);	
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			$rs->close();
		}
		else if (CAMPAIGN == "aas")
		{
			$results = array();
			$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
					"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
					" and lead_ids like '%".$lead_id."%' ";
			
			if (date("Ym") == $ts)//if now is during this month.
					$sql .= " and s.received not like '".date("Ymd")."%'";
			$sql .= " group by quote_type ";	
			echo "<!--- austrailia: $sql --->\n";
			$rs = new mysql_recordset($sql);	
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			$rs->close();			
		}
		else if (CAMPAIGN == "uk")
		{
			$results = array();
			$sql = "select quote_type, count(*) 'count' from irelocation.leads_security ".
					"as s where s.received like '$ts%' and s.campaign = '".CAMPAIGN."' ".
					" and lead_ids like '%".$lead_id."%' ";
			
			if (date("Ym") == $ts)//if now is during this month.
					$sql .= " and s.received not like '".date("Ymd")."%'";
			$sql .= " group by quote_type ";	
			echo "<!--- getLeadCounts UK: $sql --->\n";
			$rs = new mysql_recordset($sql);	
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			
			$rs->fetch_array();
			$results[$rs->myarray["quote_type"]] = $rs->myarray["count"];
			$rs->close();			
		}
		
		return $results;		
	}
	function getCompanyName($lead_id)
	{
		switch($lead_id)
		{
			case APX: return "APX Home Security";
			case BRINKS: return "Brink's";
			case PROTECT_AMERICA: return "Protect America";
			case PROTECTION_ONE:return "Protection One";
			case USALARM: return "US Alarm";
			case SITEBASE: return "TopSecurityCompanies.com";
			case PINNACLE: return "Pinnacle Security";
			case GAYLORD: return "Gaylord Security";
			case APEXDIRECT: return "Apex Direct";
			case SAFETECH: return "Safe-Tech";
			case VOXCOM: return "Voxcom";			
			case BELLHOME: return "Bell Home Monitoring";	
			case MONITRONICS: return "Monitronics";
			case TYCO: return "Tyco Security";
			case SAFEHOME: return "Safe Home Security";
			case SIGNATURE: return "Signature Security Group";
			case HARVEYNORMAN: return "Harvey Norman Security";
			case CHUBB: return "Chubb Home Security ";
			case SEARSHOME: return "Home Protection Plus - Sears Home Alarm ";
			case FRONTPOINT: return "FrontPoint Security Solutions ";

			case SONITROL: return "Sonitrol ";
			case INGRID: return "Ingrid ";
			case ASM: return "ASM ";

			case SECURITEX: return "Securitex ";
			case PROTECT24: return "Protect 24 Security ";
			case SPECSECSYS: return "Specialised Security Systems ";
			case NATIONALSECURITY: return "National Security ";

			case ASA: return "ASA Security Alarms ";
			case SAFEGUARD: return "Safe-Guard ";
			case TJMSECURITY: return "TJM Security ";

			case ADTUK: return "ADT UK ";
			case ABELALARM: return "Abel Alarm Company ";
			case DEFENCE: return "Defence Home Security ";
		}	
	}
	
	function getPricePerLead($lead_id, $quote_type)
	{
		global $pricesPerLead;	
		
		if ($pricesPerLead[$lead_id][$quote_type] != "")
			return  $pricesPerLead[$lead_id][$quote_type];
		else
		{
			switch(CAMPAIGN)
			{
				case "us": $site_id = "tsc"; break;
				case "ca": $site_id = "cas"; break;
				case "aas": $site_id = "aas"; break;
				case "uk": $site_id = "utac"; break;
			}	
		
			$sql = "select * from marble.cost_per_lead where site_id = '".$site_id."' ";
			$rs = new mysql_recordset($sql);
			while($rs->fetch_array())
				$pricesPerLead[$rs->myarray['lead_id']][$rs->myarray['extra']] = $rs->myarray['cost'];
			return $pricesPerLead[$lead_id][$quote_type];
		}
	}

	function getCount($timeframe,$quote_type="all",$engine="all")
	{
		$sql = "select count(*) count from irelocation.leads_security q where received like '$timeframe%' and campaign = '".CAMPAIGN."' ";
		
		if ($quote_type != "all" && $quote_type != "")
			$sql .= " and quote_type = '$quote_type' ";											
		
		if ($site != "all")
			$sql .= " and q.source like '$site%' ";
		
		if ($engine != "all")
			$sql .= " and q.source like '%$engine%' ";
		
		if(CPC_DEBUG)
			echo "\n<!--- count $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	}

	function getCompanyArray($ts,$campaign)
	{
		$sql = "select lead_id from movingdirectory.campaign where ".
				" month='$ts' ";
		if ($campaign == "us")
			$sql .= " and site_id = 'tsc' ";
		else if ($campaign == "ca")
			$sql .= " and site_id = 'cas' ";
		else if ($campaign == "aas")
			$sql .= " and site_id =  'aas' ";
		else if ($campaign == "uk")
			$sql .= " and site_id =  'utac' ";
					
		$rs = new mysql_recordset($sql);				

		while($rs->fetch_array())
			$companies[] = $rs->myarray['lead_id'];
		return $companies;
	}

	function getTotalCost($ts)
	{
		if (CAMPAIGN == "us")
		{
			$sites = "'tsc','tsc-local','tac'";
			$aff = "security";
		}
		else if (CAMPAIGN == "ca")
		{
			$sites = "'ctsc','ctac'";
			$aff = "ca.security";
		}
		else if (CAMPAIGN == "aas")
		{
			$sites = "'atac'";
			$aff = "aas.security";
		}
		else if (CAMPAIGN == "uk")
		{
			$sites = "'utac'";
			$aff = "uk.security";
		}
		$sql = "select sum(cost) cost from movingdirectory.costperclick where site in ($sites) and ts like '$ts%'";
		echo "\n\n<!-- getTotalCost: $sql -->\n\n";
		$rs = new mysql_recordset($sql);				
		$rs->fetch_array();
		$cost = $rs->myarray['cost'];
		
		$cost += getAffiliateCost($aff,$ts);
		
		return $cost;
	}


	if (strlen($_REQUEST['ts']) != 6)
		$ts = date("Ym");
	else
		$ts = $_REQUEST['ts'];

	$array = getCompanyArray($ts,CAMPAIGN);
	
	include "skin_top.php";
	
	if (date("d") == 1 && $ts == date("Ym"))
	{
		echo "No data yet. :( ";
		exit();
	}
	?>	
	
	<table width='500'> 
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3"><font size="+1"><u><strong>TopSecurity Companies Leads</strong></u></font></td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<? if (CAMPAIGN == "ca") { ?>
		<tr>
			<td colspan="3">
				Note: Unlike the US Campaign, Only Brink's 'validates' the leads<br/> 
				we send them, everyone else only uses email.			
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>	 
		<? } else if (CAMPAIGN == "us") { ?>
		<tr>
			<td colspan="3">
				Note: ProtectionOne currently uses email, so no verification is available			
			</td>
		</tr>		
		<tr><td colspan="3">&nbsp;</td></tr>	 
		<? } ?>		
		<tr>
			<td colspan="3">
			<?
				$months = array(date("Ym",strtotime(" -2 Month")),date("Ym",strtotime(" -1 Month")),date("Ym",time()));
			
			?>				
				<form name="months" action="<?= $this_page ?>" method="get">
				Month: 
				<select name="ts">
				<? foreach($months as $m) { 
					echo "<option value='".$m."' ".(($m==$ts)?" selected ":"")." >$m</option>";
				} ?>
				</select>
				 &nbsp; &nbsp; <input type="submit" value="Update Month" /></form>
			</td>
		</tr>
		<? 
		
	//$graph = new SecurityGraph($ts);
	
	$grand_total_moneys = 0;
	
	foreach($array as $lead_id)
	{
		$leadcounts = getLeadCounts($lead_id,$ts);
		
		?> <tr><td colspan='3'><u><strong><?= getCompanyName($lead_id) ?></strong></u></td></tr> <?
		
		if ($lead_id == BELLHOME && $ts == "200709")
		{
			$leadcounts[RESIDENTIAL] -= 108;
			$leadcounts[COMMERCIAL] -= 13;
		}
		
		$total = 0;
		$ppl = getPricePerLead($lead_id,RESIDENTIAL);
		$count = $leadcounts[RESIDENTIAL];
		$total += $ppl*$count;
		$display_total = number_format(($ppl*$count),2,".",",");
		 ?> <tr><td>Res. - <?= $count ?> leads x $<?= number_format($ppl,2,".",",") ?></td><td width='50'>&nbsp;</td><td align="left"> = $<?= $display_total ?> </td></tr> <?
		
		$ppl = getPricePerLead($lead_id,COMMERCIAL);
		$count = $leadcounts[COMMERCIAL];
		if ($count > 0)
		{
			$total += $ppl*$count;
			$display_total = number_format(($ppl*$count),2,".",",");
			 ?> <tr><td>Comm. - <?= $count ?> leads x $<?= $ppl ?>.00</td><td>&nbsp;</td><td align="left"> = $<?= $display_total ?> </td></tr> <?
		}
		$grand_total_moneys += $total;
		echo "<tr><td colspan='2'>&nbsp;</td><td> <strong>Total: \$".number_format(($total),2,".",",")."</strong> </td></tr>\n";
	}	
	
	$total_cost = getTotalCost($ts);
	$profit = $grand_total_moneys - $total_cost;
	
	$total_leads = getCount($ts,"all","all");
	$cpl = number_format(($total_cost / $total_leads),2,".",",");
	
	?>
<tr><td colspan="2">&nbsp;</td><td align="center"><hr width="70%"></td></tr>
<tr><td colspan='2'>&nbsp;</td></tr>
<tr>
	<td>&nbsp;</td>
	<td align="right" colspan="2">
		 <table>
		 	<tr>
				<td align="right"><strong>Grand Total:</strong></td>
				<td align="right"><strong>$<?= number_format(($grand_total_moneys),2,".",",") ?></strong> </td>
			</tr>
			<tr>
				<td align="right"><strong>Cost ($<?= $cpl." x ".$total_leads." leads" ?>) : </strong></td>
				<td align="right"><strong>$<?= number_format(($total_cost),2,".",",") ?></strong> </td>
			</tr>
			<tr>
				<td align="right"><strong>Gross Profit:</strong></td>
				<td align="right"><strong>$<?= number_format(($profit),2,".",",") ?></strong> </td>
			</tr>
			<tr>
				<td align="right" colspan="2">(the Cost is directly pulled from the DB; the Cost Per Lead is a truncated number; Cost per Lead x Total Leads may not be exactly the same as the Total Cost due to truncating the Cost Per Lead)</td>
			</tr>
		 </table>
		 
	</td>
</tr>
</table>
<? include "skin_bottom.php"; ?>
