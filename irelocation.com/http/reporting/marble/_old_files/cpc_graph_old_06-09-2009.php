<?
	include_once "../../inc_mysql.php";
	
	define(CPC_DEBUG,true);
	
	/*--------------------------------------------------------------
	
				Top Security Campaign Functions.
	
	--------------------------------------------------------------*/

	function getSecurityCostByDay($timeframe,$site="all",$engine="all")
	{
		if ($site != "aff")
		{
			$sql = "select sum(cost) cost , substring(ts,7) 'day' from movingdirectory.costperclick where ts like '$timeframe%' ";
			
			if ($site != "all")
				$sql .= " and site like '$site%' ";
			else
			{
				$sql .= " and site in ('tsc','tsc-local','tac') ";
			}
			if ($engine != "all")
				$sql .= " and search_engine like '$engine%' ";
			
			$sql .= " group by substring(ts,7)";
			
			if(CPC_DEBUG)
				echo "\n<!---Security Count By Day: $sql --->\n";
			
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
			}
		}
		
		if ($site == "all" && $engine == "all")
		{
			$wcdata = getSecurityCountByDay($timeframe,"tsc","wc");
			foreach($wcdata as $day => $value)
			{
				$data[$day]["cost"] += $wcdata[$day]["count"] * 20;
			}
		}			
		
		return $data;
	}
	
	
	function getSecurityCountByDay($timeframe,$site="all",$engine="all")
	{
		$sql = "select count(*) count, substring(received,7,2) 'day' from irelocation.leads_security q ".
				"where received like '$timeframe%' ";
		$this_month = date("Ym");
		if ($timeframe == $this_month)
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		if ($site != "all")
		{
			$sql .= " and q.source like '".$site;
			if ($engine != "all")
				$sql .= "_$engine%' ";
			else
				$sql .= "%' ";
		}
		else 
		{
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
		}
		
		$sql .=  " group by substring(received,7,2) ";
		
		if(CPC_DEBUG)
			echo "\n<!--- Security Count By Day: $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];		
		return $data;
	}

	function showSecurityGraph($timeframe,$engine="all",$site="all")
	{
		$costs = getSecurityCostByDay($timeframe,$site,$engine);
		$counts = getSecurityCountByDay($timeframe,$site,$engine);
		
		$max_cost = 0;
		foreach($costs as $day => $values)
		{
			if ($counts[$day]["count"] > 0)			
				$max_cost = max($max_cost, ($values["cost"]/$counts[$day]["count"]));			
		}
		$max_cost = ceil($max_cost);
		
		echo "<!--- MAX: $max_cost --->";
		?>
<table border="1" cellpadding="0" cellspacing="0"><Tr><td>
<table cellpadding="0" cellspacing="0" border="1">
	<TR height="20"><td width='25'  valign="bottom">$<?= ($max_cost) ?></td><td rowspan="<?= intval($max_cost+2) ?>" valign="bottom">
	<?
		

		echo "<Table cellspacing='0' cellpadding='0' border='0'><tr valign='bottom'>";
		foreach($costs as $day => $values)
		{
			
			$counts[$day]["cost"] = $values["cost"];
			if($counts[$day]["count"]==0) $count = 1; else $count = $counts[$day]["count"];
			if ($counts[$day]["count"] > 0)			
				$counts[$day]["cpl"] = number_format(($values["cost"]/$count),1,".",",");
			else
				$counts[$day]["cpl"] = number_format($values["cost"],1,".",",");
			//echo "<!--- ".$values["cost"]." / ".$count." = ".number_format(($values["cost"]/$count),1,".",",")." --->";
			echo "<td align='center'>";
			echo "<img src='bar.php?width=25&height=".($counts[$day]["cpl"]*11)."&color=0,255,0' ";
			echo " title='\$".$counts[$day]["cpl"]."0' />";
			echo "<br/>".intval($day)."</td>";
		}
		echo "</tr></table>";
	?>
	</td></TR>
	<?
		echo "<!--- $max_cost --->";
		for($x = ($max_cost-1); $x > 0; $x -= 2)
		{		
			echo "<TR height='20'><td valign='bottom'>\$".$x."</td></tr>";
		}
	?>
	<TR height="20"><td valign="bottom">&nbsp;</td></tr>
</table>
</td></Tr></table>
<? } 
	/*--------------------------------------------------------------
	
				Top Moving Campaign Functions.
	
	--------------------------------------------------------------*/
	
	function getMovingCostByDay($timeframe,$site="all",$engine="all")
	{
		if ($site != "aff")
		{
			$sql = "select sum(cost) cost , substring(ts,7) 'day' from movingdirectory.costperclick where ts like '$timeframe%' ";
			
			if ($site == "all")
				$sql .= " and site in('tm','me','pm','tm-local') ";
			else $sql .= " and site = '$site' ";
				
			if ($engine != "all")
				$sql .= " and search_engine like '$engine%' ";
			
			$sql .= " group by substring(ts,7)";
			
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";
			
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
			}
			
			if ($site == "all" && $engine == "all")
			{
				$datad = getMovingCountByDay($timeframe,"tm","wc");
				foreach($datad as $day => $value)
				{
					$data[$day]["cost"] += $datad[$day]["count"] * 8;
				}
				$datad = getMovingCountByDay($timeframe,"pm","wc");
				foreach($datad as $day => $value)
				{
					$data[$day]["cost"] += $datad[$day]["count"] * 8;
				}
				
				$datad = getMovingCountByDay($timeframe,"tm","eleads");
				foreach($datad as $day => $value)
				{
					$data[$day]["cost"] += $datad[$day]["count"] * 8;
				}
			}
			
		}
		else
		{
			$data = getCountByDay($timeframe,"aff",$engine);
			foreach($data as $day => $value)
			{
				$data[$day]["cost"] = $data[$day]["count"] * 4;
			}
		}			
		return $data;
	}
	
	
	function getMovingCountByDay($timeframe,$site="all",$engine="all")
	{
		$sql = "select count(*) count, substring(received,7,2) 'day' from movingdirectory.quotes q ".
				"where received like '$timeframe%' ";
		$this_month = date("Ym");
		if ($timeframe == $this_month)
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		if ($site != "all")
		{
			$sql .= " and q.source like '".$site;
			if ($engine != "all")
				$sql .= "_$engine%' ";
			else
				$sql .= "%' ";
		}
		else 
		{
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' and source not like '%search%' and cat_id in (2,3) ";				
			else
				$sql .= " and q.cat_id in (2,3) ";
		}		
		$sql .=  " group by substring(received,7,2) ";
		
		$local_sql = str_replace(".quotes ",".quotes_local ",$sql);
		$local_sql = str_replace(array(" and cat_id in (2,3)"," and q.cat_id in (2,3) ")," ",$local_sql);
				
		if(CPC_DEBUG)
		{
			echo "\n<!--- $sql --->\n";	
			echo "\n<!--- $local_sql --->\n";	
		}
			
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];	
				
		
		$rs2 = new mysql_recordset($local_sql);
		while($rs2->fetch_array())
			$data[$rs2->myarray['day']]['count'] += $rs2->myarray['count'];	
		
		return $data;
	}
	
	function showMovingGraph($timeframe,$site="all",$engine="all")
	{
		$costs = getMovingCostByDay($timeframe,$site,$engine);
		$counts = getMovingCountByDay($timeframe,$site,$engine);
		
		$max_cost = 0;
		foreach($costs as $day => $values)
		{
			if ($counts[$day]["count"] > 0)			
				$max_cost = max($max_cost, ($values["cost"]/$counts[$day]["count"]));			
		}
		$max_cost = ceil($max_cost);
		
		echo "<!--- MAX: $max_cost --->";
		?>
<table border="1" cellpadding="0" cellspacing="0"><Tr><td>
<table cellpadding="0" cellspacing="0" border="1">
	<TR height="20"><td width='25'  valign="bottom">$<?= ($max_cost) ?></td><td rowspan="<?= intval($max_cost+2) ?>" valign="bottom">
	<?
		

		echo "<Table cellspacing='0' cellpadding='0' border='0'><tr valign='bottom'>";
		$total_count = 0;
		foreach($costs as $day => $values)
		{
			
			$counts[$day]["cost"] = $values["cost"];
			
			if ($counts[$day]["count"] > 0)			
				$counts[$day]["cpl"] = number_format(($values["cost"]/$counts[$day]["count"]),1,".",",");
			else
				$counts[$day]["cpl"] = number_format($values["cost"],1,".",",");
				$total_count += $counts[$day]["count"];
			//echo "\n<!--- ".$values["cost"]." / ".$counts[$day]["count"]." = ".number_format(($values["cost"]/$counts[$day]["count"]),1,".",",")." --->\n";
			echo "<td align='center'>";
			if ($counts[$day]["cpl"] > 0)
				echo "<img src='bar.php?width=25&height=".($counts[$day]["cpl"]*22)."&color=0,255,0' ";
			else
				echo "<img src='bar.php?width=25&height=".(5)."&color=0,255,0' ";
			echo " title='\$".$counts[$day]["cpl"]."0' />";
			echo "<br/>".intval($day)."</td>";
		}
		echo "</tr></table>";
	?>
	</td></TR>
	<?
		echo "<!--- $max_cost --->";
		for($x = ($max_cost-1); $x > 0; $x--)
		{		
			echo "<TR height='20'><td valign='bottom'>\$".$x."</td></tr>";
		}
	?>
	<TR height="20"><td valign="bottom">&nbsp;</td></tr>
</table>
</td></Tr></table>
<? 
	echo "<!--- total_count:  $total_count --->";   
} 

?>