<?php

	/**
	 * updates an affiliate with POST values from the affupdateform.php
	 * page. checks to see if the affid exists already, and then updates
	 * price per lead (ppl), company name (comp_name), and source tag (source).
	 * 
	 * @author david haveman
	 * @created 2/26/2008 7:20pm.
	 */

	session_start();
	include_once "../../inc_mysql.php";
	
	extract($_POST);
	
	$sql = "select * from movingdirectory.leadbased_affiliates " .
			"where affiliateid = '" . $affid . "' ";
	//echo $sql."<br/>";
	
	$rs = new mysql_recordset($sql);
	if (!$rs->fetch_array())
	{
		header("Location: affupdateform.php?affid=$affid&error=badid");
		exit();		
	}
	$category = $rs->myarray['lead_type'];
	
	$update = "update movingdirectory.leadbased_affiliates set " .
			 " comp_name = '$comp_name', source = '$source', ppl = '$ppl'" .
			 " where affiliateid = '$affid'";
	$rs = new mysql_recordset($update);
	
	header("Location: affreporting.php?category=$category");
	exit();
?>
