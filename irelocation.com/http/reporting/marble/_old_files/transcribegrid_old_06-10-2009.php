<?
	include "graphs/trans-graphing.php";
	session_start();
	define(CAMPAIGN,"transcribe");
	define(CPC_DEBUG,true);
	include "../../inc_mysql.php";


	$site = ($_POST['site']=="")?"all":$_POST['site'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	$graph = new TransGraph($timeframe,$engine,$site);
	
	
	echo "<!--- ".$_SESSION['lead_id']." --->";
	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		if (strlen($_SESSION['user']) > 0)
		header("Location: marbleindividualgrid.php");
		exit();
	}
	else
		$_SESSION['time'] = date("YmdHis");
	
	$sql = "Select co.comp_name,ca.* from movingdirectory.campaign as ca join ".
			" movingdirectory.company as co on co.comp_id = ca.comp_id where ca.site_id = 'transcribe' ".
			" and ca.month = '$timeframe'";			
	$rs = new mysql_recordset($sql );
	while ($rs->fetch_array())
	{
		$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}

	 include "skin_top.php"; 
		
	$today = date("d");
	
	$days = array(31,28,31,30,31,30,31,31,30,31,30,31);
	if ((substr($timeframe,0,4) % 4) == 0) $days[1]++;
	$days_in_month = $days[intval(substr($timeframe,4,2))];
	
	//$days_in_month = date('t');
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	
	
	
/* not sure what this is all about; it was just sitting there.  It's like this on the other grids, too.
	$days = 
*/
	

#	$sqlFilter = $graph->getSqlFilter($timeframe,$engine,$site);
	/*
	"";
	echo "<!--- ".print_r($_POST,true)." --->";
	if ($_POST['filter'] == 'Filter')
	{
		if ($site != "all")
		{
			$sqlFilter .= " and q.source like '".$site;
			if ($engine != "all")
				$sqlFilter .= "_".$engine."%' ";
			else
				$sqlFilter .= "%' ";
		}
		else
		{
			if ($engine != "all")
				$sqlFilter .= " and q.source like '%".$engine."%' and source not like '%search%' ";		
		}
	}
	echo "<!--- SQL Filter: $sqlFilter --->";
	*/

	$total_leads = $graph->getTotalLeads($timeframe,$engine,$site);

	if ($timeframe == date("Ym"))
		$total_leads_today = $graph->getTotalLeads(date("Ymd"),$engine,$site);
	else
		$total_leads_today = 0;
	
	$default_order_by = "comp_name,day";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	

	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal+c.temp_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				marble.campaign as c
				left join 
				movingdirectory.leads_transcription as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'transcribe'
				AND q.received like '$timeframe%'
				and $sqlFilter
				and c.month = '$timeframe'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				$default_order_by;";

	echo "<!---SQL 1:  $sql --->";
	$rs = new mysql_recordset($sql);
	
	
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					movingdirectory.leads_transcription as q
				where 
					q.received like '$timeframe%'					
					and $sqlFilter
				group by
					substring(received,7,2)";
	
	$total_rs = new mysql_recordset($total_sql);
	
	//include "cpc_graph.php";
?>	
<link rel='stylesheet' type='text/css' href='marble.css' />
<script>
	function checkCombo(site,engine)
	{
		var form = document.forms['sitefilter'];
		var site_value = site.options[site.selectedIndex].value;
		var engine_value = engine.options[engine.selectedIndex].value;		
	
		if (site_value == "aff" && (engine_value == "google" || engine_value == "msn" 
									|| engine_value == "overture" || engine_value == "google_local"))
			alert("If you select 'affiliates', you must select one of the affiliates.");
			
		else if (engine_value != "organic" && engine_value != "google" && engine_value != "msn" && engine_value != "all"
				&& engine_value != "overture" && engine_value != "google_local" && engine_value != "cmoves")
		{
			site.selectedIndex = 1;
			form.submit();
		}
		else
			form.submit();
	}

</script>

						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>&nbsp;</td></tr>
							<tr><td>
							<?	$graph->showGraph();  ?>
							</td></tr>
							<Tr><td>&nbsp;</td></tr>
							<tr><td>
								<table border='0' width="100%" cellspacing=0 cellpadding=2>
									<tr>
									<td width='40%'>
								<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
							</td>
							<td rowspan="3" width="20%">&nbsp;</td>
							<td rowspan="3" valign="top">
								<b><font face="Verdana" size="2">
								<?
									list($revenue,$cpl,$cost) = $graph->getData();
										
								?>
								Total Cost: $<?= $cost ?><br/>
								<? if (substr_count($revenue,"hit") == 0) echo "Total Revenue: \$"; echo $revenue; ?><br/>
								Cost Per Lead: $<?= $cpl ?><br/>
								</font>
							</td>
							</tr>
							<tr><td width='40%'>
								<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
							</td></tr>
							<tr><td width='40%'>
								<b><font face="Verdana" size="2">	

								<form action="marblegrid.php" method="post" name="sitefilter">	
									<? 	$graph->printFilter($timeframe,$engine,$site);	?>
								</form>
								</font>
								
								</tr></table>
							</td></tr>
							<Tr><td>

<?	
	
	
	if ($total_leads == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	
	$days = array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	if ((substr($timeframe,0,4) % 4) == 0) $days[2]++;
	$days_in_month = $days[intval(substr($timeframe,4,2))];
	
	for($i = 1; $i <= $days_in_month; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
	echo "<!--- $timeframe $days_in_month - $i --->";
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left' class='white'>Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count <= $days_in_month)//not a full month of data yet.
	{
		echo "<td colspan='".($days_in_month-$day_count+1)."'>&nbsp;</td>";
	}
	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	while($rs->fetch_array())
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
				
		
	
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days_in_month)//not a full month of data yet.
			{
				echo "<td colspan='".($days_in_month-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{
		
			echo "<tr class='$style'><td class='$style' >".($font_counter - 1)."</td>";
			
			
				
			$link = "<a href='Xmarbleindividualgrid.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='Xmarbleviewleads.php?ts=$timeframe$day$append'>$count</a>";
		else
			$link = "<A class='black'  href='Xmarbleviewleads.php?ts=$timeframe$day&lead_id=".$lead_id."$append'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
	
	if ($day_count < $days_in_month)//not a full month of data yet.
	{
		echo "<td colspan='".($days_in_month-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='Xmarbleviewleads.php?ts=$timeframe$append'>$total</a>";
	else
		$link = "<A class='black' href='Xmarbleviewleads.php?ts=$timeframe&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr>";
	$last_company = "";
	echo "</table>";

?>
</td></tr></table></td></tr></table></td></tr></table>