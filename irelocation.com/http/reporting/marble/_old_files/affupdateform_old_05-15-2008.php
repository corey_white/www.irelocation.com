<?
	/**
	 * form GUI to update affiliate values, linked to from affreporting.php
	 * processing page is affupdate.php
	 * 
	 * @author david haveman
	 * @created 2/26/2008 7:20pm.
	 */
	
	
	session_start();
	include_once "../../inc_mysql.php";
	
	define(AUTO,"auto");
	define(MOVING,"moving");
	define(SECURITY,"security");

	if ($_REQUEST["affid"] == "")
	{
		header("Location: affreporting.php");
		exit();
	}
	
	$sql = "select * from movingdirectory.leadbased_affiliates " .
			"where affiliateid = '" . $_REQUEST["affid"] . "' ";
	//echo $sql."<br/>";
	
	$rs = new mysql_recordset($sql);
	if (!$rs->fetch_array())
	{
		header("Location: affreporting.php");
		exit();
	}
	$aff = $rs->myarray;
	
	include "skin_top.php";
?>
<script language="javascript">
	function validate(tform)
	{
		if (tform.comp_name.value == "")
			alert("Enter the Company's Name");
		else if (tform.source.value.length < 2)
			alert("Enter the Company's Source Tag.");
		else if (tform.ppl.value == "")		
			alert("Enter the Company's Payment per Lead.");
		else if (isNaN(tform.ppl.value))
			alert("Enter the Company's Payment per Lead.");		
		else
			tform.submit();
	}
</script>

	<form action="affupdate.php" method="post" name="affadd">
		<input type='hidden' name='affid' value="<?= $_REQUEST['affid'] ?>" />
		<? if (isset($_REQUEST['dupe'])) { ?>
			<font color="#ff0000">Error, that campaign is already accounted for.</font>
		<? } ?>
		<table>
			<tr>
				<td colspan="2" align="left"><Strong>Alter an Affiliate</Strong></td>
			</tr>
			<tr>
				<td align="left">Company Name</td>
				<td align="left"><input type="text" name="comp_name" value="<?= $aff['comp_name'] ?>" /></td>
			</tr>
			<tr>
				<td align="left">Source Tag</td>
				<td align="left">
					<input type="text" name="source"  value="<?= $aff['source'] ?>" />
					<font size="-1">(ex <strong>autoextra</strong>)</font>
				</td>
			</tr>
			<tr>
				<td align="left">Payment per Lead</td>
				<td align="left"><input type="text" name="ppl" maxlength="5" size="8"
				 			value="<?= $aff['ppl'] ?>"  /> 
					<font size="-1">(ex 4.50)</font>
				</td>
			</tr>
			<tr>
				<td align="left">Month</td>
				<td align="left"><?= $aff['month']; ?></td>
			</tr>
			<tr>
				<td align="left">Lead Type</td>
				<td align="left">
					<?= $aff['lead_type']; ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<input type="button" value="Update Company" 
						onclick="validate(document.forms.affadd);" /></td>
			</tr>
		 </table>
	</form>
		


<?	include "skin_bottom.php";	?>

	