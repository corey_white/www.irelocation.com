<?
/* 
************************FILE INFORMATION**********************
* File Name:  alter_atus.php
**********************************************************************
* Description:  Takes the submitted form data from alteratus.php and acts on it
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	2/27/08 - fixed the CURL code below so that it would run
	2/28/08 - Added this comment block
**********************************************************************
*/
	session_start();
	include_once "../../inc_mysql.php";
	define(SITE_ID,"atus");
	extract($_POST);
	if ($function == "activate")
	{		
		$sql[] = "update marble.campaign set active = $active where month = '".
		date("Ym")."' and comp_id = $comp_id and site_id = '".SITE_ID."'";
	}	
	else if ($function == "updated")
	{
		if ($cur_monthly_goal != $monthly_goal && $monthly_goal != "")
		{
			$sql[] = "update marble.campaign set monthly_goal = '". 
					 $monthly_goal."' where month = '".$month."' and comp_id = '".
				     $comp_id."' and site_id = '".SITE_ID."'";
		}
		if ($cur_email != $email && $email != "")
		{
			$sql[] = "update marble.rules set lead_email = '".$email."' ".
					 "where lead_id = '".$lead_id."' site_id = '".SITE_ID."'";
		}
		if ($cur_temp_goal != $temp_goal && $temp_goal != "")
		{
			$sql[] = "update marble.campaign set temp_goal = '".$temp_goal."' ".
					 "where month = '".$month."' and comp_id = '".$comp_id."' ".
				   	 " and site_id = '".SITE_ID."'";
		}				
	}
	else
	{
		header("Location: alter".SITE_ID.".php?comp_id=$comp_id&error=invalidfunction");
		exit();	
	}
	
	if (count($sql) > 0)
		foreach($sql as $s)
		{
			//1500echo "$s<br/>";
			$rs = new mysql_recordset($s);
			$rs->close();
		}
		
	$status=$function;
	
	if ($when == "now" && ($month == date("Ym") || $month == ""))
	{
		$mapping = "http://www.irelocation.com/reporting/waiting.php?force";
		$ch = curl_init($mapping);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		$status=$function."-now";
	}
		
	header("Location: alter".SITE_ID.".php?comp_id=$comp_id&status=$status");
	
?>