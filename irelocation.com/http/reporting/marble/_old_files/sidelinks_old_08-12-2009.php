<?
/* 
************************FILE INFORMATION**********************
* File Name:  sidelinks.php
**********************************************************************
* Description:  These are the common Nav links 
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/14/08 - Rob - redesigned these links and took out some of the unused ones
**********************************************************************
*/

	$a = split("/",$_SERVER['SCRIPT_NAME']);
	$file = array_pop($a);
	echo "<!--- $file --->";

echo "<font face=\"Verdana\" size=\"2\">";

 if ($_SESSION['user'] == "uberadmin") { 
 
	 // REAL ESTATE
	 if ($file != "regrid.php") { 
		echo "<a class=\"white\" href=\"regrid.php\"> - Real Estate</a><br/>";
	 } else {
	 	echo "<span class=\"white_selected\"> - Real Estate</span><br/><br/>"; 
	 }
	echo "<br/>";
	
	// TOP AUTO
	 if ($file != "marblereport.php") { 
		echo "<a class=\"white\" href=\"marblereport.php\"> - Top Auto Report</a><br/>";
	 } else {
	 	echo "<span class='white_selected'> - Top Auto Report</span><br/>"; 
	 }
	 
	 if ($file != "marblegrid.php") { 
		echo "<a class=\"white\" href=\"marblegrid.php\"> - Grid</a><br/>";
	 } else {
	 	echo "<span class='white_selected'> - Grid</span><br/>"; 
	 }
	 
	 if ($file != "marbletest.php") { 
		echo "<a class=\"white\" href=\"marbletest.php\"> - Send Test Lead</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Send Test Lead</span><br/>"; 
	 }
	echo "<a class=\"white\" target='_blank' href=\"http://irelocation.com/marble/display.php?show#needed\"> - Display</a><br/>";
	
	echo "<br/>";
	
	
/*// TAS
	 if ($file != "tasreport.php") { 
		echo "<a class=\"white\" href=\"tasreport.php\"> - TAS Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - TAS Report</span><br/>"; 
	 }
	 
	 if ($file != "tasgrid.php") { 
		echo "<a class=\"white\" href=\"tasgrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>"; 
	}
	echo "<a class=\"white\" target='_blank' href=\"http://irelocation.com/topautoshippers/display.php?show#needed\"> - Display</a><br/>";
	
	echo "<br/>";
*/
	 
	// MOVING REPORT
	 if ($file != "movingreport.php") { 
		echo "<a class=\"white\" href=\"movingreport.php\"> - Moving Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Moving Report</span><br/>";  
	 }
	 
	 
	 if ($file != "movinggrid.php") { 
		echo "<a class=\"white\" href=\"movinggrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  		
	 }
	 
	 if ($file != "movingbreakout.php") { 
		echo "<a class=\"white\" href=\"movingbreakout.php\"> - Breakout</a><br/>";
	
	 } else {
	 	echo "<span class='white_selected'> - Breakout</span><br/>";  	
	}
	
	 if ($file != "viewleads_multiple_moving.php") { 
		echo "<a class=\"white\" href=\"viewleads_multiple_moving.php\"> - Moving Multi-View</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Moving Multi-View</span><br/>";  
	 }
	 
	echo "<br/>";
	
	
	// INTL REPORT
	 if ($file != "internationalreport.php") { 
		echo "<a class=\"white\" href=\"internationalreport.php\"> - Int. Report</a><br/>";
	
	 } else {
		 echo "<span class='white_selected'> - Int. Report</span><br/>";  
	 }
	 
	 if ($file != "internationalgrid.php") { 
		echo "<a class=\"white\" href=\"internationalgrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  	
	}
	
	echo "<br/>";
	
	// USAMOVING REPORT
	 if ($file != "usamovinggrid.php") { 
		echo "<a class=\"white\" href=\"usamovinggrid.php\"> - USAMoving Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - USAMoving Grid</span><br/>";  	
	}
	
	echo "<br/>";
	
	// VAN LINES
	 if ($file != "vanlinesreport.php") { 
		echo "<a class=\"white\" href=\"vanlinesreport.php\"> - Vanlines Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Vanlines Report</span><br/>";  
	 }	 
	 
	 if ($file != "vanlinesgrid.php") { 
		echo "<a class=\"white\" href=\"vanlinesgrid.php\"> - Vanlines Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  	
	 }
	 
	 
	 if ($file != "storageleads.php") { 
		echo "<a class=\"white\" href=\"storageleads.php\"> - Storage Leads</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Storage Leads</span><br/>";  	
	 }
	
	echo "<br/>";
	
	// ATUS
	 if ($file != "atusreport.php") { 
		echo "<a class=\"white\" href=\"atusreport.php\"> - Atus Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Atus Report</span><br/>";  
	 }
	 
	 if ($file != "atusgrid.php") { 
		echo "<a class=\"white\" href=\"atusgrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  
	 }
	echo "<a class=\"white\" target='_blank' href=\"http://irelocation.com/atus/display.php?show#needed\"> - Display</a><br/>";
	
	
	 if ($file != "atustest.php") { 
		echo "<a class=\"white\" href=\"atustest.php\"> - Send Test Lead</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Send Test Lead</span><br/>"; 
	 }
	 
	echo "<br/>";
	
	// SECURITY REPORT
	 if ($file != "securityreport.php") { 
		echo "<a class=\"white\" href=\"securityreport.php\"> - Security Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Security Report</span><br/>";  
	 }
	 
	 
	 if ($file != "securitygrid.php") { 
		echo "<a class=\"white\" href=\"securitygrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "securitybreakout.php") { 
		echo "<a class=\"white\" href=\"securitybreakout.php\"> - Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Breakout</span><br/>";  
	 }
	 
	 if ($file != "securityviewleads_multiple.php") { 
		echo "<a class=\"white\" href=\"securityviewleads_multiple.php\"> - Multi-Date Lead Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Multi-Date Lead Report</span><br/>";  
	 }
	 
	echo "<br/>";	
	
	// CANADA SECURITY REPORT
	 if ($file != "casecurityreport.php") { 
		echo "<a class=\"white\" href=\"casecurityreport.php\"> - CN Security Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - CN Security Report</span><br/>";  
	 }
	 
	 
	 if ($file != "casecuritygrid.php") { 
		echo "<a class=\"white\" href=\"casecuritygrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "casecuritybreakout.php") { 
		echo "<a class=\"white\" href=\"casecuritybreakout.php\"> - Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Breakout</span><br/>";  
	 }

	echo "<br/>";	
	
	// AUSTRALIA SECURITY REPORT
	 if ($file != "ausecurityreport.php") { 
		echo "<a class=\"white\" href=\"ausecurityreport.php\"> - AU Security Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - AU Security Report</span><br/>";  
	 }
	 
	 
	 if ($file != "ausecuritygrid.php") { 
		echo "<a class=\"white\" href=\"ausecuritygrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "ausecuritybreakout.php") { 
		echo "<a class=\"white\" href=\"ausecuritybreakout.php\"> - Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Breakout</span><br/>";  
	 }
	
	echo "<br/>";	
	
	// MATRIX REPORT
	 if ($file != "matrixreport.php") { 
		echo "<a class=\"white\" href=\"matrixreport.php\"> - Matrix Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Matrix Report</span><br/>";  
	 }
	 
	 
	 if ($file != "matrixgrid.php") { 
		echo "<a class=\"white\" href=\"matrixgrid.php\"> - Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "matrixsecuritybreakout.php") { 
		echo "<a class=\"white\" href=\"matrixsecuritybreakout.php\"> - Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Breakout</span><br/>";  
	 }
	 
	echo "<br/>";	
	
	// MERCHANT REPORTS
	 if ($file != "merchreport.php") { 
		echo "<a class=\"white\" href=\"merchreport.php\"> - Merch Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Merch Report</span><br/>";  
	 }
	 
	 if ($file != "merchgrid.php") { 
		echo "<a class=\"white\" href=\"merchgrid.php\"> - Merch Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Merch Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "merchbreakout.php") { 
		echo "<a class=\"white\" href=\"merchbreakout.php\"> - Merch Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Merch Breakout</span><br/>";  
	 }
	 
	 if ($file != "viewleads_multiple_merchant.php") { 
		echo "<a class=\"white\" href=\"viewleads_multiple_merchant.php\"> - Merchant Multi-View</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Merchant Multi-View</span><br/>";  
	 }
	 
	echo "<br/>";	
	
	// CALL CENTER REPORTS
	 if ($file != "callcenterreport.php") { 
		echo "<a class=\"white\" href=\"callcenterreport.php\"> - Call Center Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Call Center Report</span><br/>";  
	 }
	 
	 if ($file != "callcentergrid.php") { 
		echo "<a class=\"white\" href=\"callcentergrid.php\"> - Call Center Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Call Center Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "callcenterbreakout.php") { 
		echo "<a class=\"white\" href=\"callcenterbreakout.php\"> - Call Center Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Call Center Breakout</span><br/>";  
	 }
	 
	 if ($file != "viewleads_multiple_callcenter.php") { 
		echo "<a class=\"white\" href=\"viewleads_multiple_callcenter.php\"> - Call Center Multi-View</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Call Center Multi-View</span><br/>";  
	 }
	 
	echo "<br/>";	
	
	// TRANSCRIPTION REPORTS
	 if ($file != "transcribereport.php") { 
		echo "<a class=\"white\" href=\"transcribereport.php\"> - Transcription Report</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Transcription Report</span><br/>";  
	 }
	 
	 if ($file != "transcribegrid.php") { 
		echo "<a class=\"white\" href=\"transcribegrid.php\"> - Transcription Grid</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Transcription Grid</span><br/>";  
	 }
	 
	 
	 if ($file != "transcribebreakout.php") { 
		echo "<a class=\"white\" href=\"transcribebreakout.php\"> - Transcription Breakout</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Transcription Breakout</span><br/>";  
	 }
	 
	 if ($file != "viewleads_multiple_transcription.php") { 
		echo "<a class=\"white\" href=\"viewleads_multiple_transcription.php\"> - Transcription Multi-View</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Transcription Multi-View</span><br/>";  
	 }
	 
	echo "<br/>";	
	
	// LEAD PRICING
	 if ($file != "leadprices.php") { 
		echo "<a class=\"white\" href=\"leadprices.php\"> - Lead Pricing</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Lead Pricing</span><br/>"; 
	 }

	echo "<br/>";	
		
	// BOTTOM LINKS
	 if ($file != "queries.php") { 
		echo "<a class=\"white\" href=\"queries.php\"> - Daily CPC</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Daily CPC</span><br/>"; 
	 }
	 
	 
	 if ($file != "today.php") { 
		echo "<a class=\"white\" href=\"today.php\"> - Todays Leads</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Todays Leads</span><br/>"; 
	 }
	 
	 
	 if ($file != "emailreceipts.php") { 
		echo "<a class=\"white\" href=\"emailreceipts.php\"> - Email Receipts</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Email Receipts</span><br/>"; 
	 }
	 
	 
	 if ($file != "movingpercentage.php") { 
		echo "<a class=\"white\" href=\"movingpercentage.php\"> - Moving Ratio</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Moving Ratio</span><br/>";  	
	 }
	 
	 
	 if ($file != "summary.php") { 
		echo "<a class=\"white\" href=\"summary.php\"> - Summary</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Summary</span><br/>";  	
	 }
	 
	 
	 if ($file != "lookup.php") { 
		echo "<a class=\"white\" href=\"lookup.php\"> - Company Lookup</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - Company Lookup</span><br/>"; 
	 }
	 
	 
	 if ($file != "affreporting.php") { 
		echo "<a class=\"white\" href=\"affreporting.php\"> - AFF Reporting</a><br/>";
	 } else {
		 echo "<span class='white_selected'> - AFF Reporting</span><br/>"; 
	 }
	 
	echo "<br/>";
	
	echo "<a class=\"white\" href=\"marblelogout.php\"> - Log Out</a>";

 } else if ($_SESSION['user'] == "autotransporter") { 
	 if ($file != "marbleindividualgrid.php") { 
		echo "<a class=\"white\" href=\"marbleindividualgrid.php\"> - Home</a><br/>";
	 } 
	 
	 if ($file != "marbleformat.php") { 
		echo "<a class=\"white\" href=\"marbleformat.php\"> - View Lead Format</a><br/>";
	 } 
	 
	echo "<a class=\"white\" href=\"marblelogout.php\"> - Log Out</a>";
	

 } else if ($_SESSION['user'] == "a1auto") { 
	 if ($file != "marbleindividualgrid.php") { 
	echo "<a class=\"white\" href=\"marbleindividualgrid.php\"> - Home</a><br/>";
	 } 
	 if ($file != "marbleformat.php") { 
		echo "<a class=\"white\" href=\"marbleformat.php\"> - View Lead Format</a><br/>";
	 } 
	
	echo "<a class=\"white\" href=\"marblelogout.php\"> - Log Out</a>";
	

 } else if ($_SESSION['user'] == "a1auto" || $_SESSION['user'] == "wiseclick" || $_SESSION['user'] == "autoextra") { 
	 if ($file != "affiliates.php") { 
		echo "<a class=\"white\" href=\"affiliates.php\"> - Home</a><br/>";
	 } 
	echo "<a class=\"white\" href=\"marblelogout.php\"> - Log Out</a>";
 } 
echo "</font>";

?>