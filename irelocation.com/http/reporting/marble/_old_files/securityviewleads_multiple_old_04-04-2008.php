<?
/* 
************************FILE INFORMATION**********************
* File Name:  securityviewleads_multiple.php
**********************************************************************
* Description:  This is an adaptation on the securityviewleads.php script that will pull all leads for a date range.
**********************************************************************
* Creation Date:  3/14/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

session_start();

$admin = ($_SESSION['user'] == "uberadmin");

if ($_SESSION['user'] != "uberadmin")
{
	header("Location: login.php");
	exit();	
}


function formatData($myarray)
{
	switch($myarray["current_needs"])
	{
		case "new":	$myarray["current_needs"] = "New System";	break;
		case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
		case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
	}

	if ($myarray["quote_type"] == "com")
	{
		switch($myarray["sqr_footage"])
		{				
			case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
			case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
			case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
			case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
			case "0": 
			default:
				$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
		}
	}
	else
	{
		switch($myarray["sqr_footage"])
		{
			case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
			case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
			case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
			case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
			case "0": 
			default:
				$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
		}		
	}
	
	$myarray["maplink"] = getMapLink($myarray);
	$myarray["own_rent"] = ucwords($myarray["own_rent"]);
	$myarray["building_type"] = ucwords($myarray["building_type"]);
	$myarray["other_field"] = ucwords($myarray["other_field"]);
	$myarray["received"] = getReceived($myarray["received"]);
	
	return $myarray;
}

function getMapLink($myarray)
{
	return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
}		

function formatPhone($phone)
{	
	if (strlen($phone) == 10)
	{
		$p_area=substr($phone,0,3);
		$p_prefix=substr($phone,3,3);
		$p_suffix=substr($phone,6,4);

		return "($p_area) $p_prefix - $p_suffix";
	}
	else
		return $phone;
}

function getReceived($received)
{
	$r_year=substr($received,0,4);
	$r_month=substr($received,4,2);
	$r_day=substr($received,6,2);
	$r_hour=substr($received,8,2);
	$r_min=substr($received,10,2);
	if (strlen($received) == 14)
		return "$r_month/$r_day/$r_year $r_hour:$r_min";
	else
		return " -- ";
}	

function buildTSC($array,$font_counter)
{
	global $body;
	
	if ($font_counter % 2 == 0)//even
		$style = "on";
	else
		$style = "off";
	
	$array = formatData($array);
	extract($array);
	
	if ($quote_type == "res")
		$customerInfo = ucwords($name1." ".$name2)."<br/>".$email."<Br/>".formatPhone($phone1);
	else
		$customerInfo = ucwords($name1."<Br/>".$name2)."<br/>".$email."<Br/>".$phone1;
		
	$location = "<a href='$maplink' class='black' target='_blank' >$address <br/> $city, $state_code $zip</a>";
	if ($quote_type == "res")
		$quote_type = "Residential";
	else
		$quote_type = "Commercial";
	$info = $current_needs."<br/>".$building_type."<br/>".$sqr_footage;
	
	$body .= "<tr class='$style'>";
	$body .= "<td align='center'>$quote_id</td>";
	$body .= "<td align='left' >$customerInfo</td>";			
	$body .= "<td align='left' >$quote_type</td>";
	$body .= "<td align='left' >$received</td>";			
	$body .= "<td align='left' >$location</td>";			
	$body .= "<td align='left' >$info</td>";
	$body .= "<td align='left' >$source</td>";			
	$body .= "</tr>";
}

// change date format to match db, from MM/DD/YYYY to YYYY-MM-DD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Y-m-d',strtotime("$dateinput"));
    return $newDate;
}

// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}


//-------- BEGIN 
include "../../inc_mysql.php";
include "skin_top.php";


$start_date = $_REQUEST['start_date'];
$end_date = $_REQUEST['end_date'];
$lead_id = $_REQUEST['lead_id'];

$sql_start_date = ereg_replace("-","",dateYearFirst($start_date));
$sql_end_date = ereg_replace("-","",dateYearFirst($end_date));

echo "	
<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>
<form name=\"gid\" action=\"securityviewleads_multiple.php\" method=\"get\">
	<table cellpadding=\"2\">
		<tr>
			<td>Start Date</td>
			<td><input type=\"Text\" name=\"start_date\" value=\"$start_date\"><a href=\"javascript:show_calendar4('document.gid.start_date', document.gid.start_date.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a></td>
		</tr>
		<tr>
			<td>End Date</td>
			<td><input type=\"Text\" name=\"end_date\" value=\"$end_date\"><a href=\"javascript:show_calendar4('document.gid.end_date', document.gid.end_date.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a></td>
		</tr>
		<tr>
			<td>Lead ID</td>
			<td><input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\"></td>
		</tr>
		<tr>
			<td>Email to send to</td>
			<td><input name=\"email\" type=\"text\" value=\"$email\" size=\"30\"><br /><small>(Don't put an email in here unless you want it emailed to that address.  Separate multiple emails with a comma)</small></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input value=\"Go\" type=\"submit\"></td>
		</tr>
	</table>
	</form>";

/*
echo "start_date = $start_date<br />";
echo "end_date = $end_date<br />";
echo "lead_id = $lead_id<br />";
*/


if ( $start_date && $end_date && $lead_id ) { //-- if we have a start_date, end_date and lead_id, then process


	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);

	
	if (strlen($lead_id) > 0)
	{
		if (isset($_REQUEST['quote_type']))
			$append = " and q.quote_type = '".$_REQUEST['quote_type']."' ";
	
		$lead_sql = "select * from irelocation.leads_security as q where ".
					"lead_ids like '%$lead_id%' and left(received,8) >= '$sql_start_date' and left(received,8) <= '$sql_end_date' ";
	
		if (isset($orderby))
		{
			$lead_sql .= " order by $orderby";		
		}
	
		$company_info_sql = "select * from movingdirectory.company as co join ".
							" movingdirectory.campaign as ca on ca.comp_id = co.comp_id ".
							" where ca.lead_id = $lead_id and ca.site_id = 'tsc'";
		
		$company = new mysql_recordset($company_info_sql);
		$company->fetch_array();
		$comp_name = $company->myarray['comp_name'];
	}
	else
	{
		$lead_sql = "Select * from irelocation.leads_security as q where source like 'tsc_$engine' and left(received,8) >= '$sql_start_date' and left(received,8) <= '$sql_end_date' ";
		$comp_name = ucwords($_SESSION['user']);						
	}
	
	
/*
	if (isset($_REQUEST['site']))
	{
		$sqlFilter .= " and q.source like '".$_REQUEST['site'];
		if (isset($_REQUEST['engine']))
			$sqlFilter .= "_".$_REQUEST['engine']."%' ";
		else
			$sqlFilter .= "%' ";
	}
	else
	{
		if (isset($_REQUEST['engine']))
			$sqlFilter .= " and q.source like '%".$_REQUEST['engine']."%' and q.source not like '%search%' ";		
	}
*/
	
	$lead_sql  .= $sqlFilter;
							
	
	$leads = new mysql_recordset($lead_sql);
	
	$body .= "<table border=\"0\" width=\"100%\" cellspacing=0 cellpadding=1>
		<Tr><td>&nbsp;</td></tr>
		<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">Company: $comp_name </font></b>
		</td>
		</tr>";
		
		if (strlen($lead_id) > 0) { 
		
		$body .= "<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">Phone: ";
				
				$phone_num = ereg_replace("[^0-9]","",$company->myarray['phone_num']);
				if (strlen($phone_num) > 10)
				{	
					$ext =  "Ext. ".substr($phone_num,10);
					$phone_num = substr($phone_num,1,10);
				}
				if (strlen($phone_num) == 10)
				{
					$a = substr($phone_num,0,3);
					$b = substr($phone_num,3,3);
					$c = substr($phone_num,6);
					$body .= "($a) $b - $c $ext ";
				}
				
										
			$body .= "</font></b>
		</td>
		</tr>
		<tr>
		  <td>
			<b><font face=\"Verdana\" size=\"2\">View Lead Format</font></b>
		</td>
		</tr>";			
		
		} 
		$self_link = $_SERVER['PHP_SELF'];
		
		$body .= "<tr><td>&nbsp;</td></tr>
		<tr><td align='left'>
	
		<table border=\"1\" width=\"100%\" cellspacing='0' cellpadding='3'>
		<tr class=\"toprow\">";
		
			$body .= "<th><A href=\"$self_link?$filterlink&orderby=quote_id\" class='black'>Quote Id</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=email\" class='black'>Customer Info</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=quote_type\" class='black'>Quote Type</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=received\" class='black'>Time Stamp</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=state_code\" class='black'>Location</a></th>		
			<th><A href=\"$self_link?$filterlink&orderby=sqr_footage\" class='black'>Info</a></th>
			<th><A href=\"$self_link?$filterlink&orderby=source\" class='black'>Campaign</a></th>";
			
		$body .= "</tr>";
									
		 
		$font_counter = 0;
		$lead_count = 0;
		while($leads->fetch_array())
		{
			$body .= buildTSC($leads->myarray,$font_counter);		
			$font_counter ++;
			$lead_count++;
		}						
		
$body .= "	</table>
	<div align=\"left\">
		<font face='verdana' size=2>
			<strong>$lead_count</strong> Leads.
		</font>
	</div>
";	


} 

//-- if there was an email entered into the form, email the leads to that email address(es)
if ( $email ) {
    
	$headers .= 'From: iRelocation <noreply@iRelocation.com>' . "\r\n";
	$headers .= "MIME-Version: 1.0" . "\r\n";
	$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
	
	$SUBJECT = "Test Leads";
	
	$MSG_BODY = "
	<html>
	<head>
	<title>Email</title>
	</head>
	<body>
	$body
	</body>
	</html>
	";
	
	mail("$email","$SUBJECT","$MSG_BODY","$headers");

} 
echo "\n\n<!-- body start -->\n\n";
echo $body;
echo "\n\n<!-- body end -->\n\n";

include "skin_bottom.php"; ?>
