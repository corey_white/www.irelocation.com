<?
	session_start();
	
	define(CPC_DEBUG,true);
	
	include "skin_top.php";
	include "graphs/percentage.php";
	include "graphs/moving.php";
	echo "\n<!--- POST[TS] = ".$_POST['ts']."--->\n";
	$site = ($_POST['site']=="")?"all":$_POST['site'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];

	$graph = new MovingGraph($timeframe,$engine,$site);
	
	/*
	function getTMC($ts,$long=true)
	{
		if ($ts == "") $ts = date("Ym");
		
		if ($long) $long = " and destination_state != origin_state ";
		else $long = " and destination_state = origin_state ";	

		$sql = "select substring(received,7,2) day, count(*) count from movingdirectory.quotes ".
				" where source like '%tmc%' and cat_id = 2 and received like '$ts%' $long ".
				" group by substring(received,7,2);";
		$results = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$results[$rs->myarray['day']] = (int) $rs->myarray['count'];
		$rs->close();

		$sql2 = "select substring(received,7,2) day, count(*) count from movingdirectory.quotes_local".
				" where source like '%tmc%' and received like '$ts%' $long group by substring(received,7,2);";
		$rs = new mysql_recordset($sql2);
		while($rs->fetch_array())
			$results[$rs->myarray['day']] += (int) $rs->myarray['count'];
		$rs->close();

		return $results;
	}
	

	function getData($ts)
	{
		$site = ($_POST['site']=="")?"all":$_POST['site'];
		$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
		
		
		$count = getCount($ts,$site,$engine);
		if ($site != "aff")
			$cost = getCost($ts,$site,$engine);
		else //for now this works, cause they're both 4 bucks a lead.
		{
			if ($engine == "WC")
				$cost = 8*$count;
		}	
		
		if ($engine == "all" && $site == "all")
		{	
			
			$aff_cost = getAffiliateCost("moving",$ts);
			//$cost += $wc_cost +  $eleads_cost;
			$cost += $aff_cost;
		}
		echo "<!--- Lead Count: $count --->";
		
		
		$count += $vl_long + $vl_short;
		$revenue = number_format($revenue,2,".",",");	
		
		if ($count > 0)
			$cpl = number_format(($cost / $count),2,".",",");
		else 
			$cpl = $cost;
		$cost = number_format($cost,2,".",",");				
	
		if (floatval($cpl) > 13.50)
			$cpl = "<font color='red'>$cpl</font>";
		else if (floatval($cpl) > 10.00)
			$cpl = "<font color='#B5B128'>$cpl</font>";
		else
			$cpl = "<font color='green'>$cpl</font>";
			
		return array($revenue,$cpl,$cost);
	}
	
	function getCost($timeframe,$site="all",$engine="all")
	{
		$sql = "select sum(cost) cost from movingdirectory.costperclick where ts like '$timeframe%' ";
		
		if ($site != "all")
			$sql .= " and site like '$site%' ";		
		else
			$sql .= " and site in ('tm','me','pm','tm-local') ";		
			
		if ($engine != "all")
			$sql .= " and search_engine like '$engine%' ";
				
		if(CPC_DEBUG)
			echo "\n<!---Cost: $sql --->\n";
		
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		
		$cost = $rs->myarray["cost"];
		
		return $cost;		
	}	
	*/
	/*
	function getVanLinesCount($timeframe,$site="all",$engine="all",$long=true)
	{
		$sql = "select count(*) count from movingdirectory.quotes_local q where received like '$timeframe%' ";
		
		if ($long)
			$sql .= " and destination_state != origin_state ";
		else
			$sql .= " and destination_state = origin_state ";
			
		if (strlen($_POST['ts']) != 6)
			$this_month = date("Ym");
		else
			$this_month = $_POST['ts'];
		if ($timeframe == $this_month)
		{
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		}
		
		if ($site != "all")
		{
			$sql .= " and q.source like '".$site;
			if ($engine != "all")
				$sql .= "_$engine%' ";
			else
				$sql .= "%' ";
		}
		else 
		{
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
			
		}
		
		$sql .= " and q.success = 1 ";
		
		if(CPC_DEBUG)
			echo "\n<!--- $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	
	}
	
	function getCount($timeframe,$site="all",$engine="all")
	{
		$sql = "select count(*) count from movingdirectory.quotes q where received like '$timeframe%' and cat_id in (2,3) ";
		
		if (strlen($_POST['ts']) != 6)
			$this_month = date("Ym");
		else
			$this_month = $_POST['ts'];
		if ($timeframe == $this_month)
		{
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		}
		
		if ($site != "all")
		{
			$sql .= " and q.source like '".$site;
			if ($engine != "all")
				$sql .= "_$engine%' ";
			else
				$sql .= "%' ";
		}
		else 
		{
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
			
		}
		if(CPC_DEBUG)
			echo "\n<!--- $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	}
	*/
	echo "<!--- ".$_SESSION['lead_id']." --->";
	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		if (strlen($_SESSION['user']) > 0)
		header("Location: marbleindividualgrid.php");
		exit();
	}
	else
		$_SESSION['time'] = date("YmdHis");


	if (strlen($_POST['ts']) != 6)
		$this_month = date("Ym");
	else
		$this_month = $_POST['ts'];


	$sql = "Select co.comp_name,ca.* from movingdirectory.campaign as ca join ".
			"movingdirectory.company as co on co.comp_id = ca.comp_id where ca.site_id = 'pm' ".
			" and ca.month = '$this_month'";			
	$rs = new mysql_recordset($sql );
	while ($rs->fetch_array())
	{
		$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}

	
		
	$today = date("d");
	
	$days = array(0,31,28,31,30,31,30,31,31,30,31,30,31);
	if ((substr($timeframe,0,4) % 4) == 0) $days[2]++;
	$days_in_month = $days[intval(substr($timeframe,4,2))];
	
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	

	$sqlFilter = "";
	echo "<!--- ".print_r($_POST,true)." --->";
	if ($_POST['filter'] == 'Filter')
	{
		if ($_POST['site'] != "all")
		{
			$sqlFilter .= " and q.source like '".$_POST['site'];
			if ($_POST['engine'] != "all")
				$sqlFilter .= "_".$_POST['engine']."%' ";
			else
				$sqlFilter .= "%' ";
		}
		else
		{
			if ($_POST['engine'] != "all")
				$sqlFilter .= " and q.source like '%".$_POST['engine']."%' and source not like '%search%' ";		
		}
	}
	echo "<!--- SQL Filter: $sqlFilter --->";
	
	$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received like '$this_month%' and cat_id = 2 ".
					"$sqlFilter;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];	
	
	
	$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received like '$this_month$today%' and cat_id = 2 ".
					"$sqlFilter;";
	echo "<!--- top daily: $total_count --->";					
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads_today += $rs->myarray["count"];
	
	
	$default_order_by = "comp_name,day";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	if (strlen($_POST['ts']) != 6)
		$ts = date("Ym");
	else
		$ts = $_POST['ts'];
	
	
	
	
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					movingdirectory.quotes as q
				where 
					q.received like '$ts%'					
					$sqlFilter
					and cat_id in (2)
				group by
					substring(received,7,2)";
	
	$total_rs = new mysql_recordset($total_sql);
	
	include "cpc_graph.php";
	
	?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
		<?
			$site = ($_POST['site']=="")?"all":$_POST['site'];
			$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
			//showMovingGraph($this_month,$site,$engine);
			$graph->showGraph($timeframe,$engine,$site);
		?>
		</td></tr>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border='0' width="100%" cellspacing=0 cellpadding=2>
				<tr>
				<td width='40%'>
			<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b> 

		</td>
		<td rowspan="3" width="20%">&nbsp;</td>
		<td rowspan="3" valign="top">
			<b><font face="Verdana" size="2">
			<?
				list($revenue,$cpl,$cost) = $graph->getData($ts);
			?>
			Total Cost: $<?= $cost ?><br/>
			Total Revenue: $<?= $revenue ?><br/>
			Cost Per Lead: $<?= $cpl ?><br/>			
			<a href='movingbreakout.php?ts=<?= $ts ?>' class="black">Moving Breakout</a>
			</font>
		</td>
		</tr>
		<tr><td width='40%'>
			<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>			
		</td></tr>
		<tr><td width='40%'>
			<b><font face="Verdana" size="2">	

			<form action="movinggrid.php" method="post" name="sitefilter">	
			Period
			<?= printPeriodDropDown("ts",$_POST['ts']); ?><br/>
			Site:									
						<select name='site'>
						<optgroup label="Sites"/>
						<option value='all'>All</option>
					<? 
						$sites = array('tm','pm','me');
						
						foreach($sites as $site)
						{
							echo "<option value='$site'";
							if ($_POST['site'] == $site) echo " selected ";
							echo ">".strtoupper($site)."</option>\n";
						}
					?>
					<optgroup label="Affiliates"/>
					<option value="aff"<? if ($_POST['site'] == 'aff') echo ' selected'; ?> >Affiliates</option>
					</select> 										
						<input type='hidden' name="filter" value="Filter" />
					<select name='engine' >
						<optgroup label="Search Engines"/>
						<option value='all'>All</option>
					<? 
						$engines = array('google','ask','overture','msn','bizcom');
						foreach($engines as $engine)
						{
							echo "<option value='$engine'";
							if ($_POST['engine'] == $engine) echo " selected ";
							echo ">".ucwords($engine)."</option>\n";														
						}
					?>
					<optgroup label="Affiliates"/>
			<option value="wc"<?= ($_POST['engine']=='wc')?' selected':" " ?> >
				Wise Click
			</option>
			<option value="movedirect"<? if ($_POST['engine'] == 'movedirect') echo ' selected'; ?> >
				Move Direct
			</option>
			<option value="boxdev"<? if ($_POST['engine'] == 'boxdev') echo ' selected'; ?> >
				Boxes Delivered
			</option>
			<option value="A1AUTO"<? if ($_POST['engine'] == 'A1AUTO') echo ' selected'; ?> >
				A1 Auto
			</option>	
			</select>
					<br/>
					<input type='submit' name="filter" value="Filter" />
				</form>
			</font>
			
			</tr></table>
		</td></tr>
		<Tr><td>

<?	
	
	$sql = "select count(*) count from movingdirectory.quotes q where ".
			"received like '$this_month%' $sqlFilter and cat_id = 2 ";
	echo "<!--- $sql --->";
	$check_for_any = new mysql_recordset($sql);
	$check_for_any->fetch_array();
	if ($check_for_any->myarray["count"] == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
//	$days = date('t');
	for($i = 1; $i <= $days_in_month; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left' color='#00000'  >Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count <= $days_in_month)//not a full month of data yet.
	{
		echo "<td colspan='".($days_in_month-$day_count+1)."'>&nbsp;</td>";
	}
	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				movingdirectory.campaign as c
				left join 
				movingdirectory.quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'pm'
				AND q.received like '$ts%'
				$sqlFilter
				and q.cat_id in (2)
				and c.month = '$this_month'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				$default_order_by;";

	echo "<!--- $sql --->";
	$rs = new mysql_recordset($sql);
	
	while($rs->fetch_array())
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
							
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days_in_month)//not a full month of data yet.
			{
				echo "<td colspan='".($days_in_month-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{
		
			echo "<tr class='$style'><td class='$style' >$font_counter</td>";
			
			
				
			$link = "<a href='movingformat.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			//$link = $comp_name;
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='movingviewleads.php?ts=$ts$day$append'>$count</a>";
		else
			$link = "<A class='black'  href='movingviewleads.php?ts=$ts$day&lead_id=".$lead_id."$append'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
	
	if ($day_count < $days_in_month)//not a full month of data yet.
	{
		echo "<td colspan='".($days_in_month-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='movingviewleads.php?ts=$ts$append'>$total</a>";
	else
		$link = "<A class='black' href='movingviewleads.php?ts=$ts&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr>";
	/*
	function getIndyVanlinesCount($ts,$long)
	{
		if ($long)
		{
			$filter = "q.destination_state != q.origin_state ";
			$name = "Van Lines - Long";
		}
		else
		{
			$filter = "q.destination_state = q.origin_state	";
			$name = "Van Lines - Local";
		}
		$sql = "select 			
					count(q.quote_id) count
				from		
					movingdirectory.directleads as d 	
					join
					movingdirectory.quotes_local as q
				where 
					q.received like '$ts%'								
					and d.lead_id = 1516
					and q.success = 1
					and $filter";
				
		$counts = array();
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
	
		return $rs->myarray['count'];
	}	
	
	function getVanlinesCountByDay($ts,$long)
	{
		if ($long)
		{
			$filter = "q.destination_state != q.origin_state ";
			$name = "Van Lines - Long";
		}
		else
		{
			$filter = "q.destination_state = q.origin_state	";
			$name = "Van Lines - Local";
		}
		$sql = "select 			
					d.lead_id,
					'$name' as comp_name,	
					'2500' as 'goal',
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'
				from		
					movingdirectory.directleads as d 	
					join
					movingdirectory.quotes_local as q
				where 
					q.received like '$ts%'								
					and d.lead_id = 1516
					and q.success = 1
					and $filter
				group by
					substring(received,7,2);";
		$counts = array();
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
			$counts[] = $rs->myarray;						
		return $counts;
	}
	
	$last_company = "";
	$count = "";
	$total = "";
	$count = getVanlinesCountByDay($this_month,true);
	$local = getVanlinesCountByDay($this_month,false);
	$all_vanlines = array_merge($local,$count);
	foreach($all_vanlines as $daily)
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
							
		extract($daily);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days)//not a full month of data yet.
			{
				echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{			
			echo "<tr class='$style'><td class='$style' >$font_counter</td>";
			$link = "<a href='marbleindividualgrid.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			//$link = $comp_name;
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (substr_count($comp_name,"Local") > 0)
			$local = "&local";
		else
			$local = "&long";
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='movingviewleads.php?ts=$ts$day$append$local'>$count</a>";
		else
			$link = "<A class='black'  href='movingviewleads.php?ts=$ts$day&lead_id=".$lead_id."$append$local'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
		
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='movingviewleads.php?ts=$ts$append'>$total</a>";
	else
		$link = "<A class='black' href='movingviewleads.php?ts=$ts&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr>";
	
	
	*/
	

	function printRow($array,$font_counter,$comp_name,$days)
	{
		$last_company = "";
		$count = "";
		$total = "";
		$day_count = 0;

		foreach($array as $day => $count)
		{
			if ($font_counter % 2 == 0)//even
				$style = "on";
			else
				$style = "off";
					
			if ($last_company == "")
			{			
				echo "\n<tr class='$style'><td class='$style' >$font_counter</td>";
				$link = "<a name='tmc' href='#tmc' class='black'>$comp_name</a>";
				//$link = $comp_name;
				echo "<th align='left'>";			
				echo "$link</th>";	
				echo "<th class='$style'>--</th>";
				$last_company = $comp_name;
				$total = "";	
				$day_count = 0;
				$font_counter ++;
			}
	
			$day_count++;
			
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
	
			if (intval($day) == $day_count)//
				echo "<th><A class='black'  href='#tmc'>$count</a></th>";
			$total += $count;
		}
			
		if ($day_count < $days)//not a full month of data yet.
			echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			
		
		echo "<td  align='right'><A class='black' href='#tmc'>$total</a></td></tr>\n";
	}
	
	/*

	$TMC_LONG = getTMC($_POST['ts']);
	$TMC_LOCAL = getTMC($_POST['ts'],false);
	$comp_name = "TopMovCo.com";

	printRow($TMC_LONG,$font_counter++,$comp_name." - Long",$days);
	printRow($TMC_LOCAL,$font_counter++,$comp_name." - Local",$days);

	//$days = 0;

	echo "</table>";

	include "cpc.php"; 
	*/
	include "skin_bottom.php";
	?>