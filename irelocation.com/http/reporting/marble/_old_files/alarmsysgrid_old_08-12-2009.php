<?
	session_start();
	include "skin_top.php";
	
	define(SHOW_CPC_GRAPH,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");		
	define(CAMPAIGN,"alarmsys");
	define(SITE_ID,'usalarm');
	define(SUBMIT_PAGE,"alarmsysgrid.php");
	define(SITES,"alarmsys");		
	
	include "graphs/alarmsys_security.php";
	
	

	
#	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	$timeframe = ($_REQUEST['ts']=="")?date("Ym"):$_REQUEST['ts'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	//$site = ($_POST['site']=="")?"usalarm":$_POST['site'];

	if ( !$site ) {
		$site = "all";
	} 
	
	
	$graph = new MatrixSecurityGraph($timeframe,$engine,$site);



	//$graph->loadLeadPrices();
	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		if (strlen($_SESSION['user']) > 0)
		header("Location: marbleindividualgrid.php");
		exit();
	}
	else
		$_SESSION['time'] = date("YmdHis");

	if (strlen($_REQUEST['ts']) != 6)
		$this_month = date("Ym");
	else
		$this_month = $_REQUEST['ts'];


	$sql = "Select co.comp_name,ca.* from movingdirectory.campaign as ca join ".
			"movingdirectory.company as co on co.comp_id = ca.comp_id where ".
			"ca.site_id = '".SITE_ID."' and ca.month = '$this_month' ";			
	$rs = new mysql_recordset($sql );
	while ($rs->fetch_array())
	{
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}

	
		
	$today = date("d");
	$days_in_month = date('t');
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	

	$sqlFilter = "";
	echo "<!--- ".print_r($_POST,true)." --->";
	
	if ($_POST['site'] != 'all')
		$sqlFilter .= " and q.source like '".$_POST['site']."%' ";
	
	if ($_POST['engine'] != "all" && $_POST['engine'] != "organic")
		$sqlFilter .= " and q.source like '%".$_POST['engine']."%' ";

	if ($_POST['engine'] != "all" && $_POST['engine'] == "organic")
		$sqlFilter .= " and (q.source not like 'aff_%' and q.source not like '%google%' and q.source not like '%overture%' and q.source not like '%msn%' and q.source not like '%clkbth%'  )";

	echo "<!--- SQL Filter: $sqlFilter --->";
	
	$total_count = "select count(*) count from irelocation.leads_security q where ".
					" received like '$this_month%' and campaign = '".CAMPAIGN."'  ".
					"$sqlFilter ";
	if ($quote_type != "all" && $quote_type != "")
		$total_count .= " and quote_type = '$quote_type' ";						
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	echo "<!--- all: $total_count --->";			
	$total_leads = $rs->myarray["count"];
		
	$total_count = "select count(*) count from irelocation.leads_security  q where ".
					" received like '$this_month$today%' and campaign = '".CAMPAIGN."' ".
					"$sqlFilter ";
	if ($quote_type != "all" && $quote_type != "")
		$total_count .= " and quote_type = '$quote_type' ";											
	echo "<!--- today: $total_count --->";					
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	
	$default_order_by = "comp_name,day";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	if (strlen($_REQUEST['ts']) != 6)
		$ts = date("Ym");
	else
		$ts = $_REQUEST['ts'];
	
	
	
	if ($quote_type != "all" && $quote_type != "")
		$qt = " and quote_type = '$quote_type' ";
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					irelocation.leads_security as q
				where 
					q.received like '$ts%'					
					$sqlFilter
					$qt 
					and campaign = '".CAMPAIGN."' 
				group by
					substring(received,7,2)";
	
	$total_rs = new mysql_recordset($total_sql);
	
	//include "cpc_graph.php";

	
?>
<script>
	function checkCombo(site,engine)
	{
		var form = document.forms['sitefilter'];
		var site_value = site.options[site.selectedIndex].value;
		var engine_value = engine.options[engine.selectedIndex].value;		
	
		if (site_value == "aff" && (engine_value == "google" || engine_value == "msn" 
									|| engine_value == "overture" || engine_value == "google_local"))
			alert("If you select 'affiliates', you must select one of the affiliates.");
			
		else if (engine_value != "organic" && engine_value != "google" && engine_value != "msn" && engine_value != "all"
				&& engine_value != "overture" && engine_value != "google_local")
		{
			site.selectedIndex = 1;
			form.submit();
		}
		else
			form.submit();
	}

</script>
<table border="0" width="100%" cellspacing=0 cellpadding=2>

	<Tr><td>&nbsp;</td></tr>
	<tr><td>
		<table border='0' width="100%" cellspacing=0 cellpadding=2>
			<tr>
			<td width='40%'>
		<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b> 									
	</td>
	<td rowspan="3" width="20%">&nbsp;</td>
	<td rowspan="3" valign="top">
		<b><font face="Verdana" size="2">
		<?
				list($revenue,$cpl,$cost) = $graph->getData();

		?>
		Total Cost: $<?= $cost ?><br/>
		Total Revenue: $<?= $revenue ?><br/>
		Cost Per Lead: $<?= $cpl ?><br/>
		<a href='matrixsecuritybreakout.php?ts=<?= $ts ?>' class="black">Security Breakout</a>
		</font>
		<?php
		if ( $engine == "callcenter" ) {
			echo "<br />";
			echo "<p style=\"font-family: arial; font-size: 8pt;\">(Note: it is not possible to calculate the Cost per Lead on CallCenter leads, as they are charged on a per-minute basis)</p>";
		} 
		if ( eregi("clkbth",$engine) ) {
			echo "<br />";
			echo "<p style=\"font-family: arial; font-size: 8pt;\">(Note: The total listed above for Click Booth leads is for all Click Booth leads in the data range)</p>";
		} 
		?>
	</td>
	</tr>
	<tr><td width='40%'>
		<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
		
	</td></tr>
	<tr><td width='40%'>
		<b><font face="Verdana" size="2">	

		<form action="<?= SUBMIT_PAGE ?>" method="post" name="sitefilter">	
		<? $graph->printFilter($timeframe,$engine,$site);	?>
			</form>
		</font>
		
		</tr></table>
	</td></tr>
	<Tr><td>

<?	
	
	$sql = "select count(*) count from irelocation.leads_security q where ".
			"received like '$this_month%' $sqlFilter and campaign = '".CAMPAIGN."' ";
	if ($quote_type != "all" && $quote_type != "")
		$sql .= " and quote_type = '$quote_type'";
		
	echo "<!--- $sql --->";
	$check_for_any = new mysql_recordset($sql);
	$check_for_any->fetch_array();
	if ($check_for_any->myarray["count"] == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	$days = date('t');
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left' style='color:#000000' >Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count <= $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
	}

	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	if ($quote_type != "all" && $quote_type != "")
		$qt = " and quote_type = '$quote_type'";
	
	if ( $show_renters == "no" ) {
		$rent_sql = "and q.own_rent != 'rent' ";
		echo "<h3>Not Showing Renters in Counts</h3>";
	} else {
		$rent_sql = "";
	}
	
	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				movingdirectory.campaign as c
				left join 
				irelocation.leads_security as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = '".SITE_ID."'
				AND q.received like '$ts%'
				$sqlFilter
				$qt
				and c.month = '$this_month'			
				and campaign = '".CAMPAIGN."' 				
				 $rent_sql
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				$default_order_by;";

	echo "<!---MAIN: $sql --->";
	$rs = new mysql_recordset($sql);
	
	while($rs->fetch_array())
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
							
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days)//not a full month of data yet.
			{
				echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{
		
			echo "<tr class='$style'><td class='$style' >".($font_counter-1)."</td>";
			
			
				
			$link = "<a href='movingformat.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			//$link = $comp_name;
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='securityviewleads.php?ts=$ts$day$append'>$count</a>";
		else
			$link = "<A class='black'  href='securityviewleads.php?ts=$ts$day&lead_id=".$lead_id."$append'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='securityviewleads.php?ts=$ts$append'>$total</a>";
	else
		$link = "<A class='black' href='securityviewleads.php?ts=$ts&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr>";
	echo "</table>";
	
	if ( $show_renters == "no" ) {
		echo "<a href=\"matrixgrid.php?show_renters=yes&ts=" . $_REQUEST['ts'] . "\" style=\"color: blue; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Click to reload with renter counts</a>";
	} else {
		echo "<a href=\"matrixgrid.php?show_renters=no&ts=" . $_REQUEST['ts'] . "\" style=\"color: blue; font-family: helvetica,arial,sans-serif; font-size: 10pt;\">Click to reload without renter counts</a>";
	}
	
	
	echo "<table>";
	echo "<Tr><td>&nbsp;</td></tr>
	<tr><td>";
		$quote_type = ($_POST['quote_type']=="")?"all":$_POST['quote_type'];
		$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
		$site = ($_POST['site']=="")?"all":$_POST['site'];
		if (SHOW_CPC_GRAPH)
			$graph->showGraph();	
	echo "</td></tr>";

	echo "</table>";



 //include "cpc.php";
 include "skin_bottom.php";
?>