<?
	define(CPC_DEBUG,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	define(SITES,"atac,atsc");
	define(CAMPAIGN,'aas');
	define(SITE_ID,'aas');
	define(SUBMIT_PAGE,"ausecuritygrid.php");
			
	define(TYCO,1579);
	define(SAFEHOME,1577);
	define(HARVEYNORMAN,1578);
	define(CHUBB,1582);
	define(LEAD_SOURCE,"standard");
	define(CAMPAIGN_TYPE,"adshare");
	define(SHOW_CPC_GRAPH,true);
	
	/*
	OPERATIONAL NOTE:
	If you add or delete any companies from the above define statements, you need to change the ARRAY in the getTotalLeadRevenue function in graphs/security.php as well (really bad system - need to update this at some point)
	
	*/
	
	include "securitygrid.php";

?>f