<?
/* 
************************FILE INFORMATION**********************
* File Name:  movingbreakout.php
**********************************************************************
* Description:  Displays lead info for pricing, costs, revenue
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/28/08 15:40 PM - Rob - Added more leads to the "Rejection Bypass"
**********************************************************************

NOTES: When adding new moving companies to this, there are 2 parts.
	1a. If the company has the ability to reject leads, they need to be added to the getMovingValidation and getMovingUnapprovedValidation funcitons;
				This also means that they probably have (or should have) their own tables.
	1b. If the company cannot reject a lead, add them to the foreach($comp as $company) loop in the BYPASS section.
	
	2. their Cost Per Lead needs to be added to the marble.cost_per_lead table.
*/

	session_start();
	include "../../inc_mysql.php";
echo "\n\n<!-- CPC_DEBUG = " . CPC_DEBUG . " -->\n\n";	


	function getVanLinesCount($timeframe,$site="all",$engine="all",$long=true)
	{
		$sql = "select count(*) count from movingdirectory.quotes_local q where received like '$timeframe%' ";
		
		if ($long)
			$sql .= " and destination_state != origin_state ";
		else
			$sql .= " and destination_state = origin_state ";
			
		if (strlen($_POST['ts']) != 6)
			$this_month = date("Ym");
		else
			$this_month = $_POST['ts'];
		if ($timeframe == $this_month)
		{
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		}
		
		if ($site != "all")
		{
			$sql .= " and q.source like '".$site;
			if ($engine != "all")
				$sql .= "_$engine%' ";
			else
				$sql .= "%' ";
		}
		else 
		{
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
			
		}
		
		$sql .= " and q.success = 1 ";
		
		if(CPC_DEBUG)
			echo "\n<!--- Van Lines: $sql --->\n";	// <-- need to use a variant of this to produce the total number of local leads
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	
	}
	
	function getCount($timeframe,$site="all",$engine="all")
	{
		$sql = "select count(*) count from movingdirectory.quotes q where received like '$timeframe%' ";
		
		if ($site != "all" && $site != "")
		{
			$sql .= " and source like '$site%' ";
			if ($engine != "all")
				$sql .= " and q.source like '$site_$engine%' ";
		}
		else		
		if ($engine != "all")
			$sql .= " and q.source like '%$engine%' and q.source not like '%search%' ";
		
		if (date("Ym") == $timeframe)//if now is during this month.
				$sql .= " and q.received not like '$timeframe".date("d")."%'";
		
		$sql .= " and q.cat_id = 2 ";
		
		if(CPC_DEBUG)
			echo "\n<!--- getCount: $sql --->\n";	
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	}

	function getTotalCost($ts)
	{
		$sql = "select sum(cost) cost from movingdirectory.costperclick where site in ('tm','pm','me') and ts like '$ts%'";
		$rs = new mysql_recordset($sql);				
		$rs->fetch_array();
		$cost = $rs->myarray['cost'];
		$aff_data = getMovingAffiliateCost($ts);
		$cost += $aff_data['total'];
		
		echo "\n<!--- \n".print_r($aff_data,true)." \n--->\n";
		/*
		$wc = 	8*getCount($ts,"all","wc");
		$cost += $wc;
		echo "\n<!--- WiseClick: $wc --->\n";
		$dm = 8*getCount($ts,"all","dm");
		echo "\n<!--- Directory M: $dm --->\n";
		$cost += $dm;//Directory M leads.
		*/
		return $cost;
	}
	
	function getMovingAffiliateCost($ts)
	{
		$sql = "select 
					count(*),
					substring(q.source,5) 'source', 	
					a.ppl,
					count(*)*a.ppl 'cost'
				from movingdirectory.quotes as q join 
				movingdirectory.leadbased_affiliates as a
				on substring(q.source,5) = a.source
				and a.month = '$ts'
				and a.lead_type = 'moving'
				where (q.cat_id = 2 or q.cat_id = 3)
				and q.received like '$ts%' and q.source like 'aff%' 
				group by q.source;				
				";
		$rs = new mysql_recordset($sql);				
		$data = array();
		$data['total'] = 0;
		while($rs->fetch_array())
		{
			$data[$rs->myarray['source']] = $rs->myarray['cost'];
			$data['total'] += $rs->myarray['cost'];
		}
		$rs->close();
		$sql = "select 
					substring(q.source,5) 'source', 	
					count(*)*a.ppl 'cost'
				from movingdirectory.quotes_local as q join 
				movingdirectory.leadbased_affiliates as a
				on substring(q.source,5) = a.source
				and a.month = '$ts'
				and a.lead_type = 'moving'
				where q.received like '$ts%' and q.source like 'aff%' 
				group by q.source;
				";
		$rs = new mysql_recordset($sql);	
		while($rs->fetch_array())
		{
			$data[$rs->myarray['source']] += $rs->myarray['cost'];
			$data['total'] += $rs->myarray['cost'];
		}
		$rs->close();
		
		return $data;
	}
	
	function getLeadCounts($lead_id, $ts)
	{
		/*
		Rob Notes - Looks like the SQL that is generated for this is incorrect.  The part that generates this: $year$month".date("d")."%'" looks like it should be making it so that leads are not pulled for the current day... however, the vars $year and $month are not established so only the date is inserted into the SQL and that is incorrect
		*/
		$sql = "select count(distinct(q.quote_id)) count from movingdirectory.quotes as q ".
				getMovingValidation($lead_id)." where q.received like '$ts%'".
				" and q.lead_ids like '%$lead_id%' and (q.cat_id = 2)";
				
		if (date("Ym") == $ts)//if now is during this month.
				$sql .= " and q.received not like '".date("Ymd")."%'";
		
		$results = array();
		
		$rs = new mysql_recordset($sql);		
		
		$rs->fetch_array();
		echo "\n\n<!-- ACCEPTED LEADS SQL for $lead_id = $sql -->\n\n";
		return $rs->myarray["count"];		
	}

	function getMovingValidation($lead_id)
	{
		if ($lead_id == 1387)
			$sql .= "join movingdirectory.movesirva as v on q.quote_id = v.quote_id and move_id > 0 ";
		else if ($lead_id == 1388)
			$sql .= "join movingdirectory.movebekins as v on q.quote_id = v.quote_id and record_key like 'Y%'";
		else if ($lead_id == 1386)
			$sql = "join movingdirectory.movewheaton as v on q.quote_id = v.quote_id and record_key = 'accepted'";
		else
			$sql = "";
			
		return $sql;
	}
	
	
	//========= new functions
	function getUnapprovedCounts($lead_id, $ts)
	{
		$sql = "select count(distinct(q.quote_id)) count from movingdirectory.quotes as q ".
				getMovingUnapprovedValidation($lead_id)." where q.received like '$ts%'".
				" and q.lead_ids like '%$lead_id%' and (q.cat_id = 2)";
				
		if (date("Ym") == $ts)//if now is during this month.
				$sql .= " and q.received not like '".date("Ymd")."%'";
		
		$results = array();
		
		$rs = new mysql_recordset($sql);		
		
		$rs->fetch_array();
		echo "\n\n<!-- Rejected SQL for $lead_id = $sql -->\n\n";
		return $rs->myarray["count"];		
	}

	function getMovingUnapprovedValidation($lead_id)
	{
		if ($lead_id == 1387)
			$sql .= "join movingdirectory.movesirva as v on q.quote_id = v.quote_id and move_id <= 0 ";
		else if ($lead_id == 1388)
			$sql .= "join movingdirectory.movebekins as v on q.quote_id = v.quote_id and record_key not like 'Y%'";
		else if ($lead_id == 1386)
			$sql = "join movingdirectory.movewheaton as v on q.quote_id = v.quote_id and record_key <> 'accepted'";
		else
			$sql = "";
			
		return $sql;
	}

	
	function getLocalCount($timeframe)
	{
		$sql = "select count(*) count from movingdirectory.quotes_local q where received like '$timeframe%' ";
		
			
		if (strlen($_POST['ts']) != 6)
			$this_month = date("Ym");
		else
			$this_month = $_POST['ts'];
		if ($timeframe == $this_month)
		{
			//remove todays leads, we don't have CPC data for them...
			$sql .= " and received not like '".date("Ymd")."%' ";
		}
		
		
		if(CPC_DEBUG)
			echo "\n<!--- getLocalCount: $sql --->\n";	// <-- need to use a variant of this to produce the total number of local leads
			
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray["count"];		
	
	}

	//=========


	function getPricePerLead($lead_id)
	{
	/* 
	this is the old code that had the lead price hard coded 
		if ($lead_id == 1387)
			return 6.5;
		else if ($lead_id = 1388 || $lead_id = 1386)
			return 3.5;
	*/
		
		//-- this is the new code that pulls the value of the lead price from the DB
		//-- This will pull the marble.cost_per_lead where lead_id = the ID and site_id = 'pm' 
		$get_cpl = "select cost from marble.cost_per_lead where lead_id = '$lead_id' and site_id = 'pm' ";
echo "\n\n<!-- getPricePerLead = $get_cpl -->\n";
		$result_cpl = mysql_query($get_cpl);
		$row_cpl = mysql_fetch_array($result_cpl);
		
echo "<!-- Cost per Lead for $lead_id = $row_cpl[cost]  -->\n\n";
		
		if ( $row_cpl[cost] ) {
			return $row_cpl[cost];
		} else {
			return 0.00;
		}
		
			
	}

	function getCompanies($ts)
	{
		$sql = "select 
					ca.lead_id,
					ca.comp_id,
					c.comp_name
				from 
					movingdirectory.campaign as ca
					join
					movingdirectory.company as c
					on ca.comp_id = c.comp_id
				where 
					site_id = 'pm' and month = '$ts'
					and ca.lead_id not in (1527,1502)";
					
		$results = array();
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
		{
			$results[] = $rs->myarray;
		}		
		return $results;
	}
	
	
	include "skin_top.php";
	if (strlen($_REQUEST['ts']) == 6 && is_numeric($_REQUEST['ts']))
		$ts = $_REQUEST['ts'];
	else
		$ts	= date("Ym");
	if (date("d") == 1  && $ts == date("Ym")) 
	{
		echo "No data yet. :( ";
		exit();
	}
	echo "\n\n<!-- TS = $ts -->\n\n";

	?>
		<Table width="550">
			<tR><td colspan='3'>&nbsp;</td></tR>
			<tR><td colspan='3'><font size="+1"><u><strong>Top Moving Leads</strong></u></font></td></tR>
			<tR><td colspan='3'>&nbsp;</td></tR>
			<tr><td colspan="3">This page includes US moving leads, local and long distance. </td></tr>
			<tr><td colspan="3">&nbsp;</td></tr>
	<?
	

	
	$comp = getCompanies($ts);
	$total_revenue = 0;
	$total_cost = getTotalCost($ts);
	$total_count =  getCount($ts);
	
	$local_count = getLocalCount($ts);
	
	echo "<tr><td colspan='3'><span style=\"font-weight: bold; color: darkgreen; font-size: 16px;\">Total number of leads in Quotes Table for this time frame: $total_count</span></td></tr>\n";
	echo "<tr><td colspan='3'>&nbsp;</td></tr>\n";
	
	foreach($comp as $company)
	{
		$leads = getLeadCounts($company['lead_id'], $ts);
		$ppl = getPricePerLead($company['lead_id']);
		$total = $leads*$ppl;
		$total_revenue += $total;
		
		//-- Bypass known leads that don't report back whether or not a lead was accepted
		if ( $company['lead_id'] == 1583 ||  
			$company['lead_id'] == 1584 || 
			$company['lead_id'] == 1349 || 
			$company['lead_id'] == 1588 || 
			$company['lead_id'] == 1589 || 
			$company['lead_id'] == 1590 ) {
			
			$badleads = "Unknown - No reports on acceptance";
		} else {
			$badleads = getUnapprovedCounts($company['lead_id'], $ts); // my attempt to calculate the rejected leads
		}
		
		echo "<tr><td colspan='3'><strong><u><!-- (".$company['lead_id'] . ")  -->" . $company['comp_name']."</u></strong></td></tr>\n";
		echo "<tr><td>$leads leads x \$".number_format($ppl,2,".",",")." per lead</td><td>&nbsp;</td><td align='right' > =  <strong>Total: \$".number_format($total,2,".",",")."</strong> </td></tr>\n";
		echo "<td colspan='3'>Rejected Leads: $badleads</td></tr><tr><td colspan='3'>&nbsp;</td></tr><tr>";
	}
	
	$vl_short =  getVanLinesCount($ts,"all","all",false);  //-- Pulls Same Zip Local ( destination_state = origin_state )
	$vl_long = getVanLinesCount($ts);  //-- Pulls Different Zip Local ( destination_state != origin_state )
	
	$wc_cost_local = 8 * $vl_short;
	$wc_cost_long = 8 * $vl_long;
/*
	$wc_cost_local = 8*getVanLinesCount($ts,"all","wc"); //-- Local
	$wc_cost_long = 8*getVanLinesCount($ts,"all","wc",false); //-- Long
*/
	
	$wc_cost = $wc_cost_local + $wc_cost_long;
	
	$total_cost += $wc_cost;
	$total_count += $vl_long + $vl_short;
	
	$cpl = number_format(($total_cost / $total_count),2,".",",");
	$vl_revenue = $vl_long*15 + $vl_short*6;
	
	$total_revenue += $vl_revenue;
	
	echo "<tr><td colspan='3'><hr><span style=\"font-weight: bold; color: darkgreen; font-size: 16px;\">Total number of Local Leads in Quotes_Local Table for this time frame: $local_count</span></td></tr>\n";
	echo "<tr><td colspan='3'>&nbsp;</td></tr>\n";

	echo "<tR><td colspan='3'><strong><u>Vanlines</u></strong></td></tR>\n";
	echo "<tR><td>Long Distance: $vl_long  leads x \$15.00 per lead</td><td>&nbsp;</td><td align='right' > =  <strong>\$".number_format(($vl_long*15),2,".",",")."</strong> </td></tr>\n";	
	echo "<tR><td>Local: $vl_short  leads x \$6.00 per lead</td><td>&nbsp;</td><td align='right' > =  <strong>\$".number_format(($vl_short*6),2,".",",")."</strong> </td></tr>\n";
	echo "<tR><td colspan='3' align='right'><strong>Total: \$".number_format($vl_revenue,2,".",",")."</strong></td></tR>";
	echo "<tR><td colspan='3'>&nbsp;</td></tR>";
	
	echo "<tR><td colspan='3'><hr>&nbsp;</td></tR>";
	echo "<tR><td colspan='2'>&nbsp;</td><td><hr width='70%'></td></tR>";
	echo "<tR><td colspan='2'>&nbsp;</td><td align='right' ><strong>Grand Total: \$".number_format($total_revenue,2,".",",")."</strong></td></tr>\n";
	echo "<tR><td colspan='2'>&nbsp;</td><td align='right' ><strong>Cost: (\$$cpl x $total_count leads): \$".number_format($total_cost,2,".",",")."</strong></td></tr>\n";
	$profit = $total_revenue - $total_cost;
	echo "<tR><td colspan='2'>&nbsp;</td><td align='right' ><strong>Total Profit: \$".number_format($profit,2,".",",")."</strong></td></tr>\n";
	echo "</table>";
	
	
	echo "<p>vl_long = $vl_long</p>";
	echo "<p>vl_short = $vl_short</p>";
	echo "<p>Vanlines Local = $wc_cost_local <-- this wan't being calculated correctly, nor was it a part of the above total (and being calculated at $8 CPL)</p>";
	echo "<p>Vanlines Long = $wc_cost_long</p>";
	include "skin_bottom.php";
?>