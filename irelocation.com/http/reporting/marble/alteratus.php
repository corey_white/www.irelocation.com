<?
/* 
************************FILE INFORMATION**********************
* File Name:  alteratus.php
**********************************************************************
* Description:  This is the form for updating the status and lead goals for a campaign
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	2/28/08 - added this comment block
**********************************************************************
*/
	session_start();
	include "skin_top.php";
	
	$comp_id = $_REQUEST['comp_id'];
	
	$sql = "select ca.*,c.comp_name,r.lead_email as 'email' from marble.campaign as ca join ".
			" movingdirectory.company as c on ca.comp_id = c.comp_id join ".
			" marble.rules as r on r.lead_id = ca.lead_id where month = ".
			" '".date("Ym")."' and ca.comp_id = $comp_id and ca.site_id = 'atus'".
			" and r.site_id = 'atus' ";
	$rs = new mysql_recordset($sql);
	
	$rs->fetch_array();
	
	extract($rs->myarray);	
	?>
	<div class="filterfont">
		Auto-Transport.us Lead Campaign:<br/>
		<br/>
		<form action="alter_atus.php" method="post" name="leads">
			<input type="hidden" name="lead_id" value="<?= $lead_id ?>"/>
			<input type="hidden" name="comp_id" value="<?= $comp_id ?>"/>
			<input type="hidden" name="function" value="updated"/>
			<input type="hidden" name="cur_monthly_goal" value="<?= $monthly_goal ?>"/>
			<input type="hidden" name="cur_email" value="<?= $email ?>"/>
			<input type="hidden" name="cur_temp_goal" value="<?= $temp_goal ?>"/>
			<table class="filterfont">
				<? if ($_REQUEST['status'] != "") { 
					echo "<tr><td colspan='2'>";
					switch($_REQUEST['status'])
					{
						case "updated":
							echo "the company has been updated.";
							break;
						case "updated-now":
							echo "the company has been updated.<br/>The mapping was re-run.";
							break;
						case "activated":
							echo "the status of the company has been changed.";
							break;
						case "activated-now":
							echo "the status of the company has been changed.<br/>The mapping was re-run.";
							break;
						case "invalidfunction":
							echo "Something broke.. tell dave.";
							break;
					}
					echo "</td></tr>";
				?>
				<tr><td colspan="2">&nbsp;</td></tr>				
				<? } ?>
				<tr>
				<tr>
					<td>Company:</td>
					<td><?= $comp_name ?></td>
				</tr>
				<tr>
					<td>Current Lead Quota:</td>
					<td><?= $monthly_goal ?>
						<?
							if ($temp_goal != 0)
								echo "<font color='#00FF00'>( + $temp_goal )</font>";
						?>
	
					</td>
				</tr>	
				<tr>
					<td>Email:</td>
					<td><?= $email ?></td>
				</tr>	
				<tr>
					<td>Status:</td>
					<td><?= ($active==1)?"Active":"Inactive" ?></td>
				</tr>
				<tr><td colspan="2"><hr/></td></tr>			
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td>Update Email Address:</td>
					<td><input type="text" value="<?= $email ?>" name="email" /></td>
				</tr>
				<tr>
					<td valign="top">
						Set new Quota:
					</td>
					<td>
						<input type="text" value="<?= $monthly_goal ?>" name="monthly_goal" />
					</td>
				</tr>
				<tr>
					<td valign="top">
						Set Temp Goal
					</td>
					<td>
						<input type="text" value="<?= $temp_goal ?>" name="temp_goal" />
					</td>
				</tr>
				<!---
				<tr>
					<td valign="top">
						Non-Competes
					</td>
					<td>
						<input type="text" value="<?= $non_compete ?>" name="non_compete" />
					</td>
				</tr>
				--->
				<tr>
					<td valign="top">					
						For: 
					</td>
					<td>
						<input type="radio" name="month"					
						 value="<?= date("Ym") ?>" checked="checked"/>This Month
						<br/>
						 <input type="radio" name="month"
					 	value="<?= date("Ym",strtotime("+1 month")) ?>"/>Next Month
					</td>
				</tr>
				<tr>
					<td valign="top">
						Make This Change: 
					</td>
					<td>
						<input type="radio" name="when"
					 value="tonight" checked="checked"/>Tonight at Midnight
					 <br/>
				 <input type="radio" name="when"
				 	value="now"/>Right Now
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						<input type="submit" value="udpate" />
					</td>
				</tr>
				<tr><td colspan="2"><hr/></td></tr>
			</table>
		</form>
		<br/>
		<form action="alter_atus.php" method="post" name="leads">
			<input type="hidden" name="comp_id" value="<?= $comp_id ?>"/>
			<input type="hidden" name="function" value="activate"/>
			<table class="filterfont">				
				<tr>
					<td valign="top">
						Status: 
					</td>
					<td>
						<input type="radio" value="0" 
							<? if($active==0) echo "checked"; ?> name="active" />Inactive
						<input type="radio" value="1" 
							<? if($active==1) echo "checked"; ?> name="active" />Active
					</td>
				</tr>
				<tr>
					<td valign="top">
						Make This Change: 
					</td>
					<td>
						<input type="radio" name="when"
					 value="tonight" checked="checked"/>Tonight at Midnight
					 <br/>
				 <input type="radio" name="when"
				 	value="now"/>Right Now
					</td>
				</tr>
				<tr>
					<td colspan="2"> 
						<input type="submit" value="udpate" />
					</td>
				</tr>
			</table>
		</form>
	</div>
	
<? 
	include "skin_bottom.php";
?>