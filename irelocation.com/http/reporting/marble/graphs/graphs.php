<?
	class GraphFunctions
	{
		var $timeframe;
		var $site;
		var $engine;
		var $campaign;
			
		function GraphFunctions($timeframe,$engine="all",$site="all")
		{
			$this->timeframe = $timeframe;
			$this->site = $site;
			$this->engine = $engine;
			echo "<!---\n GraphFunctions(".$timeframe.",".$engine.",".$site.");\n --->";
		}		
	
		function showGraph($timeframe="x",$engine="x",$site="x")
		{
			echo "<!---\n showGraph(".$timeframe.",".$engine.",".$site.");\n --->";
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($timeframe=="x") $timeframe = date("Ym");
			if ($engine=="x") $engine = $this->engine;
			if ($engine=="x") $engine = "all";
			if ($site=="x") $site = $this->site;
			$counts = $this->getCountsByDay($timeframe,$engine,$site);
			echo "\n<!--- Counts: ".print_r($counts,true)." --->\n";
			$costs = $this->getCostsByDay($timeframe,$engine,$site);
			if (!is_array($costs))
			{			
				$cost = $costs;
				$costs = array();
				$days = array_keys($counts);				
				foreach($days as $day)
				{
					$count = $counts[$day]['count'];
					$costs[$day]['cost'] = $cost*$count;
				}
			}
			echo "\n<!--- Costs: ".print_r($costs,true)." --->\n";
			
			
			

			echo "<!--- graph.showGraph($timeframe,$engine,$site) --->\n";
			$max_cost = 0;
			foreach($costs as $day => $values)
			{
				if ($counts[$day]["count"] > 0)			
				$max_cost = max($max_cost, ($values["cost"]/$counts[$day]["count"]));			
			}
			$max_cost = ceil($max_cost);
			
			echo "<!--- MAX: $max_cost --->";
			?>
			<table border="1" cellpadding="0" cellspacing="0"><Tr><td>
			<table cellpadding="0" cellspacing="0" border="1">
			<TR height="20"><td width='25'  valign="bottom">$<?= ($max_cost+1) ?></td><td rowspan="<?= intval($max_cost+2) ?>" valign="bottom">
			<?
			
			//-- Determine whether we are going to scale the graph
			if ( $max_cost > 100 ) {
				$scale_factor = 10;
			} elseif ( $max_cost > 80 ) {
				$scale_factor = 8;
			} elseif ( $max_cost > 60 ) {
				$scale_factor = 6;
			} elseif ( $max_cost > 40 ) {
				$scale_factor = 4;
			} elseif ( $max_cost > 20 ) {
				$scale_factor = 2;
			} else {
				$scale_factor = 1;
			}
			
			echo "<Table cellspacing='0' cellpadding='0' border='0'><tr valign='bottom'>";
			foreach($costs as $day => $values)
			{
			
				$counts[$day]["cost"] = $values["cost"];
				
				if ($counts[$day]["count"] > 0)			
				$counts[$day]["cpl"] = number_format(($values["cost"]/$counts[$day]["count"]),1,".",",");
				else
				$counts[$day]["cpl"] = number_format($values["cost"],1,".",",");
				
				//-- Added the below to see if we could 1) control the scaling and 2) limit out-of-whack values
				$bar_height = ($counts[$day]["cpl"]*22) / $scale_factor;
	echo "\n\n<!-- bar_height = $bar_height -->\n\n";
	/*
				if ( $bar_height > 100 ) {
					$bar_height = 100;
				} 
	*/
				
				echo "<td align='center'>";
	#			echo "<img src='bar.php?width=25&height=".($counts[$day]["cpl"]*22)."&color=0,255,0' "; // original
				echo "<img src='bar.php?width=25&height=$bar_height&color=0,255,0' ";
				echo " title='\$".$counts[$day]["cpl"]."0' />";
				echo "<br/>".intval($day)."</td>";
			}
			echo "</tr></table>";
			?>
			</td></TR>
			<?
/*
			for($x = ($max_cost); $x > 0; $x--)
			echo "<TR height='20'><td valign='bottom'>\$$x</td></tr>";
*/
			
			$x = ($max_cost);
			while($x > 0) {
				echo "<TR height='20'><td valign='bottom'>\$$x</td></tr>";
				$x = $x-$scale_factor;
			}
			?>
			<TR height="20"><td valign="bottom">&nbsp;</td></tr>
			</table>
			</td></Tr></table> <?
		}
		
	}
?>