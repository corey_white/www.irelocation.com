<?
	include_once "graphs.php";

	

	class RealEstateGraph extends GraphFunctions
	{
		function RealEstateGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- RealEstateGraph($timeframe,$engine,$site) --->\n";
			$this->campaign = "realestate";
			$this->table = "movecomp.leads_prd";
			$this->sqlfilter = "";
		}		
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$revenue = "??";
			$cost = $this->getCost();
			$count =  $this->getCount();
			
			if ($count > "0") {
				$cpl = $cost / $count;
			} else {
				$cpl = $cost;
			}
			$cpl = number_format($cpl,2,".",",");
			
			return array($revenue,$cpl,$cost);
		}
		
		function sqlCall($sql)
		{
			return new movecompanion($sql);
		}
		
		function getSqlFilter($timeframe,$engine,$site,$include_today)
		{
			if ($this->sqlfilter == "")
			{
				$year = substr($timeframe,0,4);
				$month = substr($timeframe,4,2);
				
				$mctime = "$year-$month";
				
			
				$sql = " leadDate like '".$mctime."%' ";
				if ($engine != "all")
					$sql .= " and campaign like '%".$engine."' ";
					
				if ($site != "all")
					$sql .= " and campaign like '".$site."%' ";
					
				if (!$include_today)
				{
					//exclude todays leads
					$today = date("Y-m-d");
					$sql .=  " and leadDate not like '".$today."%' ";
				}
				$this->sqlfilter = $sql;
			}
			return $this->sqlfilter;			
		}	
		
		function getTotalLeadsToday($timeframe="x",$engine="x",$site="x",$today=true)
		{
			$total_count = "select count(*) count from ".$this->table." q where ".
					$this->getSqlFilter($timeframe,$engine,$site,$today).
					" and leadDate like '".date("Y-m-d")."%' ";
			echo "<!--- total($timeframe) $total_count  --->";
			$rs = $this->sqlCall($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			return $total_leads;	
		}
		
		function getTotalLeads($timeframe="x",$engine="x",$site="x",$today=true)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$total_count = "select count(*) count from ".$this->table." q where ".
					$this->getSqlFilter($timeframe,$engine,$site,$today).";";
			echo "<!--- total($timeframe) $total_count  --->";
			$rs = $this->sqlCall($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			return $total_leads;
		}
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			$sql = "select count(*) count from ".$this->table." q where ".
					$this->getSqlFilter($timeframe,$engine,$site,true);
							
			echo "\n<!--- $sql --->\n";	
						
			$rs = $this->sqlCall($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];		
		}
		
	function printGridHeader($timeframe)
	{
		echo "<table border='1'>";
		echo "<tr class='header' ><td> &nbsp; </td>".
				"<th align='left' class='white' >Company</th>";
		
		$days = getDays($timeframe);
		
		for($i = 1; $i <= $days; $i++)
		{
			echo "<th class='white'>$i</th>";
		}
				
		echo "<th class='white'>Total</th></tr>";
		
		
		echo "<tr class='off'><td class='$style' > - </td>";
		echo "<th align='left' color='#000000'>Site Total</th>";	
		
			
		$day_count = 1;
		$leads = $this->getCountsByDay();
		$day_array = array_keys($leads);
		
		foreach($day_array as $day)
		{
			$count = $leads[$day]['count'];
			
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
	
			if (intval($day) == $day_count)//
				echo "<th class='off'>$count</th>";
			$total += $count;		
			$day_count++;
		}
		
		if ($day_count <= $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
		}
		echo "<th class='off' align='right' >$total</th></tr>";	
		
		
	}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select sum(cost) 'cost' from movingdirectory.costperclick ".
					" where ts like '".$timeframe."%' ";
					
			if ($engine != "all")
				$sql .= " and search_engine = '".$engine."' ";
				
			if ($site != "all")
				$sql .= " and site = '".$site."' ";
			else
				$sql .= " and site in ('fmar','irelo') ";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["cost"];	
		}
		
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$filter = $this->getSqlFilter($timeframe,$engine,$site,false);
			$sql = "select count(*) count, substring(leadDate,9,2) 'day' from ".
					$this->table." q where ".$filter." group by "
					." substring(leadDate,9,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = $this->sqlCall($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
			}
			return $data;	
		}
		
		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$data = array();
			
			$sql = "select substring(ts,7,2) 'day',sum(cost) 'cost' from movingdirectory.costperclick ".
					"where site in ('irelo','fmar') and ts like '".$timeframe."%' group by ts;";
			$rs = new mysql_recordset($sql);
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
			}	
			return $data;		
		}
	}
	

?>