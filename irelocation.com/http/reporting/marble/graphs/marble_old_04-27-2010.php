<?
	include "graphs.php";
	define(MARBLE_COMPS_PER_LEAD,7);
	define(MARBLE_LEAD_PRICE,1);
	
	class MarbleGraph extends GraphFunctions
	{			
		var $campaign;
		var $lead_prices;
		var $company_totals;
		var $revenue;
			
		function MarbleGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- MarbleGrah($timeframe,$engine,$site) --->\n";
			$this->campaign = "auto";
			$this->sqlfilter = "";
			
		}		
	
		function getCompanyTotals($timeframe="x",$engine="x",$site="x",$today=true)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$total_count = "select ca.lead_id, count(*) 'count' from 
marble.auto_quotes as q join marble.campaign as ca on locate(ca.lead_id,q.lead_ids) > 0
where ca.month = '$timeframe' and ca.site_id = '".$this->campaign."' and ".$this->getSqlFilter($timeframe,$engine,$site,$today)." group by ca.lead_id; ";
			echo "<!--- total_comp_sql:\n$total_count --->";
			$rs = new mysql_recordset($total_count);
			$this->company_totals = array();
			while($rs->fetch_array())
				$this->company_totals[$rs->myarray['lead_id']] = $rs->myarray['count'];						
		}
	
		//loadLeadPrices() and loadCompanyArray() have to be called first.
		function calculateRevenue()
		{
			$lead_ids = array_keys($this->company_totals);
			$revenue = 0;
			foreach($lead_ids as $li)
			{
				$cpl = $this->lead_prices[$li];
				if ($cpl == "" || $cpl == 0) $cpl = MARBLE_LEAD_PRICE;
				$revenue += $cpl * $this->company_totals[$li];
			}
			$this->revenue = $revenue;
		}
	
		//$lead_prices[lead_id][quote_type] = <price>
		
		//loads all prices from database..
		//getting away from hard coding the values.
		function loadLeadPrices()
		{
			$site_id = "auto";
			
			$sql = "select lead_id,cost from marble.cost_per_lead where site_id = '$site_id' ";
			echo "<!--- lead_prices_sql:\n$sql --->";
			$rs = new mysql_recordset($sql);
			$this->lead_prices = array();
			while($rs->fetch_array())
				$this->lead_prices[$rs->myarray['lead_id']] = $rs->myarray['cost'];																		
		}

		function loadCompanyArray()
		{
			$sql = "Select co.comp_name,ca.* from marble.campaign as ca join ".
			" marble.companies as co on co.comp_id = ca.comp_id where ca.site_id = 'auto' ".
			" and ca.month = '".$this->timeframe."'";			
			$rs = new mysql_recordset($sql );
			while ($rs->fetch_array())
			{
				$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
				$companies[$rs->myarray["lead_id"]] = $rs->myarray;
			}
			return $comapnies;
		}

		function getTotalLeads($timeframe="x",$engine="x",$site="x",$today=true)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$total_count = "select count(*) count from marble.auto_quotes q where ".
					" processed and ".$this->getSqlFilter($timeframe,$engine,$site,$today).";";
			echo "<!--- total($timeframe) $total_count  --->";
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			return $total_leads;
		}

		function getGridSql($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
		
			$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal+c.temp_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join marble.companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'auto'
				and ".$this->getSqlFilter($timeframe,$this->engine,$this->site)." 
				and c.month = '".$timeframe."'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				comp_name,day ";
		
			return $sql;
		}

		function getSqlFilter($timeframe,$engine,$site,$include_today=true)
		{
			if ($this->sqlfilter == "" || strlen($timeframe) == 8)
			{
				$sql = " received like '".$timeframe."%' ";
				$sql .= " and campaign = 'auto' ";			
				
				if ($site != "all")
					$sql .= " and source like '$site%' ";		
							
				if ($engine != "all" && $engine != "organic")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
				else if ($engine == "organic")
				{
					$sql .= " and ((length(source) < 4) OR (source like '%_search_%') ".
									"OR (source like '%_email')) ";
				}
				
				if (!$include_today)
				{
					$sql .= " and received not like '".date("Ymd")."%' ";
				}
				
				if (strlen($timeframe) != 8)
					$this->sqlfilter = $sql;
				else
					return $sql;
			}
			
			
			return $this->sqlfilter;
		}
		
		function test($selected,$value)
		{
			if ($selected == $value) return " selected ";
			else return " ";
		}
			
		
		function printJavascript()
		{
		
		}
		
		
		function printFilter($timeframe="x",$sel_engine="all",$sel_site="all")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			
			$sql = "select distinct(source) 'source' ,count(*) 'leads' from marble.auto_quotes ".
					"where campaign = 'auto' and received like '".$timeframe."%' group by source ".
					"order by source";
			echo "\n\n<!-- printFilter SQL: $sql -->\n\n";
			$rs = new mysql_recordset($sql);
			
			$sites = array();
			$affiliates = array();
			$engines = array();
			$organics = array();
			
			while ($rs->fetch_array())
			{
				extract($rs->myarray);
				if (substr($source,0,3) == "aff")
					$affiliates[substr($source,4)] = $leads;
				else
				{
					if ((substr_count($source,"_") == 1 || substr_count($source,"local") == 1 || substr_count($source,"cs_") == 1)
						&& (substr_count($source,"_mmc") == 0) && substr_count($source,"_cs") == 0
						&& substr_count($source,"_1md") == 0 && substr($source,-2,2) != "me"
						&& substr($source,-2,2) != "pm" && substr($source,0,3) != "tmc"
						
						&& substr($source,-2,2) != "tm" )
					{
						list($site,$engine) = split("_",$source,2);
						if ($engine != "")
							$engines[$engine] += $leads;//double counting?
						$sites[$site] += $leads;
					}
					else
						$organics[$source] += $leads;
				}
			}			
			$rs->close();
						
			echo "<!--- organic: ".print_r($organics,true)." --->";
			
			echo "<table>";
			
			echo "<tr><td class='filterfont'>Month:</td><td>";
			printPeriodDropDown('ts',$_POST['ts']);
			echo "</td></tr><tr><td class='filterfont'>Sites:</td><td>";
			echo "<select name='site'>\n";
			echo "<option value='all'".$this->test("all",$sel_site).">All</option>\n";
			if (count($affiliates) > 0)
				echo "<option value='aff'".$this->test('aff',$sel_site).">affiliates</option>\n";
			foreach($sites as $site=>$count)
				echo "<option value='$site'".$this->test($site,$sel_site).">$site</option>\n";			
			echo "</select>";
			echo "</td></tr><tr><td class='filterfont'>Sources:</td><td>";
			echo "<select name='engine'>\n";
				echo "<option value='all'".$this->test("all",$sel_engine).">All</option>\n";	
				if (count($organics) > 0)
					echo "<option value='organic'".$this->test("organic",$sel_engine).">organic</option>\n";
				echo "<optgroup label='Pay per Click'>\n";
				foreach($engines as $engine=>$count)
					echo "<option value='$engine'".$this->test($engine,$sel_engine).">$engine</option>\n";
				echo "</optgroup>\n";
				echo "<optgroup label='Affiliates'>\n";
				foreach($affiliates as $aff=>$count)
					echo "<option value='$aff'".$this->test($aff,$sel_engine).">$aff</option>\n";
				echo "</optgroup>\n";	
			echo "</select>\n";	
			echo "</td></tr>";
			echo "<tr><td class='filterfont'>Submit:</td><td><input type='button' ".
				 "onClick='javascript:checkCombo(document.sitefilter.site,document.sitefilter.engine);' ".
				 " value='Submit' ></td></tr></table>";
		}
		
		
		
		function  getAffiliateCount($timeframe,$includeToday)
		{
			$sql = "select count(*) 'count' From marble.auto_quotes where source like 'aff%' ".
					" and received like '$timeframe%' ";
			if ($includeToday)
				$sql .= " and received not like '".date("Ymd")."%' ";
			
			$rs = new mysql_recordset($sql);
			
			$rs->fetch_array();
			return $rs->myarray['count'];
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$count = $this->getCount($timeframe,$engine,$site);
				
			if ($site != "aff" && $site != "all")
				$cost =  $this->getCost($timeframe,$engine,$site);				
			else if ($site == "aff")
			{				
				if ($engine == "all") $engine = "*";			
				$aff = getAffiliateCost("auto",$timeframe,$engine);				
				echo "\n<!--- aff cost: $aff $engine --->\n";
				$cost += $aff;
			}	
			else if ($site == "all")
			{
				$cost =  $this->getCost($timeframe,$engine,$site);
				if ($engine == "all")
				{					
					$aff = getAffiliateCost("auto",$timeframe,"*");				
					echo "\n<!--- aff cost: $aff $engine --->\n";
					$cost += $aff;
				}
			}
	
			$cap = get("topauto_revenue_cap");

			echo "\n<!--- CAP: $cap --->";
			echo "\n<!--- $count ".MARBLE_COMPS_PER_LEAD." --->\n";
			echo "\n<!--- count*MARBLE_COMPS_PER_LEAD: ".($count*MARBLE_COMPS_PER_LEAD)." --->";

			$this->loadLeadPrices();			
			//$timeframe="x",$engine="x",$site="x",$today=true
			$this->getCompanyTotals($timeframe,$engine,$site,false);
			$this->calculateRevenue();
			$revenue = $this->revenue;
			//$revenue = $count*MARBLE_COMPS_PER_LEAD*MARBLE_LEAD_PRICE;				
				
			if($revenue > $cap)
				$revenue = "<font color='green'>Revenue Cap of \$".number_format($cap,2,".",",")." hit.</font>";
			else
				$revenue = "<span title='Revenue Cap: \$".$cap."'>".number_format($revenue,2,".",",")."</span>";				
			
			//we didnt switch to 7 companies until the 5th of march.
			
			//ONLY FOR MARCH.		
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > (MARBLE_COMPS_PER_LEAD*.9))
				$cpl = "<font color='red'>$cpl</font>";
			else if (floatval($cpl) > (MARBLE_COMPS_PER_LEAD*.75))
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			if ($engine == "organic")
				return 0;
		
			$sql = "select sum(cost) cost from movingdirectory.costperclick where ts like '$timeframe%' ";
			
			if ($engine != "google_local")
			{
				if ($site != 'all' && $site != "aff")
					$sql .= " and site = '$site' ";
				else if ($site == "all")
					$sql .= " and site in ('asc-local','csq','cq','cs','mmc','asc') ";
					
				if ($engine == "google" ||  $engine == 'overture' || $engine == 'msn')
					$sql .= " and search_engine like '$engine%' ";
			}
			else if ($engine == "google_local")
			{
				if ($site != "all")
					$sql .= " and site = '".$site."-local' and search_engine = 'google' ";	
				else
					$sql .= " and site like '%-local' and search_engine = 'google' ".
							" and (site like 'cs%' or site like 'asc%' or site like 'mmc%') ";
			}					
			if(CPC_DEBUG)
				echo "\n<!---Cost: $sql --->\n";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			
			$cost = $rs->myarray["cost"];		
			return $cost;		
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			$sql = "select count(*) count from marble.auto_quotes q where ".
					$this->getSqlFilter($timeframe,$engine,$site);
			
			if ($timeframe == date("Ym"))
					$sql .= " and received not like '".date("Ymd")."%' ";
			
			echo "\n<!--- $sql --->\n";	
						
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];		
		}
	
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$filter = $this->getSqlFilter($timeframe,$engine,$site);
			$sql = "select count(*) count, substring(received,7,2) 'day' from ".
					"marble.auto_quotes q where ".$filter." group by ".
					" substring(received,7,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
			}
			return $data;	
		}
		
		
		
		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "<!--- COST BY DAY: site: $site engine: $engine --->";
			
			$data = array();
			
			if ($engine == "all")
				$aff_engine = "*";
			else
				$aff_engine = $engine;
				
			$affcost = getAffiliateCostByDay("auto",$timeframe,$aff_engine);
		
			if ($engine == "organic")
			{
				for($i = 1; $i < date("d"); $i++)
				{
					if ($i < 10) $d = "0$i";
					else $d = $i;
					$data[$d]['cost'] = 0;
				}
				//ORGANIC NO COST!
				return $data;
			}	
			
			if ($site != "all" && $site != "aff")//particular site.
			{
				echo "<!--- site: $site --->";
				$sql = "select sum(cost) cost , substring(ts,7) 'day' from ".
						"movingdirectory.costperclick where ts like '$timeframe%' ";
												
				if ($engine != "google_local")
				{
					$sql .= " and site = '$site' ";
									
					if ($engine != "all")
						$sql .= " and search_engine like '".$engine."%' ";
				}
				else if ($engine == "google_local")
				{
					$sql .= " and site = '".$site."-local' and search_engine = 'google' ";
				
				}				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}				
			}
			else if ($site == "aff")
			{
				echo "<!--- site: aff --->";
				$days = array_keys($affcost);
				foreach($days as $day)
				{
					$data[$day]['cost'] += $affcost[$day];
				}
			}
			else//both
			{
				echo "<!--- COST BY DAY site: all engine: $engine --->";
				
				$sql = "select sum(cost) cost , substring(ts,7) 'day' from ".
						"movingdirectory.costperclick where ts like '$timeframe%' ";
								
				//NOTE: only because its the only				
				// site that feesd this system				
						
				if ($engine != "google_local")
				{
					$sql .= " and site in ('asc-local','csq','cq','cs','mmc','asc') ";
									
					if ($engine != "all")
						$sql .= " and search_engine like '".$engine."%' ";
				}
				else if ($engine == "google_local")
				{
					$sql .= " and site like '%-local' and search_engine = 'google' ".
							" and (site like 'cs%' or site like 'asc%' or site like 'mmc%') ";				
				}						
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}		
				$rs->close();
				$days = array_keys($affcost);
				foreach($days as $day)
				{
					$data[$day]['cost'] += $affcost[$day];
				}
			}	
		
			return $data;		
		}				
	}
?>