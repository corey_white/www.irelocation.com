<?
	include "graphs.php";
	define(AFF_CAT_NAME,"usalarm.security");
	
	class MatrixSecurityGraph extends GraphFunctions
	{			
		var $campaign;
			
		var $lead_prices = array();
		//$lead_prices[lead_id][quote_type] = <price>
		
		//loads all prices from database..
		//getting away from hard coding the values.
		function loadLeadPrices()
		{
			$site_id = CAMPAIGN;
			$this_month = date("Ym");
			$sql = " select cpl.lead_id,cpl.cost, cpl.extra from marble.cost_per_lead cpl ". 
					" join movingdirectory.campaign ca on ca.lead_id = cpl.lead_id and " .
					" ca.site_id = '$site_id' where cpl.site_id = '$site_id' and " .
					" month = '$this_month' ; "; 
			echo "<!--- CPL SQL: $sql --->";
			$rs = new mysql_recordset($sql);
			$this->lead_prices = array();
			$this->companies = array();
			while($rs->fetch_array())
			{
				$this->lead_prices[$rs->myarray['lead_id']][$rs->myarray['extra']] = $rs->myarray['cost'];
				$this->companies[] = $rs->myarray['lead_id'];
			}												
				
			//echo "<!--- \n".print_r($this->lead_prices,true)."\n --->";
		}
		
			
		function MatrixSecurityGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- SecurityGraph($timeframe,$engine,$site) --->\n";
			$this->campaign = "tsc";
			//call it so its ready.
			$this->loadLeadPrices();
		}		
		
		function printFilter($timeframe="x",$sel_engine="all",$sel_site="all")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$table = "irelocation.leads_security";
			$sql = "select distinct(source) 'source' ,count(*) 'leads' from $table ".
					"where received like '".$timeframe."%'".
					" and campaign = '".CAMPAIGN."' group by source order by source";
			echo "\n<!--- filter: $sql --->\n";	
			$rs = new mysql_recordset($sql);
			
			$sites = array();
			$affiliates = array();
			$engines = array();
			$organics = array();
			
			while ($rs->fetch_array())
			{
				extract($rs->myarray);
				if (substr($source,0,3) == "aff")
				{				
					if (strpos($source,"bpm") > 0)
					{
						$affiliates['bpm'] += $leads;
					}
					else
						$affiliates[substr($source,4)] = $leads;
				}
				else
				{
					if (substr_count($source,"_google") == 1 || substr_count($source,"_overture") || substr_count($source,"_callcenter") ||
						substr_count($source,"_msn") || substr_count($source,"_citys"))
					{
						list($site,$engine) = split("_",$source,2);
						if ($engine != "")
							$engines[$engine] += $leads;//double counting?
						$sites[$site] += $leads;
					}
					else
						$organics[$source] += $leads;
				}
			}			
			$rs->close();
						
			echo "<!--- organic: ".print_r($organics,true)." --->";
			
			echo "<table>";
			
			echo "<tr><td class='filterfont'>Month:</td><td>";
			printPeriodDropDown('ts',$_REQUEST['ts'],7);
			
			//-- SITES
			echo "</td></tr><tr><td class='filterfont'>Sites:</td><td>";
			echo "<select name='site'>\n";
			echo "<option value=\"all\"".$this->test("all",$sel_site).">All</option>\n";
			if (count($affiliates) > 0)
				echo "<option value=\"aff\"".$this->test('aff',$sel_site).">affiliates</option>\n";
			foreach($sites as $site=>$count)
				echo "<option value=\"$site\"".$this->test($site,$sel_site).">$site</option>\n";			
			echo "</select>";
			
			//-- SOURCES 
			echo "</td></tr><tr><td class='filterfont'>Sources:</td><td>";
			echo "<select name='engine'>\n";
				echo "<option value='all'".$this->test("all",$sel_engine).">All</option>\n";	
				if (count($organics) > 0)
					echo "<option value='organic'".$this->test("organic",$sel_engine).">organic</option>\n";
				echo "<optgroup label='Pay per Click'>\n";
				foreach($engines as $engine=>$count)
					echo "<option value='$engine'".$this->test($engine,$sel_engine).">$engine</option>\n";
				echo "</optgroup>\n";
				echo "<optgroup label='Affiliates'>\n";
				foreach($affiliates as $aff=>$count)
					echo "<option value='$aff'".$this->test($aff,$sel_engine).">$aff</option>\n";
				echo "</optgroup>\n";	
			echo "</select>\n";	
			
			//-- QUOTE TYPE 
			echo "</td></tr>";
			echo "<tr  class='filterfont'><td>Quote Type:</td>";
			echo "<td><select name='quote_type'>";
				echo "<option value='all' ".(($_POST['quote_type']==""||$_POST['quote_type']=="all")?" selected ":"").")>All</option>\n";
				echo "<option value='res' ".(($_POST['quote_type']=="res")?" selected ":"").">Residential</option>\n";
				echo "<option value='com' ".(($_POST['quote_type']=="com")?" selected ":"").">Commercial</option>\n";
			echo "</select></td></tr>";
			echo "<tr><td class='filterfont'>Submit:</td><td><input type='button' ".
				 "onClick='javascript:checkCombo(document.sitefilter.site,document.sitefilter.engine);' ".
				 " value='Submit' ></td></tr></table>";
		}
		
		function test($selected,$value)
		{
			$value - strtolower($value);
			$selected - strtolower($selected);
			if ($selected == $value) return " selected ";
			else return " ";
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			if ($engine == "dm" || $engine == "wc")
			{
				$_POST['site'] = 'aff';
				$site = "aff";
			}
			
			if ( eregi("clkbth",$engine ) ) {
				$engine = "clkbth";
			} 
			
			$count = $this->getCount($timeframe,$engine,$site);
			if ($site != "aff")
				$cost = $this->getCost($timeframe,$engine,$site);
			else //for now this works, cause they're both 4 bucks a lead.
			{
				$sql = "select ppl from movingdirectory.leadbased_affiliates ".
						" where lead_type = 'security' and source = '$engine' ".
						" and month = '".substr($timeframe,0,6)."' ";
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				$cpl = $rs->myarray['ppl'];
				$cost = $count*$cpl;
			}	
			
			
			$revenue = $this->getTotalLeadRevenue(substr($timeframe,4),substr($timeframe,0,4),$engine,$site);
			$revenue = number_format($revenue ,2,".",",");
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > 28.00)
				$cpl = "<font color='red'>$cpl</font>";
			else if (floatval($cpl) > 24.00)
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		/*----*/
		
		function getTotalLeadRevenue($month,$year,$engine,$site)
		{
			$revenue = $this->getLeadRevenue($month,$year,$engine,$site);

			 /*
			 	The big query at the bottom will give you a good breakout of all the leasd
			 	sent to the companies in this campaign broken out by company count.
			 	a sum of the last column for each row will give you the total UNVALIDATED 
			 	revenue.
			 	 			  
			    --select lead_id from movingdirectory.campaign where site_id = 'usalarm';
				--select * from irelocation.leads_security where campaign = 'usalarm';
				
				select 
					ca.lead_id,
					count(*) 'count',
					length(replace(s.lead_ids,'|',''))/4 'comp_count',
					cpl.cost 'lead_price',
					count(*) * cpl.cost 'sub_total_cost'
				from 
					irelocation.leads_security as s 
					join movingdirectory.campaign as ca
					on LOCATE(ca.lead_id,s.lead_ids) > 0	
					and ca.site_id = s.campaign
					join marble.cost_per_lead as cpl
					on ca.lead_id = cpl.lead_id and cpl.extra*4 = length(replace(lead_ids,'|',''))
				where 
					s.campaign = 'usalarm'
					and ca.month = '200801'
				group by ca.lead_id,length(replace(s.lead_ids,'|',''))
			  
			  
			  */
			 
			 return $revenue;
		}
		
		function getLeadRevenue($month,$year,$engine,$site)
		{			
			$sql = "select 
						ca.lead_id, 
						cpl.extra, 
						cost, 
						count(*),
						cost*count(*) 'sub_total'
					from 
						movingdirectory.campaign as ca 
						join
						irelocation.leads_security q
						on 
						locate(ca.lead_id,q.lead_ids) > 0 and q.campaign = ca.site_id
						join 
						marble.cost_per_lead cpl 
						on 
						cpl.lead_id = ca.lead_id and cpl.extra*4 = length(replace(q.lead_ids,'|',''))
					where 
						ca.month = '$year$month' 
						and ca.site_id = '".CAMPAIGN."' 
						and q.received like '$year$month%'
						and q.received not like '".date("Ymd")."%' 
						and q.ready_to_send = 3 ";
				
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' ";
				if ($site != "all" && $site != "usalarm")
					$sql .= " and q.source like '$site%' ";
					
				$sql .= "group by
						ca.lead_id,cpl.extra;";
				
				echo "<!--- Matrix Revenue Sql:\n$sql --->";		
				$rs = new mysql_recordset($sql);
				$total_cost = 0;
				while($rs->fetch_array())
					$total_cost += $rs->myarray['sub_total'];
			echo "<!--- Total matrix Revenue: $total_cost --->";
			return $total_cost;		
		}
		function getCompanyName($lead_id)
		{
			switch($lead_id)
			{
				case APX: return "Apex";
				case BRINKS: return "Brink's";
				case PROTECT_AMERICA: return "Protect America";
				case PROTECTION_ONE:return "Protection One";
				case USALARM: return "US Alarm";
				case SITEBASE: return "TopSecurityCompanies.com";
				case PINNACLE: return "Pinnacle Security";
				case GAYLORD: return "Gaylord Security";
				case APEXDIRECT: return "Apex Direct";
				case SAFETECH: return "Safe-Tech";
				case VOXCOM: return "Voxcom";				
				case BELLHOME: return "Bell Home Monitering";
				case MONITRONICS: return "Monitronics";
				case DEFENDER: return "Defender Direct";
			}	
		}				
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			if ($site != "aff")
			{
				$sql = "select sum(cost) cost from movingdirectory.costperclick ".
						" where ts like '$timeframe%' ";
#				echo "getCost SQL: $sql<br />";
				if ($engine != "all")
					$sql .= " and search_engine like '$engine%' ";
				if ($site == "all")
				{
					if (CAMPAIGN == "us")
						$sql .= " and site in ('tsc','tsc-local','tac') ";
					else if (CAMPAIGN == "defushome")
						$sql .= " and site in ('ushome') ";
					else
						$sql .= " and site in ('ctsc','ctac') ";
				}
				else
					$sql .= "and site = '".$site."'";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				
				$cost = $rs->myarray["cost"];
	
				if ($site == "all" && $engine == "all") {
					$cost += getAffiliateCost(AFF_CAT_NAME,$timeframe);
				}
				
				return $cost;	
			}
			else
				return getAffiliateCost(AFF_CAT_NAME,$timeframe,$engine);
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
				
/*
			echo "timeframe = $timeframe<br />";
			echo "engine = $engine<br />";
			echo "site = $site<br />";
*/

			$sql = "select count(*) count from irelocation.leads_security ".
					" q where received like '$timeframe%' and campaign = '".CAMPAIGN."' ";
					
						
			if (strlen($_REQUEST['ts']) != 6)
				$this_month = date("Ym");
			else
				$this_month = $_REQUEST['ts'];
			if ($timeframe == $this_month)
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			
			if ($site != "all") {
				$sql .= " and q.source like '".$site."%' ";
				if ($engine == "callcenter") {
					$sql .= " and q.source like 'usalarm%' ";
				}
			}
				
			if ($engine != "all") {
				$sql .= " and q.source like '%$engine%' ";
			}
			
			if(CPC_DEBUG)
				echo "\n<!--- getCount: $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
	
	
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from irelocation.leads_security q where received ".
					" like '$timeframe%' and campaign = '".CAMPAIGN."' ";
			$this_month = date("Ym");
			if ($timeframe == $this_month)
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
			}
			
			$sql .=  " group by substring(received,7,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- Security Count By Day: $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];		
			return $data;				
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{			
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			if ($site != "aff")
			{
				$sql = "select sum(cost) cost , substring(ts,7) 'day' ".
						" from movingdirectory.costperclick where ts like ".
						" '$timeframe%' ";
				
				if ($site != "all")
					$sql .= " and site like '$site' ";
				else
				{
					if (CAMPAIGN == "us")
						$sql .= " and site in ('tsc','tsc-local','tac') ";
					else if (CAMPAIGN == "defushome")
						$sql .= " and site in ('ushome') ";
					else
						$sql .= " and site in ('ctsc','ctac') ";
				}
				
				if ($engine != "all")
					$sql .= " and search_engine like '$engine%' ";
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Security Count By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				$data = array();
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}
			}
			else
			{
				$sql = "select ppl from movingdirectory.leadbased_affiliates ".
						" where lead_type = 'security' and source = '$engine' ".
						" and month = '".substr($timeframe,0,6)."' ";
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				return $rs->myarray['ppl'];
			}
			
			if ($site == "all" && $engine == "all")
			{
				$affdata = getAffiliateCostByDay(AFF_CAT_NAME,$timeframe);
				foreach($affdata as $day => $value)
				{
					$data[$day]["cost"] += $affdata[$day];
				}
			}						
			return $data;
		}	
	}
?>