<?
	include "graphs.php";
	
	class VanlinesGraph extends GraphFunctions
	{			
		var $campaign;
		var $percentage;
			
		function VanlinesGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- VanlinesGraph($timeframe,$engine,$site) --->\n";
			$this->percentage = new Percentage($timeframe,"van",$engine,$site);
			$this->campaign = "vanlines";
		}										
		
		function getGridSql($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$sql = "select 							
						1 as 'active',
						if(destination_state=origin_state,'local','long') as 'comp_name',
						substring(received,7,2) 'day',
						count(q.quote_id) 'count'
					from
						movingdirectory.quotes_local as q		
					where 	
						q.received like '$timeframe%'	
						".$this->sqlFilter."		
						 and lead_ids like '%1516%'					
					group by
						if(destination_state=origin_state,'local','long'),
						substring(received,7,2)";
			return $sql;
		}
		
		function getSqlFilter()
		{
			$sqlFilter = "";
		
			if ($this->site != "all")
			{
				$sqlFilter .= " and q.source like '".$this->site;
				if ($this->engine != "all")
					$sqlFilter .= "_".$this->engine."%' ";
				else
					$sqlFilter .= "%' ";
			}
			else
			{
				if ($this->engine != "all")
					$sqlFilter .= " and q.source like '%".$this->engine.
									"%' and source not like '%search%' ";		
			}
			$this->sqlFilter = $sqlFilter;
		}
		
		function getTotalLeads($timeframe="x")
		{
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			
			$total_count = "select count(*) count from movingdirectory.quotes_local q where ".
					" received like '$timeframe%' ".$this->sqlFilter.
					"  and lead_ids like '%1516%';";			
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			$rs->close();
			return $total_leads;
		}
		
		function getTotalLeadsToday($timeframe="x")
		{			
			$total_leads_today = 0;
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($timeframe == date("Ym"))
			{
				$total_count = "select count(*) count from movingdirectory.quotes_local q where received like '".date("Ymd")."%' ".$this->sqlFilter."  and lead_ids like '%1516%' ;";
									
				$rs = new mysql_recordset($total_count);
				$rs->fetch_array();				
				$total_leads_today = $rs->myarray["count"];
				$rs->close();
			}
			return $total_leads_today;
		}
		
		function getTypeCount($timeframe="x", $type)//type == local or Long
		{
			if ($type == "local")
				$filter = " and origin_state = destination_state ";
			else
				$filter = " and origin_state != destination_state ";
				
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$select = "select count(*) 'count' From movingdirectory.quotes_local q ".
					" where received like '$timeframe%' $filter ".$this->sqlFilter.
					"  and lead_ids like '%1516%'; ";
			echo "<!--- revenue $type $select --->\n";
			$rs = new mysql_recordset($select);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "\n\n<!-- vanlines.php timeframe = $timeframe -->\n\n";
			echo "\n\n<!-- vanlines.php engine = $engine -->\n\n";
			echo "\n\n<!-- vanlines.php site = $site -->\n\n";
			
			$count = $this->getCount($timeframe,$engine,$site);
			
			if ($site != "aff")
				$cost =  $this->getCost($timeframe,$engine,$site);
				
			/*
			else //for now this works, cause they're both 4 bucks a lead.
			{				
				$cost = getAffiliateCost("vanlines",$timeframe);	
			}	
			*/
			
			if ($engine == "all" && $site == "all")
			{	
				echo "\n<!--- affiliates added in at getData() --->\n";
				$cost += getAffiliateCost("vanlines",$timeframe);				
			}				
			
			$revenue = $this->getRevenue($timeframe);					
			$revenue = number_format($revenue,2,".",",");
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > 10)
				$cpl = "<font color='#FF0000'>$cpl</font>";
			else if (floatval($cpl) > 8)
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getRevenue($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			$types = array();
			
			$types['long'] = 15;
			$types['local'] = 6;
			
			$revenue = 0;
			foreach($types as $type => $ppl)
				$revenue += $this->getTypeCount($timeframe,$type) * $ppl;			
			
			return $revenue;
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($site == "all" || $site == "aff")
			{
				echo "<!--- affiliates added in at getCost() --->";
				$cost = getAffiliateCost("vanlines",$timeframe);			
			}
			$cost += $this->percentage->getCost($timeframe,"van",$site,$engine);
			
			return $cost;
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			$sql = "select count(*) count from movingdirectory.quotes_local ".			
				 " q where received like '$timeframe%' and lead_ids like '%1516%' ";
		
			if ($timeframe == date("Ym"))			
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";			
			
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";					
			}
			
			echo "\n\n<!-- vanlines.php getCount: $sql -->\n\n";
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
				
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from movingdirectory.quotes_local q where ".
					" received like '$timeframe%' and lead_ids like '%1516%'";
					
			$this_month = date("Ym");
			if ($timeframe == $this_month)
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not ".
							" like '%search%'";				
			}		
			$sql .=  " group by substring(received,7,2) ";						
					
			if(CPC_DEBUG)
			{
				echo "\n<!--- $sql --->\n";	
			}
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];	
		
			return $data;		
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$data = $this->percentage->getCostByDay($timeframe,"van",$site,$engine);
			
			$aff = getAffiliateCostByDay("vanlines",$timeframe);
			
			$keys = array_keys($aff);
			foreach($keys as $k)
			{
				$data[$k]['cost'] += $aff[$k];
			}									
			return $data;							
		}	
	}
	
	
?>