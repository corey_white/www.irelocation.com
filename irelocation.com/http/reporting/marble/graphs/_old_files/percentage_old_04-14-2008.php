<?
	/*
		OVERHAUL THIS CLASS TO RUN PERCENTAGES 
		AGAINST INTERNATIONAL-TOPMOVING-VANLINES....
		
		SAVE IT ALL OUT TO THREE VARIABLES PER DAY
		
		int_perc_[day]
		top_perc_[day]
		van_perc_[day]
	*/
	class Percentage
	{
		function Percentage($timeframe="x",$campaign,$site="all",$engine="all")
		{
			$this->timeframe = ($timeframe=="x")?date("Ym"):$timeframe;
			$this->campaign = $campaign;
			$this->site = $site;
			$this->engine = $engine;
		}	
		
		function getCost($timeframe="x",$campaign="x",$site="x",$engine="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($campaign=="x") $campaign = $this->campaign;
			if ($site=="x") $site = $this->site;
			if ($engine=="x") $engine = $this->engine;
			
			//Total CPC
			$sql = "select sum(cost) 'sum' from movingdirectory.percentages ".
					" where timeframe like '".$timeframe."%' and campaign = '".$campaign."' ";
			if ($site != "aff")
			{
				if ($site != "all")
					$sql .= " and site = '".$site."' ";
					
				if ($engine != "all")
					$sql .= " and engine = '".$engine."' ";
			}
			else
			{
				$sql .= " and engine = 'affiliates' ";
				if ($site != "all")
					$sql .= " and site = '$engine' ";
			
			}
			echo "<!--- \ngetCost(".$timeframe.",".$campaign.",".$site.",".$engine."):\n".$sql."\n--->";		
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray['sum'];
		}
		
		function getCostByDay($timeframe="x",$campaign="x",$site="x",$engine="x")
		{
			echo "\n<!--- percentages->getCostbyDay($timeframe,$campaign,$site,$engine) --->\n";
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($campaign=="x") $campaign = $this->campaign;
			if ($site=="x") $site = $this->site;
			if ($engine=="x") $engine = $this->engine;
			echo "\n<!--- percentages->getCostbyDay($timeframe,$campaign,$site,$engine) --->\n";		
			$sql = "select substring(timeframe,7,2) 'day', sum(cost) 'value' from ".
			"movingdirectory.percentages where campaign = '".$campaign."' and timeframe ".
			"like '".$timeframe."%' ";
			
			if ($site != "aff")
			{
				if ($site != "all")
					$sql .= " and site = '".$site."' ";
					
				if ($engine != "all")
					$sql .= " and engine = '".$engine."' ";
			}
			else
			{
				$sql .= " and engine = 'affiliates' ";
				if ($site != "all")
					$sql .= " and site = '$engine' ";
			}
			
			$sql .= " group by substring(timeframe,7,2); ";
					
			echo "<!--- \ngetCostByDay(".$timeframe.",".$campaign.",".$site.",".$engine."):\n".$sql."\n--->";				
					
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['cost'] = $rs->myarray['value'];
			}
			return $data;
		}
	}
	

	
?>