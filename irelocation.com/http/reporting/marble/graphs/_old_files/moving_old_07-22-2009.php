<?
	include "graphs.php";
	
	class MovingGraph extends GraphFunctions
	{			
		var $campaign;
		var $percentage;
		var $lead_prices;
			
		function MovingGraph($timeframe,$engine="all",$site="all")
		{
			
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- MovingGraph($timeframe,$engine,$site) --->\n";
			$this->percentage = new Percentage($timeframe,"top",$site,$engine);
			$this->campaign = "moving";
			$this->loadLeadPrices();
		}										
		
		function loadLeadPrices()
		{
			$site_id = ($this->campaign=="moving")? "pm" : "??";
			
			$sql = "select * from marble.cost_per_lead where site_id = '$site_id' ";
			$rs = new mysql_recordset($sql);
			$this->lead_prices = array();
			while($rs->fetch_array())
				$this->lead_prices[$rs->myarray['lead_id']] = $rs->myarray['cost'];											
				
			//echo "<!--- \n".print_r($this->lead_prices,true)."\n --->";
		}
		
		
		function getGridSql($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				movingdirectory.campaign as c
				left join 
				movingdirectory.quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'pm'
				AND q.received like '$timeframe%'
				".$this->sqlFilter."
				and q.cat_id = 2
				and c.month = '$timeframe'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				c.lead_id";
			return $sql;
		}
		
		function getSqlFilter()
		{
			$sqlFilter = "";
		
			if ($this->site != "all")
			{
				$sqlFilter .= " and q.source like '".$this->site;
				if ($this->engine != "all")
					$sqlFilter .= "_".$this->engine."%' ";
				else
					$sqlFilter .= "%' ";
			}
			else
			{
				if ($this->engine != "all")
					$sqlFilter .= " and q.source like '%".$this->engine.
									"%' and source not like '%search%' ";		
			}
			$this->sqlFilter = $sqlFilter;
		}
		
		function getTotalLeads($timeframe="x")
		{
			/*
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			
			$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received like '$timeframe%' and cat_id = 2 ".$this->sqlFilter.";";			
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			$rs->close();
			return $total_leads;
			*/
			
			//only gets costperclick leads!
			$total_count = "select sum(leads) 'count' from movingdirectory.percentages where ".
					" campaign = 'top' and timeframe like '".$timeframe."%' ";
			if ($this->site != "all")
				$sql .= " and site = '".$this->site."' ";
				
			if ($this->engine != "all")
				$sql .= " and engine = '".$this->engine."' ";
				
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			$rs->close();
			return $total_leads;
		}
		
		function getTotalLeadsToday($timeframe="x")
		{			
			$total_leads_today = 0;
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($timeframe == date("Ym"))
			{
				$total_count = "select sum(leads) 'count' from movingdirectory.percentages where ".
					" campaign = 'top' and timeframe like '".$timeframe."%' and timeframe not ".
					"like '".date("Ymd")."%' ";
					
				if ($this->site != "all")
					$sql .= " and site = '".$this->site."' ";
					
				if ($this->engine != "all")
					$sql .= " and engine = '".$this->engine."' ";
									
				$rs = new mysql_recordset($total_count);
				$rs->fetch_array();				
				$total_leads_today = $rs->myarray["count"];
				$rs->close();
			}
			return $total_leads_today;
		}
		
		function getSentCount($timeframe="x", $lead_id,$engine,$site)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$select = "select count(*) 'count' From movingdirectory.quotes where cat_id = 2 and ".
					" lead_ids like '%$lead_id%' and received like '$timeframe%'";
			if ($site != "all")			
				$select .= " and source like '".$site."%' ";

			if ($engine != "all")			
				$select .= " and source like '%".$engine."'";

			echo "\n\n<!-- getSentCount:  $select -->\n\n";
			$rs = new mysql_recordset($select);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			echo "\n\n<!-- getData SITE IS SET TO $site -->\n\n";
			
			$count = $this->getCount($timeframe,$engine,$site);			
			
			$cost = $this->getCost($timeframe,$engine,$site);	
			
			/*
			if ($engine == "all" && $site == "all")
			{	
				$cost += getAffiliateCost("moving",$timeframe);				
			}
			*/
			
			$revenue = $this->getRevenue($timeframe);
				
	
			$revenue = number_format($revenue,2,".",",");
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > 12)
				$cpl = "<font color='#FF0000'>$cpl</font>";
			else if (floatval($cpl) > 10)
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getRevenue($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$companies = array();
			
			/*
			$companies[1386] = 3.5;
			$companies[1387] = 6.5;
			$companies[1388] = 3.5;
			
			$revenue = 0;
			foreach($companies as $lead_id => $ppl)
			{
				$revenue += $this->getSentCount($timeframe,$lead_id,$engine,$site) * $ppl;
			}
			*/
			foreach($this->lead_prices as $lead_id => $ppl)
			{
				$revenue += $this->getSentCount($timeframe,$lead_id,$engine,$site) * $ppl;
			}
			
			return $revenue;
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($site == "aff")
			{
				echo "<!--- adding in affiliates at getCost() --->";
				$cost = getAffiliateCost("moving",$timeframe);			
			}
			else
			{
				/*
				if($site == "all")
					$cost = getAffiliateCost("moving",$timeframe);			
				*/
				$cost += $this->percentage->getCost($timeframe,"top",$site,$engine);	
			}
			echo "\n\n<!-- FOUND COST FOR ENGINE: $engine and SITE: $site and the cost is $cost -->\n\n";
			return $cost;
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			$sql = "select count(*) count from movingdirectory.quotes ".			
				 " q where received like '$timeframe%' and cat_id = 2 ";
		
			if (strlen($_POST['ts']) != 6)
				$this_month = date("Ym");
			else
				$this_month = $_POST['ts'];
			if ($timeframe == $this_month)
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";					
			}
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
				
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from movingdirectory.quotes q where received like ".
					" '$timeframe%' and cat_id = 2 ";
					
			$this_month = date("Ym");
			if ($timeframe == $this_month)
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not ".
							" like '%search%'";				
			}		
			$sql .=  " group by substring(received,7,2) ";						
					
			if(CPC_DEBUG)
			{
				echo "\n<!--- $sql --->\n";	
			}
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];	
		
			return $data;		
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "<!---\n moving->getCostsByDay(".$timeframe.",top,".$site.",".$engine."); --->\n";
			$data = $this->percentage->getCostByDay($timeframe,"top",$site,$engine);
			echo "<!---PERC DATA: \n ".print_r($data,true)." \n--->";
			
			/*
			if ($site == "all" && $engine == "all")
			{
				echo "<!---\n moving->getAffiliateCostByDay(moving,".$timeframe."); --->\n";
				$aff = getAffiliateCostByDay("moving",$timeframe);
				$keys = array_keys($aff);
				echo "<!---AFF: \n ".print_r($aff,true)." \n--->";
				foreach($keys as $k)
				{				
					$data[$k]['cost'] += $aff[$k];
				}									
			}
			*/
			return $data;							
		}	
	
//-- ADDED this function, taken it from marblehelper (could not include marbelhelper due to a function name conflict)
	function getAffiliateCost($category,$ts="*",$aff="*")
	{
		if ($ts=="*") $ts = date("Ym");	
		if ($aff == "*")
			$aff = "";
		else
			$aff = " and lba.source = '$aff' ";
		
		$campaign = " ";
		
		if ($ts == date("Ym"))
			$today = " and received not like '".date("Ymd")."%' ";
			
		if ($category == "moving")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 2 ";
		}
		else if ($category == "vanlines")
		{			
			$tables = array("movingdirectory.quotes_local");	
			$category = "moving";
		}
		else if ($category == "international")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 3 ";
			$category = "moving";
		}	
		else if ($category == "auto-tas")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'tas' ";
			$category = "auto";
			
		}
		else if ($category == "auto-atus")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'atus' ";
			$category = "auto";
		}		
		else if ($category == "auto")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'auto' ";
			$category = "auto";
		}			
		else if ($category == "security")
		{
			$tables = array("irelocation.leads_security");
			$campaign = " and q.campaign = 'us' ";
		}
		else if ($category == "ca.security")
		{
			$tables = array("irelocation.leads_security");
			$campaign = " and q.campaign = 'ca' ";
		}
		else if ($category == "aas.security" || $category == "au.security")
		{
			$tables = array("irelocation.leads_security");
			$campaign = " and q.campaign = 'aas' ";
		}
		else if ($category == "usalarm.security")
		{
			$tables = array("irelocation.leads_security");
			$campaign = " and q.campaign = 'usalarm' ";
		}
		$data = 0;		

		foreach($tables as $table)
		{				
			
			if ($table == "movingdirectory.quotes_local")
				$cost = "lba.localppl";
			else			
			
				$cost = "lba.ppl";
			
			$select = "select comp_name,count(*)*$cost 'total' from ".
					" movingdirectory.leadbased_affiliates as lba join $table ".
					" as  q on q.source like concat('%',lba.source,'%') where month = '".
					substr($ts,0,6)."' and q.received like '$ts%' $campaign ".
					" and lead_type = '$category' $aff $today group by ".
					" lba.comp_name ";
			
			echo "\n\n<!-- getAffiliateCost: $select -->\n\n";
			
			$rs = new mysql_recordset($select);
			
			$cost = 0;
			while($rs->fetch_array())
				$cost += $rs->myarray['total'];//will keep last entry.
						
			$data += $cost;
			$rs->close();
		}
		return $data;
	}

	
	}
	
	
?>