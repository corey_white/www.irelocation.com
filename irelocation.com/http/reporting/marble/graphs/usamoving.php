<?
	include "graphs.php";
	
	class InternationalGraph extends GraphFunctions
	{			
		var $campaign;
		var $percentage;
			
		function InternationalGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- InternationalGraph($timeframe,$engine,$site) --->\n";
			$this->percentage = new Percentage($timeframe,"int");
			$this->campaign = "international";
		}										
		
		function getGridSql($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				movingdirectory.campaign as c
				left join 
				movingdirectory.quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join movingdirectory.company as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'int'
				AND q.received like '$timeframe%'
				".$this->sqlFilter."
				and q.cat_id = 13
				and c.month = '$timeframe'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				c.lead_id";
			return $sql;
		}
		
		function getSqlFilter()
		{
			$sqlFilter = "";
		
			if ($this->site != "all")
			{
				$sqlFilter .= " and q.source like '".$this->site;
				if ($this->engine != "all")
					$sqlFilter .= "_".$this->engine."%' ";
				else
					$sqlFilter .= "%' ";
			}
			else
			{
				if ($this->engine != "all")
					$sqlFilter .= " and q.source like '%".$this->engine.
									"%' and source not like '%search%' ";		
			}
			$this->sqlFilter = $sqlFilter;
		}
		
		function getTotalLeads($timeframe="x")
		{
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			
			$total_count = "select count(*) count from movingdirectory.quotes q where ".
					" received like '$timeframe%' and cat_id in (13) ".$this->sqlFilter.";";			
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			$rs->close();
			return $total_leads;
		}
		
		function getTotalLeadsToday($timeframe="x")
		{			
			$total_leads_today = 0;
			$this->getSqlFilter();
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($timeframe == date("Ym"))
			{
				$total_count = "select count(*) count from movingdirectory.quotes q where ".
						" received like '".date("Ymd")."%' and cat_id in (13) ".$this->sqlFilter.";";
									
				$rs = new mysql_recordset($total_count);
				$rs->fetch_array();				
				$total_leads_today = $rs->myarray["count"];
				$rs->close();
			}
			return $total_leads_today;
		}
		
		function getSentCount($timeframe="x", $lead_id)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$select = "select count(*) 'count' From movingdirectory.quotes where cat_id = 13 and ".
					" lead_ids like '%$lead_id%' and received like '$timeframe%'";
			
			$rs = new mysql_recordset($select);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			
			$count = $this->getCount($timeframe,$engine,$site);
			if ($site != "aff")
				$cost =  $this->getCost($timeframe,$engine,$site);
			else //for now this works, cause they're both 4 bucks a lead.
			{	
				echo "<!--- adding in affiliate cost at getData() --->\n";
				$cost = getAffiliateCost("usamoving",$timeframe);	
			}	
			
			if ($engine == "all" && $site == "all")
			{	
				echo "<!--- adding in affiliate cost at getData() --->\n";
				$cost += getAffiliateCost("usamoving",$timeframe);				
			}
				
			
			$revenue = $this->getRevenue($timeframe);
				
	
			$revenue = number_format($revenue,2,".",",");
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > 5)
				$cpl = "<font color='#FF0000'>$cpl</font>";
			else if (floatval($cpl) > 4)
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getRevenue($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$companies = array();
			
			$companies[1387] = 4;
			$companies[1502] = 2;
			$companies[1516] = 10;
			
			$revenue = 0;
			foreach($companies as $lead_id => $ppl)
			{
				$revenue += $this->getSentCount($timeframe,$lead_id) * $ppl;
			}
			
			return $revenue;
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			if ($site == "all" || $site == "aff")
			{
				echo "<!--- adding in affiliate cost at getCost() --->";
				$cost = getAffiliateCost("usamoving",$timeframe);			
			}
			$cost += $this->percentage->getCost($timeframe,"int");
			
			return $cost;
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			$sql = "select count(*) count from movingdirectory.quotes ".			
				 " q where received like '$timeframe%' and cat_id in (13) ";
		
			if (strlen($_POST['ts']) != 6)
				$this_month = date("Ym");
			else
				$this_month = $_POST['ts'];
			if ($timeframe == $this_month)
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";					
			}
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
				
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from movingdirectory.quotes q where received like ".
					" '$timeframe%' and cat_id = 13 ";
					
			$this_month = date("Ym");
			if ($timeframe == $this_month)
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not ".
							" like '%search%'";				
			}		
			$sql .=  " group by substring(received,7,2) ";						
					
			if(CPC_DEBUG)
			{
				echo "\n<!--- $sql --->\n";	
			}
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];	
		
			return $data;		
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$data = $this->percentage->getCostByDay($timeframe,"usamoving"); //-- this should pull up nothing
			
			$aff = getAffiliateCostByDay("usamoving",$timeframe);
			
			$keys = array_keys($aff);
			foreach($keys as $k)
			{
				$data[$k]['cost'] += $aff[$k];
			}									
			return $data;							
		}	
	}
	
	
?>