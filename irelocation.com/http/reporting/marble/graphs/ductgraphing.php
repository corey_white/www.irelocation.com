<?
/* 
************************FILE INFORMATION**********************
* File Name:  ductgraphing.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  7/21/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

	include "graphs.php";
	
	if (CAMPAIGN == "quoteit")
		define(AFF_CAT_NAME,"quoteit");
	
	class DuctGraph extends GraphFunctions
	{			
		var $campaign;
			
		var $lead_prices = array();
		//$lead_prices[lead_id][quote_type] = <price>
		
		//loads all prices from database..
		//getting away from hard coding the values.
		function loadLeadPrices()
		{
			$site_id = "quoteit"; 
			echo "\n\n<!-- DUCTGRAPHING.PHP SITE_ID = $site_id -->\n\n";
			
			$sql = "select * from marble.cost_per_lead where site_id = '$site_id' ";
			$rs = new mysql_recordset($sql);
			$this->lead_prices = array();
			while($rs->fetch_array())
				$this->lead_prices[$rs->myarray['lead_id']][$rs->myarray['extra']] = $rs->myarray['cost'];											
				
			echo "<!--- \n loadLeadPrices PULL: \n".print_r($this->lead_prices,true)."\n --->";
		}
		
			
		function DuctGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- DuctGraph($timeframe,$engine,$site) --->\n";
			$this->campaign = "quoteit";
			//call it so its ready.
			$this->loadLeadPrices();
		}		
		
		function printFilter($timeframe="x",$sel_engine="all",$sel_site="all")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			$table = "irelocation.leads_ductcleaning";
			$sql = "select distinct(source) 'source' ,count(*) 'leads' from $table ".
					"where received like '".$timeframe."%'".
					" and campaign = '".CAMPAIGN."' group by source order by source";
			echo "\n<!--- filter: $sql --->\n";	
			$rs = new mysql_recordset($sql);
			
			$sites = array();
			$affiliates = array();
			$engines = array();
			$organics = array();
			
			while ($rs->fetch_array())
			{
				extract($rs->myarray);
				if (substr($source,0,3) == "aff")
				{				
					if (strpos($source,"bpm") > 0)
					{
						$affiliates['bpm'] += $leads;
					}
					else
						$affiliates[substr($source,4)] = $leads;
				}
				else
				{
					if (substr_count($source,"_google") == 1 || substr_count($source,"_overture") ||
						substr_count($source,"_msn") || substr_count($source,"_citys") || substr_count($source,"_sensis"))
					{
						list($site,$engine) = split("_",$source,2);
						if ($engine != "")
							$engines[$engine] += $leads;//double counting?
						$sites[$site] += $leads;
					}
					else
						$organics[$source] += $leads;
				}
			}			
			$rs->close();
						
			echo "<!--- organic: ".print_r($organics,true)." --->";
			
			echo "<table>";
			
			echo "<tr><td class='filterfont'>Month:</td><td>";
			printPeriodDropDown('ts',$_POST['ts'],7);
			echo "</td></tr><tr><td class='filterfont'>Sites:</td><td>";
			echo "<select name='site'>\n";
			echo "<option value='all'".$this->test("all",$sel_site).">All</option>\n";
			if (count($affiliates) > 0)
				echo "<option value='aff'".$this->test('aff',$sel_site).">affiliates</option>\n";
			foreach($sites as $site=>$count)
				echo "<option value='$site'".$this->test($site,$sel_site).">$site</option>\n";			
			echo "</select>";
			echo "</td></tr><tr><td class='filterfont'>Sources:</td><td>";
			echo "<select name='engine'>\n";
				echo "<option value='all'".$this->test("all",$sel_engine).">All</option>\n";	
				if (count($organics) > 0)
					echo "<option value='organic'".$this->test("organic",$sel_engine).">organic</option>\n";
				echo "<optgroup label='Pay per Click'>\n";
				foreach($engines as $engine=>$count)
					echo "<option value='$engine'".$this->test($engine,$sel_engine).">$engine</option>\n";
				echo "</optgroup>\n";
				echo "<optgroup label='Affiliates'>\n";
				foreach($affiliates as $aff=>$count)
					echo "<option value='$aff'".$this->test($aff,$sel_engine).">$aff</option>\n";
				echo "</optgroup>\n";	
			echo "</select>\n";	
			echo "</td></tr>";
			echo "<tr  class='filterfont'><td>Quote Type:</td>";
			echo "<td><select name='quote_type'>";
				echo "<option value='all' ".(($_POST['quote_type']==""||$_POST['quote_type']=="all")?" selected ":"").")>All</option>\n";
				echo "<option value='res' ".(($_POST['quote_type']=="res")?" selected ":"").">Residential</option>\n";
				echo "<option value='com' ".(($_POST['quote_type']=="com")?" selected ":"").">Commercial</option>\n";
			echo "</select></td></tr>";
			echo "<tr><td class='filterfont'>Submit:</td><td><input type='button' ".
				 "onClick='javascript:checkCombo(document.sitefilter.site,document.sitefilter.engine);' ".
				 " value='Submit' ></td></tr></table>";
		}
		
		function test($selected,$value)
		{
			if ($selected == $value) return " selected ";
			else return " ";
		}
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "\n\n<!-- getData timeframe = $timeframe -->\n\n";
			echo "\n\n<!-- getData engine = $engine -->\n\n";
			echo "\n\n<!-- getData site = $site -->\n\n";
			
			if ($engine == "dm" || $engine == "wc")
			{
				$_POST['site'] = 'aff';
				$site = "aff";
			}
			$count = $this->getCount($timeframe,$engine,$site);
			
			if ($site != "aff") {
				echo "\n\n<!-- HIT COST 1 IN getData -->\n\n";
				echo "\n\n<!-- 
				timeframe = $timeframe <br />
				engine = $engine <br />
				site = $site <br />
				-->\n\n";
				
				$cost = $this->getCost($timeframe,$engine,$site);
				
			} else { //for now this works, cause they're both 4 bucks a lead.
				echo "\n\n<!-- HIT COST 2 IN getData -->\n\n";
				$sql = "select ppl from movingdirectory.leadbased_affiliates ".
						" where lead_type = 'duct' and source = '$engine' ".
						" and month = '".substr($timeframe,0,6)."' ";
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				$cpl = $rs->myarray['ppl'];
				$cost = $count*$cpl;
			}	
			
			
			$revenue = $this->getTotalLeadRevenue(substr($timeframe,4),substr($timeframe,0,4),$engine,$site);
			$revenue = number_format($revenue ,2,".",",");
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > 28.00)
				$cpl = "<font color='red'>$cpl</font>";
			else if (floatval($cpl) > 24.00)
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		/*----*/
		
		function getTotalLeadRevenue($month,$year,$engine,$site) { //-- THIS IS ANOTHER MESSED UP FUNCTION! (no accounting for AUS)
			 if (CAMPAIGN == 'ca')
				 $array = array(ARIZONA_AIR_VENT,HONEST_ABE,DUCTZ);
			 
			 foreach($array as $comp)
			 {
				$revenue += $this->getLeadRevenue($comp, $month,$year,$engine,$site);
			 }
			 
			 if (CAMPAIGN == 'ca' && $month == "09" && $year == "2007")
			 {
			 	$revenue -= 1815;//Bell Home's Free Leads.
			 }
			 
			 return $revenue;
		}
		
		function getLeadRevenue($lead_id, $month,$year,$engine,$site)
		{
			$sql = "select count(quote_type) count, quote_type from ".
					"irelocation.leads_ductcleaning where lead_ids != '' and ".
					"received like '$year$month%' and lead_ids like '%$lead_id%'".
					" and campaign = '".CAMPAIGN."' ";			
			
			if ($engine != "all")
				$sql .= " and source like '%$engine%' ";
			if ($site != "all")
				$sql .= " and source like '$site%' ";
			
			if (date("m") == $month)//if now is during this month.
					$sql .= " and received not like '$year$month".date("d")."%'";
			$sql .= " group by quote_type ";
			
			echo "\n\n<!-- getLeadRevenue = $sql -->\n\n";
			
			$rs = new mysql_recordset($sql);		
			$rs->fetch_array();
			$price = $this->lead_prices[$lead_id][$rs->myarray["quote_type"]];
			$results += $rs->myarray["count"] * $price;
			
			echo "<!--- ".$this->getCompanyName($lead_id)." --->\n";
			echo "<!--- ".$rs->myarray["quote_type"].": ".$rs->myarray["count"]." at \$".$price." per lead --->\n";
			
			$rs->fetch_array();
			$price = $this->lead_prices[$lead_id][$rs->myarray["quote_type"]];
			$results += $rs->myarray["count"] * $price;
			
			echo "<!--- ".$rs->myarray["quote_type"].": ".$rs->myarray["count"]." at \$".$price." per lead --->\n";
			
			return $results;		
		}
		function getCompanyName($lead_id)
		{
			switch($lead_id)
			{
				case ARIZONA_AIR_VENT: return "Arizona Air Vent Cleaning";
				case HONEST_ABE: return "A Honest Abe's";
				case DUCTZ: return "Ductz of North Phoenix/Deer Valley";
			}	
		}
		/*
		function getPricePerLead($lead_id, $quote_type)
		{
			return $this->lead_prices[$lead_id][$quote_type];
			/*
			$prices = array(RESIDENTIAL => 8,COMMERCIAL => 10);
			
			switch($lead_id)
			{
				case APX:
					if ($quote_type=="res") return 11.5;
					else return 0;
				case DEFENDER:
					return 50;
				case MONITRONICS:
					if($quote_type=="res") return 11;
					else return 12;
				case BELLHOME:
					if($quote_type=="res") return 15;
					else return 16;
				case BRINKS: 		
					if (CAMPAIGN == "us")
					{	
						if($quote_type=="res") return 10;
						else return 11;
					}
					else
					{
						if($quote_type=="res") return 12;
						else return 13;
					}
					break;
				case VOXCOM:
					if($quote_type=="res") return 15;
					else return 16;
					break;
				case APEXDIRECT:
					if($quote_type=="res") return 15;
					else return 16;
					break;
				case SAFETECH:
					if($quote_type=="res") return 14;
					else return 15;
					break;
				case GAYLORD:
					if (CAMPAIGN == "us")
					{	
						if($quote_type=="res") return 11;
						else return 12;
					}
					else
					{
						if($quote_type=="res") return 15;
						else return 16;
					}
					break;
				case PROTECTION_ONE:							
					return $prices[$quote_type]-1;				
					break;
				case PROTECT_AMERICA:
					return 9;
					break;
				case PINNACLE:
					return $prices[$quote_type]+2;	
					break;
				case USALARM:
					if ($quote_type == RESIDENTIAL)
						return 11;
					else
						return 0;
					break;
			}		
			*/
		//}
		
		
		
		/*----*/
		
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "\n\n<!-- getCost timeframe = $timeframe -->\n\n";
			echo "\n\n<!-- getCost engine = $engine -->\n\n";
			echo "\n\n<!-- getCost site = $site -->\n\n";
			
			if ($site != "aff")
			{
				$sql = "select sum(cost) cost from movingdirectory.costperclick ".
						" where ts like '$timeframe%' ";
					
				if ($engine != "all")
					$sql .= " and search_engine like '$engine%' ";
					
				if ($site == "all") 
				{
					if (CAMPAIGN == "us")
						$sql .= " and site in ('tsc','tsc-local','tac') ";
					else if (CAMPAIGN == "defushome")
						$sql .= " and site in ('ushome') ";
					else if (CAMPAIGN == "aas") 
						$sql .= " and site in ('atac','atsc') "; //-- NEEDED TO ADD AUS stuff 
					else
						$sql .= " and site in ('ctsc','ctac') ";
						
				} else {
					$sql .= "and site = '".$site."'";
				}
				
				if(CPC_DEBUG)
					echo "\n<!---Cost: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				
				$cost = $rs->myarray["cost"];
	
				if ($site == "all" && $engine == "all")
					$cost += getAffiliateCost(AFF_CAT_NAME,$timeframe);
										
				return $cost;	
			}
			else
				return getAffiliateCost(AFF_CAT_NAME,$timeframe,$engine);
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
				
			$sql = "select count(*) count from irelocation.leads_ductcleaning ".
					" q where received like '$timeframe%' and campaign = '".CAMPAIGN."' ";
					
						
			if (strlen($_POST['ts']) != 6)
				$this_month = date("Ym");
			else
				$this_month = $_POST['ts'];
			if ($timeframe == $this_month)
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			
			if ($site != "all")
				$sql .= " and q.source like '".$site."%' ";
			if ($engine != "all")
				$sql .= " and q.source like '%$engine%' ";
			
			
			if(CPC_DEBUG)
				echo "\n<!--- getCount: $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];	
		}
	
	
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "\n\n<!-- getCountsByDay timeframe = $timeframe -->\n\n";
			echo "\n\n<!-- getCountsByDay engine = $engine -->\n\n";
			echo "\n\n<!-- getCountsByDay site = $site -->\n\n";
			
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from irelocation.leads_ductcleaning q where received ".
					" like '$timeframe%' and campaign = '".CAMPAIGN."' ";
			$this_month = date("Ym");
			if ($timeframe == $this_month)
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
			}
			
			$sql .=  " group by substring(received,7,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- Duct Count By Day: $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];		
			return $data;				
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{			
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "\n\n<!-- getCostsByDay timeframe = $timeframe -->\n\n";
			echo "\n\n<!-- getCostsByDay engine = $engine -->\n\n";
			echo "\n\n<!-- getCostsByDay site = $site -->\n\n";
			
			if ($site != "aff")
			{
				$sql = "select sum(cost) cost , substring(ts,7) 'day' ".
						" from movingdirectory.costperclick where ts like ".
						" '$timeframe%' ";
				
				if ($site != "all")
					$sql .= " and site like '$site' ";
				else
				{
					if (CAMPAIGN == "us")
						$sql .= " and site in ('tsc','tsc-local','tac') ";
					else if (CAMPAIGN == "defushome")
						$sql .= " and site in ('ushome') ";
					else if (CAMPAIGN == "aas") 
						$sql .= " and site in ('atac','atsc') "; //-- NEEDED TO ADD AUS stuff 
					else
						$sql .= " and site in ('ctsc','ctac') ";
				}
				
				if ($engine != "all")
					$sql .= " and search_engine like '$engine%' ";
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Duct Count By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				$data = array();
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}
			}
			else
			{
				$sql = "select ppl from movingdirectory.leadbased_affiliates ".
						" where lead_type = 'duct' and source = '$engine' ".
						" and month = '".substr($timeframe,0,6)."' ";
				$rs = new mysql_recordset($sql);
				$rs->fetch_array();
				return $rs->myarray['ppl'];
			}
			
			if ($site == "all" && $engine == "all")
			{
				$affdata = getAffiliateCostByDay(AFF_CAT_NAME,$timeframe);
				foreach($affdata as $day => $value)
				{
					$data[$day]["cost"] += $affdata[$day];
				}
			}						
			return $data;
		}	
	}
?>