<?
	include "graphs.php";
	define(COMPS_PER_LEAD,5);
	define(TAS_COMPS_PER_LEAD,5);
	define(TAS_LEAD_PRICE,1.25);
	
	if (!defined(CAMPAIGN))
		define(CAMPAIGN,"tas");

	function loadGraph($campaign,$timeframe,$engine="all",$site="all")
	{
		return new TasGraph($timeframe,$engine,$site);
	}
	
	class TasGraph extends GraphFunctions
	{			
		var $campaign;
			
		function TasGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- TasGraph($timeframe,$engine,$site) --->\n";
			$this->campaign = CAMPAIGN;
			$this->sqlfilter = "";
		}		

		function loadCompanyArray()
		{
			$sql = "Select co.comp_name,ca.* from marble.campaign as ca join ".
			" marble.companies as co on co.comp_id = ca.comp_id where ca.site_id = '".CAMPAIGN."' ".
			" and ca.month = '".$this->timeframe."'";			
			$rs = new mysql_recordset($sql );
			while ($rs->fetch_array())
			{
				$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
				$companies[$rs->myarray["lead_id"]] = $rs->myarray;
			}
			return $comapnies;
		}

		function getTotalLeads($timeframe="x",$engine="x",$site="x",$today=true)
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$total_count = "select count(*) count from marble.auto_quotes q where ".
					" processed and ".$this->getSqlFilter($timeframe,$engine,$site,$today).";";
			echo "<!--- total($timeframe) $total_count  --->";
			$rs = new mysql_recordset($total_count);
			$rs->fetch_array();
			$total_leads = $rs->myarray["count"];
			return $total_leads;
		}

		function getGridSql($timeframe="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
		
			$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal+c.temp_goal 'goal',
				count(q.quote_id) 'count',
				c.active
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join marble.companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = '".CAMPAIGN."'
				and ".$this->getSqlFilter($timeframe,$this->engine,$this->site)." 
				and c.month = '".$timeframe."'									
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				comp_name,day ";
		
			return $sql;
		}

		function getSqlFilter($timeframe,$engine,$site,$include_today=true)
		{
			if ($this->sqlfilter == "" || strlen($timeframe) == 8)
			{
				$sql = " received like '".$timeframe."%' ";
				
			
				$sql .= " and campaign = '".CAMPAIGN."' ";			
				
				if ($site != "all")
					$sql .= " and source like '$site%' ";		
							
				if ($engine != "all" && $engine != "organic")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
				else if ($engine == "organic")
				{
					$sql .= " and ((length(source) < 4) OR (source like '%_search_%') ".
									"OR (source like '%_email')) ";
				}
				if (strlen($timeframe) != 8)
					$this->sqlfilter = $sql;
				else
					return $sql;
			}
			return $this->sqlfilter;
		}
		
		function test($selected,$value)
		{
			if ($selected == $value) return " selected ";
			else return " ";
		}
			
		
		function printFilter($timeframe="x",$sel_engine="all",$sel_site="all")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			
			$sql = "select distinct(source) 'source' ,count(*) 'leads' from marble.auto_quotes ".
					"where campaign = '".CAMPAIGN."' and received like '".$timeframe."%' group by source ".
					"order by source";
					
			$rs = new mysql_recordset($sql);
			
			$sites = array();
			$affiliates = array();
			$engines = array();
			$organics = array();
			
			while ($rs->fetch_array())
			{
				extract($rs->myarray);
				if (substr($source,0,3) == "aff")
					$affiliates[substr($source,4)] = $leads;
				else
				{
					if (substr_count($source,"_") == 1 || substr_count($source,"local") == 1)
					{
						list($site,$engine) = split("_",$source,2);
						$engines[$engine] += $leads;//double counting?
						$sites[$site] += $leads;
					}
					else
						$organics[$source] += $leads;
				}
			}			
			$rs->close();
						
			
			echo "<table>";
			
			echo "<tr><td class='filterfont'>Month:</td><td>";
			printPeriodDropDown('ts',$_POST['ts']);
			echo "</td></tr><tr><td class='filterfont'>Sites:</td><td>";
			echo "<select name='site'>\n";
			echo "<option value='all'".$this->test("all",$sel_site).">All</option>\n";
			foreach($sites as $site=>$count)
				echo "<option value='$site'".$this->test($site,$sel_site).">$site</option>\n";
			if (count($affiliates) > 0)
				echo "<option value='aff'".$this->test('aff',$sel_site).">affiliates</option>\n";
			echo "</select>";
			echo "</td></tr><tr><td class='filterfont'>Sources:</td><td>";
			echo "<select name='engine'>\n";
				echo "<option value='all'".$this->test("all",$sel_engine).">All</option>\n";	
				if (count($organics) > 0)
					echo "<option value='organic'".$this->test("organic",$sel_engine).">organic</option>\n";
				echo "<optgroup label='Pay per Click'>\n";
				foreach($engines as $engine=>$count)
					echo "<option value='$engine'".$this->test($engine,$sel_engine).">$engine</option>\n";
				echo "</optgroup>\n";
				echo "<optgroup label='Affiliates'>\n";
				foreach($affiliates as $aff=>$count)
					echo "<option value='$aff'".$this->test($aff,$sel_engine).">$aff</option>\n";
				echo "</optgroup>\n";	
			echo "</select>\n";	
			echo "</td></tr>";
			echo "<tr><td class='filterfont'>Submit:</td><td><input type='submit'></td></tr></table>";
		}
		
		
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			if ($engine == "x") $engine = "all";
			if ($timeframe == "x") $timeframe = date("Ym");
			
			$count = $this->getCount($timeframe,$engine,$site);
			if ($site != "aff" && $site != "all")
				$cost =  $this->getCost($timeframe,$engine,$site);
			else if ($site == "aff")
			{				
				if ($engine == "all") $engine = "*";			
				$aff = getAffiliateCost("auto-".CAMPAIGN,$timeframe,$engine);				
				echo "\n<!--- aff cost: $aff $engine --->\n";
				$cost += $aff;
			}	
			else if ($site == "all")
			{
				$cost =  $this->getCost($timeframe,$engine,$site);
				if ($engine == "all") $engine = "*";			
				$aff = getAffiliateCost("auto-".CAMPAIGN,$timeframe,$engine);				
				echo "\n<!--- aff cost: $aff $engine --->\n";
				$cost += $aff;
			}
			/*
			if ($site == "all" || $site == "aff")
			{
				if ($engine == "all") $engine = "*";			
				$aff = getAffiliateCost("auto-tas",$timeframe,$engine);				
				echo "\n<!--- aff cost: $aff $engine --->\n";
				$cost += $aff;
			}
			*/
	
			$cap = get(CAMPAIGN."_revenue_cap");

			echo "\n<!--CAP: $cap --->";
			echo "\n<!--- $count ".TAS_COMPS_PER_LEAD." --->\n";
			echo "\n<!--- count*TAS_COMPS_PER_LEAD: ".($count*TAS_COMPS_PER_LEAD)." --->";
	
			$revenue = $count*TAS_COMPS_PER_LEAD*TAS_LEAD_PRICE;				
				
			if($revenue >$cap)
				$revenue = "<font color='green'>Revenue Cap of \$".number_format($cap,2,".",",")." hit.</font>";
			else
				$revenue = "<span title='Revenue Cap: \$".$cap."'>".number_format($revenue,2,".",",")."</span>";				
			
			//we didnt switch to 7 companies until the 5th of march.
			
			//ONLY FOR MARCH.		
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > (COMPS_PER_LEAD*.9))
				$cpl = "<font color='red'>$cpl</font>";
			else if (floatval($cpl) > (COMPS_PER_LEAD*.75))
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			if ($engine == "organic")
				return 0;
		
			$sql = "select sum(cost) cost from movingdirectory.costperclick where ts like '$timeframe%' ";
			if (CAMPAIGN == "tas")
			{
				//if ($site == "tas" || $site == "all")
					$sql .= " and site = 'tas' ";
			}
			else if (CAMPAIGN == "atus")
			{
				if ($site != "all")
					$sql .= " and site in ('atus','123cm') ";
				else
					$sql .= " and site = '$site' ";
			}	
			if ($engine == "google" || $engine == 'overture' || $engine == 'msn')
				$sql .= " and search_engine like '$engine%' ";
					
			if(CPC_DEBUG)
				echo "\n<!---Cost: $sql --->\n";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			
			$cost = $rs->myarray["cost"];		
			return $cost;		
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			$sql = "select count(*) count from marble.auto_quotes q where ".
					$this->getSqlFilter($timeframe,$engine,$site);
							
			echo "\n<!--- $sql --->\n";	
						
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];		
		}
	
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$filter = $this->getSqlFilter($timeframe,$engine,$site);
			$sql = "select count(*) count, substring(received,7,2) 'day' from ".
					"marble.auto_quotes q where ".$filter." group by "
					." substring(received,7,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
			}
			return $data;	
		}
		
		
		
		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{						
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			$data = array();
			
			if ($engine == "all")
				$aff_engine = "*";
			else
				$aff_engine = $engine;
				
			$affcost = getAffiliateCostByDay("auto-".CAMPAIGN,$timeframe,$aff_engine);
		
			if ($engine == "organic")
			{
				for($i = 1; $i < date("d"); $i++)
				{
					if ($i < 10) $d = "0$i";
					else $d = $i;
					$data[$d]['cost'] = 0;
				}
				//ORGANIC NO COST!
				return $data;
			}	
			
			if ($site != "all" && $site != "aff")//particular site.
			{
				echo "<!--- site: $site --->";
				$sql = "select sum(cost) cost , substring(ts,7) 'day' from ".
						"movingdirectory.costperclick where ts like '$timeframe%' ";
								
				$sql .= " and site like '".$site."%' ";	
				if ($engine != "all")
					$sql .= " and search_engine like '".$engine."%' ";
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}				
			}
			else if ($site == "aff")
			{
				echo "<!--- site: aff --->";
				$days = array_keys($affcost);
				foreach($days as $day)
				{
					$data[$day]['cost'] += $affcost[$day];
				}
			}
			else//both
			{
				echo "<!--- site: all --->";
				
				$sql = "select sum(cost) cost , substring(ts,7) 'day' from ".
						"movingdirectory.costperclick where ts like '$timeframe%' ";
								
				//NOTE: only because its the only				
				// site that feesd this system		
				
				if (CAMPAIGN == "tas")
				{						
					$sql .= " and site = 'tas' ";
				}
				else if(CAMPAIGN == "atus")
				{
					$sql .= " and site in ('atus','123cm') ";
				}
				
				if ($engine != "all")
					$sql .= " and search_engine like '".$engine."%' ";
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}		
				$rs->close();
				$days = array_keys($affcost);
				foreach($days as $day)
				{
					$data[$day]['cost'] += $affcost[$day];
				}
			}	
		
			return $data;		
		}				
	}
?>