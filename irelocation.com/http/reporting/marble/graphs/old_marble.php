<?
	include "graphs.php";
	define(COMPS_PER_LEAD,7);
	
	class MarbleGraph extends GraphFunctions
	{			
		var $campaign;
			
		function MarbleGraph($timeframe,$engine="all",$site="all")
		{
			parent::GraphFunctions($timeframe,$engine,$site);
			echo "<!--- MarbleGraph($timeframe,$engine,$site) --->\n";
			$this->campaign = "marble";
		}		
		
		
		function getData($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			
			$count = $this->getCount($timeframe,$engine,$site);
			if ($site != "aff")
				$cost =  $this->getCost($timeframe,$engine,$site);
			else //for now this works, cause they're both 4 bucks a lead.
			{
				if ($engine == "A1AUTO")
					$cost = 4.5*$count;
				else if ($engine == "WC")
					$cost = 4*$count;
			}	
			
			if ($engine == "all" && $site == "all")
			{	
				$cost += getAffiliateCost("auto",$timeframe);				
			}
	
			$cap = get("topauto_revenue_cap");
			echo "\n<!--CAP: $cap --->";
			echo "\n<!--- count*COMPS_PER_LEAD: ".($count*COMPS_PER_LEAD)." --->";
	
			$revenue = $count*COMPS_PER_LEAD;
			if (date("Ym") == "200703")	$revenue -= 359;	
	
			if($revenue >$cap)
				$revenue = "<font color='green'>Revenue Cap of \$".number_format($cap,2,".",",")." hit.</font>";
			else
				$revenue = "<span title='Revenue Cap: \$".$cap."'>".number_format($revenue,2,".",",")."</span>";				
			
			//we didnt switch to 7 companies until the 5th of march.
			
			//ONLY FOR MARCH.		
			
			if ($count > 0)
				$cpl = number_format(($cost / $count),2,".",",");
			else 
				$cpl = $cost;
			$cost = number_format($cost,2,".",",");				
		
			if (floatval($cpl) > (COMPS_PER_LEAD*.9))
				$cpl = "<font color='red'>$cpl</font>";
			else if (floatval($cpl) > (COMPS_PER_LEAD*.75))
				$cpl = "<font color='#B5B128'>$cpl</font>";
			else
				$cpl = "<font color='green'>$cpl</font>";
				
			return array($revenue,$cpl,$cost);
		}
		
		function getCost($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			$sql = "select sum(cost) cost from movingdirectory.costperclick where ts like '$timeframe%' ";
			
			if ($site != "all")
				$sql .= " and site like '$site%' ";		
			else
				$sql .= " and site in ('cs','asc','mmc','tas','pas','asc-local') ";		
				
			if ($engine != "all")
				$sql .= " and search_engine like '$engine%' ";
				
			if(CPC_DEBUG)
				echo "\n<!---Cost: $sql --->\n";
			
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			
			$cost = $rs->myarray["cost"];		
			return $cost;		
		}	
		
		function getCount($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
		
			$sql = "select count(*) count from marble.auto_quotes q where ".
					"received like '$timeframe%' ";
			$sql .= " and campaign = 'auto' ";	
			
			if ($timeframe ==  date("Ym"))
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_$engine%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%$engine%' and source not like '%search%' ";	
				
			}
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray["count"];		
		}
	
	
		function getCountsByDay($timeframe="x",$engine="x",$site="x")
		{
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			echo "<!--- marble.getCountsByDay($timeframe,$engine,$site) --->\n";
			$sql = "select count(*) count, substring(received,7,2) 'day' ".
					" from marble.auto_quotes q where received like '".$timeframe."%' ";
		
			if ($timeframe == date("Ym"))
			{
				//remove todays leads, we don't have CPC data for them...
				$sql .= " and received not like '".date("Ymd")."%' ";
			}
			$sql .= " and campaign = 'auto' ";	
			if ($site != "all")
			{
				$sql .= " and q.source like '".$site;
				if ($engine != "all")
					$sql .= "_".$engine."%' ";
				else
					$sql .= "%' ";
			}
			else 
			{
				if ($engine != "all")
					$sql .= " and q.source like '%".$engine.
							"%' and source not like '%search%' ";					
			}
			
			$sql .=  " group by substring(received,7,2) ";
			
			if(CPC_DEBUG)
				echo "\n<!--- $sql --->\n";	
				
			$rs = new mysql_recordset($sql);
			$data = array();
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']]['count'] = $rs->myarray['count'];
			}
			return $data;		
		}

		function getCostsByDay($timeframe="x",$engine="x",$site="x")
		{			
			if ($timeframe=="x") $timeframe = $this->timeframe;
			if ($engine=="x") $engine = $this->engine;
			if ($site=="x") $site = $this->site;
			
			echo "<!--- marble.getCostsByDay($timeframe,$engine,$site) --->\n";
			if ($site != "aff")
			{
				$sql = "select sum(cost) cost , substring(ts,7) 'day' from".
						" movingdirectory.costperclick where ts like '$timeframe%' ";
				
				if ($site != "all")
					$sql .= " and site like '$site%' ";
				else
					$sql .= " and site in ('cs','asc','mmc','tas','pas','asc-local','cq') ";
					
				if ($engine != "all")
					$sql .= " and search_engine like '$engine%' ";
				
				$sql .= " group by substring(ts,7)";
				
				if(CPC_DEBUG)
					echo "\n<!---Cost By Day: $sql --->\n";
				
				$rs = new mysql_recordset($sql);
				$data = array();
				while($rs->fetch_array())
				{
					$data[$rs->myarray['day']]['cost'] = $rs->myarray['cost'];
				}
				if ($site == "all" && $engine == "all")
				{
					$data_aff = $this->getCountsByDay("x","wc","aff");
					foreach($data as $day => $value)
					{
						$data[$day]["cost"] += $data_aff[$day]["count"] * 4;
					}
					$data_aff = $this->getCountsByDay("x","A1AUTO","aff");
					foreach($data as $day => $value)
					{
						$data[$day]["cost"] += $data_aff[$day]["count"] * 4.5;
					}
				}
			}
			else
			{
				$data = $this->getCountsByDay("x","aff","x");
				foreach($data as $day => $value)
				{
					$data[$day]["cost"] = $data[$day]["count"] * 4;
				}
			}						
			return $data;		
		}	
		
		
	}
?>