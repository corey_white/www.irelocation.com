<?
	session_start();
	
	unset($_SESSION['user']);
	unset($_SESSION['time']);
	unset($_SESSION['lead_id']);
	unset($_SESSION['username']);
	
	header("Location: marbleloggedout.php");
	exit();
?>