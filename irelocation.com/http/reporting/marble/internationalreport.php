<?
	session_start();
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		header("Location: login.php");
		exit();
	}
	else
	{
		$_SESSION['time'] = date("YmdHis");
	}
	define(DEVIATION,5);
	include "skin_top.php";
	include "movingtrends.php";

	function lastWeek()
	{		
		$msg = "";
		if ($_POST['ts'] == date("Ym") || $_POST['ts'] == "")
		{
			$lwtime = time()-(7*24*60*60);
			$lastweek = date("YmdHis",$lwtime);
			$lastweekday  = date("Ymd",$lwtime);
			$sql = "select count(*) 'count' from movingdirectory.quotes where ".
				  " cat_id = 3 and received like '$lastweekday%' and received < '$lastweek' ";		
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$leads = $rs->myarray['count'];
			
			$msg = "We received $leads leads this time last ".date("l",$lwtime).".";
		}
		return $msg;
	}

	if (strlen($_POST['ts']) == 6 && $_POST['ts'] != date("Ym"))
	{
		$today = getNDays(substr($_POST['ts'],4),substr($_POST['ts'],0,4));
		$this_month = $_POST['ts'];
		$full_days = $days_in_month;
		$show_old_data = true;
		$days_in_month = $today;
		$days_left = 0;
		$today_sql = "";
	}
	else
	{
		$today_sql = " and received not like '".date("Ymd")."%' ";
		$today = date("d");
		$full_days = $today-1 + getTrends();
		if ($full_days == 0)
			$full_days = 1;
		$this_month = date("Ym");
		$show_old_data = false;
		$days_in_month = date('t');
		$days_left = $days_in_month - $today + 1;
	}
	
	$sql = "select 
				c.comp_id,
				d.lead_id,
				ca.monthly_goal,
				(ca.monthly_goal/$days_in_month)*$full_days 'est_sent',				
				comp_name,
				contact_name,
				contact_email,
				replace(phone_num,'-','') 'phone_num',
				'1' active				
			from 
				movingdirectory.company as c 
				join 
				movingdirectory.directleads as d 
				on c.comp_id = d.comp_id 
				join 
				movingdirectory.campaign as ca
				on ca.lead_id = d.lead_id
			where 
				ca.site_id = 'int'
				and ca.month = '$this_month'";
	$rs = new mysql_recordset( $sql );
	echo "<!---\n $sql \n--->\n";
	while ($rs->fetch_array())
	{
		$rs2 = new mysql_recordset("select count(*) as 'Actual_Sent' from movingdirectory.quotes where lead_ids like '%".$rs->myarray['lead_id']."%' and received like '$this_month%'  and cat_id = 3 ");
		$rs2->fetch_array();
		$rs->myarray['Actual_Sent'] = $rs2->myarray['Actual_Sent'];
		$rs->myarray['deviation'] = $rs->myarray['Actual_Sent'] - $rs->myarray['est_sent'];
		$rs->myarray['leads_per_day'] = ($rs->myarray['monthly_goal'] - $rs->myarray['Actual_Sent']) / (max(1,$days_in_month-$full_days));
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;		
	}
		

	if ($days_left == 0)
		$days_left = 1;
	
	$total_count = "select count(*) count from movingdirectory.quotes where ".
					"received like '$this_month%' and cat_id = 3;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];
	
	$total_count = "select count(*) count from movingdirectory.quotes where ".
					"received like '$this_month$today%' and cat_id = 3;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	
	$default_order_by = "comp_name asc";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}	

	?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<Tr><td>&nbsp;</td></tr>
		<Tr>
			<td>
			<form action="internationalreport.php" method="post">
				<? printPeriodDropDown('ts',$this_month,0,1); ?>
				<input type='submit' name="go" value="Go" />
			</form>
			</td>
		</Tr>
		<Tr><td>&nbsp;</td></tr>

			<tr><td>
			<b><font face="Verdana" size="2">Today is the: <?= date("jS \o\f F"); ?></font></b>
		</td></tr>
		<tr><td>
			<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
			
		</td></tr>
		<? if ($_POST['ts'] == date("Ym") || $_POST['ts'] == '') { ?>
		<tr><td>
			<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
		
		</td></tr>
		<tr>
			<td>
				<b><font face="Verdana" size="2"><?= lastWeek() ?></font></b>
			</td>
		</tr>
		<? } ?>
		<Tr><td>
		
	<div align="right">
		<font face="Verdana" size="2">
		<font color="#FF0000">Greater than <?= DEVIATION ?>% differance</font>
		 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		   <Br/>
		<font color="#006600">Less than <?= DEVIATION ?>% differance</font>
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
		 <Br/>
		</div>
		</font>
	
	
<?	


	echo "<table border='1'>";
	echo "<tr class='header' ><td>&nbsp;</td>".
			"<th align='left' >".getSortLink($default_order_by,"comp_name","Company")."</th>".
			"<th>".getSortLink($default_order_by,"c.lead_id","Lead ID")."</th>".
			"<th>".getSortLink($default_order_by,"c.monthly_goal","Monthly Goal")."</th>".
			"<th>".getSortLink($default_order_by,"Actual_Sent","Sent")."</th>".
			"<th>".getSortLink($default_order_by,"Est_Sent","Est. Sent")."</th>".
			"<th>".getSortLink($default_order_by,"deviation","Deviation")."</th>";
	if (!$show_old_data)
		echo "<th>".getSortLink($default_order_by,"needed_per_day","Needed Per Day")."</th>";
	echo "<th>".getSortLink($default_order_by,"leads_needed","Leads Needed")."</th></tr>";
	$count = 1;
	
	$font_counter = 1;	
	
	foreach($companies as $comp)
	{		
		if ($comp == 0)
			continue;
		else
			extract($comp);
			
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		$est_sent = intval($est_sent);
		echo "<tr class='$style' >\n<td align='right'>";
		echo $count++."</td><td>";
		if (!$active)
			echo "<font color='red'>INACTIVE </font>";
		echo "<a class='black' title='Click to view lead format' href='movingformat.php?lead_id=$lead_id'>";
		echo "$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		echo "$monthly_goal</td><td align='right'>";
		echo $Actual_Sent."</td><td align='right'>";
		echo $est_sent."</td><td align='right'>";
	

			
		echo colorCode($Actual_Sent,$est_sent,DEVIATION)."</td>";
		
		echo "<td align='right'>".intval($leads_per_day)."</td>";
		echo "<td align='right'><strong>".($monthly_goal-$Actual_Sent)."</strong></td>";
		echo "</tr>\n";
		$font_counter++;
	}

	
	if ($font_counter % 2 == 0)//even
		$style = "on";
	else
		$style = "off";
			
	include "skin_bottom.php";
	?>