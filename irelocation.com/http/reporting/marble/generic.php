<?
	function getTotalCost($counts)
	{
		if (count($counts) == 0) return 0;
		foreach($counts as $day => $count)
			$total += $count['cost'];
		return $total;
	}
	
	function getTotalCount($counts)
	{
		if (count($counts) == 0) return 0;
		foreach($counts as $day => $count)
			$total += $count['count'];
		return $total;
	}
	
	function _getRevenue($a,$month,$engine,$site,$total="x")
	{
		if (count($a['prices']) == 1)//one price to deal with..
		{
			$ppl = $a['prices'][0]['ppl'] * $a['max_send'];
			$revenue = $ppl * $total;
		}
		else if ($a['campaign'] == "vanlines")//grr...
		{
			$prices['local'] = $a['prices'][0]['ppl'];
			$prices['long'] = $a['prices'][1]['ppl'];
			
		
			$filter = " if(destination_state=origin_state,'local','long') ";
			$sql = "select $filter 'type', count(*) 'count' from ".$a['table']." as q where ".
				   " substr(received,1,6) = '$month' ".$a['filter']." ";
			if ($site != "all")
			{
				if ($engine == "all")
					$sql .= " and source like '$site%' ";
				else
					$sql .= " and source like '".$site."_".$engine."' ";
			}
			else if ($engine != "all")
				$sql .= " and source like '%$engine' ";
			
			if ($month == date("Ym"))
				$sql .= " and received not like '".date("Ymd")."%' ";
							
			$sql .= " group by $filter ";
			
			$rs = new mysql_recordset($sql);
			$revenue = 0;
			while($rs->fetch_array())
			{
				$t = $rs->myarray['type'];
				$revenue += $rs->myarray['count']*$prices[$t];
			}			
		}
		else if ($a['campaign'] == "moving")
		{
			$price = 0;
			foreach($a['prices'] as $p)
				$price += $p['ppl'];
			$revenue = $total * $price;
		}
		else
		{
			$country = ($a['campaign']=="security")?"us":"ca";
			$sql = "select lead_ids,quote_type,count(*) 'count' from ".
					$a['table']." where campaign = '$country' and received ".
					" like '$month%' ";
			if ($site != "all")
			{
				if ($engine == "all")
					$sql .= " and source like '$site%' ";
				else
					$sql .= " and source like '".$site."_".$engine."' ";
			}
			else if ($engine != "all")
				$sql .= " and source like '%$engine' ";
			
			if ($month == date("Ym"))
				$sql .= " and received not like '".date("Ymd")."%' ";
			
			$sql .= " group by quote_type,lead_ids ";
			
			$rs = new mysql_recordset($sql);	
			$revenue = 0;		
			$prices = transposePrices($a['prices']);
			echo "<!--- ".print_r($prices,true)." --->\n";
			while($rs->fetch_array())
			{
				extract($rs->myarray);
				echo "<!--- $lead_ids --->\n";
				$lead_ids = split("\|",$lead_ids);
				foreach($lead_ids as $lead_id)
				{
					echo "<!--- $revenue --->\n";
					$revenue += ($prices[$lead_id][$quote_type] * $count);
				}
				
			}
		}
		
		return $revenue;
	}
	
	function transposePrices($prices)
	{
		$data = array();
		
		foreach ($prices as $p)
		{
			$data[$p['lead_id']][$p['lead_type']] = $p['ppl'];			
		}
		return $data;
	}
	
	function _getSummary($costs,$counts,$revenue)
	{
	
	}
	
	function getCampaignDetails($campaign,$month="x")
	{
		if ($month == "x") $month = date("Ym");
		
		if ($campaign == "moving" || $campaign == "vanlines" ||$campaign == "international")
		{
			$data['moving'] = true;
			$data['moving-type'] = substr($campaign,0,3);
			if ($data['moving-type'] == "mov") $data['moving-type'] = "top";
		}	
		else
			$data['moving'] = false;
		
		list($aff,$afflist) = _getAffiliates($campaign,$month);
		
		$data['campaign'] = $campaign;
		$data['affiliates'] = $aff;
		$data['afflist'] = $afflist;
				
		$sql = "select * from movingdirectory.campaign_details ".
				"where campaign = '".$campaign."' ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$data['campaign_code']  = $rs->myarray['campaign_code'];
		$data['target_price'] = $rs->myarray['target_price'];	
		$data['max_send'] = $rs->myarray['max_send'];												
		$data['filter'] = $rs->myarray['filter'];
		$data['table'] = $rs->myarray['table'];
		$data['sites'] = $rs->myarray['sites'];
		$rs->close();
		
		$sql = "select * from movingdirectory.campaign_leadprices ".
				"where campaign = '$campaign'  order by lead_type";
		$rs = new mysql_recordset($sql);
		
		while($rs->fetch_array())
			$data['prices'][] = $rs->myarray;
		$rs->close();
		
		return $data;										
	}
	
	function _getFilter($engine,$site)
	{
		$sql = "";
		if ($site != "all")
		{
			if ($engine != "all")
				$sql .= " and q.source like '".$site."_".$engine."' ";
			else
				$sql .= " and q.source like '".$site."%' ";
		}
		else if ($engine != "all")
		{
			$sql .= " and q.source like '%".$engine."' ";		
		}	
		return $sql;
	}
	
	function _getAffiliates($campaign,$month)
	{
		switch($campaign)
		{
			case "security":
			case "ca.security":
				$afftype = "security";
				break;
			case "moving":
			case "international":
			case "vanlines":
				$afftype = "moving";
				break;
			case "auto-tas":
			case "auto":
			case "auto-atus":
				$afftype = "auto";
				break;
		}
	
		$sql = "select * from movingdirectory.leadbased_affiliates ".
				" where month = '$month' and lead_type = '$afftype'";
				
		echo "\n<!--- Affiliates Sql : $sql --->\n";
				
		$rs = new mysql_recordset($sql);
		$afflist = array();
		while($rs->fetch_array())
		{
			$aff[] = $rs->myarray;	
			$afflist[] = array($rs->myarray['comp_name'],$rs->myarray['source']);
		}
		$rs->close();
		return array($aff,$afflist);
	}
	
?>