<?
	session_start();
	
	define(CPC_DEBUG,true);
	
	include "skin_top.php";//includes marble helper, and mysql class.
	include "graphs/percentage.php";//seperate moving CPC between Top,Van, and Int
	include "graphs/usamoving.php";
	
	$site = ($_POST['site']=="")?"all":$_POST['site'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$timeframe = ($_POST['timeframe']=="")?date("Ym"):$_POST['timeframe'];
	$graph = new InternationalGraph($timeframe,$engine,$site);
	
	$graph->getSqlFilter();
	
		
	$total_leads =  $graph->getTotalLeads();
	$total_leads_today = $graph->getTotalLeadsToday();
	
	?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<? 	showGraphAndCounts($graph);	?>
		<tr><td width='40%'>
			<font face="Verdana" size="2">	

			<form action="usamovinggrid.php" method="post" name="sitefilter">	
			Period
			<?= printPeriodDropDown("timeframe",$timeframe,1,1); ?><br/>
			Site:									
						<select name='site'>
						<optgroup label="Sites"/>
						<option value='all'>All</option>
					<? 
						$sites = array('tm','pm','me');
						
						foreach($sites as $site)
						{
							echo "<option value='$site'";
							if ($_POST['site'] == $site) echo " selected ";
							echo ">".strtoupper($site)."</option>\n";
						}
					?>
					<optgroup label="Affiliates"/>
						<option value="aff"<? if ($_POST['site'] == 'aff') echo ' selected'; ?> >
							Affiliates
						</option>
					</select> 										
					
					<input type='hidden' name="filter" value="Filter" />
					
					<select name='engine' >
						<optgroup label="Search Engines"/>
						<option value='all'>All</option>
					<? 
						$engines = array('google','ask','overture','msn','bizcom');
						foreach($engines as $engine)
						{
							echo "<option value='$engine'";
							if ($_POST['engine'] == $engine) echo " selected ";
							echo ">".ucwords($engine)."</option>\n";														
						}
					?>
						<optgroup label="Affiliates"/>						
						<option value="A1AUTO"<? if($engine == 'A1AUTO') echo ' selected'; ?> >
							A1 Auto
						</option>
					</select>
					<br/>
					<input type='submit' name="filter" value="Filter" />
				</form>
			</font>
			
			</tr></table>
		</td></tr>
		<Tr><td>

<?			
	if ($total_leads == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	$total_sql = "select 
				substring(received,7,2) 'day',
				count(q.quote_id) 'count'				
			from
				movingdirectory.quotes as q
			where 
				q.received like '$timeframe%'					
				".$graph->sqlFilter."
				and cat_id = 13
			group by
				substring(received,7,2)";

	printGridHeader($timeframe,$total_sql,true);
	
	$filter = $_POST['filter'];
	
	if ($site != "all" && $filter == "Filter")
		$append_link = "&site=".$site;
	if ($engine != "all" && $filter == "Filter")
		$append_link .= "&engine=".$engine;
	
		
	
	//$category = "moving";
	printGrid($timeframe,"usamovingviewleads.php",$graph->getGridSql(),$append_link,true);	

	echo "</table>";	
	include "skin_bottom.php";
?>