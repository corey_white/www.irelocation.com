<?
	session_start();
	define(PRICE,3.00);
	define(CPC_DEBUG,true);
	
	include "skin_top.php";

	function gatherLeadsToday()	
	{
		$today = date("Ymd");
		$sql = "select count(*) 'count' From movingdirectory.vanlinesstorage as s join movingdirectory.quotes_local as q on q.quote_id = s.quote_id and received like '$today%' and s.success ".
				"union all ".
				"select count(*) 'count' From movingdirectory.vanlinesstorage as s join movingdirectory.quotes as q on q.quote_id = s.quote_id and received like '$today%' and s.success";
		$rs = new mysql_recordset($sql);
		$result = 0;
		while($rs->fetch_array())
			$result += $rs->myarray['count'];
		return $result;
	}

	function gatherStorageCounts($month)
	{
		$total = 0;
		$data = array();
		$sql = "select substring(received,7,2) 'day', count(*) 'count' From movingdirectory.vanlinesstorage as s join movingdirectory.quotes_local as q on q.quote_id = s.quote_id and received like '$month%' and s.success group by substring(received,7,2);";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
		{
			$data[$rs->myarray['day']] = $rs->myarray['count'];
			$total += $rs->myarray['count'];
		}
		
		$sql = "select substring(received,7,2) 'day', count(*) 'count' From movingdirectory.vanlinesstorage as s join movingdirectory.quotes as q on q.quote_id = s.quote_id and received like '$month%' and s.success group by substring(received,7,2)";
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
		{
			$data[$rs->myarray['day']] += $rs->myarray['count'];
			$total += $rs->myarray['count'];
		}
		return array("total" => $total, "data" => $data);
	}
	
	if($_POST['ts'] == '')
		$ts = date("Ym");
	else
		$ts = $_POST['ts'];

	$results = gatherStorageCounts($ts);
	extract($results);
?>
	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border='0' width="100%" cellspacing=0 cellpadding=2>
				<tr>
					<td width='40%'>
						<b><font face="Verdana" size="2">Total Leads: <?= $total ?></font></b> 				
					<Br/>
						<font face="Verdana" size="2"><b>Income: $<?= number_format($total*PRICE,2,".",",") ?> </b>(@ $<?= PRICE ?>.00 /lead)</font>
					</td>
					<td rowspan="3" width="20%">&nbsp;</td>		
				</tr>
				<tr>
					<td width='40%'>
					<b><font face="Verdana" size="2">Total Leads today: <?= gatherLeadsToday() ?></font></b>					
					</td>
				</tr>
				<Tr><td>&nbsp;</td></tr>
				<tr>
					<td width='40%'>
						<b><font face="Verdana" size="2">	
							<form action="storageleads.php" method="post" name="sitefilter">	
								Period
								<?= printPeriodDropDown("ts",$_POST['ts']); ?><br/>
								<input type='submit' name="filter" value="Filter" />			
							</form>
						</font></b>
					</td>
				</tr>
			</table>
		</td></tr>
		<Tr><td>
<?
	echo "<table border='1'>";
	echo "<tr class='header' >";
	echo "<td class='white' >Category</td>";
	$month = substr($ts,4);
	$year = substr($ts,0,4);
	$days = getNDays($month,$year);
	$total_days_in_month = $days;
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
	
	$font_counter = 1;
	
	$last_company = 0;
	//$total = 0;	
	$day_count = 1;	
	
	$days = array_keys($data);
	sort($days);
	echo "\n<!--- ".print_r($days,true)." --->\n";
	echo "<tr class='on'>";
	echo "<td>Storage Leads</td>";
	foreach($days as $day)
	{
		//$day = intval($day);
		if ($day_count+1 < $day)//missing data.
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
		echo "<td>".$data[$day]."</td>";
		$day_count = $day;
	}
	
	if ($day_count < $total_days_in_month)
		echo "<td colspan='".($total_days_in_month - $day_count)."'>&nbsp;</td>";
	echo "<td>$total</td></tr>";
	echo "</table>";
	include "skin_bottom.php";

?>