<?
/* 
************************FILE INFORMATION**********************
* File Name:  affcopy_rob.php
**********************************************************************
* Description:  This runs the Affiliate copy to set up for the next month. Runs at EOM (End of Month)
**********************************************************************
* Creation Date:  2/29/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include "../../inc_mysql.php";

$sql = "insert into movingdirectory.leadbased_affiliates
(comp_name, ppl, lead_type, month, source,localppl)
select comp_name, ppl, lead_type, '".date("Ym",strtotime("+1 month"))."', source,localppl 
from movingdirectory.leadbased_affiliates where
 month = '".date("Ym")."';";

echo "$sql<br />";

/*

	include "../../inc_mysql.php";
	
	$check = "Select month,count(*) from movingdirectory.leadbased_affiliates ".
			" where month >= ".date("Ym")." group by month order by month asc";
	echo $check."<br/>";
	$rs = new mysql_recordset($check);	
	$rs->fetch_array();
	$thismonth = $rs->myarray['count'];
	if ($rs->fetch_array())
	{
		$nextmonth = $rs->myarray['count'];	
		if ($thismonth > $nextmonth)
		{
			$rs->close();
			$sql = "insert into movingdirectory.leadbased_affiliates".
					"(comp_name, ppl, lead_type, month, source,localppl)".
					"select comp_name, ppl, lead_type, '".date("Ym",time()+14*24*60*60).
					"', source,localppl from movingdirectory.leadbased_affiliates where ".
					" month = ".date("Ym").";";
			echo $sql."<br/>";
			$rs = new mysql_recordset($sql);
			$rs->close();
		}	
	}
	else//none for next month
	{
		$rs->close();
		$sql = "insert into movingdirectory.leadbased_affiliates".
				"(comp_name, ppl, lead_type, month, source,localppl)".
				"select comp_name, ppl, lead_type, '".date("Ym",time()+14*24*60*60).
				"', source,localppl from movingdirectory.leadbased_affiliates where ".
				" month = ".date("Ym").";";
		echo $sql."<br/>";
		$rs = new mysql_recordset($sql);
		$rs->close();
	}
	header("Location: affreporting.php");
*/
?>