<?
	include "../../inc_mysql.php";

	function addExistingCompany($campaign,$lead_id,$comp_id,$lead_count=1000,$month="xx")
	{
		if($month == "xx") $month = date("Ym");

		if ($lead_count < 1000 || $lead_count > 2000)
			$sort_order = 1;
		else 
			$sort_order = 2;

		if ($campaign == "atus")
		{
			$insert = "insert into movingdirectory.campaign set comp_id = '$comp_id', ".
						" lead_id = '$lead_id', cat_id = '1', monthly_goal='$lead_count', ".
						" sort_order = '$sort_order', site_id = 'atus', month = '$month', ".
						" needed = '$lead_count', leads_per_day = '40', active = 1 ;";
		}
		else if ($campaign == "auto")
		{
			$insert = "insert into marble.campaign set comp_id = '$comp_id', ".
						" lead_id = '$lead_id', cat_id = '1', monthly_goal='$lead_count', ".
						" sort_order = '$sort_order', site_id = 'auto', month = '$month', ".
						" needed = '$lead_count', leads_per_day = '40', active = 1 ;";
		}
		$rs = new mysql_recordset( $insert );
		$rs->close();
	}


	function setCustomFormat($campaign,$lead_id,$format,$format_type='email')
	{
		if ($campaign == "atus")
		{
			$insert = "insert into movingdirectory.lead_format set format_type = '$format_type', ".
						" lead_id = '$lead_id', cat_id = '1',".
						" lead_body = '$format';";
		}
		else if ($campaign == "auto")
		{
			$insert = "insert into marble.lead_format set format_type = '$format_type', ".
						" lead_id = '$lead_id', site_id = 'auto', cat_id = '1',".
						" lead_body = '$format';";
		}
		$rs = new mysql_recordset( $insert );
		$rs->close();
	}

	function useDefaultLeadFormat($campaign,$lead_id,$format)
	{
		if ($campaign == "atus")
		{
			$insert = "insert into movingdirectory.lead_format set format_type = 'email', ".
						" lead_id = '$lead_id', cat_id = '1',".
						" lead_body = '$format';";
		}
		else if ($campaign == "auto")
		{
			$insert = "insert into marble.lead_format set format_type = 'email', ".
						" lead_id = '$lead_id', site_id = 'auto', cat_id = '1',".
						" lead_body = '$format';";
		}

		$rs = new mysql_recordset( $insert );
		$rs->close();
	}

	function addEmail($campaign,$lead_id,$email)
	{
		if ($campaign == "atus")
		{
			$insert = "insert into movingdirectory.rules set email = '$email',".
						" lead_id = '$lead_id', name='Auto Transport Quote';";
		}
		else
		{
			$insert = "insert into marble.rules set lead_email = '$email',".
						" lead_id = '$lead_id', site_id = 'auto' ;";
		}
		$rs = new mysql_recordset( $insert );
		$rs->close();
		return 1;
	}

	function copyToMarble($comp_id)
	{
		$insert = "insert into marble.companies select * From movingdirectory.company  where comp_id = $comp_id;";
		$rs = new mysql_recordset( $insert );
		$rs->close();
	}
	
	function explainError($campaign,$error)
	{
		if ($campaign == "atus")
		{				
			$select[] = " This company has no format set up. ";
	
			$select[] = " This company has no email address set up. ";
	
			$select[] = " There is no information about this company set up. ";
		}	
		else if ($campaign == "auto")
		{
			$select[] = " This company has no format set up. ";
	
			$select[] = " This company has no email address set up. ";
	
			$select[] = " There is no information about this company set up. <a href='#' onClick='javascript:copyInfo();' class='black'>Copy Info</a> ";
		}
		$result = "Errors:<br/><ul>";
		$error = trim($error);
		for($i=0;$i<count($select);$i++)
		{		
			if (substr($error,$i,1)=="0")
				$result .= "<li>".$select[$i]."</li>";
		}	
		//mail("david@irelocation.com","AJAX",$error."\n".$result);
		return $result."</ul>";
	}

	function checkCompany($campaign,$comp_id,$lead_id)
	{
		if ($campaign == "atus")
		{
			//$select[] = " select comp_id from movingdirectory.campaign where comp_id = '$comp_id' and ".
					//  " lead_id = '$lead_id' and month = '".date("Ym")."' ";
	
			$select[] = " select lead_id from movingdirectory.lead_format where lead_id = '$lead_id' ";					 
	
			$select[] = " select lead_id from movingdirectory.rules where lead_id = '$lead_id' ";
					  
	
			$select[] = " select comp_id from movingdirectory.company where comp_id = '$comp_id' ";
		}	
		else if ($campaign == "auto")
		{
			//$select[] = " select comp_id from marble.campaign where comp_id = '$comp_id' and ".
					//  " lead_id = '$lead_id' and month = '".date("Ym")."' ";
	
			$select[] = " select lead_id from marble.lead_format where lead_id = '$lead_id' ".
					  " and site_id = 'auto' ";
	
			$select[] = " select lead_id from marble.rules where lead_id = '$lead_id' ".
					  " and site_id = 'auto' ";
	
			$select[] = " select comp_id from marble.companies where comp_id = '$comp_id' ";
		}
		$successes = "";		
		$mod = 1;
		foreach($select as $s)
		{
			$rs = new mysql_recordset($s);
			$successes .= ($rs->fetch_array()?"1":"0");
			$rs->close();
		}
		return $successes;
	}

	include "agent.php";
?>