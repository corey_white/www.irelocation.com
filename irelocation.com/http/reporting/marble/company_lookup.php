<?
	session_start();

	include "skin_top.php";
	$key = $_REQUEST['key'];


$body .= "	<div class=\"filterfont\">
	
	New Company Lookup<br/><br/>
	
	<form action=\"company_lookup.php\" method=\"get\">
		Company Name/Number:<input type=\"text\" name=\"key\" value=\"$key\" />
		<br/>	
		<input type=\"submit\" name=\"s\" value=\"Lookup\" />
	</form>
	";

if ( $s ) { //-- if there is a submission, process
	
	$sql = "select 					
	c.comp_id,
	c.comp_name,
	c.contact_name,
	c.contact_email,
	c.address1,
	c.address2,
	c.city,
	c.state,
	c.zip,
	c.phone_num,
	c.phone_ext,
	c.phone_fax,
	c.url,
	c.active,
	d.lead_id,
	d.cat_id,
	mc.comp_id as mc_comp_id
	
	from 
	movingdirectory.company as c
	left join movingdirectory.directleads as d on 
		c.comp_id = d.comp_id
	left join marble.companies as mc on mc.comp_id = c.comp_id 
	
	where ";
	
	$key = trim($key); //-- get rid of the pre- adn trailing spaces

	if ( is_numeric($key) ) { //-- if the submitted key is a number, then we look for the company ID
		$sql .= " c.comp_id = '$key' order by comp_name ";
	} else { //-- otherwise we search by company name
		$sql .= " c.comp_name like '%$key%' order by comp_name ";
	}
	
#	$body .= "$sql<br /><br />";

	$rs = new mysql_recordset($sql);
	$rs->fetch_array();	
	$data = $rs->myarray;
	
	//-- how many companies did we find?
	$comp_count = $rs->rowcount();
	
#	$body .= "comp_count = $comp_count";

	if (  $comp_count == 0 ) { //-- if no companies, inform
		$body .= "No companies match the search term: $key";
		
	} elseif ( $comp_count > 1 ) { //-- if the number of companies is more than 1, we need to list all of the companies found
		$body .= "<p>Found the following companies matching the search term:</p>\n";
		
		$body .= "<table cellpadding=\"2\">";
		
		while($rs->fetch_array()) {
			// $rs->myarray["name"] 
			
			$compName = $rs->myarray["comp_name"];
			$compID = $rs->myarray["comp_id"];

			$body .= "<tr>
				<td class=\"companyTitle\"><a class=\"blue\" href=\"company_lookup.php?key=$compID&s=Lookup\">$compID</a></td>
				<td class=\"companyTitle\"><a class=\"blue\" href=\"company_lookup.php?key=$compID&s=Lookup\">- $compName</a></td>
			</tr>";
		}
		$body .= "</table>";	

	} else { //-- okay, we only found 1
		
		$body .= "<table cellpadding=\"2\">";
		if ( $rs->myarray["active"] == 1 ) {
			$compStatus = "Active";
		} else {
			$compStatus = "Inactive";
		}
		if ( $rs->myarray["lead_id"] ) {
			$leadID = $rs->myarray["lead_id"];
		} else {
			$leadID = "<span style=\"color: red;\">Missing</span>";
		}
		
		$body .= "<tr><td class=\"companyTitle\" colspan=\"2\">".$rs->myarray["comp_name"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">Company ID</td><td class=\"companyNormal\">".$rs->myarray["comp_id"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">Lead ID</td><td class=\"companyNormal\">$leadID</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">Status</td><td class=\"companyNormal\">$compStatus</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">CAT ID</td><td class=\"companyNormal\">".$rs->myarray["cat_id"]."</td></tr>";
		
		$body .= "<tr><td class=\"companyTitle\">Contact:</td><td class=\"companyNormal\">".$rs->myarray["contact_name"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">&nbsp;</td><td class=\"companyNormal\">".$rs->myarray["contact_email"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">&nbsp;</td><td class=\"companyNormal\">".$rs->myarray["address1"]." ".$rs->myarray["address2"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">&nbsp;</td><td class=\"companyNormal\">".$rs->myarray["city"]." ".$rs->myarray["state"]." ".$rs->myarray["zip"]."</td></tr>";
		
		if ( $rs->myarray["phone_ext"] ) {
			$phoneExt = "ext. " . $rs->myarray["phone_ext"];
		} else {
			$phoneExt = "";
		}

		$body .= "<tr><td class=\"companyTitle\">Phone</td><td class=\"companyNormal\">".$rs->myarray["phone_num"]." $phoneExt</td></tr>";
		
		$body .= "<tr><td class=\"companyTitle\">Fax</td><td class=\"companyNormal\">".$rs->myarray["phone_fax"]."</td></tr>";
		$body .= "<tr><td class=\"companyTitle\">URL</td><td class=\"companyNormal\">".$rs->myarray["url"]."</td></tr>";
		
		$body .= "</table>";	
	}
	

} 



echo $body;
echo "</div>";

include "skin_bottom.php";
?>