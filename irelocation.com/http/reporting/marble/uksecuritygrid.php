<?
	define(CPC_DEBUG,true);
	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	define(SITES,"utac");
	define(CAMPAIGN,'uk');
	define(SITE_ID,'utac');
	define(SUBMIT_PAGE,"uksecuritygrid.php");
			
	define(ADTUK,1632);
	define(ABELALARM,1631);
	define(DEFENCE,1633);

	define(LEAD_SOURCE,"standard");
	define(CAMPAIGN_TYPE,"adshare");
	define(SHOW_CPC_GRAPH,true);
	
	/*
	OPERATIONAL NOTE:
	If you add or delete any companies from the above define statements, you need to change the ARRAY in the getTotalLeadRevenue function in graphs/security.php as well (really bad system - need to update this at some point)
	
	*/
	
	include "securitygrid.php";

?>f