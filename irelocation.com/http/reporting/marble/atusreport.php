<?
	session_start();
	define(CAMPAIGN,"atus");

	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		header("Location: login.php");
		exit();
	}
	else
	{
		$_SESSION['time'] = date("YmdHis");
	}
	define(DEVIATION,5);
	include "../../inc_mysql.php";	
	include "skin_top.php";

	
	function lastWeek()
	{		
		$msg = "";
		if ($_POST['ts'] == date("Ym") || $_POST['ts'] == "")
		{
			$lwtime = time()-7*24*60*60;
			$lastweek = date("YmdHis",$lwtime);
			$lastweekday  = date("Ymd",$lwtime);
			$sql = "select count(*) 'count' from marble.auto_quotes where ".
				  " received like '$lastweekday%' and received < '$lastweek' ".
				  " and campaign = '".CAMPAIGN."' ";
		
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$leads = $rs->myarray['count'];
			$msg = "We received $leads leads this time last ".date("l",$lwtime).".";
		}
		return $msg;
	}

	if (strlen($_POST['ts']) == 6 && $_POST['ts'] != date("Ym"))
	{
		$today = getNDays(substr($_POST['ts'],4),substr($_POST['ts'],0,4));
		$this_month = $_POST['ts'];
		$show_old_data = true;
		$days_in_month = $today;
		$days_left = 0;

		$today_sql = $today;
	}
	else
	{
		include "marbletrends.php";
		$today_sql = max(1,date("d")-1) + getTrends();
		$today = date("d");
		$this_month = date("Ym");
		$show_old_data = false;
		$days_in_month = date('t');
		$days_left = $days_in_month - $today + 1;
	}
	
	$sql = "Select co.comp_name,ca.* from marble.campaign as ca join marble.companies as co on co.comp_id = ca.comp_id ".
			" where ca.site_id = '".CAMPAIGN."' and ca.month = '$this_month'; ";
	$rs = new mysql_recordset($sql );
	while ($rs->fetch_array())
	{
		$rs->myarray['monthly_goal'] += $rs->myarray['temp_goal'];
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}
		

	if ($days_left == 0)
		$days_left = 1;
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month%' and campaign = '".CAMPAIGN."' ;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month$today%' and campaign = '".CAMPAIGN."' ;";
	$rs = new mysql_recordset($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	$default_order_by = "(monthly_goal+temp_goal) desc, comp_name asc";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	$sql = "select 
				co.comp_name,
				c.lead_id,
				c.temp_goal,
				(c.monthly_goal+c.temp_goal) 'Lead_Count',
				count(q.quote_id) 'Actual_Sent',
				ceiling((c.monthly_goal+c.temp_goal) / $days_in_month ) * $today_sql 'Est_Sent',	
				ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ ".($days_in_month-$today)." ) 'needed_per_day',	
				count(q.quote_id) - (ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ $days_in_month ) * $today_sql) 'deviation',
				c.monthly_goal - count(q.quote_id) 'leads_needed',
				c.active
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join marble.companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = '".CAMPAIGN."'
				and q.campaign = '".CAMPAIGN."'
				AND q.received like '$this_month%'
				and c.month = '$this_month'
			group by
				c.lead_id
			order by
				$default_order_by;";

	echo "<!--- $sql --->";
	$rs = new mysql_recordset($sql);
	
	
?>	
<table border="0" width="100%" cellspacing=0 cellpadding=2>	
	<Tr><td>&nbsp;</td></tr>
	<Tr>
		<td>
		<form action="<?= CAMPAIGN ?>report.php" method="post">
			<? printPeriodDropDown('ts',$this_month); ?>
			<input type='submit' name="go" value="Go" />
		</form>
		</td>
	</Tr>
	<Tr><td>&nbsp;</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Today is the: <?= date("jS \o\f F"); ?></font></b>
	</td></tr>
	<tr><td>
		<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
	</td></tr>
	<? if ($_POST['ts'] == date("Ym") || $_POST['ts'] == '') { ?>
	<tr><td>
		<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
	</td></tr>
	<tr>
		<td>
			<b><font face="Verdana" size="2"><?= lastWeek() ?></font></b>
		</td>
	</tr>
	<? } ?>
	<Tr><td>
	
<div align="right">
	<font face="Verdana" size="2">
	<font color="#FF0000">Greater than <?= DEVIATION ?>% differance</font>
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <Br/>
	<font color="#006600">Less than <?= DEVIATION ?>% differance</font>
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <Br/>
	</font>
</div>
	
	
<?	
	echo "<table border='1'>";
	echo "<tr class='header' ><td>&nbsp;</td>".
			"<th align='left' >".getSortLink($default_order_by,"comp_name","Company")."</th>".
			"<th>".getSortLink($default_order_by,"c.lead_id","Lead ID")."</th>".
			"<th>".getSortLink($default_order_by,"c.monthly_goal","Monthly Goal")."</th>".
			"<th>".getSortLink($default_order_by,"Actual_Sent","Sent")."</th>".
			"<th>".getSortLink($default_order_by,"Est_Sent","Est. Sent")."</th>".
			"<th>".getSortLink($default_order_by,"deviation","Deviation")."</th>";
	if (!$show_old_data)
		echo "<th>".getSortLink($default_order_by,"needed_per_day","Needed Per Day")."</th>";
	echo "<th>".getSortLink($default_order_by,"leads_needed","Leads Needed")."</th></tr>";
	$count = 1;
	
	$font_counter = 1;
	
	while($rs->fetch_array())
	{
		
		extract($rs->myarray);
		
		$companies[$lead_id] = 0;
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count++."</td><td>";
		if (!$active)
			echo "<font color='red'>INACTIVE </font>";
		echo "<a class='black' title='Click to view lead format' href='".CAMPAIGN."format.php?lead_id=$lead_id'>";
		echo "$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		
		if ($temp_goal > 0)
			echo "<font color='#006600'>+$temp_goal</font> ";
		else if ($temp_goal < 0)
			echo "<font color='#FF0000'>$temp_goal</font> ";
		
		echo $Lead_Count."</td><td align='right'>";
		echo $Actual_Sent."</td><td align='right'>";
		echo (($active)?floor($Est_Sent):"--")."</td><td align='right'>";
		
		if ($active)
			echo colorCode($Actual_Sent,floor($Est_Sent),DEVIATION);
		else
			echo " -- ";
			
		echo "</td>";
		
		if (!$show_old_data)
		{
			echo "<td align='right'>";		
			echo (($active)?$needed_per_day:"--")."</td>";
		}		
		echo "<td align='right'><strong>".(($active)?$leads_needed:"--")."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}
	
	foreach($companies as $comp)
	{		
		if ($comp == 0)
			continue;
		else
			extract($comp);
			
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count."</td><td>";
		$count++;
		if (!$active)
			echo "<font color='red'>INACTIVE </font>";
		echo "<a class='black' title='Click to view lead format' href='".CAMPAIGN."format.php?lead_id=$lead_id'>";
		echo "$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		echo "$monthly_goal</td><td align='right'>";
		echo "0</td><td align='right'>";
		echo "-</td><td align='right'>";
		
		$deviation = $monthly_goal;
		if ($deviation > 0)
			$deviation = "<font color='#000000'><strong>$deviation</strong></font>";
		else if ($deviation < 0)
			$deviation = "<font color='#FF0000'>$deviation</font>";
		else 
			$deviation = "<font color='#00FF00'>$deviation</font>";
			
		echo $deviation."</td>";
		
		if (!$show_old_data)
		{
			echo "<td align='right'>-</td>";
		}
		echo "<td align='right'><strong>".$monthly_goal."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}
	
	
	include "skin_bottom.php";
	
?>