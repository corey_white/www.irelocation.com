<?
	extract($_POST);
	
	include "../../inc_mysql.php";
	include "marblehelper.php";

	if ($action == "atus_percentage")
	{
		$new = (int) $new_atus_percentage;
		if ($new >= 00 && $new <= 100)
		{
			if (((int) $current) != $new)
			{
				set("atus_percentage",$new);
				header("location: movingpercentage.php?good=changed");
				exit();
			}
			else
			{
				header("location: movingpercentage.php?good=nochange");
				exit();
			}
		}
		else
		{
			header("location: movingpercentage.php?bad=range");
			exit();
		}			
	}
	else if ($action == "vanlines")
	{		
		$new = (int) $new_percent;
		if ($new >= 00 && $new <= 100)
		{
			if (((int) $current) != $new)
			{
				set("vanlines_percentage",$new);
				header("location: movingpercentage.php?good=changed");
				exit();
			}
			else
			{
				header("location: movingpercentage.php?good=nochange");
				exit();
			}
		}
		else
		{
			header("location: movingpercentage.php?bad=range");
			exit();
		}
	}
	else if ($action == "autooptin")
	{		
		if ($current == $atus)//no change!
		{
			header("location: movingpercentage.php?good=atusnochange");
			exit();
		}
		else
		{
			set("moving_to_atus",$atus);
			header("location: movingpercentage.php?good=atuschanged");
			exit();
		}
	}	
	else if ($action == "topmoving_cap")
	{			
		if ($current == $new_mover_cap)//no change!
		{
			header("location: movingpercentage.php?good=capnochange");
			exit();
		}
		else
		{
			set("topmoving_cap",$new_mover_cap);
			header("location: movingpercentage.php?good=capchanged");
			exit();
		}
	}
?>