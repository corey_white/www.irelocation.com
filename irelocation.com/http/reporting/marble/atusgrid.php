<?
	session_start();
	define(CAMPAIGN,"atus");
	define(COMPS_PER_LEAD,7);
	define(CPC_DEBUG,true);
	include "graphs/atus.php";
	include "../../inc_mysql.php";
	$timeframe = ($_POST['ts']=="")?date("Ym"):$_POST['ts'];
	$engine = ($_POST['engine']=="")?"all":$_POST['engine'];
	$site = ($_POST['site']=="")?"all":$_POST['site'];

	$graph = new AtusGraph($timeframe,$engine,$site);
	
	 //new TasGraph($timeframe,$engine,$site);
	
	echo "<!--- ".$_SESSION['lead_id']." --->";
	
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] != "uberadmin")
	{		
		if (strlen($_SESSION['user']) > 0)
		header("Location: ".CAMPAIGN."individualgrid.php");
		exit();
	}
	else
		$_SESSION['time'] = date("YmdHis");
	

	$companies = $graph->loadCompanyArray();
	 include "skin_top.php"; 
		
	$today = date("d");
	$days_in_month = date('t');
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	

	$sqlFilter = $graph->getSqlFilter($timeframe,$engine,$site);	
	
	
	$total_leads = $graph->getTotalLeads($timeframe,$engine,$site);

	if ($timeframe == date("Ym"))
		$total_leads_today = $graph->getTotalLeads(date("Ymd"),$engine,$site);
	else
		$total_leads_today = 0;
	
	$rs = new mysql_recordset($graph->getGridSql($timeframe));
	
	
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					marble.auto_quotes as q
				where 					
					$sqlFilter
				group by
					substring(received,7,2)";
	
	$total_rs = new mysql_recordset($total_sql);
?>	
<link rel='stylesheet' type='text/css' href='marble.css' />


	<table border="0" width="100%" cellspacing=0 cellpadding=2>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
		<?	$graph->showGraph($timeframe,$engine,$site);	?>
		</td></tr>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border='0' width="100%" cellspacing=0 cellpadding=2>
				<tr>
				<td width='40%' class='filterfont'>
			Total Leads: <?= $total_leads ?>
		</td>
		<td rowspan="3" width="20%">&nbsp;</td>
		<td rowspan="3" valign="top" class='filterfont'>
			
			<?	list($revenue,$cpl,$cost) = $graph->getData($timeframe);	?>
			Total Cost: $<?= $cost ?><br/>
			<? if (substr_count($revenue,"hit") == 0) echo "Total Revenue: \$"; echo $revenue; ?><br/>
			Cost Per Lead: $<?= $cpl ?><br/>
			
		</td>
		</tr>
		<tr>
			<td width='40%' class='filterfont'>
			Total Leads today: <?= $total_leads_today ?>
			</td>
		</tr>
		<tr>
			<td width='40%'>			
			<form action="<?= CAMPAIGN ?>grid.php" method="post" name="sitefilter">	
				<?	$graph->printFilter($timeframe,$engine,$site); ?>
			</form>
			</td>
		</tr>
			</table>
		</td></tr>
		<Tr><td>

<?		
	if ($total_leads == 0)
	{
		echo "<b><font face='Verdana' size='2'>No Leads.</font>";
		exit();
	}
	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	$days = date('t');
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left' class='white'>Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count <= $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
	}
	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	while($rs->fetch_array())
	{
		$append = "";
		if ($_POST['site'] != "all" && $_POST['filter'] == "Filter")
			$append = "&site=".$_POST['site'];
		if ($_POST['engine'] != "all" && $_POST['filter'] == "Filter")
			$append = "&engine=".$_POST['engine'];
				
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days)//not a full month of data yet.
			{
				echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{		
			echo "<tr class='$style'><td class='$style' >".($font_counter - 1)."</td>";
				
			$link = "<a href='".CAMPAIGN."individualgrid.php?lead_id=".$lead_id."' class='black'>$comp_name</a>";
			echo "<th align='left'>";
			if (!$active)
				echo "<font color='red'>INACTIVE </font>";
			echo "$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black'  href='".CAMPAIGN."viewleads.php?ts=$ts$day$append'>$count</a>";
		else
			$link = "<A class='black'  href='".CAMPAIGN."viewleads.php?ts=$ts$day&lead_id=".$lead_id."$append'>$count</a>";
		
		if (intval($day) == $day_count)//
			echo "<th>$link</th>";
		$total += $count;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	
	if (strlen($_SESSION['lead_id']) > 0)
		$link = "<A class='black'  href='".CAMPAIGN."viewleads.php?ts=$ts$append'>$total</a>";
	else
		$link = "<A class='black' href='".CAMPAIGN."viewleads.php?ts=$ts&lead_id=".$lead_id."$append'>$total</a>";
	
	echo "<td  align='right'>$link</td></tr></table>";

	include "skin_bottom.php";
?>