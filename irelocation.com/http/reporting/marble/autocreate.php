<?
	session_start();

	include "skin_top.php";
	include_once "../../inc_mysql.php";
	
	$comp_id = $_REQUEST['comp_id'];
	
	
	if ($comp_id == "")
	{
		if ($_REQUEST['created'] != "")
		{
			$comp_id = $_REQUEST['created'];
			echo "Company #$comp_id created. ";		
		}
		else
		{
			?>
			<form action="autocreate.php" method="get">
				Comp ID: <input type="text" name="comp_id" />
				<br/>
				<input type="submit" value="Look Up" />
			</form>
			<?
		}
		exit();		
	}
	$sql = "select * from marble.companies where comp_id = $comp_id ";
	$sql2 = "select d.lead_id,r.lead_email as 'email' from movingdirectory.directleads as d ".
			"left join marble.rules as r on r.lead_id=d.lead_id ".
			" where d.comp_id = $comp_id ";
	echo "<!--- $sql2 --->";
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();	
	$company = $rs->myarray;
	$rs->sql = $sql2;
	$rs->query();	
	$rs->fetch_array();
	$email = $rs->myarray['email'];	
	$lead_id = $rs->myarray['lead_id'];
	
	if ($lead_id != "")
	{
		$rs->sql = "select format_type,lead_body from marble.lead_format where lead_id = $lead_id ".
					" and length(lead_body) = 32 and format_type = 'jtxml' limit 1 ";
		
		$rs->query();	
		$rs->fetch_array();
		$jtid = $rs->myarray['lead_body'];
		$format_type = $rs->myarray['format_type'];
	}
	else $jtid = "";
	
?>
<script>
	var old_index = 0;
	
	function swapJT(index)
	{
		var jt = index == 2;
	
		document.forms.start.lead_body.disabled = jt;			
		document.forms.start.email.disabled = jt;			
		document.forms.start.jtid.disabled = !jt;			
		
		if (jt)
		{
			document.getElementById('email').innerHTML = "( Email Automatically Assigned. )";
			document.getElementById('body').innerHTML = "( Lead Body Automatically Assigned. )";
		}	
		else
		{
			document.getElementById('email').innerHTML = "&nbsp;";
			document.getElementById('body').innerHTML = "&nbsp;";
		}
	}

</script>
<form action="autocreate_do.php" method="post" name="start">
	<input type="hidden" name="comp_id" value="<?= $comp_id ?>" />
		<input type="hidden" name="lead_id" value="<?= $lead_id ?>" />
<table class='filterfont'>
	<? if ($lead_id == "") { ?>
	<tr>
		<td><font color="#FF0000">Error: </font></td>
		<td>Make Lead Campaign in 1st Moving Directory first!</td>
	</tr>
	<? } ?>
	<tr>
		<td>Company: </td>
		<td><?= $company['comp_name'] ?></td>
	</tr>
	<tr>
		<td>Format Type:</td>
		<td> <select name="format_type" onchange="swapJT(this.selectedIndex);">
					<option value="email" <?= ($format_type=="email")?"selected ":"" ?>>Email</option>
					<option value="xml" <?= ($format_type=="xml")?"selected ":"" ?>>XML</option>
					<option value="jtxml" <?= ($format_type=="jtxml")?"selected ":"" ?>>JTracker</option>
					<option value="post" <?= ($format_type=="post")?"selected ":"" ?>>POST</option>
				</select>
		</td>
	</tr>
	<tr>
		<td valign="top">Lead Email:</td>
		<td><input type="text" name="email" value="<?= $email ?>" />
			<Br/>
			<span id='email'>&nbsp;</span>
		</td>
	</tr>
	<Tr>
		<td>Month:</td>
		<td> <select name="month">
			<?
				$months = array(date("Ym"),date("Ym",strtotime(" +1 Month",time())),	
							date("Ym",strtotime(" +2 Month",time())));
				foreach($months as $m)
					echo "<option value='$m'>$m</option>\n";
			?>
			</select>
		</td>
	</Tr>
	<Tr>
		<td>Campaign:</td>
		<td> <select name="campaign">
				<option value="auto">Top Auto</option>
				<option value="atus">Auto-Transport.us</option>
			</select>
		</td>
	</Tr>
	
	<tr>
		<td>Lead Quota:</td>
		<td> <select name="monthly_goal">
			<? for ($i = 500; $i < 10000; $i += 500) { ?>
				<option value="<?=$i?>"><?=$i?></option>
			<? } ?>	
			 </select>
		</td>
	</tr>
	<tr>
		<td>Non-Compete Lead ID:</td>
		<td><input type="text" name="noncompete" /></td>
	</tr>
	<tr>
		<td>JTracker Broker ID:</td>
		<td><input type="text" name="jtid" value="<?= $jtid ?>" lang="32"
			 maxlength="64" <?= ($jtid=="")?"disabled='true'":"" ?>/></td>
	</tr>
	<tr>
		<td valign="top">Lead Body:</td>
		<td><textarea name="lead_body" rows="15" cols="30"
			/> </textarea>
			<Br/>
			<span id='body'>&nbsp;</span>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><input type="submit" /></td>
	</tr>
</table>
	
</form>