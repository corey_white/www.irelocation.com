<?
	session_start();
	include_once "../../inc_mysql.php";
	
	define(AUTO,"auto");
	define(MOVING,"moving");
	define(SECURITY,"security");


	include "skin_top.php";
?>
<script language="javascript">
	function validate(tform)
	{
		if (tform.comp_name.value == "")
			alert("Enter the Company's Name");
		else if (tform.source.value.length < 2)
			alert("Enter the Company's Source Tag.");
		else if (tform.ppl.value == "")		
			alert("Enter the Company's Payment per Lead.");
		else if (isNaN(tform.ppl.value))
			alert("Enter the Company's Payment per Lead.");		
		else if (tform.month.value == "")
			alert("Enter the Month for this company to be active.");	
		else
			tform.submit();
	}
</script>

	<form action="affadd.php" method="post" name="affadd">
		<? if (isset($_REQUEST['dupe'])) { ?>
			<font color="#ff0000">Error, that campaign is already accounted for.</font>
		<? } ?>
		<table>
			<tr>
				<td colspan="2" align="left"><Strong>Add an Affiliate</Strong></td>
			</tr>
			<tr>
				<td align="left">Company Name</td>
				<td align="left"><input type="text" name="comp_name" /></td>
			</tr>
			<tr>
				<td align="left">Source Tag</td>
				<td align="left">
					<input type="text" name="source" />
					<font size="-1">(ex <strong>autoextra</strong>)</font>
				</td>
			</tr>
			<tr>
				<td align="left">Payment per Lead</td>
				<td align="left"><input type="text" name="ppl" maxlength="5" size="8" /> 
					<font size="-1">(ex 4.50)</font>
				</td>
			</tr>
			<tr>
				<td align="left">Month</td>
				<td align="left"><input type="text" name="month" value="<?= date("Y/m") ?>" /></td>
			</tr>
			<tr>
				<td align="left">Lead Type</td>
				<td align="left">
					<select name="lead_type">
						<option value="auto">Auto Leads</option>
						<option value="moving">Moving Leads</option>
						<option value="security">Security Leads</option>
						<option value="realestate">Real Estate Leads</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<input type="button" value="Add Company" 
						onclick="validate(document.forms.affadd);" /></td>
			</tr>
		 </table>
	</form>
		


<?	include "skin_bottom.php";	?>

	