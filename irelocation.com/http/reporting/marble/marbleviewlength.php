<? 	
	session_start();
	include "skin_top.php";
	define(MAX_DUR,300);

	function getDistinctPages($site)
	{
		$length = strlen($site)+1;
		$keyword = str_replace("www.","",$site);
		$sql = "select 
					replace(replace(url,'$keyword/',''),'www.','') 'url',
					avg(duration) 'duration',
					count(distinct(ip)) 'visitors'
				from movingdirectory.viewlength where url like '%$keyword%'
				and duration < ".MAX_DUR."
				group by url;";

		$rs = new mysql_recordset($sql);
		$results = array();
		while($rs->fetch_array())
			$results[] = $rs->myarray;	
		$rs->close();
		return $results;
	}	

	function retrieveSitefromCode($code)
	{
		$sites = array();
		$sites['cs'] = "www.carshipping.com";
		$sites['tsc'] = "www.topsecuritycompanies.com";
		$sites['tm'] = "www.topmoving.com";
		$sites['tac'] = "www.topalarmcompanies.com";
		if (strlen($sites[$code]) > 0) return $sites[$code];
		else { echo "Site not found."; exit(); }
	}

	function gatherStats($site,$ts="x")
	{
		$queries = array();
		$condition = " AND url like '$site%' ";
		if ($ts == "x") $ts = date("Ymd");
		$condition .= " AND received like '$ts%' ";
		$queries[] = "select 'clicks' as name, count(distinct(ip)) 'clicks' from movingdirectory.viewlength where 1=1 $condition";
		$queries[] = "select 'conversions' as name, count(*) 'conversions' from movingdirectory.viewlength where url like '%thankyou%' $condition";
		$queries[] = "select 'quote' as name, avg(duration) 'quote' from movingdirectory.viewlength where url like '%quote%' and duration < ".MAX_DUR." $condition ";
		$queries[] = "select 'index' as name, avg(duration) 'index' from movingdirectory.viewlength where url like '%index%' or url = '$site/' and duration < ".MAX_DUR." $condition ";
		$queries[] = "select 'thankyou' as name, avg(duration) 'thankyou' from movingdirectory.viewlength where url like '%thankyou%' and duration < ".MAX_DUR." $condition ";
		$results = array();
		foreach($queries as $q)
		{
			$rs = new mysql_recordset($q);
			$rs->fetch_array();
			$n = $rs->myarray['name'];
			$results[$n] = $rs->myarray[$n];
			$rs->close();
		}
	
		$percentage = twodecimals($results['conversions']/$results['clicks'] * 100);
		$results['percentage'] = $percentage;
		return $results;
	}	
	
	if (isset($_REQUEST['code'])) $code = $_REQUEST['code'];
	else $code = "cs";

	if ($code != "cs" && $code != "tsc" && $code != "tac")
	{
		echo "<font family='Verdana'>";
		echo "Only <a href='marbleviewlength.php?code=cs' style='color:#0000FF'>CarShipping</a>, <a href='marbleviewlength.php?code=tac' style='color:#0000FF'>TopAlarmCompanies</a>, and <a href='marbleviewlength.php?code=tsc' style='color:#0000FF'>TopSecurityCompanies</a> are being tracked.";
		echo "</font>";
		include "skin_bottom.php";
		exit();
	}	

	$site = retrieveSitefromCode($code);
	$name = substr($site,4);
	$results = gatherStats($site,"200703");
	extract($results);
?>	
	<font face="Verdana" size="2" >
	<strong><?= ucwords($name) ?> Details</strong><Br/><Br/>
	<style> th { text-align:left; } </style>
	Conversions: <?= $percentage ?>% <Br/>
	Average View Time:
	<ul>
	<li>Index Page: <?= twodecimals($index) ?> seconds.</li>
	<li>Quote Page: <?= twodecimals($quote) ?> seconds.</li>
	<li>Thank You Page: <?= twodecimals($thankyou) ?> seconds.</li>
	</ul>
	<style>
		a
		{
			color:#0000FF;
			text-decoration:none;
		}

	</style>
	<Table><tr><th>Page</th><th>Duration</th><th>Visitors</th></tr>
	<?
		$data = getDistinctPages($site);
		foreach($data as $page)
		{
			if ($page['url'] == "" || $page['url'] == "/") $page['url'] = "index.php";
			echo "<tr><td><a target='_blank' href='http://$site/".$page['url']."'>".$page['url']."</a></td>";
			echo "<td>".twodecimals($page['duration'])."</td>";
			echo "<td>".$page['visitors']."</td></tr>";
		}
	?>
	</Table>
	</font>
<?
	include "skin_bottom.php"; 
?>

