<?
	session_start();
	include "skin_top.php";
	include "data/tracking.php";
	
	$website = ($_POST['website']!="")?$_POST['website']:1;
	$engine = ($_POST['engine']!="")?$_POST['engine']:1;
	$timeframe = ($_POST['timeframe']!="")?$_POST['timeframe']:date("Ym");
	$sortby = ($_REQUEST['sortby']!="")?$_REQUEST['sortby']:"keyword";
	
	$t = new Tracking($website,$timeframe,$sortby);


	$t->query();

	function printSortLink($thisone,$current)
	{
		if ($thisone." desc" == $current)//currently desc
			echo "<A style='color:#0000FF; text-decoration:none;' ".
					"href='tkclicks.php?sortby=$thisone+asc'><strong>^</strong></A>";
		else
			echo "<A style='color:#0000FF; text-decoration:none;' ".
					"href='tkclicks.php?sortby=$thisone+desc'>v</A>";		
	}
	
?>
	<style>
		td
		{
			font:Verdana;
			font-size:14px;
		}	
	</style>
	<form action="tkclicks.php" method="post">
		<input type="hidden" name="type" value="month"/>
		<select name='website'>
			<option value="1">CarShipping.com</option>
		</select> - 
		<select name="engine">
			<option value="1">Google</option>
		</select> - 
		<select name="timeframe">
			<option value="<?= date("Ymd") ?>">today</option>
			<option value="<?= date("Ymd",time()-24*60*60) ?>">yesterday</option>
			<option value="200705" disabled>--</option>			
			<option value="200705">May 2007</option>
		</select>
		<input type="submit" value="Update" />
	</form>
	<Br/>
	<? if ($timeframe == date("Ymd")) 
			echo "<strong>Note:</strong> Data for today ".
				" has a 15 minute lag to account for users currently on the site.<Br/>"; 
	?>
	<Br/>
	<strong>Total Clicks:</strong><?= $t->totalClicks ?><br/>
	<strong>Total Conversions:</strong><?= $t->conversions ?><br/>
	<strong>Conversion Rate:</strong><?= number_format($t->conversionRate,2,".",",") ?>%<br/>
	<Br/><Br/>
	<table width="500">
		<tr>
			<th><? printSortLink("keyword",$t->sortby); ?> Keyword </th>
			<th align="left"><? printSortLink("clicks",$t->sortby); ?> Clicks</th>
			<th><? printSortLink("conversions",$t->sortby); ?> Conversions</th>
			<th><? printSortLink("convperc",$t->sortby); ?> Conv %</th>
		</tr>	
<?
	

	foreach($t->data as $data)
	{	
		$cl = $data['clicks'];
		$cv = ($data['conversions']==0)?"0":$data['conversions'];
		$cp = $data['convperc'];
		$keyword = $data['keyword'];
		$kid = $data['keywordid'];
		$keyword = "<a href='tkkeyword.php?kid=$kid&keyword=".urlencode($keyword)."' style='color:#0000FF; text-decoration:none;' >".
					$keyword."</a>";
		
		
		echo "<tr><td>".$keyword."</td><td>".$cl."</td><td>".
				$cv."</td><td>".number_format($cp,2,".",",").
				"%</td></tr>\n";
	}
	echo "</table>";
	include "skin_bottom.php";
?>