<?
/* 
************************FILE INFORMATION**********************
* File Name:  marblecopy_rob.php
**********************************************************************
* Description:  This copies the campaigns from the current month to the next month.  To be ran at End of Month (EOM)
**********************************************************************
* Creation Date:  2/29/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

NOT WORKING YET
*/

echo "NOT WORKING YET";
exit;

session_start();

include "../../inc_mysql.php";


/*
	This script copies all campaign entries for Atus from this month
	to next month that haven't been cancelled. all companies by default
	are set to active, and the temp_goal field is cleared out.	
*/

$sql = "
		insert into marble.campaign
		(lead_id,comp_id,monthly_goal,leads_per_day,
		site_id,sort_order,needed,active,month,temp_goal)
		
		select 
		lead_id,comp_id,monthly_goal,leads_per_day, 
		site_id,sort_order,monthly_goal as 'needed',1 as 'active',
		".date("Ym",strtotime("+1 month"))." as 'month',0 as 'temp_goal'
		from 
		marble.campaign where site_id = 'auto' and ".
		" month = '".date("Ym")."' ".
		" and cancelled is null ";

echo "$sql<br />";

/*
$rs = new mysql_recordset($sql);

if ( $rs ) {
    echo "Copy Successful<br />";
} else {
    echo "Copy Not Successful<br />";
}
*/

?>