<?
	session_start();
	if($_REQUEST['ts'] == "")
		define(YESTERDAY, date("Ymd",time()-24*60*60));
	else
		define(YESTERDAY, $_REQUEST['ts']);
	include "skin_top.php";
	
	
	//include "../../inc_mysql.php";
?>
	<script>
		showRow = (navigator.appName.indexOf("Internet Explorer") != -1) ? "block":"table-row";
		function flipCell(div)
		{
			f = document.getElementById(div);
			f.style.display = (f.style.display == "none") ? showRow : "none";
		}
	</script>
	
	<Table width='500'>
		<tr>
			<Th width="30%">Site</Th>
			<th width="15%">G</th>
			<th width="15%">O</th>
			<th width="15%">M</th>
			<th width="15%">Total</th>
		</tr>		
		<tr><td colspan="5"><hr/></td></tr>
<?

	//damn google local..
	function getLocalCPC($site,$ts="*")
	{
		if ($ts == "*" ) $ts = YESTERDAY;
		$select = " select cost From movingdirectory.costperclick where site = '".$site."-local' and ".
			" search_engine = 'google' and ts = '$ts' ";
		
		$rs = new mysql_recordset($select);		
		$rs->fetch_array();
		$cost = $rs->myarray['cost'];
		return ($cost=="")?0:$cost;
		//return $rs->myarray['cost'];
	}	

	function getSql($category,$ts="*")
	{
		$campaign = "";
		echo "<!-- category: $category --->";
		if ($ts == "*" ) $ts = YESTERDAY;
		switch($category)
		{
			case "auto": 
				$table = "marble.auto_quotes"; 
				$site = ""; 
				break;
			case "moving": 
				$site = "tm"; 
				$table = "movingdirectory.quotes"; 
				break;			
			case "moving_local": 
				$category = "moving"; 
				$site = ""; 
				$table = "movingdirectory.quotes_local"; 
				break;			
			case "atus": 
				$table = "movingdirectory.quotes"; 
				$site = ""; 
				break;			
			case "us-security": 
				$table = "irelocation.leads_security"; 
				$site = "tsc"; 
				$campaign = " and maq.campaign = 'us' ";
				//$category = "security";
				break;		
			case "ca-security": 
				$table = "irelocation.leads_security"; 
				$site = "ctsc"; 
				$campaign = " and  maq.campaign = 'ca' ";
				//$category = "security";
				break;		
		}
		
		if ($site != "")
			$local = " +if(q.site='".$site."' AND q.engine = 'google',".getLocalCPC($site,$ts).",0) ";
		else
			$local = " ";
		
		$sql = "select 
					q.site,
					q.engine,
					count(maq.quote_id) 'leads',
					if
					(
						haslocal=1,
						(select cost from movingdirectory.costperclick as cpc2 where 
						 cpc2.site = concat(cpc.site,'-local') and cpc2.ts = '$ts'
						)
						,0
					) +cpc.cost $local 'total_cpc'
				from 
					movingdirectory.costperclick as cpc 
					join 
					movingdirectory.dailyqueries q 
					on
					q.site = cpc.site
					and q.engine = cpc.search_engine
					left join $table as maq
					on
					maq.source like concat(q.site,'_',q.engine,'%')					
				where
					cpc.ts = '$ts'
					$campaign 
					and maq.received like '$ts%'
					and q.category = '$category'
					group by q.site, q.engine;
					";
		echo "\n<!--- $sql --->\n";			
		return $sql;
	}

	$categories = array("auto","atus","moving","us-security","ca-security");
	$order = array("moving" => array("tm","pm","me"), "auto" => array("asc","cs","mmc","cq"),
			 "atus" => array("atus","123cm"), "us-security" => array("tsc","tac") ,
			 "ca-security" => array("ctsc","ctac")	);
	
	foreach($categories as $cat)
	{
		$rs = new mysql_recordset(getSql($cat));
		$data = array();
		
		while($rs->fetch_array())
			$data[$rs->myarray['site']][$rs->myarray['engine']] = $rs->myarray;					
		$rs->close();
	
		
		if ($cat == "moving")
		{
			$rs = new mysql_recordset(getSql("moving_local"));
			while($rs->fetch_array())
			{
				$s = $rs->myarray['site'];
				$e = $rs->myarray['engine'];
				$leads = $rs->myarray['leads'];
				$data[$s][$e]['leads'] += $leads;
			}
		}
		
		$ordered = $order[$cat];
		
		
		foreach($ordered as $sitecode)
		{	
			$site = $data[$sitecode];
			$total_leads = 0;
			$total_cost = 0;
			$total_cpl = 0;
			
			$gl = $site["google"]["leads"];
			$gc = $site["google"]["total_cpc"];
			$gl = ($gl=="")?0:$gl;
			$gcpl = ($gl != 0)?($gc/$gl):$gc;
			$total_leads += $gl;
			$total_cost += $gc;
			
			$ol = $site["overture"]["leads"];
			$oc = $site["overture"]["total_cpc"];
			$ol = ($ol=="")?0:$ol;
			$ocpl = ($ol != 0)?($oc/$ol):$oc;
			$total_leads += $ol;
			$total_cost += $oc;
			
			$ml = $site["msn"]["leads"];
			$mc = $site["msn"]["total_cpc"];
			$ml = ($ml=="")?0:$ml;
			$mcpl = ($ml != 0)?($mc/$ml):$mc;
			
			$total_leads += $ml;
			$total_cost += $mc;
			$total_cpl = ($total_leads!=0)?($total_cost/$total_leads):$total_cost;
			
			
			
			echo "<tr>";
			
			echo "<td>$sitecode</td>";
			
			echo "<td>".$gl." / \$".color($cat,$gcpl)."</td>";
			echo "<td>".$ol." / \$".color($cat,$ocpl)."</td>";
			echo "<td>".$ml." / \$".color($cat,$mcpl)."</td>";
			echo "<td>".$total_leads." / \$".color($cat,$total_cpl)."</td>";
			echo "</tr>\n";
			
		
		}
		echo "<tr><td colspan='5'>&nbsp;</td></tr>\n";
	}	
	
	function color($category,$cpl)
	{
		$BAD = "#FF0000";
		$DANGER = "#FF6600";
		$color = "#000000";
		if ($category == "moving")
		{
			if ($cpl > 13) $color = $BAD;
			else if ($cpl > 10) $color = $DANGER;
			
		}
		else if ($category == "atus" || $category == "auto")
		{
			if ($cpl > 7) $color = $BAD;
			else if ($cpl > 6) $color = $DANGER;
		}
		else if ($category == "ca-security" || $category == "us-security")
		{
			if ($cpl > 30) $color = $BAD;
			else if ($cpl > 20) $color = $DANGER;
			
		}
		if ($color != $BAD)
			return "<font color='$color'>".twodecimals($cpl)."</font>";
		else
			return "<font color='$color'><strong>".twodecimals($cpl)."</strong></font>";
	}
	
	//$rs->close();
	include "skin_bottom.php";
?>