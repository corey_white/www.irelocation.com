<?
	session_start();	
	//echo "\n<!--- ".print_r($_SESSION,true)." --->\n";
	$admin = ($_SESSION['user'] == "uberadmin");
	if ($_SESSION['user'] == "autotransporter")
	{		
		$lead_id = $_SESSION['lead_id'];		
		$_SESSION['time'] = date("YmdHis");
		
	}
	else if ($_SESSION['user'] == "uberadmin")
	{
		$_SESSION['time'] = date("YmdHis");
		
		if(strlen($_REQUEST['lead_id']) >= 0)
			$lead_id = $_REQUEST['lead_id'];
		else
		{
			header("Location: marblereport.php");
			exit();	
		}
	}
	else
	{
		header("Location: login.php");
		exit();	
	}
	
	

	include "../../marble/inc_mysql.php";
	
	$sql = "select
	r.lead_email,
	ca.active,
	co.phone_num,
	co.comp_name,
	lf.format_type,
	ca.*,
	if(lf.lead_body<0,(select lead_body from marble.lead_format where lead_id = lf.lead_body and site_id = 'auto'),lf.lead_body) as 'lead_body',
	if(lf.lead_body<0,'no','yes') 'custom'
from 
	marble.campaign as ca
	join marble.lead_format as lf
	on ca.lead_id = lf.lead_id and lf.site_id = 'auto'
	join
	marble.companies as co
	on ca.comp_id = co.comp_id
	join
	marble.rules as r
	on r.lead_id = ca.lead_id and r.site_id = 'auto'
where 
	ca.lead_id = '$lead_id'
	and ca.site_id = 'auto';";
	
	$rs = new mysql_recordset($sql);
	if ($rs->fetch_array())
	{		
		extract($rs->myarray);		
		$phone_num = ereg_replace("[^0-9]","",$phone_num);
		
	}
	else
	{
		header("Location: marblereport.php");
		exit();		
	}	
?>	
<link rel='stylesheet' type='text/css' href='marble.css' />

<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<? 
					if ($admin)
						$goto = "marblereport.php";
					else
						$goto = "marbleindividualgrid.php";
				?>
				<a href='<?= $goto ?>' class='white'>
				<font face='verdana' size=2 color='white'><b>&nbsp;.:
				Top Auto Leads</b></font></a>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing=0 cellpadding=10>
					<tr valign="top">
						<td  bgcolor='navy' width="120" nowrap>&nbsp;
							<br/><br/><br/>
							<? 	include "sidelinks.php";	?>
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>
								
							</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Company: 
								<?= $rs->myarray['comp_name'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Phone: 
								<?	
									if (strlen($phone_num) > 10)
									{	
										$ext =  "Ext. ".substr($phone_num,10);
										$phone_num = substr($phone_num,1,10);
									}
									if (strlen($phone_num) == 10)
									{
										$a = substr($phone_num,0,3);
										$b = substr($phone_num,3,3);
										$c = substr($phone_num,6);
										echo "($a) $b - $c $ext ";
									}									
								?>								
								</font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Transportation Method: 
								<?= $rs->myarray['format_type'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Email Address: 
								<?
									
									if ($_SESSION['user'] == "uberadmin")
										$leadlink = "?lead_id=".$_REQUEST['lead_id'];
									else 
										$leadlink =  "";
									echo $rs->myarray['lead_email'];
								 ?> 
								 <!--- ( <a  class='black' href='marbleemail.php<?= $leadlink ?>'>change</a> ) --->
								 </font></b>
							</td>
							</tr>
							<? if ($admin) { ?>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Custom Format: 
								<?= $rs->myarray['custom'] ?></font></b>
							</td>
							</tr>
							
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Active: 
								<? 
									if ($active == 1)
									{
										echo "<font color='green'>Yes</font>";
										echo " &nbsp; <a class='black' href='marble_edit.php?function=deactivate&lead_id=$lead_id'> (deactivate) </a>";
									}
									else
									{
										echo "<font color='red'>No</font>";
										echo " &nbsp;  <a class='black' href='marble_edit.php?function=activate&lead_id=$lead_id'> (re-activate) </a)";
									}
								?></font></b>
							</td>
							</tr>
							<? } ?>
							<tr><td>&nbsp;</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Format: 
								</font></b>
								<br/>
								<font face="Courier New, Courier, mono">
								<? 									
									$lead_body = str_replace(array("<",">","\n"),array("&lt;","&gt;","<br/>"),$lead_body);
									$colored = ereg_replace(
										"\|\|([a-zA-Z0-9_]*)\|\|",
										"<font color='blue'>||\\1||</font>",
										$lead_body);
									echo $colored;
								?>
								</font>
<?
	include "skin_bottom.php";

?>								