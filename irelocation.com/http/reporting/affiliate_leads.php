<?php

	include "../inc_mysql.php";
	include "cron_functions.php";
	
	define (VALID," and lead_ids != '' and lead_ids != '||||||' ");


	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];
	
	if (strlen($_POST["site_code"]) > 0)
	{
		$site_code = $_POST["site_code"];
	}

	
	// cat_id = 1
	//if lead_ids == -1 return numbers for whole site.
	function getNumbersForMonth($source, $lead_ids,  $month="*", $year="*", $table="movingdirectory.quotes")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($year == 2005)
			$table = "movingdirectory.quotes_archive";
		else if ($year == 2004)
			$table = "movingdirectory.quotes_old";
//		else
			//use the table passed in.
		
		if ($lead_ids == -1 || $lead_ids == "-1")
			$lead_ids = "";
		
		echo "<!-- lead_ids = $lead_ids -->";
		
		$query = "select substring(received,7,2) 'day', count(received) 'count' from $table where received like '$year$month%' $lead_ids $source ";
		if (strpos($source,"tm") !== FALSE || strpos($source,"pm") !== FALSE || strpos($source,"me") !== FALSE )
			if (strpos($table,"irelocation.leads_") === FALSE)
				$query .= VALID;		
		$query .= " group by substring(received,1,8) WITH ROLLUP;";		
		return 	$query;
	}
		
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
		
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";
		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
	

	
	///if lead_id != -1, then comp_name is a company name.
	//otherwise, comp_name, is a site name.
	function print_company_data($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
	{
		$source_code = getSourceString($site_code, $campaign);
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($lead_id != -1)
			$lead_string = " and lead_ids like '%".$lead_id."%' ";
		else
			$lead_string = -1;
			
		$query = getNumbersForMonth($source_code,$lead_string,$month,$year);
		echo "<!--- $query --->";
		print_it($site_code,$campaign,$lead_id, $query,$comp_name,$month,$year);
	}
	
	
	/*
		All calls to this method must have month and year populated allready.	
	*/
	function print_it($site_code,$campaign,$lead_id, $sql, $comp_name="Company Name Here",$month, $year, $international=0)
	{		
	
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
				
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		if ($international == 1)
			$international = "&cat_id=3";
		else
			$international = "";
		
		while ($rs->fetch_array())
		{			
			$counter++;
			$day = $rs->myarray["day"];
			$ts = $year.$month.$day;
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}
			
			
				
				
			if ($lead_id == -1)
				echo "<!--- link to leads: show_leads.php?ts=$ts&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year$international --->";
			
			
			
			
			if ($day != $last)
			{
				$last = $day;
				
				$count  = $rs->myarray["count"];
				echo "<td align='right'>";
				echo "<a href='reporting.php?page_id=5&ts=$ts&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year$international'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'> ";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else
				{
					//echo "<td>$total</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
	
	
	 function getSourceString($site_code, $campaign="all")
	 {	 	
		return " and source like '%$site_code%' ";
	 }
	 
	 
	
?>
<div align="center">
	<table>
	<tr><td align="left">
		<table>
		<tr>
			<Td align="left">
				<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
				<input type="hidden" name="site_code" value="hack" />
				<select name="campaign">
					<option value="all" <?php if ($campaign == "all")  { echo "selected"; } ?>>All Leads</option>				
<!---
					<option value="org" <?php if ($campaign == "org")  { echo "selected"; } ?>>Organic Leads</option>
					<option value="google" <?php if ($campaign == "google")  { echo "selected"; } ?>>Google</option>
					<option value="overture" <?php if ($campaign == "overture")  { echo "selected"; } ?>>Overture</option>
					<option value="msn" <?php if ($campaign == "msn")  { echo "selected"; } ?>>MSN</option>				
--->				
				</select>	
				<Select name="month">
				
					<?php
					
						$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
					
						for ($i = 1; $i < 13; $i++)
						{
							echo "<option value='";
							if ($i < 10)
								echo "0$i";
							else
								echo "$i";
							echo "' ";
							if (intval($month) == ($i)) 
								echo "selected ";
							echo ">".$months[$i-1]."</option>\n";
						}
					?>
				</Select>
				<select name="year">
					<option value="2007"  <?php if ($year == "2007" || ($year == '') )  { echo "selected"; } ?>>'07</option>	
					<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
					<option value="2005"  <?php if ($year == "2005")  { echo "selected"; } ?>>'05</option>
					<option value="2004"  <?php if ($year == "2004")  { echo "selected"; } ?>>'04</option>
					<option value="2003"  <?php if ($year == "2003")  { echo "selected"; } ?>>'03</option>
				</select>
				<input type="submit" value="Submit" />
				</form>
			</td>
		</tr>
		</table>

	</td></tr>
	<tr><td>

<?		
	if (strlen($site_code) > 0)
	{				
		print_month_header($month,$year,2);
		print_company_data("pm_boxdev","all", -1, "Boxes Delivered", $month, $year);
		print_company_data("pm_silver","all", -1, "Silver Carrot", $month, $year);
		echo "</table>";			
	}
?>
</td></tr></table>
</div>