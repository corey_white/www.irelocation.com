<?php
	include 'act_security.php';
	verify();

	include '../inc_mysql.php';
	include 'lib_functions.php'>
?>

<html>
	<body>		
			<form name="process" action="display_leads.php" method="post">
				Filter Lead Type: <select name="type_filter">
					<option value="*">All</option>
					<?php foreach ($types as $type)
					{	
						echo "<option>$type</option>\n";
					}
					?>									
				</select>			
				<br/>
				Filter Lead Source: <select name="source_filter">
					<option value="*">All</option>
					<?php foreach ($sources as $source)
					{	
						echo "<option value='".$source[1]."' >".$source[0]."</option>\n";
					}
					?>									
				</select>			
				<br/>
				Time Range <select name="time_filter">
					<option value="0">Today</option>
					<option value="-1">Month</option>					
				</select>			
				<Br/>
				<input type="submit" value="Show Leads"/>
			</form>	
	</body>
</html>

