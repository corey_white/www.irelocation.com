<?
	define(PIPE,"||");
	define(PIPE_LENGTH,2);
	define(maybe,(rand(0,100) > 50));
	include "../inc_mysql.php";
	
	class CarQuoteData
	{
		var $myarray;
		
		function CarQuoteData($thearray)
		{
			$this->myarray = $thearray;
			
			$this->myarray["fullname"] = $this->myarray["name"];
			list($this->myarray["firstname"],$this->myarray["lastname"]) = split(" ",$this->myarray["name"],2);
			
			$r_year=substr($this->myarray["received"],0,4);
			$this->myarray["ryear"] = $r_year;
			
			$r_month=substr($this->myarray["received"],4,2);			
			$this->myarray["rmonth"] = $r_month;
			
			$r_day=substr($this->myarray["received"],6,2);
			$this->myarray["rday"] = $r_day;
			
			$r_hour=substr($this->myarray["received"],8,2);
			$r_min=substr($this->myarray["received"],10,2);
			$this->myarray["received_standard"] = "$r_month/$r_day/$r_year $r_hour:$r_min";
			
			$temp_year=substr($this->myarray["est_move_date"],0,4);
			$temp_month=substr($this->myarray["est_move_date"],5,2);
			$temp_day=substr($this->myarray["est_move_date"],8,2);
		
			list($this->myarray["emonth"],$this->myarray["eday"],$this->myarray["eyear"]) = 
			explode("-",date("m-d-Y",mktime(0,0,0,$temp_month,$temp_day,$temp_year)));
			
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);
			$this->myarray["phone_dashes"] = "$p_area-$p_prefix-$p_suffix";
			
			$this->myarray["running_yes_no"] = (!(substr_count($comments ,"non-running") > 0))?"Yes":"No";
			$this->myarray["running"] = (!(substr_count($comments ,"non-running") > 0))?"running":"not-running";
			
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Vehicle Type: ','',$mycomments);
			$mycomments=str_replace('Vehicle Year: ','',$mycomments);
			$mycomments=str_replace('Vehicle Make: ','',$mycomments);
			$mycomments=str_replace('Vehicle Model: ','',$mycomments);
			$mycomments=str_replace('Comments: ','',$mycomments);
			$details=explode("\n",$mycomments);
			
			$this->myarray["type"] = $details[0];
			$this->myarray["year"] = $details[1];
			$this->myarray["make"] = $details[2];
			$this->myarray["model"] = $details[3];
			$this->myarray["customer_comments"] = $details[4];
			
			$this->myarray["destination_zip"] = $this->getDestinationZip($this->myarray["destination_city"], $this->myarray["destination_state"]);
			
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Vehicle Type: ','',$mycomments);
			$mycomments=str_replace('Vehicle Year: ','',$mycomments);
			$mycomments=str_replace('Vehicle Make: ','',$mycomments);
			$mycomments=str_replace('Vehicle Model: ','',$mycomments);
			$mycomments=str_replace('Comments: ','',$mycomments);
			$details=explode("\n",$mycomments);
			$this->myarray["html_auto_comments"]="<tr><td width='200'>Vehicle Type: </td><td>$details[0]</td></tr>
			<tr><td width='200'>Vehicle Year: </td><td>$details[1]</td></tr>
			<tr><td width='200'>Vehicle Make: </td><td>$details[2]</td></tr>
			<tr><td width='200'>Vehicle Model: </td><td>$details[3]</td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			<tr><td width='200'>Comments: </td><td>$details[5]</td></tr>
			";
			
		}
		
		function get($key,$url=0)
		{
			if (substr_count($key,"url_") > 0)
			{
				$key = substr($key,4);
				$url = 1;
			}
			if (substr_count($key,"xml_") > 0)
			{
				$key = substr($key,4);
				$url = 2;
			}
			
			if (strlen($this->myarray[$key]) > 0)
			{
				$v =  $this->myarray[$key];
				if ($url == 1)
					return urlencode($v);
				else if ($url == 2)
					return $this->xmlentities($v);
				else return $v;
			}
			else			
				return "keyword '$key' not found<br/>";
		}
		
		function xmlentities ( $string )
		{
		   return str_replace ( 
		   	array ( '&', '"', "'", '<', '>' ), 
			array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
			$string 
			);
		}

		
		function getDestinationZip($city,$state)
		{
			$destination_zip="";
			if((trim($city)!='') && ($state!=''))
			{
				$nomore=0;
				$sql="select zip from zip_codes where lower(city) like '%".strtolower(trim($city))."%' and state='".$state."'";
				$rs2=new mysql_recordset($sql);
				if($rs2->rowcount()>0)
				{
					$rs2->fetch_array();
					$destination_zip=$rs2->myarray["zip"];
				}
				else
				{
					$city_length=strlen(trim($city));
					$mycount=10;
	
					if($city_length<10)
						{$mycount=$city_length;}
	
					for($i=$mycount;$i>=3;$i--)
					{
						if($nomore==0)
						{
							for($c=0;$c<$city_length-$i+1;$c++)
							{
								if($nomore==0)
								{
									$city_like=substr(strtolower(trim($city)),$c,$i);
									$sql="select zip from zip_codes where lower(city) like '%$city_like%' and state='".$state."'";
									$rs2=new mysql_recordset($sql);
									if($rs2->rowcount()>0)
									{
											$rs2->fetch_array();
											$destination_zip=$rs2->myarray["zip"];
											$nomore=1;
									}
								}
							}
						}
					}
				}
				if($destination_zip=="")
				{
					if(intval($city)>0)
					{
						$destination_zip=intval($city);
						$sql="select city,state from zip_codes where zip='$destination_zip'";
						$rs2=new mysql_recordset($sql);
						$rs2->fetch_array();
					}
				}
				return $destination_zip;
			}
		}
		function findAllOccurences($Haystack, $needle, $limit=0)
		{
		  $Positions = array();
		  $currentOffset = 0;
		  $count=0;
		  while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
		  {
		   $Positions[] = $pos;
		   $offset = $pos + strlen($needle);
		   $count++;
		  }
		  return $Positions;
		}
		
		function merge($type)
		{
			//echo "Merging Data<br/>";
			$success = 1;
			$offset = 0;
			$variables = array();
			$results = $this->findAllOccurences($this->lead_body,PIPE);
			for ($i = 0; $i < count($results); $i +=  2)
			{				
				$variable = substr($this->lead_body,$results[$i],$results[$i+1]-$results[$i]+PIPE_LENGTH);				
				$variable = str_replace(PIPE,"",$variable);
				$variables[] = $variable;				
			}
			$variables = array_unique($variables);
			foreach ($variables as $v)
			{
				if ($type == "xml")
					$find_variable = "xml_$v";
				else if ($type == "post")
					$find_variable = "url_$v";
				else
					$find_variable = $v;
				$value = $this->get($find_variable);
				if (substr_count($value,"keyword") > 0)
					$success = 0;
									
				$this->lead_body = str_replace("||$v||",$value,$this->lead_body);
			}
			return $success;
		}
		
		function process($lead_array)
		{
			$this->lead_body = $lead_array["lead_body"];
			$success = $this->merge($lead_array["format_type"]);
			if ($success == 1)
			{
				//lead_id,lead_body,format_type,email
				if ($lead_array["format_type"] == "email")
				{
					
					list ($subject,$body) = split("\n",$this->lead_body,2);
					list($headers,$body) = split("---------------",$body,2);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = implode("\n",$body_parts);
					
					mail($lead_array["email"].",david@irelocation.com",$subject,trim($body),trim($headers));
					//mail("david@irelocation.com",$subject,trim($body),trim($headers));
				}
				else
				{
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = substr($url,4);
					$variables = split("\n",$variables);
					
					////$ch=curl_init("http://www.irelocation.com/reporting/receive.php");
					$ch=curl_init(trim($url));
					echo "curl_init(".trim($url).")";
					echo "<br/>";
					foreach($variables as $v)
					{
						if (strlen(trim($v)) > 0)
						{
							list($k,$va) = split(":",$v,2);							
							
							if ($k == "validation")
								$validation = $va;
							else
							{
								echo "$k = $va <br/>";
								curl_setopt($ch, $k, $va);
							}
						}
					}
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
					
					$results = curl_exec($ch);
					curl_close($ch);
					
					echo "URL: <input type='text' name='url' value='$url' /><br/>";
					echo "Valid Response Test: <input type='text' name='validation' value='$validation' /><br/>";
					echo "<textarea cols='50' rows='30'>$body</textarea><br/>";
					echo "Response: <input type='text' name='results' value='$results' /><br/>";
					//mail("david@irelocation.com","XML Auto Lead",$this->lead_body);
				}
			}
			else
			{
				mail("david@irelocation.com","Failed to Format Auto Lead",print_r($this->myarray,true)."\n\n".$this->lead_body);
			}
		}
	}
	
	function findAllOccurences($Haystack, $needle, $limit=0)
	{
	  $Positions = array();
	  $currentOffset = 0;
	  $count=0;
	  while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
	  {
	   $Positions[] = $pos;
	   $offset = $pos + strlen($needle);
	   $count++;
	  }
	  return $Positions;
	}
	
	exit();
	$sql = "select * From movingdirectory.quotes where email = 'david@irelocation.com' and cat_id = 1 limit 1;";
	//$sql = "select * from movingdirectory.quotes where quote_id > 1644840 and lead_ids like '%1518%';";
	$rs = new mysql_recordset($sql);
	while ($rs->fetch_array())
	{
		//echo $rs->myarray["quote_id"];
		$data = new CarQuoteData($rs->myarray);
		$sql = "select 
	(select email from movingdirectory.rules where lead_id = lf.lead_id) as 'email',
	lead_id,
	format_type,
	if(
		lead_body < 0,
		(select lead_body from movingdirectory.lead_format where lead_id = lf.lead_body),
		lf.lead_body
	  ) as 'lead_body'
	from 
	movingdirectory.lead_format as lf
where lead_id = 1382 and format_type = 'email'
group by lead_body;";

		$rs2 = new mysql_recordset($sql);
		$all_variables = array();
		
		while ($rs2->fetch_array())
		{							
			$lead_body = $rs2->myarray["lead_body"];
			$format_type = $rs2->myarray["format_type"];
			$data->process($rs2->myarray);			
		}	
	}
	
?>