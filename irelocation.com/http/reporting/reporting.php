<?php	
	include 'act_security.php';
	verify();	
	
	$page_id = $_REQUEST["page_id"];
	
	$page_count = 11;
	
	if ($page_id == '' || $page_id > $page_count || $page_id < 0)
		$page_id = 2;
		
	if (!isset($page_id))
		$page_id = 2;
		
	$pages = array("all_leads","carshipping","whose_involved",
					"show_leads","checking", "security","selfmove","view_leads",
					"local","realestate");
					
	$titles = array("Leads 2.0","CarShipping!!", "Recipiants",
					"Lead Details","Daily Look at Me Page", "Security Leads","Self Moving Quotes","Security Leads",
					"Local Leads","Real Estate Leads");
					
	$header = array(0,1,4,5,8,9);
	include "header.php";
	if ($_COOKIE["irel-homepage"] != $page_id)
	{
		echo "<div class='mainmovingtext' align='right' ><a href='sethomepage.php?page=$page_id'>Make this My Reporting Home Page</a></div><br/>";
	}
	else
		echo "<div class='mainmovingtext' align='right' ><a href='sethomepage.php?page=-1'>Clear Home Page</a></div><br/>";
	include $pages[$page_id].".php";	
	
	include "footer.php";

?>