<?php
	include '../inc_mysql.php';
	include 'lib_functions.php';
	include 'cron_functions.php';
	$distinct = 1;
	$curr_day = date('j');
	$today = date('d');
	$ndays = cal_days_in_month(CAL_GREGORIAN, date('n'), date('y'));	
	$daysleft = $ndays - date('d');
?>

<div align="center">
<table width="90%">
	<tr>
	<td class="reportingtext" style=" size:+1; ">
	<br/>
	<?php
	echo "Today is the <strong>".date('jS')."</strong> of <strong>".date('F')."</strong><br/>";
	echo "There are <strong>$ndays</strong> days total,<br/>";
	echo "and <strong>$daysleft</strong> days left in the month.<br/><br/>";

	?>
	<br/>
	
	<table border="1">
		<tr>
			<th>Site</th>
			<th>Monthly Goal</th>
			<th>Leads this Month</th>
			<th>Leads Today</th>
			<th title="The average number of leads received per day, this month." >Lookback Avg</th>
			<th title="Static amount of leads needed per day for this month.">Forcast Lead Avg</th>
			<th title="Number of leads needed per day to meet the goal.">Adjusted Avg</th>
		</tr>	
	
<?php	
	
	$needle = "%' and (source like";

	foreach($siteInfo as $site)
	{

		$month_query = getTrendsQuery($site,$distinct);
		$rs_month = new mysql_recordset($month_query);		
		$rs_month->fetch_array();
		
		$this_month = $rs_month->myarray["count"];
			
		$look_back_font = ">";
		//$sql_today = str_replace($needle,$today.$needle, $month_query);//add the day to the month query.
		$sql_today = getTrendsQuery($site,$distinct,$today);
		$rs_day = new mysql_recordset($sql_today);
		$rs_day->fetch_array();		
		$count_today = $rs_day->myarray["count"];

		if ($curr_day != 1)
			$look_back_avg = floor(($this_month - $count_today)/ ($curr_day-1));
		else
			$look_back_avg = $this_month;


		$monthly_goal = getMonthlyGoal($site);
		
		$percentage =  floor(($this_month / $monthly_goal) * 100)."%";
		
		
		$remaining_leads = $monthly_goal - $this_month;
		
		$forcast_avg = ceil(intval($monthly_goal) / intval($ndays));
		if ($look_back_avg > $forcast_avg)
			$look_back_font = " title='Above the Forcast Avg.' style='color:green' >";
		else
			$look_back_font = " title='Below the Forcast Avg.' style='color:red' >";
		if ($remaining_leads > 0 && $daysleft != 0)
			$per_day = ceil($remaining_leads / $daysleft);
		else
			$per_day = 0;
		
		echo "<tr>\n\t<td title=\"$sql_today\" >\n";	
		echo "\t\t".$site[1]."\n\t</td>\n";		
		echo "\t<td>$monthly_goal</td>\n";
		echo "\t<td>$this_month ( $percentage )</td>\n";
		echo "\t<td>$count_today </td>\n";
		echo "\t<td $look_back_font $look_back_avg</td>\n";
		echo "\t<td>$forcast_avg</td>\n";
		echo "\t<td>$per_day</td>\n";
		echo "</tr>\n";		
	}	
?>
</table>
<br/>
</td></tr>
</table>
</div>