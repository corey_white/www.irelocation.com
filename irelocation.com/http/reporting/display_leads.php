<?php
	include 'act_security.php';
	verify();
?>
<table>
	<tr>	
		<th>Type</th>
		<th>Source</th>
		<th>Company</th>
		<th>Count</th>
	</tr>
<?php
	
	include '../inc_mysql.php';
	include 'lib_functions.php';
	
	$source = $_POST["source_filter"];
	if(!isset($source))
		$source = "*";
	$type = $_POST["type_filter"];
	if(!isset($type))
		$type = "*";
		
	$time = $_POST["time_filter"];
	if(!isset($time))
		$time = "0";

	foreach($codes as $set)
	{
		if (($source == '*' || $source == $set[1]) && ($type == '*' || $type == $set[0]))
		{
			$company = "N/A";
			
			if ($set[2] != -1)
			{
				$comp = new mysql_recordset(getCompanyByLead($set[2]));	
				$comp->fetch_array();
				$company = $comp->myarray["comp_name"];
			}	
			
			
			$query = getQuery($set[0],$set[1],$set[2],0,0,$time);
			
			$rs = new mysql_recordset($query);	
			$rs->fetch_array();
			$count = $rs->myarray["count(*)"];
			
			echo "<tr>\n<td>".$set[0]."</td>\n";
			echo "<td>".displayTitle($set[1])."</td>\n";
			
			if ($company != "N/A")
				echo "<td> $company </td>\n";
			else
				echo "<td> N/A </td>\n";
			echo "<td> $count </td>\n";
			//echo "<td title=\"$query\" > Query  </td>\n";
			echo "</tr>";
		}
	}

?>
</table>