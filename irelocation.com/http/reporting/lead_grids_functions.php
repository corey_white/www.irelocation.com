<?php
function filter($source, $type, $set)
	{
		if (strlen($_POST["source"]) == 0 || $_POST["source"] == "default")
			return true;	
		if ($type == DAS && $set[0] == DAS)
		{
			return true;
		}
		else if ($type == REAL_ESTATE && $set[0] == REAL_ESTATE)
		{
			return true;
		}
		else if ($set[0] == DAS)
		{
			return false;
		}
		else if (($source == '*' || $source == $set[1]) && ($type == '*' || $type == $set[0]))
		{
			return true;
		}		
		else 
		{
			return false;
		}
	}
	
	function displayCompanyGrid($comp_array, $year, $month, $source, $type, $campaign="*")
	{
		echo "<!--- displayCompanyGrid.Campaign: $campaign --->\n";
		$curr_day = date('j');
		if ($month != '')
			$ndays = cal_days_in_month(CAL_GREGORIAN, $month, $year);	
		else
			$ndays = date('t');
						
		echo "<table border='1' class='reportingtext' >\n\t<tr>\n\t\t<th>Type of Lead</th>\n\t\t<th>Company</th>";
		for ($j = 0; $j < $ndays; $j++)
		{
			echo "\t\t<th>".forceTwo($j+1)."</th>";	
		}
		echo "\t\t<th>Total</th>";
		echo "\n\t</tr>\n";
		
		$counter = 0;
		
		echo "\n\n<!-- $month -->\n\n";
		foreach($comp_array as $set)
		{			
			if (filter($source,$type,$set))
			{
				$company = $set[3];
				
				if ($set[3] == "Unknown")
				{					
					$comp = new mysql_recordset(getCompanyByLead($set[2]));	
					$comp->fetch_array();
					$company = $comp->myarray["comp_name"];
				}	

				echo "\n\t<tr>\n\t\t<td>".$set[0]."</td>";
				echo "\n\t\t<td>$company</td>";
				for ($j = 0; $j < $ndays; $j++)
				{											
					echo "\n\t\t<td>";
					if ((date('n') == $month &&  $j+1 <= $curr_day) || (date('n') != $month))//dont do queries on days that dont exist yet
					{
						$query = getQuery($set[0],$set[1],$set[2],$year,$month,($j + 1), $campaign);
						echo "\n\t\t<!--- getQuery(".$set[0].",".$set[1].",".$set[2].",".$year.",".$month.",".($j + 1).", $campaign) --->";
						echo "\n\t\t<!--- $query --->\n";
						echo "\n\t\t<!--- running query... --->\n";
						$rs = new mysql_recordset($query);	
						$rs->fetch_array();
						echo "\n\t\t<!--- ...done. --->\n";
						$count = $rs->myarray["count"];
						if ($count > 0)
							echo "<a target='_blank' href='display_lead_group.php?index=$counter&ts=".formatDate($year,$month,($j + 1))."&distinct=$distinct' > ";
						echo $count;			
						if ($count > 0)		
							echo " </a>";
					}
					else
						echo "&nbsp;";
					echo "</td>";
				}
				
				
				$query = getQuery($set[0],$set[1],$set[2],$year,$month,-1,$campaign);//whole month
				$rs = new mysql_recordset($query);	
				$rs->fetch_array();
				$count = $rs->myarray["count"];
				echo "\n\t\t<td> $count </td>";
				echo "\n\t\t</tr>";
			}
			$counter++;
		}
		echo "</table>";
	
	
	}

?>