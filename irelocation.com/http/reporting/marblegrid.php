<style>
	.on
	{
		background-color:#E8E8FF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.off
	{
		background-color:#FFFFFF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.header 
	{
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-weight:bold;
		background-color:navy;
		font-size: 12px;
	}
	
	.white
	{
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-weight:bold;
		color:#FFFFFF;
		background-color:navy;
		font-size: 12px;
	}
	a 
	{
		text-decoration:none;		
		color:#FFFFFF;
	}
	a.black
	{
		text-decoration:none;		
		color: #000000;
		font-size:10px;
		
	}
</style>
<?
	include "../marble/inc_mysql.php";

	function getSortLink($current,$field_name,$label)
	{
		if ($current != $field_name)
			return "<a href='marblegrid.php?order=$field_name' title='Click to Sort Accending'>".ucwords($label)."</a>";
		else
			return "<a href='marblegrid.php' title='Click to Clear Sorting'>".ucwords($label)."</a>";
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}

	$sql = "Select co.comp_name,ca.* from campaign as ca join companies as co on co.comp_id = ca.comp_id where ca.site_id = 'auto' and ca.active;";
	$rs = new marble($sql );
	while ($rs->fetch_array())
	{
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}

	$this_month = date("Ym");
	$today = date("d");
	$days_in_month = getNDays();
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month%';";
	$rs = new marble($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month$today%';";
	$rs = new marble($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	$default_order_by = "comp_name,day";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	
	$ts = date("Ym");
	
	$sql = "select 
				c.lead_id,				
				co.comp_name,	
				substring(received,7,2) 'day',
				c.monthly_goal 'goal',
				count(q.quote_id) 'count'				
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'auto'
				AND q.received like '$ts%'					
				and c.active
			group by
				c.lead_id,
				substring(received,7,2)
			order by
				$default_order_by;";

	echo "<!--- $sql --->";
	$rs = new marble($sql);
	
	
	$total_sql = "select 
					substring(received,7,2) 'day',
					count(q.quote_id) 'count'				
				from
					marble.auto_quotes as q
				where 
					q.received like '$ts%'					
				group by
					substring(received,7,2)";
	
	$total_rs = new marble($total_sql);
?>	
<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<font face='verdana' size=2 color='white'><b>
				<a href='marblereport.php' class='white'>&nbsp;.:
				Top Auto Leads</a></b></font>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing=0 cellpadding=10>
					<tr>
						<td  bgcolor='navy' width="120" valign="top">
							<br/><br/><br/>
							
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>&nbsp;</td></tr>
							<tr><td>
								<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
							</td></tr>
							<tr><td>
								<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
							</td></tr>
							<Tr><td>

	
	
<?	
	echo "<table border='1'>";
	echo "<tr class='header' ><td> &nbsp; </td>".
			"<th align='left' class='white' >Company</th>".
			"<th class='white'>".getSortLink($default_order_by,"goal,comp_name,day","Goal")."</th>";
	
	$days = getNDays();
	for($i = 1; $i <= $days; $i++)
	{
		echo "<th class='white'>$i</th>";
	}
			
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	echo "<tr class='off'><td class='$style' > - </td>";
	echo "<th align='left'>Site Total</th>";	
	echo "<th class='off'> - - </th>";
	while($total_rs->fetch_array())
	{
		extract($total_rs->myarray);		
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th class='off'>$count</th>";
		$total += $count;		
		$day_count++;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
	}
	echo "<th class='off' align='right' >$total</th></tr>";		
	
	$day_count = 0;
	$font_counter++;
	
	while($rs->fetch_array())
	{
		extract($rs->myarray);
		
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
		
		if ($last_company != $comp_name && $last_company != "")//new company
		{
			if ($day_count < $days)//not a full month of data yet.
			{
				echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
			}
			echo "<td  align='right'>$total</td></tr>";
			$last_company = "";
		}
		
		if ($last_company == "")
		{
		
			echo "<tr class='$style'><td class='$style' >$font_counter</td>";
			$link = "<a href='marbleformat.php?lead_id=$lead_id' class='black'>$comp_name</a>";
			echo "<th align='left'>$link</th>";	
			echo "<th class='$style'>$goal</th>";
			$last_company = $comp_name;		
			$total = "";	
			$day_count = 0;
			$font_counter ++;
		}

		$day_count++;
		
		if ($day > $day_count)
		{
			echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
			$day_count = intval($day);
		}

		if (intval($day) == $day_count)//
			echo "<th>$count</th>";
		$total += $count;
	}
	
	if ($day_count < $days)//not a full month of data yet.
	{
		echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
	}
	echo "<td  align='right'>$total</td></tr>";
	$last_company = "";
	echo "</table>";

?>
</td></tr></table></td></tr></table></td></tr></table>