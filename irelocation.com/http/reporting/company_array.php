<?php
	
	define (ATUS,"0");
	define (CS,"1");
	define (MMC,"2");
	define (AUTOSHIP,"3");

	define (INTERSTATE, "Interstate");
	define (INTERNATIONAL, "International");
	define (AS_IRELO, "Auto Shipping iRelo");
	define (AUTO_SHIPPING, "Auto Shipping");
	define (REAL_ESTATE ,"Real Estate");
	define (DAS ,"Dependable AutoShipping");
	
	/*
		Type, 
		Source, 
		Lead_id 
			(-1 for whole site, or sirva...), 
		Company 
			(if known), 
			if not, 
			"Unknown" will tell the script to look it up.
	*/
	
	$whole_sites = array(
		array(INTERSTATE, 'tm', 0, "TopMoving.com"), 
		array(INTERSTATE, 'pm', 0, "ProMoving.com"),
		array(INTERSTATE, 'me', 0, "MovingEase.com"), 
		array(AUTO_SHIPPING, "cs", -1, "CarShipping.com"),
		array(AUTO_SHIPPING, 'mmc', -1, "MoveMyCar.com"),
		array(AUTO_SHIPPING, 'atus', -1, "AutoTransports.us")
	);
	
	$codes = array(
				array(INTERSTATE, 'tm', 0, "TopMoving.com"), 
				
				array(INTERSTATE, 'tm', 1386, "Wheaton"),
				array(INTERSTATE, 'tm', -1, "Sirva"), 
				array(INTERNATIONAL, 'tm', -1, "Sirva"),
				array(INTERNATIONAL, 'tm', 1502, "Price Break Shipping"),
				
				array(INTERSTATE, 'pm', 0, "ProMoving.com"), 
				array(INTERSTATE, 'pm', 1388, "Bekins"),				
				array(INTERSTATE, 'pm', 1527, "American World Vanlines"),
				array(INTERSTATE, 'pm', 1517, "Albert Moving"),								
				array(INTERSTATE, 'pm', -1, "Sirva"),
				array(INTERNATIONAL, 'pm', -1, "Sirva"),
				array(INTERNATIONAL, 'pm', 1502, "Price Break Shipping"),
				
				array(INTERSTATE, 'me', 0, "MovingEase.com"), 
				array(INTERSTATE, 'me', 1388, "Bekins"),			
				array(INTERSTATE, 'me', 1427, "Paul Arpin"),								
				
				array(INTERSTATE, 'me', -1, "Sirva" ),
				array(INTERNATIONAL, 'me', -1, "Sirva"),
				array(INTERNATIONAL, 'me', 1502, "Price Break Shipping"),
				
				
				array(DAS, 'pm', -1, "ProMoving.com"),
				array(DAS, 'me', -1, "MovingEase.com"),
				array(DAS, 'tm', -1, "TopMoving.com"),
				
				//cs && csq query.. breaking the mold..

				array(AUTO_SHIPPING, "cs", -1, "CarShipping.com"),
				array(AUTO_SHIPPING, "cs",1433,"Unknown"),
				array(AUTO_SHIPPING, "cs",1355,"Unknown"),				
				array(AUTO_SHIPPING, "cs",1442,"Unknown"),
				array(AUTO_SHIPPING, "cs",1394,"Unknown"),				
				array(AUTO_SHIPPING, "cs",1409,"Unknown"),
				array(AUTO_SHIPPING, "cs",1382,"Unknown"),
								
				
				array(AUTO_SHIPPING, "cs",1377,"Unknown"),
				array(AUTO_SHIPPING, "cs",1478,"Unknown"),
				array(AUTO_SHIPPING, "cs",1503,"Unknown"),
				array(AUTO_SHIPPING, "cs",1374,"Unknown"),
				
				array(AUTO_SHIPPING, 'cs',1250, "Unknown"),
				array(AUTO_SHIPPING, "cs",1457,"Unknown"),
				array(AUTO_SHIPPING, "cs",1470,"Unknown"),
				
				//array(AUTO_SHIPPING, 'cs',1353, "Unknown"),
				//array(AUTO_SHIPPING, "cs",1498,"Unknown"),
				//array(AUTO_SHIPPING, "cs",1406,"Unknown"),
				
				
				array(AUTO_SHIPPING, 'mmc', -1, "MoveMyCar.com"),
				array(AUTO_SHIPPING, 'mmc', 1406, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1382, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1433, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1442, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1478, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1503, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1500, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1493, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1501, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1505, "Unknown"),
				array(AUTO_SHIPPING, 'mmc', 1506, "Unknown"),					
				array(AUTO_SHIPPING, 'mmc', 1507, "Unknown"),


				array(AUTO_SHIPPING, 'atus', -1, "AutoTransports.us"),	
				array(AUTO_SHIPPING, 'atus',1433, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1442, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1470, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1478, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1483, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1503, "Unknown"),
				array(AUTO_SHIPPING, 'atus',1504, "Unknown"),
				
				
				array(REAL_ESTATE,"35",-1, "Moving Companion"),
				array(REAL_ESTATE,"34",-1, "Paul Arpin")
				
		);
		$sites = array("Auto Transport.us","Car Shipping.com","Move My Car.com","Autoshipping.com");

		
		$companies = array( 
			array(ATUS,"",0),
			array(ATUS,"AAA AutoMovers","$599.00",0),
			array(ATUS,"Across USA","$599.00",0),
			array(ATUS,"North American Auto Transport","$599.00",0),
			array(ATUS,"Primary Auto Transport","$599.00",0),
			array(ATUS,"Tom Love Auto Transport","$599.00",0),
			array(ATUS,"CPM Motors","$599.00",0),
			array(ATUS,"Express Auto","$599.00",0),
			
			array(CS,"",0),
			array(CS,"AAA, Inc","$850.00",0),
			array(CS,"AAA Auto Transport","$1,000.00",0),
			array(CS,"AAA Auto Movers","$850.00",0),
			array(CS,"Across USA","$1,000.00",0),
			array(CS,"American Auto Relocation","$1,000.00",0),
			array(CS,"Auto Trail","$800.00",0),
			array(CS,"BT Express","$1,000.00",0),
			array(CS,"Level One","$1,000.00",0),
			array(CS,"North American Auto Transport","$1,000.00",0),			
			array(CS,"Primary Auto","$800.00",0),
			array(CS,"TransGlobal","$1,000.00",0),
			array(CS,"Specialty Mobile","$1,000.00",0),
			
			array(MMC,"",0),
			array(MMC,"AAA AutoMovers","$850.00",0),
			array(MMC, "Abby","$1,000.00",0),
			array(MMC, "Across USA","$1,000.00",0),
			array(MMC, "BT Express","$1,000.00",0),
			array(MMC, "Executive Auto Transport","$1,000.00",0),
			array(MMC, "North American Auto Transport","$1,000.00",0),
			array(MMC, "VNC Distributions","$1,000.00",0),
			array(MMC, "Primary Auto","$1,000.00",0),
			array(MMC, "Blue Star Auto","$1,000.00",0),
			array(MMC, "True Blue Auto","$1,000.00",0),
			array(MMC, "Express Auto Transport","$1,000.00",0),
			
			array(AUTOSHIP, "",0),
			array(AUTOSHIP, "AAA AutoMovers","$4,250.00",5000),
			array(AUTOSHIP, "Abby","$950.00",1000),
			array(AUTOSHIP, "Dash Auto","$1,800.00",2000),
			array(AUTOSHIP, "Primary Auto","$1,850.00",2000),
			array(AUTOSHIP, "Specialty Mobile","$550.00",500),
			array(AUTOSHIP, "AutoGo Transport","$950.00",1000),
			array(AUTOSHIP, "Across USA","$950.00",1000),
			array(AUTOSHIP, "Adonis Auto","$550.00",500),
			array(AUTOSHIP, "Executive Auto","$950.00",1000),
			array(AUTOSHIP, "AAAll States","$950.00",1000),
			array(AUTOSHIP, "North American","$900.00",1000),
			array(AUTOSHIP, "Phoenix Auto","$550.00",500),
			array(AUTOSHIP, "Blue Star","$850.00",1000),
			//Remove As of July 1st
			array(AUTOSHIP, "Bullseye","$550.00",500),
			array(AUTOSHIP, "Precision Auto","$950.00",1000),
			array(AUTOSHIP, "Easy Auto Transport","$550.00",500),
			array(AUTOSHIP, "Auto Transport Connection","$950.00",1000),
			array(AUTOSHIP, "Fisher Shipping","$900.00",1000),
			array(AUTOSHIP, "Main Line Auto","$950.00",1000),	
			array(AUTOSHIP, "Smart Auto","$900.00",1000),			
			array(AUTOSHIP, "Creative Rides","$1,750.00",2000)
		);
		
?>