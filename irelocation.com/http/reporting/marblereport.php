<style>
	.on
	{
		background-color:#E8E8FF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.off
	{
		background-color:#FFFFFF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.header 
	{
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-weight:bold;
		background-color:navy;
		font-size: 12px;
	}
	.white
	{
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-weight:bold;
		color:#FFFFFF;
		background-color:navy;
		font-size: 12px;
	}
	a 
	{
		text-decoration:none;		
		color:#FFFFFF;
	}
	a.black
	{
		text-decoration:none;		
		color: #000000;
		font-size:10px;
		
	}
</style>
<?
	define(DEVIATION,5);
	include "../marble/inc_mysql.php";

	function getSortLink($current,$field_name,$label)
	{
		if ($current != $field_name)
			return "<a href='marblereport.php?order=$field_name' title='Click to Sort Accending'>".ucwords($label)."</a>";
		else
			return "<a href='marblereport.php?order=$field_name+desc' title='Click to Sort Descending'>".ucwords($label)."</a>";
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}

	$sql = "Select co.comp_name,ca.* from campaign as ca join companies as co on co.comp_id = ca.comp_id where ca.site_id = 'auto' and ca.active;";
	$rs = new marble($sql );
	while ($rs->fetch_array())
	{
		$companies[$rs->myarray["lead_id"]] = $rs->myarray;
	}

	$this_month = date("Ym");
	$today = date("d");
	$days_in_month = getNDays();
	$days_left = $days_in_month - $today + 1;
	if ($days_left == 0)
		$days_left = 1;
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month%';";
	$rs = new marble($total_count);
	$rs->fetch_array();
	$total_leads = $rs->myarray["count"];
	
	$total_count = "select count(*) count from marble.auto_quotes where processed and received like '$this_month$today%';";
	$rs = new marble($total_count);
	$rs->fetch_array();
	$total_leads_today = $rs->myarray["count"];
	
	$default_order_by = "monthly_goal desc, comp_name asc";
	if (strlen($_REQUEST['order']) > 0)
	{
		$default_order_by = $_REQUEST['order'];
	}
	
	$sql = "select 
				co.comp_name,
				c.lead_id,
				c.monthly_goal 'Lead_Count',
				count(q.quote_id) 'Actual_Sent',
				ceiling((c.monthly_goal - count(q.quote_id))/ $days_in_month ) * $today 'Est_Sent',	
				ceiling((c.monthly_goal - count(q.quote_id))/ $days_in_month ) 'needed_per_day',	
				count(q.quote_id) - (ceiling((c.monthly_goal - count(q.quote_id))/ $days_in_month ) * $today) 'deviation',
				c.monthly_goal - count(q.quote_id) 'leads_needed'
			from
				marble.campaign as c
				left join 
				marble.auto_quotes as q
				on locate(c.lead_id,q.lead_ids) > 0
				join companies as co
				on	
				c.comp_id = co.comp_id
			where 
				c.site_id = 'auto'
				AND q.received like '$this_month%'					
				and c.active
			group by
				c.lead_id
			order by
				$default_order_by;";

	echo "<!--- $sql --->";
	$rs = new marble($sql);
	
	
?>	
<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<font face='verdana' size=2 color='white'><b>&nbsp;.:
				Top Auto Leads</b></font>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing=0 cellpadding=10>
					<tr>
						<td  bgcolor='navy' width="120" valign="top">
							<br/><br/><br/>
								<a class='white' href="marblegrid.php"> - Daily Numbers</a>
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>&nbsp;</td></tr>
							<tr><td>
								<b><font face="Verdana" size="2">Total Leads: <?= $total_leads ?></font></b>
							</td></tr>
							<tr><td>
								<b><font face="Verdana" size="2">Total Leads today: <?= $total_leads_today ?></font></b>
							</td></tr>
							<Tr><td>

	
	
<?	
	function colorCode($current,$expected)
	{
		$low = ((100-DEVIATION)/100) * $expected;
		$high = ((100+DEVIATION)/100) * $expected;
		if ($current < $low || $current > $high)
			return "<font color='#FF0000'>".($current-$expected)."</font>";
		else
			return "<font color='#006600'>".($current-$expected)."</font>";
	}

	echo "<table border='1'>";
	echo "<tr class='header' ><td>&nbsp;</td>".
			"<th align='left' >".getSortLink($default_order_by,"comp_name","Company")."</th>".
			"<th>".getSortLink($default_order_by,"c.lead_id","Lead ID")."</th>".
			"<th>".getSortLink($default_order_by,"c.monthly_goal","Monthly Goal")."</th>".
			"<th>".getSortLink($default_order_by,"Actual_Sent","Sent")."</th>".
			"<th>".getSortLink($default_order_by,"Est_Sent","Est. Sent")."</th>".
			"<th>".getSortLink($default_order_by,"deviation","Deviation")."</th>".
			"<th>".getSortLink($default_order_by,"needed_per_day","Needed Per Day")."</th>".
			"<th>".getSortLink($default_order_by,"leads_needed","Leads Needed")."</th></tr>";
	$count = 1;
	
	$font_counter = 1;
	
	while($rs->fetch_array())
	{
		
		extract($rs->myarray);
		
		$companies[$lead_id] = 0;
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count++."</td><td>";
		echo "<a class='black' title='Click to view lead format' href='marbleformat.php?lead_id=$lead_id'>$comp_name</a>";
		echo "</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		echo $Lead_Count."</td><td align='right'>";
		echo $Actual_Sent."</td><td align='right'>";
		echo $Est_Sent."</td><td align='right'>";
		
		
		echo colorCode($Actual_Sent,$Est_Sent);
			
		echo "</td><td align='right'>";
		echo $needed_per_day."</td><td align='right'>";
		echo "<strong>".$leads_needed."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}
	
	foreach($companies as $comp)
	{
		
		if ($comp == 0)
			continue;
		else
			extract($comp);
			
		if ($font_counter % 2 == 0)//even
			$style = "on";
		else
			$style = "off";
			
		echo "<tr class='$style' ><td align='right'>";
		echo $count++."</td><td>";
		
		echo $comp_name."</td><td align='right'>";
		echo $lead_id."</td><td align='right'>";
		echo "$monthly_goal</td><td align='right'>";
		echo "0</td><td align='right'>";
		echo "-</td><td align='right'>";
		
		
		if ($deviation > 0)
			$deviation = "<font color='#000000'><strong>$deviation</strong></font>";
		else if ($deviation < 0)
			$deviation = "<font color='#FF0000'>$deviation</font>";
		else 
			$deviation = "<font color='#00FF00'>$deviation</font>";
			
		echo $deviation."</td><td align='right'>";
		echo "-</td><td align='right'>";
		echo "<strong>".$monthly_goal."</strong></td>";
		echo "</tr>";
		$font_counter++;
	}

?>
</td></tr></table></td></tr></table></td></tr></table>