<link href="irelocation.css" rel="stylesheet" type="text/css">
<br/>
<?php
	
	include "../inc_mysql.php";
	
	/*
	Select
		l.*,
		c.codeName
	from
		movecomp.leads_prd as l
		join
		code as c
		on l.leadStatusCode = c.codeID
	where
		leadDate like '2006-09-27%'
	
	
	*/
	
	//always put orderby last. much easier to strip off.	
	extract($_REQUEST);
	$sql = "select l.*,c.codeName from movecomp.leads_prd as l join code as c on l.leadStatusCode = c.codeID where 1=1";
	
	if (isset($_REQUEST['campaign']))
		$sql .= " and l.campaign  like '".$_REQUEST['campaign']."'";
				
	if (isset($month) && is_numeric($month) && isset($year) && is_numeric($year))
	{
		if (isset($day))
			$sql .= " and l.leadDate like '$year-$month-$day%' ";
		else
			$sql .= " and l.leadDate like '$year-$month-%' ";
	}
	
	
	
	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);
	
	
	
	$ordering = array("homePhone","email","lastName","sellPrice","purchasePrice","codeName");
	if (isset($orderby))
	{		
		if (array_search($orderby,$ordering))
			$sql .= " order by $orderby";						
	}
	?>
	<br/>
	   <div class="mainmovingtopic">Wise Click Leads - Real Estate Leads</div>
	<br/>
	<?
	
	$sql = str_replace("1=1 and","",$sql);
	echo "<!--- ".$_SERVER['REMOTE_ADDR']." --->";
	if ($_SERVER['REMOTE_ADDR'] == "67.72.68.198")
	{
		echo formatSQL($sql);
	}
	
	function formatSQL($display_sql)
	{	
		$display_sql = str_replace("select","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = ereg_replace("[^\.]\*","&nbsp; &nbsp; *",$display_sql);
		$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
		$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
		$display_sql = ereg_replace("('([a-z0-9%_]|-)*')","<font color=\"red\">\\1</font>",$display_sql);
		$display_sql = ereg_replace("(replace|concat|locate|substring)\(","<font color=\"blue\">\\1</font>(",$display_sql);
		return "<font face='Courier New' size='2px'>".trim($display_sql).";</font><br/><br/>";
	}
	
	if (strpos($_SERVER['QUERY_STRING'],"&orderby=") > 0 )
	{				
		echo "<a class='mainmovingtopic' href='".$_SERVER['PHP_SELF']."?$filterlink'>Clear Ordering</a><br/>";
	}	

	$rs = new movecompanion($sql);
	$rowcount = $rs->rowcount();
	
	if ($rowcount == 0)
	{
		echo "<div class='mainmovingtopic'>Zero leads.</div>";
		exit();
	}
	
?>
<table border="1">
	<tr class="mainmovingtext">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=lastName"; ?>' class='mainmovingtopic'>Customer Name</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Email</a></th		
		><th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=homePhone"; ?>' class='mainmovingtopic'>Home Phone</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=purchasePrice"; ?>' class='mainmovingtopic'>Buying Value</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=sellingPrice"; ?>' class='mainmovingtopic'>Selling Value</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=campaign"; ?>' class='mainmovingtopic'>Source</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=codeName"; ?>' class='mainmovingtopic'>codeName</a></th>
		
	</tr>
	<?
		
		while ($rs->fetch_array())
		{			
			extract($rs->myarray);
			$name = "$firstName $lastName";
			echo "<tr class='mainmovingtext'>";
			echo "<td align='left' >$name </td>";			
			echo "<td align='left' >$email</td>";
			echo "<td align='left' >".formatPhone($homePhone)."</td>";
			
			if ($purchasePrice > 0)
				echo "<td align='left' >$".$purchasePrice.",000</td>";
			else
				echo "<td align='left' > -- </td>";
				
			if ($sellPrice > 0)
				echo "<td align='left' >$".$sellPrice.",000</td>";
			else
				echo "<td align='left' > -- </td>";
			echo "<td align='left'>$campaign</td>";
			echo "<td align='left'>$codeName</td>";
			
			echo "</tr>";
		}	

		function formatPhone($phone)
		{	
			if (strlen($phone) == 10)
			{
				$p_area=substr($phone,0,3);
				$p_prefix=substr($phone,3,3);
				$p_suffix=substr($phone,6,4);
		
				return "($p_area) $p_prefix - $p_suffix";
			}
			else
				return $phone;
		}
		
		function getReceived($received)
		{
			$r_year=substr($received,0,4);
			$r_month=substr($received,4,2);
			$r_day=substr($received,6,2);
			$r_hour=substr($received,8,2);
			$r_min=substr($received,10,2);
			if (strlen($received) == 14)
				return "$r_month/$r_day/$r_year $r_hour:$r_min";
			else
				return " -- ";
		}	
	?>
</table>
<br/>
<span class="mainmovingtopic"><?php echo $rowcount; ?></span><span class="mainmovingtext"> leads.</span>
<table>
	<tr class="mainmovingtext">
		<td><a href="realestate.php" class="mainmovingtext">Home</a></td>
		<td>&nbsp;</td>
		<td><a href="logout.php" class="mainmovingtext">Logout</a></td>
		
	</tr>

</table>
<br/>