<?php
	include "../inc_mysql.php";
	include "cron_functions.php";
	
	
	function getProspectString($company, $month, $year)
	{
		$sql = "select substring(received,7,2) day, count(received) count from movingdirectory.quotes ".
				" where mover_id = $company and received like '$year$month%' and mover_status = 'sent' ".
				" group by substring(received,7,2) with rollup;";
		return $sql;
	}
	

	
	function print_it($comp_name, $month, $year, $mover_id)
	{		
		$sql = getProspectString($mover_id,$month,$year);
		
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
			
				
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		$rowcount = 0;
		$counter = 0;
		while ($rs->fetch_array())
		{
			
			$rowcount++;
			$counter++;
			
			$day = $rs->myarray["day"];
								//echo "<!--- $day --->";
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if (strlen(trim($day) > 0))
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				//echo "<a href='$link_base&received=$year$month$day'>";
				echo "$count";
				//echo "</a>";
				echo "</td>";				
			}
			else//total column
			{				
				echo "\n<!--- $ndays $counter ".$rs->rowcount()." --->\n";
				if ($rs->rowcount() < $ndays)
				{
					$delta = ($ndays+1) - $counter;
					if ($delta > 0)
					echo "<td colspan='$delta'>&nbsp;</td>";				
				}
				
				$total  = $rs->myarray["count"];
				
				if ( $mover_id == 17 || $mover_id == 46 || $mover_id == 47 || $mover_id == 48)
					$credits = getCredits($mover_id,$month,$year);
				//$total -= $losses["total"];
				echo "<td align='right' class='mainmovingtopic'";
				
				if ($credits > 0)
					echo" title='".($total - $credits)."' )";
				echo " >";
				echo "$total";
				if ($credits > 0)
					echo "*";
				echo "</td>";				
			}			
		}				
		echo "</tr>\n";
	}
	
	if (!isset($_POST["month"]))
	{		
		$month = date("m");
		$year = date("Y");
	}
	else
		extract($_POST);
	
	$sql = "select 
	u.id,
	u.name,
	count(q.quote_id) as count
from 
	movingdirectory.quotes as q 
	join
	prospect.users as u
	on q.mover_id = u.id
where 
	q.mover_status = 'sent' and q.received like '$year$month%'
group by 
	u.id
order by 
	u.name	

	";

	$rs = new mysql_recordset($sql);
	$companies = array();

	
	?>
	<form action="<?= $_SERVER['PHP_SELF']; ?>?page_id=11" method="post">
	<Select name="month">				
	<?php							
		$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");							
		for ($i = 1; $i < 13; $i++)
		{
			echo "<option value='";
			if ($i < 10)
				echo "0$i";
			else
				echo "$i";
			echo "' ";
			if (intval($month) == ($i)) 
				echo "selected ";
			echo ">".$months[$i-1]."</option>\n";
		}
	?>
	</Select>
	<select name="year">
		<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
		<option value="2007"  <?php if ($year == "2007")  { echo "selected"; } ?>>'07</option>				
	</select>
	&nbsp; &nbsp; 
	<input type='submit'>
	</form>
	<?
	
	
	print_month_header($month,$year,2);
	while ($rs->fetch_array())
	{
		extract($rs->myarray);
		$companies[] = array($id,$name,$count);	
		//echo "$id $name $count <Br/>";
		print_it($name, $month, $year, $id);
	}
	echo "</table>";
	
	function getCredits($lead_id,$month,$year)
	{	
		
		$sql = "select 
				substring(received,1,4) ry,
				substring(received,5,2) rm,
				substring(received,7,2) rd,
				substring(est_move_date,1,4) ey,
				substring(est_move_date,6,2) em,
				substring(est_move_date,9,2) ed
			    from  movingdirectory.quotes where mover_status = 'sent' and 
				received like '$year$month%' and mover_id = $lead_id;";
		$rs = new mysql_recordset($sql);
		$credit = 0;
		while ($rs->fetch_array())
		{
			extract($rs->myarray);
			$start_date=gregoriantojd($rm, $rd,$ry);
			$end_date=gregoriantojd($em, $ed,$ey);
			$differance = $end_date - $start_date;
			
			if ($lead_id == 17)//Chippmans
			{
				if ($differance > 75)
					$credit++;
			}
			else if ($lead_id >= 46 && $lead_id <= 48)//Suddath
			{
				if ($differance < 21)
					$credit++;
			}
		}
		return $credit;
	
	}
	
?>