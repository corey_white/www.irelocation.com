<?php
	include "../../inc_mysql.php";

	session_start();
	
	mail("david@irelocation.com","Auto Login","Time: ".(($_SESSION["login-valid"])?"yes":"no"));
	
	if (!$_SESSION["login-valid"])
	{
		header("Location: /reporting/auto/login.php?login");
		exit();
	}
	else if ((date("Ymdhis") + (60*20)) < $_SESSION["time"])
	{
		header("Location: /reporting/auto/login.php?old");
		exit();
	}
	else
	{
		$_SESSION["time"] = date("Ymdhms");//update session.
	}
	
	if (isset($_POST["month"]))
	{
		$month = $_POST["month"];
		$year = $_POST["year"];	
	}
	else
	{
		$year = date("Y");
		$month = date("m");
	}
	
	function getSiteName($site)
	{
		if ($site=="cs") return "CarShipping.com";
		if ($site=="asc") return "AutoShipping.com";
		if ($site=="atus") return "Auto-Transport.us";
		if ($site=="mmc") return "MoveMyCar.com";
		
		if ($site=="CarShipping.com") return "cs";
		if ($site=="AutoShipping.com") return "asc";
		if ($site=="Auto-Transport.us") return "atus";
		if ($site=="MoveMyCar.com") return "mmc";
	}
	
	function getSites($lead_id,$month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		$sql = "select ".
				"distinct(IF(locate('_',source) > 0,substring(source,1,locate('_',source)-1),source)) 'sites'".
				" from movingdirectory.quotes where	lead_ids like '%$lead_id%' and cat_id = 1 and received like".
				" '$year$month%';";
				echo "\n<!--- $sql --->\n";
		$rs = new mysql_recordset($sql);
		$sites = array();
		while ($rs->fetch_array())
		{
			$site = $rs->myarray["sites"];
			if ($site != "csq")
				$sites[] = $site;		
		}
		sort($sites);
		return $sites;
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}		
	
	function print_it($sql, $month, $year,$title)
	{		
		echo "<!--- $sql --->";
		echo "<!--- $title --->";
		$rs = new mysql_recordset($sql);
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		
		$last = -1;
		$source = getSiteName($title);
		$link_base = "view_leads.php?source=$source";				
			
		
		
		if (substr_count($title,"newwc") > 0)	
			$link_base .= "&new";
				
		echo "<tr>\n<td align='left'><Strong>$title</strong></td>";
		
		while ($rs->fetch_array())
		{
			$counter++;			
			$day = $rs->myarray["day"];											
			$ts = $year.$month.$day;			
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}								
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				echo "<a href='$link_base&received=$year$month$day'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
		echo "<!--- $month/$year --->";
	?>
		<html>
		<head>
		<title>iRelocation Reporting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="irelocation.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<div align="center">
			<form method="post" action="index.php">
			<table width="80%">
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td>&nbsp;</td>
					<td class="mainmovingtopic" colspan="2">Auto Leads </td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">
							<select name="month"  class="mainmovingtopic">
								<option value="08" <? if ($_POST["month"] == "08") echo "selected ";?>>August</option>
								<option value="09" <? if ($_POST["month"] == "09") echo "selected ";?>>September</option>
								<option value="10" <? if ($_POST["month"] == "10") echo "selected ";?>>October</option>
								<option value="11" <? if ($_POST["month"] == "11" ) echo "selected "; ?>>November</option>
								<option value="12" <? if ($_POST["month"] == "12" || !isset($_POST['month'])) echo "selected ";?>>December</option>	
								<option value="01" <? if ($_POST["month"] == "01") echo "selected ";?>>January</option>
								<option value="02" <? if ($_POST["month"] == "02") echo "selected ";?>>February</option>
								<option value="03" <? if ($_POST["month"] == "03") echo "selected ";?>>March</option>
							</select>							
						</td>
					</tr>										
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">						
							<select name="year" class="mainmovingtopic" >
								<option value="2006"<? if ($_POST["year"] == "2006") echo "selected ";?>>2006</option>
								<option value="2007"<? if ($_POST["year"] == "2007") echo "selected ";?>>2007</option>
							</select>						
						</td>
					</tr>										
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2"><input type="submit" value="Show Leads" class="mainmovingtopic" /></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td width="25">&nbsp;</td>
						<td width="*">
						<?					
							$lead_id = $_SESSION['lead_id'];
							print_month_header($month,$year);						
							$sites = getSites($lead_id,$month,$year);
							echo "\n<!--- ".print_r($sites,true)." --->\n";
							foreach($sites as $site)
							{
								$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count' from".
								" movingdirectory.quotes as s where s.received like '$year$month%' and (source ".
								" like '$site%' and lead_ids like '%$lead_id%') group by substring(s.received,1,8)".
								" WITH ROLLUP;";
								$site_name = getSiteName($site);
								
								print_it($sql, $month, $year,$site_name );	
							}
							echo "</table>";					
						?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td><td align="left" ><a href="logout.php" class="mainmovingtext">&lt;Logout&gt;</a></td>						
					</tr>
					<tr class="mainmovingtext">
						<td>&nbsp;</td><td>you will be logged out after 20 minutes of inactivity.</td>
					</tr>
			</table>		
			</form>
		</div>
	<?php echo  " <!--- ".$_SESSION["time"]." --->";?>
	

