<? session_start(); ?>
<link href="irelocation.css" rel="stylesheet" type="text/css">
<br/>
<?php
	
	$security = false;
	include "../../inc_mysql.php";
	
	//always put orderby last. much easier to strip off.	
	extract($_REQUEST);
	
	
	$sql = "select * from movingdirectory.quotes where cat_id = 1 and 1=1";
	
	$lead_id = $_SESSION["lead_id"];
	echo "<!-- $lead -->";
				
	if (isset($received) && is_numeric($received))
		$sql .= " and received like '$received%' ";

	if (isset($lead_id)  && is_numeric($lead_id))
		$sql .= " and lead_ids like '%$lead_id%' ";
		
	if (isset($source)  && !is_numeric($source))
		$sql .= " and source like '$source%' ";
	
		
	$sql = str_replace("1=1 and","",$sql);
	
	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);
		
	
	$ordering = array("received","email","origin_state","destination_state","est_move_date");
	if (isset($orderby))
	{
		if (array_search($orderby,$ordering))
			$sql .= " order by $orderby";		
	}
	?>
	<br/>
	   <div class="mainmovingtopic">Auto Transport Leads</div>
	<br/>
	<?
	echo "<!--- ".$_SERVER['REMOTE_ADDR']." --->";
	if ($_SERVER['REMOTE_ADDR'] == "67.72.68.198")
	{
		echo formatSQL($sql);
	}
	
	function formatSQL($display_sql)
	{	
		$display_sql = str_replace("select","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = ereg_replace("[^\.]\*","&nbsp; &nbsp; *",$display_sql);
		$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
		$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
		$display_sql = ereg_replace("('[a-z0-9%_]*')","<font color=\"red\">\\1</font>",$display_sql);
		$display_sql = ereg_replace("(replace|concat|locate|substring)\(","<font color=\"blue\">\\1</font>(",$display_sql);
		return "<font face='Courier New' size='2px'>".trim($display_sql).";</font><br/><br/>";
	}
	
	if (strpos($_SERVER['QUERY_STRING'],"&orderby=") > 0 )
	{				
		echo "<a class='mainmovingtopic' href='".$_SERVER['PHP_SELF']."?$filterlink'>Clear Ordering</a><br/>";
	}	
	if (!$movecomp)
		$rs = new mysql_recordset($sql);
	else 
		$rs = new movecompanion($sql);
	$rowcount = $rs->rowcount();
	
	if ($rowcount == 0)
	{
		echo "<div class='mainmovingtopic'>Zero leads.</div>";
		exit();
	}
	
?>
<table border="1">
	<tr class="mainmovingtext">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=received"; ?>' class='mainmovingtopic'>Timestamp</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=origin_state"; ?>' class='mainmovingtopic'>Origin</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=destination_state"; ?>' class='mainmovingtopic'>Destination</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=est_move_date"; ?>' class='mainmovingtopic'>Moving Date</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=size_of_move"; ?>' class='mainmovingtopic'>Size of Move</a></th>
	</tr>
	<?php 		
		while ($rs->fetch_array())
		{					
			buildSMQ($rs->myarray);						
		}	

		
		function buildSMQ($array)
		{
			extract($array);
			$received = getReceived($received);
			$customerInfo = ucwords($name)."<br/>".$email."<Br/>".formatPhone($phone_home);
			$origin = ucwords($origin_city).", ".strtoupper($origin_state)."<Br/> $origin_zip";
			$destination = ucwords($destination_city).", ".strtoupper($destination_state);
			$moving = "$est_move_date";
			$comments = str_replace("\n","<br/>",$comments);
			echo "<tr class='mainmovingtext'>";
			echo "<td align='left' valign='top' >$received </td>";
			echo "<td align='left' valign='top' >$customerInfo</td>";
			echo "<td align='left' valign='top' >$origin</td>";
			echo "<td align='left' valign='top' >$destination</td>";
			echo "<td align='left' valign='top' >$moving</td>";
			echo "<td align='left' valign='top' >$comments</td>";
			echo "</tr>";
		}				
		
		function formatPhone($phone)
		{	
			if (strlen($phone) == 10)
			{
				$p_area=substr($phone,0,3);
				$p_prefix=substr($phone,3,3);
				$p_suffix=substr($phone,6,4);
		
				return "($p_area) $p_prefix - $p_suffix";
			}
			else
				return $phone;
		}
		
		function getReceived($received)
		{
			$r_year=substr($received,0,4);
			$r_month=substr($received,4,2);
			$r_day=substr($received,6,2);
			$r_hour=substr($received,8,2);
			$r_min=substr($received,10,2);
			if (strlen($received) == 14)
				return "$r_month/$r_day/$r_year $r_hour:$r_min";
			else
				return " -- ";
		}	
	
		
		function formatData($myarray)
		{
			switch($myarray["current_needs"])
			{
				case "new":	$myarray["current_needs"] = "New System";	break;
				case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
				case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
			}
	
			if ($myarray["quote_type"] == "com")
			{
				switch($myarray["sqr_footage"])
				{				
					case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
					case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
					case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
					case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
					case "0": 
					default:
						$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
				}
			}
			else
			{
				switch($myarray["sqr_footage"])
				{
					case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
					case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
					case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
					case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
					case "0": 
					default:
						$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
				}		
			}
			
			$myarray["maplink"] = getMapLink($myarray);
			$myarray["own_rent"] = ucwords($myarray["own_rent"]);
			$myarray["building_type"] = ucwords($myarray["building_type"]);
			$myarray["other_field"] = ucwords($myarray["other_field"]);
			$myarray["received"] = getReceived($myarray["received"]);
			
			return $myarray;
		}
		function getMapLink($myarray)
		{
			return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
		}		
		
	?>
</table>
<br/>
<span class="mainmovingtopic"><?php echo $rowcount; ?></span><span class="mainmovingtext"> leads.</span>
<table>
	<tr class="mainmovingtext">
		<td><a href="index.php" class="mainmovingtext">Home</a></td>
		<td>&nbsp;</td>
		<td><a href="logout.php" class="mainmovingtext">Logout</a></td>
		
	</tr>

</table>
<br/>