<?
	/*
		This class compares key parts of each quote sequentially via levenshtein search.
		if the differance in adjecent quotes is less than 10 characters the current one is treated
		as a dupe, and skipped.
	*/

	class FlukeFixer
	{
		var $last;
		var $lastQuoteId;

		var $badquote_ids;
		function FlukeFixer()
		{
			$this->last = "";
			$this->badquote_ids = array();
		}
	
		function buildString($rs)
		{
			extract($rs);
			return $name.$email.$phone_home.$est_move_date.$origin_zip.$destination_city.$source;
		}
	
		function getBadQuotes() { return $this->$badquote_ids; }

		function outputBadSql()
		{
			$output = implode(",",$this->badquote_ids);
			$output = str_replace("==",",",$output);
			return "select * from movingdirectory.quotes where quote_id in ($output);";
		}

		//if true, skip this entry.
		function compare($nextarray)
		{
			$current = $this->buildString($nextarray);
			if (strlen($current) > 255) return false;//can't check it too long.
			if ($this->last == "")//first
			{
				$this->last = $current;			
				$this->lastQuoteId = $nextarray['quote_id'];		
				return false;
			}
			else
			{								
				if (levenshtein($this->last,$current) < 10)//bad too similar.
				{
					$this->last = $current;
					//save the quote_id this was compared to.
					$this->badquote_ids[]  = $this->lastQuoteId."==".$nextarray['quote_id'];	
					//	
					$this->lastQuoteId = $nextarray['quote_id'];
					return true;
				}
				else
				{
					$this->last = $current;
					$this->lastQuoteId = $nextarray['quote_id'];
					return false;
				}
			}
		}
	}
?>