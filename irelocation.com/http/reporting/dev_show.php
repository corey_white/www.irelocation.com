<head>
<style>

.visible 
{
	display:block;
}

.invisible
{
	display: none;
}


</style>
<script>
	
	var comp_names = new Array();
	var details = new Array();
	
	//initial setup
	function displayAll(divid)
	{
		var element = document.getElementById(divid);
		element.innerHTML = "";
		for (var j = 0; j < comp_names.length; j++)
		{
			element.innerHTML += "<Strong onclick='showDetails(" + j + ");'>" + comp_names[j] + "</strong><div id='details" + j + "'></div>";
		}
	}

	function setall(show)
	{
		//alert("Set all: " + show);
		for (var j = 0; j < comp_names.length; j++)
		{
			var element = document.getElementById("details" + j);
			if (show)
			{
				force_show(j,element);
			}
			else
			{
				force_hide(element);
			}
		}		
	}
	
	function force_show(which, element)
	{		
		element.innerHTML = "<ul><li>Company Id: " + details[which][0] + "</li><li>Lead Id: " + details[which][1] + "</li><li>Last Lead Received On: " + details[which][2] + " (server time)</li></ul>";
	}
	
	function force_hide(element)
	{		
		element.innerHTML = "";
	}
	
	function showDetails(which)
	{
		var element = document.getElementById("details" + which);
		if (element.innerHTML == "")
		{
			element.innerHTML = "<ul><li>Company Id: " + details[which][0] + "</li><li>Lead Id: " + details[which][1] + "</li><li>Last Lead Received On: " + details[which][2] + " (server time)</li></ul>";
		}
		else
		{
			element.innerHTML = "";
		}
	}
	
</script>
</head>

<?php
	include "cron_functions.php";
	
	echo "<script language='javascript' >";
	$companies = GetCompanies($posted_source,-1,-1);
	$counter = 0;
	foreach($companies as $company)
	{
		extract($company);
		/*
		$name = $company["comp_name"];
		$lead_id = $company["lead_id"];
		$comp_id = $company["comp_id"];
		$last = $company["last"];
		*/
	
		echo "comp_names[$counter] = '$comp_name';\n";
		echo "details[$counter] = new Array(3);\n";
		echo "details[$counter][0] = '$comp_id';\n";
		echo "details[$counter][1] = '$lead_id';\n";
		echo "details[$counter][2] = '$last';\n";
		$counter ++;
	}
	echo "</script>";
	
	echo "<Br/> $counter Companies <Br/>";
	echo "<a href='#' onclick='setall(true);'>Show All</a> - ";
	echo "<a href='#' onclick='setall(false);'>Hide All</a> - ";
	echo "<a href='cron_reporting.php?refresh=1'>Reload Company List</a><br/>";
	
	echo "<br/>Companies that have received any of the past <strong>".NUMBER."</strong> leads from <strong>$source_site</strong>:<br/><br/>\n";
?>
<div align="center">
<table width="90%">
	<tr><td align="left">
		<div id='content'></div>
		<br/>
	</td></tr>
</table>
</div>
<script>displayAll("content");</script>