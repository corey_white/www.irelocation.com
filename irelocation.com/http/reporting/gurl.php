<?

/*
	Name: 			gURL Class
	Author:			Markus Diersbock
	Description:		Like CURL, gURL allows you to GET/POST to an external website.
				Data can be passed both as String and Array.

	Notes:			~ $errflag is set to 1 on error
				~ Prefix $webpage with "/"

	Revisons:		2003/12/12 - Created
*/


class gURL{

	var $errflag;
	var $lasterrdesc;
	var $lasterrno;
	var $version='1.1';

	function execweb($site, $webpage, $extdata, $method){
		$port=80;
		$conntimeout=60;
		$buffer=1024;
		$extdatastr;
		$returndata;

		// Process array/string. For GETs make legal QueryString
		if(is_array($extdata)){
			foreach($extdata as $name => $value){
				if($method=="GET"){
					$extdatastr .=urlencode($name)."=".urlencode($value)."&";
				} else {
					$extdatastr .=urlencode($name)."=".urlencode($value)."&";
				}
			}

			$extdatastr = substr($extdatastr,0,strlen($extdatastr)-1);

		} else {
			$extdatastr=$extdata;
		}

		$fptr=fsockopen($site, $port, $errno, $errstr, $conntimeout);


		if(!$fptr){
			$this->errflag=1;
			$this->lasterrno .= $errno;
			$this->lasterrdesc .= $errstr;
		} else {

			$datalen = strlen($extdatastr);

			// For POSTs create body with form data
			if($method=="POST"){

				$bodystr = "POST ".$webpage." HTTP/1.0\r\n";
				$bodystr .= "Host: ".$site."\r\n";
				$bodystr .= "Content-type: application/x-www-form-urlencoded\r\n";
				$bodystr .= "Content-length: ".$datalen."\r\n";
				$bodystr .= "\r\n";
				$bodystr .= $extdatastr."\n\n";
			}


			// For GETs create body with passed QueryString
			if($method=="GET"){
				if(strlen($extdatastr)>0){
					$bodystr = "GET ".$webpage."?".$extdatastr." HTTP/1.0\r\n";
				} else {
					$bodystr = "GET ".$webpage." HTTP/1.0\r\n";
				}

				$bodystr .= "Host: ".$site."\n\n";
			}

				fputs($fptr, $bodystr);

				while(!feof($fptr)){
					$returndata .= fgets($fptr, $buffer);
				}
		}

		return $returndata;
	}
}


?>