<?
	include "../inc_mysql.php";
	include "cron_functions.php";
	include "cpc.php";
	if ($_POST['month'] == 'XX' || strlen($_POST['month']) == 0)
		$month = date("m");	
	else
		$month = $_POST['month'];

	if ($_POST['year'] == 'XX' || strlen($_POST['year']) == 0)
		$year = date("Y");
	else
		$year = $_POST['year'];
	
	if ($_POST['site'] == 'XX' || strlen($_POST['site']) == 0)
	{
		$site = 'cs';
		$database = "carshipping";
	}
	else
		$site = $_POST['site'];
		
		
	$c = $_POST['campaign'];	
	if ($c == "XX") $c = "";
	if (strlen($c) > 0)
	{
		$campaign_name = $c;
		if ($c != "organic")
			$campaign = " and q.source like 'asc%' AND q.source like '%".$c."%' and q.source not like '%search%'";
		else
			$campaign = " and (q.source like 'asc' or q.source like 'asc_search%')";
	}
	else 
	{
		$campaign = "";	
		$campaign_name = "";
	}
		
	function print_asc_month_header($engine="google", $month="*",$year="*")
	{	
		echo "<!--- Engine: $engine --->";
		$site = 'asc';
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
		$name = "";
		$cell = "";
		$total = "";

		$numbers = getCPCData($year.$month,$site ,$engine);
		
		echo "<table border='1' class='reportingtext' >";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
		{
			echo "<td $cell align='right' ";
			
			if (strlen($numbers[$i]["cost"]) > 0)
			{
				//echo " title='Cost: ".$numbers[$i]["cost"]." <Br/>Clicks: ".$numbers[$i]["clicks"]." ' ";
				$cost = $numbers[$i]["cost"];
				$clicks = $numbers[$i]["clicks"];
				$leads = $numbers[$i]["leads"];
				$cpc = $numbers[$i]["cpc"];
				$cpl = $numbers[$i]["cpl"];
				$day = intval($month)."/$i/$year";
				echo " onmouseover=\"moveData(event,'".$day."','$cost',$clicks,$leads,'$cpc','$cpl');\" onmouseout='hide();' ";
			}
			else if ($i != date("j") && $engine != "overture")
			{
				if ($i < 10)
					$day = "0$i";
				else 
					$day = $i;
				echo " onclick=\"getData('".$site."','".$day."','".$month."','google');\" title='Click to Gather Data' ";
			}

			echo " >";
			echo "$i</td>";
			
		}
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
		
	function getTotalSql($time,$campaign_info)
	{
		$sql = "select 	
					substring(received,7,2) 'day',
					count(*) 'count'
				From 
					autoshipping.quotes as q
				where 	
					q.received like '$time%' 
					$campaign_info
				group by 
					substring(received,7,2)
				with rollup;";
		return $sql;
	}
	
	$time = $year.$month;	
	$total_sql = getTotalSql($time,$campaign);
		
	$rst = new mysql_recordset($total_sql);
	
	$leadcounts = array();
	echo "<!-- ";
	$totals = array();
	
	while ($rst->fetch_array())
	{
		$day = $rst->myarray["day"];
		if (strlen($day) == 0)
			$day = "total";
		else
			$day = intval($day);
		
		$totals[$day] = $rst->myarray["count"];		
		echo "$day ".$totals[$day]."<Br/>";
	}
	
	echo " -->";
	
	//sort($leadcounts);
	$comps = array_keys($leadcounts);
	sort($comps);
	$company_count = count($comps);
	echo "<table width='100%'><tr><td width='40'>&nbsp;</td><td width='350'>";
	?>
		<div class="mainmovingtopic">
		We only have Ad-Campaigns with Ask,Google, and Overture for AutoShipping.com.
		<br/><br/>
		</div>
		<form action="<?= $_SERVER['PHP_SELF'] ?>?page_id=1" method="post">
		<select name="campaign">
			<option value="XX" <?php if ($_POST['campaign'] == "all")  { echo "selected"; } ?>>All Leads</option>				
			<option value="organic" <?php if ($_POST['campaign'] == "organic")  { echo "selected"; } ?>>Organic Leads</option>
			<option value="google" <?php if ($_POST['campaign'] == "google")  { echo "selected"; } ?>>Google</option>
			<option value="overture" <?php if ($_POST['campaign'] == "overture")  { echo "selected"; } ?>>Overture</option>
			<option value="msn" <?php if ($_POST['campaign'] == "msn")  { echo "selected"; } ?>>MSN</option>	
			<option value="ask" <?php if ($_POST['campaign'] == "ask")  { echo "selected"; } ?>>Ask.com</option>				
		</select>	
		<Select name="month">		
			<?php			
				$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");			
				for ($i = 1; $i < 13; $i++)
				{
					echo "<option value='";
					if ($i < 10)
						echo "0$i";
					else
						echo "$i";
					echo "' ";
					if (intval($month) == ($i)) 
						echo "selected ";
					echo ">".$months[$i-1]."</option>\n";
				}
			?>
		</Select>
		<select name="year">
			<option value="2007"  <?php if ($year == "2007") { echo "selected"; } ?>>'07</option>	
			<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
		</select>
		<input type="submit" value="Submit" name="submit" />
		</form>
	
	
	<?
	echo "</td>";
	
	$sql = "select sum(cost) cost from movingdirectory.costperclick where ".
			"site = 'asc' and ts like '".date("Y").date("m")."%';";		
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();	
	$cost = $rs->myarray["cost"];
	
	
	
	$sql = "select count(*) count from autoshipping.quotes where ".
			"received like '".date("Y").date("m")."%' and received not like ".
			"'".date("Y").date("m").date("d")."%'";
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();	
	$count = $rs->myarray["count"];
	
	$avg_cost_per_lead = ($cost/$count);
	$profit_per_lead = (.92*6) - $avg_cost_per_lead;
	$monthly_revenue = $count*(.92*6);
	$profit = ($monthly_revenue - $cost);
	$avg_cost_per_lead = number_format($avg_cost_per_lead,2,".",",");
	$monthly_revenue = number_format($monthly_revenue,2,".",",");
	$cost = number_format($cost,2,".",",");
	$profit_per_lead = number_format($profit_per_lead,2,".",",");
	$count = number_format($count,0,".",",");
	if ($profit < 0)
		$profit = "<font color='red'>\$$profit</font>";
	else
		$profit = "<font color='green'>\$$profit</font>";
		
	if ($profit_per_lead < 0)
		$profit_per_lead = "<font color='red'>\$$profit_per_lead</font>";
	else
		$profit_per_lead = "<font color='green'>\$$profit_per_lead</font>";
	
	echo "<td class='mainmovingtopic' valign='top' align='left'>";
	echo "<table width='350'>";
	echo "<Tr><td colspan='3'>Numbers do NOT include today. and are based on $.92 per lead per company</td><Td>&nbsp;</td></tr>";
	echo "<Tr><td align='left'>Monthly Cost:</td><td align='right'> \$".$cost."</td><Td>&nbsp;</td></tr>";
	echo "<Tr><td align='left'>Monthly Revenue: </td><td align='right'>\$".$monthly_revenue.
			"</td><Td align='right'>$profit</td></tr>";
	echo "<Tr><td align='left'>Avg Cost Per Lead:</td><td align='right'> \$".$avg_cost_per_lead.
			"</td><td align='right'>$profit_per_lead</td></tr>";
	
	echo "<Tr><td align='left'>Leads So Far:</td><td align='right'> $count</td><Td>&nbsp;</td></tr>";
	echo "</table>";
	echo "</td></tr>";
	echo "<Tr><td colspan='3'>&nbsp;</td></tr><tr><td width='40'>&nbsp;</td><td colspan='2'>";
	
	if (strlen($campaign_name) > 0)
	{		
		print_asc_month_header($campaign_name,$month,$year);
	}
	else
	{
		print_month_header($month,$year);		
	}
	$ndays = getNDays($month,$year);
	echo "<!-- $ndays -->\n";

	$today = date("d");
	if (intval($month) != intval(date("m")))
	{
		$today = $ndays+1;
	}
			
	echo "<Tr><td><strong>Site Total</strong></td>";
	for ($i = 1; $i <= $ndays && ($i <= $today); $i++)
	{
		echo "<td>".$totals[$i]."</td>";
	}
	if ($today < $ndays)
	{
		echo "<td colspan='".($ndays-$today)."'>&nbsp</td>";
	}
	echo "<td><strong>".$totals["total"]."</strong></td></tr>";
		
	foreach ($comps as $company)
	{
		$values = $leadcounts[$company];
		echo "<Tr><td><strong>$company</strong></td>";
		for ($i = 1; $i <= $ndays && ($i <= $today); $i++)
		{
			echo "<td>".$values[$i]."</td>";
		}
		if ($today < $ndays)
		{
			echo "<td colspan='".($ndays-$today)."'>&nbsp</td>";
		}
		echo "<td><strong>".$values["total"]."</strong></td></tr>";
	}
	
	echo "</table></td></tr><tr><td colspan='3'>&nbsp;</td></tr></table>";
	
?>