<?php
	
	include '../inc_mysql.php';
	include 'lib_functions.php';
	include 'lead_grids_functions.php';
	
	$mode = 0;
	$sites = array(  array("default","All Sites"),  array("das","Dependable AutoShipping"), array("cs","CarShipping.com"), array("tm","TopMoving.com"), array("pm","ProMoving.com"), array("me","MovingEase.com"), array("mmc","MoveMyCar.com"), array("atus","AutoTransports.us"), array("re","Real Estate"));

	$months = array ("January","February","March","April","May","June","July","August","September","October","November","December");
	
	$curr_month = date('n');
	$curr_day = date('j');
	
	$distinct = $_POST["distinct"];
	$campaign = $_POST["campaign"];	
	$source = $_POST["source"];

	if (strlen($source) == 0)//for url query.
	{
		$source = $_REQUEST["source"];
		$month = date('m');
		$year = date('Y');		
	}
	else
	{		
		$month =  $_POST["month"];					
		$year =  $_POST["year"];		
	}
	
	if ($source == 'default')
		$mode = 1;
		
	if (strlen($campaign) == 0)//for url query.
		$campaign = "*";
		
	echo "<!--- Campaign: $campaign --->\n";
	
	if (strlen($source) == 0)//for url query.
	{
		$mode = 1;	
		$month = date('m');
		$year = date('Y');
	}
	
	else if ($source == "das")
		$type = DAS;
	else if ($source == "re")
		$type = REAL_ESTATE;
	else
		$type = '*';
		
	if ($month != '')
	{
		$curr_month = $month;
		$ndays = cal_days_in_month(CAL_GREGORIAN, intval($curr_month), $year);	
	}
	else
		$ndays = date('t');
?>
<br/>
<div align='center'>
<table>
	<tr>
		<td class="reportingtext">
		<form action="reporting.php?page_id=0" method="post">
			<select name="source">
				<?php
					foreach($sites as $site)
						if ($site[0] == $source)
							echo "<option value='".$site[0]."' selected > ".$site[1]." </option>\n";					
						else
							echo "<option value='".$site[0]."' > ".$site[1]." </option>\n";	
				?>
			</select>			
			<select name="campaign">
				<option value="*"  <?php if ($campaign == "*") echo "selected"; ?> > All Leads </option>
				<option value="organic" <?php if ($campaign == "organic") echo "selected"; ?> > Organic Leads </option>
				<option value="msn" <?php if ($campaign == "msn") echo "selected"; ?> > Leads from MSN </option>
				<option value="google" <?php if ($campaign == "google") echo "selected"; ?> > Leads from Google </option>
				<option value="overture" <?php if ($campaign == "overture") echo "selected"; ?> > Leads from Overture </option>
			</select>
			<select name="month"> 
				<?php					
					for ($j = 1; $j < 13; $j++)
						if ($j== $curr_month)
						{
							echo "<option value='$j' selected > ".$months[$j-1]." </option>\n";				
						}
						else
							echo "<option value='$j' > ".$months[$j-1]." </option>\n";				
				?>
			</select>			
			<select name="year"> 
				<option>2004</option>
				<option>2005</option>
				<option selected>2006</option>
			</select>
			
			<input type="checkbox" name="distinct" <?php if ($distinct) { echo "checked"; } ?> /> Distinct Emails
			
			<input type="submit" value="Display"/>			
		</form>
		</td>
	</tr>
	<tr>
		<td>
<?php
	if ($mode == 0)
	{
		displayCompanyGrid($codes,$year, $month, $source, $type,$campaign);
	}
	else 
	{
		displayCompanyGrid($whole_sites,$year,  $month, $source, $type,$campaign);
	}
?>

		<br/><br/>
		</td>
	</tr>
</table>
</div>