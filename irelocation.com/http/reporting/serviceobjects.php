<?
	define(THRESHOLD,65);
	define(ADDRESS_COMP,1.23);
	define(WIRELESS_COMP,1.5);
	
	include "../inc_mysql.php";
	require_once('../nusoap/nusoap.php');
	
	function validateLead($params)
	{
		//Set Up Soap Client for LeadValidation
		$soapClient = new SoapClient( SO_WSDL_URL, "wsdl" );
		
		//Set up GetContactInfo's parameters
		
		
		//Call ValidateLead Operation
		$proxy = $soapClient->getProxy();
		$startTime = time() + microtime();
		$result = $proxy->ValidateLead($params);
		$queryTime = time() + microtime() - $startTime;
		$result = $result['ValidateLeadResult'];
		
		return $result;
	}
	
	
	define(SO_IRELO_KEY,"WS40-ZCB3-NVK1");
	define(SO_WSDL_URL,"http://trial.serviceobjects.com/lv/leadvalidation.asmx?WSDL");
	
	function getParams($quote_id)
	{
		$sql = "select * from movingdirectory.quotes_local where ".
				" quote_id = '".$quote_id."'";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		
		//Setup Parameter Values
		$name = $rs->myarray['name'];
		$city = $rs->myarray['origin_city'];
		$state = $rs->myarray['origin_state'];
		$zip = $rs->myarray['origin_zip'];
		$phone = $rs->myarray['phone_home'];
		
		if ($rs->myarray['phone_work'] != "")
		{
			$phone2 = $rs->myarray['phone_work'];
			$type = 'full2p';
		}
		else
			$type = 'full1p'; //full2p if 2 phone numbers are specified	
			
		$email = $rs->myarray['email'];
		if ($rs->myarray['remote_ip'] == "")
			$ip = '70.56.176.249';
		else
			$ip = $rs->myarray['remote_ip'];
		
		$key = SO_IRELO_KEY;
	
		$params['Name'] = $name;
		$params['Address1'] = '';
		$params['Address2'] = '';
		$params['City'] = $city;
		$params['State'] = $state;
		$params['Zip'] = $zip;
		$params['Country'] = 'USA';
		$params['Phone1'] = $phone;
		$params['Phone2'] = $phone2;
		$params['Email'] = $email;
		$params['IP'] = $ip;
		$params['TestType'] = $type;
		$params['LicenseKey'] = $key;
		
		return $params;
	}
	
	$params = getParams($_REQUEST['quote_id']);
	$result = validateLead($params);
	
	$overall = $result['OverallCertainty'];
	
	if ($result['Phone1LineOut'] == "WIRELESS")
	{
		$overall *= WIRELESS_COMP;
	}
	
	$overall *= ADDRESS_COMP;//compensate for address.
	
	echo $overall." - ";
	
	if ($overall > THRESHOLD)
		echo "Accept";
	else
		echo "Reject";
	
	echo "<br/>\n<pre>".print_r($result,true)."</pre>";
?>