<?
	include "../inc_mysql.php";
	session_start();
	
	
	$sortBy = "";
	
	function printLib()
	{
		?>	
			
			<HEAD>
			<style>
			table.sortable thead 
			{
				background-color:#eee;
				color:#666666;
				font-weight: bold;
				font-family: Verdana;
				font-size:12px;
				cursor: default;
			}
			
			tr
			{
				font-family: Verdana;
				font-size:11px;
				border:solid;
				border-color:#333333;
				
			}
			
			tr.odd
			{
				font-family: Verdana;
				font-size:11px;
				border:solid;
				border-color:#333333;
				background-color:#CCCCCC;			
			}
			
			</style>
			<script type="text/javascript" src="sorttable.js"></script>
			</HEAD>
		<?	
	}
	
	function printHeaderRow($row)
	{
		echo "<!--- header row --->";
		$names = array_keys($row);
		$row = "<thead>\n<tr>";
		for($i=0;$i < count($names); $i++)
		{
			if (!is_numeric($names[$i]))
				$row .= "<th>".$names[$i]."</th>";
		}
		$row .= "</tr>\n</thead>";
		return $row;
	}
	
	function printTable()
	{
		$sql = $_SESSION['ajax_table_sql'];
		$rs = new mysql_recordset($sql);
		$table = "<table class='sortable'>\n";
		if ($rs->fetch_array())
		{
			$table .= printHeaderRow($rs->myarray)."<tbody>\n";
			$table .= printRow($rs->myarray);		
		}
		while($rs->fetch_array())
		{			
			$table .= printRow($rs->myarray);		
		}		
		$table .= "</tbody>\n</table>\n";
		return $table;
	}
	
	function printRow($rsarray)
	{
		$row = "<tr>";
		foreach($rsarray as $key => $value)
		{
			if (is_numeric($key))
				continue; 
			if (is_numeric($value))
				$row .= "<th class='numeric'>$value</th>";
			else
				$row .= "<td>$value</td>";
		}
		$row .= "</tr>\n";
		return $row;
	}
	
	echo "</html>";
	$_SESSION['ajax_table_sql'] = " select 
			k.keyword,
			c.keywordid,
			count(clickid) 'clicks',
			(totalspend/count(clickid)) 'avgcpc',
			totalspend,
			sum(if(quoteid=0,0,1)) 'conversions',
			sum(if(quoteid=0,0,1))/count(clickid)*100 'convperc',
			count(clickid)/impressions*100 'ctr',
			avgpos,
			impressions,
			totalspend/sum(if(quoteid=0,0,1)) 'cpa'
		from 
			tk_clicks as c 
			left join 
			tk_spendings as s 
			on s.keywordid = c.keywordid 
			join
			tk_keywords as k
			on s.keywordid = k.keywordid
		where 
			received like '20070508%'
			and substring(received,1,8) = s.date
		group by
			c.keywordid";
	echo printLib();
	echo "<body>";	
	echo printTable();
	echo "</html>";
?>