<?php
	include "../../inc_mysql.php";
	
	header("Location: new.php");
	exit();
	
	session_start();
	if ($_SESSION["username"] != "wiseclick")
	{
		header("Location: /reporting/wiseclick/login.php?login");
		exit();
	}
	else if ((date("Ymdhms") + (60*20)) < $_SESSION["time"])
	{
		header("Location: /reporting/wiseclick/login.php?old");
		exit();
	}
	else
	{
		$_SESSION["time"] = date("Ymdhms");//update session.
	}
	
	if (isset($_POST["month"]))
	{
		$month = $_POST["month"];
		$year = $_POST["year"];	
	}
	else
	{
		$year = date("Y");
		$month = date("m");
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
	
	function print_realtor($month,$year, $campaign="XX")
	{
		$total_sql = "select count(*) total from movecomp.leads_prd where leadDate like '$year-$month%' and campaign like '%wc%'";
		if (is_numeric($campaign))
		{
			$extra = " and campaign like '%wc_$campaign%'";
			$title = "WC$campaign Real Estate Leads";
		}
		else
		{
			$extra = " and campaign not like '%wc_3%' and campaign not like '%wc_4%'";
			$title = "Real Estate Leads";
		}
		$total_sql = $total_sql.$extra;
		
		$rs = new movecompanion($total_sql);
		$rs->fetch_array();
		$total = $rs->myarray["total"];
		
		
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		$last = -1;
		$sql = "select substring(leadDate,9,2) 'day', count(leadDate) 'count'  from movecomp.leads_prd where leadDate like '$year-$month%' and campaign like '%wc%' $extra group by substring(leadDate,9,2);";
		
		$link_base = "realtor_leads.php?year=$year&month=$month";
		
		$rs = new movecompanion($sql);
		
		echo "<tr>\n<td align='left'><Strong>$title</strong></td>";
		if ($total == 0)
		{
			echo "<td colspan='$ndays'>&nbsp;</td><td align='right' ><strong>0</strong></td></tr>";
			return;
		}
		while ($rs->fetch_array())
		{			
			$counter++;			
			$day = $rs->myarray["day"];

			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				echo "<a href='$link_base&day=$day'>";
				echo "$count";
				echo "</a>";
				echo "</td>";				
			}			
		}				
		if ($day == $ndays)
		{
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else if ((intval($ndays) - intval($last)) > 0)
		{
			echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else if ($day < $nday)
		{
			$delta = $ndays - $day;
			echo "<td colspan='$delta'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		else
		{
			echo "<td align='right'>";
			echo "<a href='$link_base'>";
			echo $total;
			echo "</a>"; 
			echo "</td>";
		}
		echo "</tr>\n";
	}
	
	function print_it($sql, $month, $year,$title)
	{		
		$rs = new mysql_recordset($sql);
		$ndays = getNDays($month,$year);
		$counter = 0;
	
		$last = -1;
		$data = array();

		while ($rs->fetch_array())
		{
			$day = $rs->myarray['day'];
			if (intval($day) < 10) $day = "0".intval($day);
			if (intval($day) != 0)
			{
				$count = $rs->myarray['count'];			
				$total += $count;
				$data[$day] += $count;
			}
		}
		$keys = array_keys($data);
		if ($title == "Moving Leads (wc)")
		{
			$sql2 = "select substring(received,7,2) 'day', count(*) 'count'  from movingdirectory.quotes_local where received like '$year$month%' and source like '%wc%' group by substring(received,7,2);";
			$rs = new mysql_recordset($sql2);
			while ($rs->fetch_array())
			{
				$day = $rs->myarray['day'];
				if (intval($day) < 10) $day = "0".intval($day);
				if (intval($day) != 0)
				{
					$count = $rs->myarray['count'];			
					$total += $count;
					$data[$day] += $count;
				}
			}
			//mail("david@irelocation.com","WC Data Array",print_r($data,true));			
			
			$keys = array_keys($data);
			sort($data);
			sort($keys);
			mail("david@irelocation.com","WC Data Array",print_r($data,true));
		}
		$day = 0;
		$last = -1;
		if ($title == "Failed Leads")
			$link_base = "view_leads.php?failed";
		else if ($title == "Security Leads")
			$link_base = "view_leads.php?site=tsc";		
		else
			$link_base = "view_leads.php?good";				
			
		
		if (substr_count($title,"newwc") > 0)	
			$link_base .= "&new";
				
		echo "<tr>\n<td align='left'><Strong>$title</strong></td>";
		
		foreach($data as $day => $count)
		{
			
			if ($day == "total")
				continue;
		
			$counter++;			
			//$day = $rs->myarray["day"];											
			$ts = $year.$month.$day;			
			//$rs->myarray['count'] += $data[$day];
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}								
			
			if ($day != $last)
			{
				$last = $day;				
				//$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";	
				if ($day < 10) $outputday = "0".intval($day);
				else  $outputday =  $day;
				echo "<a href='$link_base&received=$year$month$outputday'>";
				echo "$count</a></td>";
			}			
		}		
		
		//$total  = $rs->myarray["count"];
		if ($day == $ndays)
		{
			echo "<td align='right'>";
			echo "<a href='$link_base&received=$year$month'>";
			echo "$total</a></td>";
		}
		else if ((intval($ndays) - intval($last)) > 0)
		{
			echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base&received=$year$month'>";
			echo "$total</a></td>";
		}
		else if ($day < $nday)
		{
			$delta = $ndays - $day;
			echo "<td colspan='$delta'>&nbsp;</td>";
			echo "<td align='right'>";
			echo "<a href='$link_base&received=$year$month'>";
			echo "$total</a></td>";
		}
		else
		{
			echo "<td align='right'>";
			echo "<a href='$link_base&received=$year$month'>";
			echo "$total</a></td>";
		}
		
				
		echo "</tr>\n";
	}
		echo "<!--- $month/$year --->";
	?>
		<html>
		<head>
		<title>iRelocation Reporting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="irelocation.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<div align="center">
			<form method="post" action="index.php">
			<table width="80%">
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2">Wise Click Leads</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">
							<select name="month"  class="mainmovingtopic">
								<option value="08" <? if ($_POST["month"] == "08") echo "selected ";?>>August</option>
								<option value="09" <? if ($_POST["month"] == "09") echo "selected ";?>>September</option>
								<option value="10" <? if ($_POST["month"] == "10") echo "selected ";?>>October</option>
								<option value="11" <? if ($_POST["month"] == "11") echo "selected ";?>>November</option>
								<option value="12" <? if ($_POST["month"] == "12") echo "selected ";?>>December</option>	
								<option value="01" <? if ($_POST["month"] == "01") echo "selected ";?>>January</option>
								<option value="02" <? if ($_POST["month"] == "02" || $_POST['month'] == "") echo "selected ";?>>February</option>
								<option value="03" <? if ($_POST["month"] == "03") echo "selected ";?>>March</option>
							</select>						
						</td>
					</tr>										
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">						
							<select name="year" class="mainmovingtopic" >
								<option value="2007"<? if ($_POST["year"] == "2007") echo "selected ";?>>2007</option>								
								<option value="2006"<? if ($_POST["year"] == "2006") echo "selected ";?>>2006</option>
								
							</select>						
						</td>
					</tr>										
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2"><input type="submit" value="Show Leads" class="mainmovingtopic" /></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td width="25">&nbsp;</td>
						<td width="*">
						<?					
							print_month_header($month,$year);						
							$sql = "select substring(received,7,2) 'day', count(received) 'count'  from movingdirectory.quotes where received like '$year$month%' and (source like '%wc%' and cat_id = '2') group by substring(received,7,2);";
							print_it($sql, $month, $year,"Moving Leads (wc)");	
							
							
							$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count'  from irelocation.leads_security as s where s.received like '$year$month%' and source like 'aff_wc%' group by substring(s.received,7,2);";
							print_it($sql, $month, $year,"Security Leads");	
							print_realtor($month,$year);
							print_realtor($month,$year,3);
							print_realtor($month,$year,4);
							echo "</table>";					
						?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td><td align="left" ><a href="logout.php" class="mainmovingtext">&lt;Logout&gt;</a></td>						
					</tr>
					<tr class="mainmovingtext">
						<td>&nbsp;</td><td>you will be logged out after 20 minutes of inactivity.</td>
					</tr>
			</table>		
			</form>
		</div>
	<?php echo  " <!--- ".$_SESSION["time"]." --->";?>
	

