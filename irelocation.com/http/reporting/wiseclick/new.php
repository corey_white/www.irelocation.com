<?	
	include "../../inc_mysql.php";
	
	function getRealEstateLeadCount($time,$campaign)
	{
		$mcdate = substr($time,0,4)."-".substr($time,4,2);
		$sql = "select
					substring(leadDate,9,2) 'day', 
					count(*) 'count'
				from 
					leads_prd 	
				where 
					campaign like '%$campaign%' 
					and leadDate like '$mcdate%' 
				group 
					by substring(leadDate,9,2);";
		$rs = new movecompanion($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']] += $rs->myarray['count'];

		echo "<!--- ";
		print_r($data);
		echo " --->";

		return $data;
	}

	function getSecurityLeadCount($time)
	{
		$sql = "Select
					substring(received,7,2) 'day',
					count(*) 'count'
				from
					irelocation.leads_security
				where
					(source like '%wc%' and
					received like '$time%') 
				group by 
					substring(received,7,2);";
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']] += $rs->myarray['count'];

		echo "<!--- ";
		print_r($data);
		echo " --->";

		return $data;
	}

	function getMovingLeadCount($time)//pass in a month - 200702.
	{
		$sql = "Select
					substring(received,7,2) 'day',
					count(*) 'count'
					from
					movingdirectory.quotes
					where
					(source like '%wc%' and
					cat_id = 2 and
					received like '$time%') 
					group by substring(received,7,2) 
				UNION ALL 
				Select
					substring(received,7,2) 'day',
					count(*) 'count'
					from
					movingdirectory.quotes_local
					where
					(source like '%wc%' and
					received like '$time%')
					group by substring(received,7,2) 
				order by 'day';";
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']] += $rs->myarray['count'];
		/*
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		*/
		return $data;
	}

	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}

	function printSelected($option,$selected) 
	{
		if ($option == $selected || ($selected == "" && $option == date("Ym")))
			return " selected ";
	}

	function printHeader($time)
	{
		echo "<table border='1'>";
		echo "<tr class='mainmovingtopic' ><th align='left' class='label' >Lead Type</th>";
		
		$days = getNDays(substr($time,4,2),substr($time,0,4));
		for($i = 1; $i <= $days; $i++)
		{
			echo "<td>$i</td>";
		}
				
		echo "<th>Total</th></tr>";
	}

	function printRow($title,$cat,$dataArray, $time)
	{
		echo "\n<tr >\n<th align='left' class='label' >$title</th>";
		$total = 0;
		$counter = 1;	

		$day_count = 1;
		$days = getNDays(substr($time,4,2),substr($time,0,4));

		foreach($dataArray as $day => $count)
		{
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
	
			if (intval($day) == $day_count)//
				echo "<td>$count</td>";
			$total += $count;		
			$day_count++;
		}
		if ($day_count <= $days)
		{
			echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
		}
		echo "<th>$total</th>";
		echo "\n</tr>\n";
	}

	$ts = $_REQUEST['ts'];
	if ($ts == "") $ts = date("Ym");
	?>
	<style>
		.dropdown
		{
			font-size:12px;
			font-family:Verdana;
			text-align:left;
		}
		.label
		{
			font-size:11px;
			font-family:Verdana;
			font-weight:bold;
			text-align:left;
		}
		th
		{
			font-size:11px;
			font-family:Verdana;
			font-weight:bold;
			text-align:right;
		}
		td
		{
			font-size:11px;
			font-family:Verdana;
			text-align:left;
		}

	</style>
	<span style="font-size:10px ">
	<font face="Verdana">
	<form action="new.php" method="get" >
	
	<table>
		<tr><td colspan="2">&nbsp;</td></tr>
		<tr>
			<td>Month:</td>
			<td><select name="ts" class="dropdown" onChange="window.location='new.php?ts='+this.options[this.selectedIndex].value;">
					<optgroup label="2007">					
						<option value="200708" <?= printSelected(200708,$ts) ?>>AUG</option>						
						<option value="200707" <?= printSelected(200707,$ts) ?>>JUL</option>						
						<option value="200706" <?= printSelected(200706,$ts) ?>>JUN</option>						
						<option value="200705" <?= printSelected(200705,$ts) ?>>MAY</option>						
						<option value="200704" <?= printSelected(200704,$ts) ?>>APR</option>						
						<option value="200703" <?= printSelected(200703,$ts) ?>>MAR</option>						
						<option value="200702" <?= printSelected(200702,$ts) ?>>FEB</option>
						<option value="200701" <?= printSelected(200701,$ts) ?>>JAN</option>
					</optgroup>
				</select> ( selecting a new month will automatically change the data. )
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">&nbsp;</td>
		</tr>
	</table>
	</form>	
	<?
	
		//print the table.
		printHeader($ts);
		printRow("Moving Leads",2,getMovingLeadCount($ts),$ts);
		printRow("Security Leads",1,getSecurityLeadCount($ts),$ts);
		printRow("Real Estate Leads (wc)",3,getRealEstateLeadCount($ts,"wc_2"),$ts);
		printRow("Real Estate Leads (wc3)",3,getRealEstateLeadCount($ts,"wc_3"),$ts);
		echo "</table>";//close it.
	?>
	</font>
	</span>