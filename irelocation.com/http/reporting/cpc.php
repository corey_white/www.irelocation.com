<Script>
// Browser Window Size and Position
// copyright Stephen Chapman, 3rd Jan 2005, 8th Dec 2005
// you may copy these functions but please keep the copyright notice as well

function getData(site,day,month,engine)
{
	window.open( "http://irelocation.com/adwords/test.php?site="+site+"&reload=1&day="+day+"&month="+month, '_blank' ,'width=250,height=250');
}

function posLeft() 
{	
	return typeof window.pageXOffset != 'undefined' ? window.pageXOffset:document.documentElement && document.documentElement.scrollLeft? document.documentElement.scrollLeft:document.body.scrollLeft? document.body.scrollLeft:0;
} 
function posTop() 
{
	return typeof window.pageYOffset != 'undefined' ? window.pageYOffset:document.documentElement && document.documentElement.scrollTop?document.documentElement.scrollTop: document.body.scrollTop?document.body.scrollTop:0;
} 

function browserX(e)
{
	return e.screenX - posLeft();
}

function browserY(e)
{
	return e.screenY- posTop();
}

function hide()
{
	var el = document.getElementById("cpcdata");
	el.style.top = "-50px";
	el.style.left = "-200px";
	el.style.visibility = "hidden";
	document.getElementById("day").innerHTML = "-1";
}

function moveData(event,day,cost,clicks,leads,cpc,cpl)
{
	if (document.getElementById("day").innerHTML != day)
	{
		var x = browserX(event);
		var y = browserY(event);
		if (x > 1280)
			x -= 1280;
		var el = document.getElementById("cpcdata");
		el.style.top = (y - 125) + "px";
		el.style.left = x + "px";
		el.style.visibility = "visible";
		document.getElementById("cost").innerHTML = "$"+cost;
		document.getElementById("clicks").innerHTML = clicks;
		document.getElementById("leads").innerHTML = leads;
		document.getElementById("cpc").innerHTML = "$"+cpc;
		document.getElementById("cpl").innerHTML = "$"+cpl;
		document.getElementById("day").innerHTML = day;
	}	
}

</Script>
<div id='cpcdata' style='visibility:hidden; position:absolute; top:-50; left:100;'>
	<Table border="1" bgcolor="#CCCCCC">		
		<tr>
			<th align='left' >Day:</th>
			<td id='day'>$cost</td>
		</tr>
		<tr>
			<th align='left' >Cost:</th>
			<td id='cost'>$cost</td>
		</tr>
		<tr>
			<th align='left' >Clicks:</th>
			<td id='clicks'>$clicks</td>
		</tr>
		<tr>
			<th align='left' >Leads:</th>
			<td id='leads'>$clicks</td>
		</tr>
		<tr>
			<th align='left' >C P C:</th>
			<td id='cpc'>$clicks</td>
		</tr>
		<tr>
			<th align='left' >C P L:</th>
			<td id='cpl'>$clicks</td>
		</tr>
	</Table>
</div>