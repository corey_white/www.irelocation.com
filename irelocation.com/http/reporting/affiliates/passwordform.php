<?
	session_start();
	if ($_SESSION['username'] == "")
	{		
		header("Location: login.php");
		exit();
	}
	include "skin_top.php";
	
	
	?>
		<style>
		td
		{
			font-family:Verdana;
			font-size:12px;
			color:#000000;
		}
		</style>
		<script language="javascript">
			function validate()
			{
				var form = document.forms['passwords'];
				if(form.newpass.value == form.newpass2.value)
				{
					if (form.current.value.length < 6 || form.newpass.value.length < 6)					
					{	alert("Your passwords are two short. (6 char min)");	}
					else
					{	form.submit(); }
				}
				else
				{
					alert("Your passwords don't Match!");
				}
			}
		</script>
		<form name='passwords' action="changepassword.php" method="post">
		<noscript>This form will not work if you do not have javascript turned on.</noscript>
		<table>
			<tr><td colspan="2">&nbsp;</td></tr>
			<? if (isset($_REQUEST['error'])) { ?>
			<tr><td colspan="2">
				<font color="#FF0000">Please try again.</font>
			</td></tr>
			<? } ?>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td colspan="2">Use this form to change your password.</td></tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td>Current Password:</td><td><input type="password" name="current"/></td></tr>
			<tr><td>New Password:</td><td><input type="password" name="newpass"/></td></tr>
			<tr><td>Type it Again:</td><td><input type="password" name="newpass2"/></td></tr>
			<tr><td colspan="2" align="right">
				<input type="button" onClick="javascript:validate();" value="Update"/>
			</td></tr>
		</table>
		</form>
	<?
	
	
	include "skin_bottom.php";
?>			