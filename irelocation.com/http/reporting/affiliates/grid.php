<?
	session_start();
	include "skin_top.php";
	
	$timeframe = $_REQUEST['timeframe'];
	
	if (strlen($timeframe) < 6)
	{
		echo "Invalid TimeFrame";
		exit();
	}
	
	$full_time = $timeframe."01010101" ;
	if ($timeframe=="")$timeframe=date("Ym");
	
	$lead_type = $_REQUEST['lead_type'];
	
	$types = loadLeadTypes($timeframe);
	if (substr_count(implode(" ",$types),$lead_type) == 0)
	{
		echo "Invalid Lead Type.";
		exit();	
	}
	
	
	
	switch($lead_type)
	{
		case "security":
			$tables[] = "irelocation.leads_security";
			break;
		case "moving":
			$tables[] = "movingdirectory.quotes";
			$tables[] = "movingdirectory.quotes_local";
			break;
		case "auto":
			$tables[] = "marble.auto_quotes";
			break;
	}
	
	function loadLeadTypes($timeframe)
	{
		$sql = "select lead_type from movingdirectory.leadbased_affiliates as lba join ".
				" movingdirectory.lba_info as i on i.source = lba.source where ".
				" i.source = '".$_SESSION['username']."' and lba.month = $timeframe";
		echo "<!-- $sql -->";
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[] = $rs->myarray['lead_type'];
		$rs->close();
		return $data;
	}
	
	function loadLeads($table,$timeframe, &$data)
	{
		$sql = " select count(*) 'count', substr(received,7,2) 'day' ".
				" from $table where substr(received,1,6) = '$timeframe' ".
				" and source like 'aff_".$_SESSION['username']."%' ".
				" group by substr(received,7,2) ";
		echo "\n<!--- $sql --->\n";
		$rs = new mysql_recordset($sql);
		$data = array();
		while($rs->fetch_array())
			$data[$rs->myarray['day']]['count'] += $rs->myarray['count'];
		$rs->close();
	}
	
	$counts = array();
	foreach($tables as $table)
	{				
		loadLeads($table,$timeframe,$counts[$table]);
	}	
	
	?>
	<form action='grid2.php' method="get">
	<? printPeriodDropDown("timeframe",$timeframe); ?>
	<br/>
	<?
		if (count($types) > 1) 
		{ 
			echo "<select name='lead_type'>";
			foreach($types as $type)
				echo "<option value='$type'>$type</option>";
			echo "</select><br/>";	
		}	
		else
			echo "<input type='hidden' name='lead_type' value='$lead_type' />";
	?>
	
	<input type="submit" value="Change month">
	<br/>
	</form>
	<?
	
	echo "<table border='1'>";
	//START OF HEADER ROW
	echo "<tr class='header' >".
			"<th align='left' class='white' >Type</th>";
	
	$days_in_months = array(31,28,31,30,31,30,31,31,30,31,30,31);
	$year = substr($timeframe,0,4);
	$month = substr($timeframe,-2);	
	$days = $days_in_months[intVal($month)-1];
	if ($year % 4 == 0 && $month == "02")
		$days += 1;
		
	for($i = 1; $i <= $days; $i++)
		echo "<th class='white'>$i</th>";
					
	echo "<th class='white'>Total</th></tr>";
		
	$font_counter = 1;
	
	$last_company = 0;
	$total = 0;	
	$day_count = 1;
	
	
	foreach($tables as $table)
	{
		$table_count = $counts[$table];	
		$totalkeys = array_keys($table_count);
		
		if (count($totalkeys) == 0)
			continue;
		
		if ($table == "movingdirectory.quotes")
			$label = "Moving Quotes";
		else if ($table == "movingdirectory.quotes_local")
			$label = "Local Moving Quotes";
		else if ($table == "marble.auto_quotes")
			$label = "Auto Quotes";
		else if ($table == "irelocation.leads_security")
			$label = "Security Quotes";
				
		echo "<tr class='off'>";
		echo "<th align='left' style='color:#000000' >".$label."</th>";		
		
		
		
		
		echo "\n<!--- ".print_r($table_count,true)." --->\n";
		$total = 0;			
		foreach($totalkeys as $day)
		{
			
			
			$count = $table_count[$day]['count'];
			echo "<!-- $day: $count -->\n";
			$day =  intval($day);
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
		
			if (intval($day) == $day_count)//
			{
				echo "<th class='off'>";
				$raw = "category.php?lead_type=$lead_type&timeframe=$timeframe";
				if ($day < 10)
					$link = $raw."0".$day;
				else
					$link = $raw.$day;
				$link = "<a style='color:blue;' href='$link'>$count</a>";
				echo $link;
				echo "</th>";
			}
			$total += $count;		
			$day_count++;
		}
		
		if ($day_count <= $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
		}
		
		echo "<th class='off' align='right' >";
		//$raw = "category.php?lead_type=$lead_type&timeframe=$timeframe";
		$link = "<a style='color:blue;' href='$raw'>$total</a>";
		echo $link;
		echo "</th></tr>";		
	}
?>