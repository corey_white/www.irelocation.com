<?
	function printSelected($value, $selected)
	{
		if ($value == $selected || ($value == date("Ym") && $selected == "")) return " selected ";
	}
	
	function twodecimals($float)
	{
		return number_format($float,2,".",",");
	}
	
	function getDays($timeframe)
	{
		if ($timeframe == date("Ym"))
			$days = date('t');	
		else
		{
			$year = substr($timeframe,0,4);
			$month = substr($timeframe,4,2);
			$days = date("t",mktime(12,30,30,$month,$year));			
		}
		
		return $days;
	}
	
	function showGraphAndCounts($graph)
	{
	?>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
		<?	$graph->showGraph(); ?>
		</td></tr>
		<Tr><td>&nbsp;</td></tr>
		<tr><td>
			<table border='0' width="100%" cellspacing=0 cellpadding=2>
				<tr>
				<td width='40%'>
			<b><font face="Verdana" size="2">Total Leads: 
				<?= $graph->getTotalLeads() ?>
			</font></b> 				
		</td>
		<td rowspan="3" width="20%">&nbsp;</td>
		<td rowspan="3" valign="top">
			<b><font face="Verdana" size="2">
			<?
				list($revenue,$cpl,$cost) = $graph->getData();
			?>
			Total Cost: $<?= $cost ?><br/>
			Total Revenue: $<?= $revenue ?><br/>
			Cost Per Lead: $<?= $cpl ?>
			</font></td>
		</tr>
		<tr><td width='40%'>
			<b>
				<font face="Verdana" size="2">
				Total Leads today: <?= $graph->getTotalLeadsToday() ?>
				</font>
			</b>			
		</td></tr>
		<?
	}
	
	function printGrid($timeframe,$category,$sql,$append_link,$showgoal="x")
	{
		$font_counter = 1;
		$last_company = "";
		$day_count = 0;
		$total = 0;
		
		$format = $category."format.php";
		$viewlink = $category."?ts=".$timeframe.$append_link;
		$days = getDays($timeframe);
		$rs = new mysql_recordset($sql);
		
		while($rs->fetch_array())
		{							
			extract($rs->myarray);
			
			if ($font_counter % 2 == 0)//even
				$style = "on";
			else
				$style = "off";
			
			if ($last_company != $comp_name && $last_company != "")//new company
			{
				if ($day_count < $days)//not a full month of data yet.
				{
					echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
				}
				echo "<td  align='right'>$total</td></tr>";
				$last_company = "";
			}
			
			if ($last_company == "")
			{		
				echo "<tr class='$style'><td class='$style' >$font_counter</td>";
				$link = "<a href='$format?lead_id=".$lead_id."' class='black'>$comp_name</a>";
				//$link = $comp_name;
				echo "<th align='left'>";
				if (!$active)
					echo "<font color='red'>INACTIVE </font>";
				echo "$link</th>";	
				if ($showgoal || $showgoal == "x") echo "<th class='$style'>$goal</th>";
				$last_company = $comp_name;		
				$total = "";	
				$day_count = 0;
				$font_counter ++;
			}
	
			$day_count++;
			
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
			
			if (strlen($_SESSION['lead_id']) > 0)
				$link = "<A class='black' href='".$viewlink."$day'>$count</a>";
			else
				$link = "<A class='black' href='".$viewlink."$day&lead_id=".$lead_id."'>$count</a>";
			
			if (intval($day) == $day_count)//
				echo "<th>$link</th>";
			$total += $count;
		}
		
		if ($day_count < $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count)."'>&nbsp;</td>";
		}
		
		if (strlen($_SESSION['lead_id']) > 0)
			$link = "<A class='black' href='".$viewlink."'>$total</a>";
		else
			$link = "<A class='black' href='".$viewlink."&lead_id=".$lead_id."'>$total</a>";
		
		echo "<td align='right'>$link</td></tr>";	
	}
	
	function printGridHeader($timeframe,$total_sql,$showgoal="x")
	{
		echo "<table border='1'>";
		echo "<tr class='header' ><td> &nbsp; </td>".
				"<th align='left' class='white' >Company</th>";
		if ($showgoal)
			echo "<th class='white'>Goal</th>";
		
		$days = getDays($timeframe);
		
		for($i = 1; $i <= $days; $i++)
		{
			echo "<th class='white'>$i</th>";
		}
				
		echo "<th class='white'>Total</th></tr>";
		
		
		echo "<tr class='off'><td class='$style' > - </td>";
		echo "<th align='left' color='#000000'>Site Total</th>";	
		if ($showgoal)
			echo "<th class='off'> - - </th>";
			
			
		$day_count = 1;
		$total_rs = new mysql_recordset($total_sql);
		
		while($total_rs->fetch_array())
		{
			extract($total_rs->myarray);		
			
			if ($day > $day_count)
			{
				echo "<td colspan='".($day-$day_count)."'>&nbsp;</td>";
				$day_count = intval($day);
			}
	
			if (intval($day) == $day_count)//
				echo "<th class='off'>$count</th>";
			$total += $count;		
			$day_count++;
		}
		
		if ($day_count <= $days)//not a full month of data yet.
		{
			echo "<td colspan='".($days-$day_count+1)."'>&nbsp;</td>";
		}
		echo "<th class='off' align='right' >$total</th></tr>";	
		
		
	}
	
	
	function getAffiliates($category,$ts="*")
	{
		if ($ts=="*") $ts = date("Ym");
		$select = "select comp_name,source from leadbased_affiliates ".
				" where month ='$ts' and lead_type = '$category' ";
				
		$data = array();
		$rs = new mysql_recordset($select);
		
		while($rs->fetch_array())
			$data[] = $rs->myarray;
			
		return $data;
	}
	
	function getAffiliateCostByDay($category,$ts="*",$aff="*")
	{
		if ($ts=="*") $ts = date("Ym");	
		if ($aff == "*")
			$aff = "";
		else
			$aff = " and lba.source = '$aff' ";
		
		if ($ts == date("Ym"))
			$today = " and received not like '".date("Ymd")."%' ";
		else
			$today = " ";
		
		$campaign = "";
		
		if ($category == "moving")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 2 ";
		}
		else if ($category == "vanlines")
		{
			$tables = array("movingdirectory.quotes_local");
			$category = "moving";
		}
		else if ($category == "international")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 3 ";
			$category = "moving";
		}	
		else if ($category == "auto-tas")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'tas' ";
			$category = "auto";			
		}
		else if ($category == "auto-atus")
		{
			$tables = array("movingdirectory.quotes");
			$category = "auto";
		}		
		else if ($category == "auto")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'auto' ";
			$category = "auto";
		}			
		else if ($category == "security")
			$tables = array("irelocation.leads_security");

		$data = array();	
		
		foreach($tables as $table)
		{
			$select = "select 
					substring(received,7,2) 'day',
					count(*)*lba.ppl 'cost'
				from 
					movingdirectory.leadbased_affiliates as lba 
					join $table as  q 
					on q.source like concat('%',lba.source,'%') 
				where 
					month = '$ts' 
					and q.received like '$ts%'
					$campaign
					and lead_type = '$category'
					$aff 
					$today 
				group by  
					substring(received,7,2);";
			echo "\n<!--- $table: $select --->\n";
			$rs = new mysql_recordset($select);
			while($rs->fetch_array())
			{
				$data[$rs->myarray['day']] += $rs->myarray['cost'];
			}
		}
		
		return $data;
	}
	
	function getAffiliateCost($category,$ts="*",$aff="*")
	{
		if ($ts=="*") $ts = date("Ym");	
		if ($aff == "*")
			$aff = "";
		else
			$aff = " and lba.source = '$aff' ";
		
		$campaign = " ";
		
		if ($ts == date("Ym"))
			$today = " and received not like '".date("Ymd")."%' ";
			
		if ($category == "moving")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 2 ";
		}
		else if ($category == "vanlines")
		{			
			$tables = array("movingdirectory.quotes_local");	
			$category = "moving";
		}
		else if ($category == "international")
		{
			$tables = array("movingdirectory.quotes");
			$campaign = " and cat_id = 3 ";
			$category = "moving";
		}	
		else if ($category == "auto-tas")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'tas' ";
			$category = "auto";
			
		}
		else if ($category == "auto-atus")
		{
			$tables = array("movingdirectory.quotes");
			$category = "auto";
		}		
		else if ($category == "auto")
		{
			$tables = array("marble.auto_quotes");
			$campaign = " and q.campaign = 'auto' ";
			$category = "auto";
		}			
		else if ($category == "security")
			$tables = array("irelocation.leads_security");

		$data = "";		

		foreach($tables as $table)
		{				
			
			if ($table == "movingdirectory.quotes_local")
				$cost = "lba.localppl";
			else			
			
				$cost = "lba.ppl";
			
			$select = "select comp_name,count(*)*$cost 'total' from ".
					" movingdirectory.leadbased_affiliates as lba join $table ".
					" as  q on q.source like concat('%',lba.source,'%') where month = '".
					substr($ts,0,6)."' and q.received like '$ts%' $campaign ".
					" and lead_type = '$category' $aff $today group by ".
					" lba.comp_name with rollup";
			echo "\n<!--- Affiliates: $select --->\n";		
			$rs = new mysql_recordset($select);
			
				
			while($rs->fetch_array())
				$temp = $rs->myarray['total'];//will keep last entry.
						
			$data += $temp;
			$rs->close();
		}
		return $data;
	}
	/*
	function getAffiliateData($category,$ts="*",$aff="*")
	{
		if ($ts=="*") $ts = date("Ym");
		if ($aff == "*")
			$aff = "";
		else
			$aff = " and lba.source = '$aff' ";
		
		if ($ts == date("Ym"))
			$today = " and received not like '".date("Ymd")."%' ";
			
		if ($category == "moving")
			$tables = array("movingdirectory.quotes","movingdirectory.quotes_local");
		else if ($category == "auto")
			$tables = array("marble.auto_quotes");
		else if ($category == "security")
			$tables = array("irelocation.leads_security");

		$data = "";		

		foreach($tables as $table)
		{	
			$select = "select lba.comp_name,count(*) 'leads' ,count(*)*lba.ppl 'total' from ".
					" leadbased_affiliates as lba join movingdirectory.quotes as q on q.source ".
					" like concat('%',lba.source,'%') where month = '$ts' and q.received like '$ts%' ".
					" and lead_type = '$category' $aff group by lba.comp_name ";
					
			$data = array();
			
			while($rs->fetch_array())
				$data[] = $rs->myarray['total'];
				
		}
		return $data;
	}
	*/
	

	function printPeriodDropDown($fieldName,$selected,$goBack=3,$goForward=1)
	{	
		$months = array("","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
		$month = date("m");
		$year = date("Y");	
		$startAt = date("Ym",time()-$goBack*24*3600*30);
		$endAt = date("Ym",time()+$goForward*24*3600*30);
		
		echo "<select name='$fieldName' >\n";
		for ($i = -$goBack; $i < ($goForward)+1; $i++)
		{
			echo "<!--- ";
			echo "i: ".$i."\n";			
			
			$time = strtotime($i. " months");
			
			echo "time: ".$time."\n";	
			$ts = date("Ym",$time);
			echo "ts: ".$ts."\n";
			$m = date("m",$time);
			echo "m: ".$m."\n";
			echo "--->";
			echo "<option value='$ts' ".printSelected($ts, $selected).">".strtoupper(date("M",$time))." - ".substr($ts,0,4)."</option>\n";
		}	
		echo "</select>\n";	
		echo "</select>\n";		
	}
	
	function get($name)
	{
		if (substr_count($name,",") == 0)
		{
			$sql = "select value from movingdirectory.irelo_variables where name = '$name' limit 1";
			echo "<!--- ".$sql." --->";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			return $rs->myarray['value'];
		}
		else
		{
			$names = "'".str_replace(",","','",$name)."'";
			$sql = "select name,value from movingdirectory.irelo_variables where name in ($names)";
			echo "<!--- ".$sql." --->";
			$rs = new mysql_recordset($sql);
			$values = array();
			while($rs->fetch_array())
				$values[$rs->myarray['name']] = $rs->myarray['value'];
			return $values;
		}
	}	

	function set($name,$value,$old=true)	
	{
		if ($old)
		{
			$sql = "update movingdirectory.irelo_variables set value = '$value' where name = '$name' ";
			$rs = new mysql_recordset($sql);
		}
		else
		{
			$sql = "insert into movingdirectory.irelo_variables set value = '$value', name = '$name'; ";
			$rs = new mysql_recordset($sql);
		}
	}
	
	function getSortLink($current,$field_name,$label)
	{
		$file = split("/",$_SERVER['SCRIPT_FILENAME']);
		$file = array_pop($file);
	
		if ($current != $field_name)
			return "<a href='$file?order=$field_name' title='Click to Sort Accending'>".ucwords($label)."</a>";
		else
			return "<a href='$file' title='Click to Clear Sorting'>".ucwords($label)."</a>";
	}

	function colorCode($current,$expected,$deviation)
	{
		$low = ((100-$deviation)/100) * $expected;
		$high = ((100+$deviation)/100) * $expected;
		$output = number_format(($current-$expected),0,".",",");
		if ($current < $low || $current > $high)
			return "<font color='#FF0000'>".$output."</font>";
		else
			return "<font color='#006600'>".$output."</font>";
	}

?>