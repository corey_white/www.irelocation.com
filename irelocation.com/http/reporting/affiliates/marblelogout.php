<?
	session_start();
	
	unset($_SESSION['id']);
	unset($_SESSION['time']);
	unset($_SESSION['comp_name']);
	unset($_SESSION['username']);
	
	header("Location: login.php");
	exit();
?>