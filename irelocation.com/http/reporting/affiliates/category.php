<?
	session_start();
	define(AUTO,"auto");
	define(MOVING,"moving");
	define(SECURITY,"security");
	
	if ($_SESSION['username'] == "")
	{		
		header("Location: login.php");
		exit();
	}
	include "skin_top.php";
	
	function orderby($in)
	{
		switch($in)
		{
			case 1: return "quote_id";
			case 2: return "name";
			case 3: return "email";
			case 4: 
				if ($_REQUEST['lead_type'] != "security")
					return "origin_state";
				else
					return "state_code";
			case 5:
				if ($_REQUEST['lead_type'] == "security")
					return "phone1";
				else if ($_REQUEST['lead_type'] == "auto")
					return "phone";
				else
					return "phone_home";
			default:	
				 return "email";
		}	
	}
	
	$orderby = orderby($_REQUEST['orderby']);
	$timeframe = $_REQUEST['timeframe'];
	$lead_type = $_REQUEST['lead_type'];
	$this_page = "category.php?timeframe=$timeframe&lead_type=$lead_type";
	
	function formatPhone($p)
	{
		$a = substr($p,0,3);
		$pr = substr($p,3,3);
		$sf = substr($p,6);
		return " ($a) $pr - $sf ";
	}
	
	function getLeads($ts,$tag,$type,$orderby="email")
	{
		$leads = array();
		
		if ($type == AUTO)
		{	
			$location = "concat(origin_state,' to ',destination_state) as location";
			$sql = "select quote_id,concat(fname,' ',lname) as  'name', ".
					" email,$location,phone from  marble.auto_quotes ".
					" where received like '$ts%' and source like '%$tag%' order by $orderby ";
			
			//echo "<!-- $sql -->\n";
			
			$rs = new mysql_recordset($sql);
			while($rs->fetch_array())
				$leads[] = $rs->myarray;
			$rs->close();
			
			if ($orderby == "phone")
				$orderby = "phone_home";
				 
			$sql = "select quote_id,name,email, $location,phone_home ".
					"  from movingdirectory.quotes where received like ".
					" '$ts%' and source like '%$tag%' and cat_id = '1' order by $orderby";
					
			//echo "<!-- $sql -->\n";
			$rs = new mysql_recordset($sql);
			while($rs->fetch_array())
				$leads[] = $rs->myarray;							
			$rs->close();				
		}
		else if ($type == SECURITY)
		{
			$location = "concat(city,', ',state_code) as location ";
			$sql = "select quote_id,concat(name1,' ',name2) as 'name', email, ".
					"$location, phone1 'phone' from irelocation.leads_security where ".
					"received like '$ts%' and source like '%$tag%' order by $orderby";
			$rs = new mysql_recordset($sql);		
			while($rs->fetch_array())
				$leads[] = $rs->myarray;							
			$rs->close();					
		}
		else if ($type == MOVING)
		{
			$local = 0;
			$count = 0;
			$location = "concat(origin_state,' to ',destination_state) as location";
			
			$sql = "select quote_id,name,email, $location,phone_home as 'phone' from ".
					"movingdirectory.quotes_local where received like ".
					"'$ts%' and source like '%$tag%'  order by $orderby";
			//echo "<!--- local-distance sql: ".$sql." --->";
			$rs = new mysql_recordset($sql);		
			while($rs->fetch_array())
				$leads[] = $rs->myarray;
			echo "<!--- ".count($leads)." local moving leads. -->";								
			$rs->close();	

			$sql = "select quote_id,name,email, $location, phone_home as 'phone' from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%$tag%' and cat_id in (2,3) ".
					" order by $orderby ";
			//echo "<!--- long-distance sql: ".$sql." --->";	
			$rs = new mysql_recordset($sql);		
			while($rs->fetch_array())
				$leads[] = $rs->myarray;					
			echo "<!--- ".count($leads)." total moving leads. -->";
			$rs->close();				
		}
		return $leads;
	}
	
	$leads = getLeads($timeframe,$_SESSION['username'],$lead_type,$orderby);
	?>
	<style>
		td
		{
			font-family:Verdana;
			font-size:12px;
			color:#000000;
		}
		a.sorter
		{
			font-family:Verdana;
			color:#000066;
			font-size:13px;
			font-weight:bold;
		}
	</style>
	<table>
		<tr>
			<th>#</th>
			<th><a class="sorter" href='<?= $this_page."&orderby=1" ?>'>Quote ID</a></th>
			<th><a class="sorter" href='<?= $this_page."&orderby=2" ?>'>Lead Name</a></th>
			<th><a class="sorter" href='<?= $this_page."&orderby=3" ?>'>Email</a></th>
			<th><a class="sorter" href='<?= $this_page."&orderby=5" ?>'>Phone #</a> &nbsp;</th>
			<th><a class="sorter" href='<?= $this_page."&orderby=4" ?>'>Location</a></th>
		</tr>
	<?
		$counter = 1;
		foreach($leads as $lead)
		{
			extract($lead);
			echo "<tr>";
				echo "<td>".$counter++.")</td>";
				echo "<td>$quote_id</td>";
				echo "<td>".ucwords($name)."</td>";
				echo "<td>$email</td>";
				echo "<td>".formatPhone($phone)." &nbsp; </td>";
				echo "<td>$location</td>";
			echo "</tr>";
		}
	?>
		<tr><td colspan='6'>&nbsp;</td></tr>
		<tr><td align='right' colspan='6'><?= count($leads) ?> leads</td></tr>
		<tr><td colspan='6'>&nbsp;</td></tr>
		</table>
<? include "skin_bottom.php"; ?>			
