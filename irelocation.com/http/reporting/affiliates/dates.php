<?
	function printSelected($value, $selected)
	{
		if ($value == $selected || ($value == date("Ym") && $selected == "")) return " selected ";
	}
	
	function printPeriodDropDown($fieldName,$selected,$goBack=3,$goForward=1)
	{	
		$months = array("","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
		$month = date("m");
		$year = date("Y");	
		$startAt = date("Ym",time()-$goBack*24*3600*30);
		$endAt = date("Ym",time()+$goForward*24*3600*30);
		
		echo "<select name='$fieldName' >\n";
		$days = date('t');
		for ($i = -$goBack; $i < ($goForward)+1; $i++)
		{
			echo "<!--- ";
			echo "i: ".$i."\n";			
			
			$time = strtotime($i. " months");
			
			echo "time: ".$time."\n";	
			$ts = date("Ym",$time);
			echo "ts: ".$ts."\n";
			$m = date("m",$time);
			echo "m: ".$m."\n";
			echo "--->";
			echo "<option value='$ts' ".printSelected($ts, $selected).">".strtoupper(date("M",$time))." - ".substr($ts,0,4)."</option>\n";
		}	
		echo "</select>\n";		
	}
	
	printPeriodDropDown("test","200902");
?>