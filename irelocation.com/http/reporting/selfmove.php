<?php
	include "cron_functions.php";
	include "cpc.php";
	include_once "../inc_mysql.php";
	define(VANLINES_PRICE, 6.00);//they buy local leads.
	define(PACKPRO,9000);
	define(DOOR2DOOR,9001);//zip code list.
	define(MOVEX,9002);
	define(GOSMARTMOVE,9003);//zip code list.
	define(ABFUPACK,9004);
	define(MOVINGSTAFFERS,1981);//not a lead id... //stopped as of 9/12/06
	define(MOVINGLABORSERVICES,1982);//not a lead id, Moving Labor Services //started as of 9/12/06
	define(SELFMOVINGQUTOES,"sitebase");
	define(WEHAUL,9005);
	define(TOPRELO,9006);
	
	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];	
	
	$campaign = "all";
	if (strlen($_POST["campaign"]) > 0 && $_POST["campaign"] != "all")
	{
		$campaign = $_POST["campaign"];
	}
	echo "<!-- $campaign --->";
	echo "<!-- $month/$year --->";
?>
	<div align="center">
		<?php 

			$leads = getTotals($month,$year);
				
			if ($month == date("m"))
				$leads -= getNumbers("leadstoday");
				
			if ($leads  != 0)
				$revenue = getTotalLeadRevenue($month,$year);
			else
				$revenue = 0;
				
			if ($leads  != 0)
				$cost = getNumbers("monthlycost",$month,$year);
			else
				$cost =  0;
				
			echo "<!--- \nLead Count: $leads \n";
			echo "Revenue: $revenue \n"; 
			echo "Cost: $cost; \n";
			
			
			if ($leads != 0)
			{
				echo "C P L: ".($cost/$leads)."\n";
				echo "R P L: ".($revenue/$leads)." \n--->";
			}
			else
				echo "--->";
			
			
			?>
		<table width="85%">
			<tr>
				<td class="mainmovingtopic" align="left ">
					<? echo "Valid: ".getNumbers("valid",$month,$year); ?>
				</td>
				<td width="20">&nbsp;</td>
				<td class="mainmovingtopic" width="100">Monhtly Cost:</td>
				<td class="mainmovingtext" width="40">$<? echo number_format($cost,"2",".",",") ; ?></td>
				<td rowspan="5" width="50%">&nbsp; </td>
			</tr>
			<tr>
				<td class="mainmovingtopic" align="left">
					<? echo "Failed: ".getNumbers("failed",$month,$year); ?>
				</td>
				<td width="20">&nbsp;</td>
				<td class="mainmovingtopic" >Revenue:</td>
				<td class="mainmovingtext">$<? echo number_format($revenue,"2",".",",") ; ?><font color="#FF0000">*</font></td>
			</tr>
			<tr>
				<td class="mainmovingtopic" align="left">
					<? echo "Limbo: ".getNumbers("not-proccessed",$month,$year); ?>
				</td>
				<td width="20">&nbsp;</td>
				<td width="20" class="mainmovingtopic"> Avg Cost Per Lead: </td>
				<td class="mainmovingtext">
					$<? if ($leads > 0) echo number_format(($cost/$leads),"2",".",",") ; else echo "0.00"; ?>
				</td>
				
				
			</tr>			
			<tr>
				<td height="64" align="left" class="mainmovingtopic">					
					<?php echo "the last time a lead was scrubbed was:<Br/> ".formatLeadTime(getLastScrubbedLead())."."; ?>
				</td>
				<td>&nbsp;</td>
				<td class="mainmovingtopic" >Avg Revenue Per Lead:</td>
				<td class="mainmovingtext">$<? if ($leads > 0) echo number_format(($revenue/$leads),"2",".",",") ; else echo "0.00";  ?><font color="#FF0000">*</font></td>
			</tr>
			<tr><td colspan="2">&nbsp;</td><td colspan="3" class="mainmovingtext"><font color="#FF0000">Note:</font> Numbers do not reflect today<Br/><font color="#FF0000">*</font>Local Leads Included</td>
			<tr>
				<td class="mainmovingtopic" align="left" colspan="5">										
					<form action="<?php echo $_SERVER['PHP_SELF']."?page_id=8"; ?>" method="post">
						Campaign: 
						<select name="campaign">
							<option value="all">All</option>					
							<?php
								$data = getSources($month,$year);
								$sources = array_keys($data);
								foreach($sources as $source)
								{	
									echo "<option";
									if ($campaign == $source) echo " selected";
									echo " value='$source'> ".ucwords($source)." (".$data[$source].")</option>\n";							
								}
							?>					
						</select>
						<Select name="month">				
							<?php							
								$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");							
								for ($i = 1; $i < 13; $i++)
								{
									echo "<option value='";
									if ($i < 10)
										echo "0$i";
									else
										echo "$i";
									echo "' ";
									if (intval($month) == ($i)) 
										echo "selected ";
									echo ">".$months[$i-1]."</option>\n";
								}
							?>
						</Select>
						<select name="year">
							<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
						</select>
						<input type="submit" name="Filter" value="Go"/>
						<?php 
							$date = gmdate("Ymd", mktime(date("H"), date("i"), date("s"), date("m") , date("d") - 1 , date("Y")));
							$sql = "select * From movingdirectory.costperclick where site = 'smq' and ts = '$date'";
							echo "<!--- $sql --->";
							$rs = new mysql_recordset($sql);
							if (!$rs->fetch_array())
							{
								echo "<a href='#' onClick='openPopup();'>Get Yesterday's Data</a>";
							}
						 ?>
					</form>
				</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="mainmovingtopic" colspan="5" align="left">
					Leads
				</td>
			</tr>
			<tr>
				<td align="left" colspan="5">
					<?php displayLeads($campaign,$month,$year);//defaults to the current month, coming from TopSecurityCompanis.com, that are Residential leads from any campaign. ?>	
				</td>
			</tr>
		</table>
	</div>
	<Br/>
<?	


	function getTotalLeadRevenue($month,$year)
	{
		
		 $array = array(DOOR2DOOR,MOVEX,ABFUPACK,WEHAUL,TOPRELO);
		 foreach($array as $comp)
		 {
		 	$r = getRevenue($comp, $month,$year);
			//echo $r;
		 	$revenue += $r;
		 }
		 
 		 $sql_local = "select count(*) as count from movingdirectory.quotes_local  where lead_ids != '' and received like '$year$month%' and source like 'smq%' and success ";
		 $rs = new mysql_recordset($sql_local);
		 if ($rs->fetch_array())
		 {
			$count = $rs->myarray["count"];		 	
		 }
		 $revenue += $count * VANLINES_PRICE;
		 
		 return $revenue;
	}
	
	function getLeadRevenue($lead_id, $month,$year)
	{				
		$sql = "select count(*) count, quote_type from irelocation.leads_selfmove where lead_ids != '' and received like '$year$month%' and lead_ids like '%$lead_id%' ";			
		if ($month == date("m"))
			$sql .= " and received not like '$year$month".date("d")."%'";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		$results += $rs->myarray["count"] * getPricePerLead($lead_id);
				
		return $results;		
	}
	
	function getNumbers($what="valid",$month="*",$year="*")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		switch($what)
		{
			case "valid":
				//-- valid Leads
				$sql = "select count(*) count from irelocation.leads_selfmove where lead_ids != '' and received like '$year$month%';";
				break;
			case "failed":
				//-- Failed Scrubbing
				$sql = "select count(*) count  from irelocation.leads_selfmove where received = '' and scrub_id != 0;";
				break;
			case "not-proccessed":
				//-- Yet to be Processed
				$sql = "select count(*) count from irelocation.leads_selfmove where scrub_is_valid = 0 and scrub_id = 0 and received like '$year$month%';";	
				break;
			case "monthlycost":
				//-- cost for advertising for the month.
				$sql = "select sum(cost) count from movingdirectory.costperclick where ts like '$year$month%' and site like 'smq%'";	
				break;
			case "leadstoday":
				//-- cost for advertising for the month.
				$sql = "select count(*) count from irelocation.leads_selfmove where lead_ids != '' and received like '$year$month".date("d")."%';";	
				//echo $sql;
				break;
		}
		//echo $sql;
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		return $rs->myarray["count"];
	}
	
	function getSummaryInfo($month="*", $year="*")
	{
		$sql = "select count(*) ";
	
	}
	
	function printSummaryTable($month="*", $year="*")
	{		
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
	}
	
	function formatLeadTime($ts)
	{
		$year = substr($ts,0,4);
		$month = substr($ts,4,2);
		$day = substr($ts,6,2);
		$hour = substr($ts,8,2);
		if (intval($hour) > 12)
		{
			$pm = 1;
			$hour -= 12;		
		}
		else if (intval($hour) == 0)
			$hour = 12;
		$min = substr($ts,10,2);
		$sec = substr($ts,12,2);
		if (strlen($ts) > 8)
		{
			$time = intval($hour).":".$min.":".$sec;
			if ($pm == 1)
				$time .= " pm ";
			else
				$time .= " am ";				
			return "$time$month-$day-$year";
		}
		else if (strlen($ts) == 6)//YYYYMM
		{
			return "$month-$year";
		}
		else
			return "$month-$day-$year";

	}
	
	function getLastScrubbedLead()
	{
		$sql = "select max(received) received from irelocation.leads_selfmove where scrub_is_valid = 1";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		return $rs->myarray["received"];
	}
	
	function displayLeads($campaign="all", $month="*", $year="*",  $site_code="smq")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		echo "<!--- in displayLeads($campaign,$month,$year) --->";
		//print_month_header($month,$year);
		if ($campaign == "google" || $campaign == "overture" || $campaign == "msn")
			print_cpc_month_header("smq",$campaign,$month,$year);
		else
			print_month_header($month,$year);

		print_it(getSelfMovingString(SELFMOVINGQUTOES,$campaign, $month, $year), $month, $year,SELFMOVINGQUTOES, $campaign, $site_code);
		
		print_it(getSelfMovingString(MOVEX,$campaign, $month, $year), $month, $year,MOVEX, $campaign, $site_code);
		print_it(getSelfMovingString(ABFUPACK,$campaign, $month, $year), $month, $year,ABFUPACK, $campaign, $site_code);
		
		print_it(getSelfMovingString(DOOR2DOOR,$campaign, $month, $year), $month, $year,DOOR2DOOR, $campaign, $site_code);
		if ($month == "08" && $year == "2006")
		{
			print_it(getSelfMovingString(PACKPRO,$campaign, $month, $year), $month, $year,PACKPRO, $campaign, $site_code);
			print_it(getSelfMovingString(GOSMARTMOVE,$campaign, $month, $year), $month, $year,GOSMARTMOVE, $campaign, $site_code);
		}
		else
		{
			print_it(getSelfMovingString(WEHAUL,$campaign, $month, $year), $month, $year,WEHAUL, $campaign, $site_code);
			print_it(getSelfMovingString(TOPRELO,$campaign, $month, $year), $month, $year,TOPRELO, $campaign, $site_code);
		}
		print_it(getMovingStaffersString($campaign, $month, $year),$month,$year,MOVINGSTAFFERS, $campaign, $site_code);
		print_it(getMovingLaborServicesString($campaign, $month, $year),$month,$year,MOVINGLABORSERVICES, $campaign, $site_code);
		echo "</table>";
	}
	
	function getCompanyName($lead_id)
	{
		switch($lead_id)
		{
			case PACKPRO: return "Pack Pro";
			case MOVEX: return "Movex";
			case ABFUPACK:return "ABF UPack";
			case DOOR2DOOR:return "Door 2 Door";
			case GOSMARTMOVE:return "Go Smart Move";
			case MOVINGSTAFFERS: return "Moving Staffers";
			case MOVINGLABORSERVICES: return "Moving Labor Services";
			case SELFMOVINGQUTOES: return "SelfMovingQutoes.com";
			case TOPRELO: return "Top Relocation";
			case WEHAUL: return "WeHaul Moving";
		}	
	}
	
	function getSelfMovingString($lead_id, $campaign="all",  $month="*", $year="*", $site_code="smq")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "select substring(received,7,2) 'day', count(received) 'count' from irelocation.leads_selfmove ".
		"where received like '$year$month%' ".
		getFilters($lead_id,$site_code, $campaign).
		" group by substring(received,1,8) WITH ROLLUP;";
		
		return $sql;
	}
	
	function getMovingStaffersString($campaign="all", $month="*", $year="*", $site_code="smq")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "select substring(received,7,2) 'day', count(received) 'count' from irelocation.leads_selfmove ".
		"where received like '$year$month%' and moving_staffer = '1' and scrub_is_valid = '1' ".	
		getFilters($lead_id,$site_code, $campaign).
		" group by substring(received,1,8) WITH ROLLUP;";
		
		return $sql;
	}
	
	function getMovingLaborServicesString($campaign="all", $month="*", $year="*", $site_code="smq")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "select substring(received,7,2) 'day', count(received) 'count' from irelocation.leads_selfmove ".
		"where received like '$year$month%' and moving_staffer > 1 and scrub_is_valid = '1' ".	
		getFilters($lead_id,$site_code, $campaign).
		" group by substring(received,1,8) WITH ROLLUP;";
		
		return $sql;
	}

	function getFilters($lead_id, $site_code="smq", $campaign="all")
	{	
		if(is_numeric($lead_id))
			$base = " and lead_ids like '%$lead_id%' and scrub_is_valid = '1' ";
		else
			$base = " and scrub_is_valid = '1' ";
			
		if ($campaign == "all")
		{
			return $base;
		}
		else if ($campaign == "org")//organic
		{						
			return $base." and (source = '$site_code' or source like '".$site_code."_search%') ";
		}
		else
		{
			$result = " and source like '$site_code";
			$result .= "_";
			$result .= "$campaign%' ";
			
			return $base.$result;
		}	
	}	

	function print_it($sql, $month, $year, $lead_id, $campaign, $site_code)
	{		
		$comp_name = getCompanyName($lead_id);
		
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
		
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		while ($rs->fetch_array())
		{
			
			$link_base = "reporting.php?page_id=9&site=smq";
			if ($campaign != "all")
				$link_base .= "&campaign=$campaign";
			if (is_numeric($lead_id))
				$link_base .= "&lead_id=$lead_id";
			
			$counter++;
			
			$day = $rs->myarray["day"];
								
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];
				echo "<td align='right'>";
				echo "<a href='".$link_base."&received=$year$month$day'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];				
				$revenue = getRevenue($lead_id,$month, $year,$total);
				if ($revenue == "--")
					$revenue = "Free Trial";
				else if ($revenue != "Unknown")
					$revenue = "Revenue: \$$revenue";
				else
					$revenue = "25$ per sale";
					
				if ($day == $ndays)
				{
					echo "<td align='right' title='$revenue'>";
					echo "$total</td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."' >&nbsp;</td>";
					echo "<td align='right' title='$revenue'> ";
					echo "$total</td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right' title='$revenue'>";
					echo "$total</td>";
				}
				else
				{
					echo "<td align='right' title='$revenue'> ";
					echo "$total</td>";
				}
			}			
			
			
		}				
		echo "</tr>\n";
	}
	
	function getRevenue($lead_id, $month, $year,$total="*")
	{
		if ($total == "*")
			$total = getTotals($month,$year,$lead_id);
		
		switch($lead_id)
		{
			case PACKPRO: return "--";
			case MOVEX: return (3*$total);
			case ABFUPACK:
				$total -= getABFLosses($month, $year);
				return (4.4*$total);
			case DOOR2DOOR:return (3.5*$total);
			case WEHAUL:return (4.0*$total);
			case TOPRELO:return (4.0*$total);
			case GOSMARTMOVE:return "--";
			case MOVINGSTAFFERS: return "Unknown";
		}		
	}
	
	function getTotals($month,$year,$lead_id="*")
	{		
		$sql = "select count(received) 'count' from irelocation.leads_selfmove ".
		"where received like '$year$month%' and scrub_is_valid = '1'";

		if ($lead_id != "*")
			$sql .= " and lead_ids like '%$lead_id%' ";
			
		$sql .= getFilters($lead_id,"smq","all").";";
		
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();

		$count = $rs->myarray["count"];
		return $count;
		
	}
	
	function getABFLosses($month, $year)
	{
		//get all failures
		$sql = "select count(*) count from leads_verified as v join leads_selfmove as l on l.quote_id = v.quote_id where v.success = 0 and v.site = 'smq' and v.lead_id = '9004' and l.received like '$year$month%' ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();

		$count = $rs->myarray["count"];
		
		if ($month == "08" && $year == "2006")
			$count += 64;
		return $count;
	}
	
	function getSources($month,$year)
	{
		$sql = "select distinct source, count(source) count from irelocation.leads_selfmove where scrub_id != 0 and lead_ids != '' and received like '$year$month%' and source like 'smq_%' group by source;";
		$rs = new mysql_recordset($sql);
		$results = array();
		while ($rs->fetch_array())
		{
			$s = substr($rs->myarray["source"],4);
			
			$results[$s] = $rs->myarray["count"];
		}
		return $results;	
	}

?>
<script>
	function openPopup()
	{
		window.open( "http://irelocation.com/adwords/test.php?site=smq&reload=1", 'update' ,'width=250,height=250');
	}	
</script>