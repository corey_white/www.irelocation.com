<?
	if (strlen($_REQUEST['lead_id']) == 0)
	{
		header("Location: marblereport.php");
		exit();		
	}
	$lead_id = $_REQUEST['lead_id'];

	include "../marble/inc_mysql.php";
	
	$sql = "select
	r.lead_email,
	ca.active,
	co.phone_num,
	co.comp_name,
	lf.format_type,
	ca.*,
	if(lf.lead_body<0,(select lead_body from lead_format where lead_id = lf.lead_body and site_id = 'auto'),lf.lead_body) as 'lead_body',
	if(lf.lead_body<0,'no','yes') 'custom'
from 
	campaign as ca
	join lead_format as lf
	on ca.lead_id = lf.lead_id and lf.site_id = 'auto'
	join
	companies as co
	on ca.comp_id = co.comp_id
	join
	rules as r
	on r.lead_id = ca.lead_id and r.site_id = 'auto'
where 
	ca.lead_id = '$lead_id'
	and ca.site_id = 'auto';";
	
	$rs = new marble($sql);
	if ($rs->fetch_array())
	{		
		extract($rs->myarray);		
		$phone_num = ereg_replace("[^0-9]","",$phone_num);
		
	}
	else
	{
		header("Location: marblereport.php");
		exit();		
	}	
?>	
<style>
	.on
	{
		background-color:#E8E8FF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.off
	{
		background-color:#FFFFFF;
		font-family:Verdana;		
		font-size: 10px;
	}
	.header 
	{
		font-family: Verdana, Arial, Helvetica, sans-serif;
		font-weight:bold;
		background-color:navy;
		font-size: 12px;
	}
	a 
	{
		text-decoration:none;		
		color:#FFFFFF;
	}
	a.black
	{
		text-decoration:none;		
		color: #0000FF;
		font-size:10px;
		
	}

</style>

<body style="margin:0px 0 0 0px;" topmargin="0" leftmargin="0" marginheight="20" marginwidth="0" bgcolor="#FFFFFF">
<table border="0" width="100%" cellspacing=0 cellpadding=0>
		<tr bgcolor='navy'>
			<td align=left>
				<a href='marblereport.php'>
				<font face='verdana' size=2 color='white'><b>&nbsp;.:
				Top Auto Leads</b></font></a>
			</td>
		</tr>
		<tr>
			<td>
				<table border="0" width="100%" cellspacing=0 cellpadding=10>
					<tr>
						<td  bgcolor='navy' width="120">&nbsp;
							
						</td>
						<td width="20">&nbsp;</td>
						<td>
						<table border="0" width="100%" cellspacing=0 cellpadding=2>
							<Tr><td>&nbsp;</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Company: 
								<?= $rs->myarray['comp_name'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Phone: 
								<?	
									if (strlen($phone_num) > 10)
									{	
										$ext =  "Ext. ".substr($phone_num,10);
										$phone_num = substr($phone_num,1,10);
									}
									if (strlen($phone_num) == 10)
									{
										$a = substr($phone_num,0,3);
										$b = substr($phone_num,3,3);
										$c = substr($phone_num,6);
										echo "($a) $b - $c $ext ";
									}
									
								?>								
								</font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Transportation Method: 
								<?= $rs->myarray['format_type'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Email Address: 
								<?= $rs->myarray['lead_email'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Custom Format: 
								<?= $rs->myarray['custom'] ?></font></b>
							</td>
							</tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Active: 
								<? 
									if ($active == 1)
									{
										echo "<font color='green'>Yes</font>";
										echo " &nbsp; <a class='black' href='marble_edit.php?function=deactivate&lead_id=$lead_id'> (deactivate) </a>";
									}
									else
									{
										echo "<font color='red'>No</font>";
										echo " &nbsp;  <a class='black' href='marble_edit.php?function=activate&lead_id=$lead_id'> (re-activate) </a)";
									}
								?></font></b>
							</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
							  <td>
								<b><font face="Verdana" size="2">Lead Format: 
								</font></b>
								<br/>
								<font face="Courier New, Courier, mono">
								<? 									
									$lead_body = str_replace(array("<",">","\n"),array("&lt;","&gt;","<br/>"),$lead_body);
									$colored = ereg_replace(
										"\|\|([a-zA-Z0-9_]*)\|\|",
										"<font color='blue'>||\\1||</font>",
										$lead_body);
									echo $colored;
								?>
								</font>
								</td>
							</tr>
					
	
	
<?	
	

?>
</td></tr></table></td></tr></table></td></tr></table>