<?
	define(AUTO,3);
	define(MOVING,2);
	define(SECURITY,4);

	$aff = array();


	$aff[] = array("A1 AUTO","A1AUTO",AUTO);
	$aff[] = array("MoveDirect","movedirect",AUTO);
	$aff[] = array("MoveDirect","movedirect",MOVING);
	$aff[] = array("Eleads","eleads",MOVING);
	$aff[] = array("Hauling Depot","hauling",MOVING);
	$aff[] = array("Auto-Extra.com","autoextra",AUTO);
	$aff[] = array("WiseClick","wc",SECURITY);
	$aff[] = array("Directory M","dm",SECURITY);
	$aff[] = array("Directory M","dm",MOVING);

	include "../../inc_mysql.php";

	foreach($aff as $comp)
	{
		echo "Company: ".$comp[0]."<br/>";
		echo "Leads: ".getLeadCount($ts,$comp[1],$comp[2])."<br/>";
		echo "Category: ".getCategory($comp[2])."<br/><br/>";
	}

	function getLeadCount($ts,$tag,$type)
	{
		if ($type == AUTO)
		{	
			$sql = "select count(*) count from marble.auto_quotes where ".
					"received like '$ts%' and source like '%tag%' ";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count  = $rs->myarray['count'];
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%tag%' and cat_id = '1'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count += $rs->myarray['count'];
			return "$count Auto Leads.";
		}
		else if ($type == SECURITY)
		{
			$sql = "select count(*) count from irelocation.leads_security where ".
					"received like '$ts%' and source like '%tag%'";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count = $rs->myarray['count'];
			return "$count Security Leads.";
		}
		else if ($type == MOVING)
		{
			$sql = "select count(*) count from movingdirectory.quotes_local where ".
					"received like '$ts%' and source like '%tag%' ";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count  = $rs->myarray['count'];
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%tag%' and cat_id = '2'".
					" and origin_state != destination_state";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$count += $rs->myarray['count'];
			
			$sql = "select count(*) count from movingdirectory.quotes where ".
					"received like '$ts%' and source like '%tag%' and cat_id = '2'".
					" and origin_state = destination_state";
			$rs = new mysql_recordset($sql);
			$rs->fetch_array();
			$local = $rs->myarray['count'];
			
			return "$count Long Distance Moving Leads, $local Local Leads.";
		}
	}

	function getCategory($cat_id)
	{
		if (AUTO == $cat_id) return "Auto Leads";
		if (MOVING == $cat_id) return "Moving Leads";
		if (SECURITY == $cat_id) return "Security Leads";
		return "Unknown($cat_id) Leads";
	}
?>

	