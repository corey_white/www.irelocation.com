<?php

	if ($_POST["distinct"] || $_REQUEST["distinct"])
		$distinct = "distinct(email)";
	else
		$distinct = "*";	
	define (DISTINCT, $distinct);
	
	
	include 'company_array.php';
	
	//filter out some bad leads.	
	define (VALID," and lead_ids != '' and lead_ids != '||||||'");
	
	/*
		Build queries for intrastate leads.
		
		if leads_id == -1, or left off, it defaults to Sirva
		otherwises uses the lead id for another comp.
	*/
	function IntrastateLeads($ts, $source, $leads_id=-1, $campaign="*")
	{	
		$table = "movingdirectory.quotes";
		$distinct = DISTINCT;
		$cat = "cat_id = 2";
		
		if ($leads_id == -1)
		{
			$table = "movingdirectory.movesirva";
			$distinct = "*";			
		}
			
		$sql = "Select count(".$distinct.") as count from $table where $cat and received like '".$ts."%' and ".getSource($source,$campaign)." ";
		
		if ($leads_id == -1)
			$sql .= " and (move_id!=-1 or move_id!=0)";
		else if ($leads_id != 0)
			$sql .= " and ready_to_send = 2 and lead_ids like '%$leads_id%'".VALID;
		else
		//if ($source == "me")
			$sql .= VALID;
		return $sql;
		
	}

	//forces two digits for month and day.
	function forceTwo($number)
	{
		if(strlen($number) == 2)
			return $number;
		if ($number > 9 && $number < 99)
			return $number;
		else
			return "0".$number;		
	}

	//international leads get treated a little diff.
	function InternationalLeads($ts, $source, $leads_id=-1, $campaign="*")
	{
		$table = "movingdirectory.quotes";
		$lead = "lead_ids like '%$leads_id%' and ready_to_send = 2";
		$distinct = DISTINCT;
		
		if ($leads_id == -1)
		{
			$table = "movingdirectory.movesirva";
			$lead = "(move_id!=-1 or move_id!=0)";
			$distinct = "*";
		}
		
		$date = $year.forceTwo($month);
		if ($day != -1)
			$date .= forceTwo($day)."%'";;
			
		$sql = "Select count(".$distinct.") as count from $table where cat_id=3 and received like '".$ts."%' and ".getSource($source,$campaign)." and $lead";
		
		return $sql;		
	}	
	
	function getSource($source, $campaign)
	{
		if($campaign == "*")
			return "source like '$source%'";
		else if ($campaign == "organic")
		{
			if ($source == "cs")//gotta treat these guys differant
				return "(source = 'cs' OR source = 'csq')"; 
			else
				return "source = '$source' ";
		}
		else
		{
			if ($source == "cs")//gotta treat these guys differant
				return "(source like 'cs_$campaign%' OR source like 'csq_$campaign%')"; 
			else
				return "source like '".$source."_".$campaign."%'";			
		}
	}
	
	function getSource_IRELO($source, $campaign)
	{
		return " referer like '$source%' ";	
	}	
	
	/*
		returns 'YYYYMMDD%' or 'YYYYMM%'	
	*/
	function formatDate($year,$month, $day)
	{
		$date = "$year".forceTwo($month);
		if ($day != -1)//specific day.
			$date .= forceTwo($day);

		return $date;
	}
	
	//Dependable AutoShipping Leads.
	function Dependable_AutoShipping($ts, $source,$campaign="*")
	{
		$datevalue = substr($ts,0,4)."-".substr($ts,4,2)."-".substr($ts,6,2)."%";
		return "Select count(".DISTINCT.") as count from irelocation.leads_auto where received like '$datevalue' and ".getSource_IRELO($source,$campaign)." and sent_to='|33|'";
	}
	
	//autoshipping leads
	//ts - day
	//source - source site
	//lead_id - 
	//	-1 for whole site,
	//	number for one company.
	function AutoShipping($ts, $source="mmc", $lead_id=-1,$campaign="*")
	{
		$base = "Select count(".DISTINCT.") as count from movingdirectory.quotes where received like '".$ts."%' and ready_to_send = 2 and cat_id = '1' and ";
		if ($source == "cs")
		{
			$base .= getSource("cs",$campaign);
			if ($lead_id == -1)//whole site.
				return $base." and lead_ids != '' ";//filter quotes that arnt sent out...
			else
				return $base." and lead_ids like '%$lead_id%'";				
		}		
		else 
		{			
			$base .= " ".getSource($source,$campaign)." and lead_ids ";
			if ($lead_id == -1)
				return $base."!= ''";
			else
				return $base."like '%$lead_id%'";
		}
	}
	
	function RealEstate($ts, $source,$campaign="*")
	{
		return "Select count(".DISTINCT.") as count from irelocation.leads_realtor where received like '".$ts."%' and sent_to='|$source|'";
	}	
			
	function displayTitle($source)
	{		
		if ($source == "tm")
			return "Topmoving.com";
		if ($source == "pm")
			return "Promoving.com";
		if ($source == "me")
			return "Movingease.com";
		if ($source == "mmc")
			return "Movemycar.com";
		if ($source == "atus")
			return "AutoTransports.us";
		if ($source == "cs")
			return "CarShipping.com";				
		if ($source == "pm_silver")
			return "Silver Carrot";
		if ($source == "pm_boxdev")
			return "Boxes Delivered";
	}
	
	//default timestamp is now.
	
	
	function getQueryTS($type, $source, $leads_id=-1, $ts, $campaign)
	{
		switch($type)
		{
			case DAS:
				return Dependable_AutoShipping($ts, $source, $campaign);
				break;
			case AUTO_SHIPPING:
				return AutoShipping($ts,$source, $leads_id, $campaign);
				break;
			case INTERNATIONAL:
				return InternationalLeads($ts, $source, $leads_id, $campaign);
				break;
			case INTERSTATE:
				return IntrastateLeads($ts, $source, $leads_id, $campaign);
				break;
			case REAL_ESTATE:
				return RealEstate($ts, $source, $campaign);
		}
	}
	
	
	function getQuery($type, $source, $leads_id=-1, $year=0, $month=0, $day=0, $campaign="*")
	{
		if ($year == 0)//default, get current
			$year = date('Y');
		if ($month == 0)
			$month = date('M');//M
		if ($day == 0)
			$day = date('D');//D
		
		$ts = formatDate($year,$month,$day);		
		
		return getQueryTS($type, $source, $leads_id, $ts,$campaign);
	} 
	
	//find out what company this is by the lead_id
	function getCompanyByLead($lead)
	{	
		return "select comp_name, c.comp_id as comp_id from movingdirectory.directleads as d join movingdirectory.company as c on c.comp_id = d.comp_id where d.lead_id = '$lead'";
	}
		
	$types = array(INTERSTATE,INTERNATIONAL,AS_IRELO,AUTO_SHIPPING,REAL_ESTATE);
	$sources = array(  array("TopMoving","tm"), 
						array("ProMoving","pm"), 
						array("MovingEase","me"), 
						array("MoveMyCar","mmc"), 
						array("AutoTransports","atus") 
					);		
	

	/*--------------------------------------------------*/
	//		Start Trends.php Functions					//
	/*--------------------------------------------------*/
	
	function getTrendsQuery($set, $distinct, $today="")
	{	
		//if distinct and if it is a moving site.
		$month = date("m");//get current month.
		$distinct = "*";
		
		if ($set[0] == "tsc")
		{
			$sql = "select count(*) as count from irelocation.leads_security where scrub_is_valid = 1 and received like '2006$month$today%'";
		}
		else if ($set[0] == "smq")
		{
			$sql = "select count(*) as count from irelocation.leads_selfmove where scrub_is_valid = 1 and received like '2006$month$today%'";
		}
		else
		{
			if ((strpos($set[1],"moving.com") !== FALSE))			
				$valid = VALID;								
			else
				$valid = "";				
			
			$sql = "select count($distinct) as count from movingdirectory.quotes where received like '2006$month$today%' and (".$set[2].") ".$valid;
		}
		return $sql;	
	}
	
	function getMonthlyGoal($set)
	{
		if ($set[0] != "smq" && $set[0] != "tsc")
		{
			$company_count = countRecentCompanies($set[0]);
			if ($company_count == 0)
			{
				updateCompaniesOnSite($set[0]);
				$company_count = countRecentCompanies($set[0]);
			}
				
			//$company_count = $set[0];//used in the eval statement sometimes :)
			eval($set[3]);//sets the $goal variable.
			if ($goal < 1000) $goal = 1000;
		}
		else
		{
			eval($set[3]);	
		}
		return $goal;
	}
	
	//use eval() with the 4th element to get the number of leads needed per month
	
	$siteInfo = array(
			array('tm',"Topmoving.com","source like 'tm%'", '$goal = 2500;'),
			array('pm',"Promoving.com/<Br/>Movingease.com","source like 'pm%' or source like 'me%'", ' $goal = 2500;'),			
			array('mmc',"Movemycar.com","source like 'mmc%'", ' $goal = 2333;'),
			array('atus',"Auto-transports.us","source like 'atus%'", ' $goal = $company_count * 100;'),
			/*array('smq',"SelfMovingQuotes.com","source like 'smq%'", ' $goal = 1000;'),*/
			array('tsc',"TopSecurityCompanies.com","source like 'tsc%'", ' $goal = 1500;')
	);	
			
	/*--------------------------------------------------*/
	//		End Trends.php Functions					//
	/*--------------------------------------------------*/

	/*--------------------------------------------------*/
	//		Start Really SICK Queries	:)				//
	/*--------------------------------------------------*/
	
	$source = "source like 'cs%'";
	$lead_ids = "lead_ids like '%1433%'";
	$year = "2006";
	$month = "06";
	
	$query = "select substring(received,7,2) 'day', count(received) 'count' from movingdirectory.quotes where received like '$year$month%' and $lead_ids and $source group by substring(received,1,8) WITH ROLLUP;";
	
	
	/*--------------------------------------------------*/
	//		End Really SICK Queries	:)				//
	/*--------------------------------------------------*/

?>