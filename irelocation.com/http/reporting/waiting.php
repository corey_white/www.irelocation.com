<?	
/* 
************************FILE INFORMATION**********************
* File Name:  waiting.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	2/28/08 - old code in the callAtMidnight function deleted records from the movingdirectory.lead_destinations table, which is obsolete.
					New code called the (newly modified) campaignmapping.php script which does what we want
**********************************************************************
*/
	header('Cache-Control: no-cache');
	header('Pragma: no-cache');
	
	include_once "../inc_mysql.php";
	define(DEBUG,false);
	define(DATABASE,"movingdirectory");
	define(SMALLEST_INCREMENT,100);
	function verify($site)
	{		
		$sql = "select distinct(lead_ids) lead_ids from ".DATABASE.".lead_destinations where site_code = '$site'";
		$rs = new mysql_recordset($sql);
		$counter = 0;
		while($rs->fetch_array())
		{
			$ids = $rs->myarray["lead_ids"];
			//echo $ids."<br/>";
			$list = split(",",$ids);
			$list = array_unique($list);
			if (count($list) != 6)
			{
				echo "Invalid lead list: $ids!! ".print_r($list,true)."<br/>";			
				$counter++;
			}
		}
		echo $counter." sets of invalid lists.";
	}
	
	function showLeadTable($site)
	{	
		$max_send = 6;
		$sql = "select lead_id, (monthly_goal+temp_goal) 'monthly_goal', leads_per_day, ((monthly_goal+temp_goal)/500)".
				" 'weight' from ".DATABASE.".campaign where site_id = '$site' and active order by monthly_goal desc;";
		$total_leads = 0;
		
		$rs = new mysql_recordset($sql);
		while($rs->fetch_array())
		{
			$array = array();
			$array[] = $rs->myarray["lead_id"];		
			$array[] = $rs->myarray["monthly_goal"];	
			$total_leads +=  ($rs->myarray["monthly_goal"]/$max_send);
			$array[] = ceil($rs->myarray["leads_per_day"]);		
			$array[] = intval($rs->myarray["weight"]);		
			$companies[] = $array;			
		}
		
		if (($total_leads%500) != 0)
			$kilo_leads = intval(($total_leads/500)+2);
		else
			$kilo_leads = intval($total_leads/500);
			
		if (DEBUG) echo $kilo_leads."<br>\n";
		
		$lead_groups = array();
		for($i = 0; ($i < $kilo_leads); $i++)
			$lead_groups[$i] = array();
		$count = 0;
		
		foreach($companies as $c)
		{				
			$weight = $c[3];
			while($weight > 0)
			{
				for($index = 0; ($index < count($lead_groups) && ($weight > 0)); $index++)
				{
					if (count($lead_groups[$index]) < $max_send && !array_search($c[0],$lead_groups[$index]))
					{
						$lead_groups[$index][] = $c[0];
						$weight--;
					}
					else
						continue;
				}	
				if (DEBUG) echo $weight."<br>\n";
			}
			$count++;
		}
		
		
		foreach($lead_groups as $row)
		{
			if (count($row) >0)
			{
				echo "<table width='500'><tr><td>";
				echo "<Table border=1 >\n";
				echo "<tr height='50'>\n";
				//sort($row);
				
				foreach($row as $entry)
				{
					echo "<td width='30' align='center'>$entry</td>\n";			
				}		
				echo "</tr>\n";
				echo "</table></td><td>(500 leads)</td></tr></table>\n";
				echo "<Br/>";
			}
		}		
	}
	
	function grabNextList($site_code)//returns a comma(,) delimeted list of lead_ids.
	{	
		$sql = "select * from ".DATABASE.".lead_destinations where site_code = ".
				"'$site_code' order by use_count asc,rand()  asc limit 1"; 
				
		echo $sql."<br/>\n";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$dest_id = $rs->myarray["dest_id"];
		$sql = "update ".DATABASE.".lead_destinations set ".
									"use_count = use_count+1 where dest_id = $dest_id";
		echo $sql."<br/>\n";
		$rs2 = new mysql_recordset($sql);
		//$lead_ids = $rs->myarray["lead_ids"];
		return $rs->myarray["lead_ids"];
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	
	function countToday($site_code)
	{
		$thisMonth = date("Ym");
		$today = date("Ymd");
		$sql = "select 
	c.lead_id,
	count(q.quote_id) 'today'
from
	".DATABASE.".campaign as c
	join 
	".DATABASE.".quotes as q
	on locate(c.lead_id,q.lead_ids) > 0
where 
	c.site_id = '$site_code'
	AND q.received like '$today%'
	and c.month = '$thisMonth'
	AND q.source like '$site_code%'
	and c.active
group by
	c.lead_id";
		echo "<pre>$sql</pre><Br/>\n";
		$numbers = array();
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
			$numbers[$rs->myarray["lead_id"]] = $rs->myarray["today"];
		$rs->close();
		return $numbers;
	}
	function countActual($site_code)
	{
		$this_month = date("Ym");
		$days_left = getNDays() - date("d") + 1;
		echo "Days Left: $days_left<br/>\n";
				
		$sql = "select 	
					lead_id,
					(monthly_goal+temp_goal) 'needed',
					ceiling((monthly_goal+temp_goal)/ ".getNDays()." ) 'per_day'
				from
					".DATABASE.".campaign 
				where 
					site_id = '$site_code'
					AND month = '$this_month'
					and active
				group by
					lead_id
				";
		$rs = new mysql_recordset($sql);		
		$numbers = array();
		while($rs->fetch_array())
		{
			$valid = true;
			$a = array();
			extract($rs->myarray);
			$a["lead_id"] = $lead_id;
			if ($needed > 0)
				$a["needed"] = $needed;
			else
				$a["needed"] = 0;
			if ($per_day > 0)
				$a["per_day"] = $per_day;
			else
				$a["per_day"] = 0;
			$numbers[] = $a;
		}		

		$sql = "select 
					c.lead_id,
					(c.monthly_goal+c.temp_goal) - count(q.quote_id) 'needed',
					ceiling(((c.monthly_goal+c.temp_goal) - count(q.quote_id))/ $days_left ) 'per_day'
				from
					".DATABASE.".campaign as c
					join 
					".DATABASE.".quotes as q
					on locate(c.lead_id,q.lead_ids) > 0
				where 
					c.site_id = '$site_code'
					AND q.received like '$this_month%'
					AND q.source like '$site_code%'
					and c.month = '$this_month'
					and c.active
				group by
					c.lead_id";
					
		echo "<pre>$sql</pre><Br/>\n";
		$rs = new mysql_recordset($sql);		
		while($rs->fetch_array())
		{
			$valid = true;
			$a = array();
			extract($rs->myarray);
			$a["lead_id"] = $lead_id;
			if ($needed > 0)
				$a["needed"] = $needed;
			else
				$a["needed"] = 0;
			if ($per_day > 0)
				$a["per_day"] = $per_day;
			else
				$a["per_day"] = 0;
			$numbers[] = $a;
		}
		return $numbers;
	}

	function callAtMidnight($site_code)
	{
		$current_numbers = countActual($site_code);
		foreach($current_numbers as $num)
		{
			extract($num);
			$sql = "update ".DATABASE.".campaign set leads_per_day = '$per_day', needed = $needed ".
					"where lead_id = $lead_id and site_id = '$site_code'";
			echo "<pre>$sql</pre><Br/>\n";
			$rs = new mysql_recordset($sql);
		}	
		
		//-- instead of the below, we need to run campaignmapping.php here

		//-- use curl to run the campaignmapping.php
		$runurl = "http://www.irelocation.com/marble/campaignmapping.php";
		$ch = curl_init($runurl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;

/* OLD CODE
		$rs = new mysql_recordset("delete from ".DATABASE.".lead_destinations where site_code = '$site_code'");
		buildNewDestinations($site_code);
*/
	}
	
	function buildNewDestinations($site_code)
	{
		$sql = "select lead_id,(monthly_goal+temp_goal) 'monthly_goal',needed,leads_per_day from ".DATABASE.".campaign ".
				"where site_id = '$site_code' and active and month = '".date("Ym")."' order by sort_order asc, needed desc ";
		echo "<pre>$sql</pre><Br/>\n";
		$leads_today = countToday($site_code);
		$rs = new mysql_recordset($sql);
		$monthly_need = 0;
		
		if ($site_code == "atus")
			$max_send = 10;
		else		
			$max_send = 6;
		
		while($rs->fetch_array())
		{
			$array = array();
			$array[] = $rs->myarray["lead_id"];		
			$array[] = $rs->myarray["needed"];	//chagned, change back to monthly_goal if not worken
			$monthly_need += ($rs->myarray["needed"]/$max_send);
			$array[] = $rs->myarray["leads_per_day"] - $leads_today[$rs->myarray["lead_id"]];		
			$companies[] = $array;
		}
		
		$days_in_month = getNDays();
		$days_left_in_month = $days_in_month - date("d");
		
		//$monthly_need = 0;
		//foreach($companies as $c)
			//$monthly_need  += ($c[1] / $max_send);
		
		if ($days_left_in_month == 0)
			$days_left_in_month = 1;
		
		$monthly_need = ceil($monthly_need);
		if (DEBUG) echo "We need $monthly_need leads by the end of this month.<br/>\n";
		$leads_per_day = ceil($monthly_need/$days_left_in_month) + 10;
		if (DEBUG) echo "We need $leads_per_day leads per day.<br/>\n";
		

		
		$lead_lists = array();
		
		for($i = 0; $i < $leads_per_day; $i++)
			$lead_lists[$i] =  array();	
		
		$full_lists = array();
		$company_list = array();
		foreach($companies as $c)
		{			
			echo $c[0]."<br>\n";
			$company_list[] = $c[0];
		
			$shift = intval(rand(0,1));
	
			if ($c[2] > 0)
			{
				$ind_leads_per_day = $c[2];
			}
			else
			{
				$ind_leads_per_day = 10;
				mail("david@irelocation.com","waiting.php line 303","Company ".$c[0]." has reached its lead count for $site_code ");
				echo "Company ".$c[0]." has reached its lead count for $site_code ";				
				continue;
			}
			
			$frequency = ceil($leads_per_day / $ind_leads_per_day);
			$allotted = 0;
			$ind_index = 0;
			$loop_count = 0;
			$invalid_spots = 0;		
			
			while ($allotted < $ind_leads_per_day && $invalid_spots < count($lead_lists))
			{
				for ($i = ($loop_count + $shift); 
						  ($i < count($lead_lists)) && ($i < $leads_per_day) && ($allotted < $ind_leads_per_day); 
						  $i += $frequency
					)
				{
					if (array_search($c[0],$lead_lists[$i]))//bad
					{
						$invalid_spots++;
						continue;
					}
					else if (count($lead_lists[$i]) == $max_send)//bad
					{
						$full_lists[] = $lead_lists[$i];
						array_splice($lead_lists,$i,1);
						$i--;					
						continue;
					}
					else
					{
						$lead_lists[$i][] = $c[0];
						$allotted++;
					}
				}
				
				if (count($lead_lists) > 40)		
					$loop_count++;
				else
				{
					$loop_count = 0;
					$shift = 0;
					$frequency = 1;
				}
	
			}		
			$count++;
			if (DEBUG) echo $c[0]." Allotted: $allotted Needed: $ind_leads_per_day<br/>\n";
		}
		$exit = false;
		$index = 0;
		
		//pull out full lists.
		while (!$exit && $index < count($lead_lists))
		{	
			if (count($lead_lists[$index]) == $max_send)
			{
				$full_lists[] = $lead_lists[$index];
				array_splice($lead_lists,$index,1);
			}
			else
				$index++;
		}
		//lists that aren't full.
		
		echo "Before Shuffling: ".print_r($company_list,true)."<br/>";
		shuffle($company_list);
		sort($company_list);
		shuffle($company_list);
		rsort($company_list);
		shuffle($company_list);
		echo "After Shuffling: ".print_r($company_list,true)."<br/>";
		for ($i = 0; $i <  count($lead_lists); $i++)
		{
			$needs = $max_send - count($lead_lists[$i]);
			
			foreach($company_list as $c)
			{
				if ($needs == 0)
					break;
				if (substr_count(implode(" ",$lead_lists[$i]),$c) != 0)
				//if (array_search($c,$lead_lists[$i]))
					continue;
				else
				{
					$lead_lists[$i][] = $c;
					$needs--;
				}
			}		
		}
		
		$all = array_merge($full_lists,$lead_lists);
			
		if (DEBUG) echo "<pre>".print_r($all,true)."</pre>";
		
		$actual = array();
		$SQL_BULK = "insert into ".DATABASE.".lead_destinations (site_code,lead_ids) values ";
	
		foreach($all as $lead)
		{
			sort($lead);			
			$lead = array_unique($lead);

			if (count($lead) != $max_send)
				echo "short list! ".print_r($lead,true);

			$lead = fill($lead, $company_list, $max_send);

			if (count($lead) != $max_send)
				echo "STILL SHORT LIST!!! ".print_r($lead,true);
			
			$SQL_BULK .= " ('$site_code','".implode(",",$lead)."'), ";
			
			foreach($lead as $a)
				$actual[$a]++;
		}
		
		$SQL_BULK = substr($SQL_BULK,0,strlen($SQL_BULK)-2).';';
	//	echo $SQL_BULK."<br>\n";
		$now = microtime();
		$rs = new mysql_recordset($SQL_BULK);
		$done = microtime() - $now;
		echo "Insert to $done seconds.";
		if (DEBUG) echo "<pre>".print_r($actual,true)."</pre>";
	}
		
	function fill($list, $companies, $fillto=6)
	{
		sort($list);		
		shuffle($companies);
		
		$list_as_string = implode(",",$list);
		
		if (count($list) != $fillto)
		{
			echo "Company List: ".implode(",",$companies)."<br/>";
			foreach($companies as $c)
			{
				if (substr_count($list_as_string,$c) == 0)
				{
					$list[] = $c;					
					sort($list);
					$list_as_string = implode(",",$list);					
					if (count($list) == $fillto)
					{
						echo "List Extended: $list_as_string<br/>";
						return $list;
					}
				}			
			}
			if (count($list) != $fillto)
			{
				mail("david@irelocation.com","Lead List Fill Error!",
					"List: ".print_r($list,true)."\n\nCompanies: ".print_r($companies,true));			
				echo "List: ".print_r($list,true)."\n\nCompanies: ".print_r($companies,true);
				exit();
			}	
			
		}		
		else
			return $list;
	}
		
	function visualization($site_code)
	{
		echo "Site: $site_code<br/>\n";
		$rs = new mysql_recordset("select co.comp_name, c.lead_id from ".DATABASE.".campaign as ".
								"c join ".DATABASE.".company as co on c.comp_id = co.comp_id where ".
								"site_id = 'mmc' order by needed asc;");
		while ($rs->fetch_array())
		{
			extract($rs->myarray);
			echo $comp_name."<br/>\n";
			$rs2 = new mysql_recordset("select count(*) count from ".DATABASE.".lead_destinations ".
										"where site_code = '$site_code' and lead_ids like '%$lead_id%'");
			if ($rs2->fetch_array())
				echo "Leads Today: ".$rs2->myarray["count"]."<br/>\n";			
		}
	}
		
	$hour = date("H");
	$minutes = date("i");
	if (DEBUG) echo $hour.":".$minutes."<br/>";
	if (isset($_REQUEST['midnight']) && $hour == "01")//01 am.
	{
		echo "Running update";		
		callAtMidnight("atus");		
		mail("david@irelocation.com","How many times is this running?","");
	}
	else if (isset($_REQUEST['table']))
	{
		showLeadTable($_REQUEST['site']);
	}
	else if (isset($_REQUEST['verify']))
	{
		verify($_REQUEST['verify']);
	}
	else if (isset($_REQUEST['force']))
	{
		echo "Running update";		
		callAtMidnight("atus");
	}
	
?>