<?php
		// page 10 is used by both SMQ and TSC, so change the title if needed.
		if ($_REQUEST["page_id"] == 10 && $_REQUEST["site"] == "tsc")
			$title = "Security Leads";
		else
			$title = $titles[$page_id]; 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>iRelocation Reporting - <?php echo $title; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="irelocation.css" rel="stylesheet" type="text/css">
</head>

<body>

<table width="90%" border="1">
	<tr class="reportingtext"> 
		
		<td width="50%"><?php echo $title; ?></td>
		
		<?php
			$count = count($header);
			for ($i = 0; $i < $count; $i++)
				echo "<td align='center' width='8%'><a href='reporting.php?page_id=".$header[$i]."'>".$titles[$header[$i]]."</a></td>";			
		?>
	</tr>
	<tr>
		<td colspan="<?php echo $page_count; ?>">