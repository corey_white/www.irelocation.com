<Style>
	td
	{
		font-family:Arial;
		font-size:12px;		
		text-align:left;
	}
	th
	{
		font-family:Arial;
		font-size:13px;		
		font-weight:bold;
		text-align:left;
	}
	.row
	{
		font-family:Arial;
		font-size:11px;		
		font-weight:bold;
		text-align:left;
	}
</Style>

<?
	define(HEADER,"<tr class='row'><td>Quote ID</td><td>TimeStamp</td><td>Name</td><td>Phone</td><td>Origin</td><td>Destination</td><td>Est. Move Date</td><td>Source</td>");
	include "../inc_mysql.php";

	$movers = array(46,48);
	$months = array(200609,200610,200611,200612);
	
	function getName($id)
	{
		return (($id==46)?"St.Petersburg":"Tampa");
	}

	function formatReceived($r)
	{
		$y = substr($r,0,4);
		$m = substr($r,4,2);
		$d = substr($r,6,2);
		return "$m/$d/$y";
	}
	function formatPhone($r)
	{
		$y = substr($r,0,3);
		$m = substr($r,3,3);
		$d = substr($r,6);
		return "($y) $m - $d";
	}

	echo "<table>";
	foreach($movers as $mover)
	{
		foreach($months as $month)
		{
			$sql = "select 
			received,name,phone_home,origin_city,origin_state,destination_city,destination_state,est_move_date,source,
			quote_id,
			substring(received,5,2) 'rm',
			substring(received,7,2) 'rd',
			if(substring(est_move_date,6,2)<9,12+substring(est_move_date,6,2),substring(est_move_date,6,2)) 'em',
			substring(est_move_date,9,2) 'ed'
			from movingdirectory.quotes 
			where 
			mover_id = '$mover'
			and received like '$month%'";
			$rs = new mysql_recordset($sql);
			$count = 0;		
			echo "<tr><th colspan='8'>".getName($id)." - ".substr($month,4)."/2006"."</th></tr>";
			echo HEADER;
			while($rs->fetch_array())
			{
				extract($rs->myarray);
				if ($em < 9) $em+12;
				$rdays = $rm*30.8+$rd;
				$edays = $em*30.8+$ed;
				$diff = ((int) ($edays - $rdays));
				if (21 <= $diff) 
				{ 
					$count++;
					echo "<Tr><td>$quote_id</td><td>".formatReceived($received)."</td><td>$name</td><td>".formatPhone($phone_home)." &nbsp; </td><td>$origin_city, $origin_state</td><td>$destination_city, $destination_state</td><td>$est_move_date</td><td>$source</td></tr>\n";
				}
			}
			echo "<tr><th colspan='8'>Leads: $count</th></tr><Tr><td colspan='8'>&nbsp;</td></tr>";
		}
	}

?>