<?php
	include "../../inc_mysql.php";

	session_start();
	if ($_SESSION["username"] != "suddath")
	{
		header("Location: /reporting/suddath/login.php?login");
		exit();
	}
	else if ((date("Ymdhms") + (60*20)) < $_SESSION["time"])
	{
		header("Location: /reporting/suddath/login.php?old");
		exit();
	}
	else
	{
		$_SESSION["time"] = date("Ymdhms");//update session.
	}
	
	if (isset($_POST["month"]))
	{
		$month = $_POST["month"];
		$year = $_POST["year"];	
	}
	else
	{
		$year = date("Y");
		$month = 10;
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
	
	function print_it($sql, $month, $year,$title)
	{		
		$rs = new mysql_recordset($sql);
		$ndays = getNDays($month,$year);
		$counter = 0;
				
		$last = -1;
				
		$link_base = "view_leads.php?good";				
			
				
		echo "<tr>\n<td align='left'><Strong>$title</strong></td>";
		
		while ($rs->fetch_array())
		{			
			$counter++;			
			$day = $rs->myarray["day"];								
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				echo "<a href='$link_base&received=$year$month$day'>";
				echo "$count</a></td>";
				//echo "$count</td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				//$total -= $losses["total"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
		echo "<!--- $month/$year --->";
	?>
		<html>
		<head>
		<title>iRelocation Reporting</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link href="irelocation.css" rel="stylesheet" type="text/css">
		</head>
		<body>
		<div align="center">
			<form method="post" action="index.php">
			<table width="80%">
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2">Suddath Leads</td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">
							<select name="month"  class="mainmovingtopic">											
								<option value="10" <? if ($_POST["month"] == "10") echo "selected ";?>>October</option>
								<option value="11" <? if ($_POST["month"] == "11") echo "selected ";?>>November</option>
							</select>						
						</td>
					</tr>										
					<tr>
						<td>&nbsp;</td>
						<td class="mainmovingtopic">						
							<select name="year" class="mainmovingtopic" >
								<option value="2006"<? if ($_POST["year"] == "2006") echo "selected ";?>>2006</option>
								<option value="2007"<? if ($_POST["year"] == "2007") echo "selected ";?>>2007</option>
							</select>						
						</td>
					</tr>										
					<tr><td>&nbsp;</td><td class="mainmovingtopic" colspan="2"><input type="submit" value="Show Leads" class="mainmovingtopic" /></td></tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td width="25">&nbsp;</td>
						<td width="*">
						<?					
							print_month_header($month,$year);						
							//all leads
							//$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count'  from movingdirectory.quotes as s where s.received like '$year$month%' and source like 'tm_wc%' and ((scrub_id != 0 and scrub_is_valid = 1) or scrub_is_valid = 0 and scrub_id = 0) group by substring(s.received,1,8) WITH ROLLUP;;";
							$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count'  from movingdirectory.quotes as s where s.received like '$year$month%' and cat_id = 3 and lead_ids like '%1528%' group by substring(s.received,1,8) WITH ROLLUP;";
							print_it($sql, $month, $year,"International Leads");								
							
							
							
							
							//$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count'  from movingdirectory.quotes as s where s.received like '$year$month%' and source like 'tm_wc%' and scrub_id != 0 and scrub_is_valid = 0 group by substring(s.received,1,8) WITH ROLLUP;;";
							//print_it($sql, $month, $year,"Failed Leads");	
							echo "</table>";					
						?>
						</td>
					</tr>
					<tr><td colspan="2">&nbsp;</td></tr>
					<tr>
						<td>&nbsp;</td><td align="left" ><a href="logout.php" class="mainmovingtext">&lt;Logout&gt;</a></td>						
					</tr>
					<tr class="mainmovingtext">
						<td>&nbsp;</td><td>you will be logged out after 20 minutes of inactivity.</td>
					</tr>
			</table>		
			</form>
		</div>
	<?php echo  " <!--- ".$_SESSION["time"]." --->";?>
	

