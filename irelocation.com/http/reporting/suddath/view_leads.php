<link href="irelocation.css" rel="stylesheet" type="text/css">
<br/>
<?php
	$security = false;
	include "../../inc_mysql.php";
	
	//always put orderby last. much easier to strip off.	
	extract($_REQUEST);
	
	$sql = "select * from movingdirectory.quotes where cat_id = 3 and lead_ids like '%1528%' ";
				
	if (isset($received) && is_numeric($received))
		$sql .= " and received like '$received%' ";

	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);
			
	$ordering = array("received","email","origin_state","destination_state","est_move_date","size_of_move");
	if (isset($orderby))
	{
		if ($orderby != "size_of_move")
		{
			if (array_search($orderby,$ordering))
				$sql .= " order by $orderby";		
		}
		else
		{
			$sql = "select
					  
						quotes.*,	
						
						concat(
						replace(	
						replace(	
						replace(	
						replace(
						replace(
						replace(
						substring(
							comments,
							locate('Furnished Rooms',comments)+17,
							(locate(' bedroom',comments) - locate('Furnished Rooms',comments))-17
							)
							,'five',5)		
							,'four',4)
							,'three',3)
							,'two',2)
							,'one',1)
							,'no',0)
							,' bedroom(s)') as 'shorterbedrooms'
					from
						movingdirectory.quotes
					where
						(source like 'tm_wc%' or source like 'pm_wc') and
						received like '$received%'
					order by shorterbedrooms
					";
		}
	}
	?>
	<br/>
	   <div class="mainmovingtopic">Suddath Leads - International Moving Leads
       </div>
    <br/>
	<?
	/*
	echo "<!--- ".$_SERVER['REMOTE_ADDR']." --->";
	if ($_SERVER['REMOTE_ADDR'] == "67.72.68.198")
		echo formatSQL($sql);
	*/
	function formatSQL($display_sql)
	{	
		$display_sql = str_replace("select","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = ereg_replace("[^\.]\*","&nbsp; &nbsp; *",$display_sql);
		$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
		$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
		$display_sql = ereg_replace("('[a-z0-9%_]*')","<font color=\"red\">\\1</font>",$display_sql);
		$display_sql = ereg_replace("(replace|concat|locate|substring)\(","<font color=\"blue\">\\1</font>(",$display_sql);
		return "<font face='Courier New' size='2px'>".trim($display_sql).";</font><br/><br/>";
	}
	
	if (strpos($_SERVER['QUERY_STRING'],"&orderby=") > 0 )
		echo "<a class='mainmovingtopic' href='".$_SERVER['PHP_SELF']."?$filterlink'>Clear Ordering</a><br/>";
			
	if (!$movecomp)
		$rs = new mysql_recordset($sql);
	else 
		$rs = new movecompanion($sql);
	$rowcount = $rs->rowcount();
	
	if ($rowcount == 0)
	{
		echo "<div class='mainmovingtopic'>Zero leads.</div>";
		exit();
	}
	
?>
<table border="1">
	<tr class="mainmovingtext">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=received"; ?>' class='mainmovingtopic'>Timestamp</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=origin_state"; ?>' class='mainmovingtopic'>Origin</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=destination_state"; ?>' class='mainmovingtopic'>Destination</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=est_move_date"; ?>' class='mainmovingtopic'>Moving Date</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=size_of_move"; ?>' class='mainmovingtopic'>Size of Move</a></th>
	</tr>
<?
		while ($rs->fetch_array())
			buildInternational($rs->myarray);									
		
		function buildInternational($array)
		{
			extract($array);
			$received = getReceived($received);
			$customerInfo = ucwords($name)."<br/>".$email."<Br/>".formatPhone($phone_home);
			$origin = ucwords($origin_city).", ".strtoupper($origin_state)."<Br/> $origin_zip";
			$destination = ucwords($destination_city).", ".ucwords($destination_country);
			$moving = "$est_move_date";
			$comments = substr($comments,strpos($comments,"Furnished Rooms:")+16);
			$comments = substr($comments,0,strpos($comments,"\n"));
			if (strlen(trim($comments)) == 0)
				$comments = "&nbsp;";
			echo "<tr class='mainmovingtext'>";
			echo "<td align='left' >$received </td>";
			echo "<td align='left' >$customerInfo</td>";
			echo "<td align='left' >$origin</td>";
			echo "<td align='left' >$destination</td>";
			echo "<td align='left' >$moving</td>";
			echo "<td align='left' >$comments</td>";
			echo "</tr>\n";
		}
				
		function formatPhone($phone)
		{	
			if (strlen($phone) == 10)
			{
				$p_area=substr($phone,0,3);
				$p_prefix=substr($phone,3,3);
				$p_suffix=substr($phone,6,4);
		
				return "($p_area) $p_prefix - $p_suffix";
			}
			else
				return $phone;
		}
		
		function getReceived($received)
		{
			$r_year=substr($received,0,4);
			$r_month=substr($received,4,2);
			$r_day=substr($received,6,2);
			$r_hour=substr($received,8,2);
			$r_min=substr($received,10,2);
			if (strlen($received) == 14)
				return "$r_month/$r_day/$r_year $r_hour:$r_min";
			else
				return " -- ";
		}	

	?>
</table>
<br/>
<span class="mainmovingtopic"><?php echo $rowcount; ?></span><span class="mainmovingtext"> leads.</span>
<table>
	<tr class="mainmovingtext">
		<td><a href="index.php" class="mainmovingtext">Home</a></td>
		<td>&nbsp;</td>
		<td><a href="logout.php" class="mainmovingtext">Logout</a></td>		
	</tr>
</table>
<br/>