<?php
	include 'lib_functions.php';
	require_once('../inc_mysql.php');
	
	$ts = $_REQUEST["ts"];
	$index = $_REQUEST["index"];
	$distinct = ($_REQUEST["distinct"] == "on");
	$campaign = $_REQUEST["campaign"];
	if (strlen($campaign) == 0)
		$campaign = "*";
	$fields = ", quote_id, received, name, phone_home, origin_city, origin_state, origin_zip,".
		"origin_country, destination_city, destination_state, destination_country, est_move_date,".
		"comments, source, lead_ids ";
	$sites = array(  "re" => "Real Estate",  "das" => "Dependable AutoShipping", "cs" => "CarShipping.com", "tm" => "TopMoving.com", "pm" => "ProMoving.com", "me" => "MovingEase.com", "mmc" => "MoveMyCar.com", "atus" => "AutoTransports.us");

	//why do i need to make this???
	/*
		In: YYYYMMDDHHMMSS
		Out: MM-DD-YYYY
	*/
	function formatTime($ts)
	{
		$year = substr($ts,0,4);
		$month = substr($ts,4,2);
		$day = substr($ts,6,2);
		return "$month-$day-$year";
	}
	
	function get($ts, $set, $distinct, $fields, $campaign)
	{
		$count_query = getQueryTS($set[0], $set[1], $set[2], $ts,$campaign);
		if (strpos($count_query,"sirva") === FALSE)
		{
			if ($distinct == 0)
			{			
				$count_query = substr($count_query,25);
				$count_query = "Select email$fields $count_query order by received";
			}
			else
			{			
				$count_query = substr($count_query,39);
				$count_query = "Select distinct(email) as email$fields $count_query group by email order by received";
			}
		}
		else 
		{
			//sirva...
		}
		return $count_query;
	}

	$set = $codes[$index];
	$source = $set[1];
	
	$query = get($ts,$set,$distinct, $fields,$campaign);
	
	$rs = new mysql_recordset($query);	
	?>
	<Strong>SQL: </Strong> <?php  echo $query; ?><br/>
	<strong>Received: <?php echo formatTime($ts); ?></strong><br/>
	<strong>Source: <?php echo $sites[$source]; ?></strong><br/>
	<br/>
	*Note: If country is not displayed, the lead is local to the US.
	<br/>
	<br/>
	
	<table border=1><tr>		
		<th>Customer</th>	
		<th>Origin</th>
		<th>Destination</th>
		<th>Est. Moving Date</th>
		<th>Comments</th>
		<th>Source</th>
		<th>Leads</th>
	</tr>
	<?php
	while($rs->fetch_array())
	{
		echo "<tr>";
				
		$name = trim($rs->myarray["name"]); 
		$phone_home = trim($rs->myarray["phone_home"]); 
		
		$received = trim($rs->myarray["received"]); 
		
		$origin_city = trim($rs->myarray["origin_city"]); 
		$origin_state = trim($rs->myarray["origin_state"]);
		$origin_zip = trim($rs->myarray["origin_zip"]);
		$origin_country = trim($rs->myarray["origin_country"]);
		
		$destination_city = trim($rs->myarray["destination_city"]);
		$destination_state = trim($rs->myarray["destination_state"]);
		$destination_country = trim($rs->myarray["destination_country"]);
		
		$est_move_date = trim($rs->myarray["est_move_date"]);
		
		$comments = trim($rs->myarray["comments"]);
		
		$source = trim($rs->myarray["source"]);
		
		$lead_ids = trim($rs->myarray["lead_ids"]);		
		echo "<td>$name<Br/>";
		if (strlen($phone_home) == 10)
		{
			echo "(".substr($phone_home,0,3).") ".substr($phone_home,3,3)."-".substr($phone_home,6)."<br/>";
		}
		else
		{
			echo "<!--- id: ".$rs->myarray["quote_id"]."--->";
			if (strlen($phone_home) > 0)
				echo $phone_home."<br/>";	
		}
		echo $rs->myarray["email"];
		echo "</td>";
		echo "<td> $origin_city, $origin_state $origin_zip";
		if ($origin_country != $destination_country )
		{
			echo "<!--- id: ".$rs->myarray["quote_id"]."--->";
			echo "<Br/> $origin_country </td>";
		}
		else
			echo "</td>";
		echo "<td> $destination_city, $destination_state";
		if ($origin_country != $destination_country  )
			echo "<Br/> $destination_country </td>";
		else
			echo "</td>";			
		echo "<td> $est_move_date </td>";
		echo "<td title='$comments'> Comments...</td>";
		echo "<td> $source </td>";
		echo "<td title='".str_replace('|',' ',$lead_ids)."'> Leads.. </td>";		
		echo "</tr>";
	}
	echo "</table>";
//	$count = $rs->myarray["count"];
	
	
	/*
		Select count(*) as count - 25
		Select count(distinct(email)) as count - 40
		
	
	*/
?>
