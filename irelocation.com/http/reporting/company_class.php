<?php


include_once '../inc_mysql.php';

class Company
{
	var $lead_id;
	var $comp_id;
	var $comp_name;
	var $lead_count;
	var $total_leads;
	function Company($lead_id)
	{
		$this->lead_id = $lead_id;
		$this->lead_count = array();
		$this->total_leads = 0;
	}
	
	function getLeads($day)
	{
		return $this->lead_count[$day];
	}
	
	function addLead($day,$amount=1)
	{
		$this->lead_count[$day] += $amount;
		$this->total_leads += $amount;
	}
	
	function getCompanyRow($ndays, $today)
	{
		$row = "<tr><td>$this->comp_name</td>";
		for ($day = 1; $day <= $ndays; $day++)
		{
			$count = $this->lead_count[$day];
			
			if ($day <= $today)			
			{
				if ($count > 0)
					$row .= "<td>$count</td>";
				else
					$row .= "<td>&nbsp;</td>";
			}
			else
			{
				$row .= "<td colspan='".($ndays - $today)."'>&nbsp;</td>";
				$row .= "<td>$this->total_leads</td>";
			}
		}
		$row .= "</tr>";
		return $row;
	}
		
	function researchCompany()
	{
		$query = "select comp_name, c.comp_id from movingdirectory.directleads as d join movingdirectory.company as c on c.comp_id = d.comp_id where d.lead_id = '".$this->lead_id."'";
		$rs = new mysql_recordset($query);
		if ($rs->fetch_array())
		{
			$this->comp_id = $rs->myarray["comp_id"];
			$this->comp_name = $rs->myarray["comp_name"];			
		}
	}
}

?>