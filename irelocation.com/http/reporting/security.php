<?php
	include "cron_functions.php";
	include "cpc.php";
	include_once "../inc_mysql.php";

	define(COMMERCIAL,"com");
	define(RESIDENTIAL,"res");
	define(SITEBASE,"sitebase");
	//Actual lead ids!!! :P
	define(BRINKS,1519);//0
	define(PINNACLE,1523);//6
	define(PROTECTION_ONE,1521);//3
	define(PROTECT_AMERICA,1520);	//2
	define(USALARM,1522);//5
	
	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];	
	
	$campaign = "all";
	if (strlen($_POST["campaign"]) > 0 && $_POST["campaign"] != "all")
	{
		$campaign = $_POST["campaign"];
	}
	
	
	echo "<!-- $campaign --->";
	echo "<!-- $month/$year --->";
	
?>
	<div align="center">
		<table width="85%">
			<tr class="mainmovingtopic">
				<td  align="left" width="25%"  colspan='2'>
					<? echo "Valid: ";
						$valid = getNumbers("valid",$month,$year); echo $valid; 
					?>
				</td>
				
				<td width="18%"><div align="left">Monthly Cost: </div></td><td align="right" width="15%" ><div align="right">$
			      <? 
					$cost = getNumbers("monthlycost",$month,$year); 
					echo "<!--- Cost: $cost --->";
					$wc = getNumbers("wiseclick",$month,$year);
					echo "<!--- $wc --->";
					$cost +=  $wc;
					
					echo "<!--- Cost (w/ wiseclick): $cost --->";
					echo number_format($cost,2,".",","); 
				?>
				</div></td>
				<td width="*">&nbsp;</td>
				
			</tr>
			<tr class="mainmovingtopic">
				<td class="mainmovingtopic" align="left" title="All leads that have ever failed.">
					<? //echo "Total Failed: ".getNumbers("failed",$month,$year); ?>
					&nbsp;
				</td>
				<td>&nbsp;</td>		
				<td><div align="left">Avg Cost Per Lead:
				</div></td><td align="right" ><div align="right">$
			      <?
				
					if ($month == date("m"))
						$today = getNumbers("leadstoday",$month,$year);
					else
						$today = 0;
					echo "<!--- $cost / $valid - $today --->";
					echo "<!--- ".($cost/$valid)." --->";
					if ($valid - $today > 0)//first day of leads
					{
						if ($month == date("m"))
							$cpl = number_format(($cost / ($valid - $today)),2,".",",");
						else
							$cpl = number_format(($cost / $valid),2,".",",");
						$first_day = true;
					}
					else
						$cpl = "0.00";
					echo "$cpl";
				?>
				 </div></td>
				 <td>&nbsp;</td>
			</tr>
			<tr class="mainmovingtopic">
				<td class="mainmovingtopic" align="left">
				<? echo "Limbo: ".getNumbers("not-proccessed",$month,$year); ?>
				</td>
				<td>&nbsp;</td>
				<td><div align="left">Monthly Revenue:</div></td><td align="right"><div align="right">$
			      <?php $revenue = getTotalLeadRevenue($month,$year); echo number_format($revenue,2,".",","); ?>
				</div></td>
				<td>&nbsp;</td>
			</tr>			
			<tr class="mainmovingtopic">
				<td class="mainmovingtopic" align="left">					
					<?php echo "the last time a lead was sent was:<Br/> ".formatLeadTime(getLastScrubbedLead())."."; ?>
				</td>
				<td>&nbsp;</td>
				<td width="25%" colspan="3"><div align="left"><font color="#FF0000">Note:</font> Numbers do not reflect today, and numbers are rounded to the nearest $.01 </div></td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="mainmovingtopic" align="left" colspan="5">										
					<form action="<?php echo $_SERVER['PHP_SELF']; ?>?page_id=5" method="post">
						Campaign: 
						<select name="campaign">
							<option value="all">All</option>					
							<?php
								$data = getSources($month,$year);
								
								$sources = array_keys($data);
								sort($sources);
								foreach($sources as $source)
								{	
									echo "<option";
									if ($campaign == $source) echo " selected";
									echo " value='$source'> ";
									if ($source == "org")
										echo "Organic";
									else
										echo ucwords($source);
									echo " (".$data[$source].")</option>\n";							
								}
							?>					
						</select>
						<Select name="month">				
							<?php							
								$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");							
								for ($i = 1; $i < 13; $i++)
								{
									echo "<option value='";
									if ($i < 10)
										echo "0$i";
									else
										echo "$i";
									echo "' ";
									if (intval($month) == ($i)) 
										echo "selected ";
									echo ">".$months[$i-1]."</option>\n";
								}
							?>
						</Select>
						<select name="year">
							<option value="2007">'07</option>				
							<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
						</select>
						<input type="submit" name="Filter" value="Go"/>
						<?php 
							$date = gmdate("Ymd", mktime(date("H"), date("i"), date("s"), date("m") , date("d") - 1 , date("Y")));
							$sql = "select * From movingdirectory.costperclick where site = 'tsc' and ts = '$date'";
							$rs = new mysql_recordset($sql);
							if (!$rs->fetch_array())
							{
								echo "<a href='#' onClick='openPopup();'>Get Yesterday's Data</a>";
							}
						 ?>
						
						
					</form>
				</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="mainmovingtopic" align="left" colspan="5">
					Residential
				</td>
			</tr>
			<tr>
				<td align="left" colspan="5">
					<?php displayLeads(RESIDENTIAL,$campaign,$month,$year);//defaults to the current month, coming from TopSecurityCompanis.com, that are Residential leads from any campaign. ?>	
				</td>
			</tr>
			<tr><td colspan="5">&nbsp;</td></tr>
			<tr>
				<td class="mainmovingtopic" colspan="5" align="left">
					Commercial
				</td>
			</tr>
			<tr>
				<td align="left" colspan="5">
					<?php displayLeads(COMMERCIAL,$campaign,$month,$year);//defaults to the current month, coming from TopSecurityCompanis.com, that are Residential leads from any campaign. ?>	
				</td>
			</tr>
		</table>
	</div>
<?	
	//Not curently used.
	function getPricePerLead($lead_id, $quote_type)
	{
		$prices = array(RESIDENTIAL => 8,COMMERCIAL => 10);
		
		switch($lead_id)
		{
			case BRINKS: 			
			case PROTECTION_ONE:			
				return $prices[$quote_type];				
			case PROTECT_AMERICA:
				return 8;
				break;
			case PINNACLE:
			case USALARM:
				return $prices[$quote_type] + 2;	
		}		
	}
	
	function getTotalLeadRevenue($month,$year)
	{
		 $array = array(BRINKS,USALARM,PROTECTION_ONE,PROTECT_AMERICA,PINNACLE);
		 foreach($array as $comp)
		 {
		 	$revenue += getLeadRevenue($comp, $month,$year);
		 }
		
		 //HARD CODED HACK FOR CONTRACT CHANGE...		
		 if ($month == 10 && $year == 2006)
		 	$revenue += 288;
		 
		 return $revenue;
	}
	
	function getLeadRevenue($lead_id, $month,$year)
	{
		$sql = "select count(quote_type) count, quote_type from irelocation.leads_security where lead_ids != '' and received like '$year$month%' and lead_ids like '%$lead_id%' ";			
		if (date("m") == $month)//if now is during this month.
				$sql .= " and received not like '$year$month".date("d")."%'";
		$sql .= " group by quote_type ";
		//echo $sql;
		echo "<!--- Company: ".getCompanyName($lead_id)." --->\n";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		$price = getPricePerLead($lead_id, $rs->myarray["quote_type"]);
		$results += $rs->myarray["count"] * $price;
		echo "<!--- ".$rs->myarray["quote_type"].": ".$rs->myarray["count"]." at \$".$price." per lead --->\n";
		$rs->fetch_array();
		$price = getPricePerLead($lead_id, $rs->myarray["quote_type"]);
		$results += $rs->myarray["count"] * $price;
		echo "<!--- ".$rs->myarray["quote_type"].": ".$rs->myarray["count"]." at \$".$price." per lead --->\n";
		
		return $results;		
	}
	
	//Get summary numbers.
	function getNumbers($what="valid",$month,$year)
	{	
		switch($what)
		{
			case "wiseclick":
				$sql = "select count(*)*20 count from irelocation.leads_security where source like '%wc%' and received like '$year$month%';";
				echo "<!---Wiseclick: $sql --->";
				break;
			case "valid":
				//-- valid Leads
				$sql = "select count(*) count from irelocation.leads_security where lead_ids != '' and received like '$year$month%';";
				break;
			case "failed":
				//-- Failed Scrubbing - cant filter by month/year
				$sql = "select count(*) count  from irelocation.leads_security where scrub_is_valid = 0 and scrub_id != 0;";
				break;
			case "not-proccessed":
				//-- Yet to be Processed - 3313 = first quote_id we received in november 06
				$sql = "select count(*) count from irelocation.leads_security where quote_id > 3313 and received like '';";
				break;		
			case "monthlycost":
				//-- cost for advertising for the month.
				$sql = "select sum(cost) count from movingdirectory.costperclick where ts like '$year$month%' and site like 'tsc%'";	
				break;
			case "leadstoday":
				//-- cost for advertising for the month.
				$sql = "select count(*) count from irelocation.leads_security where lead_ids != '' and received like '$year$month".date("d")."%';";	
				//echo $sql;
				break;
		}
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		return $rs->myarray["count"];
	}

	//format it for displaying the last lead scrubbed.
	function formatLeadTime($ts)
	{
		$year = substr($ts,0,4);
		$month = substr($ts,4,2);
		$day = substr($ts,6,2);
		$hour = substr($ts,8,2);
		if (intval($hour) > 12)
		{
			$pm = 1;
			$hour -= 12;		
		}
		else if (intval($hour) == 12)
		{
			$pm = 1;
		}
		else if (intval($hour) == 0)
			$hour = 12;
		$min = substr($ts,10,2);
		$sec = substr($ts,12,2);
		if (strlen($ts) > 8)
		{
			$time = intval($hour).":".$min.":".$sec;
			if ($pm == 1)
				$time .= " pm ";
			else
				$time .= " am ";				
			return "$time$month-$day-$year";
		}
		else if (strlen($ts) == 6)//YYYYMM
		{
			return "$month-$year";
		}
		else
			return "$month-$day-$year";

	}
	//really the last lead scrubbed AND processed...
	function getLastScrubbedLead()
	{
		$sql = "select max(received) received from irelocation.leads_security where scrub_is_valid = 1";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		return $rs->myarray["received"];
	}
		
	//print out the tables.
	function displayLeads($quote_type=RESIDENTIAL,$campaign="all", $month="*", $year="*",  $site_code="tsc")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		if ($campaign == "google" || $campaign ==  "overture" || $campaign ==  "msn" || $campaign == "google_local")
			print_cpc_month_header("tsc",$campaign,$month,$year);
		else
			print_month_header($month,$year);
		echo "<!-- displayLeads(,$month,$year) --->";
		print_it(getSecurityString(SITEBASE,$quote_type,$campaign,$month,$year), $month, $year,SITEBASE, $campaign, $site_code,$quote_type);
		print_it(getSecurityString(BRINKS,$quote_type,$campaign,$month,$year), $month, $year,BRINKS, $campaign, $site_code,$quote_type);
		print_it(getSecurityString(PROTECT_AMERICA,$quote_type,$campaign,$month,$year), $month, $year,PROTECT_AMERICA, $campaign, $site_code,$quote_type);
		print_it(getSecurityString(PROTECTION_ONE,$quote_type,$campaign,$month,$year), $month, $year,PROTECTION_ONE, $campaign, $site_code,$quote_type);		
		print_it(getSecurityString(PINNACLE,$quote_type,$campaign,$month,$year), $month, $year,PINNACLE, $campaign, $site_code,$quote_type);
		print_it(getSecurityString(USALARM,$quote_type,$campaign,$month,$year), $month, $year,USALARM, $campaign, $site_code,$quote_type);
		echo "</table>";
	}
	
	//based on "lead_id"
	function getCompanyName($lead_id)
	{
		switch($lead_id)
		{
			case BRINKS: return "Brinks";
			case PROTECT_AMERICA: return "Protect America";
			case PROTECTION_ONE:return "Protection One";
			case USALARM: return "US Alarm";
			case SITEBASE: return "TopSecurityCompanies.com";
			case PINNACLE: return "Pinnacle Security";
		}	
	}

	function getSecurityString($lead_id, $quote_type, $campaign="all", $month="*", $year="*", $site_code="tsc")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		echo "<!-- getSecurityString($lead_id,$month,$year) --->";
		$sql = "select substring(received,7,2) 'day', count(received) 'count' from irelocation.leads_security ".
		"where received like '$year$month%' ".
		getFilters($site_code, $lead_id, $campaign,$quote_type).
		" group by substring(received,1,8) WITH ROLLUP;";
		
		return $sql;
	}

	function getFilters($site_code="tsc", $lead_id, $campaign="all",$quote_type)
	{	
		if(is_numeric($lead_id))
			$base = " and lead_ids like '%$lead_id%' and quote_type = '$quote_type' ";
		else
			$base = " and quote_type = '$quote_type'";
			
		if ($campaign == "all")
		{
			return $base;
		}
		else if ($campaign == "org")//organic
		{						
			return $base." and (source = '$site_code' or source like '".$site_code."_search%') ";
		}
		else
		{
			$result = " and source = '$site_code";
			$result .= "_";
			$result .= "$campaign' ";
			
			return $base.$result;
		}	
	}

	function gatherSecurityLosses($yearmonth, $lead_id)
	{					
		$sql = "select substring(s.received,7,2) 'day', count(s.received) 'count' ".
				" from irelocation.leads_verified as v join irelocation.leads_security as s".
				" on v.quote_id = s.quote_id where s.received like '$yearmonth%' and v.success = 0".
				" and v.lead_id = '$lead_id' and s.lead_ids like '%$lead_id%' group by substring(s.received,1,8) WITH ROLLUP;";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$results = array();
		$last = -1;
		while ($rs->fetch_array())
		{
			if ($last == $rs->myarray["day"])//should be last row.
			{
				$results["total"] = $rs->myarray["count"];
				if ($results["total"] > 0)
					echo "<!--- Loss found... --->";
				return $results;
			}
			else
			{
				$last = $rs->myarray["day"];
				$results[$last] = $rs->myarray["count"];				
			}
		}
		return $results;
	}

	function print_it($sql, $month, $year, $lead_id, $campaign, $site_code,$quote_type)
	{		
		$comp_name = getCompanyName($lead_id);
		$losses = gatherSecurityLosses("$year$month", $lead_id);		
		echo "<!--- loss total: ".$losses["total"]." --->";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
		
		$link_base = "reporting.php?page_id=9&site=tsc&quote_type=$quote_type";
		
		if ($campaign != "all")
			$link_base .= "&campaign=$campaign";
		if (is_numeric($lead_id))
			$link_base .= "&lead_id=$lead_id";
		
				
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		while ($rs->fetch_array())
		{
			
			$counter++;
			
			$day = $rs->myarray["day"];
								
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			//Gap in the records.
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;				
				$count  = $rs->myarray["count"];// - $losses[$rs->myarray["day"]];				
				echo "<td align='right'>";
				echo "<a href='$link_base&received=$year$month$day'>";
				echo "$count</a></td>";
				//echo "$count</td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				//$total -= $losses["total"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
				else
				{
					echo "<td align='right'>";
					echo "<a href='$link_base&received=$year$month'>";
					echo "$total</a></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
	
	function getSources($month,$year)
	{
		$sql = "select distinct source, count(source) count from irelocation.leads_security where lead_ids != '' and source like 'tsc_%' and received like '$year$month%' group by source;";
		$rs = new mysql_recordset($sql);
		$results = array();
		while ($rs->fetch_array())
		{
			$s = substr($rs->myarray["source"],4);
			
			$results[$s] = $rs->myarray["count"];
		}
		
		$sql = "select count(source) count from irelocation.leads_security where lead_ids != '' and source like 'tsc' and received like '$year$month%'";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		$results["org"] = $rs->myarray["count"];
		return $results;	
	}

?>
<script>
	function openPopup()
	{
		window.open( "http://irelocation.com/adwords/test.php?site=tsc&reload=1", 'update' ,'width=250,height=250');
	}	
</script>