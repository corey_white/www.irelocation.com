<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Untitled Document</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script>
	function changeText(textfield)
	{	
		var value = textfield.value;
		if (value.length > 14)
			return value;
		value=value.replace("(","").replace(")","").replace("-","").replace("-","").replace(" ","");						
		//alert(value);
		if (isNaN(value))//not numbers
		{
			textfield.value = "";
			return;			
		}

		if (value.length == 3)	
		{			
			textfield.value = "("+value+")";			
		}
		else if (value.length == 6)
		{
			var pre = value.substring(0,3);
			var npp = value.substring(3);	
			textfield.value = "("+pre+") "+npp;
		}
		else if (value.length == 10)
		{
			var pre = value.substring(0,3);
			var npp = value.substring(3,6);	
			var suffix = value.substring(6);	
			textfield.value = "("+pre+") "+npp+" - "+suffix;		
		}
		else 
			return value;
	}
</script>
</head>

<body>
	<input type="text" name="phone" onkeyup="changeText(this);" value="(602) 317 - 9721" />
</body>
</html>
