<br/>
<?php
	define(PACKPRO,9000);
	define(DOOR2DOOR,9001);//zip code list.
	define(MOVEX,9002);
	define(GOSMARTMOVE,9003);//zip code list.
	define(ABFUPACK,9004);
	define(MOVINGSTAFFERS,1981);//not a lead id...
	define(SELFMOVINGQUTOES,"sitebase");
	define(BRINKS,8000);
	define(PROTECT_AMERICA,8002);
	define(PROTECTION_ONE,8003);
	define(US_ALARM,8005);
	define(MOVINGLABORSERVICES,1982);
	
	include "../inc_mysql.php";
	//always put orderby last. much easier to strip off.	
	extract($_REQUEST);
	
	$sql = "select * from ";
	if  ($site == "smq")
		$sql .= "irelocation.leads_selfmove where ";
	else if ($site == "tsc")
		$sql .= "irelocation.leads_security where ";
	else if ($site == "quotes_local")
		$sql .= "movingdirectory.quotes_local where ";
		
	$sql .= " 1=1 ";
				
	if (isset($campaign) && $source != "all")
	{
		if ($campaign != "org")
			$sql .= " and source = '".$site."_".$campaign."' ";
		else
			$sql .= " and source = '$site' ";
	}
	else if (isset($source) && $source != "all")
	{
		$sql .= " and source like '$source' ";
	}
	
	if (isset($received))
		$sql .= " and received like '$received%' ";
	if (isset($lead_id))
	{
		if ($lead_id == MOVINGSTAFFERS)
			$sql .= " and moving_staffer ";
		else if ($lead_id == MOVINGLABORSERVICES)
			$sql  .= " and moving_staffer > 1 ";
		else
			$sql .= " and lead_ids like '%$lead_id%' ";
	}
	if (isset($quote_type))
		$sql .= " and quote_type = '$quote_type' ";
		
	$sql = str_replace("1=1  and","",$sql);
	
	$filterlink = $_SERVER['QUERY_STRING'];
	//pull off old ordery by if it exists.
	$i = strpos($filterlink,"&orderby=");
	if ($i > 0)
		$filterlink = substr($filterlink,0,$i);
		
	
	
	if (isset($orderby))
	{
		$sql .= " order by $orderby";		
	}
	/*
	$display_sql = $sql;
	$display_sql = str_replace("select *","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; *",$display_sql);
	$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
	$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
	$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
	$display_sql = ereg_replace("('[a-z0-9%_]*')","<font color=\"red\">\\1</font>",$display_sql);
	*/

	echo formatSQL($sql);
	
	function formatSQL($display_sql)
	{	
		$display_sql = str_replace("select *","<font color=\"blue\">Select</font><Br/>\n&nbsp; &nbsp; *",$display_sql);
		$display_sql = str_replace("from","<br/><font color=\"blue\">from</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("where","<br/><font color=\"blue\">where</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("and","<font color=\"blue\">and</font><br/>\n&nbsp; &nbsp; ",$display_sql);
		$display_sql = str_replace("order by","<Br/><font color=\"blue\">order by</font><br/>\n&nbsp; &nbsp; ",$display_sql);	
		$display_sql = str_replace("like","<font color=\"blue\">like</font>",$display_sql);	
		$display_sql = ereg_replace("('[a-z0-9%_]*')","<font color=\"red\">\\1</font>",$display_sql);
		return "<font face='Courier New' size='2px'>".trim($display_sql).";</font><br/><br/>";
	}
	
	if (strpos($_SERVER['QUERY_STRING'],"&orderby=") > 0 )
	{				
		echo "<a class='mainmovingtopic' href='".$_SERVER['PHP_SELF']."?$filterlink'>Clear Ordering</a><br/>";
	}	
?>
<table border="1">
	<?php if ($site == "smq") { ?>
	<tr class="mainmovingtext">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=origin_zip"; ?>' class='mainmovingtopic'>Origin</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=destination_zip"; ?>' class='mainmovingtopic'>Destination</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=size_of_move"; ?>' class='mainmovingtopic'>Moving Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=source"; ?>' class='mainmovingtopic'>Campaign</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=moving_staffer"; ?>' class='mainmovingtopic'>M. S. / M. L. S.</a></th>
	</tr>
	<? } else if ($site == "tsc") { ?>
	<tr class="mainmovingtext">
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=quote_id"; ?>' class='mainmovingtopic'>Quote Id</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=quote_type"; ?>' class='mainmovingtopic'>Quote Type</a></th		
		><th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=received"; ?>' class='mainmovingtopic'>Time Stamp</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=state_code"; ?>' class='mainmovingtopic'>Location</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=sqr_footage"; ?>' class='mainmovingtopic'>Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=source"; ?>' class='mainmovingtopic'>Campaign</a></th>
		
	</tr>
	<? } else if ($site == "quotes_local") { ?>
	<tr class="mainmovingtext">		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=email"; ?>' class='mainmovingtopic'>Customer Info</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=source"; ?>' class='mainmovingtopic'>Source</a></th		
		><th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=received"; ?>' class='mainmovingtopic'>Time Stamp</a></th>
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=origin_zip"; ?>' class='mainmovingtopic'>Origin</a></th>		
		<th><A href='<?php echo $_SERVER['PHP_SELF']."?$filterlink&orderby=destination_zip"; ?>' class='mainmovingtopic'>Destination</a></th>
	</tr>
	<? } ?>
	<?php
		$rs = new mysql_recordset($sql);
		while ($rs->fetch_array())
		{
			if ($site == "smq")
				 buildSMQ($rs->myarray);			
			else if ($site == "tsc")
				 buildTSC($rs->myarray);
			else if ($site == "quotes_local")
				buildLocal($rs->myarray);
		}	
		$rowcount = $rs->rowcount();
		
		function buildLocal($array)
		{
			extract($array);
			$customerInfo = ucwords($name)."<br/>".$email."<Br/>".formatPhone($phone_home);
			$origin = ucwords($origin_city).", ".strtoupper($origin_state)."<Br/> $origin_zip";
			$destination = ucwords($destination_city).", ".strtoupper($destination_state)."<Br/> $destination_zip";
			
			$companies = str_replace("|",", ",$lead_ids);
			echo "<tr class='mainmovingtext'>";			
			echo "<td align='left' >$customerInfo</td>";
			echo "<td align='left' >$source</td>";
			echo "<td align='left' >".getReceived($received)."</td>";
			echo "<td align='left' >$origin</td>";
			echo "<td align='left' >$destination</td>";
			echo "</tr>";
		
		}
		
		//use this function if moving_staffer value > 1, AKA is a Moving Labor 
		//Service Lead. still can distinguish between movingstaffers	
		function getMovingLaborType($code)
		{
			$codes = array("No Labor","Moving Staffers","Load/Unload Long Distance","Load/Unload Local","Corporate Relo"
				,"Packing/Loading","Loadin Only","Unload Only");
			return $codes[$code];
		}
		
		function buildSMQ($array)
		{
			extract($array);
			$customerInfo = ucwords($fname." ".$lname)."<br/>".$email."<Br/>".formatPhone($phone1);
			$origin = ucwords($origin_city).", ".strtoupper($origin_state)."<Br/> $origin_zip";
			$destination = ucwords($destination_city).", ".strtoupper($destination_state)."<Br/> $destination_zip";
			$moving = "$size_of_move<Br/>$estimated_move_date<Br/>$needs";
			$code = getMovingLaborType($moving_staffer);

			$companies = str_replace("|",", ",$lead_ids);
			echo "<tr class='mainmovingtext'>";
			echo "<td align='left' >$customerInfo</td>";
			echo "<td align='left' >$origin</td>";
			echo "<td align='left' >$destination</td>";
			echo "<td align='left' >$moving</td>";
			echo "<td align='left' >$source</td>";
			
			echo "<td align='left' >$code</td>";			
			echo "</tr>";
		}
		
		function formatData($myarray)
		{
			switch($myarray["current_needs"])
			{
				case "new":	$myarray["current_needs"] = "New System";	break;
				case "upgrade":	$myarray["current_needs"] = "Upgrade Current System";	break;
				case "replace":	$myarray["current_needs"] = "Replace Current System";	break;
			}
	
			if ($myarray["quote_type"] == "com")
			{
				switch($myarray["sqr_footage"])
				{				
					case "25": $myarray["sqr_footage"] = "2,500 - 4,999 ft"; break;
					case "50": $myarray["sqr_footage"] = "5,000 - 9,999 ft"; break;
					case "100": $myarray["sqr_footage"] = "10,000 - 49,999 ft"; break;		  
					case "500": $myarray["sqr_footage"] = "50,000+ ft"; break;
					case "0": 
					default:
						$myarray["sqr_footage"] = "Up to 2,499 ft"; break;
				}
			}
			else
			{
				switch($myarray["sqr_footage"])
				{
					case "15": $myarray["sqr_footage"] = "1,500 - 2,999 ft"; break;
					case "30": $myarray["sqr_footage"] = "3,000 - 4,999 ft"; break;
					case "50": $myarray["sqr_footage"] = "5,000 - 19,999 ft"; break;		  
					case "200": $myarray["sqr_footage"] = "20,000+ ft"; break;
					case "0": 
					default:
						$myarray["sqr_footage"] = "Up to 1,499 ft"; break;
				}		
			}
			
			$myarray["maplink"] = getMapLink($myarray);
			$myarray["own_rent"] = ucwords($myarray["own_rent"]);
			$myarray["building_type"] = ucwords($myarray["building_type"]);
			$myarray["other_field"] = ucwords($myarray["other_field"]);
			$myarray["received"] = getReceived($myarray["received"]);
			
			return $myarray;
		}
	
		function getMapLink($myarray)
		{
			return "http://maps.google.com/?q=".urlencode($myarray["address"].", ".$myarray["city"].", ".$myarray["state_code"]." ".$myarray["zip"]);
		}		
		
		function formatPhone($phone)
		{	
			if (strlen($phone) == 10)
			{
				$p_area=substr($phone,0,3);
				$p_prefix=substr($phone,3,3);
				$p_suffix=substr($phone,6,4);
		
				return "($p_area) $p_prefix - $p_suffix";
			}
			else
				return $phone;
		}
		
		function getReceived($received)
		{
			$r_year=substr($received,0,4);
			$r_month=substr($received,4,2);
			$r_day=substr($received,6,2);
			$r_hour=substr($received,8,2);
			$r_min=substr($received,10,2);
			if (strlen($received) == 14)
				return "$r_month/$r_day/$r_year $r_hour:$r_min";
			else
				return " -- ";
		}	
	
		function buildTSC($array)
		{
			
			$array = formatData($array);
			extract($array);
			
			if ($quote_type == "res")
				$customerInfo = ucwords($name1." ".$name2)."<br/>".$email."<Br/>".formatPhone($phone1);
			else
				$customerInfo = ucwords($name1."<Br/>".$name2)."<br/>".$email."<Br/>".$phone1;
				
			$location = "<a href='$maplink' target='_blank' >$address <br/> $city, $state_code $zip</a>";
			if ($quote_type == "res")
				$quote_type = "Residential";
			else
				$quote_type = "Commercial";
			$info = $current_needs."<br/>".$building_type."<br/>".$sqr_footage;
			
			echo "<tr class='mainmovingtext'>";
			echo "<td align='center'>$quote_id</td>";
			echo "<td align='left' >$customerInfo</td>";			
			echo "<td align='left' >$quote_type</td>";
			echo "<td align='left' >$received</td>";			
			echo "<td align='left' >$location</td>";			
			echo "<td align='left' >$info</td>";
			echo "<td align='left' >$source</td>";			
			echo "</tr>";
		}
		
		function getCompanyName($lead_id)
		{
			switch($lead_id)
			{
				case PACKPRO: return "Pack Pro";
				case MOVEX: return "Movex";
				case ABFUPACK:return "ABF UPack";
				case DOOR2DOOR:return "Door 2 Door";
				case GOSMARTMOVE:return "Go Smart Move";
				case MOVINGSTAFFERS: return "Moving Staffers";			
				//security people.
				case BRINKS: return "Brinks";
				case PROTECT_AMERICA: return "Protect America";
				case PROTECTION_ONE:return "Protection One";
				case US_ALARM: return "US Alarm";
			}	
		}
		
	?>
</table>
<br/>
<span class="mainmovingtopic"><?php echo $rowcount; ?></span><span class="mainmovingtext"> leads.</span>
<br/>