<?php

	include "../inc_mysql.php";
	include "cron_functions.php";
	include "cpc.php";
	define (VALID," and lead_ids != '' and lead_ids != '||||||' ");
	define(AUTO_LEAD, 1);
	define(MOVING_LEAD, 2);
	define(INTERNATIONAL, 3);

	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];
	
	if (strlen($_POST["site_code"]) > 0)
	{
		$parts = explode("-",$_POST["site_code"]);
		$site_code = $parts[0];
		$cat_id = $parts[1];
	}

	$campaign = $_POST["campaign"];

			
	
	// cat_id = 1
	//if lead_ids == -1 return numbers for whole site.
	function getNumbersForMonth($source, $lead_ids,  $month="*", $year="*", $table="movingdirectory.quotes")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($year == 2005)
			$table = "movingdirectory.quotes_archive";
		else if ($year == 2004)
			$table = "movingdirectory.quotes_old";
//		else
			//use the table passed in.
		
		if ($lead_ids == -1 || $lead_ids == "-1")
			$lead_ids = "";
		
		echo "<!-- lead_ids = $lead_ids -->";
		
		$query = "select substring(received,7,2) 'day', count(received) 'count' from $table where received like '$year$month%' $lead_ids $source ";
		if (strpos($source,"tm") !== FALSE || strpos($source,"pm") !== FALSE || strpos($source,"me") !== FALSE )
			if (strpos($table,"irelocation.leads_") === FALSE)
				$query .= VALID;		
		$query .= " group by substring(received,1,8) WITH ROLLUP;";		
		return 	$query;
	}
		
	/*
		All calls to this method must have month and year populated allready.	
	*/
	function print_it($site_code,$campaign,$lead_id, $sql, $comp_name="Company Name Here",$month, $year, $international=0)
	{		
		if ($lead_id == "1386" || $lead_id == "1388")//wheaton or bekins, we have a response table from them..
		{
			$losses = gatherLosses("$year.$month", $lead_id);
			$loss = true;
			echo "<!--- loss total: ".$losses["total"]." --->";
		}
		else
		{
			$losses = 0;
			$loss = false;
		}
			
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
		
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		if ($international == 1)
			$international = "&cat_id=3";
		else
			$international = "";
		
		while ($rs->fetch_array())
		{
			
			$counter++;
			
			$day = $rs->myarray["day"];
								
			
			$ts = $year.$month.$day;
			
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}					
			
			
			if ($day != $last)
			{
				$last = $day;
				
				$count  = $rs->myarray["count"];
				if ($loss)
					$count -= $losses[$rs->myarray["day"]];
				echo "<td align='right'>";
				echo "<a href='reporting.php?page_id=5&ts=$ts&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year$international'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				if ($loss)
					$total -= $losses["total"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'> ";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else
				{
					//echo "<td>$total</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
	
	
	
?>
<div align="center">
	<table>
	<tr><td align="left">
		<table>
		<tr>
			<Td align="left" valign="top">
				<form action="reporting.php?page_id=0" method="post">
				<select name="site_code">					
					<option value="atus-1" <?php if ($site_code == "atus")  { echo "selected"; } ?> >Auto Transports.us</option>
					<option value="mmc-1" <?php if ($site_code == "mmc")  { echo "selected"; } ?> >Move My Car</option>
					<option value="tm-2" <?php if ($site_code == "tm")  { echo "selected"; } ?> >TopMoving</option>
					<option value="pm-2" <?php if ($site_code == "pm")  { echo "selected"; } ?> >ProMoving</option>
					<option value="me-2" <?php if ($site_code == "me")  { echo "selected"; } ?> >MovingEase</option>
					<option value="leads_auto" <?php if ($site_code == "leads_auto")  { echo "selected"; } ?> >D A S</option>
					<option value="leads_realtor" <?php if ($site_code == "leads_realtor")  { echo "selected"; } ?> >Realtor</option>
					<option value="tmc" <?php if ($site_code == "tmc")  { echo "selected"; } ?> >T M C</option>
				</select>
				<select name="campaign">
					<option value="all" <?php if ($campaign == "all")  { echo "selected"; } ?>>All Leads</option>				
					<option value="org" <?php if ($campaign == "org")  { echo "selected"; } ?>>Organic Leads</option>
					<option value="google" <?php if ($campaign == "google")  { echo "selected"; } ?>>Google</option>
					<option value="overture" <?php if ($campaign == "overture")  { echo "selected"; } ?>>Overture</option>
					<option value="msn" <?php if ($campaign == "msn")  { echo "selected"; } ?>>MSN</option>	
					<option value="ask" <?php if ($campaign == "ask")  { echo "selected"; } ?>>Ask.com</option>				
					<option value="silver" <?php if ($campaign == "silver")  { echo "selected"; } ?> >Silver Carrot</option>
					<option value="boxdev" <?php if ($campaign == "boxdev")  { echo "selected"; } ?> >Boxes Delivered</option>
					<option value="wc" <?php if ($campaign == "wc")  { echo "selected"; } ?> >Wise Click</option>
				</select>	
				<Select name="month">
				
					<?php
					
						$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
					
						for ($i = 1; $i < 13; $i++)
						{
							echo "<option value='";
							if ($i < 10)
								echo "0$i";
							else
								echo "$i";
							echo "' ";
							if (intval($month) == ($i)) 
								echo "selected ";
							echo ">".$months[$i-1]."</option>\n";
						}
					?>
				</Select>
				<select name="year">
					<option value="2007"  <?php if ($year == "2007" || ($year == '') )  { echo "selected"; } ?>>'07</option>	
					<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
					<option value="2005"  <?php if ($year == "2005")  { echo "selected"; } ?>>'05</option>
					<option value="2004"  <?php if ($year == "2004")  { echo "selected"; } ?>>'04</option>
					<option value="2003"  <?php if ($year == "2003")  { echo "selected"; } ?>>'03</option>
				</select>
				<input type="submit" value="Submit" name="submit" />
				</form>
			</td>
			<? if ($_POST["submit"] == "Submit") { ?>
			<td>&nbsp;</td>
			<td valign="top" class="mainmovingtopic">Totals:</td>
			<td>
				<table>
					<Tr><Td class="mainmovingtopic">Monthly Cost:</Td><td class="mainmovingtext">0</td></Tr>
					<Tr><Td class="mainmovingtopic">Monthly Revenue:</Td><td class="mainmovingtext">0</td></Tr>
					<Tr><Td class="mainmovingtopic">Average Cost per Lead:</Td><td class="mainmovingtext">0</td></Tr>
				</table>
			
			</td>
			<? } ?>
		</tr>
		</table>

	</td></tr>
	<tr><td>

<?	
	if (strlen($site_code) > 0)
	{
		if ( $site_code != "leads_realtor" && $site_code != "leads_auto" && $site_code != "tmc")
		{
			$counter = 0;
			if ($month == date("m") && $year == date("Y"))//this month
				$data_arrays = getCompanies($site_code,-1,-1);
			else
				$data_arrays = getCompaniesOnSite($site_code,$month,$year);
			
			echo "<!---";
			print_r($data_arrays);
			echo "--->";
				
			$cat_id = 1;
			if ($site_code == 'tm' || $site_code == 'pm')
			{
				$topOrPro = 1;
				$cat_id = 2;
				echo "<div align='left' >Domestic Leads:</div>";
			}
			if (($site_code == "pm" || $site_code == "tm" || $site_code == "cs" || $site_code == "mmc" || $site_code == "atus") && ($campaign == "google" || $campaign == "overture" || $campaign == "msn"))
				print_cpc_month_header($site_code, $campaign, $month,$year);			
			else
				print_month_header($month,$year,$cat_id);
			//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
			print_company_data($site_code,$campaign, -1, displayTitle($site_code), $month, $year);
			foreach ($data_arrays as $data)
			{				
				$lead_ids = "and lead_ids like '%".$data["lead_id"]."%'";
				//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
				print_company_data($site_code,$campaign,$data["lead_id"],$data["comp_name"],$month,$year);				
				$counter++;
			}
			
			echo "</table>";
			
			
			if ($topOrPro == 1)//now print international leads.
			{
				$ndays = getNDays($month,$year) + 2;
				$cat_id = 3;
				echo "International Leads:<br/>";
				$data_arrays = GetInternationalCompanies($site_code,$month,$year);
				print_month_header($month,$year,$cat_id);
				print_international_company_data($site_code,$campaign, -1, displayTitle($site_code), $month, $year);
				foreach ($data_arrays as $data)
				{				
					$lead_ids = "and lead_ids like '%".$data["lead_id"]."%'";
					//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
					print_international_company_data($site_code,$campaign,$data["lead_id"],$data["comp_name"],$month,$year);				
					$counter++;
				}
				echo "</table>";
			}	
			
			echo "<div class='mainmovingtext'/>Company Count: <Strong>$counter</strong> </div>";
		}		
		else if ($site_code == "tmc")
		{
			print_month_header($month,$year,1);
			print_tmc_data(-1 , $month, $year);
			print_tmc_data("tm" , $month, $year);
			print_tmc_data("pm", $month, $year);
			echo "</table>";
		}
		else
		{
			//print_irelo_data($site_code,$campaign,$referer,$comp_name, $month="*", $year="*")
			//echo "The Individual Leads pages does not work.<Br/>";
			$comp_name = "Dependable Auto Shippers";
			if ($site_code == "leads_realtor")
				$comp_name = "Leads Realtor";
			print_month_header($month,$year,1);
			print_irelo_data($site_code,$campaign, -1, $comp_name , $month, $year);
			print_irelo_data($site_code,$campaign, 'pm', "ProMoving", $month, $year);
			print_irelo_data($site_code,$campaign, 'tm', "TopMoving", $month, $year);
			echo "</table>";
		}
		
	}
?>
</td></tr></table>
</div>