<?php
	
	include '../inc_mysql.php';
	include 'lib_functions.php';
	
	$sites = array(  array("organic","Organic Leads"),  array("google", "Leads from Google"), array("MSN", "Leads from MSN"), array("overture","Leads from Overture"));

	$months = array ("January","February","March","April","May","June","July","August","September","October","November","December");
	
	$curr_month = date('n');
	$curr_day = date('j');
	
	$source = $_POST["source"];
	$month =  $_POST["month"];
	$year =  $_POST["year"];
	$distinct = $_POST["distinct"];
		
	if (strlen($source) == 0 || $source == "default")
	{
		$source = "organic";
		$month = date('m');
		$year = date('Y');
	}
		
	if ($month != '')
	{
		$curr_month = $month;
		$ndays = cal_days_in_month(CAL_GREGORIAN, intval($curr_month), $year);	
	}
	else
		$ndays = date('t');
?>
<br/>
<div align='center'>
<table>
	<tr>
		<td>
		<form action="reporting.php?page_id=0" method="post">
			<select name="source">
				<?php
					foreach($sites as $site)
						if ($site[0] == $source)
							echo "<option value='".$site[0]."' selected > ".$site[1]." </option>\n";					
						else
							echo "<option value='".$site[0]."' > ".$site[1]." </option>\n";	
				?>
			</select>
			
			<select name="month"> 
				<?php
					for ($j = 1; $j < 13; $j++)
						if ($j== $curr_month)
							echo "<option value='$j' selected > ".$months[$j-1]." </option>\n";				
						else
							echo "<option value='$j' > ".$months[$j-1]." </option>\n";				
				?>
			</select>
			
			<select name="year"> 
				<option>2004</option>
				<option>2005</option>
				<option selected>2006</option>
			</select>
			
			<input type="checkbox" name="distinct" <?php if ($distinct) { echo "checked"; } ?> /> Distinct Emails
			
			<input type="submit" value="Display"/>
			
		</form>
		</td>
	</tr>
	<tr>
		<td>



		<br/><br/>
		</td>
	</tr>
</table>
</div>