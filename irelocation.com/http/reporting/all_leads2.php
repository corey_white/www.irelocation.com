<?php

	include "../inc_mysql.php";
	include "cron_functions.php";
	
	define (VALID," and lead_ids != '' and lead_ids != '||||||' ");
	define(AUTO_LEAD, 1);
	define(MOVING_LEAD, 2);
	define(INTERNATIONAL, 3);

	if (strlen($_POST["month"]) == 0)
		$month = date("m");
	else
		$month = $_POST["month"];
	
	if (strlen($_POST["year"]) == 0)
		$year = date("Y");
	else
		$year = $_POST["year"];
	
	if (strlen($_POST["site_code"]) > 0)
	{
		$parts = explode("-",$_POST["site_code"]);
		$site_code = $parts[0];
		$cat_id = $parts[1];
	}

	$campaign = $_POST["campaign"];

			
	
	// cat_id = 1
	//if lead_ids == -1 return numbers for whole site.
	function getNumbersForMonth($source, $lead_ids,  $month="*", $year="*", $table="movingdirectory.quotes")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($year == 2005)
			$table = "movingdirectory.quotes_archive";
		else if ($year == 2004)
			$table = "movingdirectory.quotes_old";
//		else
			//use the table passed in.
		
		if ($lead_ids == -1 || $lead_ids == "-1")
			$lead_ids = "";
		
		echo "<!-- lead_ids = $lead_ids -->";
		
		$query = "select substring(received,7,2) 'day', count(received) 'count' from $table where received like '$year$month%' $lead_ids $source ";
		if (strpos($source,"tm") !== FALSE || strpos($source,"pm") !== FALSE || strpos($source,"me") !== FALSE )
			if (strpos($table,"irelocation.leads_") === FALSE)
				$query .= VALID;		
		$query .= " group by substring(received,1,8) WITH ROLLUP;";		
		return 	$query;
	}
		
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
		
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";
		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		if ($cat_id == 1)
			echo "<th allirgn='left'>Active</th>";
		echo "</tr>";
		//echo "</table>";
	}
	
	function print_irelo_data($site_code,$campaign,$referer,$comp_name, $month="*", $year="*")
	{
		if ($referer != -1)
			$source_code = getiReloSourceString($referer, $campaign);
		else
			$source_code = "";
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$table = "irelocation.$site_code";
		/*
		$comp_name = "TopMoving.com";
		if ($referer == "pm")
			$referer = "ProMoving.com";
		*/
		$query = getNumbersForMonth($source_code,-1,$month,$year,$table);
		echo "<!--- Site Code: ".substr($site_code,18)." --->";
		
		print_it(substr($table,18),$campaign,$referer, $query,$comp_name,$month,$year);
	}
	
	///if lead_id != -1, then comp_name is a company name.
	//otherwise, comp_name, is a site name.
	function print_company_data($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
	{
		$source_code = getSourceString($site_code, $campaign);
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($lead_id != -1)
			$lead_string = " and lead_ids like '%".$lead_id."%' ";
		else
			$lead_string = -1;
			
		$query = getNumbersForMonth($source_code,$lead_string,$month,$year);
		echo "<!--- $site_code --->";
		echo "<!--- $query --->";
		print_it($site_code,$campaign,$lead_id, $query,$comp_name,$month,$year);
	}
	
	function print_international_company_data($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
	{
		$source_code = getSourceString($site_code, $campaign);
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($lead_id != -1)
			$lead_string = " and lead_ids like '%".$lead_id."%' "." and cat_id = 3 ";
		else
			$lead_string = " and cat_id = 3 ";
			
		$query = getNumbersForMonth($source_code,$lead_string,$month,$year);
		
		print_it($site_code,$campaign,$lead_id, $query,$comp_name,$month,$year,1);
	}
	
	/*
		All calls to this method must have month and year populated allready.	
	*/
	
	function print_it($site_code,$campaign,$lead_id, $sql, $comp_name="Company Name Here",$month, $year, $international=0)
	{		
		if ($lead_id != -1 && is_numeric($lead_id))
		{
			
			$sql_active = "select cat_id, active From movingdirectory.directleads where lead_id = '$lead_id'";
			//echo $sql_active."<br/>";
			$rs2 = new mysql_recordset($sql_active);
			$rs2->fetch_array();
			$active = $rs2->myarray["active"];
			$cat_id = $rs2->myarray["cat_id"];
		}
		else
			$cat_id = 0;
		
		$rs = new mysql_recordset($sql);
		
		
		$ndays = getNDays($month,$year);
		$counter = 0;
		
		$last = -1;
		
		/*
		if (strpos($site_code,"irelocation.leads_") !== FALSE)
		{
			$referer = $lead_id;
			echo "<!-- table=$site_code&ts=$year$month15&referer=$referer&campaign=$campaign -->";
		}
		*/
		
		echo "<tr>\n<td align='left'><Strong>$comp_name</strong></td>";
		
		if ($international == 1)
			$international = "&cat_id=3";
		else
			$international = "";
		
		while ($rs->fetch_array())
		{
			
			$counter++;
			$day = $rs->myarray["day"];
			$ts = $year.$month.$day;
			if ($last == -1)
			{
				//first day WE HAVE RECORDS FOR
				if ($day != 1)//we gotta add cells..
				{
					echo "<td colspan='".($day-1)."'>&nbsp;</td>";
					$counter = $day;
				}
			}
			else if ($day > $counter)
			{
				$delta = $day - $counter;
				echo "<td colspan='$delta'>&nbsp;</td>";
				$counter += $delta;
			}
			
			
				
				
			if ($lead_id == -1)
				echo "<!--- link to leads: show_leads.php?ts=$ts&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year$international --->";
			
			
			
			
			if ($day != $last)
			{
				$last = $day;
				
				$count  = $rs->myarray["count"];
				echo "<td align='right'>";
				echo "<a href='reporting.php?page_id=5&ts=$ts&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year$international'>";
				echo "$count</a></td>";
			}
			else//total column
			{				
				$total  = $rs->myarray["count"];
				if ($day == $ndays)
				{
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ((intval($ndays) - intval($last)) > 0)
				{
					echo "<td colspan='".(intval($ndays) - intval($last))."'>&nbsp;</td>";
					echo "<td align='right'> ";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else if ($day < $nday)
				{
					$delta = $ndays - $day;
					echo "<td colspan='$delta'>&nbsp;</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				else
				{
					//echo "<td>$total</td>";
					echo "<td align='right'>";
					echo "<a href='reporting.php?page_id=5&ts=$year$month&site=$site_code&campaign=$campaign&lead_id=$lead_id&month=$month&year=$year'>";
					echo "$total</a></td>";
				}
				
				if ($cat_id == 1)
				{
					$checked = ($active==1)?" checked ":" ";
					echo "<td><input type='checkbox' name='activate' $checked value = '$lead_id'/></td>";
				}
			}			
		}				
		echo "</tr>\n";
	}
	
	function getiReloSourceString($site_code, $campaign="all")
	{	
		if ($campaign == "all")
		{
			return " and referer like '$site_code%' ";
		}
		else if ($campaign == "org")//organic
		{			
			echo "<!-- organic leads. -->";
			return " and (referer = '$site_code' or referer like '".$site_code."_search%') ";
		}
		else
		{
			$result = " and referer like '$site_code";
			$result .= "_";
			$result .= "$campaign%' ";
			
			return $result;
		}	
	}
	
	
	 function getSourceString($site_code, $campaign="all")
	 {
	 	echo "<!--- $site_code $campaign --->";

	 	if ($campaign == "all")
		{
			if($site_code == "cs")//add csq source...
				return " and (source like 'cs%' or source like 'csq%') ";
			else
				return " and source like '$site_code%' ";
		}
		else if ($campaign == "org")//organic
		{			
			echo "<!-- organic leads. -->";
			if($site_code == "cs")//add csq source...
				return " and (source = 'cs' or source = 'csq' or source like 'cs_search%' or source like 'csq_search%') ";
			else if ($site_code = "pm")
				return " and (source = 'pm' or source like 'pm_search%' or source like 'pm_mmc_search%' or source = 'pm_mmc' or source like 'pm_cs_search%' or source = 'pm_cs' or source like 'pm_atus_search%' or source = 'pm_atus') ";				
			else
				return " and (source = '$site_code' or source like '".$site_code."_search%') ";
		}
		else
		{
			if ($site_code == "cs")
			{
				$result = "and (source like 'cs_".$campaign."%' or source like 'csq_".$campaign."%' ) ";			
			}
			else
			{				
				$result = " and source like '$site_code";
				$result .= "_";
				$result .= "$campaign%' ";
			}
			
			
			return $result;
		}	 	
	 }
	
?>
<div align="center">
	<table>
	<tr><td align="left">
		<table>
		<tr>
			<Td align="left">
				<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
				<!---<form action="reporting.php?page_id=0" method="post">--->
				<select name="site_code">
					<option value="cs-1" <?php if ($site_code == "cs")  { echo "selected"; } ?> >Car Shipping</option>
					<option value="atus-1" <?php if ($site_code == "atus")  { echo "selected"; } ?> >Auto Transports.us</option>
					<option value="mmc-1" <?php if ($site_code == "mmc")  { echo "selected"; } ?> >Move My Car</option>
					<option value="tm-2" <?php if ($site_code == "tm")  { echo "selected"; } ?> >TopMoving</option>
					<option value="pm-2" <?php if ($site_code == "pm")  { echo "selected"; } ?> >ProMoving</option>
					<option value="me-2" <?php if ($site_code == "me")  { echo "selected"; } ?> >MovingEase</option>
					<option value="leads_auto" <?php if ($site_code == "leads_auto")  { echo "selected"; } ?> >D A S</option>
					<option value="leads_realtor" <?php if ($site_code == "leads_realtor")  { echo "selected"; } ?> >Realtor</option>
					
					
				</select>
				<select name="campaign">
					<option value="all" <?php if ($campaign == "all")  { echo "selected"; } ?>>All Leads</option>				
					<option value="org" <?php if ($campaign == "org")  { echo "selected"; } ?>>Organic Leads</option>
					<option value="google" <?php if ($campaign == "google")  { echo "selected"; } ?>>Google</option>
					<option value="overture" <?php if ($campaign == "overture")  { echo "selected"; } ?>>Overture</option>
					<option value="msn" <?php if ($campaign == "msn")  { echo "selected"; } ?>>MSN</option>				
				</select>	
				<Select name="month">
				
					<?php
					
						$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
					
						for ($i = 1; $i < 13; $i++)
						{
							echo "<option value='";
							if ($i < 10)
								echo "0$i";
							else
								echo "$i";
							echo "' ";
							if (intval($month) == ($i)) 
								echo "selected ";
							echo ">".$months[$i-1]."</option>\n";
						}
					?>
				</Select>
				<select name="year">
					<option value="2007"  <?php if ($year == "2007" || ($year == '') )  { echo "selected"; } ?>>'07</option>	
					<option value="2006"  <?php if ($year == "2006" || ($year == '') )  { echo "selected"; } ?>>'06</option>				
					<option value="2005"  <?php if ($year == "2005")  { echo "selected"; } ?>>'05</option>
					<option value="2004"  <?php if ($year == "2004")  { echo "selected"; } ?>>'04</option>
					<option value="2003"  <?php if ($year == "2003")  { echo "selected"; } ?>>'03</option>
				</select>
				<input type="submit" value="Submit" />
				</form>
			</td>
		</tr>
		</table>

	</td></tr>
	<tr><td>

<?	
	if (strlen($site_code) > 0)
	{
		if ( $site_code != "leads_realtor" && $site_code != "leads_auto" )
		{
			$counter = 0;
			$data_arrays = getCompaniesOnSite($site_code,$month,$year);
			$cat_id = 1;
			if ($site_code == 'tm' || $site_code == 'pm')
			{
				$topOrPro = 1;
				$cat_id = 2;
				echo "<div align='left' >Domestic Leads:</div>";
			}
			print_month_header($month,$year,$cat_id);
			//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
			print_company_data($site_code,$campaign, -1, displayTitle($site_code), $month, $year);
			foreach ($data_arrays as $data)
			{				
				$lead_ids = "and lead_ids like '%".$data["lead_id"]."%'";
				//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
				print_company_data($site_code,$campaign,$data["lead_id"],$data["comp_name"],$month,$year);				
				$counter++;
			}
			
			echo "</table>";
			
			
			if ($topOrPro == 1)//now print international leads.
			{
				$ndays = getNDays($month,$year) + 2;
				$cat_id = 3;
				echo "International Leads:<br/>";
				$data_arrays = GetInternationalCompanies($site_code,$month,$year);
				print_month_header($month,$year,$cat_id);
				print_international_company_data($site_code,$campaign, -1, displayTitle($site_code), $month, $year);
				foreach ($data_arrays as $data)
				{				
					$lead_ids = "and lead_ids like '%".$data["lead_id"]."%'";
					//($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
					print_international_company_data($site_code,$campaign,$data["lead_id"],$data["comp_name"],$month,$year);				
					$counter++;
				}
				echo "</table>";
			}	
			
			echo "<div class='mainmovingtext'/>Company Count: <Strong>$counter</strong> </div>";
		}		
		else
		{
			//print_irelo_data($site_code,$campaign,$referer,$comp_name, $month="*", $year="*")
			//echo "The Individual Leads pages does not work.<Br/>";
			$comp_name = "Dependable Auto Shippers";
			if ($site_code == "leads_realtor")
				$comp_name = "Leads Realtor";
			print_month_header($month,$year,1);
			print_irelo_data($site_code,$campaign, -1, $comp_name , $month, $year);
			print_irelo_data($site_code,$campaign, 'pm', "ProMoving", $month, $year);
			print_irelo_data($site_code,$campaign, 'tm', "TopMoving", $month, $year);
			echo "</table>";
		}
		
	}
?>
</td></tr></table>
</div>