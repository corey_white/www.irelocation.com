<?php
	include_once '../inc_mysql.php';	
	include_once 'lib_functions.php';
	define(NUMBER,50);
	define(SIRVA,1387);
	define(WHEATON,1386);
	define(BEKINS,1388);
	define(PAULARPIN,1427);
	define(ALBERT,1517);
	define(AWVL,1527);
	
	//format the timestamp to a human-readable format.
	function formatTime($ts)
	{
		$year = substr($ts,0,4);
		$month = intval(substr($ts,4,2));
		$day = intval(substr($ts,6,2));
		$pm = 0;
		$hours = substr($ts,8,2);
		if ($hours > 12)
		{
			$hours -= 12;
			$pm = 1;
		}
		$mins = intval(substr($ts,10,2));
		if ($mins < 10)
			$mins = "0".$mins;
			
		$secs = intval(substr($ts,12,2));
		
		if ($secs < 10)
			$secs = "0".$secs;

		return "$hours:$mins:$secs ".(($pm==1)?" pm":" am")." $month/$day/$year";
	}
	
	//get the count of companies on a site this month.
	function countRecentCompanies($site)
	{
		$sql = "Select count(*) as count from movingdirectory.reportcompany where site = '$site' and month = '-1' and year = '-1'";		
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return $rs->myarray["count"];
		}
		else return 0;
	}
	//get the count of companies on a site during a past month.
	function countCompanies($site, $month, $year)
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "Select count(*) as count from movingdirectory.reportcompany where site = '$site' and month = '$month' and year = '$year'";
		echo "<!--- SQL: $sql --->";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return $rs->myarray["count"];
		}
		else return 0;
	}
	//International data for TM and PM
	function GetInternationalCompanies($site, $month="*", $year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "Select * from movingdirectory.reportcompany where site = '$site' and month = '$month' and year = '$year' order by comp_name ";
		$results = array();
		$rs = new mysql_recordset($sql);
		$counter = 0;
		while ($rs->fetch_array())
		{
			$lead_id = $rs->myarray["lead_id"];//get the lead_id
			$is_moving = ($site == 'tm' || $site == 'pm');//is it a moving site with international leads?  add ME if they ever get cat_id 3 leads.
			
			//if its moving and a Domestic company add them, or if its not moving, add all of them.
			if (intval($lead_id) != AWVL && intval($lead_id) != ALBERT && intval($lead_id) != PAULARPIN && intval($lead_id) != WHEATON && intval($lead_id) != BEKINS)
			{
				$results[$counter] = $rs->myarray;			
				$results[$counter]["last"] = formatTime(getLastLead($rs->myarray["lead_id"],"source like '$site%'"));
				$counter++;
			}
			else
				echo "<!--- company excluded : $lead_id --->";
			
		}
		return $results;
	}
	//return the companies on a given site during a given month,year
	function GetCompanies($site, $month="*", $year="*")
	{
		
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "Select * from movingdirectory.reportcompany where site = '$site' and month = '$month' and year = '$year' order by comp_name ";
		$results = array();
		//echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$counter = 0;
		while ($rs->fetch_array())
		{
			$lead_id = $rs->myarray["lead_id"];//get the lead_id
			$is_moving = ($site == 'tm' || $site == 'pm');//is it a moving site with international leads?  add ME if they ever get cat_id 3 leads.
			
			//if its moving and a Domestic company add them, or if its not moving, add all of them.
			if ((($is_moving == TRUE) && (intval($lead_id) == ALBERT  ||  intval($lead_id) == PAULARPIN || intval($lead_id) == WHEATON || intval($lead_id) == SIRVA || intval($lead_id) == BEKINS  || intval($lead_id) == AWVL)) || ($is_moving == FALSE))
			{
				$results[$counter] = $rs->myarray;			
				if ($year == "-1")
				{
					$year = date("Y");
					 $month = date("m");
				}
				$results[$counter]["last"] = formatTime(getLastLead($rs->myarray["lead_id"],"source like '$site%'"));
				$counter++;
			}
			//else
				//echo "<!--- company excluded : $lead_id --->";
			
		}
		return $results;
	}
	//Delete all entries for a given month, should only be used for the
	//current month, as the old data does not need to be refreshed.
	function deleteCompanies($site)
	{
		$year = date("Y");
		$month = date("m");
		$sql = "delete from movingdirectory.reportcompany where site = '$site' and month = '-1' and year = '-1'";		
		$rs = new mysql_recordset($sql);
	}
	
	function getSQL($source_string, $table="movingdirectory.quotes")
	{
		$year = date("Y");
		$month = date("m");
		//echo $year.$month;
		$SQL = "select distinct(lead_ids) as lead_ids From $table where $source_string and received like '$year$month%' order by received desc limit ".NUMBER;


		return $SQL;
	}
	//return the timestamp of hte last lead received on a site
	function getLastLead($lead_id, $source_string, $month="*", $year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$SQL = "select max(received) as last From movingdirectory.quotes where $source_string and received like '$year$month%' and lead_ids like '%$lead_id%'";
		$rs = new mysql_recordset($SQL);
		if($rs->fetch_array())
			return 	$rs->myarray["last"];
	}
	
	
	function _cron()
	{
		updateCompaniesOnSite("cs");
		updateCompaniesOnSite("mmc");
		updateCompaniesOnSite("atus");
		updateCompaniesOnSite("me");
		updateCompaniesOnSite("tm");
		updateCompaniesOnSite("pm");
		
	}
	
	function updateCompaniesOnSite($site)
	{
		//kill the old set.
		deleteCompanies($site);
		
		$sql = getSQL("source like '$site%'");
		$rs = new mysql_recordset($sql);
		
		//if ($site == "pm")
			//echo $sql."<Br/>";	
		$distinct_leads = array();
		$counter = 0;
		
		while ($rs->fetch_array())
		{
			$leads = explode("|",$rs->myarray["lead_ids"]);
			
			
			
			foreach($leads as $lead)
			{
				if (strlen($lead) > 0)
				{
					if (array_search($lead,$distinct_leads) === FALSE)
					{
						array_push($distinct_leads,$lead);
						$comp_query = getCompanyByLead($lead);
						$rs2 = new mysql_recordset($comp_query);
						
						if ($rs2->fetch_array())
						{
							$name = $rs2->myarray["comp_name"];
							$comp_id= $rs2->myarray["comp_id"];							
						}
						//if ($site == "pm")			
							//echo " $lead, $comp_id, $name, $site, -1, -1 <Br/>";						
						addCompanyToSite($lead,$comp_id,$name,$site,-1,-1);
					}				
				}		
			}
		}	
	}
	
	function getCompaniesOnSite($site,$month,$year)
	{
		if (countCompanies($site,$month,$year) == 0)
			addPastCompanies($site,$month,$year);
		
		return GetCompanies($site,$month,$year);		
	}
	
	function addPastCompanies($site,$month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($year == 2005)
			$table = "movingdirectory.quotes_archive";
		else if ($year == 2004)
			$table = "movingdirectory.quotes_old";
		else $table = "movingdirectory.quotes";
		
		$query = "select distinct(lead_ids) From $table where received like '$year$month%' and source like '$site%' order by cat_id";
		
		$rs = new mysql_recordset($query);
		
		$distinct_leads = array();
		$counter = 0;
			
		while($rs->fetch_array())
		{
			$leads = explode("|",$rs->myarray["lead_ids"]);
			foreach($leads as $lead_id)
			{
				if (strlen($lead_id) > 0)
				{
					if (array_search($lead_id,$distinct_leads) === FALSE)
					{
						array_push($distinct_leads,$lead_id);
						$comp_query = getCompanyByLead($lead_id);
						$rs2 = new mysql_recordset($comp_query);
						
						if ($rs2->fetch_array())
						{
							$comp_name = $rs2->myarray["comp_name"];
							$comp_id= $rs2->myarray["comp_id"];							
						}
						addCompanyToSite($lead_id,$comp_id,$comp_name,$site,$month,$year);
					}				
				}		
			}		
		}
	}
	
	
	/*
		Add a company to a site for a given month,year. Defaults to the current
		month/year.
	*/
	function addCompanyToSite($lead,$comp_id,$name,$site, $month="*", $year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$sql = "insert into movingdirectory.reportcompany (site,lead_id,comp_id,comp_name,month,year) values ('$site',$lead,$comp_id,'$name', '$month','$year')";		
		$rs = new mysql_recordset($sql);		
	}
	
	function getNDays($month="*",$year="*")
	{
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		return cal_days_in_month(CAL_GREGORIAN, intval($month),$year);
	}
		
	function getCPCData($ts, $site, $engine)
	{
		$sql_site = $site;
		$source_engine = $engine;
		
		echo "<!--- SITE: $site Engine: $engine --->";
		switch($site)
		{
			case "tsc":
				$table = "irelocation.leads_security";
				$filter = "lsm.scrub_is_valid = 1 and";
				if ($engine == "google_local")
				{
					$sql_site = "tsc";
					$site = "tsc-local";
					$engine = "google";				
					$source_engine = "google_local";
				}
				else
					$filter .= " source not like '%local%' and ";
				break;
			case "smq":
				$table = "irelocation.leads_selfmove";
				$filter = "lsm.scrub_is_valid = 1 and";
				break;
			case "pm":
				$table = "movingdirectory.quotes";
				$filter = "lsm.lead_ids != '' and lsm.lead_ids != '||||||' and ";
				break;
			case "tm":
				$table = "movingdirectory.quotes";
				$filter = "lsm.lead_ids != '' and lsm.lead_ids != '||||||' and ";
				break;
			case "asc":
				$table = "autoshipping.quotes";
				$filter = "";
				break;
			case "me":
				$table = "movingdirectory.quotes";
				$filter = "lsm.lead_ids != '' and lsm.lead_ids != '||||||' and ";
				break;
			case "tm-local":
			case "tm-local":
				$table = "movingdirectory.quotes";
				$filter = "lsm.lead_ids != '' and lsm.lead_ids != '||||||' and";
				$sql_site = "tm";
				break;
			case "cs":
			case "mmc":
			case "atus":
			case "csq":
				$table = "movingdirectory.quotes";
				$filter = "";
				break;
		}
			
	
		$sql = "select 
					substring(lsm.received,7,2) as day , 
					cost, 
					clicks, 
					count(*) leads,
					(cost/count(*)) as cpl,
					(cost/clicks) as cpc
				from 
					".$table." as lsm 
					join
					movingdirectory.costperclick as cpc
				on substring(lsm.received,1,8) = cpc.ts
				where 
					lsm.received like '".$ts."%' and 
					 $filter 
					cpc.site = '".$site."' and 
					lsm.source like '".$sql_site."_".$source_engine."%' and
					cpc.search_engine = '".$engine."'
				group by day order by day asc;";
		echo "<!--- $sql --->";
		$rs = new mysql_recordset($sql);
		$results = array();
		
		while ($rs->fetch_array())
		{	
			$results[intval($rs->myarray["day"])]["cost"] = $rs->myarray["cost"];
			$results[intval($rs->myarray["day"])]["clicks"] = intval($rs->myarray["clicks"]);
			
				$results[intval($rs->myarray["day"])]["leads"] = intval($rs->myarray["leads"]);			
				//$results[intval($rs->myarray["day"])]["cpl"] = substr($rs->myarray["cpl"],0,(strpos($rs->myarray["cpl"],"\.")+3));			
				$results[intval($rs->myarray["day"])]["cpl"] = number_format($rs->myarray["cpl"],2);			
				//$results[intval($rs->myarray["day"])]["cpc"] = substr($rs->myarray["cpc"],0,(strpos($rs->myarray["cpc"],"\.")+3));	
				$results[intval($rs->myarray["day"])]["cpc"] = number_format($rs->myarray["cpc"],2);			
		}
		
		return $results;
	}
	
		
	function print_cpc_month_header($site, $engine="google", $month="*",$year="*")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
		$name = "";
		$cell = "";
		$total = "";

		$numbers = getCPCData($year.$month,$site,$engine);
		if ($site =="cs" && $engine == "google")
		{
			$extra =  getCPCData($year.$month,"csq",$engine);
			if (count($numbers) != 0)
			{
				$k = array_keys($numbers);
				$v = array_keys($numbers[$k[0]]);//get the sublist of keys
				foreach ($k as $key)
				{
					$numbers[$key]["cost"] += $extra[$key]["cost"];
					$numbers[$key]["clicks"] += $extra[$key]["clicks"];
					$numbers[$key]["leads"] += $extra[$key]["leads"];
					$numbers[$key]["cpc"] = number_format(($numbers[$key]["cost"] /$numbers[$key]["clicks"]),2);
					$numbers[$key]["cpl"] = number_format(($numbers[$key]["cost"] /$numbers[$key]["leads"]),2);					
				}
			}	
		}

		if ($site == "tm" && $engine == "google")
		{		
			$extra =  getCPCData($year.$month,"tm-local",$engine);
			if (count($numbers) != 0)
			{
				$k = array_keys($numbers);
				$v = array_keys($numbers[$k[0]]);//get the sublist of keys
				foreach ($k as $key)
				{
					$numbers[$key]["cost"] += $extra[$key]["cost"];
					$numbers[$key]["clicks"] += $extra[$key]["clicks"];					
					$numbers[$key]["cpc"] = number_format(($numbers[$key]["cost"] /$numbers[$key]["clicks"]),2);
					$numbers[$key]["cpl"] = number_format(($numbers[$key]["cost"] /$numbers[$key]["leads"]),2);					
				}
			}	
		}
		
		if ($site == "tm")
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		
		echo "<table border='1' class='reportingtext' >";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
		{
			echo "<td $cell align='right' ";
			
			if (strlen($numbers[$i]["cost"]) > 0)
			{
				//echo " title='Cost: ".$numbers[$i]["cost"]." <Br/>Clicks: ".$numbers[$i]["clicks"]." ' ";
				$cost = $numbers[$i]["cost"];
				$clicks = $numbers[$i]["clicks"];
				$leads = $numbers[$i]["leads"];
				$cpc = $numbers[$i]["cpc"];
				$cpl = $numbers[$i]["cpl"];
				$day = intval($month)."/$i/$year";
				echo " onmouseover=\"moveData(event,'".$day."','$cost',$clicks,$leads,'$cpc','$cpl');\" onmouseout='hide();' ";
			}
			else if ($i != date("j") && $engine != "overture")
			{
				if ($i < 10)
					$day = "0$i";
				else 
					$day = $i;
				echo " onclick=\"getData('".$site."','".$day."','".$month."','google');\" title='Click to Gather Data' ";
			}

			echo " >";
			echo "$i</td>";
			
		}
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
		
	function print_month_header($month="*",$year="*", $cat_id="1")
	{	
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$ndays = getNDays($month,$year);
	
		if ($cat_id == 2 || $cat_id == 3)
		{
			$name = " width='125' ";
			$cell = " width='25' ";
			$total = " width='30' ";
		}
		else
		{
			$name = "";
			$cell = "";
			$total = "";		
		}
	
		echo "<table border='1' class='reportingtext'>";
		echo "<tr><td $name align='left' >Company</td>";
		for ($i = 1; $i <= $ndays; $i++)
			echo "<td $cell align='right'><strong>$i</strong></td>";
		echo "<td $total align='right'><strong>Total</strong></td>";
		echo "</tr>";
		//echo "</table>";
	}
	
	function getIReloSourceString($referer="all", $campaign="all")
	{
		$source_string = " ";
		if ($campaign == "all")
		{
			if ($referer != "all")//no filter.
				$source_string = " and referer like '$referer%'";
		}
		else
		{
			if ($campaign == "org")//organic leads only...
			{
				if ($referer != "all")//then pm or tm
					$source_string = " and (referer like '$referer%search%' or referer = '$referer') ";
				else//all organics?? nasty.. 
					$source_string = " and (referer like '%search%' or referer = 'pm' or referer = 'tm') ";			
			}
			else
			{
				if ($referer != "all")//then pm or tm
					$source_string = " and referer like '$referer%$campaign%' and referer not like '%search%' ";
				else
					$source_string = " and referer like '%$campaign%' and referer not like '%search%' ";						
			}
		}
		return $source_string;
	}
	
	function print_irelo_data($site_code,$campaign,$referer,$comp_name, $month="*", $year="*")
	{
		if ($referer != -1)
			$source_code = getiReloSourceString($referer, $campaign);
		else
			$source_code = "";
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		$table = "irelocation.$site_code";
		/*
		$comp_name = "TopMoving.com";
		if ($referer == "pm")
			$referer = "ProMoving.com";
		*/
		$query = getNumbersForMonth($source_code,-1,$month,$year,$table);
		echo "<!--- Site Code: ".substr($site_code,18)." --->";
		
		print_it(substr($table,18),$campaign,$referer, $query,$comp_name,$month,$year);
	}
	
	///if lead_id != -1, then comp_name is a company name.
	//otherwise, comp_name, is a site name.
	function print_company_data($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
	{
		$source_code = getSourceString($site_code, $campaign);
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($lead_id != -1)
			$lead_string = " and lead_ids like '%".$lead_id."%' ";
		else
			$lead_string = -1;
			
		$query = getNumbersForMonth($source_code,$lead_string,$month,$year);
		echo "<!--- $site_code --->";
		echo "<!--- $query --->";
		print_it($site_code,$campaign,$lead_id, $query,$comp_name,$month,$year);
	}
	
	function print_international_company_data($site_code,$campaign, $lead_id,$comp_name, $month="*", $year="*")
	{
		$source_code = getSourceString($site_code, $campaign);
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($lead_id != -1)
			$lead_string = " and lead_ids like '%".$lead_id."%' "." and cat_id = 3 ";
		else
			$lead_string = " and cat_id = 3 ";
			
		$query = getNumbersForMonth($source_code,$lead_string,$month,$year);
		
		print_it($site_code,$campaign,$lead_id, $query,$comp_name,$month,$year,1);
	}
	
	
	function gatherLosses($yearmonth, $lead_id)
	{
		if ($lead_id == 1386)
			$table = "movewheaton";
		else if ($lead_id == 1388)
			$table = "movebekins";
			
		$sql = "select substring(received,7,2) 'day', count(received) 'count'  from movingdirectory.$table where received like '$yearmonth%' and record_key = -1 group by substring(received,1,8) WITH ROLLUP;";
		$rs = new mysql_recordset($sql);
		$results = array();
		$last = -1;
		while ($rs->fetch_array())
		{
			if ($last == $rs->myarray["day"])//should be last row.
			{
				$results["total"] = $rs->myarray["count"];
				if ($results["total"] > 0)
					echo "<!--- Loss found... --->";
				return $results;
			}
			else
			{
				$last = $rs->myarray["day"];
				$results[$last] = $rs->myarray["count"];
				
			}
		}
		return $results;
	}

	function getSourceString($site_code, $campaign="all")
	{
	 	echo "<!--- $site_code $campaign --->";

	 	if ($campaign == "all")
		{
		/*	csq will still get matched...
			if($site_code == "cs")//add csq source...
				return " and (source like 'cs%' or source like 'csq%') ";
			else
		*/
			return " and source like '$site_code%' ";
		}
		else if ($campaign == "org")//organic
		{			
			echo "<!-- organic leads. -->";
			switch($site_code)
			{
				case "cs":
					return " and source like 'cs%' and source not like 'cs_overture%' and source not like 'cs_msn%' and source not like 'csq_google%' and source not like 'cs_superpages%' and source not like 'cs_google%' and source not like 'cs_tas%' ";
				case "pm":					
					return " and source like 'pm%' and source not like 'pm_overture%' and source not like 'pm_msn%' and source not like 'pm_google%' and source not like 'pm_boxdev%' and source not like 'pm_silver%' and source not like 'pm_superpages%' and source not like 'pm_tmc%' and source not like 'pm_ask%' ";
				case "atus":					
					return " and source like 'atus%' and source not like 'atus_overture%' and source not like 'atus_msn%' and source not like 'atus_google%' and source not like 'atus_google%' and source not like 'atus_google%' and source not like 'atus_123%'";
				case "mmc":					
					return " and source like 'mmc%' and source not like 'mmc_overture%' and source not like 'mmc_msn%' and source not like 'mmc_google%' and source not like 'mmc_google%' and source not like 'mmc_google%' and source not like 'mmc_pas%' ";
				case "tm":					
					return " and source like 'tm%' and source not like 'tm_overture%' and source not like 'tm_msn%' and source not like 'tm_google%' and source not like 'tm_superpages%' and source not like 'tm_tmc%' and source not like 'tm_ask%' ";
				case "me":					
					return " and source like 'me%' and source not like 'me_overture%' and source not like 'me_msn%' and source not like 'me_google%' and source not like 'me_silver%' and source not like 'me_superpages%' and source not like 'me_ask%' ";					
			}			
		}
		else
		{			
			if ($site_code == "cs")
			{
				$result = "and (source like 'cs_".$campaign."%' or source like 'csq_".$campaign."%' ) ";			
			}
			else
			{				
				$result = " and source like '$site_code";
				$result .= "_";
				$result .= "$campaign%' ";
			}			
			return $result;
		}	 	
	 }
	 
	 function print_tmc_data($site , $month="*", $year="*")
	 {
		if ($year == "*") $year = date("Y");
		if ($month == "*") $month = date("m");
		
		if ($site == -1)
		{
			$comp_name = "TopMovingCompanies";
			$sourcestring = " and (source = 'pm_tmc' or source like 'tm_tmc') ";
		}
		else
		{
			$sourcestring = " and source = '".$site."_tmc'";
			if ($site == "tm")
				$comp_name = "TopMoving";
			else
				$comp_name = "ProMoving";
		}
		
	 	$sql = "select substring(received,7,2) 'day', count(received) 'count' from movingdirectory.quotes where received like '$year$month%' $sourcestring and lead_ids != '' group by substring(received,1,8) WITH ROLLUP;";
		echo "<!--- $sql --->";
	 	print_it($site,"tmc","",$sql,$comp_name,$month,$year);
	 }
?>