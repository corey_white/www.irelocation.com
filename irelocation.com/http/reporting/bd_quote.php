<?php 
	if ($_REQUEST["info"] == "irelocation")
	{
		phpinfo();
		exit;	
	}
?>
<script language=javascript>
<!--
function validate(form)
{
  if (form.fname.value == "")
  {
    alert("Please enter your first name");
    form.fname.focus()
    return false;
  }
  if (form.lname.value == "")
  {
    alert("Please enter your last name");
    form.lname.focus()
    return false;
  }
  var theStr = new String(form.email.value)
  var index = theStr.indexOf("@");
  if (index > 0)
  {
    var pindex = theStr.indexOf(".",index);
    if ((pindex > index+1) && (theStr.length > pindex+1))
    {
      //pass
    }
    else
    {
      alert("Please enter a valid email address");
      form.email.focus()
      return false;
    }
  }
  else
  {
    alert("Please enter a valid email address");
    form.email.focus()
    return false;
  }
  if (form.phone.value == "")
  {
    alert("Please enter a phone number");
    form.phone.focus()
    return false;
  }
  
  var GoodChars = "0123456789"
  var i = 0
  var TempPhone = ""
  var LastChar = ""
  var count = 0
  for (i =0; i <= form.phone.value.length -1; i++) {
    if (GoodChars.indexOf(form.phone.value.charAt(i)) != -1)
    {          
      if((TempPhone == "") && ((form.phone.value.charAt(i) == "1") || (form.phone.value.charAt(i) == "0")))
      {
      	//get rid of beginning 1 or 0
      }
      else
      {

        if(form.phone.value.charAt(i)==LastChar)
        {
          count=count+1;
          if(count==10)
          {
            alert("Please enter a valid 10-digit phone number");
            form.phone.focus()
            return false;
          }
        }        
        else
        {
          count=0;
        }

        if(LastChar=="")
        {
          count=1;
        }

        TempPhone+=form.phone.value.charAt(i);
      }
      LastChar=form.phone.value.charAt(i);      
    }
  }
  
  form.phone.value=TempPhone  
  
  if(form.phone.value.match(/^[ ]*[(]{0,1}[ ]*[0-9]{3,3}[ ]*[)]{0,1}[-]{0,1}[ ]*[0-9]{3,3}[ ]*[-]{0,1}[ ]*[0-9]{4,4}[ ]*$/)==null)
  {
    alert("Please enter a valid 10-digit phone number");
    form.phone.focus()
    return false; 
  }
  
  if (form.origin_zip.value == "")
  {
    alert("Please enter your origin zip code");
    form.origin_zip.focus()
    return false;
  }

  var today = '<?echo date("Y");?>' + '<?echo date("m");?>' + '<?echo date("d");?>';
  var move_date = form.move_year.value + form.move_month.value + form.move_day.value;  
 //alert(move_date);
  if (today>=move_date)
  {
    alert("Please enter a future move date");
    form.move_month.focus()
    return false;
  }    

  if (form.destination_city.value == "")
  {
    alert("Please enter your destination city");
    form.destination_city.focus()
    return false;
  }
  if (form.destination_city.value.match(/^\d{1,}$/)!=null)
  {
    alert("Please enter a valid destination city");
    form.destination_city.focus()
    return false;
  }
  if (form.destination_state.value == "XX")
  {
    alert("Please select your destination state");
    form.destination_state.focus()
    return false;
  }
  if (form.bedrooms.value == "")
  {
    alert("Please select the number of furnished bedrooms for your HOUSEHOLD GOODS MOVING estimate.");
    form.bedrooms.focus()
    return false;
  }
}
// -->
</script>
                <FORM name="quoteform"  action="bd_quote.php" method="POST" id="test form" onSubmit="return validate(this)">
  <p align="center"><br>
    <input type="hidden" name="source" value="pm_boxdev">
    <input type="hidden" name="destination_country" value="usa">
	<input type="hidden" name="send" value="1">
  </p>
          
  <table border="0" cellspacing="1" width="100%">
    <tr> 
      <td width="100%" colspan="3"><b><font face="Verdana" size="2" color="#4C4C4C">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        Please enter your contact information:</font></b></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">First Name:</font></td>
      <td width="61%"><font face="Verdana" color="#4e4e4e" size="1"> 
        <input name="fname" size="20">
        </font></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">Last Name:</font></td>
      <td width="61%"><font face="Verdana" color="#4e4e4e" size="1"> 
        <input name="lname" size="20">
        </font></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">E-Mail Address:</font></td>
      <td width="61%"><font face="Verdana" color="#4e4e4e" size="1"> 
        <input name="email" size="32">
        </font></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">Phone Number:</font></td>
      <td width="61%"><font face="Verdana" color="#4e4e4e" size="1"> 
        <input name="phone" size="20">
        </font></td>
    </tr>
    <input type="hidden" name="contact" value="phone">
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C"> Moving <b>From:</b></font></td>
      <td width="61%"><font size="1" color="#4C4C4C"><font face="Verdana">City</font></font> 
        <input size="9" name="origin_city"> <select size="1" name="origin_state">
          <option value="XX">- Please Select -</option>
          <option value="AL">Alabama</option>
          <option value="AK">Alaska</option>
          <option value="AZ">Arizona</option>
          <option value="AR">Arkansas</option>
          <option value="CA">California</option>
          <option value="CO">Colorado</option>
          <option value="CT">Connecticut</option>
          <option value="DC">DC</option>
          <option value="DE">Delaware</option>
          <option value="FL">Florida</option>
          <option value="GA">Georgia</option>
          <option value="HI">Hawaii</option>
          <option value="ID">Idaho</option>
          <option value="IL">Illinois</option>
          <option value="IN">Indiana</option>
          <option value="IA">Iowa</option>
          <option value="KS">Kansas</option>
          <option value="KY">Kentucky</option>
          <option value="LA">Louisiana</option>
          <option value="ME">Maine</option>
          <option value="MD">Maryland</option>
          <option value="MA">Massachusetts</option>
          <option value="MI">Michigan</option>
          <option value="MN">Minnesota</option>
          <option value="MS">Mississippi</option>
          <option value="MO">Missouri</option>
          <option value="MT">Montana</option>
          <option value="NE">Nebraska</option>
          <option value="NV">Nevada</option>
          <option value="NH">New Hampshire</option>
          <option value="NJ">New Jersey</option>
          <option value="NM">New Mexico</option>
          <option value="NY">New York</option>
          <option value="NC">North Carolina</option>
          <option value="ND">North Dakota</option>
          <option value="OH">Ohio</option>
          <option value="OK">Oklahoma</option>
          <option value="OR">Oregon</option>
          <option value="PA">Pennsylvania</option>
          <option value="RI">Rhode Island</option>
          <option value="SC">South Carolina</option>
          <option value="SD">South Dakota</option>
          <option value="TN">Tennessee</option>
          <option value="TX">Texas</option>
          <option value="UT">Utah</option>
          <option value="VT">Vermont</option>
          <option value="VA">Virginia</option>
          <option value="WA">Washington</option>
          <option value="WV">West Virginia</option>
          <option value="WI">Wisconsin</option>
          <option value="WY">Wyoming</option>
        </select> <font size="1" color="#4C4C4C">&nbsp; </font></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">&nbsp; </font></td>
      <td width="61%"><font size="1" color="#4C4C4C"> <font face="Verdana">Zip 
        Code</font></font> <input size="9" name="origin_zip"  maxlength="5" ></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C">Estimated 
        Move Date:</font></td>
      <td width="61%"> <select name="move_month">
          <option value="01">Jan</option>
          <option value="02">Feb</option>
          <option value="03">Mar</option>
          <option value="04">Apr</option>
          <option value="05">May</option>
          <option value="06">Jun</option>
          <option value="07">Jul</option>
          <option value="08">Aug</option>
          <option value="09">Sep</option>
          <option value="10">Oct</option>
          <option value="11">Nov</option>
          <option value="12">Dec</option>
        </select> <select name="move_day" id="move_day">
          <option value="01">1</option>
          <option value="02">2</option>
          <option value="03">3</option>
          <option value="04">4</option>
          <option value="05">5</option>
          <option value="06">6</option>
          <option value="07">7</option>
          <option value="08">8</option>
          <option value="09">9</option>
          <option value="10">10</option>
          <option value="11">11</option>
          <option value="12">12</option>
          <option value="13">13</option>
          <option value="14">14</option>
          <option value="15">15</option>
          <option value="16">16</option>
          <option value="17">17</option>
          <option value="18">18</option>
          <option value="19">19</option>
          <option value="20">20</option>
          <option value="21">21</option>
          <option value="22">22</option>
          <option value="23">23</option>
          <option value="24">24</option>
          <option value="25">25</option>
          <option value="26">26</option>
          <option value="27">27</option>
          <option value="28">28</option>
          <option value="29">29</option>
          <option value="30">30</option>
          <option value="31">31</option>
        </select> <select name="move_year" id="move_year">
          <option value="2006">2006</option>
          <option value="2007">2007</option>
          <option value="2008">2008</option>
          <option value="2009">2009</option>
        </select> </td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font face="Verdana" size="1" color="#4C4C4C"> Moving <b>To:</b></font></td>
      <td width="61%"><font size="1" color="#4C4C4C"> <font face="Verdana">City</font></font> 
        <input size="9" name="destination_city"> <select size="1" name="destination_state">
          <option value="XX">- Please Select -</option>
          <option value="AL">Alabama</option>
          <option value="AK">Alaska</option>
          <option value="AZ">Arizona</option>
          <option value="AR">Arkansas</option>
          <option value="CA">California</option>
          <option value="CO">Colorado</option>
          <option value="CT">Connecticut</option>
          <option value="DC">DC</option>
          <option value="DE">Delaware</option>
          <option value="FL">Florida</option>
          <option value="GA">Georgia</option>
          <option value="HI">Hawaii</option>
          <option value="ID">Idaho</option>
          <option value="IL">Illinois</option>
          <option value="IN">Indiana</option>
          <option value="IA">Iowa</option>
          <option value="KS">Kansas</option>
          <option value="KY">Kentucky</option>
          <option value="LA">Louisiana</option>
          <option value="ME">Maine</option>
          <option value="MD">Maryland</option>
          <option value="MA">Massachusetts</option>
          <option value="MI">Michigan</option>
          <option value="MN">Minnesota</option>
          <option value="MS">Mississippi</option>
          <option value="MO">Missouri</option>
          <option value="MT">Montana</option>
          <option value="NE">Nebraska</option>
          <option value="NV">Nevada</option>
          <option value="NH">New Hampshire</option>
          <option value="NJ">New Jersey</option>
          <option value="NM">New Mexico</option>
          <option value="NY">New York</option>
          <option value="NC">North Carolina</option>
          <option value="ND">North Dakota</option>
          <option value="OH">Ohio</option>
          <option value="OK">Oklahoma</option>
          <option value="OR">Oregon</option>
          <option value="PA">Pennsylvania</option>
          <option value="RI">Rhode Island</option>
          <option value="SC">South Carolina</option>
          <option value="SD">South Dakota</option>
          <option value="TN">Tennessee</option>
          <option value="TX">Texas</option>
          <option value="UT">Utah</option>
          <option value="VT">Vermont</option>
          <option value="VA">Virginia</option>
          <option value="WA">Washington</option>
          <option value="WV">West Virginia</option>
          <option value="WI">Wisconsin</option>
          <option value="WY">Wyoming</option>
        </select></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font size="1" color="#4C4C4C"> <font face="Verdana">Type 
        of Home:</font></font></td>
      <td width="61%"><select size="1" name="type_home">
          <option value="House">House</option>
          <option value="Apartment">Apartment</option>
          <option value="Condo">Condo</option>
          <option value="Storage">Storage</option>
          <option value="Other">Other</option>
        </select></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%"><font size="1" color="#4C4C4C"> <font face="Verdana">Rooms:</font></font></td>
      <td width="61%"> <table border="0" cellspacing="0" width="100%" cellpadding="0">
          <tr> 
            <td width="36%"><select name="weight">
                <option value="No bedroom Studio,2000lbs">No bedroom Studio,2000lbs</option>
                <option value="One Bedroom,2940lbs">One Bedroom,2940lbs</option>
                <option value="Two Bedrooms,4550lbs">Two Bedrooms,4550lbs</option>
                <option value="Three Bedrooms,7700lbs">Three Bedrooms,7700lbs</option>
                <option value="Four Bedrooms,9800lbs">Four Bedrooms,9800lbs</option>
                <option value="Five Bedrooms,Over 11200lbs">Five Bedrooms,Over 
                11200lbs</option>
                <option value="Six Bedrooms, Over 11200lbs">Six Bedrooms, Over 
                11200lbs</option>
              </select></td>
            <td width="64%">&nbsp; </td>
          </tr>
        </table></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%" valign="top"><font face="Verdana" size="1" color="#4C4C4C"> 
        Please add any special comments and/or describe any unique items:</font></td>
      <td width="61%"><textarea rows="7" name="moving_comments" cols="44" ></textarea></td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="61%">&nbsp;</td>
    </tr>
    <tr> 
      <td width="100%">&nbsp;</td>
      <td width="25%">&nbsp;</td>
      <td width="61%"> <input type="submit" value="Submit Quote Request" name="submit"></td>
    </tr>
  </table>
          <p align="center">&nbsp;</p>
          </form>

<?

/*
	if your server automatically extracts the variables from
	_POST and _REQUEST and so on, comment the extract($_POST) line below
*/

if ($_POST["send"] =='1') {
	extract($_POST);	
	
	include("gurl.php");
	$cururl = new gURL;

	$arypostdata["name"]=$fname." ".$lname;
	$arypostdata["email"]=$email;

	$arypostdata["phone1"]=$phone;
	$arypostdata["phone2"]=$phone;

	$arypostdata["from_city"]=$origin_city;
	$arypostdata["from_state"]=$origin_state;
	$arypostdata["from_zip"]=$origin_zip;

	$arypostdata["to_city"]=$destination_city;
	$arypostdata["to_state"]=$destination_state;
	$arypostdata["to_zip"]=$destination_zip;

	$arypostdata["to_country"]=$destination_country;

	$arypostdata["type_move"]=$type_home;

	$arypostdata["weight"]=$weight;

	$arypostdata["avail_date"]=$move_month."/".$move_day."/".$move_year;

	$arypostdata["notes"]=$moving_comments;
	
	$arypostdata["source"]=$source;

	$mydata = $arypostdata;

	$webresult = $cururl->execweb("www.1stmovingdirectory.com", "/leads_ext_post.php", $mydata,"POST");
	$send ='2';
}
?>