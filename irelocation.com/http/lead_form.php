<?
session_start();

// Setup Error messages

$error_msg = $_REQUEST["msg"];
$msgs["name"] = "Please enter your Full Name.";
$msgs["phone"] = "Please enter your Phone Number.";
$msgs["company"] = "Please enter your Company Name.";
$msgs["email"] = "Please enter your Email Address.";
$msgs["lead_type"] = "Please select your type of lead.";
$msgs["thankyou"] = "Thank you. We will contact you shortly.";
$msgs["verify"] = "Verification code is invalid.";
			
$msg = $msgs[$error_msg];

extract($_POST);


// is the form ready to check and send            
if ($send == "yes") { 
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$output = strtolower(print_r($_POST,true));
		
		if ($type == "javascript")
		{
			if (substr_count($output,"<script") > 0)
				$content = substr($output,strpos($output,"<script"));						
			else
				$content = substr($output,strpos($output,"onload="));
		}
		else if ($type == "link")
			$content = substr($output,strpos($output,"href="));	
		else if  ($type == "serverside")
		{
			if (substr_count($output,"<?") > 0)
				$content = substr($output,strpos($output,"<?"));		
			else
				$content = substr($output,strpos($output,"<%"));		
		}
		else if ($type == "dirty")
		{
			$content = "Dirty words.";
			header("Location: contact.php?msg=thankyou");
			exit;
		}	
		$content = str_replace("'","&prime;",$content);
		$content = str_replace("\"","&quot;",$content);
		
		$sql = "insert into marble.injectors (site,ip,received,content) values ".
				"('tm','$ip','".date("YmdHis")."','$content'); ";
				
		$rs = new mysql_recordset($sql);
		
		header("Location: contact.php?msg=thankyou");
		exit;
	}

	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");		
	}		
	testInjection();//if it gets past this the input 'should' be ok.
 
	// error checking
	if ($name == "") {
		header("Location: lead_form.php?msg=name&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($email == "") {
		header("Location: lead_form.php?msg=email&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($company == "") {
		header("Location: lead_form.php?msg=company&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($phone == "") {
		header("Location: lead_form.php?msg=phone&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($lead_type == "") {
		header("Location: lead_form.php?msg=lead_type&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
		
	} else if ($_SESSION['answer'] != $verify) {
        header("Location: lead_form.php?msg=verify&send=no&name=$name&company=$company&phone=$phone&email=$email&lead_type=$lead_type&comment=$comment");
        
    } else {
		//passed error checking
 		// to email address
		$to = "katrina@irelocation.com, travis@irelocation.com, vsmith@irelocation.com, markb@irelocation.com";
		// The subject
		$subject = "iRelocation PPC Lead Form";
		// The message
		$message = "Name: ".$name."\n\n";
		$message .= "Company: ".$company."\n\n";
		$message .= "Email: ".$email."\n\n";
		$message .= "Phone: ".$phone."\n\n";
		$message .= "Lead Type: ".$lead_type."\n\n";
		$message .= "Comment: ".$comment;
		mail($to, $subject, $message, "From: $email");
		header("Location: thank_you.php");
		exit;
		
	}
	
} else {

//create numbers for human verification
$q1 = rand(0,10);    
$q2 = rand(0,10);
$_SESSION['answer'] = $q1 + $q2;
    
?>         
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>iRelo | Customized Lead Generation Since 1998</title>
<style type="text/css">
<!--
.style2 {color: #fe905c}
-->
</style>
<!--[if lt IE 7.]>
<script defer type="text/javascript" src="http://www.irelocation.com/Scripts/pngfix.js"></script>
<![endif]-->

<link href="irelocation/leadform.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style3 {font-style: italic}
-->
</style>
</head>

<body>
 <!--wrapper-->
<div id="wrapper">
<!--header-->
<div id="header">
  <div id="logo"><img src="irelocation/images/irelo.png" alt="Irelo" width="162" height="75" /></div>
  <div id="tagline"><span class="style3"><strong>CALL US TODAY:</strong>&nbsp; <strong>1 (888) 571-6674</strong></span></div>
</div>
<!--end header-->
<!--content-->
<div id="content">
<div id="left"><h1>Just say &quot;no&quot; to cold calling...<br />
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and <em>yes</em> to <span class="style2">qualified leads</span>.</h1>
 
 <div id="bullets">
 <ul id="navlist">
   <li>&nbsp;&nbsp;&nbsp; Do you want to increase your ROI?<br />
     <br />
 </li>

<li>&nbsp;&nbsp;&nbsp; Do you want to accelerate your growth
and boost sales overnight?<br />
<br />
</li> 

<li>&nbsp;&nbsp;&nbsp; Are you looking for targeted leads in the 
moving, auto transport, security,  
insurance, solar, medical alert, senior care or other industries?<br />
<br />
</li>
</ul>
If you answered <strong>YES</strong>, then you are only a 
<br />
click away from receiving a lead campaign
that produces results. </div>
</div>

<div id="right">


 
 <div id="form">
<h2>I want to learn more about a 
lead generation campaign.</h2>
 
  <form id="form1" method="post" action="lead_form.php">
  <input name="send" type="hidden" value="yes" />
    <table width="300" height="330" border="0" cellpadding="1" cellspacing="0">
    <?
      	$errmsg='
      	<tr>
      	  <td align="center" colspan="2"><font color="red">'.$msg.'</font></td>
      	</tr>
		';
      	if($msg!="")
      	{
      		echo $errmsg;
      	}
    ?>
      <tr>
        <td width="51%"><p>Name: <span class="orange">* </span></p></td>
        <td width="49%"><label>
        <input name="name" type="text" id="name" value="<? echo $_REQUEST["name"]; ?>" />
        </label></td>
      </tr>
      <tr>
        <td><p>Company: <span class="orange">*</span> </p></td>
        <td><input name="company" type="text" id="company" value="<? echo $_REQUEST["company"]; ?>" /></td>
      </tr>
      <tr>
        <td><p>Email: <span class="orange">*</span> </p></td>
        <td><input name="email" type="text" id="email" value="<? echo $_REQUEST["email"]; ?>" />          </td>
      </tr>
      <tr>
        <td><p>Phone: <span class="orange">*</span> </p></td>
        <td><input name="phone" type="text" id="phone" value="<? echo $_REQUEST["phone"]; ?>" /></td>
      </tr>
      <tr>
        <td><p>Type of Leads: </p></td>
        <td><label>
          <select name="lead_type">
            <option value="Select" <? if($lead_type=='Select'){echo " selected";}?>>Select</option>
            <option value="Moving" <? if($lead_type=='Moving'){echo " selected";}?>>Moving</option>
            <option value="Car Transport" <? if($lead_type=='Car Transport'){echo " selected";}?>>Car Transport</option>
            <option value="Home Security" <? if($lead_type=='Home Security'){echo " selected";}?>>Home Security</option>
            <option value="Insurance" <? if($lead_type=='Insurance'){echo " selected";}?>>Insurance</option>
			<option value="Medical Alert" <? if($lead_type=='Medical Alert'){echo " selected";}?>>Medical Alert</option>
			<option value="Solar" <? if($lead_type=='Solar'){echo " selected";}?>>Solar</option>
			<option value="Home Senior Care" <? if($lead_type=='Home Senior Care'){echo " selected";}?>>Home Senior Care</option>
            <option value="Other" <? if($lead_type=='Other'){echo " selected";}?>>Other</option>
          </select>
        </label></td>
      </tr>
      <tr>
        <td valign="top"><p>Comments:</p></td>
        <td valign="top"><label>
          <textarea name="comment" rows="5" style="height: 40px;"><? echo $_REQUEST["comment"]; ?></textarea>
        </label></td>
      </tr>
      <tr>
        <td valign="top"><p>Human Verification:</p></td>
        <td valign="top"><span style="color: #EFA163;">What is <?=$q1 ?> + <?=$q2 ?>?</span> <input name="verify" type="text" id="verify" value="" style="width: 25px;" /></td>
      </tr>
      <tr>
        <div align="center"><td colspan="2" valign="bottom"><input name="submit" type="image" src="irelocation/images/contact_btn2.png" width="221" height="66" style="margin-left:65px;" /></div></td>
        </tr>
    </table>
  
  </form></div>
  

  
  
  </div>
</div>
<div id="bottom">
<div id="bottom1"><h1 align="center">Moving and Auto</h1> 
<h2 align="center">Moving and Car Transport Companies</h2>
<p align="center">Are you a moving or auto transport company? 
We are one of the largest resources of leads 
in the industry today. Receive a steady stream 
of quality leads year round and grow your 
moving or transport business.</p>
<h3 align="center"><strong>GET MOVING OR AUTO LEADS NOW ! </strong></h3>
</div>
<div id="bottom2"><h1 align="center">security systems</h1> 
<h2 align="center">Home Alarm System Companies</h2>
<p align="center">Our fresh home security leads will help you
reach consumers who are specifically
looking to protect their family with a home alarm system. Increase
your revenue overnight in one of the
hottest industries today. </p>
<h3 align="center"><strong>GET HOME SECURITY LEADS NOW! </strong></h3>
<p align="center"><br />
</p>
</div>
<div id="bottom3">
<?php /*
<h1 align="center">insurance</h1>
<h2 align="center">Home, Auto and Life Insurance Companies</h2>
<p align="center">Grow your client base with our quality 
insurance leads for home, auto and life.  
Our process ensures that you will only 
receive leads from consumers highly 
interested in purchasing an insurance policy.</p>
<h3 align="center"><strong>GET INSURANCE LEADS NOW! </strong></h3>
 */
 ?>
 <h1 align="center">MEDICAL</h1> 
  <h2 align="center">Medical Alert Companies </h2>
  <p align="center">Are you a medical alert company? Stay ahead of the competition with our top medical alert leads.&nbsp; We leverage Internet technologies to connect you with consumers who are ready to buy a medical alert system. </p>
  <h3 align="center"><strong>GET MEDICAL ALERT LEADS NOW ! </strong></h3>
  <p align="center">&nbsp;</p>
<p align="center"><br />
</p> 
</div>


</div>
<?php /*
<div id="bottoma">
<div id="bottom1a">
  <h1 align="center">SOLAR PANELS </h1> 
  <h2 align="center">Solar Energy Companies</h2>
  <p align="center">Increase your solar energy business sales <br />
    with our targeted solar leads.&nbsp; Our program delivers qualified prospects who are ready to conserve energy, save money and go green at home.</p>
  <h3 align="center"><strong>GET SOLAR LEADS NOW ! </strong></h3>
  <p align="center">&nbsp;</p>
</div>
<div id="bottom2a">
  <h1 align="center">MEDICAL</h1> 
  <h2 align="center">Medical Alert Companies </h2>
  <p align="center">Are you a medical alert company? Stay ahead of the competition with our top medical alert leads.&nbsp; We leverage Internet technologies to connect you with consumers who are ready to buy a medical alert system. </p>
  <h3 align="center"><strong>GET MEDICAL ALERT LEADS NOW ! </strong></h3>
  <p align="center">&nbsp;</p>
<p align="center"><br />
</p>
</div>
<div id="bottom3a">
  <h1 align="center">SENIOR CARE </h1>
  <h2 align="center">Home Senior Care Companies </h2>
  <p align="center">From home care services  to senior housing options and medical products, the senior care industry is growing rapidly.&nbsp; Get quality senior care leads in real-time and accelerate your sales immediately.</p>
  <h3 align="center"><strong>GET HOME SENIOR CARE LEADS NOW ! </strong></h3>
  <p align="center">&nbsp;</p>
</div>
</div>
 */
?>
</div>
 <!--end wrapper-->
 <!--Footer-->
<div id="footer">
  <div align="center">
    <p><br />
    Copyright 2010.&nbsp; i-Relocation Network. <a href="http://www.irelocation.com">www.irelocation.com</a> </p>
  </div>
</div>
<!--end footer-->
</body>
</html>
  <?php }  ?>
