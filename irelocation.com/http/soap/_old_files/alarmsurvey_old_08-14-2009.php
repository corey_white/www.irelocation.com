<?
/* 
************************FILE INFORMATION**********************
* File Name:  alarmsurvey.php  
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  
**********************************************************************
* Modifications (Date/Who/Details):
8/12/09 17:00 PM - Rob - updated an existing file to this file for the alarm survey 
										
**********************************************************************
NOTES: this is modified from an already modified script, so there are errant artifacts in here
*/



	include_once "../inc_mysql.php";
	//include "common.php";

	//display lots of messages?
	define(DEBUG,true);

	$survey_format['survey_keys'] = array( "company" => true,"rate_installation" => true, "stars" => true,"comments" => false);
	$survey_format['extra_keys'] = array( "source" => true, "referer" => true, "remote_ip" => true);
	
	
	/*
		Sample:
	
		house:
			$house = array( "bedrooms" => "5", "extras" => "attic,playroom,patio", 
							  "type" => "house", stairs => "yes", elevator => "no");
		LocationStruct:
			$from = array( "zip" => 85282, "city" => "Tempe", "state" => "AZ");
			$to = array( "zip" => 08260, "city" => "Wildwood", "state" => "NJ");
		
		Customer:
			$customer = array( "fname" => "Dave", "lname" => "Haveman", 
								"phone1" => "4807857400", "email" => "rob@irelocation.com",
								"contact" => "email", "est_move_date" => "YYYY-MM-DD" );
				
		Internal:
			$internal = array( "source" => "me", "referer" => "www.google.com",
								"remote_ip" => "63.229.84.218", "campaign" => "tm",
								"aff" => "false");
	*/
	
	/*
		function to recieve Auto Leads via SOAP

		inputs:
			$vehicle:	assoc array of vehicle details
			$from/$to:	assoc array of location information.
			$customer: 	assoc array of customer info.
			$internal: 	internal information, source, referer, ip	
		returns:
			$result[<success>] Success or Error
			$result[<code>] quote_id upon success, error code upon failure
			$result[<description>] text description of the error, blank if successful.
	*/	

	function SurveyResponse($survey,$extra) 
	{
		global $survey_format;
		
		$keys = array_keys($survey_format);
		

		foreach($keys as $k)
		{
			$format_array_name = substr($k,0,-6);
			
			echo "Array: $format_array_name <Br/>";
			mail("rob@irelocation.com","ALARMSURVEY.PHP","Array: $format_array_name");

			$format_array = $survey_format[$k];

			echo "Array: ".print_r($format_array,true);
			
			$param_array = $$format_array_name;
			$sub_keys = array_keys($format_array);		
			
			foreach($sub_keys  as $k)
			{											
				if($format_array[$k] && $param_array[$k] == "")
					return array("result" => "Error", 
								 "code" => $k, 
								 "description" => fail($format_array_name,$k));													
			}						
		}
		
		//print_r($to);
		$now_time = date("YmdHis");
		
		extract($survey);		
		extract($extra);
		
		//timestamp
		$received = date("Ymdhis");
				
		
		$sql = "insert into irelocation.survey_alarm set company = '$survey[company]', rate_installation = '$survey[rate_installation]', stars = '$survey[stars]', comments = '$survey[comments]', source = '$extra[source]', referer = '$extra[referer]', remote_ip = '$extra[remote_ip]', received = now() )";
		
		//get quote_id, 
		$rs = new mysql_recordset($sql);
		$quote_id = $rs->last_insert_id();
		
		
		mail("rob@irelocation.com","SOAP SURVEY DEBUG - SUBMIT",$sql."\n\n".$quote_id);
		
		//valid lead, return success.
		$results = array("result" => "Success", "code" => $quote_id, "description" => "The lead was accepted.");
		
		mail("rob@irelocation.com","SOAP - SURVEY - RESULTS",print_r($results,true));
		
		return $results;
	}
	
	
?>