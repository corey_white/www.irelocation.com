<?

	//get variables from teh movingdirectory.irelo_variables table.
	function getVariable($keys)
	{
		$keys_flat = "'".implode("','",$keys)."'";
		$count = count($keys);
			
		$sql = "select name,`value` from movingdirectory.irelo_variables where ".
				"name in ($keys_flat) limit $count";
		
		$rs = new mysql_recordset($sql);
		$results = array();
		
		while($rs->fetch_array())
		{
			extract($rs->myarray);
			$results[] = $name."=".$value;
		}
		return $results;
	}

	//format the desciption of a failure consistantly.
	function fail($group,$key)
	{
		return "Required Parameter '$key' in group '$group' is missing!";
	}
		
	/*
		validate a location array. 
		input:
			location: assoc(zip,city,state)
			country: us,ca,au..
		returns
			location: assoc(zip,city,state) if valid, or blank if not.
			
	*/
	function validateLocation($location,$country="us")
	{	
		$old = $location;
		
		if ($country == "us")
		{
			if ($location['zip'] == "")
			{
				$temp_array = findZipCode($location['city'],$location['state']);
				$location['zip'] = $temp_array['zip'];
				//mail("davidhaveman@gmail.com","SOAP DEBUG",
				//		print_r($old,true)."\n\n".print_r($location,true));
				return $location;
			}
			else if (strlen($location['state']) == 2 && strlen($location['city']) >= 3)
			{
				$location =  findZipCode($location['city'],$location['state']);
				mail("code@irelocation.com","SOAP DEBUG", print_r($old,true)."\n\n".print_r($location,true));
				return $location;
			}
			else if (strlen($location['zip']) == 5)
			{
				$location = findCityState($location['zip']);
				//mail("david@irelocation.com","SOAP DEBUG",
				//		print_r($old,true)."\n\n".print_r($location,true));
				return $location;
			}
			else
			{
				//mail("david@irelocation.com","SOAP DEBUG",
				//	print_r($location,true)."\n\nBlank!");
				return array("","","");//invalid location.			
			}
		}
	}
	/*
		validates and cleans a phone number. 
		input:
			phone: a phone number
			country: us,ca,au..
		returns
			location: a phone number if valid, or blank if not.
			
	*/	
	function validatePhone($phone, $country = "us")
	{
		if ($country == "us" || $country == "ca")
		{
			$phone = ereg_replace("[^0-9]","",$phone);
			
			if (strlen($phone) == 10)
			{
				$p_area   = substr($phone,0,3);
				$p_prefix = substr($phone,3,3);
				
				if (DEBUG) echo "area: $p_area <br/>";
				if (DEBUG) echo "prefix p_prefix: $phone <br/>";	
				
				$sql = "select * from movingdirectory.areacodes where npa = '$p_area' ".
						"and nxx = '$p_prefix' and (country = 'US' || country = 'CA') limit 1;";
				$rs = new mysql_recordset($sql);
				$result = ($rs->fetch_array())?$phone:"";
				//mail("david@irelocation.com","irelo phone validate","$phone -> $result");
				return $result;
			}
			else return "";
		}
		else
			return "";
	}
	
	//private used by validateLocation
	function findZipCode($city,$state,$country = "us")
	{
		if ($country == "us")
		{
			if (strlen($state) == 2 && strlen($city) >= 3)//find zip code.
			{
				$sql = "select zip,city,state from movingdirectory.zip_codes where city ".
						" like '$city%' and state = '$state' limit 1";
				$rs = new mysql_recordset($sql);
				if ($rs->fetch_array())
					return $rs->myarray;
				else
				{
					if (strlen($city) > 5)
					{
						$shorter = substr($city,0,-3);
						return findZipCode($state,$shorter);
					}
				}
			}
		}
		
		return array("","","");
	}
	
	//private used by validateLocation
	function findCityState($zip,$country = "us")
	{
		if ($country == "us")
		{
			$zip = ereg_replace("[^0-9]","",$zip);
			$sql = "select zip,city,state from movingdirectory.zip_codes ".
					"where zip = '".$zip."' and length(city) >= 3 limit 1";
			//mail("david@irelocation.com","SOAP DEBUG - zip",$sql);
			$rs = new mysql_recordset($sql);
			if ($rs->fetch_array())
				return $rs->myarray;
			else
				return array("zip" => $zip, "city" => "", "state" => "");				
		}	
	}

?>