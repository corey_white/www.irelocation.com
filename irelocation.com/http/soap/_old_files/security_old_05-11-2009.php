<?
	include_once "../inc_mysql.php";
	//include "common.php";

	//display lots of messages?
	define(DEBUG,false);

	/*
		format for an auto lead:	
	
		$format[<group>]
			group: name of category
		$format[<group>][<key>][<required>]
			key: name of the field
			required: boolean, is this field required.
	
	*/

	$security_format['security_keys'] = array( "building_type" => true, "own_rent" => true, 
				"quote_type" => true, "num_locations" => false, "sqr_footage" => false,
				 "current_needs" => true, "address" => true, "fire" => false,
				 "cctv" => false, "access" => false);
				 
	$security_format['location_keys'] = array( "zip" => true,"city" => true,
										"state" => true, "country" => false);
	$security_format['customer_keys'] = array( "fname" => true,"lname" => true,
												"phone" => true,"phone_alt" => false,
												"email" => true,"contact" => false,
							"est_move_date" => false, "comments" => false);
	$security_format['internal_keys'] = array( "source" => true, "referer" => false,
								"remote_ip" => true, "campaign" => true,
								"aff" => true);
	
	/*
		Sample:
	
		Vehicle:
			$vehicle = array( "year" => 2003, "make" => "Nissan", 
							  "model" => "Frontier", "type" => "Pickup Truck",
							  "condition" => "running");
		LocationStruct:
			$location = array( "zip" => 85282, "city" => "Tempe", "state" => "AZ");
		
		Customer:
			$customer = array( "fname" => "Dave", "lname" => "Haveman", 
								"phone1" => "4807857400", "email" => "david@irelocation.com",
								"contact" => "email", "est_move_date" => "YYYY-MM-DD" );
				
		Internal:
			$internal = array( "source" => "mmc", "referer" => "www.autoextra.com",
								"remote_ip" => "63.229.84.218", "campaign" => "auto");
	*/
	
	/*
		function to recieve Auto Leads via SOAP

		inputs:
			$vehicle:	assoc array of vehicle details
			$from/$to:	assoc array of location information.
			$customer: 	assoc array of customer info.
			$internal: 	internal information, source, referer, ip	
		returns:
			$result[<success>] Success or Error
			$result[<code>] quote_id upon success, error code upon failure
			$result[<description>] text description of the error, blank if successful.
	*/	
	function SecurityLead($security,$location,$customer,$internal)
	{
		global $security_format;
		include_once "../logging/Logger.php";
		$log = new Logger("soap_security_leads.txt");
		$log->info("------------Incoming Lead------------");
		
		//validate locations, if not valid, blank them out so it will fail later.
		$location = validateLocation($location);		
		
		//validate phones, if not valid, blank them out so it will fail later.
		$customer['phone'] = validatePhone($customer['phone']);
		$customer['phone_alt'] = validatePhone($customer['phone_alt']);
		
		/*
		implies:
			phone is valid.
			from and to are valid locations
		*/
		$keys = array_keys($security_format);
		
		//print_r($keys);
		
		//validate all elements are present.
		foreach($keys as $k)
		{
			$format_array_name = substr($k,0,-5);
			
			//echo "Array: $format_array_name <Br/>";
			
			$format_array = $security_format[$k];

			//echo "Array: ".print_r($format_array,true);
			
			$param_array = $$format_array_name;
			$sub_keys = array_keys($format_array);		
			
			foreach($sub_keys  as $k)
			{											
				if($format_array[$k] && $param_array[$k] == "")
					return array("result" => "Error", 
								 "code" => $k, 
								 "description" => fail($format_array_name,$k));													
			}						
		}
		
		if ($security['sqr_footage'] == 0 || $security['sqr_footage'] == "")
			$security['sqr_footage'] = "0";
		
		extract($security);
		extract($customer);
		extract($location);		
		extract($internal);
		$today = date("Ym");
		$sql = " select * from leads_security where phone1 = '$phone' and zip = '$location[zip]'".
				" and source like '$source%' and received like '$today%'; ";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return array("result" => "Error", 
								 "code" => "duplicate", 
								 "description" => "This entry is a duplicate");		
		}	
		$received = date("YmdHis");
			
		$test_date = date("YmdHis",strtotime("now"));
#		mail("rob@irelocation.com","TIME RECEIVED FROM SECURITY.PHP","received = $received\ntest_date = $test_date");
			
		//append affiliate source convention.
		if ($internal['aff'] == "true")
			$source = "aff_$source";
		if ($num_location == 0 || $num_location == "") $num_location = 1;
		//save lead.
		$sql = "insert into irelocation.leads_security 
	(received, source, referrer, quote_type, name1, name2, phone1, phone2, email, address, city, state_code, zip, current_needs, building_type, num_location, sqr_footage, own_rent, fire, basement, prewired, access, cctv, other_field, comments, campaign, remote_ip)
	values
	('$received', '$source', '$referrer', '$quote_type', '$fname', '$lname', '$phone', '$phone2', '$email', '$address', '$location[city]', '$location[state]', '$location[zip]', '$current_needs', '$building_type', '$num_location', '$sqr_footage', '$own_rent', '$fire', '$basement', '$prewired', '$access', '$cctv', '$other_field', '$comments', '$campaign', '$remote_ip')";
		
		//get quote_id, 
		$rs = new mysql_recordset($sql);
		$quote_id = $rs->last_insert_id();
		
		//-- mail me more info
		#mail("rob@irelocation.com","TIME RECEIVED FROM SECURITY.PHP","quote_id = $quote_id\nreceived = $received\ntest_date = $test_date");

		//return it.
		return array("result" => "Success", "code" => $quote_id, 
						"description" => "The lead was accepted.");
	}
	
	function securityFail($quote_id,$reason)
	{
		return array("result" => "Error", "code" => $quote_id, 
						"description" => $reason);
	}
	
	/*
		This is used for the matrix site, usalarmcompanies.com, to assign variable lead ids to a lead.m
		the lead_ids are passed in as an array.
	*/
	function assignSecurityLead($quote_id,$lead_ids)
	{
		//mail("davidhaveman@gmail.com","SOAP - Assignment Call",
		//		$quote_id."\n".implode(",",$lead_ids));
		
		if (!is_array($lead_ids)) 
			return securityFail($quote_id,"No Lead Id's Given");
		if (count($lead_ids) == 0) 
			return securityFail($quote_id,"No Lead Id's Given");
		if (strlen($quote_id) == 0 || !is_numeric($quote_id)) 
			return securityFail($quote_id,"Invalid Quote Id");
		
		$sql = "select campaign from irelocation.leads_security ".
				"where quote_id = '$quote_id' and campaign = 'usalarm' ".
				" and (lead_ids = '' OR lead_ids is null) ";
		$rs = new mysql_recordset($sql);
		if (!$rs->fetch_array())	
			return securityFail($quote_id,"Invalid Quote Id");
		
		foreach($lead_ids as $lead)
		{
			if (!is_numeric($lead)) return securityFail($quote_id,"Invalid Lead ID: $lead");			
		}
		
		$sql = "update irelocation.leads_security set lead_ids = '".
				implode("|",$lead_ids)."' where quote_id = '$quote_id' ";
		$rs = new mysql_recordset($sql);
		
		//return it.
		return array("result" => "Success", "code" => $quote_id, 
						"description" => "The lead was assigned.");
	}
?>