<?
	include_once "../inc_mysql.php";
	//include "common.php";

	//display lots of messages?
	define(DEBUG,false);

	/*
		format for an auto lead:	
	
		$format[<group>]
			group: name of category
		$format[<group>][<key>][<required>]
			key: name of the field
			required: boolean, is this field required.
	
	*/
	$moving_format['house_keys'] = array( "bedrooms" => true,"extras" => false,
									"type" => true,"stairs" => false, 
									"elevator" => false);
	$moving_format['from_keys'] = array( "zip" => true,"city" => true,
								"state" => true, "country" => false);
	$moving_format['to_keys'] = array( "zip" => true,"city" => true,
								"state" => true, "country" => false);
	$moving_format['customer_keys'] = array( "fname" => true,"lname" => true,"phone" => true,
							"phone_alt" => false,"email" => true,"contact" => true,
							"est_move_date" => true, "comments" => false);
	$moving_format['internal_keys'] = array( "source" => true, "referer" => false,
								"remote_ip" => true, "campaign" => false,
								"aff" => true);
	
	/*
		Sample:
	
		house:
			$house = array( "bedrooms" => "5", "extras" => "attic,playroom,patio", 
							  "type" => "house", stairs => "yes", elevator => "no");
		LocationStruct:
			$from = array( "zip" => 85282, "city" => "Tempe", "state" => "AZ");
			$to = array( "zip" => 08260, "city" => "Wildwood", "state" => "NJ");
		
		Customer:
			$customer = array( "fname" => "Dave", "lname" => "Haveman", 
								"phone1" => "4807857400", "email" => "david@irelocation.com",
								"contact" => "email", "est_move_date" => "YYYY-MM-DD" );
				
		Internal:
			$internal = array( "source" => "me", "referer" => "www.google.com",
								"remote_ip" => "63.229.84.218", "campaign" => "tm",
								"aff" => "false");
	*/
	
	/*
		function to recieve Auto Leads via SOAP

		inputs:
			$vehicle:	assoc array of vehicle details
			$from/$to:	assoc array of location information.
			$customer: 	assoc array of customer info.
			$internal: 	internal information, source, referer, ip	
		returns:
			$result[<success>] Success or Error
			$result[<code>] quote_id upon success, error code upon failure
			$result[<description>] text description of the error, blank if successful.
	*/	
	function MovingLead($house,$from,$to,$customer,$internal)
	{
		global $moving_format;
		
		//validate locations, if not valid, blank them out so it will fail later.
		$from = validateLocation($from);		
		$to = validateLocation($to);
		mail("rob@irelocation.com","SOAP - MOVING",print_r($customer,true));
		//validate phones, if not valid, blank them out so it will fail later.
		$customer['phone'] = validatePhone($customer['phone'],"us");
		$customer['phone_alt'] = validatePhone($customer['phone_alt'],"us");
		$comments = $customer['comments'];
		
		mail("rob@irelocation.com","comments","$comments");
		
		
		/*
		implies:
			phone is valid.
			from and to are valid locations
		*/
		$keys = array_keys($moving_format);
		
		//print_r($keys);
		mail("rob@irelocation.com","SOAP - MOVING",print_r($keys,true));
		//validate all elements are present.
		foreach($keys as $k)
		{
			$format_array_name = substr($k,0,-5);
			
			//echo "Array: $format_array_name <Br/>";
			
			$format_array = $moving_format[$k];

			//echo "Array: ".print_r($format_array,true);
			
			$param_array = $$format_array_name;
			$sub_keys = array_keys($format_array);		
			
			foreach($sub_keys  as $k)
			{											
				if($format_array[$k] && $param_array[$k] == "")
					return array("result" => "Error", 
								 "code" => $k, 
								 "description" => fail($format_array_name,$k));													
			}						
		}
		
		//print_r($to);
		$one_day = date("YmdHis",time()-(24*60*60));
		$phone = $customer['phone'];
		$email = $customer['email'];
		$campaign = $internal['campaign'];
		if ($from['state'] == $to['state'])
		{
			$sql = "select * from movingdirectory.quotes_local where (email = '$email' or ".
					" phone_home = '$phone') and (received > '$one_day' or received = '') and ".
					" origin_zip = '$from[zip]' limit 1";			
		}
		else
		{
			$sql = "select * from movingdirectory.quotes where (email = '$email' or ".
					" phone_home = '$phone') and (received > '$one_day' or received = '') and ".
					" origin_zip = '$from[zip]' limit 1";		
		
		}
		mail("rob@irelocation.com","SOAP - MOVING - SQL",$sql);
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			return array("result" => "Error", 
						 "code" => "duplicate", 
						 "description" => "This is a duplicate lead.");			
		}
		//print_r($from);
		
		extract($customer);
		extract($from,EXTR_PREFIX_ALL,"origin_");
		$int = ($to['country'] != "us" && $to['country'] != "");
		//echo "From City: $origin_city <br/>";
		
		extract($to,EXTR_PREFIX_ALL,"destination_");
		extract($house);		
		extract($internal);
		
		mail("rob@irelocation.com","SUBJECT","$customer[lname]");
		
		//timestamp
		$received = date("Ymdhis");
				
		//append affiliate source convention.
		if ($internal['aff'] == "true")
			$source = "aff_$source";
		//save lead.
		
		if ($from['state'] == $to['state'])
		{
			$sql = "insert into movingdirectory.quotes_local
	(name, email, phone_home, phone_work, contact, est_move_date, origin_city, origin_state, origin_zip, destination_city, destination_state, destination_zip, comments,source, keyword, remote_ip)
	values
	('$customer[fname] $customer[lname]', '$customer[email]', '$customer[phone]', '$customer[phone_alt]', '$customer[contact]', '$customer[est_move_date]', '$from[city]', '$from[state]', '$from[zip]', '$to[city]', '$to[state]', '$to[zip]', '$customer[comments]','$source', '$keyword', '$remote_ip')";
			
		}
		else
		{
			$cat_id = 2;
			$to_country = "usa";
			if ($int)
			{
				$cat_id = 3;
				$to_country = $to['country'];
			}	
			
			$sql = "insert into movingdirectory.quotes 
	(cat_id, name, email, phone_home, phone_work, contact, est_move_date, origin_city, origin_state, origin_zip, destination_city, destination_state, destination_zip, destination_country, comments,source, keyword, remote_ip)
	values
	($cat_id, '$customer[fname] $customer[lname]', '$customer[email]', '$customer[phone]', '$customer[phone_alt]', '$customer[contact]', '$customer[est_move_date]', '$from[city]', '$from[state]', '$from[zip]', '$to[city]', '$to[state]', '$to[zip]', '$to_country', '$customer[comments]','$source', '$keyword', '$remote_ip')";
		
		}
		
		//get quote_id, 
		$rs = new mysql_recordset($sql);
		$quote_id = $rs->last_insert_id();
		
		
		mail("rob@irelocation.com","SOAP DEBUG - SUBMIT",$sql."\n\n".$quote_id);
		
		//valid lead, return success.
		$results = array("result" => "Success", "code" => $quote_id, "description" => "The lead was accepted.");
		
		mail("rob@irelocation.com","SOAP - MOVING - RESULTS",print_r($results,true));
		
		return $results;
	}
	
	function saveStorageResponse($quote_id,$source,$response)
	{
		//if its a number, and not 0, it counts  as a success.
		if (is_numeric($response) && $response > 0)//valid
			$success = 1;			
		else
		{	
			$success = 0;			
		}

		$sql = " insert into movingdirectory.vanlinesstorage (quote_id,source,response,success) ".
				" values ('$quote_id','$source','$response','$success')";
		$rs = new mysql_recordset($sql);				
	}
	
?>