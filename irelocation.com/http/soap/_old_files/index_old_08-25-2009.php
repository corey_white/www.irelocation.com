<?
	/*
		This is the main SOAP server code.
		register all methods here.
	*/
	
	
	//Pull in the NuSOAP code
	require_once('../nusoap/nusoap.php');
	//Create the server instance
	$server = new soap_server;
	
	//currenlty there are 3 methods.
	$server->configureWSDL('SecurityLead', 'urn:leads');
	$server->configureWSDL('AutoLead', 'urn:leads');
	$server->configureWSDL('MovingLead', 'urn:leads');
	$server->configureWSDL('validateLocation', 'urn:leads');
	$server->configureWSDL('validatePhone', 'urn:leads');	
	$server->configureWSDL('assignSecurityLead','urn:leads');
	$server->configureWSDL('getVariable','urn:leads');
	$server->configureWSDL('saveStorageResponse','urn:leads');
	$server->configureWSDL('SurveyResponse','urn:leads');
	// Register the method to expose
	
	//A Struct to hold security system info.
	/*
	$security_format['security_keys'] = array( "building_type" => true, "own_rent" => true, 
				"quote_type" => true, "num_locations" => false, "sqr_footage" => true,
				 "current_needs" => true, "address" => true, "fire" => false,
				 "cctv" => false, "access" => false);
	*/
	$server->wsdl->addComplexType(
    'Security',
    'complexType',
    'struct',
    'all',
    'A Structure to information about a security lead',
    array(
        'building_type' => array('name' => 'building_type', 'type' => 'xsd:string'),
        'own_rent' => array('name' => 'own_rent', 'type' => 'xsd:string'),
		'quote_type' => array('name' => 'quote_type', 'type'  => 'xsd:string'),
		'num_locations' => array('name' => 'num_locations', 'type' => 'xsd:int'),
		'sqr_footage' => array('name' => 'sqr_footage', 'type' => 'xsd:int'),
        'current_needs' => array('name' => 'current_needs', 'type' => 'xsd:string'),				
		'address' => array('name' => 'address', 'type'  => 'xsd:string'),
		'fire' => array('name' => 'fire', 'type'  => 'xsd:int'),
		'cctv' => array('name' => 'cctv', 'type'  => 'xsd:int'),
		'access' => array('name' => 'access', 'type'  => 'xsd:int')
	));
	
	/*
	$house = array( "bedrooms" => "5", "extras" => "attic,playroom,patio", 
					"type" => "house", stairs => "yes", elevator => "no");
	*/
	//a struct to hold house info.
	$server->wsdl->addComplexType(
    'House',
    'complexType',
    'struct',
    'all',
    'A Structure to information about a House lead',
    array(
        'bedrooms' => array('name' => 'bedrooms', 'type' => 'xsd:string'),
        'extras' => array('name' => 'extras', 'type' => 'xsd:string'),
		'type' => array('name' => 'type', 'type'  => 'xsd:string'),
		'stairs' => array('name' => 'stairs', 'type' => 'xsd:string'),
		'elevator' => array('name' => 'elevator', 'type' => 'xsd:string')      
	));
	
	//A Struct to hold locations.
	$server->wsdl->addComplexType(
    'Location',
    'complexType',
    'struct',
    'all',
    'A Structure to hold Location information',
    array(
        'zip' => array('name' => 'zip', 'type' => 'xsd:string'),
        'city' => array('name' => 'city', 'type' => 'xsd:string'),
        'state' => array('name' => 'state', 'type' => 'xsd:string'),
		'country' => array('name' => 'country', 'type' => 'xsd:string')
    ));
	/*
	$customer = array( "fname" => "Dave", "lname" => "Haveman",  "comments" => "hi",
						"phone" => "4807857400", "email" => "david@irelocation.com",
						"contact" => "email", "est_move_date" => "MM-DD-YYYY" );
	//A Struct to hold customer information
	*/
	$server->wsdl->addComplexType(
    'Customer',
    'complexType',
    'struct',
    'all',
    'A Struct to hold customer information',
    array(
        'fname' => array('name' => 'fname', 'type' => 'xsd:string'),
        'lname' => array('name' => 'lname', 'type' => 'xsd:string'),
        'phone' => array('name' => 'phone', 'type' => 'xsd:string'),
		'phone_alt' => array('name' => 'phone_alt','type' => 'xsd:string'),
		'email' => array('name' => 'email', 'type' => 'xsd:string'),
		'contact' => array('name' => 'contact', 'type' => 'xsd:string'),
		'est_move_date' => array('name' => 'est_move_date', 'type' => 'xsd:string'),
		'comments' => array('name' => 'comments', 'type' => 'xsd:string')		
    ));
	/*
	$vehicle = array( "year" => 2003, "make" => "Nissan", 
			  "model" => "Frontier", "type" => "Pickup Truck",
			  "condition" => "r");
	A Struct to hold Vehicle Related details.
	*/
	$server->wsdl->addComplexType(
    'Vehicle',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'year' => array('name' => 'year', 'type' => 'xsd:string'),
        'make' => array('name' => 'make', 'type' => 'xsd:string'),
        'model' => array('name' => 'model', 'type' => 'xsd:string'),
		'type' => array('name' => 'type','type' => 'xsd:string'),
		'condition' => array('name' => 'condition', 'type' => 'xsd:string')
    ));


	/*
	$internal = array( "source" => "mmc", "referer" => "www.autoextra.com",
			"remote_ip" => "63.229.84.218", "campaign" => "auto",
			"aff" => "false");	
	A Struct to hold Internal details such as tracking, ip, etc.
	*/
	$server->wsdl->addComplexType(
    'Internal',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'source' => array('name' => 'source', 'type' => 'xsd:string'),
        'referer' => array('name' => 'referer', 'type' => 'xsd:string'),
        'remote_ip' => array('name' => 'remote_ip', 'type' => 'xsd:string'),
		'campaign' => array('name' => 'campaign','type' => 'xsd:string'),
		'aff' => array('name' => 'aff', 'type' => 'xsd:string')
    ));
	
	
	//A Struct to hold the results of the call.
	$server->wsdl->addComplexType(
    'Results',
    'complexType',
    'struct',
    'all',
    '',
    array(       
		'result' => array('name' => 'result','type' => 'xsd:string'),
		'code' => array('name' => 'code','type' => 'xsd:string'),
		'description' => array('name' => 'description', 'type' => 'xsd:string')
    ));
	
	
	//$r = SecurityLead($security,$location,$customer,$internal)
	/*
		A function to submit auto leads.	
	*/
	$server->register(
		'SecurityLead',
		array('security' => 'tns:Security',
			  'location' => 'tns:Location',			  
			  'customer' => 'tns:Customer',
			  'internal' => 'tns:Internal'),
		array('return' => 'tns:Results'),  
		'urn:leads',                         // namespace
	    'urn:leads#security',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'Accept a security lead.'   
	);
	
	

	$server->wsdl->addComplexType
	(
		'ArrayOfstring',
		'complexType',
		'array',
		'',
		'SOAP-ENC:Array',
		array(),
		array(array('ref'=>'SOAP-ENC:arrayType','wsdl:arrayType'=>'string[]')),
		'xsd:string'
	);
	
	$server->register(
		'getVariable',
		array('keys' => 'tns:ArrayOfstring'),
		array('results' => 'tns:ArrayOfstring'),  
		'urn:leads',                         // namespace
	    'urn:leads#getVariable',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'get a system type variable.'   
	);
	
	//$r = assignSecurityLead($quote_id,$lead_ids)
	/*
		A function to submit auto leads.	
	*/
	$server->register(
		'assignSecurityLead',
		array('quote_id' => 'xsd:string',
			  'lead_ids' => 'tns:ArrayOfstring'),
		array('return' => 'tns:Results'),  
		'urn:leads',                         // namespace
	    'urn:leads#assignSecurity',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'assign a security lead.'   
	);
	
	//$r = MovingLead($house,$from,$to,$customer,$internal);
	/*
		A function to submit house moving leads.	
	*/
	$server->register(
		'MovingLead',
		array('house' => 'tns:House',
			  'from' => 'tns:Location',
			  'to' => 'tns:Location',
			  'customer' => 'tns:Customer',
			  'internal' => 'tns:Internal'),
		array('return' => 'tns:Results'),  
		'urn:leads',                         // namespace
	    'urn:leads#moving',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'Accept an house moving lead.'   
	);
	
	
	//$r = AutoLead($vehicle,$from,$to,$customer,$internal);/
	/*
		A function to submit auto leads.	
	*/
	$server->register(
		'AutoLead',
		array('vehicle' => 'tns:Vehicle',
			  'from' => 'tns:Location',
			  'to' => 'tns:Location',
			  'customer' => 'tns:Customer',
			  'internal' => 'tns:Internal'),
		array('return' => 'tns:Results'),  
		'urn:leads',                         // namespace
	    'urn:leads#auto',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'Accept an auto shipping lead.'   
	);
	
	/*
		A function to Complete and Validate a location.
	*/
	$server->register(
		'validateLocation',
		array('location' => 'tns:Location',			  
			  'country' => 'xsd:string'),
		array('return' => 'tns:Location'),  
		'urn:leads',                         // namespace
	    'urn:leads#validationLocation',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'Complete and Validate a location.'   
	);
	/*
		validates a us or canadian phone number.
	*/
	$server->register(
		'validatePhone',
		array('phone' => 'xsd:string',
			  'country' => 'xsd:string'),
		array('return' => 'xsd:string'),  
		'urn:leads',                         // namespace
	    'urn:leads#validatePhone',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'validates a us or canadian phone number.'   
	);
	//saveStorageResponse($quote_id,$source,$response);
	$server->register(
		'saveStorageResponse',
		array('quote_id' => 'xsd:string',
			  'source' => 'xsd:string',
			  'response' => 'xsd:string'),
		array(),  
		'urn:leads',                    	      // namespace
	    'urn:leads#saveStorageResponse',          // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'save the results of sending a storage lead to Vanlines.'   
	);
		



	//-- for the usalarm survey
	/*
	$extra = array( "source" => $source, "referer" => $referer, "remote_ip" => $remote_ip);
	A Struct to hold extra details such as tracking, ip, etc. from the usalarm survey
	*/
	$server->wsdl->addComplexType(
    'Extra',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'source' => array('name' => 'source', 'type' => 'xsd:string'),
        'referer' => array('name' => 'referer', 'type' => 'xsd:string'),
        'remote_ip' => array('name' => 'remote_ip', 'type' => 'xsd:string')
    ));
	
	/*
	$survey = array( "company" => $company, "rate_installation" => $rate_installation,  "comments" => $comments, "stars" => $stars);
	A Struct to hold usalarm survey details.
	*/
	$server->wsdl->addComplexType(
    'Survey',
    'complexType',
    'struct',
    'all',
    '',
    array(
        'company' => array('name' => 'company', 'type' => 'xsd:string'),
        'rate_installation' => array('name' => 'rate_installation', 'type' => 'xsd:string'),
        'stars' => array('name' => 'stars', 'type' => 'xsd:string'),
		'comments' => array('name' => 'comments','type' => 'xsd:string')
    ));


		/*
		$results = $proxy->SurveyResponse($survey,$extra);
		
		A function to submit alarm surveys.	
	*/
	$server->register(
		'SurveyResponse',
		array('survey' => 'xsd:string',
			  'extra' => 'xsd:string',),
		array('return' => 'xsd:string'),  
		'urn:leads',                         // namespace
	    'urn:leads#SurveyResponse',                   // soapaction
		'rpc',                                    // style
	    'encoded',                                // use
    	'Accept an alarm survey response.'   
	);
	
	

	
	//include functions to do the actual processing.
	include "common.php";
	include "auto.php";
	include "security.php";
	include "moving.php";
	include "alarmsurvey.php";

// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>