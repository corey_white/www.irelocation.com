<?
/* 
************************FILE INFORMATION**********************
* File Name:  service.php
**********************************************************************
* Description:  this is the service portion of the soap class - initially for use with the quote-it.com website
**********************************************************************
* Creation Date:  6/13/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

	include_once "../inc_mysql.php";

	//display lots of messages?
	define(DEBUG,false);

	/*
		format for an service lead:	
	
		$format[<group>]
			group: name of category
		$format[<group>][<key>][<required>]
			key: name of the field
			required: boolean, is this field required.
	
	*/

	$service_format['service_keys'] = array(	"furnace" => false,
																	"heat_pump" => false, 
																	"air_cond" => false, 
																	"dryer_vent" => false, 
																	"other" => false,
																	"sq_ft" => false, 
																	"registers" => false, 
																	"system_loc" => false,
																	"ac_service" => false);
				 
	$service_format['customer_keys'] = array( "fname" => true,
																		"lname" => true,
																		"phone" => true,
																		"phone_alt" => false,
																		"email" => true,
																		"contact" => false,
																		"est_move_date" => false,
																		"comments" => false);

	$service_format['internal_keys'] = array(		"source" => true, 
																		"referer" => false,
																		"remote_ip" => true, 
																		"campaign" => true,
																		"aff" => false);

	$service_format['location_keys'] = array(	"zip" => true,
																		"city" => false,
																		"state" => false, 
																		"country" => false);
	
	$foo = print_r($service_format,true);
	mail("rob@irelocation.com","Service_Format Service.php","$foo");
	/*
		function to recieve Service Leads via SOAP

		inputs:
			$service:	assoc array of service details
			$location:	assoc array of location information.
			$customer: 	assoc array of customer info.
			$internal: 	internal information, source, referer, ip	
		returns:
			$result[<success>] Success or Error
			$result[<code>] quote_id upon success, error code upon failure
			$result[<description>] text description of the error, blank if successful.
	*/	
	function ServiceLead($service,$location,$customer,$internal) {
		global $service_format;
		
		mail("rob@irelocation.com","From ServiceLead","From the ServiceLead function");
/*
		include_once "../logging/Logger.php";
		$log = new Logger("soap_service_leads.txt");
		$log->info("------------Incoming Lead------------");
*/
		
		//validate locations, if not valid, blank them out so it will fail later.
		$location = validateLocation($location);		
		
		//validate phones, if not valid, blank them out so it will fail later.
		$customer['phone'] = validatePhone($customer['phone']);
		
		$keys = array_keys($service_format);
		
		//print_r($keys);
		
		//validate all elements are present.
		foreach($keys as $k)
		{
			$format_array_name = substr($k,0,-5);
			
			//echo "Array: $format_array_name <Br/>";
			
			$format_array = $service_format[$k];

			//echo "Array: ".print_r($format_array,true);
			
			$param_array = $$format_array_name;
			$sub_keys = array_keys($format_array);		
			
			foreach($sub_keys  as $k)
			{											
				if($format_array[$k] && $param_array[$k] == "")
					return array("result" => "Error", 
								 "code" => $k, 
								 "description" => fail($format_array_name,$k));													
			}						
		}
		
		
		extract($service);
		extract($location);
		extract($customer);
		extract($internal);
		
		$today = date("Ym");
		
		$sql = " select * from irelocation.leads_ductcleaning where phone = '$customer[phone]' and zip = '$location[zip]' and source like '$source%' and received like '$today%'; ";
		
		mail("rob@irelocation.com","DUP CHECK SQL from Service.php","$sql");
		
		$rs = new mysql_recordset($sql);
		
		if ($rs->fetch_array()) {
			return array("result" => "Error", 
								 "code" => "duplicate", 
								 "description" => "This entry is a duplicate");		
		}	
		
		$received = date("Ymdhis");
			
#		$test_date = date("Ymdhis",strtotime("now"));
#		mail("rob@irelocation.com","TIME RECEIVED FROM SERVICE.PHP","received = $received\ntest_date = $test_date");
			
		//append affiliate source convention.
		if ($internal['aff'] == "true")
			$source = "aff_$source";
			
		// Save the lead
		$sql = "insert into irelocation.leads_ductcleaning set
						received = '$received',  
						source = '$source',  
						referrer = '$referrer',  
						fname = '$fname',  
						lname = '$lname',  
						email = '$email',  
						phone = '$phone',  
						zip = '$zip',  
						furnace = '$furnace',  
						air_cond = '$air_cond',  
						heat_pump = '$heat_pump',  
						dryer_vent = '$dryer_vent',  
						other = '$other',  
						sq_footage = '$sq_ft',  
						num_registers = '$num_registers',  
						system_location = '$system_location',  
						ac_service = '$ac_service',
						campaign = '$campaign',  
						remote_ip = '$remote_ip' ";
		
		//get quote_id, 
		$rs = new mysql_recordset($sql);
		$quote_id = $rs->last_insert_id();
		
		//-- mail me more info
		mail("rob@irelocation.com","TIME RECEIVED FROM SERVICE.PHP","quote_id = $quote_id\nreceived = $received\ntest_date = $test_date\nsql = $sql");

		//return it.
		return array("result" => "Success", "code" => $quote_id, "description" => "The lead was accepted.");
	}
	
	function serviceFail($quote_id,$reason)
	{
		return array("result" => "Error", "code" => $quote_id, "description" => $reason);
	}
	
	//-- Send leads to the lead_ids (companies) passed into function
	function assignServiceLead($quote_id,$lead_ids)
	{
		//mail("davidhaveman@gmail.com","SOAP - Assignment Call",
		//		$quote_id."\n".implode(",",$lead_ids));
		
		if (!is_array($lead_ids)) 
			return serviceFail($quote_id,"No Lead Id's Given");
			
		if (count($lead_ids) == 0) 
			return serviceFail($quote_id,"No Lead Id's Given");
			
		if (strlen($quote_id) == 0 || !is_numeric($quote_id)) 
			return serviceFail($quote_id,"Invalid Quote Id");
		
		$sql = "select campaign from irelocation.leads_ductcleaning where quote_id = '$quote_id' and campaign = 'quoteit' and (lead_ids = '' OR lead_ids is null) ";
		$rs = new mysql_recordset($sql);
		
		if (!$rs->fetch_array())	
			return serviceFail($quote_id,"Invalid Quote Id");
		
		foreach($lead_ids as $lead) {
			if (!is_numeric($lead)) return serviceFail($quote_id,"Invalid Lead ID: $lead");			
		}
		
		$sql = "update irelocation.leads_ductcleaning set lead_ids = '".
				implode("|",$lead_ids)."' where quote_id = '$quote_id' ";
		$rs = new mysql_recordset($sql);
		
		//return it.
		return array("result" => "Success", "code" => $quote_id, 
						"description" => "The lead was assigned.");
	}
?>