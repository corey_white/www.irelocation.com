<?
	include_once "../inc_mysql.php";
	//include "common.php";

	//display lots of messages?
	define(DEBUG,false);

	/*
		format for an auto lead:	
	
		$format[<group>]
			group: name of category
		$format[<group>][<key>][<required>]
			key: name of the field
			required: boolean, is this field required.
	
	*/
	$format['vehicle_keys'] = array( "year" => true,"make" => true,"model" => true,
									"type" => true,"condition" => true);
	$format['from_keys'] = array( "zip" => true,"city" => true,
								"state" => true, "country" => false);
	$format['to_keys'] = array( "zip" => true,"city" => true,
								"state" => true, "country" => false);
	$format['customer_keys'] = array( "fname" => true,"lname" => true,"phone" => true,
							"phone_alt" => false,"email" => true,"contact" => true,
							"est_move_date" => true, "comments" => false);
	$format['internal_keys'] = array( "source" => true, "referer" => false,
								"remote_ip" => true, "campaign" => false,
								"aff" => true);
	
	/*
		Sample:
	
		Vehicle:
			$vehicle = array( "year" => 2003, "make" => "Nissan", 
							  "model" => "Frontier", "type" => "Pickup Truck",
							  "condition" => "running");
		LocationStruct:
			$location = array( "zip" => 85282, "city" => "Tempe", "state" => "AZ");
		
		Customer:
			$customer = array( "fname" => "Dave", "lname" => "Haveman", 
								"phone1" => "4807857400", "email" => "david@irelocation.com",
								"contact" => "email", "est_move_date" => "YYYY-MM-DD" );
				
		Internal:
			$internal = array( "source" => "mmc", "referer" => "www.autoextra.com",
								"remote_ip" => "63.229.84.218", "campaign" => "auto");
	*/
	
	/*
		function to recieve Auto Leads via SOAP

		inputs:
			$vehicle:	assoc array of vehicle details
			$from/$to:	assoc array of location information.
			$customer: 	assoc array of customer info.
			$internal: 	internal information, source, referer, ip	
		returns:
			$result[<success>] Success or Error
			$result[<code>] quote_id upon success, error code upon failure
			$result[<description>] text description of the error, blank if successful.
	*/	
	function AutoLead($vehicle,$from,$to,$customer,$internal)
	{
		global $format;
		
		include_once "../logging/Logger.php";
		$log = new Logger("soap_auto_leads.txt");
		$log->info("------------Incoming Lead------------");
		
		//validate locations, if not valid, blank them out so it will fail later.
		$from = validateLocation($from);		
		$to = validateLocation($to);
		//mail("david@irelocation.com","ATUS",print_r($customer,true));
		//validate phones, if not valid, blank them out so it will fail later.
		$customer['phone'] = validatePhone($customer['phone'],"us");
		$customer['phone_alt'] = validatePhone($customer['phone_alt'],"us");
		
		/*
		implies:
			phone is valid.
			from and to are valid locations
		*/
		$keys = array_keys($format);
		
		//print_r($keys);
		
		//validate all elements are present.
		foreach($keys as $k)
		{
			$format_array_name = substr($k,0,-5);
			
			//echo "Array: $format_array_name <Br/>";
			
			$format_array = $format[$k];

			//echo "Array: ".print_r($format_array,true);
			
			$param_array = $$format_array_name;
			$sub_keys = array_keys($format_array);		
			
			foreach($sub_keys  as $k)
			{											
				if($format_array[$k] && $param_array[$k] == "")
				{
					$log->error(fail($format_array_name,$k));
					return array("result" => "Error", 
								 "code" => $k, 
								 "description" => fail($format_array_name,$k));					
				}													
			}	
			$log->debug("input ".$k." passed.");					
		}
		
		//print_r($to);
		$one_day = date("YmdHis",time()-(24*60*60));
		$phone = $customer['phone'];
		$email = $customer['email'];
		$campaign = $internal['campaign'];
		$sql = "select * From marble.auto_quotes where (phone = '$phone' or  ".
				" email = '$email') and received > '$one_day' and campaign =  '$campaign' ";
		//mail("david@irelocation.com","ATUS SQL",$sql);
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$log->error("This is a duplicate lead.");
			return array("result" => "Error", 
						 "code" => "duplicate", 
						 "description" => "This is a duplicate lead.");			
		}
		//print_r($from);
		
		extract($customer);
		extract($from,EXTR_PREFIX_ALL,"origin_");
		
		//echo "From City: $origin_city <br/>";
		
		extract($to,EXTR_PREFIX_ALL,"destination_");
		extract($vehicle);		
		extract($internal);
		
		
		//timestamp
		$received = date("Ymdhis");
				
		//append affiliate source convention.
		if ($internal['aff'] == "true")
		{
			//previously extracted from internal.		
			$log->debug("affiliate lead: ".$source);
			$source = "aff_$source";
		}	
		//save lead.
		$sql = "insert into marble.auto_quotes 
		(received, source, remote_ip, referrer, fname, lname, email, phone, ".
		 " phone_alt, customer_comments, origin_city, origin_state, origin_zip, ".
		 " destination_city, destination_state, destination_zip, est_move_date, contact, ".
		 " vmake, vmodel, vyear, vtype, running, campaign) 	values ".
		 " ".
		 " ('$received', '$source', '$remote_ip', '$referer', '$fname', '$lname', '$email', ".
		 " '$phone', '$phone_alt', '$comments', '$from[city]', '$from[state]', '$from[zip]', ".
		 " '$to[city]', '$to[state]', '$to[zip]', '$est_move_date', ".
		 " '$contact', '$make', '$model', '$year', '$type', '$condition', '$campaign' )";
		
		//-- ok, now we need to mail me what the referer is that was received on this end
		#mail("code@irelocation.com","AUTO-PHP 1","This is from the auto.php include on irelo soap \n\n$sql");

		//get quote_id, 
		$rs = new mysql_recordset($sql);
		$quote_id = $rs->last_insert_id();
		//mail("david@irelocation.com","SOAP DEBUG - SUBMIT",$sql."\n\n".$quote_id);
		
		//return it.
		$log->debug("lead accepted: ".$quote_id);
		$log->debug("");
		return array("result" => "Success", "code" => $quote_id, 
						"description" => "The lead was accepted.");
	}
?>