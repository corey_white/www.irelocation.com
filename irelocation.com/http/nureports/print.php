<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	include_once "Base.php";
 
 	function printHeader($report,$names)
 	{
		$cols = count($names);
		$header = "<strong>Title:</strong>".$report->name." &nbsp; &nbsp; &nbsp; &nbsp;";
		$header .= "<a href='run_report.php?report_id=".$report->report_id."&format=csv'>Download to <img src='images/xls_icon.gif' border='0'/></a><br/>";
		if ($report->description != "")
			$header .= "<strong>Description:</strong><Br/>".$report->description."<br/><Br/>";
		else
			$header .= "<br/>";
		$header .= "<tr>";
		
		if($report->format == LEAD_DISPLAY)
		{
			$header .= "<td>&nbsp;</td>";		
			$cols++;			
		}		
		
		for ($i = 0; $i < count($names); $i++)
			$header .= "<td align='center' nowrap><strong>".$names[$i]."</strong></td>";	
		$header .= "</tr>";
		$header .= "<tr><td colspan='$cols'><hr/></td></tr>";
		return $header;
 	}
 
 	function printRow($names,$myarray,$rc=DEFAULT_PARAMETER)
 	{
 		$row = "<tr>";
 		
 		if($rc != DEFAULT_PARAMETER)
			$row .= "<td>$rc</td>";	
 		
		for ($i = 0; $i < count($names); $i++)
			$row .= "<td>".$myarray[$names[$i]]."</td>";	
		$row .= "</tr>";
		return $row;
 	} 
 	
 	function printFooter($columns,$rows)
 	{
 		if ($columns > 2) $columns++;
 		$row = "<tr><td colspan='$columns' align='right'>"; 	 				
		$row .= "$rows row(s) returned.</td></tr>";
		return $row;
 	}
 	
 	
 	function printHTML($report)
 	{
 		$results = $report->getSQL();
 		if (count($results) > 0)
 		{
 			$sql = $results[0];
 			
 			echo "<!--- $sql --->\n";
 			
 			$names = $results[1];
 			echo "<table>";
			echo printHeader($report,$names);
 			$rs = new mysql_recordset($sql);
 			
 			if ($report->format == LEAD_DISPLAY)
 				$rows = 1;
 			else
 				$rows = "*";
 				
 			while($rs->fetch_array())
 			{
 				echo printRow($names,$rs->myarray,$rows);
 				if ($report->format == LEAD_DISPLAY)
 					$rows++; 					
 			}
 			if ($report->format == LEAD_DISPLAY)
 				echo printFooter(count($names),$rows-1);
 			echo "</table>";
 		}
 	}
 	
 	function printCSV($report)
 	{
 		$results = $report->getSQL();
 		if (count($results) > 0)
 		{
 			$sql = $results[0];
 			$names = $results[1];

			echo printCSVHeader($report,$names);
 			$rs = new mysql_recordset($sql);
 			
 			if ($report->format == LEAD_DISPLAY)
 				$rows = 1;
 			else
 				$rows = "*";
 				
 			while($rs->fetch_array())
 			{
 				echo printCSVRow($names,$rs->myarray,$rows);
 				if ($report->format == LEAD_DISPLAY)
 					$rows++; 					
 			}
 			if ($report->format == LEAD_DISPLAY)
 				echo printCSVFooter(count($names),$rows-1);
 			echo "\n";
 		}
 	}
 	
 	function printCSVHeader($report,$names)
 	{
		$cols = count($names);
		$header = "Title:,".$report->name."\n";
		if ($report->description != "")
			$header .= "Description:\n".$report->description."\n\n";
		else
			$header .= "\n";
		
		
		if($report->format == LEAD_DISPLAY)
		{
			$header .= ",";		
			$cols++;			
		}		
		
		for ($i = 0; $i < count($names); $i++)
			$header .= $names[$i].",";	
		$header .= "\n";		
		return $header;
 	}
 	
 	function printCSVRow($names,$myarray,$rc=DEFAULT_PARAMETER)
 	{
 		$row = "";
 		
 		if($rc != DEFAULT_PARAMETER)
			$row .= "$rc,";	
 		
		for ($i = 0; $i < count($names); $i++)
			$row .= "\"".$myarray[$names[$i]]."\",";	
		$row .= "\n";
		return $row;
 	} 
 	
 	function printCSVFooter($columns,$rows)
 	{
		$row = "$rows row(s) returned.\n";
		return $row;
 	}
 	
?>