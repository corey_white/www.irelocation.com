<?php
  header( "content-type: text/xml" );
  header( "Content-disposition: attachment; filename=Report.xls");
  
  require_once( "../inc_mysql.php" );
  
  $data = array();
  
$sql = " select 
	name, report_id, class_id, created, campaign, nr.lead_id,
	co.comp_name
from
	irelocation.nureports as nr 
	join 
	movingdirectory.directleads as dl
	on dl.lead_id = nr.lead_id 
	join 
	movingdirectory.company as co 
	on co.comp_id = dl.comp_id";
  
  $res = new mysql_recordset( $sql );
  
  $rows = array();
  while( $res->fetch_array() ) 
  { $rows[] = $res->myarray; }
  print "<?xml version=\"1.0\"?>\n";
  print "<?mso-application progid=\"Excel.Sheet\"?>\n";
?>
  <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:o="urn:schemas-microsoft-com:office:office"
  xmlns:x="urn:schemas-microsoft-com:office:excel"
  xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
  xmlns:html="http://www.w3.org/TR/REC-html40">
  <DocumentProperties 
     xmlns="urn:schemas-microsoft-com:office:office">
 <Author>David Haveman</Author>
  <LastAuthor>>David Haveman</LastAuthor>
  <Created>2008-01-31T12:00:00Z</Created>
  <LastSaved>2008-01-31T12:00:00Z</LastSaved>
  <Company>iRelocation Network, llc.</Company>
  <Version>1.0</Version>
  </DocumentProperties>
  <ExcelWorkbook 
     xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>8535</WindowHeight>
  <WindowWidth>12345</WindowWidth>
  <WindowTopX>480</WindowTopX>
  <WindowTopY>90</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
  </ExcelWorkbook>
  <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
  <Alignment ss:Vertical="Bottom"/>
  <Borders/>
  <Font/>
  <Interior/>
  <NumberFormat/>
  <Protection/>
  </Style>
  <Style ss:ID="s21" ss:Name="Hyperlink">
  <Font ss:Color="#0000FF" ss:Underline="Single"/>
  </Style>
  <Style ss:ID="s23">
  <Font x:Family="Swiss" ss:Bold="1"/>
  </Style>
  </Styles>
  <Worksheet ss:Name="Names">
  <Table ss:ExpandedColumnCount="4"
  ss:ExpandedRowCount="12"
  x:FullColumns="1" x:FullRows="1">
  <Column ss:Index="4" ss:AutoFitWidth="0" ss:Width="154.5"/>
  <Row ss:StyleID="s23">
  <Cell><Data ss:Type="String">Report Id</Data></Cell>
  <Cell><Data ss:Type="String">Name</Data></Cell>
  <Cell><Data ss:Type="String">Campaign</Data></Cell>
  <Cell><Data ss:Type="String">Company</Data></Cell>
  </Row>
    <Row>
  <Cell>
  	<Data ss:Type="String">10</Data>
  </Cell>
  <Cell><Data ss:Type="String">Test Brinks Report  </Data></Cell>
  <Cell><Data ss:Type="String">us  </Data></Cell>
  <Cell>
  	<Data ss:Type="String"></Data>
  </Cell>
  </Row>
 
  <?php 
  	
  	foreach( $rows as $row ) { 
  		
  ?>
  <Row>
  <Cell>
  	<Data ss:Type="String"><?php echo( $row['report_id'] ); ?></Data>
  </Cell>
  <Cell><Data ss:Type="String"><?php echo( $row['name'] ); ?>
  </Data></Cell>
  <Cell><Data ss:Type="String"><?php echo( $row['campaign'] ); ?>
  </Data></Cell>
  <Cell>
  	<Data ss:Type="String"><?php echo( $row['comp_name'] ); ?></Data>
  </Cell>
  </Row>
  <?php } ?>
  </Table>
      </Table>
  <WorksheetOptions 
     xmlns="urn:schemas-microsoft-com:office:excel">
  <Print>
  <ValidPrinterInfo/>
  <HorizontalResolution>300</HorizontalResolution>
  <VerticalResolution>300</VerticalResolution>
  </Print>
  <Selected/>
  <Panes>
  <Pane>
  <Number>3</Number>
  <ActiveRow>1</ActiveRow>
  </Pane>
  </Panes>
  <ProtectObjects>False</ProtectObjects>
  <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
  </Worksheet>
  </Workbook>