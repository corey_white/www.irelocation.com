<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 	class Col
 	{
 		var $field;
 		var $display;
 		
 		function Col($f,$d)
 		{
 			$this->field = $f;
 			$this->display = $d;
 		}
 		
 		function sql()
 		{
 			return $this->field." as '".$this->display."' ";
 		}
 	}
 
 	class Basic
 	{
 		var $field;
 		var $value;
 		
 		function Basic($f,$v)
 		{
 			$this->field = $f;
 			$this->value = $v;
 		}
 		
 		function get()
 		{
 			return $this->field . " = '". $this->value."' ";
 		} 		
 	}
 	
 	class RangeQual
 	{
 		var $min_v,$max_v;
 		
 		function RangeQual($f,$min,$max)
 		{
 			parent::Basic($f,0);
 			$this->min_v = $min;
 			$this->max_v = $max;
 		} 		 			
 		
 		function get()
 		{
 			return " ( ".$this->field . " > '". $this->min."' and ".
 					$this->field . " < '". $this->max."' ) ";	
 		}
 	}
 	
 	class EnumQual
 	{ 		
 		function EnumQual($f,$ac)
 		{
 			parent::Basic($f,$ac);	
 		}
 		
 		function get()
 		{
 			return $this->field . " in ('" .
 					implode("','",$this->value). "')";		
 		}		
 	}
?>
