<?php
	
	$csv = ($_REQUEST['format'] == "csv");
	
	if (($report_id = $_REQUEST['report_id']) == "")
	{
		header("Location: list_reports.php");
		exit();
	}

	include_once "Base.php";
	include_once "../inc_mysql.php";
	
	$report = loadReport($report_id);
	if ($report != NO_REPORT_FOUND)
	{
		include "print.php";
		
		if ($csv)
		{
			header( "content-type: text/csv" );
  			header( "Content-disposition: attachment; filename=Report.csv");  
			printCSV($report);
		}
		else
		{
			printHTML($report);
		} 		
	}
		
?>
<? if (!$csv) { ?>
<a href='list_reports.php'>All Reports</a><br/>
<a href='report_form.php'>New Report</a><br/>
<? } ?>