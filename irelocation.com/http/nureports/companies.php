<?php
/*
 * Created on Mar 5, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
 function loadCompanies()
 {
 	
	 
	 $autos = array("auto","atus");
	 $auto_campaigns = "('".implode("','",$autos)."')";
	 
	 $others = array("tsc","cas","aas","local","pm");
	 $other_campaigns = "('".implode("','",$others)."')";
	 
	 //echo "after implodes<br/>";
	 
	 $auto_sql = "select co.comp_name,lead_id,site_id from marble.campaign as ca join ".
			" movingdirectory.company as co on ca.comp_id = co.comp_id where ".
			" site_id in $auto_campaigns and month > '".date("Ym",strtotime("-9 months"))."'" .
			" group by lead_id,site_id order by site_id,comp_name; "; 
	//  echo "after $auto_sql<br/>";		
	 $rest_sql = "select co.comp_name,lead_id,site_id from movingdirectory.campaign as ca join".
				" movingdirectory.company as co on ca.comp_id = co.comp_id where".
				" site_id in $other_campaigns and month > '".date("Ym",strtotime("-9 months"))."' " .
				" group by lead_id,site_id order by site_id,comp_name;";
	 //echo "after $rest_sql<br/>";
	 $companies = array(); 
	 
	 include_once "../inc_mysql.php";
	 
	 $autos = new mysql_recordset($auto_sql);
	 // echo "after auto query <br/>";
	 while($autos->fetch_array())
	 {
	 	$company = array();
	 	$company[comp_name] = $autos->myarray['comp_name'];
	 	$company[site_id] = $autos->myarray['site_id'];
	 	$company[lead_id] = $autos->myarray['lead_id'];
	 	$companies[] = $company;
	 }
	
	 $rest = new mysql_recordset($rest_sql);
	 //echo "after other query <br/>";
	 while($rest->fetch_array())
	 {
	 	$company = array();
	 	$company[comp_name] = $rest->myarray['comp_name'];
	 	$company[site_id] = $rest->myarray['site_id'];
	 	$company[lead_id] = $rest->myarray['lead_id'];
	 	$companies[] = $company;
	 }
	 
	 return $companies; 
 }	 
 include_once "Base.php";
 
 
 function printCompanies()
 {
 	$companies = loadCompanies();
	 
	 
	 echo "<select name='lead_id'>";
	 $last_campaign = "";
	 
	 foreach($companies as $co)
	 {
	 	if ($last_campaign != $co['site_id'])	
	 	{
	 		if ($last_campaign != "") echo "</optgroup>";
	 		echo "<optgroup label='".getNameOfCampaign($co['site_id'])."'>";
	 		$last_campaign = $co['site_id'];
	 	}
	 	echo "<option value='".$last_campaign."-".$co['lead_id']."'>".$co['comp_name']."</option>";
	 }
	 
	 echo "</optgroup>";
	 echo "</select>";
 }
?>

