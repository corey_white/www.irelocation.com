<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 
$sql = " select 
	name, report_id, class_id, created, campaign, nr.lead_id,
	co.comp_name
from
	irelocation.nureports as nr 
	join 
	movingdirectory.directleads as dl
	on dl.lead_id = nr.lead_id 
	join 
	movingdirectory.company as co 
	on co.comp_id = dl.comp_id";
 	
 	include_once "Base.php";
 	include_once "../inc_mysql.php";
 	
 	$rs = new mysql_recordset($sql);
 	
 	function printListHeader()
 	{
 		?>
 			<tr>
 				<td>Name</td>
 				<td>Class</td>
 				<td>Campaign</td>
 				<td>Company</td>
 				<td>Created</td> 		
 				<td>csv/xls</td>
 				<td>Edit</td>			
 			</tr> 				
 		<?
 	}
 	
 	function printListRow($myarray)
 	{
 		$id = $myarray['report_id'];		
 	
 		?>
 			<tr>
 				<td><a href='run_report.php?report_id=<?= $id ?>'><?= $myarray['name'] ?></a></td> 				
 				<td><?= getNameOfClass($myarray['class_id']) ?></td>
 				<td><?= getNameOfCampaign($myarray['campaign']) ?></td>
 				<td><?= $myarray['comp_name'] ?></td>
 				<td><?= $myarray['created'] ?></td> 
 				<td>
 					<a href='run_report.php?report_id=<?= $id ?>&format=csv'>
 						<img src='images/xls_icon.gif' title='Download to CSV' border='0' /> 						
 					</a>
 				</td>
 				<td>	
 					<a href='alter_report.php?report_id=<?= $id ?>'>
 						<img src='images/update.jpg' title='Update Report' border='0' width='20' height='20' /> 						
 					</a>
				</td>		
 			</tr> 				
 		<?
 	}
 	
 	echo "Click on a Report's name to run it.<br/><br/>";
 	
 	echo "<table border='1'>";
 	printListHeader();
 	while($rs->fetch_array())
 		printListRow($rs->myarray); 		
 	
 	echo "</table>";
 	
 	echo "<br/><a href='report_form.php'>Create Report</a>";
?>
