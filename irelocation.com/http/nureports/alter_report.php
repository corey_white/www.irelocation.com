<?
	include_once "Base.php";

	$report = loadReport($_REQUEST['report_id']);
	
	if ($report == NO_REPORT_FOUND)
	{
		header("Location: list_reports.php");
		exit();
	}
	
?>
<html>
<head>
	<title>Alter a Report </title>
</head>
<body>
	<form action='create_report.php' method='post'>
		<table border='1'>
			<tr><td colspan='4'><strong>Report Type:</strong></td></tr>
			<tr>				
				<td width='50'>&nbsp;</td>
				<td valign='top'>
					<table>
						<tr><td colspan='2'><strong>Auto Reports</strong></td></tr>
						<tr>
							<td><input type="radio" name='type' value="1-auto"/></td>
							<td>Top Auto Leads</td>
						</tr>
						<tr>
							<td><input type="radio" name='type' value="1-atus"/></td>
							<td>Auto-Transport.us</td>
						</tr>
					</table>				
				</td>
				<td valign='top'>
					<table>
						<tr><td colspan='2'><strong>Moving Reports</strong></td></tr>
						<tr>
							<td><input type="radio" name='type' value="2-long"/></td>
							<td>Long Distance</td>
						</tr>
						<tr>
							<td><input type="radio" name='type' value="2-local"/></td>
							<td>Local Movers</td>
						</tr>
					</table>				
				</td>
				<td valign='top'>
					<table>
						<tr><td colspan='2'><strong>Security Reports</strong></td></tr>
						<tr>
							<td><input type="radio" name='type' value="4-us"/></td>
							<td>United States</td>
						</tr>
						<tr>
							<td><input type="radio" name='type' value="4-ca"/></td>
							<td>Canada </td>
						</tr>
						<tr>
							<td><input type="radio" name='type' value="4-aas"/></td>
							<td>Australia</td>
						</tr>
						<tr>
							<td><input type="radio" name='type' value="4-usalarm"/></td>
							<td>US Alarm</td>
						</tr>
					</table>				
				</td>
			</tr>
			<tr><td colspan='4'>&nbsp;</td></tr>
			<tr><td colspan='4'><strong>Report Format:</strong></td></tr>
			<tr>
				<td>&nbsp;</td>
				<td>Formats:</td>
				<td>
					<select name='format'>
						<option value='*'>select..</option>
						<optgroup label='Moving'>
							<option value='0'>Lead Details</option>
							<option value='2'>Grouped by Month</option>
							<option value='1'>Grouped by Date</option>		
						</optgroup>
						<optgroup label='Moving'>			
							<option value='3'>Grouped by Origin</option>
							<option value='4'>Grouped by Destination</option>							
						</optgroup>
						<optgroup label='Security'>
							<option value='5'>Grouped by Res/Com</option>							
							<option value='6'>Grouped by Own/Rent</option>
							<option value='7'>Grouped by State(Province)</option>
						</optgroup>
					</select>
				</td>	
				<td>&nbsp;</td>
			</tr>
			<tr><td colspan='4'>&nbsp;</td></tr>
			<tr><td colspan='4'><strong>Date Range:</strong></td></tr>
			<tr>
				<td>&nbsp;</td>
				<td colspan='3'>Select one method, starting dates are inclusive, while ending dates are exclusive.</td>
			</tr>
			<tr>
				<td align='right'>
					<input type='radio' name='datetype' value='month'
					<?= (($report->datetype == "month")?"checked":"") ?> />
					</td>
				<td>Select Month:</td>
				<td colspan='2'>
					<select name='month'>
						<option value='*'>select..</option>
					<?
						$back = -6;
						$forward = 0;
						for ($i = $forward; $i >= $back; $i--)
						{
							$m = date("Ym",strtotime($i." months"));
							echo "<option value='$m'>".date("Y-m",strtotime($i." months"))."</option>";
						}										
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>
					<input type='radio' name='datetype' value='range'
					<?= (($report->datetype == "range")?"checked":"") ?> />
				</td>
					
				<td>Select Range:</td>
				<td colspan='2'>
					<select name='start_date'>
						<option value='*'>select..</option>
					<?
						$back = -6;
						$forward = 0;
						for ($i = $forward; $i >= $back; $i--)
						{
							$m = date("Ym",strtotime($i." months"));
							echo "<option value='$m'>".date("Y-m",strtotime($i." months"))."</option>";
						}										
					?>
					</select>
					 to 				
					<select name='end_date'>
						<option value='*'>select..</option>
					<?
						$back = -5;
						$forward = 1;
						for ($i = $forward; $i >= $back; $i--)
						{
							$m = date("Ym",strtotime($i." months"));
							echo "<option value='$m'>".date("Y-m",strtotime($i." months"))."</option>";
						}										
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>
					<input type='radio' name='datetype' value='year' 
					<?= (($report->datetype == "year")?"checked":"") ?> />
				</td>
				<td>Select Year:</td>
				<td colspan='2'>
					<select name='year'>
						<option value='*'>select..</option>
					<?
						$back = -1;
						$forward = 0;
						for ($i = $forward; $i >= $back; $i--)
						{
							$m = date("Y",strtotime($i." years"));
							echo "<option value='$m'>".date("Y",strtotime($i." years"))."</option>";
						}										
					?>
					</select>
				</td>
			</tr>
			<tr>
				<td align='right'>
					<input type='radio' name='datetype' value='raw' 
					<?= (($report->datetype == "raw")?"checked":"") ?> />
				</td>
				<td>Raw timeframe:</td>
				<td colspan='2'>
					<input type='text' name='start_date' maxlength='14' />
					 to 				
					<input type='text' name='end_date' maxlength='14' />
				</td>				
			</tr>
			<tr><td colspan='4'>&nbsp;</td></tr>
			<tr><td colspan='4'><strong>Report Details:</strong></td></tr>
			<tr>
				<td>&nbsp;</td>
				<td>Report Title:</td>
				<td colspan='2'>
					<input type='text' name='name' value='<?= $report->name ?>' />
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>Company:</td>
				<td colspan='2'>
					<input type='text' name='lead_id' value='<?= $report->lead_id ?>' /> (lead_id)
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td valign='top'>Summary:</td>
				<td colspan='2'>
					<textarea name='description' rows='5' cols='30' ><?= $report->description ?></textarea>
				</td>				
			</tr>
			<tr><td colspan='4'>&nbsp;</td></tr>
			<tr>
				<td>&nbsp;</td>				
				<td><input type='submit' value='Update Report' /></td>
				<td><input type='submit' value='Save as Copy' /></td>
				<td>&nbsp;</td>
			</tr>
		</table>
		<br/>
		<a href='list_reports.php'>All Reports</a><br/>		
	</form>
</body>
</html>
