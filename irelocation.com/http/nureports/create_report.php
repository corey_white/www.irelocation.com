<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 	extract($_POST);
 	
 	list($class,$campaign) = split("-",$type,2); 
     
    list($lead_id_campaign,$lead_id) = split("-",$lead_id);
    
    if ($lead_id_campaign == "tsc") $lead_id_campaign = "us";
    else if ($lead_id_campaign == "cas") $lead_id_campaign = "ca";
    
    if ($lead_id_campaign != $campaign)
    {
    	header("report_form.php?error=mismatch");
    	exit();
    }
    
 	include_once "Base.php";
 	include_once "../inc_mysql.php";
 
 	switch($class)
	{
		case AUTO_CLASS:
			$report = new Auto($campaign,$lead_id);	
			break;
		case MOVING_CLASS:
			$report = new Moving($campaign,$lead_id);	
			if ($campaign == "pm")
				$report->table = "movingdirectory.quotes";
			else
				$report->table = "movingdirectory.quotes_local";
			break;
		case SECURITY_CLASS:
			$report = new Security($campaign,$lead_id);	
			break;
	}
 
 	switch($datetype)
 	{
 		case "month";
 			$report->setMonth($month);
 			break;
 		case "year";
 			$report->setYear($year);
 			break;
 		case "range";
 		case "raw";
 			$report->setStartDate($start_date);
 			$report->setEndDate($end_date);
 			break; 		
 	}	
 
 	$report->format = $format;
 	$report->name = ($name=="")?"Test Report":$name;
 	$report->description = $description;
	$report_id = $report->saveReport();
 	
 	header("Location: run_report.php?report_id=".$report_id);
?>
