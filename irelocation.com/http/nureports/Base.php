<?php
/*
 * Created on Mar 3, 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 	define(DEFAULT_PARAMETER,"*");
 
 	define(INVALID_F_N_COUNT, -10);
 	define(NO_REPORT_FOUND, -20);
 
	define(LEAD_DISPLAY,0);
	 
	define(LEADS_BY_DAY,1);
	define(LEADS_BY_MONTH,2);
	 
	define(LEADS_BY_ORIGIN_STATE,3); 
	define(LEADS_BY_DEST_STATE,4);
 
 	define(SEC_BY_QUOTE_TYPE,5);
 	define(SEC_BY_OWN_RENT,6);
 	define(SEC_BY_STATE,7);
 	define(USALARM_BY_COMP_COUNT,8);
 	
 
 	define(AUTO_CLASS,1);
 	define(MOVING_CLASS,2);
 	define(SECURITY_CLASS,4);
 	
 	include_once "../inc_mysql.php";
 	
 	function getNameOfClass($class)
 	{
 		switch($class)
 		{
 			case AUTO_CLASS: return "Auto Leads";
 			case MOVING_CLASS: return "Moving Leads";
 			case SECURITY_CLASS: return "Security Leads";
 		}	
 	}
 	
 	function getNameOfCampaign($campaign)
 	{
 		switch($campaign)
 		{
			case "auto": return "Top Auto";
 			case "atus": return "Auto-Transport.us";
 			case "tsc":
 			case "us": return "United States Security";
 			case "cas":
 			case "ca": return "Canada Security";
 			case "aas": return "Australian Security";	
 			case "aas": return "US Alarm Security";
 			case "pm": 
 			case "long": return "Long Distance Movers";
 			case "local": return "Local Movers";
 			case "usalarm": return "Matrix Security";
 			
 			default:
 				return "Other";
 		}	
 	}
 	
 	function getReportQuery($report_id)
 	{
 		$report = loadReport($report_id);
 		if ($report != NO_REPORT_FOUND)
 			return $report->getSQL();		 		
 		else
 			return ""; 		
 	} 	
 	
 	function deleteReport($report_id)
 	{
 		$sql = "delete from irelocation.leads_security where report_id = '$report_id' ";
 		$rs = new mysql_recordset($sql);
 	}
 	
 	function loadReport($report_id)
	{
		$sql = "select * from irelocation.nureports where report_id = '$report_id' ";
		//echo "<!-- $sql --->\n";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$lead_id = $rs->myarray['lead_id'];
			$campaign = $rs->myarray['campaign'];	
			//echo "<!-- $lead_id $campaign --->\n";
			switch($rs->myarray['class_id'])
			{
				case AUTO_CLASS:
					$report = new Auto($campaign,$lead_id);	
					break;
				case MOVING_CLASS:
					$report = new Moving($campaign,$lead_id);
					if ($campaign == "pm")
						$report->table = "movingdirectory.quotes";
					else
						$report->table = "movingdirectory.quotes_local";
					break;						
				case SECURITY_CLASS:
					$report = new Security($campaign,$lead_id);	
					break;
			}
		
			
		
			$report->report_id = $report_id;
			$report->datetype = $rs->myarray['datetype'];
			$report->format = $rs->myarray['format'];
			$report->setStartDate($rs->myarray['start_date']);
			$report->setEndDate($rs->myarray['end_date']);
			$report->description = $rs->myarray['description'];
			$report->name = $rs->myarray['name'];
			$report->created = $rs->myarray['created'];
			$report->last_run = $rs->myarray['last_run'];
			//echo "<!-- ".print_r($report,true)." --->";
			return $report;
		}
		else return NO_REPORT_FOUND;
	}
 	
 
 	class Report
 	{
 		
 		var $datetype;
 		var $name;
 		var $report_id = -1;
 		var $reportType;
 		var $description;
 		var $fields;
 		var $names;
 		
 		var $table;
 		var $campaign;
 		var $lead_id;
 		var $start_date;
 		var $end_date;
 		
 		var $created = "";
 		var $last_run = "";
 		
 		var $format; 		
 		
 		function Report()
 		{
 			$this->fields = array();
 			$this->names = array(); 		
 			
 			$this->format = LEADS_BY_DAY;
 			$this->start_date = date("Ym")."00000000";
 			$this->end_date = date("Ym", strtotime("+1 month"))."00000000"; 				  		
 		}
 		
 		function saveReport()
 		{
 			$sql = "insert into irelocation.nureports set name = '".$this->name."', ".
					" class_id = $this->reportType, format = $this->format, ".
					" start_date = '".$this->start_date."', end_date = '".$this->end_date."', ".
					" campaign = '$this->campaign', description = '$campaignthis->description', ".
					" lead_id = '$this->lead_id'";
			//echo "Save SQL: $sql <br/>";
			$rs = new mysql_recordset($sql);
			$report_id = $rs->last_insert_id();
			$this->report_id = $report_id;
			return $report_id;
 		}
 		
 		function updateRunTime()
 		{
 			if ($report_id < 0) return;
 			$sql = "update irelocation.nureports set last_run = NOW() where " .
 					"report_id = ".$this->report_id;
 			//echo "<!-- $sql -->";
 			$rs = new mysql_recordset($sql);
 		}
 		
 		function add($field,$name=DEFAULT_PARAMETER)
 		{
 			$this->fields[] = $field;
 			if ($name == DEFAULT_PARAMETER)
 				$this->names[] = $field;
 			else
 				$this->names[] = $name;
 		}
 		
 		function setYear($year=DEFAULT_PARAMETER)
 		{
 			$this->datetype = "year";
 			if ($year == DEFAULT_PARAMETER)
 			{
 				$this->start_date = date("Y")."0000000000";
 				$this->end_date = date("Y",strtotime("+1 year"))."0000000000"; 				
 			}
 			else
 			{
 				$this->start_date = substr($year,0,4)."0000000000";
 				//again, if the year has less than 13 months, we're good.
 				$this->end_date = substr($year,0,4)."1300000000"; 				
 			} 			
 		}
 		
 		function setMonth($month=DEFAULT_PARAMETER)
 		{
 			
 			$this->datetype = "month";
 			if ($month == DEFAULT_PARAMETER)
 			{
 				$this->start_date = date("Ym")."00000000";
 				$this->end_date = date("Ym", strtotime("+1 month"))."00000000";
 			}
 			else
 			{
 				$month = substr($month,0,6);
 				$this->start_date = $month;
 				//this will work as long as months don't have more than 31 days :)
 				$this->end_date = $month."32000000";
 			}
 		}	
 		
 		function setStartDate($start)
 		{
 			$this->datetype = "range";
 			while (strlen($start) < 14)
 				$start .= "0";
 			$this->start_date = $start; 			
 		}
 		
 		function setEndDate($end)
 		{
 			$this->datetype = "range";
 			while (strlen($end) < 14)
 				$end .= "0";
 				
 			$this->end_date = $end; 			
 		}
 		
 		function getSQL()
 		{
 			$this->updateRunTime();
 			 		
 			switch($this->format)
 			{

 				case LEADS_BY_DAY: 					
 					if ($this->datetype == "month")
 						return $this->_leadsBy();	
 					else
 					{
 						$groupBy = $groupBy=" substring(received,5,4) ";
 						$display="concat(substring(received,5,2),'/',substring(received,7,2))";
 						return $this->_leadsBy($groupBy,$display,'Month/Day');
 						
 					}	
 				case LEADS_BY_MONTH:
 					$groupBy=" substring(received,5,2) ";
 					$display="substring(received,5,2)";
 					return $this->_leadsBy($groupBy,$display,'Month');
 				case LEADS_BY_ORIGIN_STATE:
 					$groupBy=" origin_state ";
 					$display=" origin_state";
 					return $this->_leadsBy($groupBy,$display,'Origin State');
 					
 				case LEADS_BY_DEST_STATE:
 					$groupBy=" destination_state ";
 					$display=" destination_state";
 					return $this->_leadsBy($groupBy,$display,'Destination State');
 					
 				case LEAD_DISPLAY:
 					return $this->_leadDisplay();
 				
 				default:
 					return array();
 			}
 		} 	 		 	
 		
 		function _qualifications()
 		{
 			return "   where received > '". $this->start_date . "' " .
			" 	and received <  '". $this->end_date . "' ".
			"	and campaign = '". $this->campaign ."' " .
			"   and lead_ids like '%". $this->lead_id ."%' ";
 		}
 		
 		function _leadDisplay()
 		{
 			$sql = "select ";
 			 	
 			if (($fc = count($this->fields)) != count($this->names))
 				return INVALID_F_N_COUNT;	
 			
			for ($i = 0; $i < $fc; $i++)
			{
				$sql .= $this->fields[$i]." as '".$this->names[$i]."'";
				
				if ($i+1 != $fc)
					$sql .= ", ";
			}	
				
			$sql .= " from ".
				$this->table . $this->_qualifications(); 
			 
			return array($sql,$this->names);
 		}
 		
 		function _leadsBy($groupBy=" substring(received,7,2) ",
 						  $display="substring(received,7,2)",
 						  $displayName = "Date")
 		{
 			$sql = "select count(*) 'Lead Count', ".$display. 
 			" '". $displayName."' from ". $this->table . 
 			$this->_qualifications() . " group by ".$groupBy;
			return array($sql,array($displayName,"Lead Count"));
 		}
 	}

 
 	class Auto extends Report
 	{
 		function Auto($campaign,$lead_id=DEFAULT_PARAMETER)
 		{ 	
 			parent::Report();
 			
 			if ($lead_id != DEFAULT_PARAMETER)
 				$this->lead_id = $lead_id;
 			 
 			$this->_init();
 			$this->campaign = $campaign;			
 		} 	
 		
 		function _init()
 		{
 			$this->reportType = AUTO_CLASS;
 			$this->table = "marble.auto_quotes";	
 			$this->fields = array("quote_id","formatReceived(received)","lname","email","concat(origin_city,', ',origin_state)","concat(destination_city,', ',destination_state)","concat(vmake,' ',vmodel)", "est_move_date");
			$this->names = array("Quote #","Timestamp","Customer Last Name","Email","Origin","Destination","Vehicle","Moving Date");  
 		} 		
 	}
 	
 	class AutoTransport extends Auto
 	{
 		function AutoTransport($lead_id=DEFAULT_PARAMETER)
 		{
 			parent::Auto("atus",$lead_id); 		
 			$this->name = "Auto-Transport Leads";	
 		} 			
 	}
 	
 	class TopAuto extends Auto
 	{
 		function TopAuto($lead_id=DEFAULT_PARAMETER)
 		{
 			parent::Auto("auto",$lead_id);
 			$this->name = "Top Auto Leads"; 		 			 
 		} 		
 	}
 	
 	class Moving extends Report
 	{
 		function Moving($campaign,$lead_id=DEFAULT_PARAMETER)
 		{ 	
 			parent::Report();
 			
 			if ($lead_id != DEFAULT_PARAMETER)
 				$this->lead_id = $lead_id;
 			 
 			$this->_init();
 			$this->campaign = $campaign;			
 		} 	
 		
 		function _init()
 		{
 			$this->reportType = MOVING_CLASS;			
 			$this->fields = array("quote_id","formatReceived(received)","email","concat(origin_city,', ',origin_state)","concat(destination_city,', ',destination_state)", "est_move_date");
			$this->names = array("Quote #","Timestamp","Email","Origin","Destination","Moving Date");  
 		}
 		
 		function _qualifications()
 		{
 			return "   where received > '". $this->start_date . "' " .
			" 	and received <  '". $this->end_date . "' ".			
			"   and lead_ids like '%". $this->lead_id ."%' ";
 		} 		 		
 	}
 	
 	class Local extends Moving
 	{
 		function Local($lead_id=DEFAULT_PARAMETER)
 		{	
 			parent::Moving($lead_id);
 			$this->table = "movingdirectory.quotes_local";
 			$this->name = "Local Moving Leads"; 	
 		} 	
 	}

 	class Long extends Moving
 	{
 		function Long($lead_id=DEFAULT_PARAMETER)
 		{	
 			parent::Moving($lead_id);
 			$this->table = "movingdirectory.quotes";
 			$this->name = "Long Distance Moving Leads";	
 		} 	
 	}
 	
 	class Security extends Report
 	{
 		var $quote_type = DEFAULT_PARAMETER;	
 	
 		function Security($campaign,$lead_id=DEFAULT_PARAMETER)
 		{ 	
 			parent::Report();
 			
 			if ($lead_id != DEFAULT_PARAMETER)
 				$this->lead_id = $lead_id; 
 			$this->_init();
 			$this->campaign = $campaign;			
 		} 	
 		
 		function _init()
 		{
 			$this->reportType = SECURITY_CLASS;
 			$this->table = "irelocation.leads_security"; 	 			
 			$this->fields = array("quote_id","formatReceived(received)","name2","email","concat(city,', ',state_code)","own_rent","building_type","current_needs");
			$this->names = array("Quote #","Timestamp","Customer/Business","Email","Location","Own/Rent","Building","Service Requested");  
 		}
 		
 		function setQuoteType($qt)
 		{
 			$this->quote_type = $qt;
 		}
 		
 		function _qualifications()
 		{
 			$sql = "   where received > '". $this->start_date . "' " .
			" 	and received <  '". $this->end_date . "' ".			
			"   and lead_ids like '%". $this->lead_id ."%' ";
			if ($this->quote_type != DEFAULT_PARAMETER)
				$sql .= " and quote_type = '$this->quote_type' ";				
			return $sql;
 		} 		 		
 		
 		function  getSQL()
 		{
 			$sql = parent::getSQL();
 			if (count($sql) != 0) return $sql;
 			
 			//echo "<!-- in security getSQL() --->\n";
 			
 			switch($this->format)
 			{
 				case SEC_BY_OWN_RENT:
 					$groupBy = " own_rent ";
 					$display = " own_rent";
 					return $this->_leadsBy($groupBy,$display,'Own/Rent');
 				case SEC_BY_STATE:
 					$groupBy = " state_code ";
 					$display = " state_code";
 					return $this->_leadsBy($groupBy,$display, 'State/Province');
 				case SEC_BY_QUOTE_TYPE:
 					$groupBy = " quote_type ";
 					$display = " quote_type ";
 					return $this->_leadsBy($groupBy,$display,'Com/Res');
 				default:
 					return array();
 			}
 		}
 	}
?>