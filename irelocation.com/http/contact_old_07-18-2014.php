<?php session_start();


// Setup Error messages

$error_msg = $_REQUEST["msg"];
$msgs["name"] = "Please enter your Full Name.";
$msgs["phone"] = "Please enter your Phone Number.";
$msgs["company"] = "Please enter your Company Name.";
$msgs["email"] = "Please enter your Email Address.";
$msgs["lead_type"] = "Please select your type of lead.";
$msgs["thankyou"] = "Thank you. We will contact you shortly.";
$msgs["city"] = "Please enter a City.";
$msgs["state"] = "Please enter a State.";
$msgs["code"] = "The code you entered is incorrect.";
			
$msg = $msgs[$error_msg];

extract($_POST);


// is the form ready to check and send            
if ($send == "yes") { 
	
	function failInjection($type)
	{
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$output = strtolower(print_r($_POST,true));
		
		if ($type == "javascript")
		{
			if (substr_count($output,"<script") > 0)
				$content = substr($output,strpos($output,"<script"));						
			else
				$content = substr($output,strpos($output,"onload="));
		}
		else if ($type == "link")
			$content = substr($output,strpos($output,"href="));	
		else if  ($type == "serverside")
		{
			if (substr_count($output,"<?") > 0)
				$content = substr($output,strpos($output,"<?"));		
			else
				$content = substr($output,strpos($output,"<%"));		
		}
		else if ($type == "dirty")
		{
			$content = "Dirty words.";
			header("Location: contact.php?msg=thankyou");
			exit;
		}	
		$content = str_replace("'","&prime;",$content);
		$content = str_replace("\"","&quot;",$content);
		
		$sql = "insert into marble.injectors (site,ip,received,content) values ".
				"('tm','$ip','".date("YmdHis")."','$content'); ";
				
		$rs = new mysql_recordset($sql);
		
		header("Location: contact.php?msg=thankyou");
		exit;
	}

	function testInjection()
	{
		$output = strtolower(print_r($_POST,true));
		if(substr_count($output,"<script") > 0 || substr_count($output,"onload=") > 0)//javascript injection
			failInjection(javascript);
		if (substr_count($output,"href=") > 0)//link injection
			failInjection('link');		
		if (substr_count($output,"<?") > 0 || substr_count($output,"<%") > 0 )//server side?
			failInjection('serverside');
			
		$words = array("fuck"," ass"," shit ","damn ","bitch", "shit ");
		foreach($words as $dirty)
			if (substr_count($output,$dirty) > 0)
				failInjection("dirty");		
	}		
	testInjection();//if it gets past this the input 'should' be ok.	
 
	// error checking
	if ($name == "") {
		header("Location: contact.php?msg=name&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else if ($company == "") {
		header("Location: contact.php?msg=company&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else if ($email == "") {
		header("Location: contact.php?msg=email&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else if ($phone == "") {
		header("Location: contact.php?msg=phone&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else if ($city == "") {
		header("Location: contact.php?msg=city&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else if ($state == "") {
		header("Location: contact.php?msg=state&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
		exit;
	} else {
		include("captcha/securimage.php");
		$img = new Securimage();
		$valid = $img->check($_POST['code']);

		if($valid == true) {
			//passed error checking
			// to email address
			$to = "vsmith@irelocation.com, katrina@irelocation.com, travis@irelocation.com, markb@irelocation.com";
			// The subject
			$subject = "iRelocation SEO Lead Form";
			// The message
			$message = "Name: ".$name."\n\n";
			$message .= "Company: ".$company."\n\n";
			$message .= "Email: ".$email."\n\n";
			$message .= "Phone: ".$phone."\n\n";
			$message .= "City: ".$city."\n\n";
			$message .= "State: ".$state."\n\n";
			$message .= "Interest: ".$interest."\n\n";
			$message .= "Comment: ".$comment;
			mail($to, $subject, $message, "From: $email");
			header("Location: contact.php?msg=thankyou&send=no");
			exit;
		} else {
			header("Location: contact.php?msg=code&send=no&name=$name&city=$city&phone=$phone&email=$email&state=$state&interest=$interest&comment=$comment&company=$company");
			exit;
		}
		
	}
	
} else { 

$pageTitle = "Contact Us | iRelo";

$metaDescription = "My page description, less than 160 characters.";

$metaTags = "a list, of appropriate, tags, separated with, commas";

include("irelocation/includes/header.php");


 ?>  
  
  <div id="subpagetop">
    <h1>Contact Us </h1>
</div><div id="subpage">
  <img src="irelocation/images/contact_pic.jpg" alt="Web Design" width="878" height="223" style="margin-left:13px" /><br />


      
 
<div id="form">
   
      <br />
    <p>For questions, comments or more information, please use the form below:<br />
        <br />
        <strong>iRelocation Network</strong><br />
        P.O. Box 11018<br />
        Tempe, AZ 85284<br />
      P.&nbsp; 480.785.7400      <br />
      <br />
      
     
      
      
      <strong>Fields marked with an * are required. </strong></p>
      <form id="form1" method="post" action="contact.php">
      <input name="send" type="hidden" value="yes" />
        <table width="75%" border="0" cellspacing="3" cellpadding="3">
        <?
      	$errmsg='
      	<tr>
      	  <td align="center" colspan="2"><font color="red">'.$msg.'</font></td>
      	</tr>
		';
      	if($msg!="")
      	{
      		echo $errmsg;
      	}
    ?>
          <tr>
            <td style="width:48%">Name:&nbsp;</td>
            <td style="width:52%"><input name="name" type="text" id="name" value="<? echo $_REQUEST["name"]; ?>" /> 
            <span class="red">*</span> </td>
          </tr>
		  <tr>
            <td style="width:48%">Company:&nbsp;</td>
            <td style="width:52%"><input name="company" type="text" id="company" value="<? echo $_REQUEST["company"]; ?>" /> 
            <span class="red">*</span> </td>
          </tr>
          <tr>
            <td>Phone Number: </td>
            <td><input name="phone" type="text" id="phone" value="<? echo $_REQUEST["phone"]; ?>" /> 
            <span class="red"> *</span> </td>
          </tr>
          <tr>
            <td>Email:</td>
            <td><input name="email" type="text" id="email" value="<? echo $_REQUEST["email"]; ?>" /> 
            <span class="red">*</span> </td>
          </tr>
          <tr>
            <td>City: </td>
            <td><input name="city" type="text" id="city" value="<? echo $_REQUEST["city"]; ?>" /></td>
          </tr>
          <tr>
            <td>State:</td>
            <td><select name="state" >
                    
                    <option value="Please Select"<? if($state=='Please Select'){echo " selected";}?>>Please Select</option>
                    <option value="Alabama"<? if($state=='Alabama'){echo " selected";}?>>Alabama</option>
                    <option value="Alaska"<? if($state=='Alaska'){echo " selected";}?>>Alaska</option>
                    <option value="Arizona"<? if($state=='Arizona'){echo " selected";}?>>Arizona</option>
                    <option value="Arkansas"<? if($state=='Arkansas'){echo " selected";}?>>Arkansas</option>
                    <option value="California"<? if($state=='California'){echo " selected";}?>>California</option>
                    <option value="Colorado"<? if($state=='Colorado'){echo " selected";}?>>Colorado</option>
                    <option value="Connecticut"<? if($state=='Connecticut'){echo " selected";}?>>Connecticut</option>
                    <option value="Delaware"<? if($state=='Delaware'){echo " selected";}?>>Delaware</option>
                    <option value="Florida"<? if($state=='Florida'){echo " selected";}?>>Florida</option>
                    <option value="Georgia"<? if($state=='Georgia'){echo " selected";}?>>Georgia</option>
                    <option value="Hawaii"<? if($state=='Hawaii'){echo " selected";}?>>Hawaii</option>
                    <option value="Idaho"<? if($state=='Idaho'){echo " selected";}?>>Idaho</option>
                    <option value="Illinois"<? if($state=='Illinois'){echo " selected";}?>>Illinois</option>
                    <option value="Indiana"<? if($state=='Indiana'){echo " selected";}?>>Indiana</option>
                    <option value="Iowa"<? if($state=='Iowa'){echo " selected";}?>>Iowa</option>
                    <option value="Kansas"<? if($state=='Kansas'){echo " selected";}?>>Kansas</option>
                    <option value="Kentucky"<? if($state=='Kentucky'){echo " selected";}?>>Kentucky</option>
                    <option value="Louisiana"<? if($state=='Louisiana'){echo " selected";}?>>Louisiana</option>
                    <option value="Maine"<? if($state=='Maine'){echo " selected";}?>>Maine</option>
                    <option value="Maryland"<? if($state=='Maryland'){echo " selected";}?>>Maryland</option>
                    <option value="Massachusetts"<? if($state=='Massachusetts'){echo " selected";}?>>Massachusetts</option>
                    <option value="Michigan"<? if($state=='Michigan'){echo " selected";}?>>Michigan</option>
                    <option value="Minnesota"<? if($state=='Minnesota'){echo " selected";}?>>Minnesota</option>
                    <option value="Mississippi"<? if($state=='Mississippi'){echo " selected";}?>>Mississippi</option>
                    <option value="Missouri"<? if($state=='Missouri'){echo " selected";}?>>Missouri</option>
                    <option value="Montana"<? if($state=='Montana'){echo " selected";}?>>Montana</option>
                    <option value="Nebraska"<? if($state=='Nebraska'){echo " selected";}?>>Nebraska</option>
                    <option value="Nevada"<? if($state=='Nevada'){echo " selected";}?>>Nevada</option>
                    <option value="New Hampshire"<? if($state=='New Hampshire'){echo " selected";}?>>New Hampshire</option>
                    <option value="New Jersey"<? if($state=='New Jersey'){echo " selected";}?>>New Jersey</option>
                    <option value="New Mexico"<? if($state=='New Mexico'){echo " selected";}?>>New Mexico</option>
                    <option value="New York"<? if($state=='New York'){echo " selected";}?>>New York</option>
                    <option value="North Carolina"<? if($state=='North Carolina'){echo " selected";}?>>North Carolina</option>
                    <option value="North Dakota"<? if($state=='North Dakota'){echo " selected";}?>>North Dakota</option>
                    <option value="Ohio"<? if($state=='Ohio'){echo " selected";}?>>Ohio</option>
                    <option value="Oklahoma"<? if($state=='Oklahoma'){echo " selected";}?>>Oklahoma</option>
                    <option value="Oregon"<? if($state=='Oregon'){echo " selected";}?>>Oregon</option>
                    <option value="Pennsylvania"<? if($state=='Pennsylvania'){echo " selected";}?>>Pennsylvania</option>
                    <option value="Puerto Rico"<? if($state=='Puerto Rico'){echo " selected";}?>>Puerto Rico</option>
                    <option value="Rhode Island"<? if($state=='Rhode Island'){echo " selected";}?>>Rhode Island</option>
                    <option value="South Carolina"<? if($state=='South Carolina'){echo " selected";}?>>South Carolina</option>
                    <option value="South Dakota"<? if($state=='South Dakota'){echo " selected";}?>>South Dakota</option>
                    <option value="Tennessee"<? if($state=='Tennessee'){echo " selected";}?>>Tennessee</option>
                    <option value="Texas"<? if($state=='Texas'){echo " selected";}?>>Texas</option>
                    <option value="Utah"<? if($state=='Utah'){echo " selected";}?>>Utah</option>
                    <option value="Vermont"<? if($state=='Vermont'){echo " selected";}?>>Vermont</option>
                    <option value="Virginia"<? if($state=='Virginia'){echo " selected";}?>>Virginia</option>
                    <option value="Washington"<? if($state=='Washington'){echo " selected";}?>>Washington</option>
                    <option value="Washington DC"<? if($state=='Washington DC'){echo " selected";}?>>Washington DC</option>
                    <option value="West Virginia"<? if($state=='West Virginia'){echo " selected";}?>>West Virginia</option>
                    <option value="Wisconsin"<? if($state=='Wisconsin'){echo " selected";}?>>Wisconsin</option>
                    <option value="Wyoming"<? if($state=='Wyoming'){echo " selected";}?>>Wyoming</option>
                  </select></td>
          </tr>
          <tr>
            <td>Area(s) of Interest:</td>
            <td><select name="interest" size="1" id="interest">
              <option value="General" <? if($interest=='General'){echo " selected";}?>>General</option>
              <option value="Lead Generation" <? if($interest=='Lead Generation'){echo " selected";}?>>Lead Generation</option>
              <option value="SEO" <? if($interest=='SEO'){echo " selected";}?>>SEO</option>
              <option value="Pay Per Click" <? if($interest=='Pay Per Click'){echo " selected";}?>>Pay Per Click</option>
              <option value="Web Development" <? if($interest=='Web Development'){echo " selected";}?>>Web Development</option>
              <option value="Other" <? if($interest=='Other'){echo " selected";}?>>Other</option>
            </select>            </td>
          </tr>
          <tr>
            <td>Comments:</td>
            <td><label>
              <textarea name="comment" cols="50" rows="4" id="comment"><? echo $_REQUEST["comment"]; ?></textarea>
            </label></td>
          </tr>
          <tr>
            <td colspan="2"><img id="siimage" style="padding-right: 5px; border: 0; float:left" src="/captcha/securimage_show.php?sid=<?php echo md5(time()) ?>" />
            
			 <object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,0,0" width="19" height="19" id="SecurImage_as3" align="middle">
			    <param name="allowScriptAccess" value="sameDomain" />
			    <param name="allowFullScreen" value="false" />
			    <param name="movie" value="/captcha/securimage_play.swf?audio=securimage_play.php&bgColor1=#777&bgColor2=#fff&iconColor=#000&roundedCorner=5" />
			    <param name="quality" value="high" />
			
			    <param name="bgcolor" value="#ffffff" />
			    <embed src="/captcha/securimage_play.swf?audio=/captcha/securimage_play.php&bgColor1=#777&bgColor2=#fff&iconColor=#000&roundedCorner=5" quality="high" bgcolor="#ffffff" width="19" height="19" name="SecurImage_as3" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
			  </object>
			  <br />
        
        <!-- pass a session id to the query string of the script to prevent ie caching -->
        <a tabindex="-1" style="border-style: none" href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = '/captcha/securimage_show.php?sid=' + Math.random(); return false"><img src="securimage/images/refresh.gif" alt="Reload Image" border="0" onclick="this.blur()" align="bottom" /></a>
		</td>
          </tr>
		  <tr>
            <td style="width:48%">Code:&nbsp;</td>
            <td><input type="text" name="code" size="12" /></td>
            
          </tr>
          <tr>
            <td colspan="2">
              <div style="text-align:center">
              <label>  <input type="submit" name="Submit" value="Submit" /></label>
                </div>
            </td>
          </tr>
        </table>
        <br />
      </form>
  </div>
<p>&nbsp;</p>
  </div>
  <div id="subpagebottom"></div>
  </div>
<?php
include'irelocation/includes/footer.php';
} 
?>