<?
	session_start();
	include "../inc_mysql.php";

	function fail($why,$link=false)
	{
		if ($link)				
			header("Location: createlisting.php?$why&linkinjection");
		else
			header("Location: createlisting.php?$why");
	}

	function injectionTest($in)
	{
		if (substr_count($in,"href=") > 0 || substr_count($in,"url=") > 0)
			return false;
		else
		{
			$in = str_replace("'","\'",$in);
			$in = str_replace("insert","add",$in);
			$in = str_replace("update","change",$in);
			$in = str_replace("delete","remove",$in);
			$in = str_replace("create","make",$in);
			$in = str_replace("drop","release",$in);
			$in = str_replace("select","choose",$in);
		}
		return true;
	}	
	
	//trim and scrub data.
	$keys = array_keys($_POST);
	foreach($keys as $k)
	{
		$_POST[$k] = trim($_POST[$k]);
		if (injectionTest($_POST[$k]))
			$_SESSION[$k] = $_POST[$k];
		else
			fail($k,true);
	}

	extract($_POST);

	$zip_code = ereg_replace("[^0-9]","",$zip_code);	

	if (strlen($name) == 0)	fail(name);
	if (strlen($description) == 0)	fail(description);
	if (strlen($address) == 0)	fail(address);
	if (strlen($city) == 0)	fail(city);
	if (strlen($zip_code) < 5)	fail(zip_code);
	if (strlen($mlsnumber) == 0 || !is_numeric($mlsnumber))	fail(mlsnumber);
	if (strlen($garage) == 0)	fail(garage);
		
	$sql = "select city,state,zip from movingdirectory.area_codes where zip = '$zip_code' and state = '$state' and city = '$city' limit 1";
	$rs = new mysql_recordset($sql);
?>