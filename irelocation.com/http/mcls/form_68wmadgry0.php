<!-- The data encoding type, enctype, MUST be specified as below -->
<? 
	$msg = $_REQUEST['error'];
	$errors[filesize] = "Your file is too large. please resize it.";
	$errors[nofile] = "Please specify a file to upload..";
	$errors[extension] = "Your images must be in JPEG format.";
	$errors[dimensions] = "Your image must be smaller than 500 x 500 pixels.";
	$errors[nonimage] = "The file you upload must be an image.";
	$errors[description] = "Please provide a breif description of your image.";
	$error_message = $errors[$msg];
?>
<strong>Please use this form to upload images of your listing.</strong><Br/>
<form enctype="multipart/form-data" action="upload.php" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
	<input type="hidden" name="listing_id" value="<?= rand(0,10000) ?>" /><? //random for now... ?>
    <table width="450">
		<? if (strlen($error_message) > 0){ ?>
		<tr>
		<td colspan="2" align="left"><font color="#FF0000"><?= $error_message ?></font></td>
		</tr>
		<? } ?>
    	<tr>
			<td align="left">Image to upload:</td>
			<td align="left"><input name="userfile" type="file" /></td>
		</tr>	

		<tr>
			<td align="left">Describe this image:</td>
			<td align="left"><textarea name="image_description" rows="5" cols="20"></textarea></td>
		</tr>	
		<tr>
			<td colspan="2" align="left">This image can have a maximum file size of 300 Kilobytes, and must have dimensions smaller than 500x500 pixels</td>
		</tr>
		<? /*
		<tr>
			<td><img src="captcha.php" border="1" /> validation code: </td>
			<td><input type='text' name="captcha" maxlength="5" size="7"/></td>
		</tr>
		*/ ?>
		<tr>
			<td colspan="2" align="center"> <input type="submit" value="Send File" /></td>
		</tr>
	</table>
</form>
<!-- MMDW:success -->