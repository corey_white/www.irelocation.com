<!-- The data encoding type, enctype, MUST be specified as below -->
<!--MMDW 0 --><? 
	$msg = $_REQUEST['error'];
	$errors[filesize] = "Your file is too large. please resize it.";
	$errors[nofile] = "Please specify a file to upload..";
	$errors[extension] = "Your images must be in JPEG format.";
	$errors[dimensions] = "Your image must be smaller than 500 x 500 pixels.";
	$errors[nonimage] = "The file you upload must be an image.";
	$errors[description] = "Please provide a breif description of your image.";
	$error_message = $errors[$msg];
?><!--MMDW 1 -->
<strong>Please use this form to upload images of your listing.</strong><Br/>
<form mmdw=0 enctype="multipart/form-data" action="upload.php" method="POST">
	<input mmdw=1 type="hidden" name="MAX_FILE_SIZE" value="300000" />
	<input mmdw=2 type="hidden" name="listing_id" value="<?= rand(0,10000) ?>" /><!--MMDW 2 --><? //random for now... ?><!--MMDW 3 -->
    <table mmdw=3 width="450">
		<!--MMDW 4 --><? if (strlen($error_message) > 0){ ?><!--MMDW 5 -->
		<tr>
		<td mmdw=4 colspan="2" align="left"><font mmdw=5 color="#FF0000"><!--MMDW 6 --><?= $error_message ?><!--MMDW 7 --></font></td>
		</tr>
		<!--MMDW 8 --><? } ?><!--MMDW 9 -->
    	<tr>
			<td mmdw=6 align="left">Image to upload:</td>
			<td mmdw=7 align="left"><input mmdw=8 name="userfile" type="file" /></td>
		</tr>	

		<tr>
			<td mmdw=9 align="left">Describe this image:</td>
			<td mmdw=10 align="left"><textarea mmdw=11 name="image_description" rows="5" cols="20"></textarea></td>
		</tr>	
		<tr>
			<td mmdw=12 colspan="2" align="left">This image can have a maximum file size of 300 Kilobytes, and must have dimensions smaller than 500x500 pixels</td>
		</tr>
		<!--MMDW 10 --><? /*
		<tr>
			<td><img src="captcha.php" border="1" /> validation code: </td>
			<td><input type='text' name="captcha" maxlength="5" size="7"/></td>
		</tr>
		*/ ?><!--MMDW 11 -->
		<tr>
			<td mmdw=13 colspan="2" align="center"> <input mmdw=14 type="submit" value="Send File" /></td>
		</tr>
	</table>
</form>
<!-- MMDW:success -->