<?
	include_once "../../inc_mysql.php";
	
	extract($_POST);
	function fail($reason)
	{
		header("Location: index.php?page=quote&listing_id=".$_POST['listing_id']."&error=".$reason);
		exit();
	}

	if (strlen($fname) < 3) fail(fname);
	if (strlen($lname) < 3) fail(lname);
	if (strlen($email) < 6) fail(email);

	$buying_valid = ($buy_state != "xx" && $buy_value != "xx");
	$selling_valid = ($sell_state != "xx" && $sell_value != "xx");

	if ($buy_state != "xx" && $buy_value == "xx") fail(buy_value);//partial
	if ($buy_state == "xx" && $buy_value != "xx") fail(buy_state);//partial

	if ($sell_state != "xx" && $sell_value == "xx") fail(sell_value);//partial
	if ($sell_state == "xx" && $sell_value != "xx") fail(sell_state);//partial
	
	if (!$buying_valid && !$selling_valid) fail(select);//nothing

	if ($buying_valid && $selling_valid)
	{
		$leadTypeCode = 1007;
		$location = " moveState = '$buy_state', purchasePrice = '$buy_value', ".
					" sellState = '$sell_state', sellPrice = '$sell_value' ";
	}
	else if ($buying_valid)
	{
		$location = " moveState = '$buy_state', purchasePrice = '$buy_value' ";
		$leadTypeCode = 1005;
	}
	else if ($selling_valid)
	{
		$location = " sellState = '$sell_state', sellPrice = '$sell_value' "; 
		$leadTypeCode = 1006;
	}
	$leadStatusCode = 1000;
	$leadSourceCode = 1075;
	$campaign = "mcls";
	
	$sql = "insert into movecomp.leads_prd set firstName = '$fname', lastName = '$lname', email='$email',".
			" homePhone = '$phone', cellPhone = '$altphone', $location, leadStatusCode = '1000', ".
			" leadSourceCode = 1075, campaign = 'mcls' leadDate = NOW(), clickThruDate = NOW(), qualData = NOW();";
	$rs = new movecompanion($sql);
	if ($listing_id > 0)
	{
		$_SESSION['haveform'] = true;
		header("Location: index.php?page=view&listing_id=$listing_id");
		exit();
	}			
?>
	