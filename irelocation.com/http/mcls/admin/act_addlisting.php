<?
	session_start();
	include "../../inc_mysql.php";

	function fail($why,$link=false)
	{
		if ($link)				
			header("Location: index.php?page=add&error_message=$why&linkinjection");
		else
			header("Location: index.php?page=add&error_message=$why");
		exit();
	}

	function injectionTest($in)
	{
		if (substr_count($in,"href=") > 0 || substr_count($in,"url=") > 0)
			return false;
		else
		{
			$in = str_replace("'","\'",$in);
			$in = str_replace("insert","add",$in);
			$in = str_replace("update","change",$in);
			$in = str_replace("delete","remove",$in);
			$in = str_replace("create","make",$in);
			$in = str_replace("drop","release",$in);
			$in = str_replace("select","choose",$in);
		}
		return true;
	}	
	
	//trim and scrub data.
	$keys = array_keys($_POST);
	foreach($keys as $k)
	{
		$_POST[$k] = trim($_POST[$k]);
		if (injectionTest($_POST[$k]))
			$_SESSION[$k] = $_POST[$k];
		else
			fail($k,true);
	}

	extract($_POST);

	$zip = ereg_replace("[^0-9]","",$zip);	

	if (strlen($name) == 0)	fail(name);
	if (strlen($description) == 0)	fail(description);
	if (strlen($address) == 0)	fail(address);
	if (strlen($city) == 0)	fail(city);
	if (strlen($zip) < 5) fail(zip_code);
	if ($sellingprice == "xx") fail(sellingprice);
	if ($pool != "Y" && $pool != "N") fail(pool);
	//if (strlen($mlsnumber) == 0 || !is_numeric($mlsnumber))	fail(mlsnumber);
	if ($garage == "xx")	fail(garage);
		
	$sql = "select city,state,zip from movingdirectory.zip_codes where zip = '$zip' and state = '$state' and city = '$city' limit 1";
	$rs = new mysql_recordset($sql);
	if (!$rs->fetch_array()) fail(location);
	else
	{
		if (is_array($extra_features))
			$extra_features = implode(",",$extra_features);
		$now = date("Ymdhis");
		$sql = "insert into irelocation.mclistings set name = '$name', description = '$description', bedrooms = '$bedrooms', ".
				" bathrooms = '$bathrooms', floors = '$floors', address = '$address', city = '$city', state = '$state', ".
				" zip = '$zip', sqr_footage = '$sqr_footage', age = '$age', extra_features = '$extra_features', lot_size = '$lot_size', ".
				" mlsnumber = '$mlsnumber', agent_id = '$agent_id', listingDate = '$now', garage = '$garage', ".
				" dwelling_type = '$dwelling_type', sellingprice ='$sellingprice'; ";
		//echo $sql;
		$rs = new mysql_recordset($sql);
		$insert_id = $rs->last_insert_id();
		header("Location: index.php?page=addpictures&listing_id=$insert_id");
		exit();
	}
?>