<body>
<!-- The data encoding type, enctype, MUST be specified as below -->
<? 
	$msg = $_REQUEST['error'];
	$errors[filesize] = "Your file is too large. please resize it.";
	$errors[nofile] = "Please specify a file to upload..";
	$errors[extension] = "Your images must be in JPEG format.";
	$errors[dimensions] = "Your image must be smaller than 500 x 500 pixels.";
	$errors[nonimage] = "The file you upload must be an image.";
	$errors[description] = "Please provide a breif description of your image.";
	$error_message = $errors[$msg];
?>

<form enctype="multipart/form-data" action="index.php?action=upload" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="300000" />
	<input type="hidden" name="listing_id" value="<?= $listing_id ?>" />
    <table width="450" class="AdminTable">
		<tr class="BodyHeader2">
			<td colspan="3" align="center">Please use this form to upload images of your listing.</td>
		</tr>
		<tr>
			<td width="22%">&nbsp;</td>
			<td width="33%">&nbsp;</td>
			<td width="*">&nbsp;</td>
		</tr>
		<? if (strlen($error_message) > 0){ ?>
		<tr>
		<td colspan="3" align="left"><font color="#FF0000"><?= $error_message ?></font></td>
		</tr>
		<? } ?>
		<tr class="BodySmall">
			<td>&nbsp;</td>
			<td colspan="2" align="left">Images will display in the order they are uploaded.<br/> The first image uploaded will be used as the primary image.</td>			
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
    	<tr class="BodySmall">
			<td align="right">Image #1:</td>
			<td align="left"><input name="userfile[]" type="file" /></td>
			<td>&nbsp;</td>
		</tr>	
		<tr class="BodySmall">
			<td align="right">Image #2:</td>
			<td align="left"><input name="userfile[]" type="file" /></td>
			<td>&nbsp;</td>
		</tr>	
		<tr class="BodySmall">
			<td align="right">Image #3:</td>
			<td align="left"><input name="userfile[]" type="file" /></td>
			<td>&nbsp;</td>
		</tr>
		<tr class="BodySmall">
			<td align="right">Image #4:</td>
			<td align="left"><input name="userfile[]" type="file" /></td>
			<td>&nbsp;</td>
		</tr>	
		<tr class="BodySmall">
			<td align="right">Image #5:</td>
			<td align="left"><input name="userfile[]" type="file" /></td>
			<td>&nbsp;</td>
		</tr>	
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr class="BodySmall">
			<td>&nbsp;</td>
			<td colspan="2" align="left">This image can have a maximum file size of 300 Kilobytes,<Br/> and must have dimensions smaller than 500x500 pixels</td>			
		</tr>
		<? /*
		<tr>
			<td><img src="captcha.php" border="1" /> validation code: </td>
			<td><input type='text' name="captcha" maxlength="5" size="7"/></td>
		</tr>
		*/ ?>
		<tr class="BodySmall">
			<td colspan="2" align="center"> <input type="submit" value="Upload Images" /></td>
		</tr>
	</table>
</form>
</body>
