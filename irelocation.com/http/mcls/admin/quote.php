<body>
<form action="index.php?action=submitquote" method="post">
<? if ($listing_id > 0) { ?>
	<input type="hidden" name="listing_id" value="<?= $listing_id ?>" />
<? } ?>
<table width="600" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4">Quote Form Verbiage!</td>
	</tr>
	<Tr><td colspan="4">&nbsp;</td></Tr>
	<? if (strlen($_REQUEST['error']) > 0) 
	{ 
		$msg  = $_REQUEST['error'];
		$msgs["fname"] = "Please enter your first name.";
		$msgs["lname"] = "Please enter your last name.";
		$msgs["phone"] = "Please enter your phone number.";
		$msgs["invalid_phone"] = "Please enter a valid phone number.";
		$msgs["email"] = "Please enter your email address.";				
		$msgs["buy_state"] = "Please Select where you would like to Buy.";
		$msgs["sell_state"] = "Please Select what State you are selling in.";
		$msgs["buy_value"] = "Please Select how much you would like to spend.";
		$msgs["sell_value"] = "Please Select how much you would like to receive for your house.";
		$msgs["select"] = "Please Select how we can help you.";
		$msgs["duplicate"] = "We see that you have already submitted a quote, please wait for our agents to call you.";
		$msg= $msgs[$msg];
	

	?>
	<tr>
		<td colspan="4"><font color="#FF0000"><?= $msg ?></font></td>
	</tr>
	<Tr><td colspan="4">&nbsp;</td></Tr>
	<? } ?>
	<tr>
		<td align="left">First Name:</td>
		<td align="left"><input type="text" name="fname" value="<?= $fname ?>" /></td>
		<td	align="right">Last Name:</td>
		<td align="left"><input type="text" name="lname" value="<?= $lname ?>" /></td>
	</tr>
	<tr>
		<td align="left">E-mail Address:</td>
		<td align="left" colspan="3"><input type="text" name="email" value="<?= $email ?>" /></td>
	</tr>
	<tr>
		<td align="left">Phone Number:</td>
		<td align="left"><input type="text" name="phone" value="<?= $phone ?>" /></td>
		<td	align="right">Alt. Phone:</td>
		<td align="left"><input type="text" name="altphone" value="<?= $altphone ?>" /></td>
	</tr>	
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><td colspan="4" align="left">Buying Info:</td></tr>
	<tr>
		<td align="right">State:</td>
		<td align="left"><input type="text" name="buy_state" value="<?= $buy_state ?>" /></td>
		<td	align="right">Price Range:</td>
		<td align="left">
			<select name="buy_value">
				<option value="xx">Select...</option>		
				<?
					$end = 850;
					for($i = 100; $i < $end; $i += 25)
						echo "<option value='$i'>\$$i,000</option>\n";
				?>
			</select>
		</td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><td colspan="4" align="left">Selling Info:</td></tr>
	<tr>
		<td align="right">State:</td>
		<td align="left"><input type="text" name="sell_state" value="<?= $sell_state ?>" /></td>
		<td	align="right">Home Value:</td>
		<td align="left">
			<select name="sell_value">
				<option value="xx">Select...</option>			
				<?
					$end = 850;
					for($i = 100; $i < $end; $i += 25)
						echo "<option value='$i'>\$$i,000</option>\n";
				?>
			</select>
		</td>
	</tr>	
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td align="left">Comments:</td>
		<td align="left" colspan="3"><textarea name="comments" cols="40"><?= $comments ?></textarea></td>
	</tr>
	<tr><td colspan="4">&nbsp;</td></tr>
	<tr><td>&nbsp;</td><td colspan="3" align="left"><input type='submit' name="s" value="Find me an Agent" /></td></tr>
</table>
</form>
</body>
</html>
