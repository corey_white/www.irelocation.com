<?
	include "../../inc_mysql.php";
	
	function formatExtraFeatures($extra_features)
	{		
		$array['mfbathroom'] = "Main Floor Bathroom";
		$array['basement'] = "Basement";
		$array['fireplace'] = "Fireplace";
		$array['gashc'] = "Gas Heating/Cooling";
		$array['centralair'] = "Central Air";
		$array['mfbedroom'] = "Main Floor Bedroom";		
		$array['den'] = "Den/Office";
		$array['hwfloors'] = "Hardwood Floors";
		$array['spa'] = "Spa/Hot Tub";
		$array['diningroom'] = "Dining Room";
		$array['horseface'] = "Horse Facilities";
		$array['horses'] = "Horses Allowed";		
		$array['familyroom'] = "Family Room";
		$array['laundryroom'] = "Laundry Room";
		$array['disabled'] = "Disability Features";
		if (substr_count($extra_features,",") > 0)
		{
			$ef = split(",",$extra_features);
			$result = array();
			foreach($ef as $e) $result[] = $array[$e];
			return implode(", ",$result);
		}
		else
			return "None Specified.";
	}

	function displayInformation($info,$image)
	{
		extract($info);
		?>
<table width="600" cellpadding="0" cellspacing="0" border="0">
			<tr><td class="BodyHeader3" align="right">
				Property Details
			</td></tr>
		</table>
<table width="600">
	<Tr>
		<td class="BodySmall" width="400" id='primary_image'>
			<img src="http://irelocation.com/mcls/uploads/<?= $image['source'] ?>"><br/>
			<?= $image['caption'] ?>
		</td>
		<td valign="top">			
			<table width="200" border="0">
			<tr class="BodySmall">
				<td align="right">Property Title:</td>
				<td><?= $name ?></td>
				<td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right" valign="top">Description:</td>
				<td><?= $description ?></td>
				<td>&nbsp;</td>			
			</tr>		
			<tr class="BodySmall">
				<td align="right">Bedrooms:</td>	
				<td><?= $bedrooms ?></td>
				<td>&nbsp;</td>
			</tr>
			<tr class="BodySmall">
				<td align="right">Bathrooms:</td>	
				<td><?= $bathrooms ?></td>
				<td>&nbsp;</td>
			</tr>
			<tr class="BodySmall">
				<td align="right">Floors/Levels:</td>	
				<td><?= $floors ?></td>
				<td>&nbsp;</td>
			</tr>
			<tr class="BodySmall">
				<td align="right">Address:</td>
				<td><?= $address ?></td><td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right">City:</td>
				<td><?= $city ?></td><td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right">State:</td>
				<td><?= $state ?></td>
				<td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right">Zip Code:</td>
				<td><?= $zip ?></td>
				<td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right">Square Footage:</td>
				<td><?= $sqr_footage ?></td>
				<td>&nbsp;</td>
			</tr>		
			<tr class="BodySmall">
				<td align="right" valign="top">Dwelling type:</td>
				<td><?= $dwelling_type ?>
				</td><td>&nbsp;</td>
			</tr>			
			
			<tr class="BodySmall">
				<td align="right">Pool:</td>
				<td><? if ($pool == "Y") echo "Yes"; else echo "No"; ?></td>
				<td>&nbsp;</td>
			</tr>				
			<tr class="BodySmall">
				<td valign="top" align="right">Extra Features:</td>
				<td><?= wordwrap(formatExtraFeatures($extra_features),30,"<br/>") ?></td>
				<Td>&nbsp;</Td>
			</tr>
			<tr class="BodySmall">
				<td align="right">Lot Size:</td>
				<td><?= $lot_size ?></td>
				<td>&nbsp;</td>
			</tr>		
				<tr class="BodySmall">
				<td align="right">Garage Type:</td>
				<td><?= ucwords($garage) ?></td>
				<td>&nbsp;</td>
			</tr>		
			<? if ($mlsnumber > 0)	{ ?>
			<tr class="BodySmall">
				<td align="right">MLS Number:</td>
				<td><?= $mlsnumber ?></td><td>&nbsp;</td>
			</tr>		
			<? } ?>
			</table>
		</td>
	</Tr>
</table>
<Br/>
		<?
	}

	function displayImages($images,$listing_id)
	{
		?>
		<table width="600" cellpadding="0" cellspacing="0" border="0">
			<tr><td class="BodyHeader3">
				Listing Images:
			</td></tr>
		</table>
		<table width="600" cellpadding="0" cellspacing="0" border="0">
			<tr><td width="70">&nbsp;</td>
		<?
		$i = 1;
		foreach($images as $image)
		{
			?>		
			<td width="120">
				<a name='picture<?= $image['picture_id'] ?>' />
				<table width="140" cellpadding="0" cellspacing="0"><tr><td align="center">
				<Table>
					<tr>
						<td align="left">
							<!--- <a href='index.php?page=view&listing_id=<?= $listing_id ?>&image=<?= $image['index'] ?>' border='0'> --->
							<A href='#picture<?= $image['picture_id'] ?>' onClick="call_image(<?= $image['picture_id'] ?>);">
								<img src='http://irelocation.com/mcls/uploads/thumbs/<?= $image['source'] ?>' border="0"/>
							</a>
						</td>
						<td>&nbsp;</td>
					</tr>
				</Table>
				</td></tr></table>
			</td>
		<? 
			if ($i % 4 == 0 && $i != 0)
			{
				echo "</tr><tr><td width='70'>&nbsp;</td>";
			}	
			$i++;
		} 
		echo "</tr></table>";
	}
	
	function getPictures($listing_id)
	{
		$sql = "select picture_id, source,caption, sort_order-1 'index' from irelocation.mcpictures ".
				" where listing_id = '$listing_id' order by sort_order limit 5";
		$rs = new mysql_recordset($sql);
		$images = array();
		$count = 0;
		while($rs->fetch_array())
		{
			//$rs->myarray['index'] = $count++;
			$images[] = $rs->myarray;
		}
		return $images;		
	}

	function getInfo($listing_id)
	{
		$sql = "select * from irelocation.mclistings ".
				" where listing_id = '$listing_id' limit 1";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		return $rs->myarray;
	}
	//array_splice ( array &$input, int $offset [, int $length [, array $replacement]] )


	$image = 0;
	$images = getPictures($listing_id);
	//mail("david@irelocation.com","images",print_r($images,true));
	$primary = $images[$image];
	

	?>
	<script>
		function call_image(image_id)
		{
			call('functions.php','getImage','callback_image',image_id);
		}
		
		function callback_image(result)
		{
			var output = document.getElementById('primary_image');
			output.innerHTML = result;
		}
	</script>
	<?
	displayInformation(getInfo($listing_id),$primary);
	displayImages($images,$listing_id);
	?>


<body>	
	
</body>