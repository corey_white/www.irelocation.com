<?	
	session_start();
	$haveform = strlen(trim($_SESSION['haveform'])) > 0;
	$agent_id = $_SESSION['agent_id'];
	$is_agent = (strlen($agent_id)>0);
	//echo "<!--- ".(($haveform)?"true":"false")." --->";
	$page = $_REQUEST['page'];
	$listing_id = $_REQUEST['listing_id'];
	$include_page = "$page.php";

	if (!isset($_REQUEST['action']))
	{
		switch($page)
		{
			case 'add':
				$title = "Add Listings";				
				break;
			case 'quote':
				$title = "Find a Real Estate Agent";
			case 'addpictures':
				$title = "Add Pictures";
				break;
			case 'addpictureinfo':
				$title = "Add Picture Description(s)";
				break;
			case 'edit':
				$title = "Edit Listings";
				break;
			case 'delete':
				$title = "Delete a Listing";
				break;
			case 'view':
				if ($haveform || $is_agent)
				{
					//echo "<!--- have form. --->";
					if (strlen($listing_id) != 0)	
						$title = "View a Listing";														
					else
					{
						$title = "View Listings";
						$include_page = "viewall.php";
					}
				}
				else
				{
					//echo "<!--- don't have form. --->";
					$title = "Find a Real Estate Agent";
					$include_page = "quote.php";
				}				
				break;
			case 'viewall':			
			default:
				$title = "View Listings";
				$include_page = "viewall.php";
				break;
		}

		include "header.php";
		include $include_page;
	}
	else
	{
		//action pages... update stuff.. no visible output.
		$page = $_REQUEST['action'];
		switch($page)
		{
			case 'add':
				include "act_addlisting.php";
				break;
			case 'upload':
				include "act_uploadimages.php";
				break;
			case 'adddescriptions':
				include "act_adddescriptions.php";
				break;
			case 'submitquote':
				include "act_submitquote.php";
				break;
		}
	}
?>