<?php
	//echo "Under Contstruction...";
	//exit();

	include "../../inc_mysql.php";
	include "resize.php";
	define(ERROR_FILESIZE_TOO_BIG,2);
	define(MAX_WIDTH,500);
	define(MAX_HEIGHT,500);

	function fail($why,$listing_id)
	{
		header("Location: index.php?page=addpictures&listing_id=".$listing_id."&error=$why");
		exit();
	}

	function savePicture($listing_id,$source,$caption)
	{
		$sql = "select max(sort_order)+1 'next_order' from irelocation.mcpictures where listing_id = $listing_id";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
		{
			$next_sort_order = $rs->myarray['next_order'];						
		}
		if ($next_sort_order == 0) $next_sort_order = 1;

		$sql_insert = "insert into irelocation.mcpictures set listing_id = '$listing_id', source = '$source', ".
	  				  " sort_order = '$next_sort_order', caption = '$caption' ";
		$rs = new mysql_recordset($sql_insert);	
		return $rs->last_insert_id();
	}
	
	//echo "<pre>".print_r($_FILES,true)."</pre><br/>";
	$count = count($_FILES['userfile']['name']);
	//echo $count." possible files.<br/>";
	$upload_count = 0;
	
	$valid_files = array();
	$uploaddir = '../uploads/';
	for ($i = 0; $i < $count; $i++)
	{
		$type = $_FILES['userfile']['type'][$i];		
		$filename = trim(basename($_FILES['userfile']['name'][$i]));
		$listing_id = $_POST['listing_id'];		
	
		$array = split(".",$filename);
		$ext = strtolower(array_pop($array));
		if ($ext == "jpg" || $ext == "jpeg")
		{
			$_SESSION['valid_files'] = $valid_files;
			fail(extension,$listing_id);
		}		
		if (strlen($filename) != 0)
		{
			$upload_count++;
			//echo "Temp File: ".$_FILES['userfile']['tmp_name']."<br/>";
			list($width, $height, $type, $attr) = getimagesize($_FILES['userfile']['tmp_name'][$i]);
			if ($width <= MAX_WIDTH && $height <= MAX_HEIGHT)
			{	
				$uploadfile = $uploaddir . $filename;
				//echo '<pre>';
				if (move_uploaded_file($_FILES['userfile']['tmp_name'][$i], $uploadfile)) 
				{		   					
					resizejpeg($filename,$listing_id);
					$valid_files[] = $filename;
				} 
				else 
				{					
					if($_FILES['error'] == ERROR_FILESIZE_TOO_BIG)//too big
					{
						$_SESSION['valid_files'] = $valid_files;
						fail(filesize,$listing_id);
					}
				}
			}
			else
			{
				$_SESSION['valid_files'] = $valid_files;
				fail(dimensions,$listing_id);
			}
		}		
	}
	$_SESSION['valid_files'] = $valid_files;
	if ($upload_count > 0)
	{
		header("Location: index.php?page=addpictureinfo&listing_id=".$listing_id);
	}
	else
		header("Location: index.php?page=addpictures&listing_id=".$listing_id);
?> 