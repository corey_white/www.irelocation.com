<? session_start(); ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Upload a new Listing</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<form name="addlisting" action="addlisting.php" method="post">
	<?	if ($agent_id == 0)	$agent_id = rand(1000,3000); ?>
	<input type="hidden" name="agent_id" value="<?= $agent_id ?>" />
	<table width="450">
		<? if (strlen($error_message) > 0) { ?>
		<tr>
			<td align="right">Property Title:</td>
			<td><input type='text' name="name" value="<?= $name ?>" /></td>
			<td>&nbsp;</td>
		</tr>		
		<? } ?>
		<tr>
			<td align="right">Property Title:</td>
			<td><input type='text' name="name" value="<?= $name ?>" /></td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">Description:</td>
			<td><textarea name="description" rows="5" cols="30"><?= $description ?></textarea></td>
			<td>&nbsp;</td>			
		</tr>		
		<tr>
			<td align="right">Bedrooms:</td>	
			<td>
				<select name="bedrooms">
				<? for($i = 1; $i < 7; $i++) { ?>
					<option value="<?= $i ?>" <? if ($bedrooms == $i) echo " selected "; ?>><?= $i ?></option>
				<? } ?>				
				</select>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td align="right">Bathrooms:</td>	
			<td>
				<select name="bathrooms">
				<? for($i = 1; $i < 5; $i++) { ?>
					<option value="<?= $i ?>" <? if ($bathrooms == $i) echo " selected "; ?>><?= $i ?></option>
				<? } ?>				
				</select>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td align="right">Floors/Levels:</td>	
			<td>
				<select name="floors">
					<option value="one" >1</option>
					<option value="two+" <? if ($floors == "two+") echo " selected "; ?>>2+</option>
				</select>
			</td><td>&nbsp;</td>
		</tr>
		<tr>
			<td align="right">Address:</td>
			<td><input type='text' name="address" value="<?= $address ?>" /></td><td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">City:</td>
			<td><input type='text' name="city" value="<?= $city ?>" /></td><td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">State:</td>
			<td>
				<select name="state" size="1" >
					<option value="XX" <? if ($state == "XX") echo " selected "; ?> >Select One</option>
					<option value="AL" <? if ($state == "AL") echo " selected "; ?> >Alabama</option>
					<option value="AK" <? if ($state == "AK") echo " selected "; ?> >Alaska</option>
					<option value="AZ" <? if ($state == "AZ") echo " selected "; ?> >Arizona</option>
					<option value="AR" <? if ($state == "AR") echo " selected "; ?> >Arkansas</option>
					<option value="CA" <? if ($state == "CA") echo " selected "; ?> >California</option>
					<option value="CO" <? if ($state == "CO") echo " selected "; ?> >Colorado</option>
					<option value="CT" <? if ($state == "CT") echo " selected "; ?> >Connecticut</option>
					<option value="DE" <? if ($state == "DE") echo " selected "; ?> >Delaware</option>
					<option value="DC" <? if ($state == "DC") echo " selected "; ?> >District Of Columbia</option>
					<option value="FL" <? if ($state == "FL") echo " selected "; ?> >Florida</option>
					<option value="GA" <? if ($state == "GA") echo " selected "; ?> >Georgia</option>
					<option value="HI" <? if ($state == "HI") echo " selected "; ?> >Hawaii</option>
					<option value="ID" <? if ($state == "ID") echo " selected "; ?> >Idaho</option>
					<option value="IL" <? if ($state == "IL") echo " selected "; ?> >Illinois</option>
					<option value="IN" <? if ($state == "IN") echo " selected "; ?> >Indiana</option>
					<option value="IA" <? if ($state == "IA") echo " selected "; ?> >Iowa</option>
					<option value="KS" <? if ($state == "KS") echo " selected "; ?> >Kansas</option>
					<option value="KY" <? if ($state == "KY") echo " selected "; ?> >Kentucky</option>
					<option value="LA" <? if ($state == "LA") echo " selected "; ?> >Louisiana</option>
					<option value="ME" <? if ($state == "ME") echo " selected "; ?> >Maine</option>
					<option value="MD" <? if ($state == "MD") echo " selected "; ?> >Maryland</option>
					<option value="MA" <? if ($state == "MA") echo " selected "; ?> >Massachusetts</option>
					<option value="MI" <? if ($state == "MI") echo " selected "; ?> >Michigan</option>
					<option value="MN" <? if ($state == "MN") echo " selected "; ?> >Minnesota</option>
					<option value="MS" <? if ($state == "MS") echo " selected "; ?> >Mississippi</option>
					<option value="MO" <? if ($state == "MO") echo " selected "; ?> >Missouri</option>
					<option value="MT" <? if ($state == "MT") echo " selected "; ?> >Montana</option>
					<option value="NE" <? if ($state == "NE") echo " selected "; ?> >Nebraska</option>
					<option value="NV" <? if ($state == "NV") echo " selected "; ?> >Nevada</option>
					<option value="NH" <? if ($state == "NH") echo " selected "; ?> >New Hampshire</option>
					<option value="NJ" <? if ($state == "NJ") echo " selected "; ?> >New Jersey</option>
					<option value="NM" <? if ($state == "NM") echo " selected "; ?> >New Mexico</option>
					<option value="NY" <? if ($state == "NY") echo " selected "; ?> >New York</option>
					<option value="NC" <? if ($state == "NC") echo " selected "; ?> >North Carolina</option>
					<option value="ND" <? if ($state == "ND") echo " selected "; ?> >North Dakota</option>
					<option value="OH" <? if ($state == "OH") echo " selected "; ?> >Ohio</option>
					<option value="OK" <? if ($state == "OK") echo " selected "; ?> >Oklahoma</option>
					<option value="OR" <? if ($state == "OR") echo " selected "; ?> >Oregon</option>
					<option value="PA" <? if ($state == "PA") echo " selected "; ?> >Pennsylvania</option>
					<option value="RI" <? if ($state == "RI") echo " selected "; ?> >Rhode Island</option>
					<option value="SC" <? if ($state == "SC") echo " selected "; ?> >South Carolina</option>
					<option value="SD" <? if ($state == "SD") echo " selected "; ?> >South Dakota</option>
					<option value="TN" <? if ($state == "TN") echo " selected "; ?> >Tennessee</option>
					<option value="TX" <? if ($state == "TX") echo " selected "; ?> >Texas</option>
					<option value="UT" <? if ($state == "UT") echo " selected "; ?> >Utah</option>
					<option value="VT" <? if ($state == "VT") echo " selected "; ?> >Vermont</option>
					<option value="VA" <? if ($state == "VA") echo " selected "; ?> >Virginia</option>
					<option value="WA" <? if ($state == "WA") echo " selected "; ?> >Washington</option>
					<option value="WV" <? if ($state == "WV") echo " selected "; ?> >West Virginia</option>
					<option value="WI" <? if ($state == "WI") echo " selected "; ?> >Wisconsin</option>
					<option value="WY" <? if ($state == "WY") echo " selected "; ?> >Wyoming</option>
				  </select>
			</td><td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">Zip Code:</td>
			<td><input type='text' name="zip" value="<?= $zip ?>" maxlength="9" size="11" /></td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">Square Footage:</td>
			<td>
				<select name="sqr_footage" >
					<option value="1000">Up to 1,000 sq ft</option>
					<option value="1500" <? if ($sqr_footage == 1500) echo " selected "; ?>>1,001 - 1,500 sq ft</option>
					<option value="2000" <? if ($sqr_footage == 2000) echo " selected "; ?>>1,501 - 2,000 sq ft</option>
					<option value="3000" <? if ($sqr_footage == 3000) echo " selected "; ?>>2,001 - 3,000 sq ft</option>
					<option value="3500" <? if ($sqr_footage == 3500) echo " selected "; ?>>More than 3,000 sq ft</option>
				</select>
			</td><td>&nbsp;</td>
		</tr>		
		<tr>
			<td align="right">Dwelling type:</td>
			<td>
				<select name="dwelling_type" >
					<option value="Vacant Land" <? if ($dwelling_type == "Vacant Land") echo " selected "; ?> >Vacant Land</option>				
					<option value="Condo/Town Home" <? if ($dwelling_type == "Condo/Town Home") echo " selected "; ?> >Condo/Town Home</option>
					<optgroup label="Single Family Attached">
						<option value="Duplex" <? if ($dwelling_type == "Duplex") echo " selected "; ?> >Duplex</option>
						<option value="Triplex" <? if ($dwelling_type == "Triplex") echo " selected "; ?> >Triplex</option>
						<option value="Fourplex" <? if ($dwelling_type == "Fourplex") echo " selected "; ?> >Fourplex</option>
						<option value="Other Plex" <? if ($dwelling_type == "Other Plex") echo " selected "; ?> >Other</option>
					</optgroup>
					<option value="Single Family Detached" <? if ($dwelling_type == "Single Family Detached") echo " selected "; ?> >Single Family Detached</option>				
				</select>
			</td><td>&nbsp;</td>
		</tr>			
		<tr>
			<td align="right">Pool:</td>
			<td>
				<input type="radio" value="N" name="pool"  <? if ($pool == "N") echo " selected "; ?>/> No 
				<input type="radio" value="Y" name="pool"  <? if ($pool == "Y" || $pool == "") echo " selected "; ?>/> Yes 
			</td><td>&nbsp;</td>
		</tr>				
		<tr>
			<td align="right">Lot Size:</td>
			<td>
				<select name="lot_size">
					<option value="7500" <? if ($lot_size == "7500") echo " selected "; ?> >up to 7,500 sq ft</option>
					<option value="10000" <? if ($lot_size == "10000") echo " selected "; ?> >7,501 - 10,000 sq ft</option>
					<option value="12500" <? if ($lot_size == "12500") echo " selected "; ?> >10,001 -  12,500 sq ft</option>
					<option value="15000" <? if ($lot_size == "15000") echo " selected "; ?> >12,501 -  15,000 sq ft (1/3 Acre)</option>
					<option value="30000" <? if ($lot_size == "30000") echo " selected "; ?> >15,001 -  30,000 sq ft (1/2 Acre)</option>
					<option value="43560" <? if ($lot_size == "43560") echo " selected "; ?> >30,001 sq ft - 1 Acre</option>
					<option value="100000" <? if ($lot_size == "100000") echo " selected "; ?> >1 - 2.5 Acre</option>
					<option value="100000" <? if ($lot_size == "100000") echo " selected "; ?> >2.5 Acre or more</option>
				</select>
			</td><td>&nbsp;</td>
		</tr>		
			<tr>
			<td align="right">Garage Type:</td>
			<td>
				<select name="garage">					
					<option value="xx" <? if ($garage == "xx") echo " selected "; ?> >Select...</option>
					<option value="none" <? if ($garage == "none") echo " selected "; ?>>None</option>
					<optgroup label="Garage">
						<option value="one car garage" <? if ($garage == "one car garage") echo " selected "; ?> >One Car</option>
						<option value="two car" <? if ($garage == "two car") echo " selected "; ?> >Two Car</option>
						<option value="three car garage" <? if ($garage == "three car garage") echo " selected "; ?> >Three Car</option>
					</optgroup>
					<optgroup label="Car Port">
						<option value="one car carport" <? if ($garage == "one car carport") echo " selected "; ?> >One Car</option>
						<option value="two car carport" <? if ($garage == "two car carport") echo " selected "; ?> >Two Car</option>
						<option value="three car carport" <? if ($garage == "three car carport") echo " selected "; ?> >Three Car</option>
					</optgroup>
				</select>
			</td><td>&nbsp;</td>
		</tr>		
			
		<tr>
			<td align="right">MLS Number:</td>
			<td><input type='text' name="mlsnumber" value="<?= $mlsnumber ?>" /></td><td>&nbsp;</td>
		</tr>		
		<tr>
			<td>&nbsp;</td>
			<td colspan="2" align="left"><input type="submit" value="Add Listing"></td>
		</tr>
	</table>
</form>
</body>
</html>
