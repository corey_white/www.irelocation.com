<?
	$keyword = strtolower($_REQUEST['key']);	
	if (strlen($_REQUEST['key']) > 0)
		define(KEYWORD,strtolower($_REQUEST['key']));
	else
		define(KEYWORD,"moving companies");
	
	$source = $_REQUEST['source'];
	
	function getHome()
	{
		$words = array("household","home","house","business","residence","apartment","condo");
		foreach($words as $w)
			if (substr_count(KEYWORD,$w) > 0)
				return $w;			
		
		return $word[rand(0,count($words)-1)];
	}
	
	function getCompany($plural=true)
	{
		$words = array("moving company","moving van line","hauling company",
					   "professional mover","household mover","moving service",
					   "mover","moving specialists");
		$noun = $words[rand(0,count($words)-1)];
		foreach($words as $w)
		{
			if (substr_count(KEYWORD,$w) > 0)
			{
				$noun = $w;
				break;
			}
		}
		if ($plural)
			$noun = (substr($noun,-1,1) == "y")?(substr($noun,0,-1)."ies"):$noun."s";
				
		return $noun;
	}
	
	function getProfessional()
	{
		$words = array("top","professional","national",
						"leading","local","quality","cheep");
		
		foreach($words as $w)
			if (substr_count(KEYWORD,$w) > 0)
				return $w;			
		
		return $words[rand(0,count($words)-1)];
	}
	
	function getQuote($plural=true)
	{
		$words = array("estimate","quote");
		$noun = $words[rand(0,1)];
		foreach($words as $w)
		{
			if (substr_count(KEYWORD,$w) > 0)
				$noun = $w;	break;			
		}
		if ($plural) $noun .= "s";
				
		return $noun;
	}

?>


<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title><?= ucwords(KEYWORD);?></title>
<meta name="keywords" content="<?= KEYWORD ?>"/>
<meta name="description" content=" The best Site on the Web for <?= ucwords(getCompany()) ?> Information & Resources "/>
</head>
<body>

<div align="left">
<table width="562" border="0" align="left" bgcolor="#DFE0FF">
      <tr>
        <td width="499">
			<font face='verdana' color="navy" size="2">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= ucwords(KEYWORD) ?> and <?= getCompany() ?> can be very useful when you are looking  to move your <?= getHome() ?>. <?= ucwords(getCompany()) ?> and <?= ucwords(KEYWORD) ?> have state of the art carriers capable of relocating your <?= getHome() ?>, safely and quickly. </p>
</font>
			<p>There  are a lot of questions that come with the decision to move. Some are  basic, and some are questions that most people don't know to ask.  Including questions from: "Who is my information being given to?", and  "What questions should I ask before signing a contract?". Our <?= ucwords(getCompany(false)) ?>  guide is designed to provide you with that information, to ensure that  you are well informed before making any commitments.</p>
			<p> Topmoving.com is committed to working with industry <?= getProfessional() ?>s only  and if you are in need for a <?= getCompany(false) ?> and required unmatched  service, simply submit a request for quote, and 4 of the top <?= getCompany() ?> in the country will supply fast, fair and competitive quotes  to help you make an educated decision about what is best for you, your  family, and your belongings. </p>
			<p>TopMoving.com has                 been providing the best way to find <?= getProfessional() ?> <?= getCompany(true) ?>                 since 2002. Our network of <?= ucwords(getCompany()) ?> is fully insured to protect your                 investment and give you peace of mind. </p>
			<p>Moving a household                 is an expert job and can be a simple and secure process in the right                 hands. Your first task should be to find a suitably <?= getProfessional() ?> <?= getCompany(false) ?>                 that can manage your move for you. As with all moving specialists                 you should be looking for one that has done it before. Depending                 on your individual needs you might be looking for <?= getCompany() ?> with                 experience of moving pianos, large TVs, antique furniture, or even                 moving internationally. Some items such as pianos may need special                 lifting equipment to put them on to their moving trailer so you                 really need to deal with a <?= getCompany(false) ?> that can offer this to minimize                 the chances of something going wrong. </p>
			<p>Moving within your                 home country is generally simple enough but an international move                 may require special skills. You'll need to find someone that can                 help you with the documentation, fees, restrictions and regulations                 of your chosen destination country as well as get your belongings                 there in one piece.</p>
			<p>TopMoving.com can                 take care of any and all of your moving needs; we can help ease                 the stress of moving your belongings to your new <?= getHome() ?>. Whatever                 your moving needs are, TopMoving.com is there with one goal...</p>		</td>
	</tr>
</table>
</div>
