<link href="../irelo.css" rel="stylesheet" type="text/css" />
<table width="786" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="603" align="center" class="footer">
	  <form id="form1" name="form1" method="post" action="">
      
	    <table width="551" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><p align="center" class="formtopic"><strong>iRelocation           will help you find your dream home!</strong></p>
                <span class="intro">We           have over 20,000+ hand-selected Realtors from the nation's leading <br />
                  Real Estate Companies ready to help you find your new home. </span>
                <p align="left" class="style3"> <span class="intro">A           consultant will contact you first to discuss your needs, and then put           you in contact with a 
                  Realtor that best fits you! Most of our realtors have exclusive listings           available near you.</span></p>
              <p align="left" class="style3">&nbsp;</p></td>
          </tr>
        </table>
	    <table width="550" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td colspan="4" class="formtopic">Contact Information </td>
        </tr>
      <tr>
        <td width="92" class="formtitle">First Name </td>
        <td width="193" class="formtitle"><input type="text" maxlength="25" size="25" name="first_name" value="" /></td>
        <td width="86" class="formtitle">Last Name </td>
        <td width="155" class="formtitle"><input type="text" maxlength="25" size="25"  name="last_name" value="" /></td>
      </tr>
      <tr>
        <td class="formtitle">Email</td>
        <td class="formtitle"><input type="text" maxlength="125" size="32" name="email" value="" /></td>
        <td class="formtitle">Phone</td>
        <td class="formtitle"><input type="text" maxlength="20" size="18" name="phone" value="" /></td>
      </tr>
    </table>
	  <br />
	  <table width="550" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td colspan="4" class="formtopic">Seller Information </td>
        </tr>
        <tr>
          <td width="92" class="formtitle">Street Address </td>
          <td width="194" class="formtitle"><input type="text" maxlength="10" size="5" name="street_number" value="" />
            <input type="text" maxlength="50" size="20" name="street_name" value="" /></td>
          <td width="85" class="formtitle">City</td>
          <td width="155" class="formtitle"><input type="text" maxlength="50" size="17" name="city" value="" /></td>
        </tr>
        <tr>
          <td class="formtitle">State</td>
          <td class="formtitle"><select name="state">
            <option value="__none__">Please select</option>
            <option value="AL" >Alabama</option>
            <option value="AK" >Alaska</option>
            <option value="AZ" >Arizona</option>
            <option value="AR" >Arkansas</option>
            <option value="CA" >California</option>
            <option value="CO" >Colorado</option>
            <option value="CT" >Connecticut</option>
            <option value="DE" >Delaware</option>
            <option value="DC" >District of Columbia</option>
            <option value="FL" >Florida</option>
            <option value="GA" >Georgia</option>
            <option value="HI" >Hawaii</option>
            <option value="ID" >Idaho</option>
            <option value="IL" >Illinois</option>
            <option value="IN" >Indiana</option>
            <option value="IA" >Iowa</option>
            <option value="KS" >Kansas</option>
            <option value="KY" >Kentucky</option>
            <option value="LA" >Louisiana</option>
            <option value="ME" >Maine</option>
            <option value="MD" >Maryland</option>
            <option value="MA" >Massachusetts</option>
            <option value="MI" >Michigan</option>
            <option value="MN" >Minnesota</option>
            <option value="MS" >Mississippi</option>
            <option value="MO" >Missouri</option>
            <option value="MT" >Montana</option>
            <option value="NE" >Nebraska</option>
            <option value="NV" >Nevada</option>
            <option value="NH" >New Hampshire</option>
            <option value="NJ" >New Jersey</option>
            <option value="NM" >New Mexico</option>
            <option value="NY" >New York</option>
            <option value="NC" >North Carolina</option>
            <option value="ND" >North Dakota</option>
            <option value="OH" >Ohio</option>
            <option value="OK" >Oklahoma</option>
            <option value="OR" >Oregon</option>
            <option value="PA" >Pennsylvania</option>
            <option value="RI" >Rhode Island</option>
            <option value="SC" >South Carolina</option>
            <option value="SD" >South Dakota</option>
            <option value="TN" >Tennessee</option>
            <option value="TX" >Texas</option>
            <option value="UT" >Utah</option>
            <option value="VT" >Vermont</option>
            <option value="VI" >Virgin Islands</option>
            <option value="VA" >Virginia</option>
            <option value="WA" >Washington</option>
            <option value="WV" >West Virginia</option>
            <option value="WI" >Wisconsin</option>
            <option value="WY" >Wyoming</option>
            <option value="OT" >Other</option>
          </select></td>
          <td class="formtitle">Zip</td>
          <td class="formtitle"><input type="text" maxlength="5" size="5" name="zip_code" value="" /></td>
        </tr>
        <tr>
          <td class="formtitle">Bedrooms</td>
          <td class="formtitle"><select name="bed_rooms">
            <option value="__none__">Please select</option>
            <option value="0" >0</option>
            <option value="100" >1</option>
            <option value="130" >Studio</option>
            <option value="200" >2</option>
            <option value="300" >3</option>
            <option value="400" >4</option>
            <option value="500" >5</option>
          </select></td>
          <td class="formtitle">Baths</td>
          <td class="formtitle"><select name="bath_rooms">
            <option value="__none__">Please select</option>
            <option value="0.0" >0</option>
            <option value="1.0" >1</option>
            <option value="1.5" >1.5</option>
            <option value="2.0" >2</option>
            <option value="2.5" >2.5</option>
            <option value="3.0" >3</option>
            <option value="3.5" >3.5</option>
            <option value="4.0" >4</option>
            <option value="4.5" >4.5</option>
          </select></td>
        </tr>
        <tr>
          <td class="formtitle">Home Type </td>
          <td class="formtitle"><select onchange="Unitcheck();" name="property_type">
            <option value="__none__">Please select</option>
            <option value="1" >Single Family Detached</option>
            <option value="4" >Condominium</option>
            <option value="2" >Townhouse</option>
            <option value="3" >Rental</option>
            <option value="9" >Multi-Family</option>
            <option value="8" >Vacation Home</option>
            <option value="100" >Other</option>
          </select></td>
          <td class="formtitle">Selling Price </td>
          <td class="formtitle"><select name="min_price_range">
            <option value="__none__">Please select</option>
            <option value="50000" >$50,000</option>
            <option value="75000" >$75,000</option>
            <option value="100000" >$100,000</option>
            <option value="125000" >$125,000</option>
            <option value="150000" >$150,000</option>
            <option value="175000" >$175,000</option>
            <option value="200000" >$200,000</option>
            <option value="225000" >$225,000</option>
            <option value="250000" >$250,000</option>
            <option value="275000" >$275,000</option>
            <option value="300000" >$300,000</option>
            <option value="350000" >$350,000</option>
            <option value="400000" >$400,000</option>
            <option value="450000" >$450,000</option>
            <option value="500000" >$500,000</option>
            <option value="550000" >$550,000</option>
            <option value="600000" >$600,000</option>
            <option value="650000" >$650,000</option>
            <option value="700000" >$700,000</option>
            <option value="800000" >$800,000</option>
            <option value="900000" >$900,000</option>
            <option value="1000000" >$1,000,000</option>
            <option value="1250000" >$1,250,000</option>
            <option value="1500000" >$1,500,000</option>
            <option value="10000000" >$2,000,000 or more</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2" class="formtitle"><span class="label_normal">When do you expect to sell?</span></td>
          <td colspan="2" class="formtitle"><select name="expect_to_sell">
              <option value="__none__">Please select</option>
              <option value="1" >as soon as possible</option>
              <option value="2" >within next 2 weeks</option>
              <option value="3" >within 3-4 weeks</option>
              <option value="4" >within 5-7 weeks</option>
              <option value="5" >within 2-4 months</option>
              <option value="6" >within 5-6 months</option>
              <option value="7" >more than 6 months</option>
                    </select></td>
          </tr>
        <tr>
          <td colspan="2" class="formtitle"><span class="label_normal">What is your reason for the sale?</span></td>
          <td colspan="2" class="formtitle"><select name="reason_for_sale">
              <option value="__none__">Please select</option>
              <option value="1" >Need a Bigger Home</option>
              <option value="3" >Corporate Relocation-New Job</option>
              <option value="2" >Need a Smaller Home</option>
              <option value="8" >Attracted to New Community</option>
              <option value="6" >Retirement</option>
              <option value="12" >InvestmentProperty</option>
              <option value="5" >Divorce/Separation</option>
              <option value="13" >Financial Reasons</option>
              <option value="10" >Selling Vacation / Other Home</option>
              <option value="100" >Other</option>
                    </select></td>
          </tr>
      </table>
	  <br />
	  </form></td>
    <td width="183" align="center" valign="top" class="footer">&nbsp;</td>
  </tr>
</table>
