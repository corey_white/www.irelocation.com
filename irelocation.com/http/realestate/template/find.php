<link href="../irelo.css" rel="stylesheet" type="text/css" />

<table width="786" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="603" align="center" class="footer">
	  <form id="form1" name="form1" method="post" action="">
        <table width="551" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><p align="center" class="formtopic"><strong>iRelocation           will help you find your dream home!</strong></p>               
              <span class="intro">We           have over 20,000+ hand-selected Realtors from the nation's leading <br />
              Real Estate Companies ready to help you find your new home.              </span>
              <p align="left" class="style3"> <span class="intro">A           consultant will contact you first to discuss your needs, and then put           you in contact with a 
            Realtor that best fits you! Most of our realtors have exclusive listings           available near you.</span></p>
            <p align="left" class="style3">&nbsp;</p></td>
          </tr>
        </table>
        <table width="550" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td colspan="4" class="formtopic">Contact Information </td>
  </tr>
  <tr>
    <td width="92" class="formtitle">First Name </td>
    <td width="193" class="formtitle"><input type="text" maxlength="25" size="25" name="first_name" value="" /></td>
    <td width="86" class="formtitle">Last Name </td>
    <td width="155" class="formtitle"><input type="text" maxlength="25" size="25"  name="last_name" value="" /></td>
  </tr>
  <tr>
    <td class="formtitle">Email</td>
    <td class="formtitle"><input type="text" maxlength="125" size="32" name="email" value="" /></td>
    <td class="formtitle">Phone</td>
    <td class="formtitle"><input type="text" maxlength="20" size="18" name="phone" value="" /></td>
  </tr>
</table>
<br /><br />
	  <table width="550" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td colspan="4" class="formtopic">Buyer Information </td>
        </tr>
        <tr>
          <td width="85" class="formtitle">Desired Type </td>
          <td width="192" class="formtitle"><select name="type_home_to_buy_list">
            <option value="__none__">Please select</option>
            <option value="1" >Single Family Detached</option>
            <option value="4" >Condominium</option>
            <option value="2" >Townhouse</option>
            <option value="3" >Rental</option>
            <option value="9" >Multi-Family</option>
            <option value="8" >Vacation Home</option>
            <option value="100" >Other</option>
          </select></td>
          <td width="75" class="formtitle">Target Price </td>
          <td width="174" class="formtitle"><select style="width:162px;" name="max_price_range">
            <option value="__none__">Please select</option>
            <option value="6" >$50,000</option>
            <option value="9" >$75,000</option>
            <option value="11" >$100,000</option>
            <option value="12" >$125,000</option>
            <option value="13" >$150,000</option>
            <option value="14" >$175,000</option>
            <option value="15" >$200,000</option>
            <option value="16" >$225,000</option>
            <option value="17" >$250,000</option>
            <option value="18" >$275,000</option>
            <option value="19" >$300,000</option>
            <option value="21" >$350,000</option>
            <option value="23" >$400,000</option>
            <option value="24" >$450,000</option>
            <option value="25" >$500,000</option>
            <option value="26" >$550,000</option>
            <option value="27" >$600,000</option>
            <option value="28" >$650,000</option>
            <option value="29" >$700,000</option>
            <option value="31" >$800,000</option>
            <option value="33" >$900,000</option>
            <option value="35" >$1,000,000</option>
            <option value="36" >$1,250,000</option>
            <option value="37" >$1,500,000</option>
            <option value="38" >$2,000,000 and more</option>
          </select></td>
        </tr>
        <tr>
          <td class="formtitle">Bedrooms</td>
          <td class="formtitle"><select style="width:162px;" name="select">
            <option value="__none__">Please select</option>
            <option value="0" >0</option>
            <option value="100" >1</option>
            <option value="130" >Studio</option>
            <option value="200" >2</option>
            <option value="300" >3</option>
            <option value="400" >4</option>
            <option value="500" >5</option>
          </select></td>
          <td class="formtitle">Bath</td>
          <td class="formtitle"><select style="width:162px;" name="select2">
            <option value="__none__">Please select</option>
            <option value="0.0" >0</option>
            <option value="1.0" >1</option>
            <option value="1.5" >1.5</option>
            <option value="2.0" >2</option>
            <option value="2.5" >2.5</option>
            <option value="3.0" >3</option>
            <option value="3.5" >3.5</option>
            <option value="4.0" >4</option>
            <option value="4.5" >4.5</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2" class="formtitle">Enter your desired Zip codes </td>
          <td colspan="2" class="formtitle"><input type="text" id="zips0" name="target_zipcodes_1" size="5" maxlength="5" value="" />
            <input type="text" id="zips1" name="target_zipcodes_2" size="5" maxlength="5" value="" />
            <input type="text" id="zips2" name="target_zipcodes_3" size="5" maxlength="5" value="" />
            <input type="text" id="zips3" name="target_zipcodes_4" size="5" maxlength="5" value="" />
            <input type="text" id="zips4" name="target_zipcodes_5" size="5" maxlength="5" value="" />
            <input type="text" id="zips5" name="target_zipcodes_6" size="5" maxlength="5" value="" /></td>
          </tr>
        <tr>
          <td colspan="4" class="formtitle"><span style="font-size:10px;"><a href="JavaScript:grab_mapmyZIPCode()">Find ZIP Codes</a>. (Requires Flash Player 7 or higher.) Don't have Flash? <a href="javascript:newwin('http://www.macromedia.com/shockwave/download/', 800, 800)">Download here</a> (it's free) or <a href="JavaScript:grab_upsmyZIPCode()">use the USPS Lookup</a> instead. </span></td>
          </tr>
        <tr>
          <td class="formtitle"><span class="label_normal">Down payment</span></td>
          <td class="formtitle"><select name="cash_available">
            <option value="__none__">Please select</option>
            <option value="0" >$0</option>
            <option value="1" >Undecided</option>
            <option value="10000" >$10,000</option>
            <option value="20000" >$20,000</option>
            <option value="30000" >$30,000</option>
            <option value="40000" >$40,000</option>
            <option value="50000" >$50,000</option>
            <option value="60000" >$60,000</option>
            <option value="70000" >$70,000</option>
            <option value="80000" >$80,000</option>
            <option value="90000" >$90,000</option>
            <option value="100000" >$100,000</option>
            <option value="125000" >$125,000</option>
            <option value="150000" >$150,000</option>
            <option value="175000" >$175,000</option>
            <option value="200000" >$200,000</option>
            <option value="225000" >$225,000</option>
            <option value="250000" >$250,000</option>
            <option value="275000" >$275,000</option>
            <option value="300000" >$300,000</option>
            <option value="325000" >$325,000</option>
            <option value="350000" >$350,000</option>
            <option value="375000" >$375,000</option>
            <option value="400000" >$400,000</option>
            <option value="450000" >$450,000</option>
            <option value="500000" >$500,000</option>
            <option value="550000" >More than $500,000</option>
          </select></td>
          <td colspan="2" class="formtitle">Currently Own 
            <input type="radio" name="own_home" value="0">
            No&nbsp;&nbsp;&nbsp;
            <input type="radio" name="own_home" value="1">
            Yes&nbsp;&nbsp;</td>
          </tr>
        <tr>
          <td colspan="2" class="formtitle"><span class="label_normal">What is your reason for purchase?</span></td>
          <td colspan="2" class="formtitle"><select name="reason_to_buy_list">
            <option value="__none__">Please select</option>
            <option value="1" >Need a Bigger Home</option>
            <option value="8" >Attracted to New Community</option>
            <option value="3" >Need a Smaller Home</option>
            <option value="6" >Dissatisfied with Current Community</option>
            <option value="9" >New Family Members</option>
            <option value="10" >Vacation Home</option>
            <option value="4" >Corporate Relocation-New Job</option>
            <option value="7" >Build Home Equity</option>
            <option value="13" >Retirement</option>
            <option value="5" >Tax Write -Off</option>
            <option value="11" >Divorce/Separation</option>
            <option value="12" >InvestmentProperty</option>
            <option value="2" >No Longer Wish to Rent</option>
          </select></td>
          </tr>
        <tr>
          <td colspan="2" class="formtitle"><span class="label_normal">What is your mortgage status?</span></td>
          <td colspan="2" class="formtitle"><select name="mortgage_status">
            <option value="__none__">Please select</option>
            <option value="1" >I don't know.</option>
            <option value="2" >I have not applied.</option>
            <option value="3" >I am pre-approved.</option>
            <option value="4" >I am pre-qualified.</option>
            <option value="5" >I am pre-approved and pre-qualified.</option>
          </select></td>
          </tr>
        <tr>
          <td colspan="2" class="formtitle"><span class="label_normal">When do you expect to choose an agent?</span></td>
          <td colspan="2" class="formtitle"><select name="expect_to_choose_agent">
            <option value="__none__">Please select</option>
            <option value="1" >as soon as possible</option>
            <option value="2" >within next 2 weeks</option>
            <option value="3" >within 3-4 weeks</option>
            <option value="4" >within 5-7 weeks</option>
            <option value="5" >within 2-4 months</option>
            <option value="6" >in 5 or more months</option>
          </select></td>
          </tr>
      </table>
	  </form></td>
    <td width="183" align="center" valign="top" class="footer">&nbsp;</td>
  </tr>
</table>
