<link href="../irelo.css" rel="stylesheet" type="text/css" />
<table width="786" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td width="603" align="center" class="footer">
	  <form id="form1" name="form1" method="post" action="">
      
	    <table width="551" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><p align="center" class="formtopic"><strong>iRelocation           will help you find your dream home!</strong></p>
                <span class="intro">We           have over 20,000+ hand-selected Realtors from the nation's leading <br />
                  Real Estate Companies ready to help you find your new home. </span>
                <p align="left" class="style3"> <span class="intro">A           consultant will contact you first to discuss your needs, and then put           you in contact with a 
                  Realtor that best fits you! Most of our realtors have exclusive listings           available near you.</span></p>
              <p align="left" class="style3">&nbsp;</p></td>
          </tr>
        </table>
	    <table width="550" border="0" cellspacing="0" cellpadding="3">
      <tr>
        <td colspan="4" class="formtopic">Contact Information </td>
        </tr>
      <tr>
        <td width="92" class="formtitle">First Name </td>
        <td width="193" class="formtitle"><input type="text" maxlength="25" size="25" name="first_name" value="" /></td>
        <td width="86" class="formtitle">Last Name </td>
        <td width="155" class="formtitle"><input type="text" maxlength="25" size="25"  name="last_name" value="" /></td>
      </tr>
      <tr>
        <td class="formtitle">Email</td>
        <td class="formtitle"><input type="text" maxlength="125" size="25" name="email" value="" /></td>
        <td class="formtitle">Phone</td>
        <td class="formtitle"><input type="text" maxlength="20" size="18" name="phone" value="" /></td>
      </tr>
    </table>
	  <br />
	  <table width="550" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td colspan="4" class="formtopic">Seller Information </td>
        </tr>
        <tr>
          <td width="92" class="formtitle">Street Address </td>
          <td width="194" class="formtitle"><input type="text" size="25" name="streetaddress" value="" maxlength="80" /></td>
          <td width="85" class="formtitle">City</td>
          <td width="155" class="formtitle"><input type="text" size="25" name="city" value="" maxlength="80" /></td>
        </tr>
        <tr>
          <td class="formtitle">State</td>
          <td class="formtitle"><select name="state">
            <option value="__none__">Please select</option>
            <option value="AL" >Alabama</option>
            <option value="AK" >Alaska</option>
            <option value="AZ" >Arizona</option>
            <option value="AR" >Arkansas</option>
            <option value="CA" >California</option>
            <option value="CO" >Colorado</option>
            <option value="CT" >Connecticut</option>
            <option value="DE" >Delaware</option>
            <option value="DC" >District of Columbia</option>
            <option value="FL" >Florida</option>
            <option value="GA" >Georgia</option>
            <option value="HI" >Hawaii</option>
            <option value="ID" >Idaho</option>
            <option value="IL" >Illinois</option>
            <option value="IN" >Indiana</option>
            <option value="IA" >Iowa</option>
            <option value="KS" >Kansas</option>
            <option value="KY" >Kentucky</option>
            <option value="LA" >Louisiana</option>
            <option value="ME" >Maine</option>
            <option value="MD" >Maryland</option>
            <option value="MA" >Massachusetts</option>
            <option value="MI" >Michigan</option>
            <option value="MN" >Minnesota</option>
            <option value="MS" >Mississippi</option>
            <option value="MO" >Missouri</option>
            <option value="MT" >Montana</option>
            <option value="NE" >Nebraska</option>
            <option value="NV" >Nevada</option>
            <option value="NH" >New Hampshire</option>
            <option value="NJ" >New Jersey</option>
            <option value="NM" >New Mexico</option>
            <option value="NY" >New York</option>
            <option value="NC" >North Carolina</option>
            <option value="ND" >North Dakota</option>
            <option value="OH" >Ohio</option>
            <option value="OK" >Oklahoma</option>
            <option value="OR" >Oregon</option>
            <option value="PA" >Pennsylvania</option>
            <option value="RI" >Rhode Island</option>
            <option value="SC" >South Carolina</option>
            <option value="SD" >South Dakota</option>
            <option value="TN" >Tennessee</option>
            <option value="TX" >Texas</option>
            <option value="UT" >Utah</option>
            <option value="VT" >Vermont</option>
            <option value="VI" >Virgin Islands</option>
            <option value="VA" >Virginia</option>
            <option value="WA" >Washington</option>
            <option value="WV" >West Virginia</option>
            <option value="WI" >Wisconsin</option>
            <option value="WY" >Wyoming</option>
            <option value="OT" >Other</option>
          </select></td>
          <td class="formtitle">Zip</td>
          <td class="formtitle"><span class="arial13">
            <input type="text" size="5" name="zip" value="" maxlength="5" />
          </span></td>
        </tr>
        <tr>
          <td class="formtitle">Bedrooms</td>
          <td class="formtitle"><span style="padding-right:15px">
            <select name="bed_rooms" class="ddStyle">
              <option value="__none__">Please select</option>
              <option value="0" >0</option>
              <option value="100" >1</option>
              <option value="130" >Studio</option>
              <option value="200" >2</option>
              <option value="300" >3</option>
              <option value="400" >4</option>
              <option value="500" >5</option>
            </select>
          </span></td>
          <td class="formtitle">Baths</td>
          <td class="formtitle"><select name="bath_rooms" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="0" >0</option>
            <option value="100" >1</option>
            <option value="150" >1.5</option>
            <option value="200" >2</option>
            <option value="250" >2.5</option>
            <option value="300" >3</option>
            <option value="350" >3.5</option>
            <option value="400" >4</option>
            <option value="450" >4.5</option>
          </select></td>
        </tr>
        <tr>
          <td class="formtitle">Home Type </td>
          <td class="formtitle"><select name="property_type" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >Single Family Detached</option>
            <option value="4" >Condominium</option>
            <option value="2" >Townhouse</option>
            <option value="3" >Rental</option>
            <option value="8" >Vacation Home</option>
            <option value="9" >Multi-Family</option>
            <option value="5" >Loft</option>
            <option value="6" >Mixed Use</option>
            <option value="7" >Co-op</option>
            <option value="10" >Manufactured House</option>
            <option value="100" >Other</option>
          </select></td>
          <td class="formtitle">Condition</td>
          <td class="formtitle"><select name="property_condition" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >Excellent</option>
            <option value="2" >Good</option>
            <option value="3" >Needs Some Work</option>
            <option value="4" >Fixer upper</option>
          </select></td>
        </tr>
        <tr>
          <td class="formtitle">Relationship</td>
          <td class="formtitle"><select name="relation" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >Property owner</option>
            <option value="2" >Renter</option>
            <option value="3" >Investor</option>
            <option value="4" >Potential buyer</option>
            <option value="5">Other</option>
                    </select></td>
          <td class="formtitle">Lot Size</td>
          <td class="formtitle"><select name="lot_size" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >Less than 1/4 acre</option>
            <option value="2" >1/4 acre to 1/2 acre</option>
            <option value="3" >1/2 acre to 1 acre</option>
            <option value="4" >1 to 3 acres</option>
            <option value="5" >More than 3 acres</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2" class="formtitle"><b>Is the home currently listed?</b></td>
          <td colspan="2" class="formtitle"><select name="listed" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="0" >Not listed</option>
            <option value="1" >Currently listed with an agent</option>
            <option value="2" >For sale by owner</option>
          </select></td>
          </tr>
        <tr>
          <td colspan="2" class="formtitle"><b>Looking to buy another home?</b></td>
          <td colspan="2" class="formtitle"><input type="radio" name="description" value="sell+buy" />
Yes
  <input type="radio" name="description" value="sell-only" />
No </td>
        </tr>
        <tr>
          <td colspan="2" class="formtitle"><b>Purpose</b> for selling your home? </td>
          <td colspan="2" class="formtitle"><select name="select2" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >Want to list and sell my home</option>
            <option value="4" >Interested in market</option>
            <option value="5" >Interested in value for refinance</option>
            <option value="3" >Want to purchase a home</option>
          </select></td>
        </tr>
        <tr>
          <td colspan="2" class="formtitle"><b>Timeframe to Sell</b></td>
          <td colspan="2" class="formtitle"><select name="select3" class="ddStyle">
            <option value="__none__">Please select</option>
            <option value="1" >0 - 3 months</option>
            <option value="3" >4 - 6 months</option>
            <option value="6" >More than 6 months</option>
          </select></td>
        </tr>
      </table>
	  <br />
	  </form></td>
    <td width="183" align="center" valign="top" class="footer">&nbsp;</td>
  </tr>
</table>
