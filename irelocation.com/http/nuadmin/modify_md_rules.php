<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_md_rules.php
**********************************************************************
* Description:  Modifies a record in movingdirectory.rules
**********************************************************************
* Creation Date:  5/27/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
lead_id
user_id
origin
destination
email
name
is_premium
*/



include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":
		
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_md_rules.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Company ID*</b></legend>
					<input name=\"user_id\" type=\"text\" value=\"$user_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Origin</b></legend>
					<input name=\"origin\" type=\"text\" value=\"$origin\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Destination</b></legend>
					<input name=\"destination\" type=\"text\" value=\"$destination\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead Email*</b></legend>
					<input name=\"email\" type=\"text\" value=\"$email\" size=\"40\">
					</fieldset>
					</div>";
				
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update Rule</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_rule\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;

    case "insert_rule": 
		$sql = "insert into movingdirectory.rules set lead_id = '$lead_id', user_id = '$user_id', origin = '$origin', destination = '$destination', email = '$email' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Rule has been inserted for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

		
		break;


    case "get_record": //-- get the rule record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			
			$sql = "select * from movingdirectory.rules where lead_id = '$lead_id' ";
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id</p>";
				$body .= "<p><a href=\"modify_md_rules.php\">Try Again</a></p>";
				$body .= "<p><a href=\"modify_md_rules.php?action=add&lead_id=$lead_id\">Or Add a new Record</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id</p>";
				$body .= "<p>Notify Programmer</p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$id = $rs->myarray["id"];
				$user_id = $rs->myarray["user_id"];
				$origin = $rs->myarray["origin"];
				$destination = $rs->myarray["destination"];
				$email = $rs->myarray["email"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_md_rules.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Update Lead for $lead_id</b></legend>";


						$body .= "<div>
						<fieldset>
						<legend><b>Company ID</b></legend>
						<input name=\"user_id\" type=\"text\" value=\"$user_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Origin</b></legend>
						<input name=\"origin\" type=\"text\" value=\"$origin\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Destination</b></legend>
						<input name=\"destination\" type=\"text\" value=\"$destination\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Lead Email*</b></legend>
						<input name=\"email\" type=\"text\" value=\"$email\" size=\"40\">
						</fieldset>
						</div>";
					
	
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Update Rule</legend>
						<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Notes</legend>
						<p></p>
						</fieldset>
						</div>";
						
				$body .= "</fieldset></div>";
						
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_rule\">\n";
					$body .= "<input name=\"id\" type=\"hidden\" value=\"$id\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;


    case "update_rule": //-- update the record
        
		$sql = "update movingdirectory.rules set user_id = '$user_id', origin = '$origin', destination = '$destination', email = '$email' where id = '$id' ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Rule has been updated for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		
		if ( $lead_id ) { //-- if we have a lead_id, go ahead and redirect (no need to click a button again)
			header("location: modify_md_rules.php?action=get_record&lead_id=$lead_id");
			exit;
		} 
		
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">View/Modify Record in the <strong>MovingDirectory.Rules</strong> Table.</p>";
		
		$body .= "<form name=\"mod_rule\" action=\"modify_md_rules.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup Rule</legend>
			<input name=\"mod_rule\" type=\"submit\" value=\"Get Rule\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>