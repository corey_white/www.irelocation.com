<?php 
/* 
************************FILE INFORMATION**********************
* File Name:  end_of_month.php
**********************************************************************
* Description:  Runs the end of month Db inserts
**********************************************************************
* Creation Date:  11/30/10
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: This used to be run through different scripts, but there's no reason to run them separately.  This script will automatically run all three of the inserts.
**********************************************************************
*/

extract($_POST);

if ( $goAheadAndRunIt == "yes" ) {

	$foox = explode("/",$month);
	$dateX = $foox[0] . "/01/" . $foox[1];

	$new_month =date("Ym",strtotime($dateX));
	$get_month = date("Ym",strtotime("$dateX -28 days"));
	
	
	include_once "../inc_mysql.php";
	
	echo "Now running inserts for month $dateX<br /><br />";
	
	//-- Checking to see if there are already records for the rollover month
	$check = "select month from marble.campaign where month = '$new_month' limit 1 ";
	$rs = new mysql_recordset($check);

	if ( $rs->rowcount() > 0 ) {
		echo "Records already exist in <b>marble.campaign</b> for this month.  Insert terminated.<br />";
	} else {
		echo "Running insert on <b>marble.campaign</b>:<br />";
		$sql = "insert into marble.campaign (lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,needed,active,month,temp_goal) select  lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,monthly_goal as 'needed',1 as 'active', '$new_month' as 'month',0 as 'temp_goal' from marble.campaign where month = '$get_month' and active = 1 ";
	
		if ( $rs = new mysql_recordset($sql) ) {
			echo "Success!<br />";
		} else {
			echo "FAILED!<br />";
		}
		echo "<blockquote>$sql</blockquote>";

	}

	echo "<hr />";

	//-- Checking to see if there are already records for the rollover month
	$check = "select month from movingdirectory.campaign where month = '$new_month' limit 1 ";
	$rs = new mysql_recordset($check);
	
	if ( $rs->rowcount() > 0 ) {
		echo "Records already exist in <b>movingdirectory.campaign</b> for this month.  Insert terminated.<br />";
	} else {
	
		echo "Running insert on <b>movingdirectory.campaign</b>:<br />";
		$sql = "insert into movingdirectory.campaign (lead_id, comp_id, monthly_goal, leads_per_day, site_id, sort_order, needed, active, month, temp_goal, min_bedrooms, pull_order) select  lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,monthly_goal as 'needed',1 as 'active', '$new_month' as 'month',0 as 'temp_goal', min_bedrooms, pull_order from movingdirectory.campaign where month = '$get_month' and active = 1 ";
	
		if ( $rs = new mysql_recordset($sql) ) {
			echo "Success!<br />";
		} else {
			echo "FAILED!<br />";
		}
		echo "<blockquote>$sql</blockquote>";

	}

	echo "<hr />";
	
	
	//-- Checking to see if there are already records for the rollover month
	$check = "select month from movingdirectory.leadbased_affiliates where month = '$new_month' limit 1 ";
	$rs = new mysql_recordset($check);
	
	if ( $rs->rowcount() > 0 ) {
		echo "Records already exist in <b>movingdirectory.leadbased_affiliates</b> for this month.  Insert terminated.<br />";
	} else {
		echo "Running insert on <b>movingdirectory.leadbased_affiliates</b>:<br />";
		$sql = "insert into movingdirectory.leadbased_affiliates (comp_name, ppl, lead_type, month, source,localppl) select comp_name, ppl, lead_type,  '$new_month', source,localppl from movingdirectory.leadbased_affiliates where month = '$get_month' ";
	
		if ( $rs = new mysql_recordset($sql) ) {
			echo "Success!<br />";
		} else {
			echo "FAILED!<br />";
		}
		echo "<blockquote>$sql</blockquote>";

	}

	echo "<hr />";
	
	echo "<br />";
	
	echo "All Db inserts have completed.<br />";

} else {
	
	echo "This script will run the End of Month Db insertions needed for the reporting tool.<br /><br />";
	
	echo "<form action=\"end_of_month.php\" method=\"post\">Enter month to populate in MM/YYYY format: <input name=\"month\" type=\"text\" size=\"8\" maxlength=\"7\"><input name=\"goAheadAndRunIt\" type=\"submit\" value=\"yes\"></form>";
	
}




?>