<?php
/* 
************************FILE INFORMATION**********************
* File Name:  securitex_security_zips.php
**********************************************************************
* Description:  this script will update the zip codes for security leads going to Securitex (1622)
**********************************************************************
* Creation Date:  7-14-2008
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: this script deletes all of the leads for 1622 first, then checks to see if the zip code is already in the table and if not, inserts a new record for it for 1622 (Apex).
*/

echo "Code Stopped - see code.<br />";
exit;


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$added_zip = "";
$denied_zip = "";
$added_count = 0;
$denied_count = 0;


echo "Working on the $ZIP_TABLE table...<br /><br />";

//-- first, we delete all of the lead_ids of 1622 
$sql = "delete from $ZIP_TABLE where lead_id = 1622 ";
$rs = new mysql_recordset($sql);
/*
*/


//-- Enter all the zips into this array:
$zip_array = array("92152", );


//-- Need to insert all sequential zip codes from 3000 to 3999
for ( $i = 3000; $i < 4000; $i++ ) {
    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1622 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1622, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}

$added_zip = trim($added_zip,", ");
$denied_zip = trim($denied_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";
echo "<br />";
echo "$denied_count Zips that were not added: $denied_zip<br />";

?>