<?php
/* 
************************FILE INFORMATION**********************
* File Name:  sss_security_zips.php
**********************************************************************
* Description:  this script will update the zip codes for security leads going to Specialised Security Systems (1626)
**********************************************************************
* Creation Date:  5/19/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: this script deletes all of the leads for 1626 first, then inserts a new record for it for Specialised Security Systems (1626).
*/

/*
echo "Code Stopped - see code.<br />";
exit;
*/


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$added_zip = "";
$denied_zip = "";
$added_count = 0;
$denied_count = 0;


echo "Working on the $ZIP_TABLE table...<br /><br />";

//-- first, we delete all of the lead_ids of 1626 
$sql = "delete from $ZIP_TABLE where lead_id = 1626 ";
$rs = new mysql_recordset($sql);
/*
*/


//-- Enter all the zips into this array:
$zip_array = array("2000", "2021", "2022", "2024", "2027", "2029", "2030", "2034", "2035", "2040", "2041", "2093", "2095", "2103", "2107", "2108", "2110", "2112", "2154", "2060", "2062", "2063", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2088", "2089");


//-- now we check each zip code to see if this zip is already in use for another company
foreach($zip_array as $key => $value){
	$insert = "insert into $ZIP_TABLE set lead_id = 1626, zip = '$value' ";
	$rs_insert = new mysql_recordset($insert);
	$added_zip .= "$value, ";
	$added_count++;
}

$added_zip = trim($added_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";

?>