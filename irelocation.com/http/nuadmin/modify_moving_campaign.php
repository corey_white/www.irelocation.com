<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_moving_campaign.php
**********************************************************************
* Description:  Updates the campaign record in the movingdirectory.campaign table.  
						Moving/QUOTEMAILER.PHP has been updated to use this info for lead_id = 1386 - soon to be all lead_ids.
**********************************************************************
* Creation Date:  3/24/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
* NOTES: The leads_per_day column in the movingdirectory.campaign table is not currently being used for anything before, but now we are going to start using it for temporarilt changing the amount a moving company can get.  Implemented this in moving/quotemailer.php for Wheaton.
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add";
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_moving_campaign.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Campaign Month</b></legend>
					$trunc_month
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\" readonly>
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Company ID*</b></legend>
					<input name=\"comp_id\" type=\"text\" value=\"$comp_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Monthly Goal*</b></legend>
					<input name=\"monthly_goal\" type=\"text\" value=\"$monthly_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Leads Per Day*</b></legend>
					<input name=\"leads_per_day\" type=\"text\" value=\"$leads_per_day\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Temp Goal</b></legend>
					<input name=\"temp_goal\" type=\"text\" value=\"$temp_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Site ID*</b></legend>
					<input name=\"site_id\" type=\"text\" value=\"pm\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Active*</b></legend>
					<select name=\"active\">
						<option value=\"1\">Active</option>
						<option value=\"0\">Not Active</option>
					</select>

					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Add Campaign</legend>
					<input name=\"submit\" type=\"submit\" value=\"Add\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p></p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_campaign\">\n";
					$body .= "<input name=\"trunc_month\" type=\"hidden\" value=\"$trunc_month\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
		
		break;



	case "insert_campaign":
		$sql = "insert into movingdirectory.campaign set lead_id = '$lead_id', comp_id = '$comp_id', monthly_goal = '$monthly_goal', leads_per_day = '$leads_per_day', temp_goal = '$temp_goal', site_id = '$site_id', active = '$active', month = '$trunc_month'  ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Rule has been inserted for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}


		break;



    case "get_record": //-- get the campaign record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		$ts = trim($ts);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		If (!$ts ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing DATE</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			//-- convert data to proper format
			$ts = dateYearFirst($ts);
			$trunc_month = substr($ts,0,6);
			
			
			$sql = "select * from movingdirectory.campaign where lead_id = '$lead_id' and month = '$trunc_month' ";
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id for Month: $trunc_month</p>";
				$body .= "<p><a href=\"modify_moving_campaign.php\">Try Again</a></p>";
				$body .= "<p><a href=\"modify_moving_campaign.php?action=add&lead_id=$lead_id&trunc_month=$trunc_month\">Add a Moving Campaign for this Comapny</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id for Month: $trunc_month</p>";
				$body .= "<p>Notify Programmer</p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$cid = $rs->myarray["cid"];
				$comp_id = $rs->myarray["comp_id"];
				$monthly_goal = $rs->myarray["monthly_goal"];
				$leads_per_day = $rs->myarray["leads_per_day"];
				$temp_goal = $rs->myarray["temp_goal"];
				$site_id = $rs->myarray["site_id"];
				$active = $rs->myarray["active"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_moving_campaign.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Campaign Month</b></legend>
					$trunc_month
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Company ID*</b></legend>
					<input name=\"comp_id\" type=\"text\" value=\"$comp_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Monthly Goal*</b></legend>
					<input name=\"monthly_goal\" type=\"text\" value=\"$monthly_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Leads Per Day*</b></legend>
					<input name=\"leads_per_day\" type=\"text\" value=\"$leads_per_day\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Temp Goal</b></legend>
					<input name=\"temp_goal\" type=\"text\" value=\"$temp_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Site ID*</b></legend>
					<input name=\"site_id\" type=\"text\" value=\"$site_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Active*</b></legend>
					<select name=\"active\">
						<option value=\"1\"";
						if ( $active == "1" ) {
							$body .= " selected";
						} 
						$body .= ">Active</option>
						<option value=\"0\"";
						if ( $active == "0" ) {
							$body .= " selected";
						} 
						$body .= ">Not Active</option>
					</select>

					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update Campaign</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p></p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_campaign\">\n";
					$body .= "<input name=\"lead_id\" type=\"hidden\" value=\"$lead_id\">\n";
					$body .= "<input name=\"trunc_month\" type=\"hidden\" value=\"$trunc_month\">\n";
					$body .= "<input name=\"cid\" type=\"hidden\" value=\"$cid\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;

    case "update_campaign": //-- update the record
        
		$sql = "update movingdirectory.campaign set monthly_goal = '$monthly_goal', leads_per_day = '$leads_per_day', site_id = '$site_id', active = '$active' where cid = '$cid' ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Moving Campaign has been updated for: $lead_id for Month: $trunc_month</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;

    default: //-- display the initial form

/*
		if ( $lead_id ) { //-- if we have a lead_id, go ahead and redirect (no need to click a button again)
			header("location: modify_moving_campaign.php?action=get_record&lead_id=1600");
			exit;
		} 
*/
		
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">Modify record in the movingdirectory.campaign table.</p>";
		
		$body .= "<form name=\"mod_camp\" action=\"modify_moving_campaign.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Date</b> (choose any date in the month to choose that month)</legend>
				<input type=\"text\" name=\"ts\" value=\"$current_date\" readonly><a href=\"javascript:show_calendar4('document.mod_camp.ts', document.mod_camp.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
				</fieldset>
				</div>
			
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup Campaign</legend>
			<input name=\"mod_camp\" type=\"submit\" value=\"Get Campaign\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>