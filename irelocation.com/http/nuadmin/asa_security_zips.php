<?php
/* 
************************FILE INFORMATION**********************
* File Name:  asa_security_zips.php
**********************************************************************
* Description:  this script will adds the zip codes for security leads going to ASA (1634) for the range 4000 - 4299
**********************************************************************
* Creation Date:  8/22/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

*/

echo "Code Stopped - see code.<br />";
exit;


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$added_zip = "";
$denied_zip = "";
$added_count = 0;
$denied_count = 0;


echo "Working on the $ZIP_TABLE table...<br /><br />";

//-- Need to insert all sequential zip codes from 4000 to 4299
for ( $i = 4000; $i < 4300; $i++ ) {
    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1634 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1634, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}

$added_zip = trim($added_zip,", ");
$denied_zip = trim($denied_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";
echo "<br />";
echo "$denied_count Zips that were not added: $denied_zip<br />";

?>