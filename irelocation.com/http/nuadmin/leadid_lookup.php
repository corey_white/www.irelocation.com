<?
/* 
************************FILE INFORMATION**********************
* File Name:  leadid_lookup.php
**********************************************************************
* Description:  Looks up the Company ID from the lead_id
**********************************************************************
* Creation Date:  5/27/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));

if ( $action == "get_record" ) {

	//-- clean up the inputed data
	$lead_id = trim($lead_id);
	
	//-- Validation
	$error_msg = "";
	
	If (!$lead_id ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing LEAD ID</li>";
	}
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		
		
		$sql = "select comp_id from movingdirectory.directleads where lead_id = '$lead_id' ";
		#echo $sql;
		
		$rs=new mysql_recordset($sql);
		$row_count = $rs->rowcount();
		
		
		if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
			
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p class=\"\" align=\"center\">No Company ID found for Lead ID: $lead_id</p>";
			
			$body .= "<p><a href=\"$PHP_SELF\">Try Again</a></p>";
			
			$body .= "</div>";
			
			$body .= "</div>";
			
		} elseif ( $row_count > 1 ) { // whoa, found more than 1
		
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p class=\"\" align=\"center\">Found more than one COMP_ID for Lead ID: $lead_id</p>";
			$body .= "<p>Notify Programmer</p>";
			
			$body .= "</div>";
			
			$body .= "</div>";
		
		} else { //-- okay, we found a record, let's show it
			
			$camp_arr = $rs->fetch_array();
			
			$comp_id = $rs->myarray["comp_id"];

			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			
			$body .= "<div style=\"position: static;\">";

				$body .= "<div>
				<fieldset>
				<legend><b></b></legend>
				<p>Company ID for $lead_id is: $comp_id</p>
				<p><a href=\"http://www.irelocation.com/nuadmin/company_lookup.php?key=$comp_id&type=auto&s=Lookup\">Look Up Auto Company with this ID</a></p>
				<p><a href=\"http://www.irelocation.com/nuadmin/company_lookup.php?key=$comp_id&type=moving&s=Lookup\">Look Up Moving Company with this ID</a></p>
				<p><a href=\"index.php\">NuAdmin Index</a></p>
				</fieldset>
				</div>";
			
			$body .= "</div>";
			
			$body .= "</div>";
			
		}


	}

} else {
	$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
	
	$body .= "<p align=\"center\">View/Modify Record in the <strong>$TABLE</strong> Table.</p>";
	
	$body .= "<form name=\"disp1\" action=\"$PHP_SELF\" method=\"post\">";
	
	$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
		<div style=\"position: static;\">
		
			<div>
			<fieldset>
			<legend><b>LEAD ID</b></legend>
			<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
			</fieldset>
			</div>
			
		</div>
	
		<div style=\"clear: both;\">
		<fieldset>
		<legend>Lookup Company ID</legend>
		<input name=\"submit\" type=\"submit\" value=\"Lookup\"> <a href=\"index.php\">Cancel</a>
		</fieldset>
		</div>
	
	</div>
	<input name=\"action\" type=\"hidden\" value=\"get_record\">
	</form>";
        
}


echo $body;

?>