<?
/* 
************************FILE INFORMATION**********************
* File Name:  view_cpc.php
**********************************************************************
* Description:  Views all of the CPC for a given date
**********************************************************************
* Creation Date:  3/21/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}
// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}

$yesterday = date('m/d/Y',strtotime("yesterday"));

if ( !$ts ) {
    $ts = $yesterday;
} 


$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
$body .= "<p align=\"center\">Enter Date to view CPC Data.</p>";
$body .= "<form name=\"viewCPC\" action=\"view_cpc.php\" method=\"post\">
<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">

<div style=\"position: static;\">

<div>
<fieldset>
<legend><b>Date</b></legend>
<input type=\"text\" name=\"ts\" value=\"$ts\" readonly><a href=\"javascript:show_calendar4('document.viewCPC.ts', document.viewCPC.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
</fieldset>
</div>


</div>


<div style=\"clear: both;\">
<fieldset>
<legend>Get Data</legend>
<input name=\"view_cpc\" type=\"submit\" value=\"View CPC\"> <a href=\"index.php\">Cancel</a>";

if ( $view_cpc == "View CPC" ) {
	$body .= "<p><a href=\"add_cpc.php\">Add New CPC Data</a></p>";
}

$body .= "</fieldset>
</div>

</div>

</form>";

if ( $view_cpc == "View CPC" ) { // if we have a submission, process

	//-- clean up the inputed data
	$ts = trim($ts);
	
    //-- Validation
	$error_msg = "";
	
	If (!$ts ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing DATE</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		
		//-- convert data to proper format
		$ts = dateYearFirst($ts);
		
		
		//-- we're doing the SQL this way so that I can later convert this into a multi-form input (inserting multiple records at once from the form)
		$sql = "select * from movingdirectory.costperclick where ts = '$ts' ";
		#echo $sql;
		$rs = new mysql_recordset($sql);
		
		$display_date = dateYearLast($ts);
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";

		$body .= "<table>";
			$body .= "<tr>
			<td colspan=\"5\"><p align=\"center\"><strong>CPC Data for $display_date</strong></p></td>
			</tr>";
			$body .= "<tr>
			<td>Search Engine</td>
			<td>Site Id</td>
			<td>Cost</td>
			<td>Clicks</td>
			<!-- <td>Edit</td> -->
			</tr>";
		
		while ($row_rs = $rs->fetch_array()) {
			$id = $rs->myarray["id"];
			$search_engine = $rs->myarray["search_engine"];
			$site = $rs->myarray["site"];
			$cost = $rs->myarray["cost"];
			$clicks = $rs->myarray["clicks"];
			
			$body .= "<tr>
			<td width=\"150\">$search_engine</td>
			<td width=\"100\">$site</td>
			<td width=\"75\">$cost</td>
			<td width=\"75\">$clicks</td>
			<td width=\"100\"><a href=\"edit_cpc.php?id=$id&action=lookup\">Edit</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href=\"edit_cpc.php?id=$id&action=confirm\">Delete</a></td>
			</tr>";
		}
		$body .= "</table>";
		$body .= "</div>";

	}
		
}
    

echo $body;

?>