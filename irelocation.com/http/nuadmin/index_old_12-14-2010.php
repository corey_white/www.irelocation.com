<?php
/* 
************************FILE INFORMATION**********************
* File Name:  index.php
**********************************************************************
* Description:  The main home page for the new set of admin scripts that are being created.
**********************************************************************
* Creation Date:  3/20/08
**********************************************************************
* Modifications (Date/Who/Details):
		3/21/08  - Rob - Added sessions and security stuff
		5/27/08 - Rob - reworked code so that there was a distinction between auto and moving code.
**********************************************************************
*/

session_start();	
include_once $_SERVER['DOCUMENT_ROOT']."/nuadmin/inc/security.php";

$body .= "<html>
<head>
<title>Rob's Super Cool NuAdmin Scripts</title>
<style type=\"text/css\">
p { color: black; font-family: helvetica,arial,sans-serif; font-size: 10pt; font-style: normal; font-variant: normal; font-weight: normal;  }
a { color: blue; font-family: helvetica,arial,sans-serif; font-size: 10pt; font-style: normal; font-variant: normal; font-weight: normal;  }
a:visited { color: darkblue; font-family: helvetica,arial,sans-serif; font-size: 10pt; font-style: normal; font-variant: normal; font-weight: normal;  }
legend { color: black; font-family: helvetica,arial,sans-serif; font-size: 12pt; font-style: normal; font-variant: normal; font-weight: bold;  }

</style>

</head>
<body>";


//-- BACK TO PROJECTS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"clear: both;\">
	<fieldset>
	<p class=\"\"><a href=\"http://www.irelocation.com/project/home.php\">Back to Projects</a></p>
	</fieldset>
	</div>

	</div>

	</div>
";

//-- COMPANY LOOKUP ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Company Lookup Admin</b></legend>
	<p class=\"\"><a href=\"company_lookup.php?type=auto\">Company Lookup: AUTO/ATUS (not fully functional)</a></p>
	<p class=\"\"><a href=\"company_lookup.php?type=moving\">Company Lookup: MOVING (not fully functional)</a></p>
	</fieldset>
	</div>

	</div>

	</div>
";


//-- COMPANY LOOKUP BASED ON LEAD ID
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Lead ID Lookup</b></legend>
	<p class=\"\"><a href=\"leadid_lookup.php?type=auto\">Company Lookup by Lead ID</a></p>
	</fieldset>
	</div>

	</div>

	</div>
";


//-- MOVING ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
		<div style=\"position: static;\">
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend><b>Moving, Other Admin</b></legend>

			<p class=\"\"><a href=\"add_moving_zip_codes.php\">Add Zip Codes to the Marble.Moving_Zip_Codes Table</a></p>

			<p class=\"\"><a href=\"add_security_zip_codes.php\">Add Zip Codes to the Irelocation.Security_Zip_Codes Table</a></p>

			<p class=\"\"><a href=\"modify_moving_campaign.php\">View/Edit a Moving Campaign</a></p>

			</fieldset>
			</div>
	
		</div>

	</div>
";
		
//-- CPC ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
		<div style=\"position: static;\">
			
			<div style=\"clear: both;\">
			<fieldset>
			<legend><b>CPC Admin</b></legend>

			<p class=\"\"><a href=\"add_cpc.php\">Add CPC Data</a></p>
			<p class=\"\"><a href=\"view_cpc.php\">View CPC Data</a></p>
			<p class=\"\"><a href=\"cpc_import.php\">Import Bulk CPC Data</a></p>

			</fieldset>
			</div>
		
	
		</div>

	</div>
";
		
		
//-- AUTO & ATUS ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>AUTO & ATUS Admin</b></legend>
	<p class=\"\"><a href=\"modify_auto_atus_campaign.php\">Modify AUTO/ATUS Campaign</a></p>
	<p class=\"\"><a href=\"rerun_campaignmapping.php\">Rerun CampaignMapping</a></p>
	</fieldset>
	</div>

	</div>
";
	
	
	
//-- MISC ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Miscellany</b></legend>
	<p class=\"\"><a href=\"callcenter_leads.php\">Call Center Lead Check</a></p>
	<p class=\"\"><a href=\"merchant_leads.php\">Merchant Lead Check</a></p>
	<p class=\"\"><a href=\"import_security_lead_ushome.php\">Import USHOME Security Lead</a></p>
	<p class=\"\"><a href=\"import_security_lead_DUP_CSV_IMPORT.php\">Transfer Security Lead from our Db into LeadExec</a></p>
	<p class=\"\"><a href=\"import_security_lead_CSV_IMPORT.php\">Import Security CSV Line into LeadExec</a></p>
	<p class=\"\"><a href=\"import_auto_lead_CSV_IMPORT.php\">Import AUTO CSV Line into LeadExec</a></p>
	<p class=\"\"><a href=\"date_time_check.php\">Check Date/Time on Server</a></p>
	<p class=\"\"><a href=\"sequence_numbers.php\">Output range of numbers (like Zips)</a></p>
	<p class=\"\"><a href=\"http://topmoving.com/radius.php\" target=\"_blank\">Zip Code Radius</a></p>
	<p class=\"\"><a href=\"modify_cost_per_click.php\">View/Edit Cost Per Lead </a></p>
	<p class=\"\"><a href=\"bad_emails_bettercheck.php\">Deep Email Check </a></p>
	<p class=\"\"><a href=\"simple_email_check.php\">Simple Email Check </a></p>
	<p class=\"\"><a href=\"http://www.irelocation.com/project/clickbooth_leads.php\">Click Booth Leads </a></p>
	<p class=\"\"><a href=\"end_of_month.php\">End of Month Rollover </a></p>
	</fieldset>
	</div>

	</div>
";
	

//-- RETRO-IMPORT LINKS - taking a CSV file and importing data back into our local Db
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Retro-Import</b></legend>
	<p class=\"\"><a href=\"import_le_auto_into_db.php\">Retro-Import Auto Leads</a></p>
	<p class=\"\"><a href=\"import_le_security_into_db.php\">Retro-Import Security Leads</a></p>
	</fieldset>
	</div>

	</div>
";
	

//-- XXX ADMIN LINKS
/*
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>XXX Admin</b></legend>
	<p class=\"\">XXX</p>
	</fieldset>
	</div>

	</div>
";
*/
	
$body .= "</body></html>";



echo $body;
?>