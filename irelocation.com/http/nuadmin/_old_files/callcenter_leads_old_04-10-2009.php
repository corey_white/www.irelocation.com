<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_md_rules.php
**********************************************************************
* Description:  Modifies a record in movingdirectory.rules
**********************************************************************
* Creation Date:  5/27/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

OP NOTES:

GOAL - display all call center leads so they can be called on and verified as good leads.  Once called, leads are marked as either 'bad' or 'good.'  If marked as good, they can be sent, if marked bad, they are not sent in through the quotemailer.

Mark good by setting ready_to_send = 1 and probably should add a flag so we know that this was pushed through this process.
Mark bad by setting ready_to_send = 4 (or something)



*/

#extract($_GET);

include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$row_clr = "#FFFFFF";
$row_clr2 = "#EEEEEE";
$thead_clr = "#B0CACE";

function color_mix() {
  global $color_cnt, $row_clr, $row_clr2;
  if(!$color_cnt) $color_cnt = 0;
  if($color_cnt % 2 == 0) $mix_bg = $row_clr2;
  else $mix_bg = $row_clr;
  $color_cnt++;
  return $mix_bg;
}


$current_date = date('m/d/Y',strtotime("now"));

$body .= "<style type=\"text/css\">
p,td { color: black; font-family: helvetica,arial,sans-serif; font-size: 8pt; font-style: normal; font-variant: normal; font-weight: normal;  }
th { color: black; font-family: helvetica,arial,sans-serif; font-size: 9pt; font-style: normal; font-variant: normal; font-weight: bold;  }

</style>";



if ( $mark == "good" ) {
    //-- update record
	$sql = "update movingdirectory.leads_callcenter set ready_to_send = 1 where quote_id = '$qid' ";
	#$body .= "$sql<br />";
	$rs = new mysql_recordset($sql);

    $body .= "<h3 align=\"center\">$qid is marked as good</h3>";
} 

if ( $mark == "bad" ) {
    //-- update record
	$sql = "update movingdirectory.leads_callcenter set ready_to_send = 8 where quote_id = '$qid' ";
	#$body .= "$sql<br />";
	$rs = new mysql_recordset($sql);

    $body .= "<h3 align=\"center\">$qid is marked as bad</h3>";
} 


//-- get call center leads that haven't been checked yet (ready_to_send = 9)

$sql = "select * from movingdirectory.leads_callcenter where ready_to_send = 9 ";
$rs = new mysql_recordset($sql);

$body .= "<table width=\"1000\"  style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\" align=\"center\">";
$body .= "<tr bgcolor=\"cba987\">";
$body .= "<th width=\"75\" align=\"left\">Quote ID</td>";
$body .= "<th width=\"75\" align=\"left\">Date</td>";
$body .= "<th width=\"100\" align=\"left\">Name</td>";
$body .= "<th width=\"150\" align=\"left\">Phone/Email</td>";
$body .= "<th width=\"150\" align=\"left\">Service Type/<br />Call Qty/<br />Time Frame/<br />Best Time</td>";
$body .= "<th width=\"200\" align=\"left\">Comments</td>";
$body .= "<th width=\"75\" align=\"left\">&nbsp;</td>";
$body .= "<th width=\"75\" align=\"left\">&nbsp;</td>";
$body .= "</tr>";

$L_count = 0;
while ($rs->fetch_array()) {
	$L_count++;
	$body .= "<tr bgcolor=\"".color_mix()."\">";
	
#	$date = substr($rs->myarray["dt"],5,2) . "-" . substr($rs->myarray["dt"],8,2) . "-" . substr($rs->myarray["dt"],0,4);
	$phone = substr($rs->myarray["phone"],0,3) . "-" . substr($rs->myarray["phone"],3,3) . "-" . substr($rs->myarray["phone"],6,4);
	
	$body .= "<td>" . $rs->myarray["quote_id"] . "</td>";
	$body .= "<td>" . $rs->myarray["dt"] . "</td>";
	$body .= "<td>" . $rs->myarray["first_name"] . " " . $rs->myarray["last_name"] . "</td>";
	$body .= "<td>$phone<br />" . $rs->myarray["email"] . "</td>";
	$body .= "<td>" . $rs->myarray["service_type"] . "<br />" . $rs->myarray["call_num"] . "<br />" . $rs->myarray["timeline"] . "<br />" . $rs->myarray["contact"] . "</td>";
	$body .= "<td>" . $rs->myarray["comment"] . "</td>";
	$body .= "<td><a href=\"callcenter_leads.php?mark=good&qid=" . $rs->myarray["quote_id"] . "\">Good Lead</a></td>";
	$body .= "<td><a href=\"callcenter_leads.php?mark=bad&qid=" . $rs->myarray["quote_id"] . "\">Bad Lead</a></td>";

	$body .= "</tr>";
}

if ( $L_count == 0 ) {
    $body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"8\" align=\"right\"><img src=\"bananahuge.gif\" alt=\"\" width=\"182\" height=\"180\" border=\"0\"></td></tr>";
} 

$body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"8\"><a href=\"callcenter_leads.php\">Reset</a></td></tr>";

$body .= "</table>";

echo $body;

?>