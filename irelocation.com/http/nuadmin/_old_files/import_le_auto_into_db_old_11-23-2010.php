<?php 
/* 
************************FILE INFORMATION**********************
* File Name:  import_le_auto_into_db.php
**********************************************************************
* Description:  This script takes a CSV file of lead data from LeadExec and imports back into our local Db so we can use our existing reporting tool.
**********************************************************************
* Creation Date:  10/11/10 - Rob Hanus
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once "../inc_mysql.php";

switch ( $action ) {

	case "get_file": //-- upload the file and process it
	
		echo "Processing: " . $_FILES['userfile']['name'] . "<br /><br />";
		
	
		// Where the file is going to be placed 
		$target_path = "csv_uploads/";
		
		//- Add the original filename to our target path.  Result is "uploads/filename.extension" 
		$target_path = $target_path . $_FILES['userfile']['name']; 
		
		echo "Uploading to: $target_path<br />";
		
		if(!move_uploaded_file($_FILES['userfile']['tmp_name'], $target_path)) {
			echo "There was an error uploading the file, please try again!";
			exit;
		}
		
		$lines = file($target_path);
		
	
		// Loop through our array, show HTML source as HTML source; and line numbers too.
		foreach ($lines as $line_num => $line) {
			echo "Line #{$line_num} : " . htmlspecialchars($line) . "\n";
			
			//-- Check to see if there is a | character, which preceeds the lead_id (it's presumed that a pipe won't be added to the company name unless the lead_id was also added)
			$pos = strpos($line, "|");
			if ( $pos === false ) {
				echo "<br /><span style=\"color: red; font-weight: bold;\">MISSING LEAD ID: $line</span><br /><br />";
				$missing_ids .= "$line<br />";
			} else {
				
				$data = explode(",",$line);
				$data2 = explode("|",$data[0]);
				
				$lead_id = "|" . $data2[1];
				$quote_id = trim($data[1]," ");
				
				if ( $quote_id < 100 ) { //-- Test to make sure we have a quote_id (I'm not sure why I have to test for it this way, but other methods didn't work.
					echo "<span style=\"color: green; font-weight: normal;\">MISSING Quote ID on Line #{$line_num} ~ Not Imported Into Database</span>";
				} else {

					$updated_quoteids .= $quote_id . ", ";
					
					if ( $test_run != "yes" ) { //-- we only dump into the Db if we're not set to TEST RUN
						$sql = "update marble.auto_quotes set lead_ids=concat(lead_ids,'$lead_id') where quote_id = $quote_id ";
						#$rs = new mysql_recordset($sql);
						echo "<br />&nbsp;&nbsp;&nbsp;&nbsp;$sql";
						
					} else {
						echo "<span style=\"color: blue; font-weight: normal;\">Test Run ~ Not Imported Into Database</span>";
					}
					
				}
				
				echo "<br /><br />";
			}
			
		}
		
		echo "<hr /><hr />";
		
		$updated_quoteids = trim($updated_quoteids,", ");
		echo "<b>Quote IDs updated:</b> <br />$updated_quoteids";
		echo "<br /><br />";
		
		echo "<span style=\"color: red; font-weight: bold;\">~~~~ Missing IDS: ~~~~</span><br />$missing_ids<br />";
		echo "<hr />";

    default: //-- display the initial form
		
		$body .= "<h1 align=\"center\">WARNING: As soon as you click the Upload button, these records will be entered into the database!</h1>";
		$body .= "<p align=\"center\">Select CSV File to import AUTO Lead Data</p>";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
			<form enctype=\"multipart/form-data\" action=\"import_le_auto_into_db.php\" method=\"POST\">
			<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"100000\" />
			Choose a file to upload: <input name=\"userfile\" type=\"file\" /><br />
			Test Run (no import): <input name=\"test_run\" type=\"checkbox\" value=\"yes\" checked><br /><br />
			<input type=\"submit\" value=\"Upload File\" />
			<input name=\"action\" type=\"hidden\" value=\"get_file\">
			</form>
		
		</div>";
        
        break;
}


echo $body;




?>