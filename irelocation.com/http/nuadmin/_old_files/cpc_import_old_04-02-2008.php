<?
/* 
************************FILE INFORMATION**********************
* File Name:  cpc_import.php
**********************************************************************
* Description:  This will import the CP data from pasted text
**********************************************************************
* Creation Date:  3/31/08
**********************************************************************
* Modifications (Date/Who/Details):
	4/1/08 16:03 PM - Rob - finishing up initial code
**********************************************************************
NOTES: This is the first phase, where we are simply taking text that has been pasted into a textarea and parsing it out for the CPC data, and inserting it into the DB.

Second phase will be making the script get the data automatically from the email server.
*/

include_once "../inc_mysql.php";


// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("yesterday"));

//-- create site_id array
//-- ######>>>  ADD ADDITIONAL SITE_IDS HERE
$site_id_arr = array(
"autoshipping.com - all" => "asc",
"autoshipping.com - content" => "asc",
"movemycar.com - all" => "mmc",
"topmoving.com_all" => "tm",
"movemycar.com - content" => "mmc",
"canada.topalarmcompanies.com" => "ctac",
"promoving.com - all" => "pm",
"topalarmcompanies.com - all" => "tac",
"australia.topalarmcompanies.com" => "atac",
"topalarmcompanies.com - content" => "tac",
"canada.topalarmcompanies.com - content" => "ctac",
"australia.topalarmcompanies.com - content" => "atac",
"promoving.com - content" => "pm",
"movingease.com - all" => "me"
);

/*
echo "<pre>";
print_r($site_id_arr);
echo "</pre>";
*/

/*
if ( array_key_exists("canada.topalarmcompanies.com - content",$site_id_arr) ) { //-- testing to see if the value is a key
	echo "TEST IN ARRAY<br /><br />";
} else {
	echo "TEST NOT in array<br /><br />";
}
*/

switch ( $action ) {

    case "import": //-- update the record
        
		//-- split all lines
		$lines_arr = split("\r",$cpc_text);
		
		//-- convert date
		$date = dateYearFirst($ts);
		
		foreach($lines_arr as $key => $value){
			#echo"Key: $key - Value: $value<br>";
			
			//-- split each value by tab
			$val_arr = split("\t",$value);
			
/*
			echo "val_arr[0] = $val_arr[0]<br />";
			echo "val_arr[1] = $val_arr[1]<br />";
			echo "val_arr[2] = $val_arr[2]<br />";
*/
			
			$low_val = strtolower(trim($val_arr[0]));
			
			#echo "CHECKING $low_val ...<br />";
			
			//-- check $val_arr[0] to see if it is in the site_id array, and if so, then set up for import into db
			if ( array_key_exists($low_val,$site_id_arr) ) { //-- value is in array
				
				$site_id = $site_id_arr[$low_val];
				$clicks = trim($val_arr[1]);
				$cost = trim($val_arr[2]);
				
				/*
				echo "Site ID = $site_id<br />";
				echo "Clicks = $clicks<br />";
				echo "Cost = $cost<br />";
				*/

				//-- strip $ and , (comma) from cost
				$cost = str_replace("$","",$cost);
				$cost = str_replace(",","",$cost);
				
				$values .= "('$search_engine','$site_id','$cost','$clicks','$date'), ";
				
			} 
			
		}
		
		$values = trim($values,", ");
		$insert = "insert into movingdirectory.costperclick (search_engine,site,cost,clicks,ts) values $values";
		
		#echo "<br>$insert<br><br>";
		
		if ( $rs=new mysql_recordset($insert) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>CPC Data has been added for $date</p>";
			$body .= "<p>Here is what was entered:</p>";
			
			$output = split(", ",$values);
			$body .= "<blockquote>";
			foreach($output as $key => $value){
				$body .= "$value <br />";
			}
			$body .= "</blockquote>";
			
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		
		$body .= "<p align=\"center\">View/Modify Record in the <strong>$TABLE</strong> Table.</p>";
		
		$body .= "<form name=\"disp1\" action=\"cpc_import.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>CPC Date</b></legend>
				<input type=\"text\" name=\"ts\" value=\"$current_date\" readonly><a href=\"javascript:show_calendar4('document.disp1.ts', document.disp1.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Search Engine</b></legend>
				<select name=\"search_engine\">
				<option value=\"google\">Google</option>
				<option value=\"overture\">Yahoo / Overture</option>
				<option value=\"msn\">MSN</option>
				</select>
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Paste CPC Text Here</b></legend>
				<textarea name=\"cpc_text\" rows=\"20\" cols=\"40\"></textarea>
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Submit CPC Data</legend>
			<input name=\"submit\" type=\"submit\" value=\"Import\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"import\">
		</form>";
        
        break;
}


echo $body;

?>