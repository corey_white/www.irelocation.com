<?
/* 
************************FILE INFORMATION**********************
* File Name:  add_security_zip_codes.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  7/22/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once "../inc_mysql.php";


if ( $add_submit == "Add Security Lead" ) { // if we have a submission, process
    
    //-- Validation
	$error_msg = "";
	
	If (!$sec_lead ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Lead Info</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		$sec_lead = trim($sec_lead,",");
		
		//-- put zips into array
		$sec_lead_arr = split(",",$sec_lead);
		
		$source = addslashes($sec_lead_arr[0]);
		$quote_type = addslashes($sec_lead_arr[1]);
		$fname = addslashes($sec_lead_arr[2]);
		$lname = addslashes($sec_lead_arr[3]);
		$phone1 = addslashes($sec_lead_arr[4]);
		$phone2 = addslashes($sec_lead_arr[5]);
		$email = addslashes($sec_lead_arr[6]);
		$address = addslashes($sec_lead_arr[7]);
		$city = addslashes($sec_lead_arr[8]);
		$state = addslashes($sec_lead_arr[9]);
		$zip = addslashes($sec_lead_arr[10]);
		$current_needs = addslashes($sec_lead_arr[11]);
		$building_type = addslashes($sec_lead_arr[12]);
		$num_locations = addslashes($sec_lead_arr[13]);
		$sqr_footage = addslashes($sec_lead_arr[14]);
		$own_rent = addslashes($sec_lead_arr[15]);
		$remote_ip = addslashes($sec_lead_arr[16]);
		$campaign = addslashes($sec_lead_arr[17]);
		
		echo "<br />";
		
		//-- Check for Dup
		$daysago2 = date("Ymd",strtotime("-2 days"));
		$dup_check = "select count(*) as count from irelocation.leads_security where left(received,8) >= '$daysago2' and source = '$sec_lead_arr[0]' and phone1 = '$sec_lead_arr[4]' and email = '$sec_lead_arr[6]'  ";
		$rs_dup = new mysql_recordset($dup_check);
		$rs_dup->fetch_array();
		$count = $rs_dup->myarray["count"];

		
/*
		echo "daysago2 = $daysago2<br />";
		echo "dup_check = $dup_check<br />";
		echo "There are $count dups for this lead.<br />";
		echo "<br />";
*/
		
		if ( $count > 0 ) {
			echo "This is a Duplicate Lead - Not entered into Db.<br />";
		} else {
			
			$sql = "insert into irelocation.leads_security set received = '',  source = '$source', quote_type = '$quote_type', name1 = '$fname', name2 = '$lname', phone1 = '$phone1', phone2 = '$phone2', email = '$email', address = '$address', city = '$city', state_code = '$state', zip = '$zip', current_needs = '$current_needs', building_type = '$building_type', num_location = '$num_locations', sqr_footage = '$sqr_footage', own_rent = '$own_rent', remote_ip = '$remote_ip', ready_to_send = 1, campaign = '$campaign'  ";
			
			echo $sql;
			
			if ( $rs=new mysql_recordset($sql) ) {
				$body .= "<p align=\"center\">Security Lead Inserted.</p>";

				$quote_id = $rs->last_insert_id();
				
				//-- import into LeadExec
				include "lead_exec_code_security.php";
				$body .= "<p align=\"center\">Security Lead imported into LeadExec</p>";
	
				$body .= "<p align=\"center\"><a href=\"import_security_lead.php\">Add another Security Lead into irelocation.leads_security</a></p>";
				
			} else {
				$body .= "Error inserting into the database.  See programmer.";
			}

		}
		

	}
    
} else { // display form
    
    $body .= "<p align=\"center\">Enter Lead into the irelocation.leads_security table.</p>";
	$body .= "<form action=\"import_security_lead.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"float: left; margin-left: 0px; width: 500px;\">
	<fieldset>
	<legend><b>Data Columns:</b></legend>
	<small>Source Code, Quote Type (res/com), First Name, Last Name, Phone 1, Phone 2, Email, Address, City, State, Zip, Current Needs, Building Type, Num Locations, Sqr Footage, Own/Rent, IP Address, Campaign</small>
	</fieldset></div>
		
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Security Lead in CSV format:</b></legend>
	<textarea name=\"sec_lead\" rows=\"15\" cols=\"60\"></textarea>
	</fieldset>
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_submit\" type=\"submit\" value=\"Add Security Lead\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";

}

echo $body;

?>