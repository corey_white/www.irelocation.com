<?php
/* 
************************FILE INFORMATION**********************
* File Name:  index.php
**********************************************************************
* Description:  The main home page for the new set of admin scripts that are being created.
**********************************************************************
* Creation Date:  3/20/08
**********************************************************************
* Modifications (Date/Who/Details):
		3/21/08  - Rob - Added sessions and security stuff
**********************************************************************
*/

session_start();	
include_once $_SERVER['DOCUMENT_ROOT']."/nuadmin/inc/security.php";

//-- COMPANY LOOKUP ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Company Lookup Admin</b></legend>
	<p class=\"\"><a href=\"company_lookup.php\">Company Lookup (not fully functional)</a></p>
	</fieldset>
	</div>
	</div>
";


//-- MOVING ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
		<div style=\"position: static;\">
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend><b>Moving Admin</b></legend>

			<p class=\"\"><a href=\"add_moving_zip_codes.php\">Add Zip Codes to the Marble.Moving_Zip_Codes Table</a></p>

			<p class=\"\"><a href=\"modify_moving_campaign.php\">View/Edit a Moving Campaign</a></p>

			</fieldset>
			</div>
	
		</div>

	</div>
";
		
//-- CPC ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
		<div style=\"position: static;\">
			
			<div style=\"clear: both;\">
			<fieldset>
			<legend><b>CPC Admin</b></legend>

			<p class=\"\"><a href=\"add_cpc.php\">Add CPC Data</a></p>
			<p class=\"\"><a href=\"view_cpc.php\">View CPC Data</a></p>
			<p class=\"\"><a href=\"cpc_import.php\">Import Bulk CPC Data</a></p>

			</fieldset>
			</div>
		
	
		</div>

	</div>
";
		
		
//-- AUTO & ATUS ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>AUTO & ATUS Admin</b></legend>
	<p class=\"\"><a href=\"modify_auto_atus_campaign.php\">Modify AUTO/ATUS Campaign</a></p>
	<p class=\"\"><a href=\"rerun_campaignmapping.php\">Rerun CampaignMapping</a></p>
	</fieldset>
	</div>

	</div>
";
	
	
	
//-- MISC ADMIN LINKS
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Miscellany</b></legend>
	<p class=\"\"><a href=\"http://topmoving.com/radius.php\" target=\"_blank\">Zip Code Radius</a></p>
	<p class=\"\"><a href=\"modify_cost_per_click.php\">View/Edit Cost Per Lead </a></p>
	<p class=\"\"><a href=\"bad_emails_bettercheck.php\">Deep Email Check </a></p>
	</fieldset>
	</div>

	</div>
";
	

//-- XXX ADMIN LINKS
/*
$body .= "
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>XXX Admin</b></legend>
	<p class=\"\">XXX</p>
	</fieldset>
	</div>

	</div>
";
*/
	

echo $body;
?>