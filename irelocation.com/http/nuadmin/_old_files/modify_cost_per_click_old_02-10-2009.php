<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_cost_per_click.php
**********************************************************************
* Description:  Updates, View, Add the Cost Per Lead to the marble.cost_per_lead table
**********************************************************************
* Creation Date:  3/28/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

$TABLE = "marble.cost_per_lead";
$DATATYPE = "Cost Per Lead";
// Using $PHP_SELF for form actions

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":
		
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"$PHP_SELF\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID*</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						<br />Auto - 1
						<br />Moving (home) - 2
						<br />Intl Moving - 3
						<br />Security - 4
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Site ID*</b></legend>
						<input name=\"site_id\" type=\"text\" value=\"$site_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Cost Per Lead</b></legend>
						<input name=\"cost\" type=\"text\" value=\"$cost\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Extra</b></legend>
						<input name=\"extra\" type=\"text\" value=\"$extra\" size=\"10\">
						<br />(things like res or com)
						</fieldset>
						</div>";
					

					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update $DATATYPE</legend>
					<input name=\"submit\" type=\"submit\" value=\"Add\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_action\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;


    case "insert_action": 
    
		$sql = "insert into $TABLE set lead_id = '$lead_id', cat_id = '$cat_id', site_id = '$site_id', cost = '$cost', extra = '$extra'  ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>$DATATYPE has been inserted for: $lead_id</p>";
			
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

		break;


    case "get_record": //-- get the lead_format record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			if ( $siteID ) {
				$sql = "select * from $TABLE where lead_id = '$lead_id' and site_id = '$siteID' and extra = '$extra' ";
			} else {
				$sql = "select * from $TABLE where lead_id = '$lead_id' ";
			}
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No $DATATYPE Record found for Lead ID: $lead_id</p>";
				
				$body .= "<p><a href=\"$PHP_SELF\">Try Again</a></p>";
				
				$body .= "<p><a href=\"$PHP_SELF?action=add&lead_id=$lead_id\">Or Add a new $DATATYPE Record</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id</p>";
				$body .= "<p>Please choose which one you want to view/edit:</p>";
				
				$body .= "<p>";
				
				while ($row_rs = $rs->fetch_array()) {
					$siteID = $rs->myarray["site_id"];
					$extra = $rs->myarray["extra"];
					
					$body .= "<a href=\"$PHP_SELF?lead_id=$lead_id&siteID=$siteID&extra=$extra&action=get_record\">$siteID - $extra</a><br>";
				}
				
				$body .= "</p>";
				
				$body .= "<p><a href=\"$PHP_SELF?action=add&lead_id=$lead_id\">Add new CPL for $lead_id</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$id = $rs->myarray["id"];
				$cat_id = $rs->myarray["cat_id"];
				$lead_id = $rs->myarray["lead_id"];
				$site_id = $rs->myarray["site_id"];
				$cost = $rs->myarray["cost"];
				$extra = $rs->myarray["extra"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"$PHP_SELF\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Update $DATATYPE for $lead_id</b></legend>";


						$body .= "<div>
						<fieldset>
						<legend><b>Lead ID*</b></legend>
						<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID*</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						<br />Auto - 1
						<br />Moving (home) - 2
						<br />Intl Moving - 3
						<br />Security - 4
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Site ID*</b></legend>
						<input name=\"site_id\" type=\"text\" value=\"$site_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Cost Per Lead</b></legend>
						<input name=\"cost\" type=\"text\" value=\"$cost\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Extra</b></legend>
						<input name=\"extra\" type=\"text\" value=\"$extra\" size=\"10\">
						</fieldset>
						</div>";
					
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Update $DATATYPE</legend>
						<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Notes</legend>
						<p></p>
						</fieldset>
						</div>";
						
				$body .= "</fieldset></div>";
						
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_action\">\n";
					$body .= "<input name=\"id\" type=\"hidden\" value=\"$id\">\n";
					
					$body .= "</form>";
				
				$body .= "<p><a href=\"$PHP_SELF?action=add&lead_id=$lead_id\">Add new CPL for $lead_id</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;


    case "update_action": //-- update the record
        
		$sql = "update $TABLE set lead_id = '$lead_id', cat_id = '$cat_id', site_id = '$site_id', cost = '$cost', extra = '$extra' where id = '$id' ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>$DATATYPE has been updated for: $lead_id</p>";
			
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		
		$body .= "<p align=\"center\">View/Modify Record in the <strong>$TABLE</strong> Table.</p>";
		
		$body .= "<form name=\"disp1\" action=\"\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup $DATATYPE</legend>
			<input name=\"submit\" type=\"submit\" value=\"Get $DATATYPE\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>