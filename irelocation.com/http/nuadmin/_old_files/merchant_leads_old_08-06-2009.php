<?
/* 
************************FILE INFORMATION**********************
* File Name:  merchant_leads.php
**********************************************************************
* Description:  Modifies a record in movingdirectory.rules
**********************************************************************
* Creation Date:  8/5/09
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************


*/

include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$row_clr = "#FFFFFF";
$row_clr2 = "#EEEEEE";
$thead_clr = "#B0CACE";

function color_mix() {
  global $color_cnt, $row_clr, $row_clr2;
  if(!$color_cnt) $color_cnt = 0;
  if($color_cnt % 2 == 0) $mix_bg = $row_clr2;
  else $mix_bg = $row_clr;
  $color_cnt++;
  return $mix_bg;
}


$current_date = date('m/d/Y',strtotime("now"));

$body .= "<style type=\"text/css\">
p,td { color: black; font-family: helvetica,arial,sans-serif; font-size: 8pt; font-style: normal; font-variant: normal; font-weight: normal;  }
th { color: white; font-family: helvetica,arial,sans-serif; font-size: 9pt; font-style: normal; font-variant: normal; font-weight: bold;  }

</style>";

$result_comment = htmlentities($result_comment,ENT_QUOTES); 


if ( $result ) { //-- update record with inputted results

	switch ( $result_type ) {
		case "good":
			$sql = "update movingdirectory.leads_merchant set ready_to_send = 1, result_comment = '$result_comment' where quote_id = '$qid' ";
			$body .= "<h3 align=\"center\">$qid is marked as good</h3>";
			break;
	
		case "bad":
			$sql = "update movingdirectory.leads_merchant set ready_to_send = 8, result_comment = '$result_comment' where quote_id = '$qid' ";
			$body .= "<h3 align=\"center\">$qid is marked as bad</h3>";
			break;
	
		case "pending":
			$sql = "update movingdirectory.leads_merchant set ready_to_send = 9, result_comment = '$result_comment' where quote_id = '$qid' ";
			$body .= "<h3 align=\"center\">Updated Comments for $qid, kept as pending</h3>";
			break;
	
		default:
			echo "ERROR - ERROR - ERROR - Halting Process<br />";
			echo "Make sure you selected a status for the lead.<br />";
			exit;
			break;
	}
	$rs = new mysql_recordset($sql);
	
} 



//-- get merchant leads that haven't been checked yet (ready_to_send = 9)

$sql = "select * from movingdirectory.leads_merchant where ready_to_send = 9 ";
$rs = new mysql_recordset($sql);

$body .= "<table cellspacing=\'2\" cellpadding=\"3\" width=\"1025\"  style=\"border-color: black; 	border-width: 1px; 	border-style: solid; border-collapse: collapse;\" align=\"center\">";
$body .= "<tr bgcolor=\"#527DBD\">";
$body .= "<th width=\"50\" align=\"left\">Quote ID</td>";
$body .= "<th width=\"100\" align=\"left\">Date</td>";
$body .= "<th width=\"125\" align=\"left\">Name/Company/Phone/Email</td>";
$body .= "<th width=\"150\" align=\"left\">Lead Details</td>";
$body .= "<th width=\"250\" align=\"left\">Comments</td>";
$body .= "<th width=\"250\" align=\"left\">&nbsp;</td>";
$body .= "</tr>";

$L_count = 0;
while ($rs->fetch_array()) {
	$L_count++;
	$body .= "<tr bgcolor=\"".color_mix()."\">";
	
	$phone = substr($rs->myarray["phone"],0,3) . "-" . substr($rs->myarray["phone"],3,3) . "-" . substr($rs->myarray["phone"],6,4);
	
	$body .= "<td>" . $rs->myarray["quote_id"] . "</td>";
	$body .= "<td>" . $rs->myarray["dt"] . "</td>";
	$body .= "<td><big>" . $rs->myarray["first_name"] . " " . $rs->myarray["last_name"] . "</big><br /><big>" . $rs->myarray["company"] . "</big><br /><span style=\"font-weight: bold; font-size: 12pt; color: #005EED;\">$phone</span><br />" . $rs->myarray["email"] . "</td>";
	$body .= "<td>currently_accept_cards: " . $rs->myarray["currently_accept_cards"] . "<br />receive_cc_orders: " . $rs->myarray["receive_cc_orders"] . "<br />typical_value: " . $rs->myarray["typical_value"] . "<br />card_volume: " . $rs->myarray["card_volume"] . "<br />time_frame: " . $rs->myarray["time_frame"] . "</td>";

	$body .= "<td>" . $rs->myarray["comment"] . "</td>";

	$body .= "<td>
<form action=\"merchant_leads.php\" method=\"get\">
<input name=\"result_type\" type=\"radio\" value=\"good\">Good &nbsp; <input name=\"result_type\" type=\"radio\" value=\"bad\">Bad &nbsp; <input name=\"result_type\" type=\"radio\" value=\"pending\" checked>Keep Pending<br />
<textarea name=\"result_comment\" rows=\"10\" cols=\"35\">" . $rs->myarray["result_comment"] . "</textarea><br />
<input name=\"qid\" type=\"hidden\" value=\"" . $rs->myarray["quote_id"] . "\">
<input name=\"result\" type=\"submit\" value=\"Process\">
</form></td>";

	$body .= "</tr>";
}

if ( $L_count == 0 ) {
    $body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"6\" align=\"center\"><p><big><b>Awesome Job!  Here's a cow kiss:</b></big></p><img src=\"cow_kiss.jpg\" border=\"0\"></td></tr>";
 #   $body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"6\" align=\"right\"><p>You've done a great job, but there are no more.  So, here's a bunny with a waffle on its head.</p><img src=\"waffle-bunny.jpg\" border=\"0\"></td></tr>";
#    $body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"6\" align=\"right\"><img src=\"bananahuge.gif\" alt=\"\" width=\"182\" height=\"180\" border=\"0\"></td></tr>";
} 

$body .= "<tr bgcolor=\"".color_mix()."\"><td colspan=\"6\"><a href=\"merchant_leads.php\">Reset</a></td></tr>";

$body .= "</table>";

echo $body;

?>