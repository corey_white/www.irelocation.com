<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_auto_atus_campaign.php
**********************************************************************
* Description:  Modifies an auto/atus campaign 
**********************************************************************
* Creation Date:  3/25/08
**********************************************************************
* Modifications (Date/Who/Details):
	4/1/08 09:23 AM - Rob - Added function to Add a campaign
**********************************************************************
*/



include_once "../inc_mysql.php";

$TABLE = "marble.campaign";
$DATATYPE = "Auto/Atus Campaign";
// Using $PHP_SELF for form actions

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":
		
				$ts = trim($ts);
				$trunc_month = substr($ts,0,6);

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"$PHP_SELF\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Campaign Month</b></legend>
					$trunc_month
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Company ID*</b></legend>
					<input name=\"comp_id\" type=\"text\" value=\"$comp_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Monthly Goal*</b></legend>
					<input name=\"monthly_goal\" type=\"text\" value=\"$monthly_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Leads Per Day*</b></legend>
					<input name=\"leads_per_day\" type=\"text\" value=\"$leads_per_day\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Temp Goal</b></legend>
					<input name=\"temp_goal\" type=\"text\" value=\"$temp_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Site ID*</b></legend>
					<select name=\"site_id\">
					<option value=\"none\">Select</option>
					<option value=\"auto\">AUTO</option>
					<option value=\"atus\">ATUS</option>
					</select>
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Active*</b></legend>
					<select name=\"active\">
					<option value=\"1\">Active</option>
					<option value=\"0\">Not Active</option>
					</select>
					</fieldset>
					</div>";
					

					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update $DATATYPE</legend>
					<input name=\"submit\" type=\"submit\" value=\"Add\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_action\">\n";
					$body .= "<input name=\"trunc_month\" type=\"hidden\" value=\"$trunc_month\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;


    case "insert_action": 

		// validate fields
		$error_msg = "";
		
		If (!$trunc_month ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Month (if you get this error, something is really wrong)</li>";
		}
		
		If (!$comp_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Company ID</li>";
		}
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Lead ID</li>";
		}
		
		If (!$monthly_goal ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Monthly Goal</li>";
		}
		
		If (!$leads_per_day ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Leads Per Day</li>";
		}
		
		If ($site_id == "none" ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Site ID</li>";
		}
		
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
		  
		} else { // process form data 
			
			$sql = "insert into $TABLE set comp_id = '$comp_id', lead_id = '$lead_id', month = '$trunc_month', monthly_goal = '$monthly_goal', leads_per_day = '$leads_per_day', site_id = '$site_id', active = '$active', temp_goal = '$temp_goal' ";
			echo $sql;
			
			if ( $rs=new mysql_recordset($sql) ) {
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p>$DATATYPE has been inserted for: $lead_id</p>";
				
				$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
				$body .= "</div>";
				$body .= "</div>";
				
			} else {
				$body .= "Error inserting into the database.  See programmer.";
			}
		}

		break;


    case "get_record": //-- get the campaign record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		$ts = trim($ts);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		If (!$ts ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing DATE</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			//-- convert data to proper format
			$ts = dateYearFirst($ts);
			$trunc_month = substr($ts,0,6);
			
			if ( $siteID ) {
				$sql = "select cid, comp_id, monthly_goal, leads_per_day, site_id, active, temp_goal from $TABLE where site_id != 'test' and lead_id = '$lead_id' and month = '$trunc_month' and site_id = '$siteID' ";
			} else {
				$sql = "select cid, comp_id, monthly_goal, leads_per_day, site_id, active, temp_goal from $TABLE where site_id != 'test' and lead_id = '$lead_id' and month = '$trunc_month' and site_id != 'test' ";
			}
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id for Month: $trunc_month</p>";
				$body .= "<p><a href=\"$PHP_SELF\">Try Again</a></p>";
				$body .= "<p><a href=\"$PHP_SELF?action=add&lead_id=$lead_id&ts=$ts\">Or create a new campaign</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // found more than 1, which is okay for AUTO/ATUS cuz there can both
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id for Month: $trunc_month</p>";
				$body .= "<p>Please choose which one you want to view/edit:</p>";
				
				$body .= "<p>";
				
				while ($row_rs = $rs->fetch_array()) {
					$siteID = $rs->myarray["site_id"];
					
					$body .= "<a href=\"$PHP_SELF?lead_id=$lead_id&ts=$ts&siteID=$siteID&action=get_record\">$siteID</a><br>";
				}
				
				$body .= "</p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$cid = $rs->myarray["cid"];
				$comp_id = $rs->myarray["comp_id"];
				$monthly_goal = $rs->myarray["monthly_goal"];
				$leads_per_day = $rs->myarray["leads_per_day"];
				$site_id = $rs->myarray["site_id"];
				$active = $rs->myarray["active"];
				$temp_goal = $rs->myarray["temp_goal"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"$PHP_SELF\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Campaign Month</b></legend>
					$trunc_month
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID</b></legend>
					$lead_id
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Comapny ID</b></legend>
					$comp_id
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Monthly Goal*</b></legend>
					<input name=\"monthly_goal\" type=\"text\" value=\"$monthly_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Leads Per Day*</b></legend>
					<input name=\"leads_per_day\" type=\"text\" value=\"$leads_per_day\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Temp Goal</b></legend>
					<input name=\"temp_goal\" type=\"text\" value=\"$temp_goal\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Site ID*</b></legend>
					<select name=\"site_id\">
					<option value=\"auto\"";
					if ( $siteID == "auto" ) {
						$body .= " selected";
					} 
					$body .= ">AUTO</option>
					<option value=\"atus\"";
					if ( $siteID == "atus" ) {
						$body .= " selected";
					} 
					$body .= ">ATUS</option>
					</select>
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Active*</b></legend>
					<select name=\"active\">
					<option value=\"1\"";
					if ( $active == "1" ) {
						$body .= " selected";
					} 
					$body .= ">Active</option>
					<option value=\"0\"";
					if ( $active == "0" ) {
						$body .= " selected";
					} 
					$body .= ">Not Active</option>
					</select>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update Campaign</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>To temporarily change the number of leads for a campaign, you need to change the Monthly Goal field.  This is a fuzzy number which is calculated by the number of days in the month, multiplied by the temp number of leads per day - rounded to the nearest 500 number (1000, 1500, 2000, 2500, etc.), and then tested to see if the number of records in the marble.lead_destiniations table is approximately equal to the number of leads you want them to have for the remaining days of the month.  This may need to be adjusted at the beginning of the month. </p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_campaign\">\n";
					$body .= "<input name=\"lead_id\" type=\"hidden\" value=\"$lead_id\">\n";
					$body .= "<input name=\"trunc_month\" type=\"hidden\" value=\"$trunc_month\">\n";
					$body .= "<input name=\"cid\" type=\"hidden\" value=\"$cid\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;

    case "update_campaign": //-- update the record
        
		$sql = "update $TABLE set monthly_goal = '$monthly_goal', leads_per_day = '$leads_per_day', site_id = '$site_id', active = '$active', temp_goal = '$temp_goal' where cid = '$cid' ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>TOPAUTO Campaign has been updated for: $lead_id for Month: $trunc_month</p>";
			$body .= "<p><a href=\"rerun_campaignmapping.php\">Don't forget you need to run the Campaign Mapping</a></p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;

    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">Modify Record in the <strong>$TABLE</strong> Table.</p>";
		
		$body .= "<form name=\"mod_camp\" action=\"$PHP_SELF\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Date</b> (choose any date in the month to choose that month)</legend>
				<input type=\"text\" name=\"ts\" value=\"$current_date\" readonly><a href=\"javascript:show_calendar4('document.mod_camp.ts', document.mod_camp.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
				</fieldset>
				</div>
			
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup Campaign</legend>
			<input name=\"mod_camp\" type=\"submit\" value=\"Get Campaign\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>