<?
/* 
************************FILE INFORMATION**********************
* File Name:  eom_marble_copy.php
**********************************************************************
* Description:  This copies the campaigns from the current month to the next month.  To be ran at End of Month (EOM)

	This script copies all campaign entries for Atus from this month
	to next month that haven't been cancelled. all companies by default
	are set to active, and the temp_goal field is cleared out.	

**********************************************************************
* Creation Date:  2/29/08
**********************************************************************
* Modifications (Date/Who/Details):
	4/30/08 16:45 PM - Rob - Finished this script so that it's working
**********************************************************************
NOTES:
Using the following from Dave as a base:

set @this_month = '200712';
set @next_month = '200801';

insert into campaign
(lead_id,comp_id,monthly_goal,leads_per_day,
site_id,sort_order,needed,active,month,temp_goal)
select 
lead_id,comp_id,monthly_goal,leads_per_day, 
site_id,sort_order,monthly_goal as 'needed',1 as 'active',
'NEXT_MONTH' as 'month',0 as 'temp_goal'
from campaign where month = 'THIS_MONTH' and active = 1 and cancelled is null;


*/


if ( $goAheadAndRunIt == "yes_run_it" ) {
	
	include_once "../inc_mysql.php";
	
	switch ( $dbtb ) {
		case "marble":
			$sql = "insert into $dbtb.campaign (lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,needed,active,month,temp_goal) select  lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,monthly_goal as 'needed',1 as 'active', '".date("Ym",strtotime("+30 days"))."' as 'month',0 as 'temp_goal' from $dbtb.campaign where month = '".date("Ym")."' and active = 1 ";
			break;
	
		case "movingdirectory":
			$sql = "insert into $dbtb.campaign (lead_id, comp_id, monthly_goal, leads_per_day, site_id, sort_order, needed, active, month, temp_goal, min_bedrooms, pull_order) select  lead_id,comp_id,monthly_goal,leads_per_day, site_id,sort_order,monthly_goal as 'needed',1 as 'active', '".date("Ym",strtotime("+30 days"))."' as 'month',0 as 'temp_goal', min_bedrooms, pull_order from $dbtb.campaign where month = '".date("Ym")."' and active = 1 ";
			break;
	
	
		default:
			echo "Error, no table selected<br />";
			exit;
			break;
	}
	
	$rs = new mysql_recordset($sql);

	echo "Ran the following SQL:<br />";
	echo "$sql<br />";

} else {
    echo "<p class=\"\" align=\"center\">To run the End of Month Affiliate copy for MARBLE, click <a href=\"eom_marble_copy.php?goAheadAndRunIt=yes_run_it&dbtb=marble\">here</a>.</p>";

	echo "<br />";
    echo "<p class=\"\" align=\"center\">To run the End of Month Affiliate copy for MOVINGDIRECTORY, click <a href=\"eom_marble_copy.php?goAheadAndRunIt=yes_run_it&dbtb=movingdirectory\">here</a>.</p>";
}

?>