<?
/* 
************************FILE INFORMATION**********************
* File Name:  eom_affiliate_copy.php
**********************************************************************
* Description:  This runs the Affiliate copy to set up for the next month. Runs at EOM (End of Month)

	Copy all the entries in the movingdirectory.leadbased_affiliates table the same way adjusting the month field to the new month.
	
**********************************************************************
* Creation Date:  2/29/08
**********************************************************************
* Modifications (Date/Who/Details):
	4/30/08 16:45 PM - Rob - Finished this script so that it's working
**********************************************************************
NOTES:
Using this SQL call from Dave as a base to work from:

set @this_month = '200712';
set @next_month = '200801';

insert into movingdirectory.leadbased_affiliates
(comp_name, ppl, lead_type, month, source,localppl)
select comp_name, ppl, lead_type, @next_month, source,localppl 
from movingdirectory.leadbased_affiliates where
 month = @this_month;


*/


if ( $goAheadAndRunIt == "yes_run_it" ) {
	
	include_once "../inc_mysql.php";
	
	$sql = "insert into movingdirectory.leadbased_affiliates (comp_name, ppl, lead_type, month, source,localppl) select comp_name, ppl, lead_type,  '".date("Ym",strtotime("+30 days"))."', source,localppl from movingdirectory.leadbased_affiliates where month = '".date("Ym")."' ";
	
	$rs = new mysql_recordset($sql);

	echo "Ran the following SQL:<br />";
	echo "$sql<br />";

} else {
    echo "<p class=\"\" align=\"center\">To run the End of Month Affiliate copy, click <a href=\"eom_affiliate_copy.php?goAheadAndRunIt=yes_run_it\">here</a>.</p>";
}

?>