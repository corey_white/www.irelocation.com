<?
/* 
************************FILE INFORMATION**********************
* File Name:  cpc_import.php
**********************************************************************
* Description:  This will import the CP data from pasted text
**********************************************************************
* Creation Date:  3/31/08
**********************************************************************
* Modifications (Date/Who/Details):
	4/1/08 16:03 PM - Rob - finishing up initial code
	9/24/08 - Rob Updated the Yahoo code so that it removes double-tabs, which was screwing up the import (thanks Yahoo)
**********************************************************************
NOTES: This is the first phase, where we are simply taking text that has been pasted into a textarea and parsing it out for the CPC data, and inserting it into the DB.

Second phase will be making the script get the data automatically from the email server.
*/

include_once "../inc_mysql.php";

$debug_flag = "off"; //-- on or off

// this function is used to print debug statements if the flag is turned on; debug_flag is on the individual page
function debug($foo){ 
  global $debug_flag;
  if($debug_flag == "on"){
    echo "$foo<br>\n";
  }
}

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}
// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}


$yesterday = date('m/d/Y',strtotime("yesterday"));
$lastdate = dateYearLast($lastdate);

//-- create site_id array
//-- ######>>>  ADD ADDITIONAL SITE_IDS HERE
$site_id_arr = array(
"123carmoving.com" => "123cm",
"australia.topalarmcompanies.com" => "atac",
"australia.topalarmcompanies.com - content" => "atac",
"australia.topalarmcompanies" => "atac",
"auto-transport.us" => "atus",
"autoshipping.com" => "asc",
"autoshipping.com - all" => "asc",
"autoshipping.com - content" => "asc",
"callautoshippers-b" => "c2c",
"callautoshippers-c-b" => "c2c",
"callautoshippers-c" => "c2c",
"callautoshippers" => "c2c",
"canada.topalarmcompanies.com" => "ctac",
"canada.topalarmcompanies.com - content" => "ctac",
"canada.topsecuritycompanies" => "ctsc",
"carshipping.com" => "cs",
"carshipping.com - all" => "cs",
"carshippingquote" => "cq",
"carshippingquote-b" => "cq",
"carshippingquote-c" => "cq",
"duct cleaning" => "duct",
"duct cleaning-b" => "duct",
"duct cleaning - b" => "duct",
"duct cleaning - e" => "duct",
"movemycar.com" => "mmc",
"movemycar.com - all" => "mmc",
"movemycar.com - content" => "mmc",
"movingease.com" => "me",
"movingease.com - all" => "me",
"movingease.com - content" => "me",
"promoving.com" => "pm",
"promoving.com - all" => "pm",
"promoving.com - content" => "pm",
"promoving.com - c" => "pm",
"promoving geo" => "pm",
"promoving-ca,fl,tx" => "pm",
"promoving-content" => "pm",
"quote-it-merchant-b" => "merch",
"quote-it-merchant-p" => "merch",
"quote-it-merchant-e" => "merch",
"quote-it-merchant-c" => "merch",
"quote-it-callcenter-b" => "callctr",
"quote-it-callcenter-p" => "callctr",
"quote-it-callcenter-e" => "callctr",
"quote-it-callcenter" => "callctr",
"quote-it-merchant" => "merch",
"quote-it-transcription" => "transcribe",
"quote-it-merchant-c-banners" => "merch",
"quote-it-merchant-placement-text" => "merch",
"topalarmcompanies.com" => "tac",
"topalarmcompanies.com - content" => "tac",
"topalarmcompanies.com - all" => "tac",
"topalarmcompanies - hvs" => "tac",
"topalarm-pl" => "tac",
"topmoving.com" => "tm",
"topmoving.com_all" => "tm",
"topmoving.com_content" => "tm",
"topmoving.com - all" => "tm",
"top security companies" => "tsc",
"top security companies - all" => "tsc",
"topsecuritycompanies.com" => "tsc",
"usalarmcompanies" => "usalarm",
"us auto transport" => "usat",
"us auto transport - content" => "usat",
"usautotransport" => "usat",
"usalarmcompanies" => "usalarm",
"usalarmcompanies - content" => "usalarm",
"usalarmcompanies-e" => "usalarm",
"usalarmcompanies-b" => "usalarm",
"usalarmcompanies-p" => "usalarm",
"usalarmcompanies - state" => "usalarm",
"usalarmcompanies - c- state" => "usalarm",
"usalarmcompanies - city" => "usalarm",
"usalarmcompanies - placement - text" => "usalarm",
"uk.topalarmcompanies-b" => "utac",
"uk.topalarmcompanies-e" => "utac",
"uk.topalarmcompanies-c" => "utac"
);


echo "\n\n<!-- <pre>";
print_r($site_id_arr);
echo "</pre> -->\n\n";

$value_count = 0;

switch ( $action ) {

    case "import": //-- update the record
        
        //-- check entire content for MSN
		if ( eregi("MSN_Campaign_performance", $cpc_text) ) {
			$import_type = "msn";
			
		} elseif ( eregi("Ysm Performance By Date", $cpc_text) || eregi("Ysm Campaign Performance", $cpc_text) || eregi("Yahoo", $cpc_text) ) { //-- check entire content for Yahoo
			$import_type = "overture";
			
		} else {
			$import_type = "google";
			
		}
		
		debug("IMPORT TYPE IS $import_type");
		
		//-- split all lines
		$lines_arr = split("\r",$cpc_text);
		
		//-- convert date
		$date = dateYearFirst($ts);

		//-- Process based on Import Type
		//-- Process based on Import Type
		//-- Process based on Import Type
		switch ( $import_type ) {
		
			case "google": //-- GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE GOOGLE 
				
				debug("HIT GOOGLE");
	
				foreach($lines_arr as $key => $value){
					debug("Key: $key - Value: $value");
					
					//-- split each value by tab
					$val_arr = split("\t",$value);
					
					debug("val_arr[0] = $val_arr[0]");
					debug("val_arr[1] = $val_arr[1]");
					debug("val_arr[2] = $val_arr[2]");
					
					$low_val = strtolower(trim($val_arr[0]));
					debug("low_val = $low_val");
					
					//-- check $val_arr[0] to see if it is in the site_id array, and if so, then set up for import into db
					if ( array_key_exists($low_val,$site_id_arr) ) { //-- value is in array
						
						$site_id = $site_id_arr[$low_val];
						$clicks = trim($val_arr[1]);
						$cost = trim($val_arr[2]);
		
						debug("ID = $site_id");
						debug("Clicks = $clicks");
						debug("Cost = $cost");
		
						//-- strip $ and , (comma) from cost
						$cost = str_replace("$","",$cost);
						$cost = str_replace(",","",$cost);
						
						$values .= "('$import_type','$site_id','$cost','$clicks','$date'), ";
						$value_count++;
					} 
						
				}
				break;


			case "overture": //-- YAHOO-OVERTURE YAHOO-OVERTURE YAHOO-OVERTURE YAHOO-OVERTURE YAHOO-OVERTURE 
				
				debug("HIT YAHOO-OVERTURE");
				
				foreach($lines_arr as $key => $value){
					debug("Key: $key - Value: $value");
					
					//-- split each value by tab
					$val_arr = ereg_replace("\t\t","\t",$value);
					$val_arr = split("\t",$val_arr);
					
					debug("val_arr[0] = $val_arr[0]");
					debug("val_arr[1] = $val_arr[1]");
					debug("val_arr[2] = $val_arr[2]");
					
					$low_val = strtolower(trim($val_arr[0]));
					debug("low_val = $low_val");
					
					//-- check $val_arr[0] to see if it is in the site_id array, and if so, then set up for import into db
					if ( array_key_exists($low_val,$site_id_arr) ) { //-- value is in array
						
						$site_id = $site_id_arr[$low_val];
						$clicks = trim($val_arr[1]);
						$cost = trim($val_arr[2]);
						
						debug("Site ID = $site_id");
						debug("Clicks = $clicks");
						debug("Cost = $cost");
		
						//-- strip $ and , (comma) from cost
						$cost = str_replace("$","",$cost);
						$cost = str_replace(",","",$cost);
						
						$values .= "('$import_type','$site_id','$cost','$clicks','$date'), ";
						$value_count++;
					} 

				}
				break;


			case "msn": //-- MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN MSN 
				
				debug("HIT MSN");
				
				foreach($lines_arr as $key => $value){
					debug("tKey: $key - Value: $value");
					
					//-- split each value by tab
					$val_arr = split("\t",$value);
					
					debug("val_arr[0] = $val_arr[0]"); //-- Account Name
					debug("val_arr[1] = $val_arr[1]"); //-- Date
					debug("val_arr[2] = $val_arr[2]"); //-- Campaign
					debug("val_arr[3] = $val_arr[3]"); //-- Imp
					debug("val_arr[4] = $val_arr[4]"); //-- Clicks
					debug("val_arr[5] = $val_arr[5]"); //-- CTR
					debug("val_arr[6] = $val_arr[6]"); //-- Avg CPC
					debug("val_arr[7] = $val_arr[7]"); //-- Total Cost
					debug("val_arr[8] = $val_arr[8]"); //-- Avg Pos
					
					$low_val = strtolower(trim($val_arr[2]));
					debug("low_val = $low_val");
					
					
					//-- check $val_arr[0] to see if it is in the site_id array, and if so, then set up for import into db
					if ( array_key_exists($low_val,$site_id_arr) ) { //-- value is in array
						
						$site_id = $site_id_arr[$low_val];
						$clicks = trim($val_arr[4]);
						$cost = trim($val_arr[7]);
								
						debug("Site ID = $site_id");
						debug("Clicks = $clicks");
						debug("Cost = $cost");
		
						//-- strip $ and , (comma) from cost
						$cost = str_replace("$","",$cost);
						$cost = str_replace(",","",$cost);
						
						$values .= "('$import_type','$site_id','$cost','$clicks','$date'), ";
						$value_count++;
					} 
						
				}
				break;


			default:
				echo "ERROR: no discernable Import Type (MSN, Overture, Google, Etc.)<br />";
				break;
		}
        
		debug("<br />VALUE STRING IS:: $values");
			
			$values = trim($values,", ");
			$insert = "insert into movingdirectory.costperclick (search_engine,site,cost,clicks,ts) values $values";
		
		debug("<br>$insert<br>");
		
		if ( $rs=new mysql_recordset($insert) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>CPC Data has been added for $date</p>";
			$body .= "<p>Here is what was entered:</p>";
			
			$output = split(", ",$values);
			$body .= "<blockquote>";
			foreach($output as $key => $value){
				$body .= "$value <br />";
			}
			$body .= "</blockquote>";
			
			$body .= "<p>Count = $value_count</p>";
			
			$body .= "<p><a href=\"cpc_import.php?lastdate=$date\">Import another Bulk set with Same Date</a></p><p><a href=\"cpc_import.php\">Import another Bulk set with New Date</a></p><p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break; //-- this break is for the outside swtich


    default: //-- display the initial form
		if ( $lastdate ) {
			$next_date = $lastdate;
		} else {
			$next_date = $yesterday;
		}
		
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		
		$body .= "<p align=\"center\">View/Modify Record in the <strong>$TABLE</strong> Table.</p>";
		
		$body .= "<form name=\"disp1\" action=\"cpc_import.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>CPC Date</b></legend>
				<input type=\"text\" name=\"ts\" value=\"$next_date\" readonly><a href=\"javascript:show_calendar4('document.disp1.ts', document.disp1.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
				</fieldset>
				</div>
				
<!-- 
				<div>
				<fieldset>
				<legend><b>Search Engine</b></legend>
				<select name=\"search_engine\">
				<option value=\"google\">Google</option>
				<option value=\"overture\">Yahoo / Overture</option>
				<option value=\"msn\">MSN</option>
				</select>
				</fieldset>
				</div>
 -->
				
				<div>
				<fieldset>
				<legend><b>Paste CPC Text Here</b></legend>
				<textarea name=\"cpc_text\" rows=\"20\" cols=\"40\"></textarea>
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Submit CPC Data</legend>
			<input name=\"submit\" type=\"submit\" value=\"Import\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"import\">
		</form>";
        
        break;
}


echo $body;

?>