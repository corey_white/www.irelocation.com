<?php 

/*
This script posts some data to Boruch's site and gets a estimated price in return, also in XML
*/

function value_in($element_name, $xml, $content_only = true) {
    if ($xml == false) {
        return false;
    }
    $found = preg_match('#<'.$element_name.'(?:\s+[^>]+)?>(.*?)'.
            '</'.$element_name.'>#s', $xml, $matches);
    if ($found != false) {
        if ($content_only) {
            return $matches[1];  //ignore the enclosing tags
        } else {
            return $matches[0];  //return the full pattern match
        }
    }
    // No match found: return false.
    return false;
}


/*
$FromZip = urlencode($_GET['ZZZZ']);
$ToZip = urlencode($_GET['ZZZZ']);
$Make = urlencode($_GET['ZZZZ']);
$Model = urlencode($_GET['ZZZZ']);
$IsVehicleRunning = urlencode($_GET['ZZZZ']);
$ShippingDate = urlencode($_GET['ZZZZ']);
$Weight = urlencode($_GET['ZZZZ']);
*/

$FromZip = "62901";
$ToZip = "10001";
$Make = "Ford";
$Model = "Taurus";
$IsVehicleRunning = "True";
$ShippingDate = "5/25/2012";
$Weight = "5000";



//-- Build Auto Transport Body
$check_body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<AutoTransportPrice>
<FromZip>$FromZip</FromZip>
<ToZip>$ToZip</ToZip>
<Make>$Make</Make>
<Model>$Model</Model>
<IsVehicleRunning>$IsVehicleRunning</IsVehicleRunning>
<ShippingDate>$ShippingDate</ShippingDate>
</AutoTransportPrice>";

/*
//-- Build Moving Body
$check_body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<FullServicePrice>
<FromZip>$FromZip</FromZip>
<ToZip>$ToZip</ToZip>
<Weight>$Weight</Weight>
</FullServicePrice>";
*/


$url = "http://www.container-shipping.com/Services/AutoTransportPrice.aspx"; //-- Auto Transport
#$url = "http://www.container-shipping.com/Services/FullServicePrice.aspx"; //-- Moving

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded'));
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 0); //-- 0 to see error, 1 to capture into var
curl_setopt($ch, CURLOPT_POSTFIELDS, trim($check_body));


if( ! $result = curl_exec($ch)) { 
	trigger_error(curl_error($ch)); 
} 
curl_close($ch); 
#print_r($result); 


$price = value_in('Price', $result);


echo "Estimated price is \$$price.";


?>