<?
/* 
************************FILE INFORMATION**********************
* File Name:  rerun_campaignmapping.php
**********************************************************************
* Description:  Displays links for rerunning the AUTO/ATUS campaign mapping
**********************************************************************
* Creation Date:  3/25/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


$body .= "<p align=\"center\">Rerun Campain Mapping for AUTO/ATUS Campaigns.</p>";


$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">

	<div style=\"position: static;\">
	
		<div>
		<fieldset>
		<legend><b>Remap</b></legend>
		<p><a href=\"http://www.irelocation.com/marble/campaignmapping.php?campaign=all&delete=y\" target=\"_blank\">Delete and Rerun Campaign Mapping both AUTO and ATUS Campaigns</a></p>
		<p>(Must run campaign mapping for both as that's how the code was written)</p>
		<p><a href=\"index.php\">Back to Index</a></p>
		</fieldset>
		</div>		
	
	</div>

</div>
<input name=\"action\" type=\"hidden\" value=\"get_record\">
</form>";


echo $body;

?>