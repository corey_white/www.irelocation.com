<?php
/* 
************************FILE INFORMATION**********************
* File Name:  safeguard_security_zips.php
**********************************************************************
* Description:  this script will update the zip codes for security leads going to Safe-guard (1641)
**********************************************************************
* Creation Date:  9/8/08 
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: this script will check to see if they already have the zip code before adding it.

Post codes they want

- 1000 to 2263
- 2500 to 2530
- 2555 to 2574
- 2740 to 2786
*/

/*
echo "Code Stopped - see code.<br />";
exit;
*/


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$added_zip = "";
$denied_zip = "";
$added_count = 0;
$denied_count = 0;


echo "Working on the $ZIP_TABLE table...<br /><br />";

//-- first, we delete all of the lead_ids of 1641 
/*  -- NOT NEEDED
$sql = "delete from $ZIP_TABLE where lead_id = 1641 ";
$rs = new mysql_recordset($sql);
*/


//-- Need to insert all sequential zip codes from 3000 to 3341
for ( $i = 3000; $i <= 3341; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3425 to 3443
for ( $i = 3425; $i <= 3443; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3750 to 3811
for ( $i = 3750; $i <= 3811; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3910 to 3920
for ( $i = 3910; $i <= 3920; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3926 to 3944
for ( $i = 3926; $i <= 3944; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3972 to 3978
for ( $i = 3972; $i <= 3978; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 3980 to 3983
for ( $i = 3980; $i <= 3983; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}


//-- Need to insert all sequential zip codes from 8000 to 8999
for ( $i = 8000; $i <= 8999; $i++ ) {

    $sql = "select lead_id from $ZIP_TABLE where zip = '$i' and lead_id = 1641 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1641, zip = '$i' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$i, ";
		$added_count++;
	} else {
		$denied_zip .= "$i, ";
		$denied_count++;
	}
}




$added_zip = trim($added_zip,", ");
$denied_zip = trim($denied_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";
echo "<br />";
echo "$denied_count Zips that already existed: $denied_zip<br />";

?>