<?
/* 
************************FILE INFORMATION**********************
* File Name:  add_moving_zip_codes.php
**********************************************************************
* Description:  This page will take a string of inputed zip codes and inset them into the marble.moving_zip_codes table.
						NEEDED INPUT: lead_id, Zip codes, cat_id, local or long
**********************************************************************
* Creation Date:  3/19/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once "../inc_mysql.php";


if ( $add_submit == "Add to Table" ) { // if we have a submission, process
    
    //-- Validation
	$error_msg = "";
	
	If (!$lead_id ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing LEAD ID</li>";
	}
	If ($cat_id == "none" ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing CAT ID</li>";
	}
	If ($extra == "none" ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing LOCAL or LONG</li>";
	}
	If (!$zip_codes ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Zip Codes</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		$zip_codes = trim(ereg_replace(" ","",$zip_codes),",");
		
		//-- put zips into array
		$zip_arr = split(",",$zip_codes);
		
		#echo "lead_id = $lead_id<br />";
		#echo "cat_id = $cat_id<br />";
		#echo "extra = $extra<br />";
		
		$zip_count = 0;
		
		foreach($zip_arr as $key => $value){
			$zip_count++;
			#echo"Key: $key - Value: $value<br>";
			$sql_values .= "(NULL, '$lead_id', '$value', '$cat_id', '$extra'),";
		}
		
		$sql_values = trim($sql_values,",");
		#echo $sql_values;
		#echo "<br />";
		
		$sql = "insert into marble.moving_zip_codes (zid,lead_id,zip,cat_id,extra) values $sql_values ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<p align=\"center\">$zip_count zip codes have been inserted for $lead_id.</p>";
			$body .= "<p align=\"center\"><a href=\"add_moving_zip_codes.php\">Add more zip codes to Marble.Moving_Zip_Codes</a></p>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

	}
    
} else { // display form
    
    $body .= "<p align=\"center\">Enter zip codes into the marble.moving_zip_code table for a Moving Company.</p>";
	$body .= "<form action=\"add_moving_zip_codes.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"float: left; margin-left: 0px; width: 100px;\">
	<fieldset>
	<legend><b>Lead ID</b></legend>
	<input name=\"lead_id\" type=\"text\" size=\"6\" maxlength=\"6\">
	</fieldset></div>
	
	<div style=\"float: left; margin-left: 0px; width: 200px;\">
	<fieldset>
	<legend><b>CAT ID</b></legend>
	<select name=\"cat_id\">
		<option value=\"none\">Select</option>
		<option value=\"1\">1 - Auto Shipping</option>
		<option value=\"2\">2 - Moving (house)</option>
		<option value=\"3\">3 - Intl Moving</option>
		<option value=\"4\">4 - Security</option>
	</select>
	</fieldset></div>
	
	<div style=\"float: left; margin-left: 0px; width: 200px;\">
	<fieldset>
	<legend><b>Local or Long Distance</b></legend>
	<select name=\"extra\">
		<option value=\"none\">Select</option>
		<option value=\"local\">Local</option>
		<option value=\"long\">Long</option>
	</select>
	</fieldset>
	</div>
	
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Zip Codes</b></legend>
	<textarea name=\"zip_codes\" rows=\"15\" cols=\"60\"></textarea>
	</fieldset>
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_submit\" type=\"submit\" value=\"Add to Table\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";

}

echo $body;

?>