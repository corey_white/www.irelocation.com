<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_marble_leadformat.php  
**********************************************************************
* Description:  Modifies a record in marble.lead_format 
**********************************************************************
* Creation Date:  3/25/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/



include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":
		
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_marble_leadformat.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
						$body .= "<div>
						<fieldset>
						<legend><b>Format Type</b></legend>
							<select name=\"format_type\">
							<option value=\"email\">EMAIL</option>
							<option value=\"jtxml\">JTXML</option>
							<option value=\"post\">POST</option>
							<option value=\"xml\">XML</option>
							</select>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Lead Body</b></legend>
						<textarea name=\"lead_body\" rows=\"20\" cols=\"40\">$lead_body</textarea>
						<br />
						<p><div style=\"font-family: arial; font-size: 9pt;\">For Non-JTracker:<blockquote>-5 for AUTO<br />-1 for ATUS</blockquote>For Jtracker:<blockquote>Top Auto (auto): 		39fb629f189a6cafb760aee661f108c5 <br />Auto-transport.us (atus): 	80ba1964eb26f42df6c900d68aa4e61e</blockquote></div></p>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						<br />
						<p><div style=\"font-family: arial; font-size: 9pt;\">1 - Auto<br />2 - Moving<br />3 - Int'l Moving<br />4 - Security<br />5 - Call Auto Shippers<br />6 - Merchant<br />7 - Transcription (non medical)<br />8 - Call Center<br />9 - Self Moving<br />10 - Transcription Medical<br />13 - Vanlines Direct</div></p>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Site ID</b></legend>
						<input name=\"site_id\" type=\"text\" value=\"$site_id\" size=\"10\">
						<br />
						<p><div style=\"font-family: arial; font-size: 9pt;\">auto - Top Auto<br />atus - Auto transport<br />pm - Pro Moving<br />usalarm - US Alarm<br />alarmsys - AlarmSys<br />tsc - Top Security/Top Alarm<br />cas - Canada Security<br />aas - Austrlia Security<br />callctr - Call Center<br />merch - Merchant<br />transcribe - Transcribe<br /></div></p>
						</fieldset>
						</div>";

					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update Lead Format</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_lead_format\">\n";
					$body .= "<input name=\"lead_id\" type=\"hidden\" value=\"$lead_id\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;


    case "insert_lead_format": 
    
		$sql = "insert into marble.lead_format set lead_id = '$lead_id', format_id = '$format_id', format_type = '$format_type', lead_body = '$lead_body', cat_id = '$cat_id', site_id = '$site_id' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Lead Format has been inserted for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

		break;


    case "get_record": //-- get the lead_format record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			
			$sql = "select * from marble.lead_format where lead_id = '$lead_id' ";
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id</p>";
				$body .= "<p><a href=\"modify_marble_leadformat.php\">Try Again</a></p>";
				$body .= "<p><a href=\"modify_marble_leadformat.php?action=add&lead_id=$lead_id\">Or Add a new Record</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id</p>";
				$body .= "<p>Notify Programmer</p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$format_id = $rs->myarray["format_id"];
				$format_type = $rs->myarray["format_type"];
				$lead_body = $rs->myarray["lead_body"];
				$cat_id = $rs->myarray["cat_id"];
				$site_id = $rs->myarray["site_id"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_marble_leadformat.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Update Lead for $lead_id</b></legend>";


						$body .= "<div>
						<fieldset>
						<legend><b>Format Type</b></legend>
							<select name=\"format_type\">
							<option value=\"email\"";
								if ( $format_type == "email" ) {
									$body .= " selected";
								} 
								$body .= ">EMAIL</option>
							<option value=\"jtxml\"";
								if ( $format_type == "jtxml" ) {
									$body .= " selected";
								} 
								$body .= ">JTXML</option>
							<option value=\"post\"";
								if ( $format_type == "post" ) {
									$body .= " selected";
								} 
								$body .= ">POST</option>
							<option value=\"xml\"";
								if ( $format_type == "xml" ) {
									$body .= " selected";
								} 
								$body .= ">XML</option>
							</select>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Lead Body</b></legend>
						<textarea name=\"lead_body\" rows=\"20\" cols=\"40\">$lead_body</textarea>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Site ID</b></legend>
						<input name=\"site_id\" type=\"text\" value=\"$site_id\" size=\"10\">
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Update Lead Format</legend>
						<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Notes</legend>
						<p></p>
						</fieldset>
						</div>";
						
				$body .= "</fieldset></div>";
						
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_lead_format\">\n";
					$body .= "<input name=\"format_id\" type=\"hidden\" value=\"$format_id\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;


    case "update_lead_format": //-- update the record
        
		$sql = "update marble.lead_format set format_type = '$format_type', lead_body = '$lead_body', cat_id = '$cat_id', site_id = '$site_id' where format_id = '$format_id' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Lead Format has been updated for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">View/Modify Record in the <strong>Marble.Lead_Format</strong> Table.</p>";
		
		$body .= "<form name=\"mod_lead_format\" action=\"modify_marble_leadformat.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup Lead Format</legend>
			<input name=\"mod_lead_format\" type=\"submit\" value=\"Get Lead Format\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>