<?php
/* 
************************FILE INFORMATION**********************
* File Name:  add_sequential_security_zips.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  7/22/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: This script will add sequential zip codes for a number range for a security company (particularly useful for AU zips).  This code is designed to first delete all the zips for a $LEAD_ID, then add them in a sequential order.

						**** MODIFY AS NEEDED ****
*/

/*
*/
echo "Code Stopped - see code.<br />";
exit;


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$LEAD_ID = "1627";

$added_zip = "";
$added_count = 0;

if ( !$LEAD_ID ) {
    echo "No Lead Id.  Program Halt.<br />";
    exit;
} 

echo "Working on the $ZIP_TABLE table...<br /><br />";

/*
//-- first, we delete all of the lead_ids of $LEAD_ID 
$sql = "delete from $ZIP_TABLE where lead_id = '$LEAD_ID' ";
$rs = new mysql_recordset($sql);
*/


//-- Need to insert all sequential zip codes from 6000 to 6999
for ( $i = 6000; $i < 7000; $i++ ) {

	$insert = "insert into $ZIP_TABLE set lead_id = '$LEAD_ID', zip = '$i' ";
	$rs_insert = new mysql_recordset($insert);
	$added_zip .= "$i, ";
	$added_count++;

}

$added_zip = trim($added_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";

?>