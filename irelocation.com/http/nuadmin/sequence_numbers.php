<?php
/* 
************************FILE INFORMATION**********************
* File Name:  sequence_numbers.php
**********************************************************************
* Description:  this script will output a range of numbers for inputting into the zip code tables (it's sometimes easier to get all the numbers in a range and enter them using another script, as opposed to trying to build code for each small range of numbers needed to be added
**********************************************************************
* Creation Date:  11-10-2008
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/



if ( $_REQUEST['Submit'] ) {
    
	for ( $i = $start; $i <= $stop; $i++ ) {
	
		$added_zip .= "$i, ";
		$added_count++;
	}
	
	$added_zip = trim($added_zip,", ");
	
	echo "<a href=\"sequence_numbers.php\">New Range</a><br /><br />";
	echo "$added_count numbers in range.<br /><br />";
	echo "$added_zip<br />";

} else {
    
	$body .= "<form action=\"sequence_numbers.php\" method=\"post\">
	<fieldset>
	<legend><b>Enter number range to output</b></legend>
	Start: <input type=\"text\" name=\"start\" size=\"20\">
	Stop: <input type=\"text\" name=\"stop\" size=\"20\">
	</fieldset>
	<input name=\"Submit\" type=\"submit\" value=\"submit\">
	</form>";
	
	echo $body;
    
}




?>