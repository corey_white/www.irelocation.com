<?
/* 
************************FILE INFORMATION**********************
* File Name:  bad_emails.php
**********************************************************************
* Description:  goes to the database and checks all of the emails that it finds
**********************************************************************
* Creation Date:  4/8/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

function checkEmailValid($email) { 
	//-- Checks to see if there is an MX record for the domain and if we can make a socket connection to to SMTP port

	list($userName, $mailDomain) = split("@", $email);  //-- split email address into the username and domain. 
	
	if (!checkdnsrr($mailDomain, "MX")) { //-- this checks to see if there is an MX DNS record for the domain
		return false;
	}
	
	
	return true;
}



$monthToCheck = "200804";
$i=0;

echo "<p>Checking $monthToCheck</p>";

$select = "select email from marble.auto_quotes where left(received,6) = '$monthToCheck' and source = 'aff_A1AUTO' order by rand() limit 0,500 ";
$rs = new mysql_recordset($select);

while ($rs->fetch_array()) {
	$i++;
	$email = strtolower($rs->myarray["email"]);
	
	echo "$i - Checking: $email - ";

	if ( !checkEmailValid($email) ) {
		echo "<span style=\"color: red;\">NOT VALID</span><br />";
	} else {
		echo "VALID<br />";
	}

}

echo "Fin<br />";

?>