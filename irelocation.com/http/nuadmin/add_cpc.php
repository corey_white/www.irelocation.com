<?
/* 
************************FILE INFORMATION**********************
* File Name:  add_cpc.php
**********************************************************************
* Description:  This will add to the movingdirectory.costperclick table

						NEEDED INPUT: Search Engine, Site ID, Cost, Clicks, Date (YYYYMMDD, e.g. 20080319)
**********************************************************************
* Creation Date:  3/20/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$yesterday = date("m/d/Y", strtotime("yesterday"));

if ( $add_cpc == "Add CPC" ) { // if we have a submission, process

	//-- clean up the inputed data
	$search_engine = trim($search_engine);
	$site_id = trim($site_id);
	$cost = trim($cost);
	$clicks = trim($clicks);
	$ts = trim($ts);
	
    //-- Validation
	$error_msg = "";
	
	If ($search_engine == "none" ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Search Engine</li>";
	}
	If ($site_id == "none" ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing SITE ID</li>";
	}
	If (!$cost || !is_numeric($cost) ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing COST (or not a number)</li>";
	}
	If (!$clicks ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing CLICKS</li>";
	}
	If (!$ts ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing DATE</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		
		//-- convert data to proper format
		$ts = dateYearFirst($ts);
		
		
		//-- we're doing the SQL this way so that I can later convert this into a multi-form input (inserting multiple records at once from the form)
		$sql = "insert into movingdirectory.costperclick (id,search_engine,site,cost,clicks,ts) 
						values ( NULL,'$search_engine','$site_id','$cost','$clicks','$ts' ) ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<p align=\"center\">CPC data has been inserted with the following:</p>";
			
			$body .= "<p align=\"center\"><strong>$search_engine - $site_id - $cost - $clicks - $ts</strong></p>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

	}
    
}    
    $body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
    $body .= "<p align=\"center\">Enter CPC Data into the movingdirectory.costperclick table.</p>";
	$body .= "<form name=\"addCPC\" action=\"add_cpc.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div>
	<fieldset>
	<legend><b>Search Engine</b></legend>
	<select name=\"search_engine\">
		<option value=\"none\">Select</option>
		<option value=\"google\">Google</option>
		<option value=\"overture\">Yahoo</option>
		<option value=\"msn\">MSN</option>
	</select>
	</fieldset>
	</div>
	
	<div>
	<fieldset>
	<legend><b>Site ID</b></legend>
	<select name=\"site_id\">
		<option value=\"none\">Select</option>
		<option value=\"123cm\">123CM</option>
		<option value=\"asc\">ASC</option>
		<option value=\"atus\">ATUS</option>
		<option value=\"atac\">ATAC</option>
		<option value=\"cs\">CS</option>
		<option value=\"ctac\">CTAC</option>
		<option value=\"me\">ME</option>
		<option value=\"mmc\">MMC</option>
		<option value=\"pm\">PM</option>
		<option value=\"tac\">TAC</option>
		<option value=\"tm\">TM</option>
		<option value=\"tsc\">TSC</option>
		<option value=\"usalarm\">USALARM</option>
		<option value=\"usat\">USAT</option>
	</select>
	</fieldset>
	</div>
	
	<div>
	<fieldset>
	<legend><b>Cost</b> (no commas or symbols)</legend>
	<input name=\"cost\" type=\"text\" size=\"10\">
	</fieldset>
	</div>
	
	<div>
	<fieldset>
	<legend><b>Clicks</b></legend>
	<input name=\"clicks\" type=\"text\" size=\"10\">
	</fieldset>
	</div>
	
	<div>
	<fieldset>
	<legend><b>Date</b></legend>
	<input type=\"text\" name=\"ts\" value=\"$yesterday\" readonly><a href=\"javascript:show_calendar4('document.addCPC.ts', document.addCPC.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
	</fieldset>
	</div>
	
	
	</div>
	
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_cpc\" type=\"submit\" value=\"Add CPC\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";


echo $body;

?>