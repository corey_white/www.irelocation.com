<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_marble_rules.php
**********************************************************************
* Description:  Modifies a record in marble.rules 
**********************************************************************
* Creation Date:  3/25/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/



include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":
		
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_marble_rules.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Origin</b></legend>
					<input name=\"origin\" type=\"text\" value=\"$origin\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Destination</b></legend>
					<input name=\"destination\" type=\"text\" value=\"$destination\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Origin Range</b></legend>
					<input name=\"origin_range\" type=\"text\" value=\"$origin_range\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Destination Range</b></legend>
					<input name=\"destination_range\" type=\"text\" value=\"$destination_range\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Earilest Move Days</b></legend>
					<input name=\"earliest_move_days\" type=\"text\" value=\"$earliest_move_days\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Latest Move Days</b></legend>
					<input name=\"latest_move_days\" type=\"text\" value=\"$latest_move_days\" size=\"10\" size=\"10\">
					</fieldset>
					</div>";
					
					$body .= "<div>
					<fieldset>
					<legend><b>Local Moves</b></legend>
					<input name=\"local_moves\" type=\"text\" value=\"$local_moves\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead Email*</b></legend>
					<input name=\"lead_email\" type=\"text\" value=\"$lead_email\" size=\"10\">
					</fieldset>
					</div>";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Site ID*</b></legend>
					<select name=\"site_id\">
						<option value=\"auto\">AUTO</option>
						<option value=\"atus\">ATUS</option>
					</select>
					</fieldset>
					</div>";

					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update Rule</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_rule\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;

    case "insert_rule": 
		$sql = "insert into marble.rules set lead_id = '$lead_id', origin = '$origin', destination = '$destination', origin_range = '$origin_range', destination_range = '$destination_range', earliest_move_days = '$earliest_move_days', latest_move_days = '$latest_move_days', local_moves = '$local_moves', lead_email = '$lead_email', site_id = '$site_id' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Rule has been inserted for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}


		
		break;


    case "get_record": //-- get the rule record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			
			$sql = "select * from marble.rules where lead_id = '$lead_id' ";
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id</p>";
				$body .= "<p><a href=\"modify_marble_rules.php\">Try Again</a></p>";
				$body .= "<p><a href=\"modify_marble_rules.php?action=add&lead_id=$lead_id\">Or Add a new Record</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id</p>";
				$body .= "<p>Notify Programmer</p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$rule_id = $rs->myarray["rule_id"];
				$origin = $rs->myarray["origin"];
				$destination = $rs->myarray["destination"];
				$origin_range = $rs->myarray["origin_range"];
				$destination_range = $rs->myarray["destination_range"];
				$earliest_move_days = $rs->myarray["earliest_move_days"];
				$latest_move_days = $rs->myarray["latest_move_days"];
				$local_moves = $rs->myarray["local_moves"];
				$lead_email = $rs->myarray["lead_email"];
				$site_id = $rs->myarray["site_id"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_marble_rules.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Update Lead for $lead_id</b></legend>";


						$body .= "<div>
						<fieldset>
						<legend><b>Origin</b></legend>
						<input name=\"origin\" type=\"text\" value=\"$origin\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Destination</b></legend>
						<input name=\"destination\" type=\"text\" value=\"$destination\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Origin Range</b></legend>
						<input name=\"origin_range\" type=\"text\" value=\"$origin_range\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Destination Range</b></legend>
						<input name=\"destination_range\" type=\"text\" value=\"$destination_range\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Earilest Move Days</b></legend>
						<input name=\"earliest_move_days\" type=\"text\" value=\"$earliest_move_days\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Latest Move Days</b></legend>
						<input name=\"latest_move_days\" type=\"text\" value=\"$latest_move_days\" size=\"10\" size=\"10\">
						</fieldset>
						</div>";
						
	
						$body .= "<div>
						<fieldset>
						<legend><b>Local Moves</b></legend>
						<input name=\"local_moves\" type=\"text\" value=\"$local_moves\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Lead Email*</b></legend>
						<input name=\"lead_email\" type=\"text\" value=\"$lead_email\" size=\"40\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Site ID*</b></legend>
						<select name=\"site_id\">
							<option value=\"auto\"";
							if ( $site_id == "auto" ) {
								$body .= " selected";
							} 
							$body .= ">AUTO</option>
							<option value=\"atus\"";
							if ( $site_id == "atus" ) {
								$body .= " selected";
							} 
							$body .= ">ATUS</option>
						</select>
						</fieldset>
						</div>";
						
	
	
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Update Rule</legend>
						<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Notes</legend>
						<p></p>
						</fieldset>
						</div>";
						
				$body .= "</fieldset></div>";
						
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_rule\">\n";
					$body .= "<input name=\"rule_id\" type=\"hidden\" value=\"$rule_id\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;


    case "update_rule": //-- update the record
        
		$sql = "update marble.rules set origin = '$origin', destination = '$destination', origin_range = '$origin_range', destination_range = '$destination_range', earliest_move_days = '$earliest_move_days', latest_move_days = '$latest_move_days', local_moves = '$local_moves', lead_email = '$lead_email', site_id = '$site_id' where rule_id = '$rule_id' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>Rule has been updated for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">View/Modify Record in the <strong>Marble.Rules</strong> Table.</p>";
		
		$body .= "<form name=\"mod_rule\" action=\"modify_marble_rules.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup Rule</legend>
			<input name=\"mod_rule\" type=\"submit\" value=\"Get Rule\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>