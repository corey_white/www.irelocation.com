<?php 
#include_once "mysql.php";

//-- Code to send to LeadExec that gets put into the submit page AFTER the insertion into our Db, so we have the quote_id to pass into LE
# include "lead_exec_code_security.php";


//-- Clean up phone2
if ( !is_numeric($phone2) || strlen($phone2) != 10 ) {
	$phone2 = "";
}


//-- Split Business and Personal names
if ($quote_type == "com") {
	list($fname,$lname) = split(" ",$name2);
	
	$businessname = $name1;	
	
	$fname = substr($fname,0,20);//ifthis is a long company name, it will get chopped.
	$lname = substr($lname,0,20);//same with contact name, or long lastname.
	$name1 = $fname;
	$name2 = $lname;

} else {
	$name1 = substr($name1,0,20);//ifthis is a long company name, it will get chopped.
	$name2 = substr($name2,0,20);//same with contact name, or long lastname.
	
	$businessname = "";	

}


//-- This code will hack apart the source code to get just the source root
if ( preg_match("/clkbth/i", $source) ) {
   $source = str_replace("clkbth","clkbth_",$source);
} 
$source = str_replace("aff_","",$source);
$feh = explode("_",$source);
$source_root = $feh[0];

//-- set the marketing type
if ( $source_root == "clkbth" ) {
    $marketing_type = "email";
} else {
    $marketing_type = "cpc";
}


$CAMPAIGN = "ushome";			//-- which campaign these leads are for
$POST_URL = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver";			//-- Posting URL

$LEAD_BODY = 
"VID=1340&".
"AID=2327&".
"LID=371&".
"quote_id=$quote_id&".
"received=&".
"source=".urlencode($source)."&".
"referrer=".urlencode($referer)."&".
"quote_type=".urlencode($quote_type)."&".
"firstname=".urlencode($fname)."&".
"lastname=".urlencode($lname)."&".
"phone1=".urlencode($phone1)."&".
"phone2=".urlencode($phone2)."&".
"email=".urlencode($email)."&".
"address=".urlencode($address)."&".
"city=".urlencode($city)."&".
"state_code=".urlencode($state)."&".
"zip=".urlencode($zip)."&".
"own_rent=".urlencode($own_rent)."&".
"comments=".urlencode($comments)."&".
"campaign=".urlencode($CAMPAIGN)."&".
"marketing_type=".urlencode($marketing_type)."&".
"source_root=".urlencode($source_root)."&".
"remote_ip=".urlencode($remote_ip);



function leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY) {	
	$ch=curl_init($POST_URL."?".$LEAD_BODY);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
	$response = curl_exec($ch);
	curl_close($ch);
	
	$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";
	#echo "<br />-------------------------<br />LeadExec Repsonse <br />Url: ".$POST_URL."?".$LEAD_BODY." <br/>-------------------------<br/>Response: $response<br/>-------------------------<br />";
	
	if (substr_count(strtolower($response),"lead was accepted") == 1) { 
	
		#mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN","$content");
		#save_leadexec_response($QUOTE_ID, "Lead Accepted :: $response",1,"$CAMPAIGN");

	} else {
		
		mail("rob@irelocation.com","LeadExec Repsonse Failure - $CAMPAIGN","$content \n\n$response");
		#save_leadexec_response($QUOTE_ID, "Failure - $response",0,"$CAMPAIGN");
	
	}
}

//save the response in this table. mainy used for security leads.
function save_leadexec_response($quote_id, $result, $sucess, $site) {

	$result = addslashes($result);
	$success = addslashes($sucess);
	$site = addslashes($site);
	
	$sql = "insert into movingdirectory.lead_exec_responses_transcription (quote_id,result,success,site) values ($quote_id,'$result',$sucess,'$site')";
#mail("rob@irelocation.com","LEAD EXEC SQL","$sql");
	$rs = new mysql_recordset($sql);	
	
}		

leadexec_send($QUOTE_ID,$CAMPAIGN,$POST_URL,$LEAD_BODY);
 ?>