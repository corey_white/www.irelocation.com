<?php
/* 
************************FILE INFORMATION**********************
* File Name:  asa_security_zips2.php
**********************************************************************
* Description:  this script will update the zip codes for security leads going to ASA (1634)
**********************************************************************
* Creation Date:  9/8/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: this script first, then checks to see if the zip code is already in the table and if not, inserts a new record for it for 1634 (ASA).
*/

/*
*/
echo "Code Stopped - see code.<br />";
exit;


include_once "../inc_mysql.php";

$ZIP_TABLE = "irelocation.security_zip_codes";
$added_zip = "";
$denied_zip = "";
$added_count = 0;
$denied_count = 0;


echo "Working on the $ZIP_TABLE table...<br /><br />";


//-- Enter all the zips into this array:
$zip_array = array("4205","4207","4208","4209","4210","4211","4212","4213","4214","4215","4216","4217","4218","4220","4221","4222","4223","4224","4225","4226","4227","4228","4229","4230","4131","4132","4133","4151","4152","4153","4154","4155","4156","4157","4158","4159","4160","4161","4163","4164","4165","4169","4170","4171","4172","4173","4174","4178","4179","4183");


//-- now we check each zip code to see if this zip is already in use for another company
foreach($zip_array as $key => $value){
    $sql = "select lead_id from $ZIP_TABLE where zip = '$value' and lead_id = 1634 ";
	$rs = new mysql_recordset($sql);
	$row_count = $rs->rowcount();

	if ( $row_count == 0 ) {
		$insert = "insert into $ZIP_TABLE set lead_id = 1634, zip = '$value' ";
		$rs_insert = new mysql_recordset($insert);
		$added_zip .= "$value, ";
		$added_count++;
	} else {
		$denied_zip .= "$value, ";
		$denied_count++;
	}
}

$added_zip = trim($added_zip,", ");
$denied_zip = trim($denied_zip,", ");

echo "$added_count Zips that were added: $added_zip<br />";
echo "<br />";
echo "$denied_count Zips that already existed: $denied_zip<br />";

?>