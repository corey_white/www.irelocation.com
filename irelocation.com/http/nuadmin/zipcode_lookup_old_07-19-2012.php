<?
/* 
************************FILE INFORMATION**********************
* File Name:		zipcode_lookup.php
**********************************************************************
* Description:  Just a quick script to look up the posted zip and return the city and state.
**********************************************************************
* Creation Date:  July 18, 2012 16:17
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/
error_reporting(E_ERROR | E_WARNING | E_PARSE);

include_once "../inc_mysql.php";

$lookup_zip = preg_replace('/[^0-9]/', '', $_GET['zip']);
$passed_key = preg_replace('/[^A-Za-z]/', '', $_GET['key']);
$passed_code = preg_replace('/[^A-Za-z0-9]/', '', $_GET['code']);

if ( (!$passed_key != "42324q76QQj4ZL" && $passed_code != "M96693x2Ya3h2x") || !$lookup_zip  ) {
    echo "Fail";
    exit;
} else {

	$sql = "select city,state from movingdirectory.zip_codes where zip = '$lookup_zip' ";
	
	$rs = new mysql_recordset($sql);
	$rs->fetch_array();
	
	$city = $rs->myarray["city"];
	$state = $rs->myarray["state"];
	
	echo "$city,$state";
	
}


?>