<?
/* 
************************FILE INFORMATION**********************
* File Name:  import_auto_lead_CSV_IMPORT.php
**********************************************************************
* Description:  This will import csv data into Lead Exec for Auto Leads.  First created to import leads that were not going into LE due to a (other than mine) coding error.
**********************************************************************
* Creation Date:  8/19/10
**********************************************************************
* Modifications (Date/Who/Details):

* NOTES:  The CSV line must contain the following data in this order:

quote_id, source, referrer, fname, lname, email, phone, origin_city, origin_state, origin_zip, destination_city, destination_state, destination_zip, est_move_date, vmake, vmodel, vyear, vtype, running, campaign, remote_ip

**********************************************************************
*/

include_once "../inc_mysql.php";


if ( $add_submit == "Add AUTO Lead" ) { // if we have a submission, process
    
    //-- Validation
	$error_msg = "";
	
	If (!$sec_lead ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Lead Info</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		$sec_lead = trim($sec_lead,",");
		
		//-- put zips into array
		//-- quote_id, source, referrer, fname, lname, email, phone, origin_city, origin_state, origin_zip, destination_city, destination_state, destination_zip, est_move_date, vmake, vmodel, vyear, vtype, running, campaign, remote_ip
		$sec_lead_arr = split(",",$sec_lead);
		
		$quote_id = addslashes($sec_lead_arr[0]);
		$source = addslashes($sec_lead_arr[1]);
		$referrer = addslashes($sec_lead_arr[2]);
		$fname = addslashes($sec_lead_arr[3]);
		$lname = addslashes($sec_lead_arr[4]);
		$email = addslashes($sec_lead_arr[5]);
		$phone = addslashes($sec_lead_arr[6]);
		$origin_city = addslashes($sec_lead_arr[7]);
		$origin_state = addslashes($sec_lead_arr[8]);
		$origin_zip = addslashes($sec_lead_arr[9]);
		$destination_city = addslashes($sec_lead_arr[10]);
		$destination_state = addslashes($sec_lead_arr[11]);
		$destination_zip = addslashes($sec_lead_arr[12]);
		$est_move_date = addslashes($sec_lead_arr[13]);
		$vmake = addslashes($sec_lead_arr[14]);
		$vmodel = addslashes($sec_lead_arr[15]);
		$vyear = addslashes($sec_lead_arr[16]);
		$vtype = addslashes($sec_lead_arr[17]);
		$running = addslashes($sec_lead_arr[18]);
		$campaign = addslashes($sec_lead_arr[19]);
		$remote_ip = addslashes($sec_lead_arr[20]);
		
		echo "<br />";

		//-- Check for Dup
/*		--- BYPASSING this dup check, because this script is for specifically pushing to LE leads that are in our Db but not LE

*/
		
		include "lead_exec_code_auto_csvimport.php";

		$body .= "<p align=\"center\"><a href=\"import_auto_lead_CSV_IMPORT.php\">Import another Auto Lead CSV Line</a></p>";
		

	}
    
} else { // display form
    
    $body .= "<H2 align=\"center\">Lead Exec AUTO LEAD CSV Import</H2>";
    $body .= "<h3 align=\"center\">This differs from the USHome import.  This is to be used for importing leads that already exist in our internal Db.</h3>";
    $body .= "<h3 align=\"center\">The CSV line must contain the following data in this order:</h3>";
    $body .= "<h4 align=\"center\">Quote_Id, Source, Referrer, Fname, Lname, Email, Phone, Origin_City, Origin_State, Origin_Zip, Destination_City, Destination_State, Destination_Zip, Est_Move_Date, Vmake, Vmodel, Vyear, Vtype, Running, Campaign, Remote_Ip</h4>";

	$body .= "<form action=\"import_auto_lead_CSV_IMPORT.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"float: left; margin-left: 0px; width: 500px;\">
	<fieldset>
	<legend><b>Data Columns:</b></legend>
	<small>Quote_Id, Source, Referrer, Fname, Lname, Email, Phone, Origin_City, Origin_State, Origin_Zip, Destination_City, Destination_State, Destination_Zip, Est_Move_Date, Vmake, Vmodel, Vyear, Vtype, Running, Campaign, Remote_Ip</small>
	</fieldset></div>
		
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>AUTO Lead in CSV format:</b></legend>
	<textarea name=\"sec_lead\" rows=\"15\" cols=\"60\"></textarea>
	</fieldset>
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_submit\" type=\"submit\" value=\"Add AUTO Lead\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";

}

echo $body;

?>