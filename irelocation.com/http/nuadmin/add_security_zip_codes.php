<?
/* 
************************FILE INFORMATION**********************
* File Name:  add_security_zip_codes.php
**********************************************************************
* Description:  
**********************************************************************
* Creation Date:  7/22/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once "../inc_mysql.php";


if ( $add_submit == "Add to Table" ) { // if we have a submission, process
    
    //-- Validation
	$error_msg = "";
	
	If (!$lead_id ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing LEAD ID</li>";
	}
	If (!$zip_codes ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Zip Codes</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		$zip_codes = trim(ereg_replace(" ","",$zip_codes),",");
		
		//-- put zips into array
		$zip_arr = split(",",$zip_codes);
		
		#echo "lead_id = $lead_id<br />";
		#echo "cat_id = $cat_id<br />";
		#echo "extra = $extra<br />";
		
		$zip_count = 0;
		
		foreach($zip_arr as $key => $value){
			$zip_count++;
			#echo"Key: $key - Value: $value<br>";
			$sql_values .= "(NULL, '$value', '$lead_id'),";
		}
		
		$sql_values = trim($sql_values,",");
		#echo $sql_values;
		#echo "<br />";
		
		$sql = "insert into irelocation.security_zip_codes (sz_id,zip,lead_id) values $sql_values ";
		#echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<p align=\"center\">$zip_count zip codes have been inserted for $lead_id.</p>";
			$body .= "<p align=\"center\"><a href=\"add_security_zip_codes.php\">Add more zip codes to irelocation.security_zip_codes</a></p>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

	}
    
} else { // display form
    
    $body .= "<p align=\"center\">Enter zip codes into the irelocation.security_zip_codes table for a Security Company.</p>";
	$body .= "<form action=\"add_security_zip_codes.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"float: left; margin-left: 0px; width: 100px;\">
	<fieldset>
	<legend><b>Lead ID</b></legend>
	<input name=\"lead_id\" type=\"text\" size=\"6\" maxlength=\"6\">
	</fieldset></div>
		
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Zip Codes</b></legend>
	<textarea name=\"zip_codes\" rows=\"15\" cols=\"60\"></textarea>
	</fieldset>
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_submit\" type=\"submit\" value=\"Add to Table\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";

}

echo $body;

?>