<?php

function checkEmailValid($email) { 
	//-- Checks to see if there is an MX record for the domain and if we can make a socket connection to to SMTP port

	list($userName, $mailDomain) = split("@", $email);  //-- split email address into the username and domain. 
	
	if (!checkdnsrr($mailDomain, "MX")) { //-- this checks to see if there is an MX DNS record for the domain
		return false;
	}

	return true;
}


if ( !$email ) {
    $email = "rob@irelocation.com";
} 

if ( checkEmailValid($email) ) {
    echo "Email $email is valid<br />";
} else {
    echo "Email $email is NOT valid<br />";
}

echo "<p>&nbsp;</p>";
echo "( To check for a different email, add \"?email=xxx@yyy.zzz\" to the end of the end of the URL )<br />";

?>