<?
/* 
************************FILE INFORMATION**********************
* File Name:  resend_auto_leads.php
**********************************************************************
* Description:  resend leads for an AUTO campaign
**********************************************************************
* Creation Date:  4/8/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
NOTES: Needed input: Lead_ID, string of quote IDs, verification email (optional)
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {

    case "resend": //-- update the record
        
        echo "lead_id = $lead_id<br />";
        echo "quote_ids = $quote_ids<br />";
        echo "email = $email<br />";
        exit;
        
		$sql = "update $TABLE set XXX = '$XXX', XXX = '$XXX', XXX = '$XXX', XXX = '$XXX', XXX = '$XXX', XXX = '$XXX'  where QQQ = '$QQQ' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>$DATATYPE has been updated for: $lead_id</p>";
			
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		
		$body .= "<p align=\"center\">Resend <strong>AUTO</strong> Leads.</p>";
		
		$body .= "<form name=\"disp1\" action=\"$PHP_SELF\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Quote IDs (separated by commas)</b></legend>
				<textarea name=\"quote_ids\" rows=\"20\" cols=\"40\"></textarea>
				</fieldset>
				</div>
				
				<div>
				<fieldset>
				<legend><b>Verification email (sends out a CC of the lead) - Optional</b></legend>
				<input name=\"email\" type=\"text\" size=\"20\">
				</fieldset>
				</div>
			
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Submit</legend>
			<input name=\"submit\" type=\"submit\" value=\"Resend AUTO Leads\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"resend\">
		</form>";
        
        break;
}


echo $body;

?>