<?
/* 
************************FILE INFORMATION**********************
* File Name:  import_security_lead_DUP_CSV_IMPORT.php
**********************************************************************
* Description:  This will import a lead that already exists in our internal Db into Lead Exec.  First created to import leads that were not going into LE due to a (other than mine) coding error.
**********************************************************************
* Creation Date:  7/19/10
**********************************************************************
* Modifications (Date/Who/Details):

* NOTES:  The CSV line must contain the following data in this order:

quote_id,source,referrer,quote_type,name1,name2,phone1,phone2,email,address,city,state_code,zip,own_rent,campaign,remote_ip

**********************************************************************
*/

include_once "../inc_mysql.php";


if ( $add_submit == "Add Security Lead" ) { // if we have a submission, process
    
    //-- Validation
	$error_msg = "";
	
	If (!$sec_lead ) {
		$validation_error = "yes";
		$error_msg .= "<li>Missing Lead Info</li>";
	}
	
	/* add additional checks here */
	
	if ( $validation_error == "yes" ) {
		$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
		$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
		$body .= "<ul class=\"bolder\">$error_msg</ul>";
		$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
		$body .= "</td></tr></table>";
			  
	} else {
		// process form data code here
		$sec_lead = trim($sec_lead,",");
		
		//-- put zips into array
		$sec_lead_arr = split(",",$sec_lead);
		
		$quote_id = addslashes($sec_lead_arr[0]);
		$source = addslashes($sec_lead_arr[1]);
		$referer = addslashes($sec_lead_arr[2]);
		$quote_type = addslashes($sec_lead_arr[3]);
		$name1 = addslashes($sec_lead_arr[4]);
		$name2 = addslashes($sec_lead_arr[5]);
		$phone1 = addslashes($sec_lead_arr[6]);
		$phone2 = addslashes($sec_lead_arr[7]);
		$email = addslashes($sec_lead_arr[8]);
		$address = addslashes($sec_lead_arr[9]);
		$city = addslashes($sec_lead_arr[10]);
		$state_code = addslashes($sec_lead_arr[11]);
		$zip = addslashes($sec_lead_arr[12]);
		$own_rent = addslashes($sec_lead_arr[13]);
		$campaign = addslashes($sec_lead_arr[14]);
		$remote_ip = addslashes($sec_lead_arr[15]);
		
		echo "<br />";

		//-- Check for Dup
/*		--- BYPASSING this dup check, because this script is for specifically pushing to LE leads that are in our Db but not LE
		$daysago2 = date("Ymd",strtotime("-2 days"));
		$dup_check = "select count(*) as count from irelocation.leads_security where left(received,8) >= '$daysago2' and source = '$sec_lead_arr[0]' and phone1 = '$sec_lead_arr[4]' and email = '$sec_lead_arr[6]'  ";
		$rs_dup = new mysql_recordset($dup_check);
		$rs_dup->fetch_array();
		$count = $rs_dup->myarray["count"];

*/
		
		include "lead_exec_code_security_csvimport.php";

		$body .= "<p align=\"center\"><a href=\"import_security_lead_DUP_CSV_IMPORT.php\">Import another CSV Line</a></p>";
		

	}
    
} else { // display form
    
    $body .= "<H2 align=\"center\">Lead Exec CSV Import</H2>";
    $body .= "<h3 align=\"center\">This differs from the USHome import.  This is to be used for importing leads that already exist in our internal Db.</h3>";
    $body .= "<h3 align=\"center\">The CSV line must contain the following data in this order:</h3>";
    $body .= "<h4 align=\"center\">quote_id,source,referrer,quote_type,name1,name2,phone1,phone2,email,address,city,state_code,zip,own_rent,campaign,remote_ip</h4>";
    $body .= "<p align=\"center\">Enter Lead into the irelocation.leads_security table.</p>";
	$body .= "<form action=\"import_security_lead_DUP_CSV_IMPORT.php\" method=\"post\">
	<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
	
	<div style=\"position: static;\">
	
	<div style=\"float: left; margin-left: 0px; width: 500px;\">
	<fieldset>
	<legend><b>Data Columns:</b></legend>
	<small>Quote ID, Source Code, Referrer, Quote Type, First Name, Last Name, Phone1, Phone2, Email, Address, City, State, Zip, Own/Rent, Campaign, IP Address</small>
	</fieldset></div>
		
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend><b>Security Lead in CSV format:</b></legend>
	<textarea name=\"sec_lead\" rows=\"15\" cols=\"60\"></textarea>
	</fieldset>
	</div>
	
	<div style=\"clear: both;\">
	<fieldset>
	<legend>Add to Table</legend>
	<input name=\"add_submit\" type=\"submit\" value=\"Add Security Lead\"> <a href=\"index.php\">Cancel</a>
	</fieldset>
	</div>
	
	</div>

</form>";

}

echo $body;

?>