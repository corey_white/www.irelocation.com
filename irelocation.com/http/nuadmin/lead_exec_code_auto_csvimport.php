<?php 
//-- Code to import into LeadExec

/* include code for submit page:
$quote_id = $rs2->last_insert_id();

//-- Import into LeadExec
include "lead_exec_code_auto.php";
*/

//-- Clean up phone2
if ( !is_numeric($phone_alt) || strlen($phone_alt) != 10 ) {
	$phone_alt = "";
}

//-- This code will hack apart the source code to get just the source root
$mysource = str_replace("aff_","",$mysource);
$feh = explode("_",$mysource);
$source_root = $feh[0];


//-- Override the Running status
if ( $running == "r" ) {
    $running = "Yes";
} else {
    $running = "No";
}

$SITE = "Manual Lead CSV Import"; //-- this is simply for easy identification of which site is generating the emails.
$CAMPAIGN = $campaign; //-- artifact from the original import script. Easier to reassign than change all the code.
$POST_URL = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver";			//-- Posting URL

//-- Need to set which campaign goes into LE, as this is how we know how many to send to
if ( $campaign == "auto" ) { //-- TOP AUTO
    $VID = "1463";
    $AID = "2568";
    $LID = "366";
} else { //-- AUTO TRANSPORT
    $VID = "1464";
    $AID = "2569";
    $LID = "366";
}

$LEAD_BODY = 
"VID=$VID&".
"AID=$AID&".
"LID=$LID&".
"quote_id=$quote_id&".
"received=&".
"source=".urlencode($source)."&".
"referrer=".urlencode($referrer)."&".
"fname=".urlencode($fname)."&".
"lname=".urlencode($lname)."&".
"email=".urlencode($email)."&".
"phone=".urlencode($phone)."&".
"origin_city=".urlencode($origin_city)."&".
"origin_state=".urlencode($origin_state)."&".
"origin_zip=".urlencode($origin_zip)."&".
"origin_country=US&".
"destination_city=".urlencode($destination_city)."&".
"destination_state=".urlencode($destination_state)."&".
"destination_zip=".urlencode($destination_zip)."&".
"destination_country=US&".
"est_move_date=".urlencode($est_move_date)."&".
"contact=email&".
"vmake=".urlencode($vmake)."&".
"vmodel=".urlencode($vmodel)."&".
"vyear=".urlencode($vyear)."&".
"vtype=".urlencode($vtype)."&".
"running=".urlencode($running)."&".
"campaign=".urlencode($campaign)."&".
"remote_ip=".urlencode($_SERVER['REMOTE_ADDR']);


function leadexec_send($QUOTE_ID,$CAMPAIGN,$SITE,$POST_URL,$LEAD_BODY) {	

	$ch=curl_init($POST_URL."?".$LEAD_BODY);	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
	$response = curl_exec($ch);
	curl_close($ch);
	
	$content = "Url: ".$POST_URL."?".$LEAD_BODY." \n\nResponse: $response<br/>";

	if (substr_count(strtolower($response),"lead was accepted") == 1) { 
	
		echo "$QUOTE_ID - Lead Accepted <br />$response <br />$CAMPAIGN";

		mail("rob@irelocation.com","LeadExec Repsonse Accepted - $CAMPAIGN - $SITE","$content");
		
	} else {
		
		echo "$QUOTE_ID - Lead Repsonse Failure <br />$response <br />$CAMPAIGN";
		
		mail("rob@irelocation.com","LeadExec Repsonse Failure - $CAMPAIGN - $SITE","$content \n\n$response");

	}

/*
	if (substr_count(strtolower($response),"duplicate_lead") > 0) { // do not fire tracking pixels for email campaigns if it is a duplicate lead
		return 1;
	} else { 
		return 0;
	}
*/
	
}

//save the response in this table. mainy used for security leads.
function save_leadexec_response($quote_id, $result, $sucess, $site) {

	$result = addslashes($result);
	$success = addslashes($sucess);
	$site = addslashes($site);
	
	$sql = "insert into movingdirectory.lead_exec_responses_transcription (quote_id,result,success,site) values ($quote_id,'$result',$sucess,'$site')";
#mail("rob@irelocation.com","LEAD EXEC SQL","$sql");
	$rs = new mysql_recordset($sql);	
	
}		

leadexec_send($QUOTE_ID,$CAMPAIGN,$SITE,$POST_URL,$LEAD_BODY);
 ?>