<?
/* 
************************FILE INFORMATION**********************
* File Name:  modify_md_directleads.php
**********************************************************************
* Description:  Modifies a record in MovingDirectory.DirectLeads
**********************************************************************
* Creation Date:  3/25/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/



include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}

$current_date = date('m/d/Y',strtotime("now"));


switch ( $action ) {
	case "add":  //--  ADD PART NOT FINISHED
		
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_md_directleads.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Lead ID*</b></legend>
					<input name=\"lead_id\" type=\"text\" value=\"$lead_id\" size=\"10\">
					</fieldset>
					</div>";
				
						$body .= "<div>
						<fieldset>
						<legend><b>Company ID*</b></legend>
						<input name=\"comp_id\" type=\"text\" value=\"$comp_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Credit</b></legend>
						<input name=\"credit\" type=\"text\" value=\"$credit\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Cost Per Lead</b></legend>
						<input name=\"cost_per_lead\" type=\"text\" value=\"$cost_per_lead\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Total Leads Sent</b></legend>
						<input name=\"total_leads_sent\" type=\"text\" value=\"$total_leads_sent\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Active</b></legend>
						<input name=\"active\" type=\"text\" value=\"$active\" size=\"10\" size=\"10\">
						</fieldset>
						</div>";
	
						$body .= "<div>
						<fieldset>
						<legend><b>Advantage</b></legend>
						<input name=\"advantage\" type=\"text\" value=\"$advantage\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Time Stamp</b></legend>
						<input name=\"timestamp\" type=\"text\" value=\"$timestamp\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Email</b></legend>
						<input name=\"email\" type=\"text\" value=\"$email\" size=\"10\" readonly>
						</fieldset>
						</div>";
						
						$body .= "<div>
						<fieldset>
						<legend><b>Custom Format</b></legend>
						<input name=\"custom_format\" type=\"text\" value=\"$custom_format\" size=\"10\" readonly>
						</fieldset>
						</div>";

					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Update DirectLeads</legend>
					<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
					</fieldset>
					</div>";
					
					$body .= "<div style=\"clear: both;\">
					<fieldset>
					<legend>Notes</legend>
					<p>* = Required</p>
					</fieldset>
					</div>";
				
					$body .= "<input name=\"action\" type=\"hidden\" value=\"insert_dl\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";

		break;

    case "insert_dl": //-- INSERT NOT FINISHED
		$sql = "insert into movingdirectory.directleads set lead_id = '$lead_id', comp_id = '$comp_id', cat_id = '$cat_id', credit = '$credit', cost_per_lead = '$cost_per_lead', total_leads_sent = '$total_leads_sent', active = '$active', advantage = '$advantage', email = '$email', custom_format = '$custom_format',     ";
		echo $sql;
		
/*
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>DirectLeads has been inserted for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}
*/


		
		break;


    case "get_record": //-- get the DirectLeads record and display update form 

		//-- clean up the inputed data
		$lead_id = trim($lead_id);
		
		//-- Validation
		$error_msg = "";
		
		If (!$lead_id ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing LEAD ID</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			
			$sql = "select * from movingdirectory.directleads where lead_id = '$lead_id' ";
			#echo $sql;
			
			$rs=new mysql_recordset($sql);
			$row_count = $rs->rowcount();
			
			
			if ( $row_count == 0 ) {  //--  hey, if we don't find anything, let's stop right here!
				
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p class=\"\" align=\"center\">No Record found for Lead ID: $lead_id</p>";
				$body .= "<p><a href=\"modify_md_directleads.php\">Try Again</a></p>";
				#$body .= "<p><a href=\"modify_md_directleads.php?action=add&lead_id=$lead_id\">Or Add a new Record</a></p>";
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			} elseif ( $row_count > 1 ) { // whoa, found more than 1
			
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
					$body .= "<div style=\"position: static;\">";
					
					$body .= "<p class=\"\" align=\"center\">Found more than one record for Lead ID: $lead_id</p>";
					$body .= "<p>Notify Programmer</p>";
					
					$body .= "</div>";
				
				$body .= "</div>";
			
			} else { //-- okay, we found a record, let's show it
				
				$camp_arr = $rs->fetch_array();
				
				$comp_id = $rs->myarray["comp_id"];
				$cat_id = $rs->myarray["cat_id"];
				$credit = $rs->myarray["credit"];
				$cost_per_lead = $rs->myarray["cost_per_lead"];
				$total_leads_sent = $rs->myarray["total_leads_sent"];
				$active = $rs->myarray["active"];
				$advantage = $rs->myarray["advantage"];
				$timestamp = $rs->myarray["timestamp"];
				$email = $rs->myarray["email"];
				$custom_format = $rs->myarray["custom_format"];

				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				
				$body .= "<div style=\"position: static;\">";
				
					$body .= "<form action=\"modify_md_directleads.php\" method=\"post\">";
				
					$body .= "<div>
					<fieldset>
					<legend><b>Update Lead for $lead_id</b></legend>";


						$body .= "<div>
						<fieldset>
						<legend><b>Company ID</b></legend>
						<input name=\"comp_id\" type=\"text\" value=\"$comp_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>CAT ID</b></legend>
						<input name=\"cat_id\" type=\"text\" value=\"$cat_id\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Credit</b></legend>
						<input name=\"credit\" type=\"text\" value=\"$credit\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Cost Per Lead</b></legend>
						<input name=\"cost_per_lead\" type=\"text\" value=\"$cost_per_lead\" size=\"10\">
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Total Leads Sent</b></legend>
						<input name=\"total_leads_sent\" type=\"text\" value=\"$total_leads_sent\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Active</b></legend>
						<input name=\"active\" type=\"text\" value=\"$active\" size=\"10\" size=\"10\">
						</fieldset>
						</div>";
	
						$body .= "<div>
						<fieldset>
						<legend><b>Advantage</b></legend>
						<input name=\"advantage\" type=\"text\" value=\"$advantage\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Time Stamp</b></legend>
						<input name=\"timestamp\" type=\"text\" value=\"$timestamp\" size=\"10\" readonly>
						</fieldset>
						</div>";
					
						$body .= "<div>
						<fieldset>
						<legend><b>Email</b></legend>
						<input name=\"email\" type=\"text\" value=\"$email\" size=\"10\" readonly>
						</fieldset>
						</div>";
						
						$body .= "<div>
						<fieldset>
						<legend><b>Custom Format</b></legend>
						<input name=\"custom_format\" type=\"text\" value=\"$custom_format\" size=\"10\" readonly>
						</fieldset>
						</div>";
						
	
	
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Update DirectLeads</legend>
						<input name=\"submit\" type=\"submit\" value=\"Update\"> <a href=\"index.php\">Cancel</a>
						</fieldset>
						</div>";
						
						$body .= "<div style=\"clear: both;\">
						<fieldset>
						<legend>Notes</legend>
						<p></p>
						</fieldset>
						</div>";
						
				$body .= "</fieldset></div>";
						
					$body .= "<input name=\"action\" type=\"hidden\" value=\"update_dl\">\n";
					
					$body .= "</form>";
				
				
				$body .= "</div>";
				
				$body .= "</div>";
				
			}
	

		}
				
        break;


    case "update_dl": //-- update the record
        
		$sql = "update movingdirectory.directleads set comp_id = '$comp_id', cat_id = '$cat_id', credit = '$credit', cost_per_lead = '$cost_per_lead', total_leads_sent = '$total_leads_sent', active = '$active', advantage = '$advantage', email = '$email', custom_format = '$custom_format' where lead_id = '$lead_id' ";
		echo $sql;
		
		if ( $rs=new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			$body .= "<p>DirectLeads has been updated for: $lead_id</p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";
			
		} else {
			$body .= "Error inserting into the database.  See programmer.";
		}

        break;


    default: //-- display the initial form

		if ( $lead_id ) { //-- if we have a lead_id, go ahead and redirect (no need to click a button again)
			header("location: modify_md_directleads.php?action=get_record&lead_id=$lead_id");
			exit;
		} 
		
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">View/Modify Record in the <strong>MovingDirectory.DirectLeads</strong> Table.</p>";
		
		$body .= "<form name=\"mod_dl\" action=\"modify_md_directleads.php\" method=\"post\">";
		
		$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
			<div style=\"position: static;\">
			
				<div>
				<fieldset>
				<legend><b>LEAD ID</b></legend>
				<input name=\"lead_id\" type=\"text\" size=\"10\" value=\"$lead_id\">
				</fieldset>
				</div>
				
			</div>
		
			<div style=\"clear: both;\">
			<fieldset>
			<legend>Lookup DirectLeads</legend>
			<input name=\"mod_dl\" type=\"submit\" value=\"Get DirectLeads\"> <a href=\"index.php\">Cancel</a>
			</fieldset>
			</div>
		
		</div>
		<input name=\"action\" type=\"hidden\" value=\"get_record\">
		</form>";
        
        break;
}


echo $body;

?>