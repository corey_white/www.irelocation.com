<?php
/* 
************************FILE INFORMATION**********************
* File Name:  bad_emails_bettercheck.php
**********************************************************************
* Description:  performs a very deep check to see if an email is valid
**********************************************************************
* Creation Date:  4/16/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


$body .= "<div id=\"divContainer\" style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">

	<div id=\"divUpper\" style=\"position: static;\">
	
		<div id=\"divFieldsetContainer\" style=\"background-color: #dddddd;\">
		<fieldset>
		<legend><strong>Deep Email Check</strong></legend>

			<div id=\"div1\">
			<fieldset>
			<legend><strong>Email to check</strong></legend>
			<form action=\"bad_emails_bettercheck.php\" method=\"post\">
			<input name=\"email\" type=\"text\" size=\"25\">
			<input name=\"check_email\" type=\"hidden\" value=\"yes\">
			<input name=\"submit\" type=\"submit\" value=\"Check\">
			</form>
			</fieldset>
			</div>
		
		</fieldset>
		</div>
		
	</div>


	<div id=\"divBottom\" style=\"clear: both;\">
	<fieldset>
	<legend><strong>Notes</strong></legend>
	<p>This script does a very deep check on the email address submitted.  It first checks the format of the address, then checks all of the MX servers that are avaiable for the domain name and checks each of them to see if any will accept a connection for that email address. </p>
	</fieldset>
	</div>

</div>
";

function bettercheckEmail( $email, $chFail = false ) {
    global $msgs;
    
    $msgs = Array(); $msgs[] = 'Received email address: '.$email;
    //check for email pattern (adapted and improved from http://karmak.org/archive/2003/02/validemail.html)
    //incorrectly allows IP addresses with block numbers > 256, but those will fail to create sockets anyway
    //unicode norwegian chars cannot be used: C caron, D stroke, ENG, N acute, S caron, T stroke, Z caron (PHP unicode limitation)
    if( !preg_match( "/^(([^<>()[\]\\\\.,;:\s@\"]+(\.[^<>()[\]\\\\.,;:\s@\"]+)*)|(\"([^\"\\\\\r]|(\\\\[\w\W]))*\"))@((\[([0-9]{1,3}\.){3}[0-9]{1,3}\])|(([a-z\-0-9]+\.)+[a-z]{2,}))$/i", $email ) ) {
        $msgs[] = 'Email address was not recognised as a valid email pattern';
        return $chFail ? Array( false, $msgs ) : false; }
    $msgs[] = 'Email address was recognised as a valid email pattern';
    //get the mx host name
    if( preg_match( "/@\[[\d.]*\]$/", $email ) ) {
        $mxHost[0] = preg_replace( "/[\w\W]*@\[([\d.]+)\]$/", "$1", $email );
        $msgs[] = 'Email address contained IP address '.$mxHost[0].' - no need for MX lookup';
    } else {
        //get all mx servers - if no MX records, assume domain is MX (SMTP RFC)
        $domain = preg_replace( "/^[\w\W]*@([^@]*)$/i", "$1", $email );
        if( !getmxrr( $domain, $mxHost, $weightings ) ) { $mxHost[0] = $domain;
            $msgs[] = 'Failed to obtain MX records, defaulting to '.$domain.' as specified by SMTP protocol';
        } else { array_multisort( $weightings, $mxHost );
            $cnt = ''; $co = 0; foreach( $mxHost as $ch ) { $cnt .= ( $cnt ? ', ' : '' ) . $ch . ' (' . $weightings[$co] . ')'; $co++; }
            $msgs[] = 'Obtained the following MX records for '.$domain.': '.$cnt; }
    }
    //check each server until you are given permission to connect, then check only that one server
    foreach( $mxHost as $currentHost ) {
        $msgs[] = 'Checking MX server: '.$currentHost;
        if( $connection = @fsockopen( $currentHost, 25 ) ) {
            $msgs[] = 'Created socket ('.$connection.') to '.$currentHost;
            if( preg_match( "/^2\d\d/", $cn = fgets( $connection, 1024 ) ) ) {
                $msgs[] = $currentHost.' sent SMTP connection header - no futher MX servers will be checked: '.$cn;
                while( preg_match( "/^2\d\d-/", $cn ) ) { $cn = fgets( $connection, 1024 );
                    $msgs[] = $currentHost.' sent extra connection header: '.$cn; } //throw away any extra rubbish
                if( !$_SERVER ) { global $HTTP_SERVER_VARS; $_SERVER = $HTTP_SERVER_VARS; }
                //attempt to send an email from the user to themselves (not <> as some misconfigured servers reject it)
                $localHostIP = gethostbyname(preg_replace("/^.*@|:.*$/",'',$_SERVER['HTTP_HOST']));
                $localHostName = gethostbyaddr($localHostIP);
                fputs( $connection, 'HELO '.($localHostName?$localHostName:('['.$localHostIP.']'))."\r\n" );
                if( $success = preg_match( "/^2\d\d/", $hl = fgets( $connection, 1024 ) ) ) {
                    $msgs[] = $currentHost.' sent HELO response: '.$hl;
                    fputs( $connection, "MAIL FROM: <$email>\r\n" );
                    if( $success = preg_match( "/^2\d\d/", $from = fgets( $connection, 1024 ) ) ) {
                        $msgs[] = $currentHost.' sent MAIL FROM response: '.$from;
                        fputs( $connection, "RCPT TO: <$email>\r\n" );
                        if( $success = preg_match( "/^2\d\d/", $to = fgets( $connection, 1024 ) ) ) {
                            $msgs[] = $currentHost.' sent RCPT TO response: '.$to;
                        } else { $msgs[] = $currentHost.' rejected recipient: '.$to; }
                    } else { $msgs[] = $currentHost.' rejected MAIL FROM: '.$from; }
                } else { $msgs[] = $currentHost.' rejected HELO: '.$hl; }
                fputs( $connection, "QUIT\r\n");
                fgets( $connection, 1024 ); fclose( $connection );
                //see if the transaction was permitted (i.e. does that email address exist)
                $msgs[] = $success ? ('Email address was accepted by '.$currentHost) : ('Email address was rejected by '.$currentHost);
                return $chFail ? Array( $success, $msgs ) : $success;
            } elseif( preg_match( "/^550/", $cn ) ) {
                $msgs[] = 'Mail domain denies connections from this host - no futher MX servers will be checked: '.$cn;
                return $chFail ? Array( false, $msgs ) : false;
            } else { $msgs[] = $currentHost.' did not send SMTP connection header: '.$cn; }
        } else { $msgs[] = 'Failed to create socket to '.$currentHost; }
    } $msgs[] = 'Could not establish SMTP session with any MX servers';
    return $chFail ? Array( false, $msgs ) : false;
}

if ( $check_email == "yes" ) {
    
	$check = bettercheckEmail($email,true);

	while ( list ( $key, $value ) = each ( $check[1] ) ) {
		$check_steps .= "$key $value <br />";
	}

	
	if ( $check[0] == 0 ) {
		$email_valid =  "<span style=\"color: red; font-weight: bold;\">EMAIL IS NOT VALID</span><br />";
	} else {
		$email_valid = "<span style=\"color: green; font-weight: bold;\">EMAIL IS VALID</span><br />";
	}


$body .= "<div id=\"divContainer\" style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">

	<div id=\"divUpper\" style=\"position: static;\">
	
		<div id=\"divFieldsetContainer\" style=\"background-color: #dddddd;\">
		<fieldset>
		<legend><strong>Result</strong></legend>

			<div id=\"div1\">
			<fieldset>
			<legend><strong>Valid?</strong></legend>
			<p>$email_valid</p>
			</fieldset>
			</div>
		
			<div id=\"div2\">
			<fieldset>
			<legend><strong>Check Output</strong></legend>
			<p>$check_steps</p>
			</fieldset>
			</div>

		</fieldset>
		</div>
		
	</div>


	<div id=\"divBottom\" style=\"clear: both;\">
	<fieldset>
	<legend><strong>Notes</strong></legend>
	<p>Some mail servers will accept any connections and will always return a valid response.  You can test for this by sending it a known garbage email address.</p>
	</fieldset>
	</div>

</div>
";	
} 

echo $body;

?>