<?
/* 
************************FILE INFORMATION**********************
* File Name:		zipcode_dups.php
**********************************************************************
* Description:  Lists the duplicate zip codes and lists the lead_id's
**********************************************************************
* Creation Date:  4/22/08 10:24 AM
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

$sql = "select zip, count(zip) from marble.moving_zip_codes group by zip having count(zip) > 1";
$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {
	$zip = $rs->myarray["zip"];
	echo "$zip<br />";
	echo "<blockquote>";
	
	$sql2 = "select lead_id from marble.moving_zip_codes where zip = '$zip' ";
	$rs2 = new mysql_recordset($sql2);

	while ($rs2->fetch_array()) {
		$lead_id = $rs2->myarray["lead_id"];
		echo "$lead_id<br />";
	}
	echo "</blockquote>";

}


?>