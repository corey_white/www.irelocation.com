<?php
/**
 * Script to save quote form results
 */
 
if(!$_POST){
    header('HTTP/1.0 404 Not Found');
    exit;
}
 
date_default_timezone_set('America/Phoenix');
define("LEADEXEC", true); //Send lead to LeadExec
define("DEBUG", true); //if true, will not save to database

session_start();

//Set Some Options
	$options = array(
		'source'    => $_SESSION['source'] ? $_SESSION['source'] : tm,
		'referrer'  => $_SESSION['referer'],
		'remote_ip' => $_SERVER['REMOTE_ADDR']
	);
// Set Moving Options
	$moving_options = array( 
		'VID'       => 1216,
		'AID'       => 2151,
		'LID'       => 370,
		'source'    => $_SESSION['source'] ? $_SESSION['source'] : tm,
		'referrer'  => $_SESSION['referer'],
		'remote_ip' => $_SERVER['REMOTE_ADDR'],
		'campaign'  => 'Moving',
		'site' => 'TopMoving'
	);

//Set Vehicle Options
	$vehicle_options = array(
		'VID'       => 1463,
		'AID'       => 2568,
		'LID'       => 366,
		'source'    => $_SESSION['source'] ? $_SESSION['source'] : tm,
		'referrer'  => $_SESSION['referer'],
		'remote_ip' => $_SERVER['REMOTE_ADDR'],
		'campaign'  => 'auto',
		'site' => 'topmoving.com'
	);



include_once('config.php');

$link = mysql_connect(MYSQL_HOST, MYSQL_USER, MYSQL_PASS);
mysql_select_db(MYSQL_DB);




/**
 * Function to pull the city/state for the given zip code
 * Pass the city and state by reference so that the values will be set
 * @return bool 
 * @since 2011-09-27 jolawski Updated sql query to use zip_primaries database
 */
function get_city_state($zip, &$city, &$state){
    $result = mysql_query("SELECT city,state FROM zip_primaries WHERE zip='$zip'");
    if (mysql_num_rows($result) > 0) {
        $row = mysql_fetch_row($result);
        $city = $row[0];
        $state = $row[1];
        return true;
    }
    else {
        return false;
    }
}

/**
 * Function to get the zip for the given city/state
 * Pass the zip by reference so that the value will be set
 * @return bool
 * @since 2011-09-27 jolawski Updated sql query to use zip_primaries database
 */
function get_zip($city, $state, &$zip){
    $result = mysql_query("SELECT zip FROM zip_primaries WHERE LOWER(city)='".strtolower($city)."' AND state='$state'");
    if (mysql_num_rows($result) > 0) {
        $row = mysql_fetch_row($result);
        $zip = $row[0];
        return true;
    }
    else {
        return false;
    }
}

/**
 * Function to validate the phone number via the areacodes matching database
 * @TODO should probably alert someone if this fails so that we can identify missing area codes
 * @return bool
 * @since 2011-09-27 jolawski Updated sql queries
 */
function validate_phone($phone){
    list($p1, $p2, $p3) = explode("-", $phone);
    $result = mysql_query("SELECT country FROM `movingdirectory`.areacodes WHERE npa='$p1' and nxx='$p2' LIMIT 1");
    if (mysql_num_rows($result) > 0) {
        return true;
    }
    else {
        return false;
    }
}

//Read the POST and sanitize the fields

//Split the name into first/last
$name = trim( ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_POST['full_name']) ) ) );
if(substr_count($name, " ") == 0){
    $first_name = $name;
    $last_name = "N/A";
}
else{
    list($first_name, $last_name) = explode(" ", $name, 2);
}

//This will sanitize all fields so that you don't have to worry about injection
$fields = array(
    'fname'         => $first_name,
    'lname'         => $last_name,
    'phone'         => (preg_replace('/[^0-9]/', '', $_POST['phone1']) . '-' . preg_replace('/[^0-9]/', '', $_POST['phone2']) . '-' . preg_replace('/[^0-9]/', '', $_POST['phone3'])),
    'email'         => strtolower( preg_replace('/[^A-Za-z0-9\\.@-_]/', '', $_POST['email']) ),
    'move_date'     => preg_replace('/[^0-9\/]/', '', $_POST['move_date']),
    'from_zip'      => preg_replace('/[^0-9]/', '', $_POST['from_zip']),
    'from_city'     => ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_POST['from_city']) ) ),
    'from_state'    => strtoupper( preg_replace('/[^A-Za-z]/', '', $_POST['from_state']) ),
    'to_zip'        => preg_replace('/[^0-9]/', '', $_POST['to_zip']),
    'to_city'       => ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_POST['to_city']) ) ),
    'to_state'      => strtoupper( preg_replace('/[^A-Za-z]/', '', $_POST['to_state']) ),
	'bedrooms'     => ucwords( strtolower( preg_replace('/[^A-Za-z0-9\\s]/', '', $_POST['bedrooms']) ) ),
	'type_home'     => ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_POST['type_home']) ) ),
	'move_type'     => ucwords( strtolower( preg_replace('/[^A-Za-z\\s]/', '', $_POST['move_type']) ) )
);


//Read the vehicles.  By default we allow up to 5
//Again, this will sanitize all the fields
if ($car == "yes") {
	$vehicles = array();
	for($i=0; $i<5; $i++){
		$make = preg_replace('/[^A-Za-z\\s-]/', '', $_POST['make'.$i]);
		$model = preg_replace('/[^A-Za-z0-9\\s-|]/', '', $_POST['model'.$i]);
		$year = preg_replace('/[^0-9]/', '', $_POST['year'.$i]);
		$condition = preg_replace('/[^A-Za-z\\s]/', '', $_POST['condition'.$i]);
		
		if(substr_count($model, "|") > 0){
			list($model, $type) = explode("|", $model);
		}
		
		if($make != '' && $model != '' && $year != '' && $condition != ''){
			$vehicles[$i] = array(
				'make' => $make,
				'model' => $model,
				'year' => $year,
				'type' => $type,
				'condition' => $condition
			);
		}
	}
}

//Validate all the fields
$errors = array();

foreach($fields as $field => $val){
    switch($field){
        case 'fname':
            if(strlen($val) < 2){
                $errors[] = "full_name";
            }
            break;
        case 'lname':
            if(strlen($val) < 2){
                $errors[] = "full_name";
            }
            break;
        case 'phone':
            if(!preg_match('/[0-9]{3}-[0-9]{3}-[0-9]{4}/', $val)){
                $errors[] = "phone1";
                $errors[] = "phone2";
                $errors[] = "phone3";
            }
            else{
                if(!validate_phone($val)){
                    $errors[] = "phone1";
                    $errors[] = "phone2";
                    $errors[] = "phone3";
                }
            }
            break;
        case 'email':
            if(!preg_match('/([\\w-+]+(?:\\.[\\w-+]+)*@(?:[\\w-]+\\.)+[a-zA-Z]{2,7})/', $val)){
                $errors[] = $field;
            }
            else{
                //split email and check domain MX
                list($user, $domain) = split("@", $val);
                if(!DEBUG){
                    if(!checkdnsrr($domain, "MX")) {
                        $errors[] = $field;
                    } 
                }
            }
            break;
        case 'move_date':
            if(!preg_match('/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/', $val)){
                $errors[] = $field;
            }
            else{
                //need to make sure it's a valid date 
                list($month, $day, $year) = explode("/", $val);
                $time = mktime(0,0,0,$month,$day,$year);
                if($time < time()){
                    $errors[] = $field;
                }
                else{
                    //this ensures that the date is valid (for example if the visitor chooses 2011-02-31, it will change to 2011-03-03)
                    $fields['move_date'] = date("Y-m-d", $time);
                }
            }
            break;
        case 'from_zip':
            if(!preg_match('/[0-9]{5}/', $val)){
                //No zip, so check city/state
                $zip="";
                if(get_zip($fields['from_city'], $fields['from_state'], &$zip)){
                    //Found zip
                    $fields['from_zip'] = $zip;
                }
                else{
                    //No zip found
                    $errors[] = 'from_city';
                }
            }
            else{
                //Pull city/state for this zip
                $city="";
                $state="";
                if(get_city_state($val, &$city, &$state)){
                    //Found city/state
                    $fields['from_city'] = $city;
                    $fields['from_state'] = $state;
                }
                else{
                    //No city/state found
                    $errors[] = $field;
                }
            }
            break;
        case 'to_zip':
            if(!preg_match('/[0-9]{5}/', $val)){
                //No zip, so check city/state
                $zip="";
                if(get_zip($fields['to_city'], $fields['to_state'], &$zip)){
                    //Found zip
                    $fields['to_zip'] = $zip;
                }
                else{
                    //No zip found
                    $errors[] = 'to_city';
                }
            }
            else{
                //Pull city/state for this zip
                $city="";
                $state="";
                if(get_city_state($val, &$city, &$state)){
                    //Found city/state
                    $fields['to_city'] = $city;
                    $fields['to_state'] = $state;
                }
                else{
                    //No city/state found
                    $errors[] = $field;
                }
            }
            break;
    }
}

//Additional Validation
if($fields['from_zip'] == $fields['to_zip']){
    $errors[] = 'from_zip';
    $errors[] = 'to_zip';
}

if ($car == "yes") {
	if(count($vehicles)==0){
		$errors[] = 'vehicles';
	}

	//Check for duplicates in the past 24 hours
	$time = date("YmdHis",time() - (24 * 3600));

	/**
	 * @since 2011-09-27 jolawski Updated SQL query functions
	 */
	$sql = "SELECT * FROM `marble`.auto_quotes WHERE received > '".$time."' ".
			"AND vyear = '".$vehicles[0]['year']."'".
			"AND vmake like '%".$vehicles[0]['make']."%' ".
			"AND vmodel like '%".$vehicles[0]['model']."%' ".
			"AND phone = '".str_replace('-','',$fields['phone'])."' LIMIT 1 ";

	$result = mysql_query($sql);
	if(mysql_num_rows($result) > 0){
		$errors[] = 'duplicate';
	}
}
//Return on error
if(count($errors) > 0){
    echo json_encode( array('isSuccess' => 'false', 'errors' => $errors) );
    exit;
}

//This code will hack apart the source code to get just the source root
$options['source'] = str_replace("aff_","",$options['source']);
$feh = explode("_",$options['source']);
$source_root = $feh[0];

//Save the leads to the local database
$now = gmdate("YmdHis");

/**
 * @since 2011-09-27 jolawski Updated SQL query functions
 */

if ($car == "yes") { // if vehicles are added

	

	foreach($vehicles as $vehicle){
		$sql = "INSERT INTO `marble`.auto_quotes SET received = '$now', ".
		" fname = '".$fields['fname']."', lname = '".$fields['lname']."', email = '".$fields['email']."', ".
		" source = '".$options['source']."', remote_ip = '".$options['remote_ip']."', referrer = '".$options['referrer']."', ".
		" phone = '".$fields['phone']."', origin_city = '".$fields['from_city']."', origin_state ".
		" = '".$fields['from_state']."', origin_zip = '".$fields['from_zip']."', destination_city = '".$fields['to_city']."',".
		" destination_state = '".$fields['to_state']."', customer_comments = '',".
		" destination_zip = '".$fields['to_zip']."', est_move_date = '".$fields['move_date']."', ".
		" contact ='email', running = '".$vehicle['condition']."', vmake = '".$vehicle['make']."', ".
		" vmodel = '".$vehicle['model']."', vyear = '".$vehicle['year']."', vtype ='".$vehicle['type']."', ".
		" campaign = '".$vehicle_options['campaign']."'";
		
		if(!DEBUG){
			mysql_query($sql);
			$result = mysql_query("SELECT LAST_INSERT_ID()");
			$row = mysql_fetch_row($result);
			$quote_id = $row[0];
		}

		//Process leadexec
		if(LEADEXEC){       
			// create a new cURL resource
			$ch = curl_init();
			
			$post_fields = array(
				'quote_id' => $quote_id,
				'fname' => $fields['fname'],
				'lname' => $fields['lname'],
				'email' => $fields['email'],
				'phone' => $fields['phone'],
				'phone_alt' => "",
				'customer_comments' => "",
				'origin_city' => $fields['from_city'],
				'origin_state' => $fields['from_state'],
				'origin_zip' => $fields['from_zip'],
				'origin_country' => "US",
				'destination_city' => $fields['to_city'],
				'destination_state' => $fields['to_state'],
				'destination_zip' => $fields['to_zip'],
				'destination_country' => "US",
				'est_move_date' => $fields['move_date'],
				'contact' => 'email',
				'vmake' => $vehicle['make'],
				'vmodel' => $vehicle['model'],
				'vyear' => $vehicle['year'],
				'vtype' => $vehicle['type'],
				'running' => ($vehicle['condition']!='Running' ? 'No' : 'Yes'),
				'marketing_type' => 'cpc',
				'source_root' => $source_root
			);
			
			// set URL and other appropriate options
			$settings = array(
				//CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HEADER => false,
				CURLOPT_POST => true,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_POSTFIELDS => array_merge($vehicle_options, $options, $post_fields),
				CURLOPT_URL => "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver"
			);
			
			curl_setopt_array($ch, $settings);
			
			// grab URL and pass it to the browser
			if(!DEBUG){
				$result = curl_exec($ch);
			}
			else{
				$result = '<isValidPost>true</isValidPost>';
			}
			
			// close cURL resource, and free up system resources
			curl_close($ch);
			
			// parse the response
			$valid = substr_count($result, '<isValidPost>true</isValidPost>');
			
			if($valid > 0){
				$rpart_a = explode('<LeadIdentifier>', $result);
				$rpart_b = $rpart_a[1];
				$rpart_c = explode('</LeadIdentifier>', $rpart_b);
				$lead_identifier = $rpart_c[0];
				
				/**
				 * @since 2011-09-27 jolawski Updated SQL query functions
				 */
				$sql = "INSERT INTO `irelocation`.lemailer SET `date` = '$now', site = '".str_replace('www.', '', $_SERVER["SERVER_NAME"])."', email = '".$fields['email']."', leid = '$lead_identifier'";
				
				if(!DEBUG){
					mysql_query($sql);
				}
			}
			else {
				$link = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver?";
				foreach(array_merge($vehicle_options, $options, $post_fields) as $key => $val){
					$link .= $key . '=' . urlencode($val);
				}
				mail("mark@irelocation.com","LeadExec Repsonse Failure - ".$vehicle_options['campaign']." - ".$vehicle_options['site'], $link . "\n\n" . print_r(array_merge($vehicle_options, $options, $post_fields), true) . "\n\nResponse: " . $result);
			}
		}
	}

}  
//process moving lead

	

	//-- Calculate No. Bedrooms and Poundage
switch ( $fields['bedrooms'] ) {
    case "no bedroom":
        $number_bedrooms = "0";
        $poundage = "1500";
        break;

    case "one bedroom":
        $number_bedrooms = "1";
        $poundage = "4000";
        break;

    case "two bedrooms":
        $number_bedrooms = "2";
        $poundage = "6500";
        break;

    case "three bedrooms":
        $number_bedrooms = "3";
        $poundage = "9000";
        break;

    case "four bedrooms":
        $number_bedrooms = "4";
        $poundage = "12000";
        break;

    case "five bedrooms":
         $number_bedrooms = "5";
        $poundage = "Over 12000";
       break;

    case "six bedrooms":
         $number_bedrooms = "6";
        $poundage = "Over 12000";
       break;

    default:
        $number_bedrooms = "2";
        $poundage = "6500";
        break;
}

	

	$sql1 = "INSERT INTO `movingdirectory`.quotes SET received = '$now', ".
		" name = '".$name."', cat_id = '2', email = '".$fields['email']."', ".
		" source = '".$options['source']."', remote_ip = '".$options['remote_ip']."', referrer = '".$options['referrer']."', ".
		" phone_home = '".$fields['phone']."', origin_city = '".$fields['from_city']."', origin_state = '".$fields['from_state']."', ".
		" origin_zip = '".$fields['from_zip']."', destination_city = '".$fields['to_city']."', ".
		" destination_state = '".$fields['to_state']."', comments = '', destination_country = 'US', origin_country = 'US', ".
		" destination_zip = '".$fields['to_zip']."', est_move_date = '".$fields['move_date']."', ".
		" contact ='email', campaign = '".$moving_options['campaign']."'";
		
		if(!DEBUG){
			mysql_query($sql1);
			$result1 = mysql_query("SELECT LAST_INSERT_ID()");
			$row1 = mysql_fetch_row($result1);
			$quote_id1 = $row1[0];
		}

		//Process leadexec
		if(LEADEXEC){       
			// create a new cURL resource
			$ch = curl_init();
			
			$post_fields = array(
	
				
				
				'quote_id' => $quote_id1,
				'FirstName' => $fields['fname'],
				'LastName' => $fields['lname'],
				'email' => $fields['email'],
				'phone_home' => $fields['phone'],
				'phone_alt' => "",
				'customer_comments' => "",
				'lead_ids' => $fields['cid'],
				'cat_id' => $fields['cat_id'],
				'origin_city' => $fields['from_city'],
				'origin_state' => $fields['from_state'],
				'origin_zip' => $fields['from_zip'],
				'origin_country' => "US",
				'destination_city' => $fields['to_city'],
				'destination_state' => $fields['to_state'],
				'destination_zip' => $fields['to_zip'],
				'destination_country' => "US",
				'est_move_date' => $fields['move_date'],
				'contact' => $fields['contact'],
				'number_bedrooms' => $number_bedrooms,
				'poundage' => $poundage,
				'contact' => 'email',
				'marketing_type' => 'cpc',
				'source_root' => $source_root
			);
			
			// set URL and other appropriate options
			$settings = array(
				//CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HEADER => false,
				CURLOPT_POST => true,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_POSTFIELDS => array_merge($moving_options, $options, $post_fields),
				CURLOPT_URL => "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver"
			);
			
			curl_setopt_array($ch, $settings);
			
			// grab URL and pass it to the browser
			if(!DEBUG){
				$result = curl_exec($ch);
			}
			else{
				$result = '<isValidPost>true</isValidPost>';
			}
			
			// close cURL resource, and free up system resources
			curl_close($ch);
			
			// parse the response
			$valid = substr_count($result, '<isValidPost>true</isValidPost>');
			
			if($valid > 0){
				
				if(!DEBUG){
					mysql_query($sql);
				}
			}
			else {
				$link = "https://www.leadproweb.com/services/interfaces/public/Leadimport.asmx/LeadReceiver?";
				foreach(array_merge($moving_options, $options, $post_fields) as $key => $val){
					$link .= $key . '=' . urlencode($val);
				}
				mail("mark@irelocation.com","LeadExec Repsonse Failure - ".$moving_options['campaign']." - ".$moving_options['site'], $link . "\n\n" . print_r(array_merge($moving_options, $options, $post_fields), true) . "\n\nResponse: " . $result);
			}
		}


if(DEBUG){
    echo "OPTIONS: <pre>".print_r($options ,true)."</pre><br>";
	echo "MOVING_OPTIONS: <pre>".print_r($moving_options ,true)."</pre><br>";
	echo "VEHICLE_OPTIONS: <pre>".print_r($vehicle_options ,true)."</pre><br>";
    echo "FIELDS: <pre>".print_r($fields ,true)."</pre><br>";
    echo "VEHICLES: <pre>".print_r($vehicles ,true)."</pre><br>";
    echo "ERRORS: <pre>".print_r($errors ,true)."</pre><br>";
}
else{
    echo json_encode( array('isSuccess' => 'true') );
}
