<?
/* 
************************FILE INFORMATION**********************
* File Name:  edit_cpc.php
**********************************************************************
* Description:  This will edit an existing CPC data record in the movingdirectory.costperclick table
**********************************************************************
* Creation Date:  3/20/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/


include_once "../inc_mysql.php";

// change date format to match db, from MM/DD/YYYY to YYYYMMDD
function dateYearFirst($dateinput){
    $dateinput = ereg_replace("-","/",$dateinput);
    $newDate = date('Ymd',strtotime("$dateinput"));
    return $newDate;
}
// change date format to match JS, from YYYY-MM-DD to MM/DD/YYYY
function dateYearLast($dateinput){
	$dateinput = ereg_replace("-","/",$dateinput);
	$newDate = date('m/d/Y',strtotime("$dateinput"));
	return $newDate;
}


if ( !$id ) {
    $action = "";
} 

switch ( $action ) {
    case "confirm":
        
		//-- get the info from the db
		$sql = "select * from movingdirectory.costperclick where id = '$id'  ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		
		$search_engine = $rs->myarray["search_engine"];
		$site = $rs->myarray["site"];
		$cost = $rs->myarray["cost"];
		$clicks = $rs->myarray["clicks"];
		$ts = dateYearLast($rs->myarray["ts"]);

        $body .= "<p align=\"center\">Are you sure you want to delete:<br><i>$search_engine - $site - $cost - $clicks - $ts</i>?</p>";
        $body .= "<p align=\"center\"><a href=\"edit_cpc.php?action=delete&id=$id\">Yes</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href=\"edit_cpc.php\">No</a></p>";
        $body .= "<p align=\"center\">This action cannot be undone.</p>";
        break;


    case "delete":
		$sql = "delete from movingdirectory.costperclick where id = '$id'  ";
		
		if ( $rs = new mysql_recordset($sql) ) {
			$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
			$body .= "<div style=\"position: static;\">";
			
			$body .= "<p>CPC has been Deleted</p>";
			
			$body .= "<p><a href=\"view_cpc.php\">View CPC</a></p>";
			$body .= "<p><a href=\"index.php\">Back to Index</a></p>";
			$body .= "</div>";
			$body .= "</div>";

		} else {
			$body .= "Error deleting record from database.  See programmer.";
		}

        break;


    case "update":
        
		if ( !$id ) {
			$body .= "Missing ID.  End run.";
			break;
		} 

        
		//-- clean up the inputed data
		$search_engine = trim($search_engine);
		$site = trim($site);
		$cost = trim($cost);
		$clicks = trim($clicks);
		$ts = trim($ts);
		
		//-- Validation
		$error_msg = "";
		
		If ($search_engine == "none" ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing Search Engine</li>";
		}
		If ($site == "none" ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing SITE ID</li>";
		}
		If (!$cost || !is_numeric($cost) ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing COST (or not a number)</li>";
		}
		If (!$clicks ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing CLICKS</li>";
		}
		If (!$ts ) {
			$validation_error = "yes";
			$error_msg .= "<li>Missing DATE</li>";
		}
		
		/* add additional checks here */
		
		if ( $validation_error == "yes" ) {
			$body .= "<table width=\"400\" border=\"0\" align=\"center\"><tr><td>";
			$body .= "<p class=\"bolder\">Missing or incorrect information:</p>";
			$body .= "<ul class=\"bolder\">$error_msg</ul>";
			$body .= "<p class=\"bolder\">Please go back and fill in the appropriate fields.</p>";
			$body .= "</td></tr></table>";
				  
		} else {
			// process form data code here
			
			//-- convert data to proper format
			$ts = dateYearFirst($ts);
			
			
			//-- we're doing the SQL this way so that I can later convert this into a multi-form input (inserting multiple records at once from the form)
			$sql = "update movingdirectory.costperclick set search_engine = '$search_engine', site = '$site', cost = '$cost', clicks = '$clicks', ts = '$ts' where id = '$id' ";

			#echo $sql;
			
			if ( $rs=new mysql_recordset($sql) ) {
					
				$body .= "<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">";
				$body .= "<div style=\"position: static;\">";
				
				$body .= "<p align=\"center\">CPC data has been updated with the following:</p>";
				$body .= "<p align=\"center\"><strong>$search_engine - $site - $cost - $clicks - $ts</strong></p>";
				$body .= "<p><a href=\"view_cpc.php\">View CPC</a></p>";
				$body .= "<p><a href=\"index.php\">Back to Index</a></p>";

				$body .= "</div>";
				$body .= "</div>";

				
			} else {
				$body .= "Error inserting into the database.  See programmer.";
			}
	
		}
        break;


    case "lookup":
        
		//-- get the info from the db
		$sql = "select * from movingdirectory.costperclick where id = '$id'  ";
		$rs = new mysql_recordset($sql);
		$rs->fetch_array();
		
		$search_engine = $rs->myarray["search_engine"];
		$site = $rs->myarray["site"];
		$cost = $rs->myarray["cost"];
		$clicks = $rs->myarray["clicks"];
		$ts = dateYearLast($rs->myarray["ts"]);
		
		$body .= "<script language=\"JavaScript\" src=\"js/ts_picker4.js\"></script>";
		$body .= "<p align=\"center\">Update CPC Data into the movingdirectory.costperclick table.</p>";
		$body .= "<form name=\"editCPC\" action=\"edit_cpc.php\" method=\"post\">
		<div style=\"width: 500px; margin-left: auto; margin-right: auto; background-color: #eeeeee;\">
		
		<div style=\"position: static;\">
		
		<div>
		<fieldset>
		<legend><b>Search Engine</b></legend>
		<select name=\"search_engine\">
			<option value=\"none\">Select</option>
			<option value=\"google\"";
				if ( $search_engine == "google" ) {
					$body .= " selected";
				} 
				$body .= ">Google</option>
			<option value=\"overture\"";
				if ( $search_engine == "overture" ) {
					$body .= " selected";
				} 
				$body .= ">Yahoo</option>
			<option value=\"msn\"";
				if ( $search_engine == "msn" ) {
					$body .= " selected";
				} 
				$body .= ">MSN</option>
		</select>
		</fieldset>
		</div>
		
		<div>
		<fieldset>
		<legend><b>Site ID</b></legend>
		<select name=\"site\">
			<option value=\"none\">Select</option>
			<option value=\"123cm\"";
				if ( $site == "123cm" ) {
					$body .= " selected";
				} 
				$body .= ">123CM</option>
			<option value=\"asc\"";
				if ( $site == "asc" ) {
					$body .= " selected";
				} 
				$body .= ">ASC</option>
			<option value=\"atus\"";
				if ( $site == "atus" ) {
					$body .= " selected";
				} 
				$body .= ">ATUS</option>
			<option value=\"atac\"";
				if ( $site == "atac" ) {
					$body .= " selected";
				} 
				$body .= ">ATAC</option>
			<option value=\"cs\"";
				if ( $site == "cs" ) {
					$body .= " selected";
				} 
				$body .= ">CS</option>
			<option value=\"ctac\"";
				if ( $site == "ctac" ) {
					$body .= " selected";
				} 
				$body .= ">CTAC</option>
			<option value=\"me\"";
				if ( $site == "me" ) {
					$body .= " selected";
				} 
				$body .= ">ME</option>
			<option value=\"mmc\"";
				if ( $site == "mmc" ) {
					$body .= " selected";
				} 
				$body .= ">MMC</option>
			<option value=\"pm\"";
				if ( $site == "pm" ) {
					$body .= " selected";
				} 
				$body .= ">PM</option>
			<option value=\"tac\"";
				if ( $site == "tac" ) {
					$body .= " selected";
				} 
				$body .= ">TAC</option>
			<option value=\"tm\"";
				if ( $site == "tm" ) {
					$body .= " selected";
				} 
				$body .= ">TM</option>
			<option value=\"tsc\"";
				if ( $site == "tsc" ) {
					$body .= " selected";
				} 
				$body .= ">TSC</option>
			<option value=\"usalarm\"";
				if ( $site == "usalarm" ) {
					$body .= " selected";
				} 
				$body .= ">USALARM</option>
			<option value=\"usat\"";
				if ( $site == "usat" ) {
					$body .= " selected";
				} 
				$body .= ">USAT</option>
		</select>
		</fieldset>
		</div>
		
		<div>
		<fieldset>
		<legend><b>Cost</b> (no commas or symbols)</legend>
		<input name=\"cost\" type=\"text\" size=\"10\" value=\"$cost\">
		</fieldset>
		</div>
		
		<div>
		<fieldset>
		<legend><b>Clicks</b></legend>
		<input name=\"clicks\" type=\"text\" size=\"10\" value=\"$clicks\">
		</fieldset>
		</div>
		
		<div>
		<fieldset>
		<legend><b>Date</b></legend>
		<input type=\"text\" name=\"ts\" value=\"$ts\" readonly><a href=\"javascript:show_calendar4('document.editCPC.ts', document.editCPC.ts.value);\"><img src=\"js/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Click Here to Pick up the date\"></a>
		</fieldset>
		</div>
		
		
		</div>
		
		
		<div style=\"clear: both;\">
		<fieldset>
		<legend>Edit CPC Record</legend>
		<input name=\"edit_cpc\" type=\"submit\" value=\"Update CPC\"> <a href=\"index.php\">Cancel</a>
		<input name=\"id\" type=\"hidden\" value=\"$id\">
		<input name=\"action\" type=\"hidden\" value=\"update\">
		</fieldset>
		</div>
		
		</div>
	
		</form>";
        break;


    default:
        $body .= "Missing ID or Action.  End run.";
        break;
}



echo $body;

?>