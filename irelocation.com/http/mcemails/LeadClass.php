<?
	class Lead
	{
		var $leadID;
		var $env;
		var $myarray;
		var $thisclass = "Lead";
		
		function Lead($leadID="",$env="prd")
		{
			$this->env = $env;
			$this->myarray = array();
			
			if ($leadID != "")
			{
				$this->leadID = $leadID;
				$this->myarray['leadID'] = $leadID;
				$this->load();
			}
			else
				$this->leadID = 0;
							
			$this->myarray['leadID'] = $leadID;
		}	
		
		function add()
		{			
			if ($this->leadID == 0)
			{
				$keys = array_keys($this->myarray);
				$sql = "insert into movecomp.leads".$this->env." set ";
				foreach($keys as $k)
				{
					if (!is_numeric($k))
						$sql .= "$k = '".$this->myarray[$k]."', ";				
				}		
				$sql = substr($sql,0,-2)."; ";
				$rs = new movecompanion($sql);
				$this->leadID = $rs->last_insert_id();
				$this->myarray['leadID'] = $rs->last_insert_id();
				return true;
			}
			else return false;
		}
		
		function load()
		{
			if ($this->leadID > 0)
			{
				$sql = "select * from movecomp.leads_".$this->env." where leadID = ".$this->leadID;
				$rs = new movecompanion($sql);
				if ($rs->fetch_array())
				{
					$this->myarray = $rs->myarray;			
					return true;
				}
				else 
					return false;
			}
			else 
				return false;

		}
		
		function loadNotes()
		{
			if ($this->leadID != 0)
			{
				$sql = "select noteID from notes_prd where noteScopeCode = 1033 and notRefID = ".$this->leadID.";";
				$rs = new movecompanion($sql);
				
				while ($rs->fetch_array())
					$notes[] = new Note($rs->myarray['noteID']);
					
				return count($notes);
			}
			else 
				return -1;
		}
		
		function update()
		{
			if ($this->leadID > 0)
			{
				$keys = array_keys($this->myarray);
				$sql = "update movecomp.leads_".$this->env." set ";
				foreach($keys as $k)
				{
					if (!is_numeric($k)  && $k != "leadID")
						$sql .= "$k = '".$this->myarray[$k]."', ";				
				}		
				$sql = substr($sql,0,-2)." where leadID = ".$this->leadID.";";
				$rs = new movecompanion($sql);
				return true;
			}
			else
				return false;
		}
	}
?>