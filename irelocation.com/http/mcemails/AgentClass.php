<?
	class Agent
	{
		var $agentID;
		var $env;
		var $myarray;
		var $User;
		var $notes = array();
		var $thisclass = "Agent";
		
		function Agent($agentID="",$env="prd")
		{
			$this->env = $env;
			$this->myarray = array();
			
			if ($agentID != "")
			{
				$this->agentID = $agentID;
				$this->myarray['agentID'] = $agentID;
				$this->load();
			}
			else
				$this->agentID = 0;
				
			
			$this->myarray['agentID'] = $agentID;
		}	
		
		function loadNotes()
		{
			if ($this->agentID != 0)
			{
				$sql = "select noteID from notes_prd where noteScopeCode = 1032 and notRefID = ".$this->agentID.";";
				$rs = new movecompanion($sql);
				
				while ($rs->fetch_array())
					$notes[] = new Note($rs->myarray['noteID']);
					
				return count($notes);
			}
			else 
				return -1;
		}
		
		
		function add()
		{			
			if ($this->agentID == 0)
			{
				$keys = array_keys($this->myarray);
				$sql = "insert into movecomp.agents_".$this->env." set ";
				foreach($keys as $k)
				{
					if (!is_numeric($k) && $k != "agentID")
						$sql .= "$k = '".$this->myarray[$k]."', ";				
				}		
				$sql = substr($sql,0,-2)."; ";
				$rs = new movecompanion($sql);
				$this->agentID = $rs->last_insert_id();
				$this->myarray['agentID'] = $rs->last_insert_id();
				return true;
			}
			else return false;
		}
		
		function load()
		{
			if ($this->agentID > 0)
			{
				$sql = "select *  from movecomp.agents_".$this->env." where agentID = ".$this->agentID;
				$rs = new movecompanion($sql);
				if ($rs->fetch_array())
				{
					$this->myarray = $rs->myarray;			
					$User = new User($this->myarray['userID'],$this->env);
					$User->load();
					$this->User = $User;
					return true;
				}
				else 
					return false;
			}
			else
				return false;
		}
		
		function update()
		{
			if ($this->agentID > 0)
			{
				$keys = array_keys($this->myarray);
				$sql = "update movecomp.agents_".$this->env." set ";
				foreach($keys as $k)
				{
					if (!is_numeric($k))
						$sql .= "$k = '".$this->myarray[$k]."', ";				
				}		
				$sql = substr($sql,0,-2)." where agentID = ".$this->agentID.";";
				$rs = new movecompanion($sql);
				return true;
			}
			else
				return false;
		}
	}
?>