<?
	include_once "../inc_mysql.php";
	
	class MCObject
	{
		var $hash = array(
					"User" => array("User","userID","aclUser_"),
					"Agent" => array("Agent","agentID","agents_"),
					"Note" => array("Note","noteID","notes_"),
					"Task" => array("Task","task_id","tasks_"),
					"Referral" => array("Referral","refID","referrals_"),
					"Lead" => array("Lead","leadID","leads_"),
					"ClosingDetail" => array("ClosingDetail","closingID","closingDetails_")
			);	
		var $loaded = false;
		var $myarray;
		var	$env;
		var $primaryKey;
		var $type;
		var $tableName;
		
		function MCObject($type,$key="",$env="prd")
		{
			$this->myarray = array();
			$this->env = $env;
			$this->type = $type;
			$this->primaryKey = $this->hash[$type][1];
			$this->tableName = $this->hash[$type][2].$env;
			
						
			if ($key != "")
			{
				$this->myarray[$this->primaryKey] = $key;
				$this->load();				
			}
		}	
	
		function load()
		{
			if ($this->myarray[$this->primaryKey] <= 0)
				return false;
				
			$sql = "select * from movecomp.".$this->tableName." where ".$this->primaryKey." = "
					.$this->myarray[$this->primaryKey]."; ";
			$rs = new movecompanion($sql);
			if ($rs->fetch_array())
			{
				$this->myarray = $rs->myarray;
				
				$keys = array_keys($this->myarray);
				foreach($keys as $k)
				{
					if (!is_numeric($k))
						$this->keys[] = $k;					
				}
				$this->loaded = true;
				return true;
			}
			else
				return false;		
		}
		
		function update()
		{
			if ($this->myarray[$this->primaryKey] <= 0)
				return false;
				
			$sql = "update movecomp.".$this->tableName." set ";
			
			foreach($this->keys as $k)
			{
				$sql .= "$k = '".$this->myarray[$k]."', ";
			}
			$sql .= "where ".$this->primaryKey." = ".$this->myarray[$this->primaryKey].";";
			$sql = str_replace(", where"," where",$sql);
			echo $sql;
		}
		
		function loadFields()
		{
			$sql = " describe movecomp.".$this->tableName.";";
			$rs = new movecompanion($sql);
			$this->keys = array();
			while ($rs->fetch_array())
			{
				$this->keys[] = $rs->myarray['Field'];
			}
		}
		
		function add()
		{
			if ($this->myarray[$this->primaryKey] <= 0)
			{
				$sql = "insert into movecomp.".$this->tableName." set ";
				
				foreach($this->keys as $k)
				{
					$sql .= "$k = '".$this->myarray[$k]."', ";
				}
				$sql .= "where ".$this->primaryKey." = ".$this->myarray[$this->primaryKey].";";
				$sql = str_replace(", where"," where",$sql);
				echo $sql;
			}
			else
				$this->update();
		}
	}
	
	class Referral extends MCObject
	{
		var $Agent;
		var $Lead;
		var $User;
		
		function Referral($key="",$env="prd")
		{
			parent::MCObject("Referral",$key,$env);
			if ($this->loaded)
			{
				$this->User = new User($this->myarray['createUser'],$env);			
				$this->Lead = new Lead($this->myarray['leadID'],$env);
				$this->Agent = new Agent($this->myarray['agentID'],$env);			
			}
		}
	}
	
	class User extends MCObject
	{
		function User($key="",$env="prd")
		{
			parent::MCObject("User",$key,$env);			
		}		
	}
	
	class Lead extends MCObject
	{
		function Lead($key="",$env="prd")
		{
			parent::MCObject("Lead",$key,$env);			
		}		
	}

	class Agent extends MCObject
	{
		var $User;
		
		function Agent($key="",$env="prd")
		{
			parent::MCObject("Agent",$key,$env);
			if ($this->loaded)
				$this->User = new User($this->myarray['userID'],$env);			
		}		
	}
	
	class Note extends MCObject
	{
		function Note($key="",$env="prd")
		{
			parent::MCObject("Note",$key,$env);			
		}		
	}
	
	class Task extends MCObject
	{
		function Task($key="",$env="prd")
		{
			parent::MCObject("Task",$key,$env);			
		}		
	}
	
	class ClosingDetail extends MCObject
	{
		function ClosingDetail($key="",$env="prd")
		{
			parent::MCObject("ClosingDetail",$key,$env);			
		}		
	}
?>