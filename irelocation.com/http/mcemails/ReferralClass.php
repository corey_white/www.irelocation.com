<?
	class Referral
	{
		var $refID;
		var $env;
		var $myarray;
		
		var $Agent;
		var $Lead;
		
		function Referral($refID="",$env="prd")
		{
			if ($refID != "")
			{
				$this->refID = $refID;
				$this->load();
			}
			else
				$this->refID = 0;
				
			$this->env = $env;
			$this->myarray = array();
			$this->myarray['refID'] = $refID;
		}	
		
		function loadNotes()
		{
			if ($this->refID != 0)
			{
				$sql = "select noteID from notes_prd where noteScopeCode = 1034 and notRefID = ".$this->refID.";";
				$rs = new movecompanion($sql);
				
				while ($rs->fetch_array())
					$notes[] = new Note($rs->myarray['noteID']);
					
				return count($notes);
			}
			else 
				return -1;
		}
		
		function add()
		{			
			//this one is more complicated..
		}
		
		function load()
		{
			if ($this->refID > 0)
			{
				$sql = "select * movecomp.referrals_".$this->env." where refID = ".$this->refID;
				$rs = new movecompanion($sql);
				if ($rs->fetch_array())
				{
					$this->myarray = $rs->myarray;
					
					if ($this->myarray['leadID'] > 0)
						$this->Lead = new Lead($this->myarray['leadID'],$this->env);					
					if ($this->myarray['agentID'] > 0)
						$this->Agent = new Agent($this->myarray['agentID'],$this->env);					
						
					return true;
				}
				else 
					return false;
			}
			else 
				return false;
		}
		
		function update()
		{
			if ($this->refID > 0)
			{
				$keys = array_keys($this->myarray);
				$sql = "update movecomp.referrals_".$this->env." set ";
				foreach($keys as $k)
				{
					if (!is_numeric($k))
						$sql .= "$k = '".$this->myarray[$k]."', ";				
				}		
				$sql = substr($sql,0,-2)." where refID = ".$this->refID.";";
				$rs = new movecompanion($sql);
				return true;
			}
			else 
				return false;
		}
	}
?>