<?
	function sendAttachment($to,$subject,$body,$from,$f_name ,$filetype="application/pdf")
	{	
		$eol = "\n";
		
		
		$handle=fopen($f_name, 'rb');
		$f_contents=fread($handle, filesize($f_name));
		$f_contents=chunk_split(base64_encode($f_contents));    //Encode The Data For Transition using base64_encode();
		$f_type=filetype($f_name);
		
		fclose($handle);
		# To Email Address
		$emailaddress= $to;
		# Message Subject
		$emailsubject= $subject;
				  		
		
		# Common Headers
		$headers .= "From: ".$from."".$eol;
		$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">".$eol;
		$headers .= "X-Mailer: PHP v".phpversion().$eol;          // These two to help avoid spam-filters
		# Boundry for marking the split & Multitype Headers
		$mime_boundary=md5(time());
		$headers .= 'MIME-Version: 1.0'.$eol;
		$headers .= "Content-Type: multipart/related; boundary=\"".$mime_boundary."\"".$eol;
		$msg = "";
		
		# Attachment
		$msg .= "--".$mime_boundary.$eol;
		$msg .= "Content-Type: application/pdf; name=\"".$f_name."\"".$eol;  // sometimes i have to send MS Word, use 'msword' instead of 'pdf'
		$msg .= "Content-Transfer-Encoding: base64".$eol;
		$msg .= "Content-Disposition: attachment; filename=\"".$f_name."\"".$eol.$eol; // !! This line needs TWO end of lines !! IMPORTANT !!
		$msg .= $f_contents.$eol.$eol;
		# Setup for text OR html
		$msg .= "Content-Type: multipart/alternative".$eol;
		
		# Text Version
		$msg .= "--".$mime_boundary.$eol;
		$msg .= "Content-Type: text/plain; charset=iso-8859-1".$eol;
		$msg .= "Content-Transfer-Encoding: 8bit".$eol;
		$msg .= "This is a multi-part message in MIME format.".$eol;
		$msg .= "If you are reading this, please update your email-reading-software.".$eol.$eol;
		
		# HTML Version
		$msg .= "--".$mime_boundary.$eol;
		$msg .= "Content-Type: text/html; charset=iso-8859-1".$eol.$eol;
		$msg .= $body.$eol.$eol;
		
		# Finished
		$msg .= "--".$mime_boundary."--".$eol.$eol;  // finish with two eol's for better security. see Injection.
		
		# SEND THE EMAIL
		ini_set(sendmail_from,$from);  // the INI lines are to force the From Address to be used !
		  mail($emailaddress, $emailsubject, $msg, $headers);
		ini_restore(sendmail_from); 
	}
	
	$show_file_name = "Move Companion Referral Policy & Agreement 12_06.pdf";
	
	sendAttachment("david@irelocation.com","MoveComp Agreement Form",
					"Move Agreement Form","noreply@movecompanion.com",$show_file_name);
?>