<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for moving clients (bekins, sirva, wheaton, etc)
**********************************************************************
* Creation Date:  4/23/08 14:51 PM
**********************************************************************
* Modifications (Date/Who/Details):
	
	4/9/10 10:56 AM - This quotemailer is deprecated.  All leads are distributed from LeadExec.  But, we need this script to run to update the received column so the lead counts show up on our internal reporting tool.


**********************************************************************
*/

/*  WE DO NEED THIS TO RUN SO THAT THE RECEIVED FIELD IS UPDATED.  THIS IS EASIER THAN TRYING TO UPDATE ALL OF THE MULTITUDES OF SUBMIT CODE THAT WE HAVE. */

//include library functions
include_once "../inc_mysql.php";

$mytime = substr($mytime,0,10);//YYYYMMDDHH


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");
$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3 OR cat_id = 13)  and email not like '%randomkeystrokes@gmail.com%' and ready_to_send != 8 ";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{
	$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
	$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
	$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
	$neworigin_state=strtoupper($rs->myarray["origin_state"]);
	$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
	$newdestination_state=strtoupper($rs->myarray["destination_state"]);
	$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
	$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

	$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
	"phone_home='$newphone_home',".
	"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
	"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
	"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
	$body .= "UPDATE SQL $update_sql<br /><br />\n\n";
	$rs2=new mysql_recordset($update_sql);
}	



echo "Finished.<br />";

?>