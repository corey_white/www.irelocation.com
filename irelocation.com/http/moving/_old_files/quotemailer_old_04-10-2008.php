<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for moving clients (bekins, sirva, wheaton, etc)
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/4/08 09:39 AM - Rob - Changed the checkZipCode function to sendToLeadCheck and modified it so that it accounted for more lead distribution
	3/5/08 09:43 AM - Rob - Change the whole script so that the outputting is in a var, so it can be emailed to me.
	3/7/08 10:36 AM - Rob - Adding the 3+ bedroom condition for 1583 (East End Transfer & Storage - 6000)
	3/13/08 09:19 AM - Rob - Adding code so that that if the lead is in the moving_zip_codes table for 1583 (East End) but the # of bedrooms is less than 3, then it will still go to one of the smaller companies
	3/20/08  - Rob - Added Move Pros to sendToLeadCheck function.  Added 3 vars to the function's GLOBAL declaration in hopes that solves the issue for EE getting undesired leads.
	3/21/08  - Rob - Move-Pros is getting 1 BR leads and they shouldn't.  EE is still getting less than 3 BR leads, too.  Added more conditions to the default area of the switch in the function.  Looks like we should be making sure we exclude those companies that have special requirements so that they aren't sent the lead (IOW, if they fail their specific conditions, then they shouldn't be able to squeek by on the drop through to the ELSE
	
	3/24/08 10:54 AM - Rob - Updated the code that pre-determines whether 1583 and 1588 want that lead.
	
	3/25/08 11:20 AM - Rob - updated max daily limit for Wheaton
**********************************************************************
*/

	//include library functions
	include_once "../inc_mysql.php";
	include_once "../cronlib/.functions.php";
	include "CompanyClasses.php";
	include "FlukeFixer.php";
	updateCronStatus("moving","started");
	
	define(LIVE,true);//signal this IS a live quotemailer.
	
function getBedroomNum($lead_arr) { 
	// this function gets the comment area that is supposed to have the number of bedrooms and converts it into a number

	$comments = $lead_arr->myarray['comments'];
	$mycomments=$lead_arr->myarray["comments"];
	$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
	$details=explode("\n",$mycomments);
	$lead_arr->myarray["furnished_rooms"] = trim($details[2]);
	$furnished_rooms = $details[2];
	#echo "furnished_rooms = $furnished_rooms<br />";

	//-- check to see if we can figure out how many bed rooms
	//-- clear out var from last loop
	$no_of_bedrooms ="";
	if ( eregi("0 bedroom",$furnished_rooms) || eregi("zero bedroom",$furnished_rooms) || eregi("no bedroom",$furnished_rooms) || !eregi("bedroom",$furnished_rooms) || !$furnished_rooms) {
		$no_of_bedrooms = 0;
	} 
	if ( eregi("1 bedroom",$furnished_rooms) || eregi("one bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 1;
	} 
	if ( eregi("2 bedroom",$furnished_rooms) || eregi("two bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 2;
	} 
	if ( eregi("3 bedroom",$furnished_rooms) || eregi("three bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 3;
	} 
	if ( eregi("4 bedroom",$furnished_rooms) || eregi("four bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 4;
	} 
	if ( eregi("5 bedroom",$furnished_rooms) || eregi("five bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 5;
	} 

	return $no_of_bedrooms;

}

	//if ($_REQUEST['test'] != "true") exit(); 
	//echo "new code!";
	
	#
	# Delete Test Leads.
	#
	$sql="delete from movingdirectory.quotes where (lower(name) like '%joe debug%') or (lower(name) like '%testmark%') or (lower(name) like 'test%') or (lower(name) like '%joe customer%' or email = 'test@test.com')";
	$rs=new mysql_recordset($sql);
	

	#
	# Clean up entries
	#
	$mytime = date("YmdHis");
	$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)  and email not like '%randomkeystrokes@gmail.com%'";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array())
	{
		$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
		$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
		$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
		$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
		$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
		$neworigin_state=strtoupper($rs->myarray["origin_state"]);
		$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
		$newdestination_state=strtoupper($rs->myarray["destination_state"]);
		$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
		$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

		$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
		"phone_home='$newphone_home',".
		"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
		"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
		"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
		$body .= "UPDATE SQL $update_sql<br /><br />\n\n";
		$rs2=new mysql_recordset($update_sql);
	}	

	$mytime = substr($mytime,0,10);//YYYYMMDDHH
	//select all leads that are ready to go, and moving or internationals.
	$sql = "select * from movingdirectory.quotes where ready_to_send = 1 ".
			" AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";

	//not sure if we need this still..
	$fixer = new FlukeFixer();

	$rs = new mysql_recordset($sql);
	$count = 0;
	while ($rs->fetch_array())
	{				
		$count++;
		if (!$fixer->compare($rs->myarray))		
		{
			if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
			{
				$data = new MovingQuoteData($rs->myarray);
				$body .= "\n\n<p>----------------------------------------------------------------------------------------------------------------- \n----------------------------------------------------------------------------------------------------------------- \n-----------------------------------------------------------------------------------------------------------------\n</p> \n\n";
				$body .= "Processing Moving-Quote<br/>\n";
			}
				
			$comp_sql = getCompanySQL($rs->myarray["source"],$rs->myarray["cat_id"]);
			$body .= "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
			$leads_rs = new mysql_recordset($comp_sql);
			
			$lead_count = 0;
			
			$ozip = $rs->myarray['origin_zip'];
			$dzip = $rs->myarray['destination_zip'];		
			
			$wheaton_sent = "no"; 		// var flag that checks if the lead was sent to Wheaton
			$nationwide_sent = "no"; 	// var flag that checks if the lead was sent to Nationwide relcoation service
			$smallco_sent = "no"; 			// var flag that checks if the lead was sent to smaller companies that can't get Wheaton leads
			$eastend_sent = "no"; 		// var flag that checks if the lead was sent to East End Transfer & Storage
			
			$ee_can_get_it = "no";
			$mp_can_get_it = "no";
		
			while ($leads_rs->fetch_array()) { // this runs a loop on EACH lead_id and sends it to a function that checks whether they can get the lead	
				// test output of comments
				$body .= "<br /><br />Looping<br /><br />\n";
				$body .= "<br /><br />Looping<br /><br />\n";
				$body .= "<br /><br />Looping<br /><br />\n\n";
				$body .= "comments:: " . $rs->myarray['comments'] . "<br />\n";
				
				$body .= "leads_rs->myarray = <pre>" . print_r ($leads_rs->myarray) . "</pre><br><br>\n\n";
				
				// this finds out how many bedrooms.  
				$bed_num = getBedroomNum($rs);
				$body .= "\n<br /><br />Number of bedrooms found: $bed_num<br /><br />\n\n";
				
				if ( sendToLeadCheck($ozip,$leads_rs->myarray['lead_id'],$bed_num) ) { //-- this sends to the function that checks to see if the specific lead can get it
					$lead_count++;
					$data->process($leads_rs->myarray);
					$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
				}	
			}	
			$data->finishLead();//update ready_to_send = 2.						
			$body .= "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
		
		}
		else
		{
			$body .= "Quote ".$rs->myarray["quote_id"]." found to be a duplicate, skipping.";
			$sql = "update movingdirectory.quotes set received = concat('2097',substring(received,5)) ".
					" where quote_id = '".$rs->myarray["quote_id"]."'; ";
			$rsa = new mysql_recordset($sql);
		}
		
	}
	
	if ($count > 0)
	{
		$body .= $count." leads processed.";
		$bad	 = $fixer->getBadQuotes();
	
		if (count($bad)>0)
		{
			mail("rob@irelocation.com","Duplicate Moving Leads",print_r($bad,true)."SQL: ".$fixer->outputBadSql());	
			$body .= count($bad)." duplicate leads skipped.";
		}
		else $body .= "no dupes found.";
	}
	else
		$body .= "no leads processed.";
		
		
	/****
	 //-- NEW FUNCTIONALITY
	 This modification needs to accomplish the folliwing:
		- Wheaton is only to get 1000 leads - determined by its daily total.  
		- National gets the rest (~1500 leads).
		- There are some "little guys" that get long distance leads, based on a zip code table (marble.moving_zip_codes)
		- small moving companies = Smallco
		
		BUT
		
		If Wheaton and National cannot get the same leads, and cannot get any lead that goes to a little guy.
		
		A LEAD ONLY GOES OUT TO A TOTAL OF 3 CLIENTS:
		Bekins
		Sirva
		Wheaton OR National OR East End Transfer & Storage OR 'little guys'
		
		LEAD_IDs:
		Bekins - 1388
		Sirva - 1387
		Wheaton - 1386
		East End Transfer & Storage - 1583 comp_id - 6000
		nationwide relcoation service  - 1349  comp_id - 5421 
	 */	

/* New Function 
	This function looks at a SINGLE lead_id at a time and determine whether or not that lead_id can get the lead.
	This makes it rather dificult because we need to retain who is getting all the leads in aggregate, not singly.
	Yes, it's a programming challenge :o)
*/
	function sendToLeadCheck($zip,$lead_id,$bed_num) { // this test whether or not the company get the lead ot not
		global $body, $wheaton_sent, $smallco_sent, $nationwide_sent, $eastend_sent, $ee_can_get_it, $mp_can_get_it; //-- make sure we can use these vars anywhere
		
		$body .= "<hr>\n------------------------------------ <br>\nBegin Function<br>\n";
		$body .= "<p align=\"center\">NOW CHECKING LEAD_ID $lead_id AND ZIP CODE $zip</p>\n\n";
		
		$zip_in_db = "no";
		$currtime = date("Ymd");
		$campaign_month = date("Ym");
		
		//----------------------------------- BEGIN WHEATON TEMP CODE
		//-- THIS IS TEMP CODE to pull the monthly_goal for Wheaton from the movingdirectory.campaign table. SOON, we'll do this for everyone (that's why it's stuck in this function instead of outside it).
		
		$w_sql = "select leads_per_day from movingdirectory.campaign where lead_id = '1386' and month = '$campaign_month' ";
		$body .= "<p>Wheaton Daily Goal Pull :: $w_sql</p>\n";
		
		$rsw = new mysql_recordset($w_sql);
		
		$daily_arr = $rsw->fetch_array();		
		$wheaton_max_per_day = $rsw->myarray['leads_per_day'];
		
		$body .= "<p>wheaton_max_per_day = $wheaton_max_per_day</p>\n";
		
		//----------------------------------- END WHEATON TEMP CODE
	
		//-- Check to see if zip code is in the database
		$zsql = "select lead_id from marble.moving_zip_codes where zip = '$zip' and extra = 'long' "; //-- check to see if this zipcode is reserved for Smallco
		$body .= "zip code check for Nationwide/smallco = $zsql<br /><br />\n\n";
		$rsz = new mysql_recordset($zsql);
		$zip_count = $rsz->rowcount();
		
		$body .= "<p>ZIP CODE COUNT = $zip_count</p>\n\n";
		
		if ( $zip_count > 0 ) {
			$zip_in_db = "yes";
			
			//-- NOW...
			//-- need to determine if 1583 is going to want the lead before we test for them (if there are less than 3 bedrooms, 1583 won't want it)
			//---- thought process: if the lead_id found in marble.moving_zip_codes is 1583 and the number of bedrooms is < 3, then we need to flag it that 1583 can't get it.
			
			$zip_arr = $rsz->fetch_array();
			$zip_lead_id = $zip_arr->myarray['lead_id'];
			
			$body .= "<p>Lead_ID that was found (for $zip) in Zip Code Table: $zip_lead_id</p>\n\n";
			
			$body .= "<p>NOW CHECKING WHETHER 1583 OR 1588 WOULD WANT THE LEAD</p>\n\n";
			
			//-- predetermine if 1583 East End... can get the lead (they want 3 BR or more)
			//-- IF THE LEAD_ID IN THE marble.moving_zip_codes is 1583 and BR is 3 or more, then East End wants this
			if ( $zip_lead_id == 1583 && $bed_num >= 3 ) {
				$ee_can_get_it = "yes";
			} else {
				$ee_can_get_it = "no";
			}
			$body .= "<p align=\"center\">Is 1583 is allowed to get this lead (bedrooms >= 3)?  $ee_can_get_it</p>\n";

			//-- predetermine if 1588 Move-Pros can get the lead (they want 2 BR or more)
			//-- IF THE LEAD_ID IN THE marble.moving_zip_codes is 1588 and BR is 2 or more, then Move-Pros wants this
			if ( $zip_lead_id == 1588 && $bed_num >= 2 ) {
				$mp_can_get_it = "yes";
			} else {
				$mp_can_get_it = "no";
			}
			$body .= "<p align=\"center\">Is 1588 is allowed to get this lead (bedrooms >= 2)?  $mp_can_get_it</p> \n";
			$body .= "\n--------------------------------------------------------------------------------------------------------------------------------\n";
			$body .= "<p>lead_id in Zip Code Table: $zip_lead_id</p>\n";
			$body .= "<p>Number of Bedrooms: $bed_num</p>\n";
			$body .= "<p>ee_can_get_it = $ee_can_get_it\n</p>";
			$body .= "<p>mp_can_get_it = $mp_can_get_it\n</p>";
			$body .= "--------------------------------------------------------------------------------------------------------------------------------\n\n";
			
		}
		$body .= "Was zip code ($zip) found in marble.moving_zip_codes: $zip_in_db <br />\n\n";
		
		
		
		//-- SWITCH TO HANDLE LEAD_ID PROCESSING
		switch ( $lead_id ) {
			case "1387": // Sirva
			case "1388": // Bekins
				$body .= "Processing Lead ($lead_id) for Sirva or Bekins<br />\n";
				$body .= "Processing Lead ($lead_id) for Sirva or Bekins<br />\n";
				$body .= "Processing Lead ($lead_id) for Sirva or Bekins<br />\n\n";
				return true;
				break;


			case "1386": // Wheaton
				$body .= "Processing Lead ($lead_id) for Wheaton<br />\n";
				$body .= "Processing Lead ($lead_id) for Wheaton<br />\n";
				$body .= "Processing Lead ($lead_id) for Wheaton<br />\n\n";
				
				//-- find out how many leads Wheaton has received today
				$w_sql = "select * from movingdirectory.quotes where lead_ids like '%1386%' and left(received,8) = '$currtime' ";
					$body .= "w_sql = $w_sql<br />\n\n";
				$rs_w = new mysql_recordset($w_sql);
				$w_count = $rs_w->rowcount();
				$body .= "w_count = $w_count<br />\n\n";
				$body .= "will calculate: $w_count >= $wheaton_max_per_day<br />\n\n";
				
				if ( ($zip_in_db == "yes" || $w_count >= $wheaton_max_per_day) ) { //-- if the zip code is found in the table, OR Wheaton has hit daily max
				
					$wheaton_sent = "no";
					$body .= "<strong>Sent to Wheaton = $wheaton_sent</strong> :: zip_in_db = $zip_in_db :: w_count - $w_count<br />\n\n";
					return false;
					
				} else { //-- candidate to send to Wheaton
				
					if ( $smallco_sent == "no" && $nationwide_sent == "no" ) { //-- if not already sent this to Smallco or Nationwide, then we can send to Wheaton
						$wheaton_sent = "yes";
						$body .= "<strong>Sent to Wheaton = $wheaton_sent</strong> :: zip_in_db = $zip_in_db :: w_count - $w_count :: nationwide_sent = $nationwide_sent<br />\n\n";
						return true;
						
					} else { //-- already sent to Smallco or Nationwide
					
						$wheaton_sent = "no";
							$body .= "<strong>Sent to Wheaton = $wheaton_sent</strong> :: zip_in_db = $zip_in_db :: w_count - $w_count :: nationwide_sent = $nationwide_sent<br />\n\n";
						return false;
						
					}
				}
				
				break;

			case "1349": // Nationwide Relcoation Service (NRS) - we can send to 1349 if we haven't sent to Wheaton AND if the zip isn't in the marble.moving_zip_codes table.  NRS and Wheaton are to alternate who gets the lead and we're using the randomizing of the lead_ids when the are pulled for that function.
				$body .= "Processing Lead ($lead_id) for Nationwide relcoation service<br />\n";
				$body .= "Processing Lead ($lead_id) for Nationwide relcoation service<br />\n";
				$body .= "Processing Lead ($lead_id) for Nationwide relcoation service<br />\n\n";
				
				if ( $zip_in_db == "yes" ||  $wheaton_sent == "yes" ) { //-- zip code found in table OR already sent to Wheaton, don't send to Nationwide
					$body .= "zip code found in table or already sent to Wheaton, not sending to Nationwide<br />\n";
					$body .= "zip_in_db = $zip_in_db and wheaton_sent = $wheaton_sent <br />\n\n";
					$body .= "<strong>Sending to Nationwide relcoation service: $nationwide_sent</strong><br />\n\n";
					return false;
					
				} else { //-- not finding the zip in the table and not having sent to Wheaton, we can send to Nationwide relcoation service
					$nationwide_sent = "yes";
					$body .= "<strong>Sending to Nationwide relcoation service: $nationwide_sent</strong>  :: zip_in_db = $zip_in_db :: wheaton_sent - $wheaton_sent<br />\n\n";
					return true;
				}
				break;


			default: //-- if not one fo the lead ids above, then we check to see if we can send to one of the smaller companies
				
				$body .= "Processing Lead ($lead_id) for Everyone Else<br />\n";
				$body .= "Processing Lead ($lead_id) for Everyone Else<br />\n";
				$body .= "Processing Lead ($lead_id) for Everyone Else<br />\n\n";
				
				//-- check to see if the zip code matches to the lead_id
				$sql = " select * from marble.moving_zip_codes where lead_id = '$lead_id' and zip = '$zip' ";
				$body .= "<br />\nChecking to see if ZIP and LEAD_ID match in DB: $sql<br />\n";
				$rs = new mysql_recordset($sql);
				$small_count = $rs->rowcount();
				
				if ( $small_count > 0 && $wheaton_sent == "no" && $nationwide_sent == "no" ) { //-- if the zip and lead_id match in table AND not sent to Wheaton AND not sent to Nationwide
					
					//-- if the lead_id is 1583, we need to make sure that the bedrooms is 3 or more
					if ( $lead_id == 1583 && $ee_can_get_it == "yes" ) {
						$body .= "Processing Lead ($lead_id) for East End Transfer & Storage<br />\n\n";
						
						if ( $bed_num >= 3 ) { //-- Bedrooms 3 or more AND lead is East End Transfer & Storage (6000)
							$smallco_sent = "yes"; //-- set var that we have sent to Smallco
							$body .= "Found more than 3 bedrooms and not already sent, <strong>sending to East End</strong><br />\n";
							return true;
						} else {
							$body .= "doesn't have 3 bedrooms or more, <strong>not sent to East End</strong>.<br />\n";
							return false;
						}
					} elseif ( $lead_id == 1588 && $mp_can_get_it == "yes" ) {
						$body .= "Processing Lead ($lead_id) for MovePros<br />\n\n";
						
						if ( $bed_num >= 2 ) { //-- Bedrooms 2 or more AND lead is Move-Pros (6260)
							$smallco_sent = "yes"; //-- set var that we have sent to Smallco
							$body .= "Found more than 2 bedrooms and not already sent, <strong>sending to Move-Pros</strong><br />\n";
							return true;
						} else {
							$body .= "doesn't have 2 bedrooms or more, <strong>not sent to Move-Pros</strong>.<br />\n";
							return false;
						}
					
					} elseif ( $lead_id != 1583 && $lead_id != 1588 ) { //-- make sure we exclude those leads that have "special requirements"
					//-- 3-21-08 - I think the issue is here, that when it drops down to this point, we're not excluding the above conditions, so that what happens is that 1583 and 1588 end up getting the lead anyway.  Was a simple ELSE, changing this to an ELSEIF that excludes the above coditions.
					
						$smallco_sent = "yes"; //-- set var that we have sent to Smallco - shouldn't have to worry about 1583 or 1588 as their zip codes are in the table
						$body .= "<strong>smallco_sent = $smallco_sent</strong> :: small_count = $small_count :: wheaton_sent = $wheaton_sent :: nationwide_sent = $nationwide_sent<br /><br />\n\n";
						return true;
						
					} else {  //-- If we get to this point, we need to simply return false.
						return false;
					}
					
				} else { //-- zip code not found or already sent to Wheaton or Nationwide, don't send
					$smallco_sent = "no"; //-- set var that we have sent to Smallco
					$body .= "<strong>smallco_sent = $smallco_sent</strong> :: small_count = $small_count :: wheaton_sent = $wheaton_sent :: nationwide_sent = $nationwide_sent<br /><br />\n\n";
					return false;
				}

				break;
		}
	

	}

echo $body;

/* turn on for trouble shooting
*/
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r\n";
mail("rob@irelocation.com","QUOTEMAILER OUTPUT","$body","$headers");

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>