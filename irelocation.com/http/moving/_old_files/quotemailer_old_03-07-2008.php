<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for moving clients (bekins, sirva, wheaton, etc)
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/4/08 09:39 AM - Rob - Changed the checkZipCode function to sendToLeadCheck and modified it so that it accounted for more lead distribution
	3/5/08 09:43 AM - Rob - Change the whole script so that the outputting is in a var, so it can be emailed to me.
**********************************************************************
*/

	//include library functions
	include_once "../inc_mysql.php";
	include_once "../cronlib/.functions.php";
	include "CompanyClasses.php";
	include "FlukeFixer.php";
	updateCronStatus("moving","started");
	
	define(LIVE,true);//signal this IS a live quotemailer.

	//if ($_REQUEST['test'] != "true") exit(); 
	//echo "new code!";
	
	#
	# Delete Test Leads.
	#
	$sql="delete from movingdirectory.quotes where (lower(name) like '%joe debug%') or (lower(name) like '%testmark%') or (lower(name) like 'test%') or (lower(name) like '%joe customer%' or email = 'test@test.com')";
	$rs=new mysql_recordset($sql);
	

	#
	# Clean up entries
	#
	$mytime = date("YmdHis");
	$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)  and email not like '%randomkeystrokes@gmail.com%'";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array())
	{
		$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
		$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
		$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
		$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
		$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
		$neworigin_state=strtoupper($rs->myarray["origin_state"]);
		$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
		$newdestination_state=strtoupper($rs->myarray["destination_state"]);
		$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
		$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

		$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
		"phone_home='$newphone_home',".
		"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
		"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
		"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
		$body .= "UPDATE SQL $update_sql<br /><br />\n\n";
		$rs2=new mysql_recordset($update_sql);
	}	

	$mytime = substr($mytime,0,10);//YYYYMMDDHH
	//select all leads that are ready to go, and moving or internationals.
	$sql = "select * from movingdirectory.quotes where ready_to_send = 1 ".
			" AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";

	//not sure if we need this still..
	$fixer = new FlukeFixer();

	$rs = new mysql_recordset($sql);
	$count = 0;
	while ($rs->fetch_array())
	{				
		$count++;
		if (!$fixer->compare($rs->myarray))		
		{
			if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
			{
				$data = new MovingQuoteData($rs->myarray);
				$body .= "Processing Moving-Quote<br/>\n";
			}
				
			$comp_sql = getCompanySQL($rs->myarray["source"],$rs->myarray["cat_id"]);
			$body .= "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
			$leads_rs = new mysql_recordset($comp_sql);
			
			$lead_count = 0;
			
			$ozip = $rs->myarray['origin_zip'];
			$dzip = $rs->myarray['destination_zip'];		
			
			$wheaton_sent = "no"; // var flag that checks if the lead was sent to Wheaton
			$nationwide_sent = "no"; // var flag that checks if the lead was sent to Nationwide relcoation service
			$smallco_sent = "no"; // var flag that checks if the lead was sent to smaller companies that can't get Wheaton leads

		
			while ($leads_rs->fetch_array()) { // this runs a loop on EACH lead_id and sends it to a function that checks whether they can get the lead	
				// test output of comments
				$body .= "comments:: " . $leads_rs->myarray['comments'] . "<br />\n";
				
				if (sendToLeadCheck($ozip,$leads_rs->myarray['lead_id'])) { //-- this sends to the function that checks to see if the specific lead can get it
					$lead_count++;
					$data->process($leads_rs->myarray);
					$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
				}			
			}	
			$data->finishLead();//update ready_to_send = 2.						
			$body .= "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
		
		}
		else
		{
			$body .= "Quote ".$rs->myarray["quote_id"]." found to be a duplicate, skipping.";
			$sql = "update movingdirectory.quotes set received = concat('2097',substring(received,5)) ".
					" where quote_id = '".$rs->myarray["quote_id"]."'; ";
			$rsa = new mysql_recordset($sql);
		}
		
	}
	
	if ($count > 0)
	{
		$body .= $count." leads processed.";
		$bad	 = $fixer->getBadQuotes();
	
		if (count($bad)>0)
		{
			mail("david@irelocation.com","Duplicate Moving Leads",print_r($bad,true)."SQL: ".$fixer->outputBadSql());	
			$body .= count($bad)." duplicate leads skipped.";
		}
		else $body .= "no dupes found.";
	}
	else
		$body .= "no leads processed.";
		
		
	/****
	 //-- NEW FUNCTIONALITY
	 This modification needs to accomplish the folliwing:
		- Wheaton is only to get 1000 leads - determined by its daily total.  
		- National gets the rest (~1500 leads).
		- There are some "little guys" that get long distance leads, based on a zip code table (marble.moving_zip_codes)
		- small moving companies = Smallco
		
		BUT
		
		If Wheaton and National cannot get the same leads, and cannot get any lead that goes to a little guy.
		
		A LEAD ONLY GOES OUT TO A TOTAL OF 3 CLIENTS:
		Bekins
		Sirva
		Wheaton OR National OR 'little guys'
		
		LEAD_IDs:
		Bekins - 1388
		Sirva - 1387
		Wheaton - 1386
		nationwide relcoation service  - 1349  comp_id - 5421 
	 */	

/* New Function 
	This function looks at a SINGLE lead_id at a time and determine whether or not that lead_id can get the lead.
	This makes it rather dificult because we need to retain who is getting all the leads in aggregate, not singly.
	Yes, it's a programming challenge :o)
*/
	function sendToLeadCheck($zip,$lead_id) { // this test whether or not the company get the lead ot not
		global $body, $wheaton_sent, $smallco_sent, $nationwide_sent; //-- make sure we can use these vars anywhere
		
		$body .= "<hr>";
		$body .= "<p align=\"center\">NOW CHECKING LEAD_ID $lead_id AND ZIP CODE $zip</p>";
		
		$zip_in_db = "no";
		$currtime = date("Ymd");
		$days_in_month = date("t");
		$wheaton_max_per_day = round(1000 / $days_in_month);
			$body .= "days_in_month = $days_in_month<br />\n";
			$body .= "wheaton_max_per_day = $wheaton_max_per_day<br />\n";
		
		//-- Check to see if zip code is in the database
		$sql = "select * from marble.moving_zip_codes where zip = '$zip' and extra = 'long' "; //-- check to see if this zipcode is reserved for Smallco
			$body .= "zip code check for Nationwide/smallco = $sql<br /><br />\n\n";
		$rs = new mysql_recordset($sql);
		$zip_count = $rs->rowcount();
		
		if ( $zip_count > 0 ) {
			$zip_in_db = "yes";
		}
		$body .= "Was zip code ($zip) found in marble.moving_zip_codes: $zip_in_db <br />\n";
		
		//-- SWITCH TO HANDLE LEAD_ID PROCESSING
		switch ( $lead_id ) {
			case "1387": // Sirva
			case "1388": // Bekins
				$body .= "Processing Lead ($lead_id) for Sirva or Bekins<br />\n<br />\n";
				return true;
				break;


			case "1386": // Wheaton
				$body .= "Processing Lead ($lead_id) for Wheaton<br />\n<br />\n";
				
				//-- find out how many leads Wheaton has received today
				$w_sql = "select * from movingdirectory.quotes where lead_ids like '%1386%' and left(received,8) = '$currtime' ";
					$body .= "w_sql = $w_sql<br />\n<br />\n";
				$rs_w = new mysql_recordset($w_sql);
				$w_count = $rs_w->rowcount();
					$body .= "w_count = $w_count<br />\n<br />\n";
				
				if ( ($zip_in_db == "yes" || $w_count >= $wheaton_max_per_day) ) { //-- if the zip code is found in the table, OR Wheaton has hit daily max
				
					$wheaton_sent = "no";
						$body .= "Sent to Wheaton = $wheaton_sent :: zip_in_db = $zip_in_db :: w_count - $w_count<br />\n<br />\n";
					return false;
					
				} else { //-- candidate to send to Wheaton
				
					if ( $smallco_sent == "no" && $nationwide_sent == "no" ) { //-- if not already sent this to Smallco or Nationwide, then we can send to Wheaton
						$wheaton_sent = "yes";
						$body .= "Sent to Wheaton = $wheaton_sent :: zip_in_db = $zip_in_db :: w_count - $w_count :: nationwide_sent = $nationwide_sent<br />\n<br />\n";
						return true;
						
					} else { //-- already sent to Smallco or Nationwide
					
						$wheaton_sent = "no";
							$body .= "Sent to Wheaton = $wheaton_sent :: zip_in_db = $zip_in_db :: w_count - $w_count :: nationwide_sent = $nationwide_sent<br />\n<br />\n";
						return false;
						
					}
				}
				
				break;

			case "1349": // Nationwide relcoation service - we can send to 1349 if we haven't sent to Wheaton AND
								 // if the zip isn't in the marble.moving_zip_codes table
				$body .= "Processing Lead ($lead_id) for Nationwide relcoation service<br />\n<br />\n";
				
				if ( $zip_in_db == "yes" ||  $wheaton_sent == "yes" ) { //-- zip code found in table OR already sent to Wheaton, don't send to Nationwide
					$body .= "zip code found in table or already sent to Wheaton, not sending to Nationwide<br />\n";
					$body .= "zip_in_db = $zip_in_db and wheaton_sent = $wheaton_sent <br />\n<br />\n";
					return false;
					
				} else { //-- not finding the zip in the table and not having sent to Wheaton, we can send to Nationwide relcoation service
					$nationwide_sent = "yes";
					$body .= "Sending to Nationwide relcoation service: $nationwide_sent  :: zip_in_db = $zip_in_db :: wheaton_sent - $wheaton_sent<br />\n<br />\n";
					return true;
				}
				break;


			default: //-- if not one fo the lead ids above, then we check to see if we can send to one of the smaller companies
				
				$body .= "Processing Lead ($lead_id) for Everyone Else<br />\n<br />\n";
				
				//-- check to see if the zip code matches to the lead_id
				$sql = " select * from marble.moving_zip_codes where lead_id = '$lead_id' and zip = '$zip' ";
				$body .= "<br />\nChecking to see if ZIP and LEAD_ID match in DB: $sql<br />\n";
				$rs = new mysql_recordset($sql);
				$small_count = $rs->rowcount();
				
				if ( $small_count > 0 && $wheaton_sent == "no" && $nationwide_sent == "no" ) { 
					//-- if the zip and lead_id match in table AND not sent to Wheaton AND not sent to Nationwide
					$smallco_sent = "yes"; //-- set var that we have sent to Smallco
					$body .= "smallco_sent = $smallco_sent :: small_count = $small_count :: wheaton_sent = $wheaton_sent :: nationwide_sent = $nationwide_sent<br />\n<br />\n";
					return true;
				} else { //-- zip code not found, don't send
					$smallco_sent = "no"; //-- set var that we have sent to Smallco
					$body .= "smallco_sent = $smallco_sent :: small_count = $small_count :: wheaton_sent = $wheaton_sent :: nationwide_sent = $nationwide_sent<br />\n<br />\n";
					return false;
				}

				break;
		}
	

	}

echo $body;

/* turn on for trouble shooting
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r\n";
mail("rob@irelocation.com","QUOTEMAILER OUTPUT","$body","$headers");
*/

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>