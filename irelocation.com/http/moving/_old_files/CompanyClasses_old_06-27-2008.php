<?
	
	
	define(PIPE,"||");
	define(PIPE_LENGTH,2);
	define(maybe,(rand(0,100) > 50));//not being used, just for fun :P
	include_once "../nusoap/nusoap.php";
	include_once "../jtracker/jtracker.lib.php";
	include_once "../inc_mysql.php";
	include_once "save_response.php";	
	include_once "countryfunction.php";		
	include_once "waiting.php";
	include_once "thankyou_function.php";
	//This class has functions that are used in both auto and moving company lead formating.
	class GenericQuoteData
	{
		var $myarray;
		var $isLocal;
		
		function GenericQuoteData($thearray)
		{
			$this->myarray = $thearray;
			$this->isLocal = false;
		}
		
		function finishLead()
		{			
			$quote_id = $this->myarray["quote_id"];
			
			
			$mover_status = ", mover_status = 'na' ";
			
			$sql = "update movingdirectory.quotes set ready_to_send = 2 $mover_status where quote_id = $quote_id;";
			$rs = new mysql_recordset($sql);			
			if (LIVE)
				thankyouEmail($this->myarray);
		}
		
		//get a value from the array by name append codes to format it.
		function get($key,$url=0)
		{				
			if (substr_count($key,"url_") > 0)
			{
				$key = str_replace('url_','',$key);
				$url = 1;
			}
			if (substr_count($key,"xml_") > 0)
			{
				$key = str_replace('xml_','',$key);
				$url = 2;
			}
			
			if (strlen($this->myarray[$key]) > 0)
			{
				$v =  $this->myarray[$key];				
				if ($url == 1)
					return urlencode($v);
				else if ($url == 2)
					return $this->xmlentities($v);
				else return $v;
			}
			else			
				return "keyword '$key' not found<br/>";
		}
		//good ol xmlentities :P
		function xmlentities ( $string )
		{
		   return str_replace ( 
		   	array ( '&', '"', "'", '<', '>' ), 
			array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
			$string 
			);
		}

		//get destination zip code based on the city and state. simple.
		function getDestinationZip($city,$state)
		{
			$destination_zip="";
			if((trim($city)!='') && ($state!=''))
			{
				$nomore=0;
				$sql="select zip from movingdirectory.zip_codes where lower(city) like '%".strtolower(trim($city))."%' and state='".$state."'";
				$rs2=new mysql_recordset($sql);
				if($rs2->rowcount()>0)
				{
					$rs2->fetch_array();
					$destination_zip=$rs2->myarray["zip"];
				}
				else
				{
					$city_length=strlen(trim($city));
					$mycount=10;
	
					if($city_length<10)
						{$mycount=$city_length;}
	
					for($i=$mycount;$i>=3;$i--)
					{
						if($nomore==0)
						{
							for($c=0;$c<$city_length-$i+1;$c++)
							{
								if($nomore==0)
								{
									$city_like=substr(strtolower(trim($city)),$c,$i);
									$sql="select zip from movingdirectory.zip_codes where lower(city) like '%$city_like%' and state='".$state."'";
									$rs2=new mysql_recordset($sql);
									if($rs2->rowcount()>0)
									{
											$rs2->fetch_array();
											$destination_zip=$rs2->myarray["zip"];
											$nomore=1;
									}
								}
							}
						}
					}
				}
				if($destination_zip=="")
				{
					if(intval($city)>0)
					{
						$destination_zip=intval($city);
						$sql="select city,state from movingdirectory.zip_codes where zip='$destination_zip'";
						$rs2=new mysql_recordset($sql);
						$rs2->fetch_array();						
					}
				}
				return $destination_zip;
			}
			else if ($state != "")
			{
				$sql="select zip from movingdirectory.zip_codes where state = '$state' ";
				$rs2=new mysql_recordset($sql);
				$rs2->fetch_array();
				$destination_zip = $rs2->myarray["zip"];
				return $destination_zip;
			}
		}
		//find all the ||[variable]|| in the lead_body column in the lead_format table.
		function findAllOccurences($Haystack, $needle, $limit=0)
		{
		  $Positions = array();
		  $currentOffset = 0;
		  $count=0;
		  while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
		  {
		   $Positions[] = $pos;
		   $offset = $pos + strlen($needle);
		   $count++;
		  }
		  return $Positions;
		}
		//merge the data from this object and the lead_body string.
		function merge($type)
		{
			//if(DEBUG) echo "Merging Data<br/>\n";
			$success = 1;
			$offset = 0;
			$variables = array();
			$results = $this->findAllOccurences($this->lead_body,PIPE);
			for ($i = 0; $i < count($results); $i +=  2)
			{					
				$end = $results[$i+1]-$results[$i]+PIPE_LENGTH;
				$variable = substr($this->lead_body,$results[$i],$end);				
				$variable = str_replace(PIPE,"",$variable);
				$variables[] = $variable;				
			}
			$variables = array_unique($variables);
			foreach ($variables as $v)
			{
				if ($type == "xml")
					$find_variable = "xml_$v";
				else
					$find_variable = $v;
				$find_variable = str_replace("xml_xml_","xml_",$find_variable);
				$find_variable = str_replace("url_url_","url_",$find_variable);
				
				$value = $this->get($find_variable);
				if (substr_count($value,"keyword") > 0)
					$success = 0;
									
				$this->lead_body = str_replace("||$v||",$value,$this->lead_body);
			}
			return $success;
		}
		//update the lead id list as this lead has been sent to each company				
		function updateLeadIds($lead_id)
		{
			if (LIVE && !$this->isLocal)//only update if this is a live run of the quotemailer.
			{				
				$quote_id = $this->myarray["quote_id"];
				if(DEBUG) echo "$quote_id<br/>";
				if (strlen($quote_id) > 0)
				{
					$sql = "update movingdirectory.quotes set lead_ids = ".
					" CONCAT(lead_ids,'|',$lead_id), num_sent = num_sent+1 ".
					" where quote_id = $quote_id;";
					$rs = new mysql_recordset($sql);				
					$rs->close();
				}
			}
		}
		//validate the response from technologically up-to-date people
		//people who don't use email.
		function validateResponse($response,$company, $rsmyarray)
		{
			$company = trim($company);
			$response = trim($response);
			$quote_id = $rsmyarray["quote_id"];
			if (strlen($company) > 0 || strlen($response) > 0)
			{
				/*
				mail("rob@irelocation.com","validation 2",
					"Company: $company\n\n"."Response: $response \n\nQuoteID:".$quote_id);
				*/
				
			}
			else
			{
				return;//no data to check against.
			}
			if ($company == "movebekins")
			{
				saveResponse($rsmyarray["quote_id"],$response,"movebekins");
				if(substr_count(strtolower($response),"transfer failed")>0)
				{
					mail("code@irelocation.com,david@irelocation.com",
						"Bekins Post Failure",$response."\nLead Data: ".print_r($this->myarray,true));
					recordLeadFailure($this->myarray,
												1388,
												"moving",
												"movingdirectory.quotes");
				}				
			}
			else if ($company == "movewheaton")
			{
				saveResponse($rsmyarray["quote_id"],$response,"movewheaton");
		
				if (strpos(strtolower($response),"lead accepted") === FALSE)//give both of us errors..
				{	
					mail("code@irelocation.com","Wheaton XML Error", 
						$wheaton_xml."<br/><Br/>Response: $response"."\nLead Data: ".print_r($this->myarray,true) , 
						"From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$email."");
					recordLeadFailure($this->myarray,
									1386,
									"moving",
									"movingdirectory.quotes");
				}				
			}
			else if ($company == "movedirect")
			{		
				if (substr_count($response,"Created:") > 0)
				{
					$success = 1;
					$response = str_replace("Created:","",$response);
				}
				else
				{
					$success = 0;	
					
				}
				saveMoveDirectResult($rsmyarray["quote_id"],$rsmyarray["received"],$success,
							$response,$rsmyarray["cat_id"],$rsmyarray["source"]);
			}
			else if ($company == "granot")
			{
				mail("david@irelocation.com","Granot Response",$response);
			}
			else if ($company == "sirva")
			{					
				$sirva_result = saveSirvaResult($response,$rsmyarray);
				if($sirva_result == "-1")
				{
					mail("code@irelocation.com","Sirva Error Message", "$response\nLead Data: ".print_r($this->myarray,true)
						, "From: '1st Moving Directory' <no_reply@1stmovingdirectory.com>\nReply-to: ".$rsmyarray["email"]."");
						
					recordLeadFailure($this->myarray,
							1387,
							"moving",
							"movingdirectory.quotes");
				}				
			}
		}	
	}
	
	class MovingQuoteData extends GenericQuoteData
	{
		//assign the quote data to the object, form variables we will need.
		function MovingQuoteData($thearray)
		{
			parent::GenericQuoteData($thearray);
			
			$this->myarray["fullname"] = $this->myarray["name"];
			list($this->myarray["firstname"],$this->myarray["lastname"]) = split(" ",$this->myarray["name"],2);
			
			$r_year=substr($this->myarray["received"],0,4);
			$this->myarray["ryear"] = $r_year;
			
			$r_month=substr($this->myarray["received"],4,2);			
			$this->myarray["rmonth"] = $r_month;
			
			$r_day=substr($this->myarray["received"],6,2);
			$this->myarray["rday"] = $r_day;
			
			$r_hour=substr($this->myarray["received"],8,2);
			$r_min=substr($this->myarray["received"],10,2);
			$this->myarray["received_standard"] = "$r_month/$r_day/$r_year $r_hour:$r_min";
			
			$temp_year=substr($this->myarray["est_move_date"],0,4);
			$temp_month=substr($this->myarray["est_move_date"],5,2);
			$temp_day=substr($this->myarray["est_move_date"],8,2);
		
			list($this->myarray["emonth"],$this->myarray["eday"],$this->myarray["eyear"]) = 
			explode("-",date("m-d-Y",mktime(0,0,0,$temp_month,$temp_day,$temp_year)));
			
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);

			
			$this->myarray["phone_dashes"] = "$p_area-$p_prefix-$p_suffix";
			
			$p2_area=substr($this->myarray["phone_work"],0,3);
			$p2_prefix=substr($this->myarray["phone_work"],3,3);
			$p2_suffix=substr($this->myarray["phone_work"],6,4);
			$this->myarray["phone_work_dashes"] = "$p2_area-$p2_prefix-$p2_suffix";
			
			$this->myarray["destination_zip"] = $this->getDestinationZip($this->myarray["destination_city"], $this->myarray["destination_state"]);
						
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Type of Move: ','',$mycomments);
			$mycomments=str_replace('Type of Home: ','',$mycomments);
			$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
			$mycomments=str_replace('Elevator: ','',$mycomments);
			$mycomments=str_replace('Stairs: ','',$mycomments);
			$mycomments=str_replace('Comments/Unique Items: ','',$mycomments);
			if ($mycomments != "")
				$this->myarray["wheaton_comments"] = $mycomments;
			else
				$this->myarray["wheaton_comments"] = "no comments";
			$details=explode("\n",$mycomments);
			$this->myarray["type_of_move"] = trim($details[0]);
			$this->myarray["type_of_home"] = trim($details[1]);
			$this->myarray["furnished_rooms"] = trim($details[2]);
			$this->myarray["elevator"] = trim($details[3]);
			$this->myarray["stairs"] = trim($details[4]);
			
			//if its not set, set it to email.
			if ($this->myarray['contact'] != "email" && 
				$this->myarray['contact'] != "phone")
			{
				$this->myarray['contact'] = "email";
			}
			
			$this->myarray["mycomments"] = $details[6];
			$this->myarray["moving_comments"] = "<tr><td width='200'>Type of Move: </td><td>$details[0]</td></tr>
			<tr><td width='200'>Type of Home: </td><td>$details[1]</td></tr>
			<tr><td width='200'>Furnished Rooms: </td><td>$details[2]</td></tr>
			<tr><td width='200'>Elevator: </td><td>$details[3]</td></tr>
			<tr><td width='200'>Stairs: </td><td>$details[4]</td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			<tr><td width='200'>Comments/Unique Items: </td><td>$details[6]</td></tr>";
			$this->myarray['nationwide_comments'] = str_replace('\n','',$this->myarray["comments"]);		
		}
		
		//city's for bekins must be 20 characters or less.
		function getBekinsCodes()
		{
			$this->myarray["bekins_destination_city"] = substr($this->myarray["destination_city"],0,20);
			$this->myarray["bekins_origin_city"] = substr($this->myarray["origin_city"],0,20);
			if(DEBUG) echo "Cleaning Lead for Bekins<br/>\n";
		}

		//for sirva, they have unique variables...
		function getSirvaCodes()
		{
			if(DEBUG) echo "Running Sirva Code<Br/>";
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Type of Move: ','',$mycomments);
			$mycomments=str_replace('Type of Home: ','',$mycomments);
			$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
			$mycomments=str_replace('Elevator: ','',$mycomments);
			$mycomments=str_replace('Stairs: ','',$mycomments);
			$mycomments=str_replace('Comments/Unique Items: ','',$mycomments);
			$details=explode("\n",$mycomments);
			
			$details[1]=strtolower($details[1]);
			$details[2] = str_replace("1","one ",$details[2]);
			if(DEBUG) echo "Sirva Room Checking: ".$details[2]."<Br/>\n";
			
			$rooms=substr($details[2],0,4);
			if(DEBUG) echo "Sirva Room Checking: $rooms<Br/>\n";
			$small_move = false;
			switch($rooms)
			{
				case "no b":
					$small_move = true;
					if(DEBUG) echo "Small Move";
					if($details[1]=='house') $dwelling="H1";
					elseif($details[1]=='apartment') $dwelling="A1";
					elseif($details[1]=='condo') $dwelling="A1";
					else $dwelling="S2";		  
					break;
				case "one ":
					$small_move = true;
					if(DEBUG) echo "Small Move";
					if($details[1]=='house') $dwelling="H1";
					elseif($details[1]=='apartment') $dwelling="A1";
					elseif($details[1]=='condo') $dwelling="A1";
					else $dwelling="S2";
					break;
				case "two ":
					if($details[1]=='house') $dwelling="H2";
					elseif($details[1]=='apartment') $dwelling="A2";
					elseif($details[1]=='condo') $dwelling="A2";
					else $dwelling="S2";
					break;
				case "thre":
					if($details[1]=='house') $dwelling="H3";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
				case "four":
					if($details[1]=='house') $dwelling="H4";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
				default:
					if($details[1]=='house') $dwelling="H4";
					elseif($details[1]=='apartment') $dwelling="A3";
					elseif($details[1]=='condo') $dwelling="A3";
					else $dwelling="S2";
					break;
			}
			
			$this->myarray["sirva_dwelling"] = $dwelling;			
			$this->myarray["sirva_fcountry"] = get_country_code($this->myarray["origin_country"]);
			$this->myarray["sirva_dcountry"] = get_country_code($this->myarray["destination_country"]);
			$source = strtolower($this->myarray["source"]);
			
			if ($this->myarray["cat_id"] == 3 && (substr_count($source,"pm") > 0 || substr_count($source,"me") > 0))
				$lead_code = "PROMVG";
			else if ($source == 'pm_silver')
				$lead_code = "IRELTST";
			else if (substr_count($source,"me") >0 || substr_count($source,"pe") >0 )
				$lead_code = "PROMVG";	
			else
				$lead_code = "IRELOPH";
			$this->myarray["sirva_lead_code"] = $lead_code;
			if(DEBUG) echo "Sirva Code Done<br/>";
			
			
			//FIND ANOTHER WAY TO DO THIS...
			
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);
			
			if ($small_move && LIVE)
			{
				$small_moves_msg="Received: ".$this->myarray["received_standard"]."\n\nE-Mail Address: ".
				$this->myarray["email"]."\nMoving From Zip: ".$this->myarray["origin_zip"]."\nMoving To City: ".
				$this->myarray["destination_city"]."\nMoving To State: ".$this->myarray["destination_state"].
				"\nEstimated Move Date: ".$this->myarray["est_move_date"]."\n\nType of Move: ".$this->myarray["type_of_move"].
				"\nType of Home: ".$details[1]."\nFurnished Rooms: ".$this->myarray["furnished_rooms"]."\nElevator: ".
				$this->myarray["elevator"]."\nStairs: ".$this->myarray["stairs"]."\n\nFirst Name: ".
				$this->myarray["firstname"]."\nLast Name: ".$this->myarray["lastname"]."\nPreferred Contact Method: ".
				$this->myarray["contact"]."\nPhone Number: ($p_area) $p_prefix - $p_suffix"."\nMoving From City: ".
				$this->myarray["origin_city"]."\nMoving From State: ".$this->myarray["origin_state"].
				"\n\nComments/Unique Items: ".$details[6]."";		
				
				mail("webleads@minimoves.com","Sirva Small Move Lead",$small_moves_msg,
					"From: 'Irelocation' <no_reply@irelocation.com>");		
//					mail("david@irelocation.com","Sirva Small Move Lead",$small_moves_msg,
		//			"From: 'Irelocation' <no_reply@irelocation.com>");		
			}
			
		}
		//for Move Direct, they have unique variables...
		/*
		function getMoveDirectCodes()
		{
			extract($this->myarray);
		
			$phone[0] = substr($phone_home,0,3);		
			$phone[1] = substr($phone_home,3,3);
			$phone[2] = substr($phone_home,6,3);
			$this->myarray["user_phone"] = "user_phone1=".$phone[0]."&user_phone2=".$phone[1]."&user_phone3=".$phone[2];
			
			$altphone[0] = substr($phone_work,0,3);
			$altphone[1] = substr($phone_work,3,3);
			$altphone[2] = substr($phone_work,6,3);
			$this->myarray["user_altphone"] = "user_altphone1=".$altphone[0]."&user_altphone2=".
												$altphone[1]."&user_altphone3=".$altphone[2];
			
			$f = strpos($comments,"Furnished Rooms:");
			$b = strpos($comments,"bed",$f);
		
			$rooms = substr($comments,$f,$b-$f);
			$rooms = str_replace("Furnished Rooms","House",$rooms);
			$rooms = str_replace("Other","House",$rooms);
			$rooms = str_replace("Office","Condo",$rooms);
			$rooms = str_replace("no","1",$rooms);
			
			$rooms = str_replace(array("one","two","three","four","five","six"),array(1,2,3,4,5,6),$rooms);
			
			list($type,$count) = split(":",$rooms);
	
			if (strlen($type) == 0)
				$type = "House";
			
			if (strlen($count) == 0 || !is_numeric($count))
				$count = 3;
			
			$types["House"] = 540;
			$types["Apartment"] = 546;
			$types["Condo"] = 552;
			
			$this->myarray["dwelling_id"] = ($types[$type]+$count-1);					
		}
		*/
		
		// VANLINES
		function changeSize($insize)
		{
			$insize = strtolower($insize);
			if ($insize == "4+")
				$size = "4";
			else
				$size = substr($insize,0,strpos($insize,"bed"));
			//if(DEBUG) echo $insize." ".$size;
			
			switch ($size)
			{
				case "no":
					return "Partial Home 500-1000 lbs";
					break;
				case "":
					return "Studio 1500 lbs";
					break;
				case "1":
				case "one": 
					if (strlen($insize) > 20)
						return "1 BR Large 4000 lbs";
					else
						return "1 BR Small 3000 lbs";
				case "2":
				case "two":
					if (strlen($insize) > 20)
						return "2 BR Large 6500 lbs";
					else
						return "2 BR Small 4500 lbs";
				case "3":
				case "three":
					if (strlen($insize) > 23)
						return "3 BR Large 9000 lbs";
					else
						return "3 BR Small 8000 lbs";
				case "4":
				case "four":
					if (strlen($insize) > 23)
						return "4 BR Large 12000 lbs";
					else
						return "4 BR Small 10000 lbs";
				default:
					return "Over 12000 lbs";
			}
		}
		
		function getVanlinesCodes()
		{			
			//mail("david@irelocation.com","Vanlines Lead?",$this->myarray['quote_id']);
			$comments = $this->myarray['comments'];
			$startat = strpos($comments,"Furnished Rooms:")+16;
			if (substr_count($source,"smq") > 0)
				$size = $endat = strpos($comments,"Comments") - $startat;
			else
				$endat = strpos($comments,"Elevator") - $startat;
			if(DEBUG) echo $startat." ".$endat."<br/>";
			$size = substr($comments,$startat,$endat);
			$size = str_replace("\n","",$size);
			$size = str_replace(" ","",$size);
			$size = $this->changeSize($size);
			$this->myarray['vanlines_type'] = $size;
			//MM/DD/YYYY 
			list($year,$month,$day) = split("-",$this->myarray['est_move_date']);
			$this->myarray['vanlines_emd'] = $month."/".$day."/".$year;

		}
		
		//process this lead array, merge it and if successful, send it.
		function process($lead_array)
		{
			$this->lead_body = $lead_array["lead_body"];
			
			if ($lead_array["lead_id"] == 1387)
				$this->getSirvaCodes();		
			else if ($lead_array["lead_id"] == 1388)
				$this->getBekinsCodes();
			else if ($lead_array["lead_id"] == 1516)
				$this->getVanlinesCodes();
			//else if ($lead_array["lead_id"] == 1527)			
			//	$this->getMoveDirectCodes();			
			
			$success = $this->merge($lead_array["format_type"]);
			if ($success == 1)
			{
				//lead_id,lead_body,format_type,email
				if ($lead_array["format_type"] == "email")
				{
					
					list ($subject,$body) = split("\n",$this->lead_body,2);
					list($headers,$body) = split("---------------",$body,2);
					$header = trim($header);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = trim(implode("\n",$body_parts));
					$this->lead_body = $body;
					mail($lead_array["email"],$subject,trim($body),trim($headers));					
				}
				else if ($lead_array["format_type"] == "xml")// curl POST
				{													 
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
										
					$ch=curl_init(trim($url));
					
					foreach($variables as $v)
					{
						if (strlen(trim($v)) > 0)
						{
							list($k,$va) = split(":",$v,2);							
							
							if ($k == "validation")
								$validation = $va;
							else
							{																
								if ($k == "CURLOPT_HTTPHEADER")
								{
									if (substr_count(trim($va),",") > 0)
										$va = split(",",$va);
									curl_setopt($ch,CURLOPT_HTTPHEADER,$va);
								}
								else
									curl_setopt($ch, trim($k), trim($va));
							}
						}
					}
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, trim($body));
					
					$response = curl_exec($ch);
					curl_close($ch);
					$this->lead_body = $body;
					if(DEBUG) echo "Moving Quote Response:$response for $validation<br/>\n";
					//mail("rob@irelocation.com","validation","Company: $validation\n\n"."Response: $response");
					$this->validateResponse($response,$validation, $this->myarray);
					
/*
					mail("rob@irelocation.com","XML Moving Lead",
						"XML:".$body."\nQuote_id:".$this->myarray["quote_id"]."\nResponse:".$response);
*/
				}
				else if ($lead_array["format_type"] == "post")//faturl
				{
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = trim(substr($url,4));
					$variables = split("\n",$variables);
					$body = trim($body);
					
					$ch=curl_init($url."?".$body);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$response = curl_exec($ch);
					if(DEBUG) echo "Moving Quote Response:$response for $validation<br/>\n";
					curl_close($ch);
					$this->lead_body = $body;
					//mail("david@irelocation.com","validation","Company: $validation\n\n"."Response: $response");
					$this->validateResponse($response,$validation, $this->myarray);
					
					//mail("david@irelocation.com","Fat Url Moving Post","URL:".$url."?".$body."\n"."Response:".$response);					
				}
				
				$this->updateLeadIds($lead_array["lead_id"]);
			}
			else
				mail("rob@irelocation.com","Failed to Format Moving Lead",print_r($this->myarray,true)."\n\n".$this->lead_body);			
		}
		
	}
	
	

	class CarQuoteData extends GenericQuoteData
	{		
		//assign the quote data to the object, form variables we will need.
		function CarQuoteData($thearray)
		{
			parent::GenericQuoteData($thearray);
			$this->myarray["new_line"] = "\n";
			$this->myarray["fullname"] = $this->myarray["name"];
			list($this->myarray["firstname"],$this->myarray["lastname"]) = split(" ",$this->myarray["name"],2);
			
			$r_year=substr($this->myarray["received"],0,4);
			$this->myarray["ryear"] = $r_year;
			
			$r_month=substr($this->myarray["received"],4,2);			
			$this->myarray["rmonth"] = $r_month;
			
			$r_day=substr($this->myarray["received"],6,2);
			$this->myarray["rday"] = $r_day;
			
			$r_hour=substr($this->myarray["received"],8,2);
			$r_min=substr($this->myarray["received"],10,2);
			$this->myarray["received_standard"] = "$r_month/$r_day/$r_year $r_hour:$r_min";
			
			$temp_year=substr($this->myarray["est_move_date"],0,4);
			$temp_month=substr($this->myarray["est_move_date"],5,2);
			$temp_day=substr($this->myarray["est_move_date"],8,2);
		
			list($this->myarray["emonth"],$this->myarray["eday"],$this->myarray["eyear"]) = 
			explode("-",date("m-d-Y",mktime(0,0,0,$temp_month,$temp_day,$temp_year)));
			
			$p_area=substr($this->myarray["phone_home"],0,3);
			$p_prefix=substr($this->myarray["phone_home"],3,3);
			$p_suffix=substr($this->myarray["phone_home"],6,4);
			$this->myarray["p_area"] = $p_area;
			$this->myarray["p_prefix"] = $p_prefix;
			$this->myarray["p_suffix"] = $p_suffix;
			if(DEBUG) echo "$p_area-$p_prefix-$p_suffix<br/>";
			$this->myarray["phone_dashes"] = "$p_area-$p_prefix-$p_suffix";
			
			$this->myarray["running_yes_no"] = (!(substr_count(strtolower($this->myarray["comments"]) ,"non-running") > 0))?"Yes":"No";
			$this->myarray["running"] = (!(substr_count(strtolower($this->myarray["comments"]) ,"non-running") > 0))?"running":"not-running";
			if(DEBUG) echo "Not Running: ".(substr_count(strtolower($this->myarray["comments"]) ,"non-running"))."<Br/>";
			
			$this->myarray['running_carmovers'] = (!(substr_count(strtolower($this->myarray["comments"]) ,"non-running") > 0))?"Y":"N";
			$this->myarray['running_granot'] = (!(substr_count(strtolower($this->myarray["comments"]) ,"non-running") > 0))?"Y":"N";
			
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Vehicle Type: ','',$mycomments);
			$mycomments=str_replace('Vehicle Year: ','',$mycomments);
			$mycomments=str_replace('Vehicle Make: ','',$mycomments);
			$mycomments=str_replace('Vehicle Model: ','',$mycomments);
			$mycomments=str_replace('Comments: ','',$mycomments);
			$details=explode("\n",$mycomments);
			
			$this->myarray["type"] = $details[0];
						
			$this->myarray["year"] = $details[1];
			
			$this->myarray["make"] = $details[2];
			
			$this->myarray["model"] = $details[3];
			
			$this->myarray["customer_comments"] = trim($details[4]);
			if (strlen(trim($details[4])) == 0)
				$this->myarray["customer_comments"] = "no comments";
			list($t,$a) = split(":",$details[4],2);
			
			
			$this->myarray["destination_zip"] = 
				$this->getDestinationZip($this->myarray["destination_city"], $this->myarray["destination_state"]);
			
			if (strlen($this->myarray["destination_zip"]) != 5)
				$this->getDestinationZip("", $this->myarray["destination_state"]);//try again and just get one.
			
			$mycomments=$this->myarray["comments"];
			$mycomments=str_replace('Vehicle Type: ','',$mycomments);
			$mycomments=str_replace('Vehicle Year: ','',$mycomments);
			$mycomments=str_replace('Vehicle Make: ','',$mycomments);
			$mycomments=str_replace('Vehicle Model: ','',$mycomments);
			$mycomments=str_replace('Comments: ','',$mycomments);
			$details=explode("\n",$mycomments);
			$this->myarray["html_auto_comments"]="<tr><td width='200'>Vehicle Type: </td><td>$details[0]</td></tr>
			<tr><td width='200'>Vehicle Year: </td><td>$details[1]</td></tr>
			<tr><td width='200'>Vehicle Make: </td><td>$details[2]</td></tr>
			<tr><td width='200'>Vehicle Model: </td><td>$details[3]</td></tr>
			<tr><td width='200'>-----</td><td></td></tr>
			<tr><td width='200'>Comments: </td><td>$details[5]</td></tr>
			";			
			
			if($this->myarray['contact'] == "")
				$this->myarray['contact'] = "email";
			
			if($this->myarray["contact"]=='phone')
			{
			  $this->myarray["das_contact"]="Daytime Phone";
			}
			else
			{
			  $this->myarray["das_contact"]="Email";
			}
		}
		
		function getAAACodes()
		{
			$this->myarray["aaa_from"] = " - Auto-Transport Leads";		
		}
		
		function getPrimaryCodes()
		{
			$this->myarray["primary_email"] = "auto-transport-us.mfarnham@jttmail.com";
			$this->myarray["primary_subject"] = "ATUS lead";			
		}		
		
		function getNorthAmericanCodes()
		{
			$this->myarray["naat_from"] = "'1st Moving Directory - Auto-Transport Leads'";
		}
		function getAbbyCodes()
		{

			$this->myarray["abby_email"] = "auto-transport-us.mred1@jttmail.com";
			$this->myarray["abby_from"] = "'1st Moving Directory - Auto-Transport'";			
		}
		
		function getAutoShippersAmericaCodes()
		{
			
			$this->myarray["asa_site_title"] = "Auto-Transport.us Lead";
			$this->myarray["asa_partnerid"] = "ATUS";			
			$this->myarray["asa_email"] = $this->myarray["email"]."\n";
			$this->myarray["asa_origin_state"] = $this->myarray["origin_state"]."\n";
		}
		
		//process this lead array, merge it and if successful, send it.
		function process($lead_array)
		{
			if ($lead_array["lead_id"] == 2000)
				$this->myarray["granot_lead_email"] = $lead_array['email'];
			
			if(DEBUG) echo "Processing<Br/>";
			if ($lead_array["lead_id"] == 1503)
				$this->getPrimaryCodes();
			else if ($lead_array["lead_id"] == 1478)
				$this->getNorthAmericanCodes();
			else if ($lead_array["lead_id"] == 1433)
				$this->getAAACodes();
			else if ($lead_array["lead_id"] == 1406)//abby
				$this->getAbbyCodes();
			else if ($lead_array["lead_id"] == 1518)//asa
				$this->getAutoShippersAmericaCodes();
			if(DEBUG) echo "After Company Functions<Br/>";	
			$this->lead_body = $lead_array["lead_body"];
			$success = $this->merge($lead_array["format_type"]);
			if ($success == 1)
			{
				if(DEBUG) echo "Lead Merged to Format correctly - ".$lead_array["format_type"]."<Br/>";	
				//lead_id,lead_body,format_type,email
				if ($lead_array["format_type"] == "email")
				{					
					list ($subject,$body) = split("\n",$this->lead_body,2);

					if ($lead_array["lead_id"] == 1503)//primary code. 
						$lead_array["email"] = $this->myarray["primary_email"];
					else if ($lead_array["lead_id"] == 1406)//abby
						$lead_array["email"] = $this->myarray["abby_email"];
						
						
					list($headers,$body) = split("---------------",$body,2);
					$body = str_replace(array("\r\n","\r","\n"),"--NL--",$body);
					$body = substr($body,6);
					$body_parts = split("--NL--",$body);
					$body = implode("\n",$body_parts);
					$this->lead_body = $body;
					mail($lead_array["email"],$subject,trim($body),trim($headers));
					//mail($lead_array["email"].",david@irelocation.com",$subject,trim($body),trim($headers));
					if(DEBUG) echo "mailing lead.<Br/>";
					//mail("code@irelocation.com",$subject,trim($body),trim($headers));
				}
				else if ($lead_array["format_type"] == "jtxml")
				{					
					
					$leadObj = getLeadObject();
					if ($leadObj == "error")
					{
						mail("david@irelocation.com","JTracker XML Lead Failure",
							  $desc."\nLead id: ".$lead_array["lead_id"]."\nCampaign: ".
							  CAMPAIGN."\nData: \n".print_r($this->myarray,true));
					
						recordLeadFailure($this->myarray,$lead_array["lead_id"],
												CAMPAIGN,"marble.auto_quotes");					
					}
					else
					{
						list($rest,$comments)
							 = split("Comments",$this->myarray['comments'],2);
						list($run,$comments) = split(" - ",$comments);
						$this->myarray['customer_comments'] = $comments;
						$running = (strpos($this->myarray['comments'],"non-Running") == 0)?"r":"n";
						$this->myarray['running'] = $running;
						populateLeadObject($leadObj,$this->myarray,
											$lead_array["lead_id"],CAMPAIGN);
	
						$status = postLead($leadObj);
						if ($status != "error")
							list ($success,$desc) = processJTrackerResponse($status);
						else
							$success = 0;
							
						if ($success == 0)
						{
							mail("david@irelocation.com","JTracker XML Lead Failure",
								  $desc."\nLead id: ".$lead_array["lead_id"]."\nCampaign: ".
								  CAMPAIGN."\nData: \n".print_r($this->myarray,true));
						
							recordLeadFailure($this->myarray,
													$lead_array["lead_id"],
													CAMPAIGN,
													"movingdirectory.quotes");
						
						}
						else
						{
							echo "Valid JTracker XML Lead! ".
							$lead_array["lead_id"]." <br/>";
						}
					}	
				}
				else
				{
					list ($variables,$body) = split("---------------",$this->lead_body,2);
					list($url,$variables) = split("\n",$variables,2);
					$url = substr($url,4);
					if(DEBUG) echo "POST URL: $url<br/>";
					$variables = split("\n",$variables);
										
					$ch=curl_init(trim($url));					
					foreach($variables as $v)
					{
						if (strlen(trim($v)) > 0)
						{
							list($k,$va) = split(":",$v,2);							
							
							if ($k == "validation")
							{
								$validation = $va;
								if(DEBUG) echo "validation: $va<br/>";
							}
							else										
							{
								if ($k == "CURLOPT_HTTPHEADER")
								{
									if (substr_count(trim($va),",") > 0)
										$va = split(",",$va);
									else
										$va = array($va);
									curl_setopt($ch,CURLOPT_HTTPHEADER,$va);
								}
								else
								{
									curl_setopt($ch, trim($k), trim($va));
									if(DEBUG) echo "$k: $va<br/>";
								}
							}
						}
					}
					$body = trim($body);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
					if(DEBUG) echo "POST BODY: $body <Br/>";
					$this->lead_body = $body;
					$response = curl_exec($ch);
					curl_close($ch);
					
					$this->validateResponse($response,$validation, $this->myarray);
					//mail("david@irelocation.com",$subject,trim($body),trim($headers));		
				}
				
				$this->updateLeadIds($lead_array["lead_id"]);
			}
			else if ($lead_array["format_type"] == "post")//faturl
			{
				list ($variables,$body) = split("---------------",$this->lead_body,2);
				list($url,$variables) = split("\n",$variables,2);
				$url = trim(substr($url,4));
				$variables = split("\n",$variables);
				$body = trim($body);
				
				$ch=curl_init($url."?".$body);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$response = curl_exec($ch);
				curl_close($ch);
				$this->lead_body = $body;
				//mail("david@irelocation.com","validation","Company: $validation\n\n"."Response: $response");
				$this->validateResponse($response,$validation, $this->myarray);
				
				//mail("david@irelocation.com","Fat Url Auto Post","URL:".$url."?".$body."\n"."Response:".$response);					
			}
			else
			{
				mail("david@irelocation.com","Failed to Format Auto Lead",
				print_r($this->myarray,true)."\n\n".$this->lead_body);
			}
		}
	}

	//this builds the sql based on the quote data, cat_id and source.
	function getCompanySQL($source, $cat_id)
	{		
		if ($cat_id == 3)
		{
			$site_id = "int";
		}
		else if ($cat_id == 2)
		{		
			$site_id = "pm";
		}
		else if ($cat_id == 1)//SHOULD BE ONLY OTHER OPTION
		{
			mail("david@irelocation.com","quotemailer using bad company function","");
			exit();
		}
	
		return "select 	format_id, 
					(select email from movingdirectory.rules 
						where lead_id = lf.lead_id) as 'email',
					lf.lead_id,
					format_type,
					lf.cat_id, 
					if(lead_body < 0,
						(select lead_body from movingdirectory.lead_format 
							where lead_id = lf.lead_body),
						lf.lead_body) as 'lead_body' 
				from
					movingdirectory.lead_format as lf 
					join 
					movingdirectory.directleads as d
					on d.lead_id = lf.lead_id 
					join 
					movingdirectory.campaign as ca
					on d.lead_id = ca.lead_id	
				where	
					ca.active
					and lf.cat_id = $cat_id 
					and ca.month = '".date("Ym")."'
					and ca.site_id = '".$site_id."'
				group by	
					ca.lead_id
				order by rand();";			
	}
	
	function getCarCompanySQL($source)
	{					
		$company_list = grabNextList("atus");
		if(DEBUG) echo "Companies: $company_list<Br/>";
		return "select 	format_id, (select email from movingdirectory.rules where lead_id = lf.lead_id) as 'email', ".
				" lf.lead_id, format_type, lf.cat_id, ".
				"if(lead_body < 0,(select lead_body from movingdirectory.lead_format where lead_id = lf.lead_body),".
				"lf.lead_body) as 'lead_body' from 	movingdirectory.lead_format as lf where lf.lead_id in ($company_list)".
				" and lf.cat_id = '1' order by rand();";		
	}
	
	//save sirvas response.
	function saveSirvaResult($response,$rsmyarray)
	{
		$tempmove=explode("<MoveId>",$response);
		$newmove=explode("</MoveId>",$tempmove[1]);
		
		$sql="insert into movingdirectory.movesirva (received,move_id,".
				"quote_id,cat_id,source) values ('".$rsmyarray["received"].
		"','".$newmove[0]."','".$rsmyarray["quote_id"]."','".$rsmyarray["cat_id"]."','".$rsmyarray["source"]."')";
		$rs=new mysql_recordset($sql);
		
		return $newmove[0];
	}
	//save the response from move direct.
	function saveMoveDirectResult($quote_id,$received,$success,$response,$cat_id,$source)
	{
		$sql = "insert into movingdirectory.movedirect set quote_id = $quote_id, ".
				" received = $received, success = $success, cat_id = $cat_id, ".
				" source = '$source', record_key = '$response'";				
		//mail("david@irelocation.com","MoveDirect - ".(($success==1)?"Success":"Failure"),$sql);	
		$rs = new mysql_recordset($sql);
		$rs->close();			
	}
?>