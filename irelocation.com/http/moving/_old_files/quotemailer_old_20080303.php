<?
	//include library functions
	include_once "../inc_mysql.php";
	include_once "../cronlib/.functions.php";
	include "CompanyClasses.php";
	include "FlukeFixer.php";
	updateCronStatus("moving","started");
	
	define(LIVE,true);//signal this IS a live quotemailer.

	//if ($_REQUEST['test'] != "true") exit(); 
	//echo "new code!";
	
	#
	# Delete Test Leads.
	#
	$sql="delete from movingdirectory.quotes where (lower(name) like '%joe debug%') or (lower(name) like '%testmark%') or (lower(name) like 'test%') or (lower(name) like '%joe customer%' or email = 'test@test.com')";
	$rs=new mysql_recordset($sql);
	

	#
	# Clean up entries
	#
	$mytime = date("YmdHis");
	$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)  and email not like '%randomkeystrokes@gmail.com%' limit 10 ";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array())
	{
		$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
		$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
		$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
		$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
		$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
		$neworigin_state=strtoupper($rs->myarray["origin_state"]);
		$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
		$newdestination_state=strtoupper($rs->myarray["destination_state"]);
		$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
		$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

		$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
		"phone_home='$newphone_home',".
		"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
		"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
		"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
		echo $update_sql."<br/>";
		$rs2=new mysql_recordset($update_sql);
	}	

	$mytime = substr($mytime,0,10);//YYYYMMDDHH
	//select all leads that are ready to go, and moving or internationals.
	$sql = "select * from movingdirectory.quotes where ready_to_send = 1 ".
			" AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";

	//not sure if we need this still..
	$fixer = new FlukeFixer();

	$rs = new mysql_recordset($sql);
	$count = 0;
	while ($rs->fetch_array())
	{				
		$count++;
		if (!$fixer->compare($rs->myarray))		
		{
			if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
			{
				$data = new MovingQuoteData($rs->myarray);
				echo "Processing Moving-Quote<br/>\n";
			}
				
			$comp_sql = getCompanySQL($rs->myarray["source"],$rs->myarray["cat_id"]);
			echo "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
			$leads_rs = new mysql_recordset($comp_sql);
			
			$lead_count = 0;
			
			$ozip = $rs->myarray['origin_zip'];
			$dzip = $rs->myarray['destination_zip'];		

//echo "<br /><br />";
//$result = mysql_query($comp_sql);
//while($rowx = mysql_fetch_array($result)) {
//	echo "1::lead_id = " . $rowx['lead_id'] . "<br />";
//	echo "2::lead_id = " . $rowx[lead_id] . "<br />";
//}
//echo "<br /><br />";
			
			//echo "$dzip<br>";
			//echo "$ozip";
			
			while ($leads_rs->fetch_array())
			{					
				echo "lead_id = " . $leads_rs->myarray['lead_id'] . "<br />";
			
				if (checkZipCode($ozip,$leads_rs->myarray['lead_id']))    
				{
					$lead_count++;
					$data->process($leads_rs->myarray);
					echo "sent to " . $leads_rs->myarray['lead_id'] . "<br />";
				}			
			}	
			$data->finishLead();//update ready_to_send = 2.						
			echo "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
		
		}
		else
		{
			echo "Quote ".$rs->myarray["quote_id"]." found to be a duplicate, skipping.";
			$sql = "update movingdirectory.quotes set received = concat('2097',substring(received,5)) ".
					" where quote_id = '".$rs->myarray["quote_id"]."'; ";
			$rsa = new mysql_recordset($sql);
		}
		
	}
	
	if ($count > 0)
	{
		echo $count." leads processed.";
		$bad	 = $fixer->getBadQuotes();
	
		if (count($bad)>0)
		{
			mail("david@irelocation.com","Duplicate Moving Leads",print_r($bad,true)."SQL: ".$fixer->outputBadSql());	
			echo count($bad)." duplicate leads skipped.";
		}
		else echo "no dupes found.";
	}
	else
		echo "no leads processed.";
		
		
	/****
	 * This function first checks for the original three (wheetan, sirva, or bekins) 
	 * and then checks the database for affiliate wanting these zip codes.
	 * 
	 * @zip the zip code to look up
	 * @lead_id the lead_id of the company to send it to.
	 * 
	 * @returns true if found, false if not.
	 * 
	 * @author david haveman
	 * @created 2/27/2008 8:15pm
	 */	
	function checkZipCode($zip,$lead_id)
	{
		if ($lead_id == 1387 || $lead_id == 1386 || $lead_id == 1388) {
			return true;	
		} else  {
		 	$sql = " select * from marble.moving_zip_codes where lead_id = '$lead_id' and zip = '$zip' ";
		 	$rs = new mysql_recordset($sql);
			if ($rs->rowcount() > 0) {
			
				return true;//($rs->fetch_array());????
			} else {
				return false;
			}
		}
	}
	
	
	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>