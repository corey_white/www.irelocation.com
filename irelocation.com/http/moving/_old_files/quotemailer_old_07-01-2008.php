<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for moving clients (bekins, sirva, wheaton, etc)
**********************************************************************
* Creation Date:  4/23/08 14:51 PM
**********************************************************************
* Modifications (Date/Who/Details):
	April 23, 2008 2:51:25 PM GMT-07:00 - this is a brand new quotemailer for the moving quotes.  The first step for this is to build this new quotemailer as a testbed and see how well it handles live data (without sending anything out).
	
	5/7/08 - Rob - updated so that it did not look at zip codes that are for local moves only (like for Reebie)

**********************************************************************
*/

//include library functions
include_once "../inc_mysql.php";
include_once "../cronlib/.functions.php";
include "CompanyClasses.php";
include "FlukeFixer.php";
updateCronStatus("moving","started");

define(LIVE,true);//signal this IS a live quotemailer.

function getBedroomNum($lead_arr) { 
	// this function gets the comment area that is supposed to have the number of bedrooms and converts it into a number
	//-- this funciton has not been altered
	
	$comments = $lead_arr->myarray['comments'];
	$mycomments=$lead_arr->myarray["comments"];
	$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
	$details=explode("\n",$mycomments);
	$lead_arr->myarray["furnished_rooms"] = trim($details[2]);
	$furnished_rooms = $details[2];
	#echo "furnished_rooms = $furnished_rooms<br />";
	
	//-- check to see if we can figure out how many bed rooms
	//-- clear out var from last loop
	$no_of_bedrooms ="";
	if ( eregi("0 bedroom",$furnished_rooms) || eregi("zero bedroom",$furnished_rooms) || eregi("no bedroom",$furnished_rooms) || !eregi("bedroom",$furnished_rooms) || !$furnished_rooms) {
		$no_of_bedrooms = 0;
	} 
	if ( eregi("1 bedroom",$furnished_rooms) || eregi("one bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 1;
	} 
	if ( eregi("2 bedroom",$furnished_rooms) || eregi("two bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 2;
	} 
	if ( eregi("3 bedroom",$furnished_rooms) || eregi("three bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 3;
	} 
	if ( eregi("4 bedroom",$furnished_rooms) || eregi("four bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 4;
	} 
	if ( eregi("5 bedroom",$furnished_rooms) || eregi("five bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 5;
	} 
	
	return $no_of_bedrooms;

}

#
# Delete Test Leads.  //-- this funciton has not been altered
#
$sql="delete from movingdirectory.quotes where (lower(name) like '%joe debug%') or (lower(name) like '%testmark%') or (lower(name) like 'test%') or (lower(name) like '%joe customer%' or email = 'test@test.com')";
$rs=new mysql_recordset($sql);


#
# Clean up entries  //-- this funciton has not been altered
#
$mytime = date("YmdHis");
$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)  and email not like '%randomkeystrokes@gmail.com%'";
$rs=new mysql_recordset($sql);
while($rs->fetch_array())
{
	$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
	$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
	$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
	$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
	$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
	$neworigin_state=strtoupper($rs->myarray["origin_state"]);
	$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
	$newdestination_state=strtoupper($rs->myarray["destination_state"]);
	$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
	$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

	$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
	"phone_home='$newphone_home',".
	"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
	"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
	"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
	$body .= "UPDATE SQL $update_sql<br /><br />\n\n";
	$rs2=new mysql_recordset($update_sql);
}	


function sendToLeadCheck($zip,$lead_id,$bed_num,$cat_id) { // this test whether or not the company get the lead ot not
	// do we need this?

}


/*  begin processing */


$mytime = substr($mytime,0,10);//YYYYMMDDHH

//select all leads that are ready to go, and moving or internationals.
$sql = "select * from movingdirectory.quotes where ready_to_send = 1 AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";

// TEMP SQL PULL that replaces the above for testing
#$sql = "select * from movingdirectory.quotes where quote_id = 1753601 ";

//not sure if we need this still... //-- still not sure if we need this, but it doesn't look like it's doing any harm, so we'll leave it for now.
$fixer = new FlukeFixer();

$rs = new mysql_recordset($sql);
$count = 0;
while ($rs->fetch_array()) { //-- BEGIN LOOP OF QUOTES

	$count++;
	if (!$fixer->compare($rs->myarray)) { //-- this checks to see if the quote_id is a dup, if not, proceed
		
		$data = new MovingQuoteData($rs->myarray);  //-- insantiate the class
		$body .= "<br/><br/><br/><br/>\n\n\n\n";
		$body .= "=====================================================<br/>\n";
		$body .= "Processing Moving-Quote<br/><br/>\n\n";
		$body .= "=====================================================<br/>\n";
		
		/*-- process:
				1.		If it's an international quote, immediately send to Vanlines (1516)
				ELSE
				2.		run the pull_order = 1 SQL call to get the appropriate lead_id (company) for this quote_id (lead) for zips that aren't local
				3.		if we get a result, send the lead to that lead_id
				4.		if no lead_id is returned from above, then we run pull_order = 2 SQL:
					a.	if wheaton has not reached their daily max, then run SQL that includes 1386 (random pull)
					b. if wheaton has reached their daily max, then run SQL that excludes 1386 (random pull)
				5.		run the pull_order = 3 SQL to send to lead_ids that get every lead
		*/
		
		//-- $body .= "<br />XXXXX<br />\n";
		//-- $body .= "<br />\n\n";
		
		//-- define some vars based on the current quote_id
		$quote_id = $rs->myarray["quote_id"];
		$cat_id = $rs->myarray["cat_id"];
		$sent_to_leads = array();
		$campaign_month = date("Ym");
		$current_date = date("Ymd");
		$origin_zip = $rs->myarray['origin_zip'];
		$destination_zip = $rs->myarray['destination_zip'];
		$comments = $rs->myarray['comments'];
		$number_of_bedrooms = getBedroomNum($rs);
		$lead_count = 0;
		
		//-- output a little info
		$body .= "<br />quote_id = $quote_id<br />\n";
		$body .= "<br />cat_id = $cat_id<br />\n";
		$body .= "<br />campaign_month = $campaign_month<br />\n";
		$body .= "<br />origin_zip = $origin_zip<br />\n";
		$body .= "<br />destination_zip = $destination_zip<br />\n";
		$body .= "<br />number_of_bedrooms = $number_of_bedrooms<br />\n";
		$body .= "<br />lead_count = $lead_count<br />\n";
		$body .= "<br />\n\n";
		
		//-- output comments for this quote_id
		$body .= "<br />comments: $comments<br />\n";
		$body .= "<br />\n\n";
		
		//-- Calc Wheaton's daily quota
		$w_sql = "select leads_per_day from movingdirectory.campaign where lead_id = '1386' and month = '$campaign_month' ";
		$body .= "<br />Wheaton Daily Goal Pull :: $w_sql<br />\n";
		$rsw = new mysql_recordset($w_sql);
		$rsw->fetch_array();		
		$wheaton_max_per_day = $rsw->myarray['leads_per_day'];
		$body .= "<br />wheaton_max_per_day = $wheaton_max_per_day<br />\n";
		
		//-- Find out how many leads Wheaton has already received today
		$w_sql = "select * from movingdirectory.quotes where lead_ids like '%1386%' and left(received,8) = '$current_date' ";
		$rs_w = new mysql_recordset($w_sql);
		$w_count = $rs_w->rowcount();
		$body .= "<br />w_sql = $w_sql<br />\n";
		$body .= "<br />Wheaton Leads Sent so far today: $w_count<br />\n";
		$body .= "<br />\n\n";

		
		//-- determine the site_id from cat_id
		switch ( $cat_id ) {
			
			case "3": //-- this is an International quote, send this quote to 1516 (Vanlines)
				$body .= "<br />RUNNING CAT_ID 3 - International Moving Quote<br />\n";
				
				$site_id = "int";
				$body .= "<br />site_id = $site_id<br />\n";
				
				$comp_sql = "select 	format_id, ca.min_bedrooms, ca.pull_order, 
						(select email from movingdirectory.rules 
							where lead_id = lf.lead_id) as 'email',
						lf.lead_id,
						format_type,
						lf.cat_id, 
						if(lead_body < 0,
							(select lead_body from movingdirectory.lead_format 
								where lead_id = lf.lead_body),
							lf.lead_body) as 'lead_body' 
					from
						movingdirectory.lead_format as lf 
						join 
						movingdirectory.directleads as d
						on d.lead_id = lf.lead_id 
						join 
						movingdirectory.campaign as ca
						on d.lead_id = ca.lead_id	
	
					where	
						ca.active
						and lf.cat_id = 3 
						and ca.month = '$campaign_month'
						and ca.site_id = 'int'
					group by	
						ca.lead_id
					order by ca.pull_order desc";
				
					$body .= "<br /><br />COMPANY SQL::<br />$comp_sql<br />\n";
					$body .= "<br />\n\n";
					
					$leads_rs = new mysql_recordset($comp_sql);
					$leads_rs->fetch_array();
					
					$body .= "<br />leads_rs->myarray = <pre>" . print_r ($leads_rs->myarray,true) . "</pre><br><br>\n\n";
					
					$lead_count++;
					$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
					$body .= "<br />Sent to Processing (\$data->process)<br />\n";
					
					$data->finishLead(); //-- update ready_to_send = 2
					$body .= "<br />Sent to Finishing (\$data->procfinishLeadess)<br />\n";
					
					$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
					$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
					$body .= "<br /><br />\n";
				
				break;
			
			
			case "2": //-- process quote as normal
				$body .= "<br />RUNNING CAT_ID 2 - Standard Moving Quote<br />\n";
				
				$site_id = "pm";
				$body .= "<br />site_id = $site_id<br />\n";
				
				//-- pull_order 1
				$comp_sql = "select 	format_id, ca.min_bedrooms, ca.pull_order, 
							(select email from movingdirectory.rules 
								where lead_id = lf.lead_id) as 'email',
							lf.lead_id,
							format_type,
							lf.cat_id, 
							if(lead_body < 0,
								(select lead_body from movingdirectory.lead_format 
									where lead_id = lf.lead_body),
								lf.lead_body) as 'lead_body' 
						from
							movingdirectory.lead_format as lf 
							join 
							movingdirectory.directleads as d
							on d.lead_id = lf.lead_id 
							join 
							movingdirectory.campaign as ca
							on d.lead_id = ca.lead_id	
							join 
							marble.moving_zip_codes as mzp
							on mzp.lead_id = ca.lead_id
		
						where	
							ca.active
							and lf.cat_id = '$cat_id' 
							and ca.month = '$campaign_month'
							and ca.site_id = '$site_id'
							and ca.min_bedrooms <= '$number_of_bedrooms'
							and ca.pull_order = 1
							and mzp.zip = '$origin_zip'
							and mzp.extra != 'local'
						group by	
							ca.lead_id
						order by ca.pull_order desc";

						$body .= "<br /><br />PULL_ORDER 2 FIRST COMPANY SQL::<br />$comp_sql<br />\n";
						$body .= "<br />\n\n";
						
						$leads_rs = new mysql_recordset($comp_sql);
						$leads_rs->fetch_array();
						
						//-- did we get any returns on the first pull?
						$pull_1_num_rows = $leads_rs->rowcount();
						$body .= "<br />pull_1_num_rows (number of zip code companies returned - should be 1 or 0) = $pull_1_num_rows<br />\n";
						
						$body .= "<br />Quick SQL for Checking Lead_ID and Zip Code: &nbsp;&nbsp;&nbsp;
						select * from marble.moving_zip_codes where zip = '$origin_zip' 
						<br />\n";
						
						$body .= "<br />Checking to see if we found a zip code<br />\n";
						if ( $pull_1_num_rows > 0 ) { //-- process one of the companies that has a zip code in the table
							
							$body .= "<br />FOUND A ZIP CODE COMPANY<br />\n";
							$body .= "<br />leads_rs->myarray = <pre>" . print_r ($leads_rs->myarray,true) . "</pre><br><br>\n\n";
							
							$lead_count++;
							$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
							$body .= "<br />Sent to Processing (\$data->process)<br />\n";
							
							$data->finishLead(); //-- update ready_to_send = 2
							$body .= "<br />Sent to Finishing (\$data->procfinishLeadess)<br />\n";
							
							$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
							$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
							$body .= "<br /><br />\n";

						} else { //-- PROCESS A QUOTA COMPANY (like Wheaton and Nationwide)
							
							$body .= "<br />DID NOT FIND A ZIP CODE COMPANY<br />\n";
							$body .= "<br />NOW Processing for QUOTA COMPANIES<br />\n";
							
							if ( $w_count < $wheaton_max_per_day ) { //-- If wheaton has not received their quota of leads for today, include them in the SQL
								$body .= "<br />INCLUDING WHEATON in company pull<br />\n";
								
								$comp_sql = "select 	format_id, ca.pull_order, 
									(select email from movingdirectory.rules 
										where lead_id = lf.lead_id) as 'email',
									lf.lead_id,
									format_type,
									lf.cat_id, 
									if(lead_body < 0,
										(select lead_body from movingdirectory.lead_format 
											where lead_id = lf.lead_body),
										lf.lead_body) as 'lead_body' 
								from
									movingdirectory.lead_format as lf 
									join 
									movingdirectory.directleads as d
									on d.lead_id = lf.lead_id 
									join 
									movingdirectory.campaign as ca
									on d.lead_id = ca.lead_id	
				
								where	
									ca.active
									and lf.cat_id = 2 
									and ca.month = '$campaign_month'
									and ca.site_id = 'pm'
									and ca.pull_order = 2
								group by	
									ca.lead_id
								order by rand()
								
								limit 1";
								
							} else { //-- if they've gotten enough, exclude 1386
								$body .= "<br />NOT Including WHEATON in company pull<br />\n";
								
								$comp_sql = "select 	format_id, ca.pull_order, 
									(select email from movingdirectory.rules 
										where lead_id = lf.lead_id) as 'email',
									lf.lead_id,
									format_type,
									lf.cat_id, 
									if(lead_body < 0,
										(select lead_body from movingdirectory.lead_format 
											where lead_id = lf.lead_body),
										lf.lead_body) as 'lead_body' 
								from
									movingdirectory.lead_format as lf 
									join 
									movingdirectory.directleads as d
									on d.lead_id = lf.lead_id 
									join 
									movingdirectory.campaign as ca
									on d.lead_id = ca.lead_id	
				
								where	
									ca.active
									and lf.cat_id = 2 
									and ca.month = '$campaign_month'
									and ca.site_id = 'pm'
									and ca.pull_order = 2
									and lf.lead_id != 1386
								group by	
									ca.lead_id
								order by rand()
								
								limit 1";
								
							}
							
							$body .= "<br /><br />PULL_ORDER 2 SECOND COMPANY SQL::<br />$comp_sql<br />\n";
							$body .= "<br />\n\n";

							$leads_rs = new mysql_recordset($comp_sql);
							$leads_rs->fetch_array();
							
							$body .= "<br />leads_rs->myarray = <pre>" . print_r ($leads_rs->myarray,true) . "</pre><br><br>\n\n";
							
							$lead_count++;
							$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
							$body .= "<br />Sent to Processing (\$data->process)<br />\n";
							
							$data->finishLead(); //-- update ready_to_send = 2
							$body .= "<br />Sent to Finishing (\$data->procfinishLeadess)<br />\n";
							
							$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
							$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
							$body .= "<br /><br />\n";

						}
						

						//-- pull_order 3 companies (the ones that get all leads)
						$body .= "<br />NOW RUNNING PULL ORDER 3<br />\n";
						$comp_sql = "select 	format_id, ca.pull_order, 
									(select email from movingdirectory.rules 
										where lead_id = lf.lead_id) as 'email',
									lf.lead_id,
									format_type,
									lf.cat_id, 
									if(lead_body < 0,
										(select lead_body from movingdirectory.lead_format 
											where lead_id = lf.lead_body),
										lf.lead_body) as 'lead_body' 
								from
									movingdirectory.lead_format as lf 
									join 
									movingdirectory.directleads as d
									on d.lead_id = lf.lead_id 
									join 
									movingdirectory.campaign as ca
									on d.lead_id = ca.lead_id	
				
								where	
									ca.active
									and lf.cat_id = 2 
									and ca.month = '$campaign_month'
									and ca.site_id = 'pm'
									and ca.pull_order = 3
								group by	
									ca.lead_id";
						
# 									and mzp.extra != 'local'

						$leads_rs = new mysql_recordset($comp_sql);
						
						while ($leads_rs->fetch_array()) { //-- loop thru each of the returned 
							$body .= "<br />leads_rs->myarray = <pre>" . print_r ($leads_rs->myarray,true) . "</pre><br><br>\n\n";
							
							$lead_count++;
							$data->process($leads_rs->myarray); //-- send for processing the quote (send to lead_id)
							$body .= "<br />Sent to Processing (\$data->process)<br />\n";
							
							$data->finishLead(); //-- update ready_to_send = 2
							$body .= "<br />Sent to Finishing (\$data->procfinishLeadess)<br />\n";
							
							$sent_to_leads[$lead_count] = $leads_rs->myarray['lead_id'];
							$body .= "sent to " . $leads_rs->myarray['lead_id'] . "<br />\n";
							$body .= "<br /><br />\n";
				
						
						}
				
				break;
			
			
			default: //-- ERROR - should not reach this condition
				$body .= "<br />ERROR- site_id not 2 or 3 :: site_id = $site_id<br />\n";
				mail("rob@irelocation.com","ERROR in Moving Quotemailer","A cat_id that was not 2 nor 3 was reached, which should not have happened as the SQL call was only pulling 2 or 3 (quote id is $quote_id)","EXTRA_HEADERS");
				break;
		}
		
		
		
		
		
		$body .= "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
		foreach($sent_to_leads as $key => $value){
			$body .= "Lead: $key sent to: $value<br />\n";
		}

	
	} else { //-- looks like we found a dup 
		
		$body .= "Quote ".$rs->myarray["quote_id"]." found to be a duplicate, skipping.";
		$sql = "update movingdirectory.quotes set received = concat('2097',substring(received,5)) ".
				" where quote_id = '".$rs->myarray["quote_id"]."'; ";
		$rsa = new mysql_recordset($sql);
	}
	
	
} //-- end the while loop (needs to stay)


/* ===== we can leave this ===== */
if ($count > 0)
{
	$body .= $count." leads processed.";
	$bad	 = $fixer->getBadQuotes();

	if (count($bad)>0)
	{
		mail("rob@irelocation.com","Duplicate Moving Leads",print_r($bad,true)."SQL: ".$fixer->outputBadSql());	
		$body .= count($bad)." duplicate leads skipped.";
	}
	else $body .= "no dupes found.";
}
else
	$body .= "no leads processed.";
	
	

echo $body;

/* turn on for trouble shooting
$headers .= 'From: Moving Quotemailer <noreply@irelocation.com>' . "\r\n";
$mail_body = ereg_replace("<br />","",$body);
mail("rob@irelocation.com","**NEW** QUOTEMAILER OUTPUT","$mail_body","$headers");
*/

	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>