<?
/* 
************************FILE INFORMATION**********************
* File Name:  rob_test.php
**********************************************************************
* Description:  This is testing the output of the comment BLOB
**********************************************************************
* Creation Date:  3/5/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

// function
function getBedroomNum($lead_arr) {

	$comments = $lead_arr->myarray['comments'];
	
	$mycomments=$lead_arr->myarray["comments"];
	$mycomments=str_replace('Furnished Rooms: ','',$mycomments);
	
	$details=explode("\n",$mycomments);
	$lead_arr->myarray["furnished_rooms"] = trim($details[2]);
	
	$furnished_rooms = $details[2];
	
	echo "furnished_rooms = $furnished_rooms<br />";

	//-- check to see if we can figure out how many bed rooms
	//-- clear out var from last loop
	$no_of_bedrooms ="";

	if ( eregi("0 bedroom",$furnished_rooms) || eregi("zero bedroom",$furnished_rooms) || eregi("no bedroom",$furnished_rooms) || !eregi("bedroom",$furnished_rooms) || !$furnished_rooms) {
		$no_of_bedrooms = 0;
	} 
	if ( eregi("1 bedroom",$furnished_rooms) || eregi("one bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 1;
	} 
	if ( eregi("2 bedroom",$furnished_rooms) || eregi("two bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 2;
	} 
	if ( eregi("3 bedroom",$furnished_rooms) || eregi("three bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 3;
	} 
	if ( eregi("4 bedroom",$furnished_rooms) || eregi("four bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 4;
	} 
	if ( eregi("5 bedroom",$furnished_rooms) || eregi("five bedroom",$furnished_rooms) ) {
		$no_of_bedrooms = 5;
	} 

	return $no_of_bedrooms;

}

//include library functions
include_once "../inc_mysql.php";

$sql = "select * from movingdirectory.quotes order by rand() limit 50  ";

$rs = new mysql_recordset($sql);

#$data = $rs->fetch_array();
while ( $rs->fetch_array() ) {

	$bed_num = getBedroomNum($rs);
	echo "<br />$bed_num<br />";
	
}

/*
	echo "received = $received<br />";
	echo "name = $name<br />";
	echo "email = $email<br />";
	echo "comments = $comments<br />";
*/




?>