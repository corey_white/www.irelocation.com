<?
/* 
************************FILE INFORMATION**********************
* File Name:  quotemailer.php
**********************************************************************
* Description:  This script sends out the leads for moving clients (bekins, sirva, wheaton, etc)
**********************************************************************
* Creation Date:  Unknown
**********************************************************************
* Modifications (Date/Who/Details):
	3/4/08 09:39 AM - Rob - Changed the checkZipCode function to sendToLeadCheck and modified it so that it accounted for more lead distribution
**********************************************************************
*/

	//include library functions
	include_once "../inc_mysql.php";
	include_once "../cronlib/.functions.php";
	include "CompanyClasses.php";
	include "FlukeFixer.php";
	updateCronStatus("moving","started");
	
	define(LIVE,true);//signal this IS a live quotemailer.

	//if ($_REQUEST['test'] != "true") exit(); 
	//echo "new code!";
	
	#
	# Delete Test Leads.
	#
	$sql="delete from movingdirectory.quotes where (lower(name) like '%joe debug%') or (lower(name) like '%testmark%') or (lower(name) like 'test%') or (lower(name) like '%joe customer%' or email = 'test@test.com')";
	$rs=new mysql_recordset($sql);
	

	#
	# Clean up entries
	#
	$mytime = date("YmdHis");
	$sql="select * from movingdirectory.quotes where (received='' or received='0') and (cat_id = 2 OR cat_id = 3)  and email not like '%randomkeystrokes@gmail.com%'";
	$rs=new mysql_recordset($sql);
	while($rs->fetch_array())
	{
		$newname=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["name"])));
		$newemail=strtolower(preg_replace("/([^-_.@A-z0-9]|\\\)|[^A-z]$/i","",$rs->myarray["email"]));
		$newphone_home=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_home"]);
		$newphone_fax=preg_replace("/([^0-9]|\\\)/i",'',$rs->myarray["phone_fax"]);
		$neworigin_city=ucwords(strtolower(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["origin_city"])));
		$neworigin_state=strtoupper($rs->myarray["origin_state"]);
		$newdestination_city=ucwords(preg_replace("/^[\ ]|([^-\ A-z]|\\\)/i","",$rs->myarray["destination_city"]));
		$newdestination_state=strtoupper($rs->myarray["destination_state"]);
		$newkeyword=str_replace("'","",str_replace("+"," ",strtolower($rs->myarray["keyword"])));
		$newsource=str_replace("'","",strtolower($rs->myarray["source"]));

		$update_sql = "update movingdirectory.quotes set name='$newname',email='$newemail',".
		"phone_home='$newphone_home',".
		"phone_fax='$newphone_fax',origin_city='$neworigin_city',origin_state='$neworigin_state',received = '$mytime',".
		"destination_city='$newdestination_city',destination_state='$newdestination_state',keyword='$newkeyword',".
		"source='$newsource' where quote_id='".$rs->myarray["quote_id"]."'";
		echo $update_sql."<br/>";
		$rs2=new mysql_recordset($update_sql);
	}	

	$mytime = substr($mytime,0,10);//YYYYMMDDHH
	//select all leads that are ready to go, and moving or internationals.
	$sql = "select * from movingdirectory.quotes where ready_to_send = 1 ".
			" AND cat_id in (2,3) AND received like '$mytime%' AND source not like 'cs%'";

	//not sure if we need this still..
	$fixer = new FlukeFixer();

	$rs = new mysql_recordset($sql);
	$count = 0;
	while ($rs->fetch_array())
	{				
		$count++;
		if (!$fixer->compare($rs->myarray))		
		{
			if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
			{
				$data = new MovingQuoteData($rs->myarray);
				echo "Processing Moving-Quote<br/>\n";
			}
				
			$comp_sql = getCompanySQL($rs->myarray["source"],$rs->myarray["cat_id"]);
			echo "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
			$leads_rs = new mysql_recordset($comp_sql);
			
			$lead_count = 0;
			
			$ozip = $rs->myarray['origin_zip'];
			$dzip = $rs->myarray['destination_zip'];		
			
			$wheaton_sent = "no"; // var flag that checks if the lead was sent to Wheaton
			$smallco_sent = "no"; // var flag that checks if the lead was sent to smaller companies that can't get Wheaton leads

		
			while ($leads_rs->fetch_array()) { // this runs a loop on EACH lead_id and sends it to a function that checks whether they can get the lead	
							
				if (sendToLeadCheck($ozip,$leads_rs->myarray['lead_id'])) { //-- this sends to the function that checks to see if the specific lead can get it
					$lead_count++;
					$data->process($leads_rs->myarray);
					echo "sent to " . $leads_rs->myarray['lead_id'] . "<br />";
				}			
			}	
			$data->finishLead();//update ready_to_send = 2.						
			echo "Quote ".$rs->myarray["quote_id"]." sent to $lead_count Companies.<br/>\n";
		
		}
		else
		{
			echo "Quote ".$rs->myarray["quote_id"]." found to be a duplicate, skipping.";
			$sql = "update movingdirectory.quotes set received = concat('2097',substring(received,5)) ".
					" where quote_id = '".$rs->myarray["quote_id"]."'; ";
			$rsa = new mysql_recordset($sql);
		}
		
	}
	
	if ($count > 0)
	{
		echo $count." leads processed.";
		$bad	 = $fixer->getBadQuotes();
	
		if (count($bad)>0)
		{
			mail("david@irelocation.com","Duplicate Moving Leads",print_r($bad,true)."SQL: ".$fixer->outputBadSql());	
			echo count($bad)." duplicate leads skipped.";
		}
		else echo "no dupes found.";
	}
	else
		echo "no leads processed.";
		
		
	/****
	 //-- NEW FUNCTIONALITY
	 This modification needs to accomplish the folliwing:
		- Wheaton is only to get 1000 leads - determined by its daily total.  
		- National gets the rest (~1500 leads).
		- There are some "little guys" that get long distance leads, based on a zip code table (marble.moving_zip_codes)
		- small moving companies = Smallco
		
		BUT
		
		If Wheaton and National cannot get the same leads, and cannot get any lead that goes to a little guy.
		
		A LEAD ONLY GOES OUT TO A TOTAL OF 3 CLIENTS:
		Bekins
		Sirva
		Wheaton OR National OR 'little guys'
		
		LEAD_IDs:
		Bekins - 1388
		Sirva - 1387
		Wheaton - 1386
		nationwide relcoation service  - 1349  comp_id - 5421 
	 */	

/* New Function 
	This function looks at a SINGLE lead_id at a time and determine whether or not that lead_id can get the lead.
	This makes it rather dificult because we need to retain who is getting all the leads in aggregate, not singly.
	Yes, it's a programming challenge :o)
*/
	function sendToLeadCheck($zip,$lead_id) { // this test whether or not the company get the lead ot not
		global $wheaton_sent, $smallco_sent, $mytime; //-- make sure we can use these vars anywhere
		
		echo "<hr>";
		
		$currtime = date("Ymd");
		$days_in_month = date("t");
		$wheaton_max_per_day = 1000 / $days_in_month;
		
		echo "days_in_month = $days_in_month<br />";
		echo "wheaton_max_per_day = $wheaton_max_per_day<br />";
	
		if ($lead_id == 1387 || $lead_id == 1388) { // If Sirva (1387) or Bekins (1388), they always get the lead
			echo "Hit Sirva and Bekins<br /><br />";
			return true;
			
			
		} else { //-- if NOT Sirva (1387) or Bekins (1388), then we need to figure out which client will get the lead
					//-- It's either going to go to Wheaton, or one of the other 'little guys' - but not both
		
			if ($lead_id == 1386) { 	// IF Wheaton, we need to check to see if they have already received their daily total, and if not, 
													// send it to them IF the zipcode doesn't exist in the marble.moving_zip_codes table (cuz those need to go to Smallco)
			
				echo "Hit Wheaton<br /><br />";
				
				$w_sql = "select * from movingdirectory.quotes where lead_ids like '%1386%' and left(received,8) = '$currtime' ";
				echo "w_sql = $w_sql<br /><br />";
				$rs_w = new mysql_recordset($w_sql);
				$w_count = $rs_w->rowcount();
				echo "w_count = $w_count<br /><br />";
				
				$sql = "select * from marble.moving_zip_codes where zip = '$zip' and extra = 'long' "; //-- check to see if this zipcode is reserved for Smallco
				echo "sql = $sql<br /><br />";
				$rs = new mysql_recordset($sql);
				
				if ( ($rs->rowcount() > 0 || $w_count >= $wheaton_max_per_day) ) { //-- if the zip code is found in the table, OR Wheaton has hit daily max
					$wheaton_sent = "no";
					echo "wheaton_sent = $wheaton_sent<br /><br />";
					return false;
				} else { //-- candidate to send to Wheaton
					if ( $smallco_sent == "no" ) { //-- if we haven't already sent this to Smallco, then we can send to Wheaton
						$wheaton_sent = "yes";
						echo "wheaton_sent = $wheaton_sent<br /><br />";
						return true;
					} else { //-- already sent to Smallco
						$wheaton_sent = "no";
						echo "wheaton_sent = $wheaton_sent<br /><br />";
						return false;
					}
				}
				
			} else { // Nationwide and other small moving companies (Smallco) listed in the moving_zip_code table
			
				echo "Hit Nationwide - Smallco<br /><br />";
				
				//-- NOW!  if lead_id = 1349 (nationwide relcoation service), then they get any leads that aren't in the marble.moving_zip_codes table

				$sql = "select * from marble.moving_zip_codes where zip = '$zip' and extra = 'long' "; //-- check to see if this zipcode is reserved for Smallco
				echo "zip code check for Nationwide/smallco = $sql<br /><br />";
				$rs = new mysql_recordset($sql);
				
				if ( $rs->rowcount() > 0 ) { //-- zip code found in table, don't send to Nationwide
					echo "zip code found in table, not sending to Nationwide<br />";
					return false;
				} else { //-- check to see if we can send to Smallco
					$sql = " select * from marble.moving_zip_codes where lead_id = '$lead_id' and zip = '$zip' ";
					$rs = new mysql_recordset($sql);
					
					if ( $rs->rowcount() > 0 && $wheaton_sent == "no" ) { //-- if we find records in the above SQL, then send the lead
						$smallco_sent = "yes"; //-- set var that we have sent to Smallco
						echo "smallco_sent = $smallco_sent<br /><br />";
						return true;
					} else { //-- zip code not found, don't send
						$smallco_sent = "no"; //-- set var that we have sent to Smallco
						echo "smallco_sent = $smallco_sent<br /><br />";
						return false;
					}
				}
				
			}
		}
	}

/* OLD FUNCTION
	 * This function first checks for the original three (wheetan, sirva, or bekins) 
	 * and then checks the database for affiliate wanting these zip codes.
	 * 
	 * @zip the zip code to look up
	 * @lead_id the lead_id of the company to send it to.
	 * 
	 * @returns true if found, false if not.
	 * 
	 * @author david haveman
	 * @created 2/27/2008 8:15pm

	function checkZipCode($zip,$lead_id)
	{
		if ($lead_id == 1387 || $lead_id == 1386 || $lead_id == 1388) {
			return true;	
		} else  {
		 	$sql = " select * from marble.moving_zip_codes where lead_id = '$lead_id' and zip = '$zip' ";
		 	$rs = new mysql_recordset($sql);
			if ($rs->rowcount() > 0) {
			
				return true;//($rs->fetch_array());????
			} else {
				return false;
			}
		}
	}
*/


	updateCronStatus("moving","ended");
	checkCronStatus("security","moving");
	checkCronStatus("topautoleads","moving");
?>