<?
	include_once "../inc_mysql.php";
	include_once "../cronlib/.functions.php";
	include "CompanyClasses.php";
	
	define(LIVE,false);//signal this is a test/fake quotemailer.
				
	//in from url.
	$quote_id = $_REQUEST['quote_id'];
	$lead_id = $_REQUEST['lead_id'];

	if ($quote_id == "" || $lead_id == "")
	{
		echo "Usage: re_moving_quotemailer.php?lead_id=<strong>lead_id</strong>".
			"&quote_id=<strong>quote_id</strong>[,<strong>quote_id</strong>]*";
		exit();

	}
	
	//select the quotes.	
	$sql = "select * from movingdirectory.quotes where quote_id in ".
			" ($quote_id) ";
	//echo "Reset Code";
	//exit();
	$rs = new mysql_recordset($sql);
	$count = 0;
	
	//select sql to get format and transmit method...
	$comp_sql = "select format_id, 
	(select email from movingdirectory.rules 
		where lead_id = lf.lead_id) as 'email',
	lf.lead_id,
	format_type,
	lf.cat_id, 
	if(lead_body < 0,
		(select lead_body from movingdirectory.lead_format 
			where lead_id = lf.lead_body),
		lf.lead_body) as 'lead_body' 
from
	movingdirectory.lead_format as lf 
	join 
	movingdirectory.directleads as d
	on d.lead_id = lf.lead_id 
	join 
	movingdirectory.campaign as ca
	on d.lead_id = ca.lead_id	
where	
	ca.active
	and lf.cat_id = 2
	and ca.month = '".date("Ym")."'
	and ca.site_id = 'pm'
	and lf.lead_id in ($lead_id)
group by	
	ca.lead_id";
				
		echo "Company SQL:<Br/>\n".$comp_sql."<Br/>\n";
	
	$leads_rs = new mysql_recordset($comp_sql);		
	//loop over each company
	if ($leads_rs->fetch_array())
	{
		//loop over each lead.
		while ($rs->fetch_array())
		{				
			$count++;		
			//if its moving international, assign and send it.
			if ($rs->myarray["cat_id"] == 2 || $rs->myarray["cat_id"] == 3)
			{
				$data = new MovingQuoteData($rs->myarray);
				echo "Processing Moving-Quote<br/>\n";
			}
			$data->process($leads_rs->myarray);			
			//$data->finishLead();//update ready_to_send = 2.		
		}
		echo $count." leads processed.";
	}
?>