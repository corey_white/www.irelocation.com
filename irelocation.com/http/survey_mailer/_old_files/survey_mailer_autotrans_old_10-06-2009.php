<?php
/* 
************************FILE INFORMATION**********************
* File Name:  survey_mailer_autotrans.php
**********************************************************************
* Description:  Loops thru a list of emails and mails them the survey for the Moving leads
**********************************************************************
* Creation Date:  9/30/009
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************


NOT FINISHED
NOT FINISHED
NOT FINISHED
NOT FINISHED
NOT FINISHED
NOT FINISHED
NOT FINISHED
NOT FINISHED

*/

include_once('class.phpmailer.php');
include_once("../inc_mysql.php");

$daysago30 = date("Ymd",strtotime("-30 days"));
$daysago60 = date("Ymd",strtotime("-60 days"));

echo "daysago30 = $daysago30<br />";
echo "daysago60 = $daysago60<br />";


### RUN 60 DAY EMAILS FOR CARSHIPPING
### RUN 60 DAY EMAILS FOR CARSHIPPING
### RUN 60 DAY EMAILS FOR CARSHIPPING

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago60' and left(source,2) = 'cs' and num_sent > 0  ";

echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$to_email = "rob@irelocation.com";
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_carshipping60.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@carshipping.com";
	$mail->FromName = "CarShipping.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited CarShipping.com for a moving quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://CarShipping.com/survey?r=15&q=$quote_id
	
	This is a short survey about the online experience when shopping for a moving quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}


//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer CARSHIPPING 60 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";

### RUN 30 DAY EMAILS FOR CARSHIPPING
### RUN 30 DAY EMAILS FOR CARSHIPPING
### RUN 30 DAY EMAILS FOR CARSHIPPING

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago30' and left(source,2) = 'cs' and num_sent > 0  ";
echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$to_email = "rob@irelocation.com";
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_carshipping30.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@carshipping.com";
	$mail->FromName = "CarShipping.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited CarShipping.com for a moving quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://CarShipping.com/survey?r=15&q=$quote_id
	
	This is a short survey about the online experience when shopping for a moving quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}

//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer CARSHIPPING 30 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";
exit;
// ====================================================================================
// ====================================================================================
// ====================================================================================

### RUN 60 DAY EMAILS FOR TOPMOVING
### RUN 60 DAY EMAILS FOR TOPMOVING
### RUN 60 DAY EMAILS FOR TOPMOVING

$emails_to_send = "";

$sql = "select distinct email from movingdirectory.quotes where left(received,8) = '$daysago60' and left(source,2) = 'tm' and ready_to_send = 2  ";

echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {
	$emails_to_send .= $rs->myarray["email"] . ",";
}

echo "Emails after sql:<br />$emails_to_send";
echo "<br /><br />";



$emails_to_send = trim($emails_to_send,",");

# test emails
/*
echo "OVERRIDING EMAILS FOR TEST<br />";
#$emails_to_send = "rob@irelocation.com,mark@irelocation.com,toni@irelocation.com,anna@irelocation.com,belinda@irelocation.com,katrina@irelocation.com,vsmith@irelocation.com";
$emails_to_send = "rob@irelocation.com";
*/

echo "<p>Sending a survey to:</p>";

$emails = explode(",",$emails_to_send);

while ( list ( $key, $value ) = each ( $emails ) ) {
	$to_email = trim($value);
	
	echo "$to_email - ";
	
	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_topmoving15.html');
	
	$body    = eregi_replace("[\]",'',$body);
	
	$mail->From     = "no_reply@topmoving.com";
	$mail->FromName = "TopMoving.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited TopMoving.com for a moving quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://topmoving.com/survey?r=15
	
	This is a short survey about the online experience when shopping for a moving quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}
	
}

//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer TOPMOVING 60 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";

### RUN 30 DAY EMAILS FOR TOPMOVING
### RUN 30 DAY EMAILS FOR TOPMOVING
### RUN 30 DAY EMAILS FOR TOPMOVING

$emails_to_send = "";

$sql = "select distinct email from movingdirectory.quotes where left(received,8) = '$daysago30' and left(source,2) = 'tm' and ready_to_send = 2  ";
echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {
	$emails_to_send .= $rs->myarray["email"] . ",";
}

echo "Emails after sql:<br />$emails_to_send";
echo "<br /><br />";



$emails_to_send = trim($emails_to_send,",");

# test emails
/*
echo "OVERRIDING EMAILS FOR TEST<br />";
#$emails_to_send = "rob@irelocation.com,mark@irelocation.com,toni@irelocation.com,anna@irelocation.com,belinda@irelocation.com,katrina@irelocation.com,vsmith@irelocation.com";
$emails_to_send = "rob@irelocation.com";
*/

echo "<p>Sending a survey to:</p>";

$emails = explode(",",$emails_to_send);

while ( list ( $key, $value ) = each ( $emails ) ) {
	$to_email = trim($value);
	
	echo "$to_email - ";
	
	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_topmoving30.html');
	
	$body    = eregi_replace("[\]",'',$body);
	
	$mail->From     = "no_reply@topmoving.com";
	$mail->FromName = "TopMoving.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited TopMoving.com for a moving quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://topmoving.com/survey?r=30
	
	This is a short survey about the online experience when shopping for a moving quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}
	
}

//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer TOPMOVING 30 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";


?>