<?php
/* 
************************FILE INFORMATION**********************
* File Name:  survey_mailer_cs.php
**********************************************************************
* Description:  Loops thru a list of emails and mails them the survey
**********************************************************************
* Creation Date:  7/10/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once('class.phpmailer.php');


if ( $_POST['mail_submit'] ) { //-- parse emails and submit

	echo "<p>Sending a survey to:</p>";
    
    $emails = explode(",",$emails_to_send);
    
	while ( list ( $key, $value ) = each ( $emails ) ) {
		$to_email = trim($value);
		
		echo "$to_email - ";
		
		$mail    = new PHPMailer();
		
		$body    = $mail->getFile('cs_contents.html');
		
		$body    = eregi_replace("[\]",'',$body);
		
		$mail->From     = "no_reply@irelocation.com";
		$mail->FromName = "CarShipping.com";
		
		$mail->Subject = "Please Take Our Survey";
		
		$mail->AltBody = "Recently you visited CarShipping.com for a quote to transport your vehicle. We are interested in your feedback. Please take a minute or two to answer our survey. 
		
		Please go to http://carshipping.com/survey/survey_form.php
		
		This is a short survey about the online experience when shopping for an auto transporter. "; // Alt Text
		
		$mail->MsgHTML($body);
		
		$mail->AddAddress("$to_email");
		
		if(!$mail->Send()) {
		  echo "Failed to send mail <br />";
		} else {
		  echo "Mail sent <br />";
		}
		
	}
    
} else { //-- display text box for email input
 ?>
 
<form action="survey_mailer_cs.php" method="post">

<fieldset>
<legend><b>Emails</legend>
<p>Separate emails with a comma and no spaces.</p>
<textarea name="emails_to_send" rows="20" cols="50"></textarea>

</fieldset>

<fieldset>
<legend><b>Mail 'em</b></legend>
<input name="mail_submit" type="submit" value="Submit">
</fieldset>
</form>
    
<?    
}


?>