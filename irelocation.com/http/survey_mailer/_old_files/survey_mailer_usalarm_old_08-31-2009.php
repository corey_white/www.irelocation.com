<?php
/* 
************************FILE INFORMATION**********************
* File Name:  survey_mailer_mmc.php
**********************************************************************
* Description:  Loops thru a list of emails and mails them the survey
**********************************************************************
* Creation Date:  7/10/08
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************
*/

include_once('class.phpmailer.php');
include_once("../inc_mysql.php");

$daysago30 = date("Ymd",strtotime("-30 days"));
$daysago15 = date("Ymd",strtotime("-15 days"));

echo "daysago30 = $daysago30<br />";
echo "daysago15 = $daysago15<br />";

$sql = "select distinct email from irelocation.leads_security where ( left(received,8) = '$daysago30' || left(received,8) = '$daysago15' ) and campaign = 'usalarm' and ready_to_send = 3  ";
echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);
$rs->fetch_array();

while ($rs->fetch_array()) {
	$emails_to_send .= $rs->myarray["email"] . ",";
}

#echo "Emails after sql:<br />$emails_to_send";
#echo "<br /><br />";



$emails_to_send = trim($emails_to_send,",");

# test emails
#$emails_to_send = "rob@irelocation.com,mark@irelocation.com,toni@irelocation.com,anna@irelocation.com,belinda@irelocation.com,katrina@irelocation.com,vsmith@irelocation.com";

echo "<p>Sending a survey to:</p>";

$emails = explode(",",$emails_to_send);

while ( list ( $key, $value ) = each ( $emails ) ) {
	$to_email = trim($value);
	
	echo "$to_email - ";
	
	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_contents.html');
	
	$body    = eregi_replace("[\]",'',$body);
	
	$mail->From     = "no_reply@USAlarmCompanies.com";
	$mail->FromName = "USAlarmCompanies.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited USAlarmCompanies.com for a quote to transport your vehicle. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://usalarmcompanies.com/survey
	
	This is a short survey about the online experience when shopping for a security alarm system. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}
	
}


?>