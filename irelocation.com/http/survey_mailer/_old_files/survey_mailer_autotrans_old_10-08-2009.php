<?php
/* 
************************FILE INFORMATION**********************
* File Name:  survey_mailer_autotrans.php
**********************************************************************
* Description:  Loops thru a list of emails and mails them the survey for the Moving leads
**********************************************************************
* Creation Date:  9/30/009
**********************************************************************
* Modifications (Date/Who/Details):

**********************************************************************

*/

include_once('class.phpmailer.php');
include_once("../inc_mysql.php");

$daysago30 = date("Ymd",strtotime("-30 days"));
$daysago60 = date("Ymd",strtotime("-60 days"));

echo "daysago30 = $daysago30<br />";
echo "daysago60 = $daysago60<br />";


### RUN 60 DAY EMAILS FOR CARSHIPPING
### RUN 60 DAY EMAILS FOR CARSHIPPING
### RUN 60 DAY EMAILS FOR CARSHIPPING

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago60' and left(source,2) = 'cs' and num_sent > 0  ";

echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$emails_to_send .= "$to_email \n";
	
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_carshipping60.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@carshipping.com";
	$mail->FromName = "CarShipping.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited CarShipping.com for a auto transportation quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://CarShipping.com/survey?r=60&q=$quote_id
	
	This is a short survey about the online experience when shopping for a auto transportation quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}


//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer CARSHIPPING 60 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";

### RUN 30 DAY EMAILS FOR CARSHIPPING
### RUN 30 DAY EMAILS FOR CARSHIPPING
### RUN 30 DAY EMAILS FOR CARSHIPPING

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago30' and left(source,2) = 'cs' and num_sent > 0  ";
echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$emails_to_send .= "$to_email \n";
	
	$emails_to_send .= "$to_email \n";
	
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_carshipping30.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@carshipping.com";
	$mail->FromName = "CarShipping.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited CarShipping.com for a auto transportation quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://CarShipping.com/survey?r=30&q=$quote_id
	
	This is a short survey about the online experience when shopping for a auto transportation quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}

//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer CARSHIPPING 30 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";

// ==================================================================================== MMC
// ==================================================================================== MMC
// ==================================================================================== MMC

### RUN 60 DAY EMAILS FOR MMC
### RUN 60 DAY EMAILS FOR MMC
### RUN 60 DAY EMAILS FOR MMC

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago60' and left(source,3) = 'mmc' and num_sent > 0  ";

echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$emails_to_send .= "$to_email \n";
	
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_mmc_60.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@movemycar.com";
	$mail->FromName = "MoveMyCar.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited MoveMyCar.com for a auto transportation quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://MoveMyCar.com/survey?r=60&q=$quote_id
	
	This is a short survey about the online experience when shopping for auto transportation quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}


//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer MoveMyCar 60 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";

### RUN 30 DAY EMAILS FOR MMC
### RUN 30 DAY EMAILS FOR MMC
### RUN 30 DAY EMAILS FOR MMC

$emails_to_send = "";

$sql = "select distinct email, quote_id from marble.auto_quotes where left(received,8) = '$daysago30' and left(source,3) = 'mmc' and num_sent > 0  ";
echo "$sql<br /><br />";

$rs = new mysql_recordset($sql);

while ($rs->fetch_array()) {

	$to_email = $rs->myarray["email"];
	$emails_to_send .= "$to_email \n";
	
	$quote_id = $rs->myarray["quote_id"];
	
	echo "$to_email - ";
	echo "$quote_id - ";

	$mail    = new PHPMailer();
	
	$body    = $mail->getFile('email_mmc_30.html');
	
	$body    = eregi_replace("[\]","",$body);
	$body    = eregi_replace("##quote_id##","$quote_id",$body);
	
	$mail->From     = "no_reply@MoveMyCar.com";
	$mail->FromName = "MoveMyCar.com";
	
	$mail->Subject = "Please Take Our Survey";
	
	$mail->AltBody = "Recently you visited MoveMyCar.com for a auto transportation quote. We are interested in your feedback. Please take a minute or two to answer our survey. 
	
	Please go to http://MoveMyCar.com/survey?r=30&q=$quote_id
	
	This is a short survey about the online experience when shopping for a auto transportation quote. "; // Alt Text
	
	$mail->MsgHTML($body);
	
	$mail->AddAddress("$to_email");
	
	if(!$mail->Send()) {
	  echo "Failed to send mail <br />";
	} else {
	  echo "Mail sent <br />";
	}

}

//-- let me know that the quotemailer was activated
mail("rob@irelocation.com","Survey Mailer MoveMyCar 30 DAY Ran","Here are the emails that were processed:\n\n$emails_to_send");

echo "<hr />";


?>