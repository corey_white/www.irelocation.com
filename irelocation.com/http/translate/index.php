<? 
	include "../inc_mysql.php";
	
	session_start();
	extract($_POST); 

	$english = str_replace("'","\'",trim($english));
	$french = str_replace("'","\'",trim($french));
	$site = trim($site);
	$page = trim($page);
	$label = trim($label);

	if ($site != "")
		$_SESSION['sites'][] = $site;
	if ($page != "")
		$_SESSION['pages'][] = $page;
		
	
	
	if ($page != "" && $site != "" && $label != "" && $french != "" && $english != "")
	{
		$sql = "insert into `marble`.`translation_buffer` 
	(`site`, `page`, `label`, `value`,`lang`)
	values
	('$site', '$page', '$label', '$english','en'),
	('$site', '$page', '$label', '$french','fr')";
		$rs = new mysql_recordset($sql);
		$id = $rs->last_insert_id();
	}
?>
<script>
	function copyTo(field,value)
	{
		document.forms.xml[field].value = value;
	}

</script>
<body>
	<form method="post" action="index.php" name="xml">
		<input type="hidden" name="posted" value="yes" />
		<table>
			<tr>
				<td valign="top">Site:</td>
				<td valign="top">
					<input type="text" name="site" value="<?= $site ?>" size="50"/>
				<? if (is_array($_SESSION['sites'])) 
				{				
					$fields = array_unique($_SESSION['sites']);	
					sort($fields);
					echo "<br/><select name='site-dd' onBlur=\"copyTo('site',this.value)\">";
					echo "<option value=''>Previous...</option>\n";					
					foreach($fields as $s)
						echo "<option value='$s'>$s</option>\n";					
					echo "</select>";
				} ?>				
				</td>
			</tr>
			<tr>
				<td valign="top">Page:</td>
				<td valign="top"><input type="text" name="page" value="<?= $page ?>" size="30"/>
				<? if (is_array($_SESSION['pages'])) {	
					$fields = array_unique($_SESSION['pages']);	
					sort($fields);
					echo "<br/><select name='page-dd' onBlur=\"copyTo('page',this.value)\">";
					echo "<option value=''>Previous...</option>\n";			
					foreach($fields as $s)
						echo "<option value='$s'>$s</option>\n";					
					echo "</select>";
				} ?>	
				</td>
			</tr>
			<tr>
				<td>Label:</td>
				<td><input type="text" name="label" value="<?= $label ?>"/></td>
			</tr>
			<tr>
				<td>Text:</td>
				<td><textarea name="english" rows='7' cols="40"></textarea></td>
			</tr>
			<tr>
				<td>Translated:</td>
				<td><textarea name="french" rows='7' cols="40"></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input type="submit" value="Save entry"></td>
			</tr>	
			<? if ($id != "") { ?>
			<tr>
				<td colspan='2'>Record #<?= $id ?> Saved.</td>
			</tr>
			<? } ?>
		</table>
	</form>
</body>
</html>
