<?
	include "../inc_mysql.php";
	$sql = "select * from marble.translation_buffer order by lang";
	
	$data = array();
	
	$rs = new mysql_recordset($sql);
	header("Content-Type: text/xml");
	echo "<langfile lang='fr'>\n";
	while($rs->fetch_array())
	{
		$data[$rs->myarray['lang']][] = $rs->myarray;
		if ($rs->myarray['lang'] == "fr")
		{
			echo printXMLRow($rs->myarray);		
		}
	}
	echo "</langfile>";
	
	function printXMLRow($array)
	{
		$keys = array_keys($array);	
		$xml = "<entry>\n";
		
		foreach($keys as $k)
		{
			if (is_numeric($k)) continue;
			$xml .= "\t<$k><![CDATA[".$array[$k]."]]></$k>\n";
		}
		$xml .= "</entry>\n";		
		return $xml;	
	
	}
?>