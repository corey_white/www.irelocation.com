<?
include_once("inc_mysql.php");

$max_send=2; //maximum number of companies to send a lead to

# get all the new leads
$rs=new mysql_recordset("select * from leads_mortgage where sent_to=''");
while($rs->fetch_array())
{
	# check to make sure the lead is not now processing from another cron process
	$rs2=new mysql_recordset("select * from leads_mortgage where id='".$rs->myarray["id"]."' and sent_to=''");
	if($rs2->rowcount()=='1')
	{
		# set this lead to processing
		$rs2=new mysql_recordset("update leads_mortgage set sent_to='processing' where id='".$rs->myarray["id"]."'");	
		
		# get all the active campaigns
		$rs2=new mysql_recordset("select * from campaigns where active='true' and mortgage_campaign='true' and mortgage_active='true' order by rand()");
		while($rs2->fetch_array() && $count!=$max_send)
		{
			# check to see if there is any criteria set for the campaign
			if(trim($rs2->myarray["mortgage_criteria"])!='')
			{
				$rs3=new mysql_recordset("select * from leads_mortgage where id='".$rs->myarray["id"]."' and ".$rs2->myarray["mortgage_criteria"]."");
				if($rs3->rowcount()=='1')
				{
					$send_the_lead=true;
				}
			}
			else
			{
				$send_the_lead=true;
			}
			
			if($send_the_lead==true)
			{
				$count++;
				if(substr($rs2->myarray["mortgage_delivery"],0,6)=='custom')
				{
					# load the custom delivery script
					list($custom,$include_file)=explode(":",$rs2->myarray["mortgage_delivery"]);
					//include("/home/httpd/vhosts/irelocation.com/custom/".$include_file);
					include("custom/$include_file");
				}
				else
				{
					# mail the lead to the customer
					$r_year=substr($rs->myarray["received"],0,4);
					$r_month=substr($rs->myarray["received"],4,2);
					$r_day=substr($rs->myarray["received"],6,2);
					$r_hour=substr($rs->myarray["received"],8,2);
					$r_min=substr($rs->myarray["received"],10,2);
					$text="Received: $r_month/$r_day/$r_year $r_hour:$r_min\n\nFirst Name: ".$rs->myarray["fname"]."\nLast Name: ".$rs->myarray["lname"]."\nE-Mail: ".$rs->myarray["email"]."\nPhone Number: ".$rs->myarray["phone"]."\nAlternate Phone: ".$rs->myarray["phone2"]."\n\nInterest: ".$rs->myarray["interest"]."\nProperty Value: ".$rs->myarray["property_value"]."\nLoan Amount: ".$rs->myarray["loan_amount"]."\nCredit Status: ".$rs->myarray["credit"]."\nState: ".$rs->myarray["state"]."\n\nComments: ".$rs->myarray["comments"];
					if($rs->myarray["campaign"]=="irelo_irelo_br")
					{
						mail($rs2->myarray["mortgage_delivery"],"Mortgage Lead - BR","$text","From: The iRelocation Network <no_reply@irelocation.com>\nReply-to: ".$rs->myarray["email"]."");
						mail("adrian@irelocation.com","Mortgage Lead - BR","$text","From: The iRelocation Network <no_reply@irelocation.com>\nReply-to: ".$rs->myarray["email"]."");
					}
					else
					{
						mail($rs2->myarray["mortgage_delivery"],"Mortgage Lead","$text","From: The iRelocation Network <no_reply@irelocation.com>\nReply-to: ".$rs->myarray["email"]."");
						mail("adrian@irelocation.com","Mortgage Lead","$text","From: The iRelocation Network <no_reply@irelocation.com>\nReply-to: ".$rs->myarray["email"]."");
					}
				}
				
				# update the sent_to field
				if($count==1)
				{
					$rs3=new mysql_recordset("update leads_mortgage set sent_to='|".$rs2->myarray["id"]."|' where id='".$rs->myarray["id"]."'");
				}
				else
				{
					$rs3=new mysql_recordset("update leads_mortgage set sent_to=concat(sent_to,'".$rs2->myarray["id"]."|') where id='".$rs->myarray["id"]."'");	
				}
				
				# add the companies information to the email message
				$message.=strtoupper($rs2->myarray["name"]).":\n\n".$rs2->myarray["mortgage_message"]."\n_________________________________________\n\n";
			}
	
			$send_the_lead=false;
		}
		
		# if no campaigns where found to send the lead to, just mark the database for now!
		if($count==0)
		{
			$rs2=new mysql_recordset("update leads_mortgage set sent_to='none' where id='".$rs->myarray["id"]."'");	
		}		
		# mail the customer a follow-up
		else
		{
			$text="Dear ".$rs->myarray["fname"]." ".$rs->myarray["lname"].",\n\nThank you for requesting to Find a Mortgage through The iRelocation Network.  You can expect a resonse from our partners within 24 hours during normal business hours. Below we have provided more information about the company or companies that will be contacting you.\n\n";
			mail($rs->myarray["email"],"Your Mortgage Request Has Been Received!","$text$message","From: The iRelocation Network <no_reply@irelocation.com>\nReply-to: ".$rs->myarray["email"]."");	
		}		
	}
	
	# reset the counter
	$count=0;	
	$message="";
}
?>