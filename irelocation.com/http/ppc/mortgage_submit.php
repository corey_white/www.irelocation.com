<?
$DO_NOT_TRACK_PAGE_VIEW=true;
include_once("inc_session.php");

#
# Clean up fields
#
$sess_fname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($fname))));
$sess_lname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($lname))));
$sess_email=trim(str_replace(" ","",str_replace("\\","",str_replace("'","",strtolower($email)))));
$sess_phone=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone));
$sess_phone2=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone2));

$property_value=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$property_value));
$loan_amount=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$loan_amount));
$comments=trim(str_replace("'","",strtolower($comments)));

#
# Check required fields
#
if($state=='XX') $msg="Please select your state!";
if($credit=='XX') $msg="Please select your estimated credit rating!";
if($loan_amount=='') $msg="Please enter your estimated loan amount!";
if($property_value=='') $msg="Please enter your estimated property value!";
if(strlen($sess_phone2)<10 || strlen($sess_phone2)>11) $msg="Please enter a valid alternate phone number including the area code!";
if(strlen($sess_phone)<10 || strlen($sess_phone)>11) $msg="Please enter a valid phone number including the area code!";
if(substr_count($sess_email,'@')==0) $msg="Please enter a valid email address!";
if($sess_fname=='' || $sess_lname=='' || $sess_email=='' || $sess_phone=='') $msg="Please fill in all required fields!";

if($msg!='')
{
  include("mortgage.php");	
  exit;
}

#
# Get original campaign, referer, & keyword of this user
#
$rs=new mysql_recordset("select * from stats_pageviews where sid='$sid' and (campaign!='' or referer!='' or keyword!='')");
if($rs->fetch_array())
{
  $campaign=$rs->myarray["campaign"];
  $referer=$rs->myarray["referer"];
  $keyword=$rs->myarray["keyword"];
}

if($campaign!='')
{
  $campaign=$site_id."_".$campaign;
}
else
{
  $campaign=$site_id;
}

#
# Enter lead in database
#
$rs=new mysql_recordset("insert into leads_mortgage (received,fname,lname,email,phone,phone2,interest,state,loan_amount,credit,property_value,comments,campaign,referer,keyword) values (NOW(),'$sess_fname','$sess_lname','$sess_email','$sess_phone','$sess_phone2','$interest','$state','$loan_amount','$credit','$property_value','$comments','$campaign','$referer','$keyword')");

#
# Direct the user to the thank you page with a close button
#
$sess_mortgage_complete=true;

include 'header.php';
?>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Your request 
	to find a mortgage has been received.&nbsp; You can expect a quick response within 
	the next 24 hours, during normal business days.&nbsp; This program was specifically 
	designed by The iRelocation Network and SIRVA Relocation to provide consumers 
	with access to the best real estate professionals nationwide while providing 
	them with HUGE rewards.</font></p>
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Additional information 
	will be emailed to you at:<b><br>
	<?echo $sess_email;?></b> <br>
	within the next 10 minutes, so please check your email!</font></p>
	<font face="Tahoma" size="6" color="#E03C41">Thank You!</font>
	<table border="0" width="100%" id="table5" cellspacing="0" cellpadding="2">
		<tr>
			<td width="71%">
			<img border="0" src="images/find_mover.gif" width="90" height="12"></td>
		</tr>
		<tr>
			<td width="71%"><font color="#5E5E5E" face="Tahoma" size="1">Through 
			our network, you can save up to 50% on your move, from the nation&#39;s 
			leading moving companies! 1 Form, 4 Quotes!</font></td>
		</tr>
		<tr>
			<td width="71%">
			<p align="right"><font face="Tahoma" size="2">
			<a style="text-decoration: none" href="http://www.topmoving.com">
			<font color="#000000">start now &gt;</font></a></font></p>
			</td>
		</tr>
	</table>
		</td>
	</tr>
</table>
<?
include 'footer.php';
?>