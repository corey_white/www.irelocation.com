<?
include 'header.php';
?>

<p><p><img border="0" src="images/find_mortgage.gif" width="114" height="12">
<p><font face="Tahoma" color="#666666" size="2">For customers with excellent credit, SIRVA Mortgage will give you a $300 closing cost credit.&nbsp; 
However, no matter what your credit rating is, we match you with experienced 
lenders who can get you great rates!</font>
<p align="center"><img border="0" src="images/sirva.gif" width="71" height="34">
<img border="0" src="images/eho.gif" width="51" height="50">
<form method="POST" action="mortgage_submit.php">
	<table border="0" width="100%" id="table5" cellspacing="0" cellpadding="2" align="center">
		<tr>
			<td align="right" width="50%">
			<font face="Tahoma" size="2">First Name:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="fname" value="<?echo $sess_fname;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71; ">*</font></td>
		</tr>
		<tr>
			<td align="right" width="50%">
			<font face="Tahoma" size="2">Last Name:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="lname" value="<?echo $sess_lname;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71">*</font></td>
		</tr>
		<tr>
			<td align="right" width="50%">
			<font face="Tahoma" size="2">Email:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="email" value="<?echo $sess_email;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71">*</font></td>
		</tr>
		<tr>
			<td align="right" width="50%">
			<font face="Tahoma" size="2">Phone:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="phone" value="<?echo $sess_phone;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71">*</font></td>
		</tr>
		<tr>
			<td align="right" width="50%">
			<font face="Tahoma" size="2">Alternate Phone:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="phone2" value="<?echo $sess_phone2;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71">*</font></td>
		</tr>
	</table><br>
	<table border="0" width="600" id="table4" cellspacing="0" bgcolor="#FDEEEE" cellpadding="2">
		<tr>
			<td colspan="2" height="22"><b><font face="Tahoma" size="1">&nbsp; I am interesting in speaking to 
			a mortgage professional regarding:&nbsp;</font></b></td>
		</tr>
		<tr>
			<td width="6%">
			&nbsp;</td>
			<td width="92%">
			<table border="0" width="459" id="table6" cellspacing="0" cellpadding="0">
				<tr>
					<td width="20">
					<input type="radio" value="purchase" name="interest"<?if($interest!='refinance'){echo " checked";}?>></td>
					<td><font face="Tahoma" size="2">Purchase</font></td>
					<td width="20">
					<input type="radio" value="refinance" name="interest"<?if($interest=='refinance'){echo " checked";}?>></td>
					<td width="63"><font face="Tahoma" size="2">Refinance</font></td>
					<td width="20">&nbsp;</td>
					<td width="96"><font face="Tahoma" size="2">Property Value:</font></td>
					<td width="55"><font face="Tahoma" size="2">
					<input type="text" name="property_value" value="<?echo $property_value;?>" size="5" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71"></font></td>
					<td width="87"><font face="Tahoma" size="2">Loan Amount:</font></td>
					<td width="46"><font face="Tahoma" size="2">
					<input type="text" name="loan_amount" value="<?echo $loan_amount;?>" size="5" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71"></font></td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td width="98%" colspan="2" height="23"><b><font face="Tahoma" size="1">&nbsp; 
			How would you rate your credit status?</font></b></td>
		</tr>
		<tr>
			<td width="6%">&nbsp;</td>
			<td width="92%">
			<select name="credit" size="1" style="font-family: Tahoma; font-size: 8pt">
			<option value="XX"<?if($credit==''){echo " selected";}?>>Select One -></option>
			<option value="excellent"<?if($credit=='excellent'){echo " selected";}?>>Excellent, Never any issues</option>
			<option value="good"<?if($credit=='good'){echo " selected";}?>>Good, No recent issues</option>
			<option value="fair"<?if($credit=='fair'){echo " selected";}?>>Fair, Recent issues or history of issues</option>
			<option value="poor"<?if($credit=='poor'){echo " selected";}?>>Poor, Major issues or Limited credit history
			</option>
			</select></td>
		</tr>
	<tr>
		<td colspan="2" height="23"><b><font face="Tahoma" size="1">&nbsp; 
			What state is the property located in?</font></b></td>
	</tr>
	<tr>
		<td width="6%">&nbsp;</td>
		<td width="92%"><font face="Tahoma" size="2">
		<select name="state" style="font-size: 8pt; font-family: Tahoma;">
                  <option value="XX">Select One-></option>
              <option value="AL"<?if($state=='AL'){echo " selected";}?>>Alabama</option>
              <option value="AK"<?if($state=='AK'){echo " selected";}?>>Alaska</option>
              <option value="AZ"<?if($state=='AZ'){echo " selected";}?>>Arizona</option>
              <option value="AR"<?if($state=='AR'){echo " selected";}?>>Arkansas</option>
              <option value="CA"<?if($state=='CA'){echo " selected";}?>>California</option>
              <option value="CO"<?if($state=='CO'){echo " selected";}?>>Colorado</option>
              <option value="CT"<?if($state=='CT'){echo " selected";}?>>Connecticut</option>
              <option value="DC"<?if($state=='DC'){echo " selected";}?>>DC</option>
              <option value="DE"<?if($state=='DE'){echo " selected";}?>>Delaware</option>
              <option value="FL"<?if($state=='FL'){echo " selected";}?>>Florida</option>
              <option value="GA"<?if($state=='GA'){echo " selected";}?>>Georgia</option>
              <option value="HI"<?if($state=='HI'){echo " selected";}?>>Hawaii</option>
              <option value="ID"<?if($state=='ID'){echo " selected";}?>>Idaho</option>
              <option value="IL"<?if($state=='IL'){echo " selected";}?>>Illinois</option>
              <option value="IN"<?if($state=='IN'){echo " selected";}?>>Indiana</option>
              <option value="IA"<?if($state=='IA'){echo " selected";}?>>Iowa</option>
              <option value="KS"<?if($state=='KS'){echo " selected";}?>>Kansas</option>
              <option value="KY"<?if($state=='KY'){echo " selected";}?>>Kentucky</option>
              <option value="LA"<?if($state=='LA'){echo " selected";}?>>Louisiana</option>
              <option value="ME"<?if($state=='ME'){echo " selected";}?>>Maine</option>
              <option value="MD"<?if($state=='MD'){echo " selected";}?>>Maryland</option>
              <option value="MA"<?if($state=='MA'){echo " selected";}?>>Massachusetts</option>
              <option value="MI"<?if($state=='MI'){echo " selected";}?>>Michigan</option>
              <option value="MN"<?if($state=='MN'){echo " selected";}?>>Minnesota</option>
              <option value="MS"<?if($state=='MS'){echo " selected";}?>>Mississippi</option>
              <option value="MO"<?if($state=='MO'){echo " selected";}?>>Missouri</option>
              <option value="MT"<?if($state=='MT'){echo " selected";}?>>Montana</option>
              <option value="NE"<?if($state=='NE'){echo " selected";}?>>Nebraska</option>
              <option value="NV"<?if($state=='NV'){echo " selected";}?>>Nevada</option>
              <option value="NH"<?if($state=='NH'){echo " selected";}?>>New Hampshire</option>
              <option value="NJ"<?if($state=='NJ'){echo " selected";}?>>New Jersey</option>
              <option value="NM"<?if($state=='NM'){echo " selected";}?>>New Mexico</option>
              <option value="NY"<?if($state=='NY'){echo " selected";}?>>New York</option>
              <option value="NC"<?if($state=='NC'){echo " selected";}?>>North Carolina</option>
              <option value="ND"<?if($state=='ND'){echo " selected";}?>>North Dakota</option>
              <option value="OH"<?if($state=='OH'){echo " selected";}?>>Ohio</option>
              <option value="OK"<?if($state=='OK'){echo " selected";}?>>Oklahoma</option>
              <option value="OR"<?if($state=='OR'){echo " selected";}?>>Oregon</option>
              <option value="PA"<?if($state=='PA'){echo " selected";}?>>Pennsylvania</option>
              <option value="RI"<?if($state=='RI'){echo " selected";}?>>Rhode Island</option>
              <option value="SC"<?if($state=='SC'){echo " selected";}?>>South Carolina</option>
              <option value="SD"<?if($state=='SD'){echo " selected";}?>>South Dakota</option>
              <option value="TN"<?if($state=='TN'){echo " selected";}?>>Tennessee</option>
              <option value="TX"<?if($state=='TX'){echo " selected";}?>>Texas</option>
              <option value="UT"<?if($state=='UT'){echo " selected";}?>>Utah</option>
              <option value="VT"<?if($state=='VT'){echo " selected";}?>>Vermont</option>
              <option value="VA"<?if($state=='VA'){echo " selected";}?>>Virginia</option>
              <option value="WA"<?if($state=='WA'){echo " selected";}?>>Washington</option>
              <option value="WV"<?if($state=='WV'){echo " selected";}?>>West Virginia</option>
              <option value="WI"<?if($state=='WI'){echo " selected";}?>>Wisconsin</option>
              <option value="WY"<?if($state=='WY'){echo " selected";}?>>Wyoming</option>
                </select></font></td>
	</tr>
		<tr>
			<td width="98%" colspan="2" height="23"><b><font face="Tahoma" size="1">&nbsp; 
			Do you have any comments that you would like to add for your 
			mortgage professional?</font></b></td>
		</tr>
		<tr>
			<td width="6%">&nbsp;</td>
			<td width="92%">
			<input type="text" name="comments" value="<?echo $comments;?>" size="75" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #E86D71"></td>
		</tr>
		</table>
<p align="center">
<input border="0" src="images/submit2.gif" name="I1" width="150" height="26" type="image">
</form>
<?
if($msg!='')
{
?>
<script language="javascript">
<!--
alert("<?echo $msg;?>");
// -->
</script>
<?	
}
include 'footer.php';
?>