<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>The iRelocation Network - Find A Realtor</title>
<SCRIPT LANGUAGE="JavaScript">
<!-- Overture Services, Inc
var pm_tagname = "shopperTag.txt";
var pm_tagversion = "1.3";
window.pm_customData = new Object();
window.pm_customData.segment="name=shopper, transId=";
// -->
</SCRIPT>
<?
include_once("inc_session.php");
?>
</head>
<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#C0C0C0">
<form method="POST" action="realtor_submit.php">
<div style="position: absolute; width: 459px; height: 81px; z-index: 1; left: 105px; top: 78px" id="layer2">
<img border="0" src="images/find_realtor.gif" width="100" height="12"><br>
<font face="Tahoma" color="#666666" size="2"><b>Get $40 for every $10,000 in 
home value</b>* when you find an agent through our service!&nbsp; We have over 10,000+ 
hand-selected realtors from the nation's leading real estate companies including 
Century21, Prudential, and Coldwell Banker. <b><font color="#FF1111">A consultant from SIRVA
will contact you first to discuss your needs, and then put you in contact with a Realtor that best fits you!  This service is FREE to you and there is
no obligation to buy/sell.</font></b></font></div>
<div style="position: absolute; width: 446px; height: 204px; z-index: 1; left: 79px; top: 202px" id="layer3">
	<table border="0" width="100%" cellspacing="0" cellpadding="2">
		<tr>
			<td align="right" width="120">
			<font face="Tahoma" size="2">First Name:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="fname" value="<?echo $sess_fname;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">*</font></td>
		</tr>
		<tr>
			<td align="right" width="120">
			<font face="Tahoma" size="2">Last Name:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="lname" value="<?echo $sess_lname;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">*</font></td>
		</tr>
		<tr>
			<td align="right" width="120">
			<font face="Tahoma" size="2">Email:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="email" value="<?echo $sess_email;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">*</font></td>
		</tr>
		<tr>
			<td align="right" width="120">
			<font face="Tahoma" size="2">Daytime Phone:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="phone" value="<?echo $sess_phone;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">*</font></td>
		</tr>
		<tr>
			<td align="right" width="120">
			<font face="Tahoma" size="2">Evening Phone:</font></td>
			<td><font face="Tahoma" size="2">&nbsp;<input type="text" name="phone2" value="<?echo $sess_phone2;?>" size="26" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8">*</font></td>
		</tr>
		<tr>
			<td><img src="images/clear.gif" border=0></td>
			<td><img src="images/clear.gif" border=0></td>
		</tr>
	</table>
	<table border="0" width="100%" cellspacing="0" bgcolor="#E0EAF8" cellpadding="2">
		<tr>
			<td colspan="2" height="22"><b><font face="Tahoma" size="1">&nbsp; I am interesting in speaking to 
			a relocation consultant regarding:&nbsp; </font></b>
			<font face="Tahoma" size="1">(check all that apply)</font></td>
		</tr>
		<tr>
			<td align="right" width="8%">
			<input type="checkbox" name="buying" value="yes"<?if($buying=='yes'){echo " CHECKED";}?>></td>
			<td width="90%"><font face="Tahoma" size="2">Buying a home in</font>
			<select name="buying_state" size="1" style="font-family: Tahoma; font-size: 8pt;">
			<option value="XX"<?if($buying_state==''){echo " selected";}?>>Select One</option>
			<option value="AL"<?if($buying_state=='AL'){echo " selected";}?>>Alabama</option>
			<option value="AK"<?if($buying_state=='AK'){echo " selected";}?>>Alaska</option>
			<option value="AZ"<?if($buying_state=='AZ'){echo " selected";}?>>Arizona</option>
			<option value="AR"<?if($buying_state=='AR'){echo " selected";}?>>Arkansas</option>
			<option value="CA"<?if($buying_state=='CA'){echo " selected";}?>>California</option>
			<option value="CO"<?if($buying_state=='CO'){echo " selected";}?>>Colorado</option>
			<option value="CT"<?if($buying_state=='CT'){echo " selected";}?>>Connecticut</option>
			<option value="DE"<?if($buying_state=='DE'){echo " selected";}?>>Delaware</option>
			<option value="DC"<?if($buying_state=='DC'){echo " selected";}?>>District Of Columbia</option>
			<option value="FL"<?if($buying_state=='FL'){echo " selected";}?>>Florida</option>
			<option value="GA"<?if($buying_state=='GA'){echo " selected";}?>>Georgia</option>
			<option value="HI"<?if($buying_state=='HI'){echo " selected";}?>>Hawaii</option>
			<option value="ID"<?if($buying_state=='ID'){echo " selected";}?>>Idaho</option>
			<option value="IL"<?if($buying_state=='IL'){echo " selected";}?>>Illinois</option>
			<option value="IN"<?if($buying_state=='IN'){echo " selected";}?>>Indiana</option>
			<option value="IA"<?if($buying_state=='IA'){echo " selected";}?>>Iowa</option>
			<option value="KS"<?if($buying_state=='KS'){echo " selected";}?>>Kansas</option>
			<option value="KY"<?if($buying_state=='KY'){echo " selected";}?>>Kentucky</option>
			<option value="LA"<?if($buying_state=='LA'){echo " selected";}?>>Louisiana</option>
			<option value="ME"<?if($buying_state=='ME'){echo " selected";}?>>Maine</option>
			<option value="MD"<?if($buying_state=='MD'){echo " selected";}?>>Maryland</option>
			<option value="MA"<?if($buying_state=='MA'){echo " selected";}?>>Massachusetts</option>
			<option value="MI"<?if($buying_state=='MI'){echo " selected";}?>>Michigan</option>
			<option value="MN"<?if($buying_state=='MN'){echo " selected";}?>>Minnesota</option>
			<option value="MS"<?if($buying_state=='MS'){echo " selected";}?>>Mississippi</option>
			<option value="MO"<?if($buying_state=='MO'){echo " selected";}?>>Missouri</option>
			<option value="MT"<?if($buying_state=='MT'){echo " selected";}?>>Montana</option>
			<option value="NE"<?if($buying_state=='NE'){echo " selected";}?>>Nebraska</option>
			<option value="NV"<?if($buying_state=='NV'){echo " selected";}?>>Nevada</option>
			<option value="NH"<?if($buying_state=='NH'){echo " selected";}?>>New Hampshire</option>
			<option value="NJ"<?if($buying_state=='NJ'){echo " selected";}?>>New Jersey</option>
			<option value="NM"<?if($buying_state=='NM'){echo " selected";}?>>New Mexico</option>
			<option value="NY"<?if($buying_state=='NY'){echo " selected";}?>>New York</option>
			<option value="NC"<?if($buying_state=='NC'){echo " selected";}?>>North Carolina</option>
			<option value="ND"<?if($buying_state=='ND'){echo " selected";}?>>North Dakota</option>
			<option value="OH"<?if($buying_state=='OH'){echo " selected";}?>>Ohio</option>
			<option value="OK"<?if($buying_state=='OK'){echo " selected";}?>>Oklahoma</option>
			<option value="OR"<?if($buying_state=='OR'){echo " selected";}?>>Oregon</option>
			<option value="PA"<?if($buying_state=='PA'){echo " selected";}?>>Pennsylvania</option>
			<option value="RI"<?if($buying_state=='RI'){echo " selected";}?>>Rhode Island</option>
			<option value="SC"<?if($buying_state=='SC'){echo " selected";}?>>South Carolina</option>
			<option value="SD"<?if($buying_state=='SD'){echo " selected";}?>>South Dakota</option>
			<option value="TN"<?if($buying_state=='TN'){echo " selected";}?>>Tennessee</option>
			<option value="TX"<?if($buying_state=='TX'){echo " selected";}?>>Texas</option>
			<option value="UT"<?if($buying_state=='UT'){echo " selected";}?>>Utah</option>
			<option value="VT"<?if($buying_state=='VT'){echo " selected";}?>>Vermont</option>
			<option value="VA"<?if($buying_state=='VA'){echo " selected";}?>>Virginia</option>
			<option value="WA"<?if($buying_state=='WA'){echo " selected";}?>>Washington</option>
			<option value="WV"<?if($buying_state=='WV'){echo " selected";}?>>West Virginia</option>
			<option value="WI"<?if($buying_state=='WI'){echo " selected";}?>>Wisconsin</option>
			<option value="WY"<?if($buying_state=='WY'){echo " selected";}?>>Wyoming</option>
			</select>
                        <select name="buying_value" size="1" style="font-family: Tahoma; font-size: 8pt;">
			<option value="XX"<?if($buying_value==''){echo " selected";}?>>Estimated Home Value</option>
                        <?for($v=75;$v<800;$v+=25){?>
			<option value="<?echo $v.",000";?>"<?if($buying_value==$v.",000"){echo " selected";}?>><?echo "\$".$v.",000";?></option>	
                        <?}?>
                        <option value="800,000+"<?if($buying_value=='800,000+'){echo " selected";}?>>$800,000+</option>
			</select></td>
		</tr>
		<tr>
			<td align="right" width="8%">
			<input type="checkbox" name="selling" value="yes"<?if($selling=='yes'){echo " CHECKED";}?>></td>
			<td width="90%"><font face="Tahoma" size="2">Selling a home in</font>
			<select name="selling_state" size="1" style="font-family: Tahoma; font-size: 8pt;">
			<option value="XX"<?if($selling_state==''){echo " selected";}?>>Select One</option>
			<option value="AL"<?if($selling_state=='AL'){echo " selected";}?>>Alabama</option>
			<option value="AK"<?if($selling_state=='AK'){echo " selected";}?>>Alaska</option>
			<option value="AZ"<?if($selling_state=='AZ'){echo " selected";}?>>Arizona</option>
			<option value="AR"<?if($selling_state=='AR'){echo " selected";}?>>Arkansas</option>
			<option value="CA"<?if($selling_state=='CA'){echo " selected";}?>>California</option>
			<option value="CO"<?if($selling_state=='CO'){echo " selected";}?>>Colorado</option>
			<option value="CT"<?if($selling_state=='CT'){echo " selected";}?>>Connecticut</option>
			<option value="DE"<?if($selling_state=='DE'){echo " selected";}?>>Delaware</option>
			<option value="DC"<?if($selling_state=='DC'){echo " selected";}?>>District Of Columbia</option>
			<option value="FL"<?if($selling_state=='FL'){echo " selected";}?>>Florida</option>
			<option value="GA"<?if($selling_state=='GA'){echo " selected";}?>>Georgia</option>
			<option value="HI"<?if($selling_state=='HI'){echo " selected";}?>>Hawaii</option>
			<option value="ID"<?if($selling_state=='ID'){echo " selected";}?>>Idaho</option>
			<option value="IL"<?if($selling_state=='IL'){echo " selected";}?>>Illinois</option>
			<option value="IN"<?if($selling_state=='IN'){echo " selected";}?>>Indiana</option>
			<option value="IA"<?if($selling_state=='IA'){echo " selected";}?>>Iowa</option>
			<option value="KS"<?if($selling_state=='KS'){echo " selected";}?>>Kansas</option>
			<option value="KY"<?if($selling_state=='KY'){echo " selected";}?>>Kentucky</option>
			<option value="LA"<?if($selling_state=='LA'){echo " selected";}?>>Louisiana</option>
			<option value="ME"<?if($selling_state=='ME'){echo " selected";}?>>Maine</option>
			<option value="MD"<?if($selling_state=='MD'){echo " selected";}?>>Maryland</option>
			<option value="MA"<?if($selling_state=='MA'){echo " selected";}?>>Massachusetts</option>
			<option value="MI"<?if($selling_state=='MI'){echo " selected";}?>>Michigan</option>
			<option value="MN"<?if($selling_state=='MN'){echo " selected";}?>>Minnesota</option>
			<option value="MS"<?if($selling_state=='MS'){echo " selected";}?>>Mississippi</option>
			<option value="MO"<?if($selling_state=='MO'){echo " selected";}?>>Missouri</option>
			<option value="MT"<?if($selling_state=='MT'){echo " selected";}?>>Montana</option>
			<option value="NE"<?if($selling_state=='NE'){echo " selected";}?>>Nebraska</option>
			<option value="NV"<?if($selling_state=='NV'){echo " selected";}?>>Nevada</option>
			<option value="NH"<?if($selling_state=='NH'){echo " selected";}?>>New Hampshire</option>
			<option value="NJ"<?if($selling_state=='NJ'){echo " selected";}?>>New Jersey</option>
			<option value="NM"<?if($selling_state=='NM'){echo " selected";}?>>New Mexico</option>
			<option value="NY"<?if($selling_state=='NY'){echo " selected";}?>>New York</option>
			<option value="NC"<?if($selling_state=='NC'){echo " selected";}?>>North Carolina</option>
			<option value="ND"<?if($selling_state=='ND'){echo " selected";}?>>North Dakota</option>
			<option value="OH"<?if($selling_state=='OH'){echo " selected";}?>>Ohio</option>
			<option value="OK"<?if($selling_state=='OK'){echo " selected";}?>>Oklahoma</option>
			<option value="OR"<?if($selling_state=='OR'){echo " selected";}?>>Oregon</option>
			<option value="PA"<?if($selling_state=='PA'){echo " selected";}?>>Pennsylvania</option>
			<option value="RI"<?if($selling_state=='RI'){echo " selected";}?>>Rhode Island</option>
			<option value="SC"<?if($selling_state=='SC'){echo " selected";}?>>South Carolina</option>
			<option value="SD"<?if($selling_state=='SD'){echo " selected";}?>>South Dakota</option>
			<option value="TN"<?if($selling_state=='TN'){echo " selected";}?>>Tennessee</option>
			<option value="TX"<?if($selling_state=='TX'){echo " selected";}?>>Texas</option>
			<option value="UT"<?if($selling_state=='UT'){echo " selected";}?>>Utah</option>
			<option value="VT"<?if($selling_state=='VT'){echo " selected";}?>>Vermont</option>
			<option value="VA"<?if($selling_state=='VA'){echo " selected";}?>>Virginia</option>
			<option value="WA"<?if($selling_state=='WA'){echo " selected";}?>>Washington</option>
			<option value="WV"<?if($selling_state=='WV'){echo " selected";}?>>West Virginia</option>
			<option value="WI"<?if($selling_state=='WI'){echo " selected";}?>>Wisconsin</option>
			<option value="WY"<?if($selling_state=='WY'){echo " selected";}?>>Wyoming</option>
			</select>
                        <select name="selling_value" size="1" style="font-family: Tahoma; font-size: 8pt;">
			<option value="XX"<?if($selling_value==''){echo " selected";}?>>Estimated Home Value</option>
                        <?for($v=75;$v<800;$v+=25){?>
			<option value="<?echo $v.",000";?>"<?if($selling_value==$v.",000"){echo " selected";}?>><?echo "\$".$v.",000";?></option>	
                        <?}?>
                        <option value="800,000+"<?if($selling_value=='800,000+'){echo " selected";}?>>$800,000+</option>
			</select></td>
		</tr>
		<tr>
			<td width="98%" colspan="2" height="23"><b><font face="Tahoma" size="1">&nbsp; 
			Do you have any comments that you would like to add for your 
			relocation consultant?</font></b></td>
		</tr>
		<tr>
			<td width="8%">&nbsp;</td>
			<td width="90%">
			<input type="text" name="comments" size="75" value="<?echo $comments;?>" style="font-size: 8pt; font-family: Tahoma; border: 1px solid #6996D8"></td>
		</tr>
		<tr>
			<td colspan="2" align="center" bgcolor="#FFFFFF"><input border="0" src="images/submit1.gif" width="150" height="26" type="image"></td>
		</tr>
		</table></div>
<div style="position: absolute; width: 96px; height: 28px; z-index: 1; left: 418px; top: 283px" id="layer6">
<img border="0" src="images/prudential.gif" width="90" height="25"></div>
<div style="position: absolute; width: 62px; height: 33px; z-index: 1; left: 435px; top: 205px" id="layer5">
<img border="0" src="images/century21.gif" width="58" height="31"></div>
<div style="position: absolute; width: 86px; height: 34px; z-index: 1; left: 424px; top: 245px" id="layer4">
<img border="0" src="images/coldwell_banker.gif" width="84" height="31"></div>
<div style="position: absolute; width: 67px; height: 65px; z-index: 1; left: 32px; top: 79px" id="layer1">
<img border="0" src="images/num1.gif" width="61" height="62"></div>
<table border="0" width="600" cellspacing="0" cellpadding="0" height="500" bgcolor="#FFFFFF">
	<tr>
		<td height="53">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
				<a target="_blank" href="http://www.irelocation.com">
				<img border="0" src="images/logo_small.gif" width="144" height="53"></a></td>
				<td>
				<img border="0" src="images/slogan.gif" width="341" height="13" align="right"></td>
			</tr>
		</table></td>
	</tr>
	<tr>
		<td background="images/bar.gif" height="18">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td>
		<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%">
			<tr>
				<td width="25">&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td>&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="25">&nbsp;</td>
			</tr>
		</table></td>
	</tr>
	<tr>
		<td height="20">
		<table border="0" width="100%" cellspacing="0" cellpadding="0" height="20">
			<tr>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#BEC3BD">
				<p align="center">
				<img border="0" src="images/clear.gif" width="1" height="1"><b><font face="Tahoma" color="#5E5E5E" size="1">The 
				iRelocation Network � 2004-<?echo Date("Y");?> � <a href="privacy.php" target="_blank"><font color="#5E5E5E">Privacy Statement</font></a> � 
<a href="terms.php" target="_blank"><font color="#5E5E5E">Terms &amp; Conditions</font></a></font></b></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table></td>
	</tr>
	<tr>
		<td height="1">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td height="3">
		<table border="0" width="100%" cellspacing="0" cellpadding="0" height="3">
			<tr>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#CBBDB9">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table></td>
	</tr>
</table>
</form>
<?
if($msg!='')
{
?>
<script language="javascript">
<!--
alert("<?echo $msg;?>");
// -->
</script>
<?	
}
?>
</body>

</html>