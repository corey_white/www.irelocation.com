<?
// Add Overture Marketing Code.
//$show_stats=true;
session_start();
$sid=session_id();

session_register("sess_fname");
session_register("sess_lname");
session_register("sess_email");
session_register("sess_phone");
session_register("sess_phone2");

session_register("sess_realtor_complete");
session_register("sess_mortgage_complete");
session_register("sess_mover_complete");

include_once("inc_mysql.php");


$domain="irelocation.com";  //websites domain without www
$site_id="irelo";
$page=str_replace('/','',strtolower($PHP_SELF));

#
# Get the user agent
#
$user_agent=strtolower($HTTP_USER_AGENT);
//check if the agent is a robot
$browsers=array('msie','opera','windows','powerpc','macintosh','[en]','gecko','win32','win16','win95','winnt','konqueror');
$robots=array('seeker','crawler','retriever','infoseek','fireball','activex','bot','spider');
foreach($browsers as $browser)
{
  if(substr_count($user_agent,$browser)>0)
  {
    $pass=true;
    foreach($robots as $robot)
    {
      if(substr_count($user_agent,$robot)>0)
      { 
        $pass=false;
      }
    }
    break;    
  }
}

if($pass==true)
{
  if($DO_NOT_TRACK_PAGE_VIEW!=true)
  {
    #
    # Get the refering domain name
    #
    $remove=array("http://","https://","www.");
    $referer=str_replace($remove,"",strtolower($HTTP_REFERER));
    if(substr_count($referer,"/")==true)
    {
      $temp=explode("/",$referer,2);
      $referer=$temp[0];
    }
    if($referer=='') $referer=='none';
    
    
    if($referer==$domain)
    {
      //enter session id, page, timestamp
     // $rs=new mysql_recordset("insert into stats_pageviews (sid,page,time) values ('$sid','$page',NOW())");
    
    }
    else
    {
      #
      # Get the refering keyword
      #
      $keys=array('?q=','&q=','?p=','&p=','query=','keywords=','&k=');
      foreach($keys as $key)
      {
        if(substr_count($temp[1],$key)>0)
        {
          $temp2=explode($key,$temp[1]);
          $temp3=explode('&',$temp2[1]);
          if(is_array($temp3)!=false)
          {
            $keyword=str_replace("'","",urldecode($temp3[0]));
          }
          else
          {
            $keyword=str_replace("'","",urldecode($temp2[1]));
          }
        }
      }
   
      //advertising campaign, if one was passed
      $campaign=str_replace("'","",strtolower($source));
  
      //enter session id, page, referer, keyword, timestamp
     // $rs=new mysql_recordset("insert into stats_pageviews (sid,page,referer,keyword,campaign,time) values ('$sid','$page','$referer','$keyword','$campaign',NOW())");
    }
  }
}
else
{
  //user is a robot
  $is_robot=true;
  $rs=new mysql_recordset("insert into stats_robots (time,page,agent) values (NOW(),'$page','$user_agent')");
}

if($show_stats==true)
{
  echo "<pre>Page statistics for $domain\n\nPage: $page\nSession ID: $sid\nReferer: $referer\nKeyword: $keyword\nLast Page: $last_page\n</pre>";
  //phpinfo();
}

?>
<script language="javascript" type="text/javascript" src="perf.js"></script>