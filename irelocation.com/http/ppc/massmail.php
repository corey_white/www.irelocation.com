<?
include('inc_mysql.php');

if($action=='send_email')
{
	if($category=="")
	{
		$errmsg="Please select a category";
	}
	if(($from=="") || ($subject=="") || ($message==""))
	{
		$errmsg="Please complete all the fields";	
	}
	if($errmsg=="")
	{
		$message=str_replace("\r\n","\n",$message);
		$message=stripslashes($message);
		$subject=stripslashes($subject);
		if($category=="auto")
		{
			$sql="select distinct(c.contact_email) from movingdirectory.company as c, movingdirectory.directlistings as d, movingdirectory.directleads as l where c.comp_id=d.comp_id and d.cat_id='16' and c.comp_id!='702' and c.comp_id!='5229' and c.comp_id!='5768' and c.comp_id!='5432' and c.comp_id!='5647' and c.comp_id!='5777' and c.comp_id!='5773' and c.comp_id!='5826' and c.comp_id!='5735'
			 and c.comp_id!='9' and c.comp_id!='5580' and c.comp_id!='5663' and c.comp_id!='5526' and c.comp_id!='5774' and c.comp_id!='5784' and c.comp_id!='5421' and c.comp_id!='5758' and c.comp_id!='5803' and c.comp_id!='4974' and c.contact_email!=''";
		}
		if($category=="move")
		{
			$sql="select distinct(c.contact_email) from movingdirectory.company as c, movingdirectory.directlistings as d where c.comp_id=d.comp_id and d.cat_id='17' and c.comp_id!='5546' and c.comp_id!='5549' and c.comp_id!='124' and c.comp_id!='5548' and c.comp_id!='5664' and c.comp_id!='5829' and c.contact_email!=''";	
		}
		if($category=="real")
		{
			$sql="select distinct(c.contact_email) from movingdirectory.company as c, movingdirectory.directlistings as d where c.comp_id=d.comp_id and d.cat_id='12' and c.contact_email!=''";
		}
		$rs=new mysql_recordset($sql);
		$i=0;
		while($rs->fetch_array())
		{
			mail($rs->myarray["contact_email"],"$subject","$message",'From: '.$from.'');
			//echo "".$rs->myarray["contact_email"]." $i<br>";
			$i++;
		}
		mail("$from","$subject","$message",'From: '.$from.'');
		$msg="You have successfully sent $i emails.  If you see this message DO NOT Reload the page, Push the back button or click the Send Email button.<br>
		Click <a href='massmail.php'>HERE</a> to send a brand new email.";
	}
}
?>
<html>
<body>
<?
if($errmsg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$errmsg</font></p>";
}
if($msg!='')
{
  echo "<p><font face='verdana' color='#FF0000' size=2>$msg</font></p>";
}
?>
<p><font face="verdana" color="#2360A5" size="3"><b> Send Emails to Companies</b></font></p>
<form name="emails" method="POST" action="massmail.php">
<input type="hidden" name="action" value="send_email">
      <table border="0" cellspacing="2" width="500" cellpadding="2">
        <tr>
          <td width="150">To:</td>
          <td width="350"><select name="category">
          <option value="">Select a Category</option>
          <option value="auto"<?if($category=='auto'){echo " selected";}?>>Auto Transport Companies</option>
		  <option value="move"<?if($category=='move'){echo " selected";}?>>Moving Companies</option>
		  <option value="real"<?if($category=='real'){echo " selected";}?>>Real Estate Agents</option></select></td>
        </tr>
        <tr>
          <td width="200">From:</td>
          <td width="300"><input type="text" name="from" value="<?=$from?>" size="40"></td>
        </tr>
        <tr>
          <td width="200">Subject:</td>
          <td width="300"><input type="text" name="subject" value="<?=$subject?>" size="40"></td>
        </tr>
        <tr>
          <td width="200">Message (text):</td>
          <td width="300"><textarea name="message" cols="40" rows="10"><?=$message?></textarea></td>
        </tr>
        <tr>
          <td width="200">&nbsp;</td>
          <td width="300" align="right"><input type="submit" name="submit" value="Send Email Now!"></td>
        </tr>
      </table>
  </form>
</body>
</html>