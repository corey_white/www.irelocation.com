<?
//$DO_NOT_TRACK_PAGE_VIEW=true;
//include_once("inc_session.php");

session_start();
$sid=session_id();

session_register("sess_fname");
session_register("sess_lname");
session_register("sess_email");
session_register("sess_phone");
session_register("sess_phone2");

session_register("sess_realtor_complete");
session_register("sess_mortgage_complete");
session_register("sess_mover_complete");

include_once("inc_mysql.php");




#
# Clean up fields
#
$sess_fname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($fname))));
$sess_lname=trim(ucwords(eregi_replace('[[:punct:]]','',strtolower($lname))));
$sess_email=trim(str_replace(" ","",str_replace("\\","",str_replace("'","",strtolower($email)))));
$sess_phone=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone));
$sess_phone2=trim(eregi_replace('[[:alpha:][:punct:][:space:]]','',$phone2));

$comments=trim(str_replace("'","",strtolower($comments)));

#
# Check required fields
#
if($selling=='yes' && $selling_state=='XX') $msg="Please select the state you are selling a home!";
if($selling=='yes' && $selling_value=='XX') $msg="Please select your estimated home value!";
if($buying=='yes' && $buying_state=='XX') $msg="Please select the state you are buying a home!";
if($buying=='yes' && $buying_value=='XX') $msg="Please select your estimated home value!";
if($buying!='yes' && $selling!='yes') $msg="Please check at least one interest below (Buying/Selling)!";
if(strlen($sess_phone2)<10 || strlen($sess_phone2)>11) $msg="Please enter a valid alternate phone number including the area code!";
if(strlen($sess_phone)<10 || strlen($sess_phone)>11) $msg="Please enter a valid phone number including the area code!";
if(substr_count($sess_email,'@')==0) $msg="Please enter a valid email address!";
if($sess_fname=='' || $sess_lname=='' || $sess_email=='' || $sess_phone=='') $msg="Please fill in all required fields!";

if($msg!='')
{
  include("index.php");	
  exit;
}
else
{
include 'header.php';
?>

<SCRIPT LANGUAGE="JavaScript">
<!-- Overture Services, Inc
var pm_tagname = "conversionTag.txt";
var pm_tagversion = "1.4";
window.pm_customData = new Object();
window.pm_customData.segment = "name=conversion, transId=, amount=30.0";
// -->
</SCRIPT>
<script language="javascript" type="text/javascript" src="perf.js"></script>
<SCRIPT LANGUAGE="JavaScript">
<!-- Overture Services Inc. 07/15/2003
var cc_tagVersion = "1.0";
var cc_accountID = "7287005370";
var cc_marketID =  "0";
var cc_protocol="http";
var cc_subdomain = "convctr";
if(location.protocol == "https:")
{
    cc_protocol="https";
     cc_subdomain="convctrs";
}
var cc_queryStr = "?" + "ver=" + cc_tagVersion + "&aID=" + cc_accountID + "&mkt=" + cc_marketID +"&ref=" + escape(document.referrer);
var cc_imageUrl = cc_protocol + "://" + cc_subdomain + ".overture.com/images/cc/cc.gif" + cc_queryStr;
var cc_imageObject = new Image();
cc_imageObject.src = cc_imageUrl;
// -->
</SCRIPT>
<?
}

#
# Get original campaign, referer, & keyword of this user
#
/*
$rs=new mysql_recordset("select * from stats_pageviews where sid='$sid' and (campaign!='' or referer!='' or keyword!='')");
if($rs->fetch_array())
{
  $campaign=$rs->myarray["campaign"];
  $referer=$rs->myarray["referer"];
  $keyword=$rs->myarray["keyword"];
}
*/
if($campaign!='')
{
  $campaign=$site_id."_".$campaign;
}
else
{
  $campaign=$site_id;
}

#
# Enter lead in database
#
if($buying_state=='XX') $buying_state='';
if($selling_state=='XX') $selling_state='';
$mytime = date("YmdHis");
$rs=new mysql_recordset("insert into leads_realtor (received,fname,lname,email,phone,phone2,buying,buying_state,buying_value,selling,selling_state,selling_value,comments,campaign,referer,keyword) values ('$mytime','$sess_fname','$sess_lname','$sess_email','$sess_phone','$sess_phone2','$buying','$buying_state','$buying_value','$selling','$selling_state','$selling_value','$comments','$campaign','$referer','$keyword')");

#
# Direct the user to the thank you page with a close button
#
$sess_realtor_complete=true;
?>

<table border="0" width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td>
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Your request 
	for a realtor has been received.&nbsp; You can expect a quick response within 
	the next 24 hours, during normal business days.&nbsp; This program was specifically 
	designed by The iRelocation Network to provide consumers 
	with access to the best real estate professionals nationwide while providing 
	them with HUGE rewards.</font></p>
	<p align="center"><font face="Tahoma" size="2" color="#5E5E5E">Additional information 
	will be emailed to you at:<b><br>
	<?echo $sess_email;?></b> <br>
	within the next 10 minutes, so please check your email!</font></p>
	<p><font face="Tahoma" size="6" color="#6996D8">Thank You!</font></p>
	<table border="0" width="100%" id="table4" cellspacing="0" cellpadding="2">
		<tr>
			<td height="10">
			<img border="0" src="images/clear.gif">
			</td>
		</tr>
	</table>
		</td>
	</tr>
</table>
<?
include 'footer.php';
?>