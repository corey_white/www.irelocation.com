<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<title>The iRelocation Network - The Mortgage Process</title>
<?
include_once("inc_session.php");
?>
</head>

<body topmargin="0" leftmargin="0" bottommargin="0" bgcolor="#C0C0C0">

<table border="0" width="100%" cellspacing="0" cellpadding="0" height="100%" bgcolor="#FFFFFF">
	<tr>
		<td height="53">
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td><a target="_blank" href="http://www.irelocation.com">
				<img border="0" src="images/logo_small.gif" width="144" height="53"></a></td>
				<td>
				<img border="0" src="images/slogan.gif" width="341" height="13" align="right"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td background="images/bar.gif" height="18">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td>
		<table border="0" width="100%" id="table3" cellspacing="0" cellpadding="0" height="100%">
			<tr>
				<td width="25">&nbsp;</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td valign="top">
				<blockquote>
&nbsp;<p><font face="Tahoma" size="5" color="#E03C41">The Mortgage Process
					</font></p>
					<p><b><font face="Tahoma" color="#376EAD">� the loan 
					appointment</font></b><font face="Tahoma" size="2"><br>
					Our Loan Facilitator will be the first to contact you. The 
					facilitator is responsible for setting your loan application 
					appointment, and will provide you with the name of your loan 
					counselor. The facilitator will also review the information 
					you will need to gather for a smooth and effortless loan 
					application. </font></p>
					<p><b><font face="Tahoma" color="#376EAD">� the loan 
					application</font></b><font face="Tahoma" size="2"><br>
					Your Loan Counselor will promptly contact you at the 
					scheduled time of your loan application. The loan counselor 
					is responsible for guiding you through the loan application 
					process, counseling you about various loan products, and 
					helping you choose the program that is right for you, so 
					your financing package conforms to meet your company's 
					relocation policy. Your loan counselor will walk you through 
					what to expect from application to signing your loan 
					documents at closing. When you're ready, your loan counselor 
					will help you select the best product and lock your interest 
					rate. </font></p>
					<p><b><font face="Tahoma" color="#376EAD">� the loan in 
					process</font></b><font face="Tahoma" size="2"><br>
					Once your loan application is taken, our operations staff 
					begins to prepare for your closing. The first step is 
					getting your loan processed and approved. Your loan 
					processor is responsible for the collection and analysis of 
					your documentation. With your initial loan package you will 
					receive a checklist of the items your processor needs in 
					order to confirm your information and get your loan 
					approved. Your processor is responsible for clearing these 
					conditions as well as ordering and reviewing your appraisal.
					</font></p>
					<p><b><font face="Tahoma" color="#376EAD">� the loan in 
					closing</font></b><font face="Tahoma" size="2"><br>
					Now you've been approved and you're ready for closing. 
					What's left to do? You will need to have an appointment set 
					with your closing agent (typically a title company or 
					closing attorney). You will also need to know how much money 
					you'll need, where to take it and if you'll need to bring 
					anything else with you. This is where our closer will assist 
					you. Our loan closers are responsible for getting your loan 
					documents to your closing agent, providing you with the 
					items you will need to bring with you and, most importantly, 
					providing you with your final closing figures. Once your 
					appointment with your closing agent has been established, 
					your closer will work hand-in-hand with your closing agent 
					to provide you with this information in a timely manner so 
					that you can prepare for the closing of your new home.
					</font></p>
					<p><b><font face="Tahoma" color="#376EAD">� after closing
					<font size="2">(SIRVA Mortgage Only)</font></font></b><font face="Tahoma" size="2"><br>
					After your loan closes, you should expect to receive the 
					following items in the mail: </font></p>
					<ol>
						<li><font face="Tahoma" size="2">The &quot;Goodbye Letter&quot;. 
						SIRVA Mortgage will sell your loan to one of our 
						investors after closing. As part of the sale, the 
						servicing of the loan will also be sold. SIRVA Mortgage 
						will notify you of your new servicer with a Goodbye 
						Letter. </font></li>
						<li><font face="Tahoma" size="2">The &quot;Hello Letter&quot;. 
						Your new servicer will also send you a notification of 
						the change. This change will not affect your loan terms 
						in any way. Your loan program cannot change with a 
						change of servicer. Your hello letter may also include 
						information on establishing direct payment to your 
						checking/savings account for your convenience. </font>
						</li>
						<li><font face="Tahoma" size="2">The Customer Service 
						Survey. At SIRVA Mortgage, we appreciate your feedback. 
						Please feel free to recognize an outstanding service or 
						provide insight on our processes.</font></li>
					</ol>
				</blockquote>
				</td>
				<td bgcolor="#E1E3E0" width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="25">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="20">
		<table border="0" width="100%" id="table2" cellspacing="0" cellpadding="0" height="20">
			<tr>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#BEC3BD">
				<p align="center">
				<img border="0" src="images/clear.gif" width="1" height="1"><b><font face="Tahoma" color="#5E5E5E" size="1">The 
				iRelocation Network � 2004-<?echo Date("Y");?> � <a href="privacy.php"><font color="#5E5E5E">Privacy Statement</font></a> � 
				<a href="terms.php"><font color="#5E5E5E">Terms &amp; Conditions</font></a></font></b></p>
				</td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E1E3E0" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="1">
		<img border="0" src="images/clear.gif" width="1" height="1"></td>
	</tr>
	<tr>
		<td height="3">
		<table border="0" width="100%" id="table1" cellspacing="0" cellpadding="0" height="3">
			<tr>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#CBBDB9">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td width="1">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
				<td bgcolor="#E7E0DF" width="25">
				<img border="0" src="images/clear.gif" width="1" height="1"></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

</body>

</html>
