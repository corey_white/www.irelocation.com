<?
	define(PIPE,"||");
	define(PIPE_LENGTH,2);
	$fh = fopen("testimonials.txt","r");
	$content = fread($fh,filesize("testimonials.txt"));
	
	$reviews = split("Submitted by",$content);
	
	
	
	$pos = findAllOccurences($content,PIPE);
	
	$words = "";
	$c = count($pos);
	for($i = 0; $i < $c; $i += 2)
	{
		$word = substr($content,$pos[$i]+PIPE_LENGTH,($pos[$i+1]-$pos[$i])-PIPE_LENGTH);
		if (substr_count($words,"-".$word."-") == 0)
			$words .= "-$word- ";
	}
	$words = str_replace("-","",trim($words));
	
	$words = split(" ",$words);
	
	
	echo "<reviews>\n";
	foreach($reviews as $r)
	{
		if (strlen(trim($r)) == 0)
			continue;
		$from = substr($r,0,strpos($r,"||mdcommay||")+12);
		
		list($name,$from) = split(" from ",$from);
		list($from,$date) = split(" on ",$from);
		$name = trim($name);
		$r = trim(substr($r,strpos($r,"||mdcommay||")+12));
		$title = trim(substr($r,0,strpos($r,"\"",3)+1),"\"");
		$r = substr($r,strpos($r,"\"",3)+1);
		
		echo "<review>\n<name>".trim(xmlentities($name)).
			"</name>\n<state>".trim(xmlentities($from)).
			"</state>\n<date>".trim(xmlentities($date))."</date>\n".
			"<title>".trim(xmlentities($title))."</title>\n<content>".
			trim(xmlentities($r))."</content>\n</review>\n";			
	}
	echo "</reviews>";
	
	function xmlentities ( $string )
	{
	   return str_replace ( 
		array ( '&', '"', "'", '<', '>' ), 
		array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), 
		$string 
		);
	}
	
	function findAllOccurences($Haystack, $needle, $limit=0)
	{
		$Positions = array();
		$currentOffset = 0;
		$count=0;
		while(($pos = strpos($Haystack, $needle, $offset)) && ($count < $limit || $limit == 0))
		{
			$Positions[] = $pos;
			$offset = $pos + strlen($needle);
			$count++;
		}
		return $Positions;
	}
	
	
	
	
	function webpage2txt($url)
	{
		$user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		
		$ch = curl_init();    // initialize curl handle
		curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);              // Fail on errors
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);    // allow redirects
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_PORT, 80);            //Set the port number
		curl_setopt($ch, CURLOPT_TIMEOUT, 15); // times out after 15s
		
		curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
		
		$document = curl_exec($ch);
		curl_close($ch);
		
		$search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
		'@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
		'@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
		'@<![\s\S]*?�[ \t\n\r]*>@',         // Strip multi-line comments including CDATA
		'/\s{2,}/',
		
		);
		
		$text = preg_replace($search, "\n", html_entity_decode($document));
		
		$pat[0] = "/^\s+/";
		$pat[2] = "/\s+\$/";
		$rep[0] = "";
		$rep[2] = " ";
		
		$text = preg_replace($pat, $rep, trim($text));
		
		return $text;
	}

?>

