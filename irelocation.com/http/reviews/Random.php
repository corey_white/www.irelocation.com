<?
	include "states.php";
	
	class Random
	{
		var $data;
		var $fnames;
		var $vtypes;
		var $vmodels;
	
		var $title;
		var $content;
		
		function assign($title,$content)
		{			
			$this->initialize();
			
			$this->title = str_replace("'","\'",$title);
			$this->content =  str_replace("'","\'",$content);		
		}
		
		function apply()
		{
			$title_keys = locate($this->title);
			$content_keys = locate($this->content);
		
			//echo "<!--- title:  ".print_r($title_keys,true)." --->\n";		
			//echo "<!--- content:  ".print_r($content_keys,true)." --->\n";
		
			foreach($title_keys as $key)
			{
				if (strlen(trim($key)) == 0) continue;
					
				$value = $this->data[$key];
				if ($value == "")
				{
					echo "Blank value for key: $key!<Br/>";
					exit();
				}
 				$this->title = str_replace("||".$key."||",$value,$this->title);
			}
		
			foreach($content_keys as $key)
			{
				if (strlen(trim($key)) == 0) continue;
				
				$value = $this->data[$key];
				if ($value == "")
				{
					echo "Blank value for key: $key!<Br/>";
					exit();
				}
				$this->content = str_replace("||".$key."||",$value,$this->content);
			}
		}
	
		function getRandomDate()
		{
			$year = rand(date("Y")-2,date("Y"));
			
			$maxmonth = 12;
			if ($year == date("Y"))
				$maxmonth = date("n");
				
		
			$month = rand(1,$maxmonth);
			$maxday = 31;
			if ($month == date("m"))
				$maxday = date("t");
			if($month < 10)
				$month = "0$month";
			else if ($month=="02")
				$maxday = 28;
			$day = rand(1,$maxday);
			if($day < 10)
				$day = "0$day";			
			return date("F jS",mktime(1,1,1,$month,$day,$year));
			
		}
	
		function Random()
		{
			$this->data = array();		
			$this->companies = easy_read("companies.txt");
			
			$this->fnames = easy_read("fnames.txt");
			$this->vtypes = easy_read("vtypes.txt");
			$this->vmodels = easy_read("vmodels.txt");
			$this->ed = easy_read("ed.txt");
			$this->ing = easy_read("ing.txt");
			$this->root = easy_read("root.txt");
			$this->shuffle();
		}
	
		function shuffle()
		{
			shuffle($this->vmodels);
			shuffle($this->fnames);
			shuffle($this->vtypes);
			shuffle($this->ed);
			shuffle($this->ing);
			shuffle($this->root);
			shuffle($this->companies);
		}
	
		function initialize()
		{
			$this->shuffle();
			
			$this->data['firstname'] = $this->fnames[0];
			$this->data['firstname1'] = $this->fnames[1];
			$this->data['firstname2'] = $this->fnames[2];
			$this->data['firstname3'] = $this->fnames[3];
			
			$locations = getRandomCity(2);
			
			$this->data['origin_city'] = $locations[0]['city'];
			$this->data['origin_state'] = $locations[0]['state'];
			
			$this->data['destination_city'] = $locations[1]['city'];
			$this->data['destination_state'] = $locations[1]['state'];
			
			$this->data['car'] = $this->vtypes[0];
			$this->data['car_type'] = $this->vtypes[1];			
			$this->data['car_model'] = $this->vmodels[0];
			
			$this->data['transport'] = $this->root[0];
			$this->data['transported'] = $this->ed[0];
			$this->data['transporting'] = $this->ing[0];
			$this->data['transporting'] = "transportation";
			//$this->data['company'] = "Fake Company";
			$days = array("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday");
			$months = array("January","February","March","April","May","June","July","August","September","October","November","December");
			shuffle($days);
			shuffle($months);
			
			$this->data['dayofweek'] = $days[0];
			$this->data['year'] = 2003 + intval(rand(1,3));
			$this->data['month_day_th'] = $this->getRandomDate();
			$this->data['month'] = $months[0];
			$this->data['state'] = getRandomState();
			
			list($this->data['comp_id'],$this->data['company']) = split(",",$this->companies[0],2);
			$this->data['company'] = ucwords(strtolower($this->data['company']));
			global $states;
			$this->data['state_id'] = $states[$this->data['destination_state']];
			$this->data['review'] = intval(rand(7,9));
		}
	}

?>
