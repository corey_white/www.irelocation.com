<?
	define(PIPE,"||");
	define(PIPE_LENGTH,2);
	
	$firstnames = array();
	
	function easy_read($file)
	{
		$fh = fopen($file,"r");
		$content = fread($fh,filesize($file));
		$lines = split("\n",$content);
		return $lines;	
	}


	function chunk($source,$tag)
	{	
		$start = strpos($source,"<".$tag);
		if ($start<0) return "";
		$source = trim(substr($source,$start));
		
		$end = strpos($source,$tag,strlen($tag));	
		//only find first instance of end tag.
		return substr($source,0,$end+strlen($tag)+1);			
	}
	
	function find($source, $tag)
	{
		$keyword = '<$tag>';
		$closer = '</$tag>';
		$start = strpos($source,$keyword);
		if ($start < 0) return "";
		
		$end = strpos($source,$closer,$start+strlen($keyword));
		if ($end < $start) return "";
		$start += strlen($keyword);
		$result = substr($source,$start,($end-$start));
		return $result;
	}
	
	function getRandomDate()
	{
		$year = rand(date("Y")-2,date("Y"));
		
		$maxmonth = 12;
		if ($year == date("Y"))
			$maxmonth = date("n");
			
	
		$month = rand(1,$maxmonth);
		$maxday = 31;
		if ($month == date("m"))
			$maxday = date("t");
		if($month < 10)
			$month = "0$month";
		else if ($month=="02")
			$maxday = 28;
		$day = rand(1,$maxday);
		if($day < 10)
			$day = "0$day";			
		return date("F jS, Y",mktime(1,1,1,$month,$day,$year));
		
	}
	
	function locate($content)
	{
		$pos = findAllOccurences($content,PIPE);
	
		$words = "";
		$c = count($pos);
		for($i = 0; $i < $c; $i += 2)
		{
			$word = substr($content,$pos[$i]+PIPE_LENGTH,($pos[$i+1]-$pos[$i])-PIPE_LENGTH);
			if (substr_count($words,"-".$word."-") == 0)
				$words .= "-$word- ";
		}
		$words = str_replace("-"," ",trim($words));
		
		$words = split(" ",trim($words));
		
		$result = array();
		
		foreach($words as $word)
		{
			$word = trim($word);
			if (strlen($word) == 0)
				continue;
			$result[] = $word;
		}
		
		return $result;	
	}
	
	


	function findAllOccurences($Haystack, $needle, $limit=0)
	{
		$Positions = array();
		$currentOffset = 0;
		$count=0;
		while(($pos = strpos($Haystack, $needle, $offset)) > -1 && ($count < $limit || $limit == 0))
		{
			$Positions[] = $pos;
			$offset = $pos + strlen($needle);
			$count++;
		}
		return $Positions;
	}

	function getRandomState($count=1)
	{
		$sql = "select state from usautotransport.zip_codes  ".
				" group by state order by rand() limit $count;";
		$rs = new mysql_recordset($sql);
		
		$states = array();
		while($rs->fetch_array())
			$cities[] = $rs->myarray['state'];
		
		return $states;	
	}

	function getRandomCity($count=1)
	{
		$sql = "select city,state,state_code from usautotransport.zip_codes  ".
				" group by state order by rand() limit $count;";
		$rs = new mysql_recordset($sql);
		
		$cities = array();
		while($rs->fetch_array())
			$cities[] = $rs->myarray;
		
		return $cities;	
	}

	
	
	include "../inc_mysql.php";
	$file = "reviews.xml";
	include "Random.php";
	$fh = fopen($file,"r");
	$reviews = fread($fh,filesize($file));
	include "xml2Array.php";
	$xml = new xml2Array();
	$data = $xml->parse($reviews);
	$data = $data[0]['children'];
	
	$rand = new Random();
	$count = 0;
	
	echo "insert into autotransportreviews.reviews ".
	" (company, rating, reviewer, state, ".
	" title, review, active) ".
	" values";
	
	foreach($data as $review)
	{	
		//print_r($review);
		$title = $review['children'][0]['tagData'];
		$content = $review['children'][1]['tagData'];
		
		$rand->assign($title,$content);
		$rand->apply();
		echo "(".$rand->data['comp_id'].','.$rand->data['review'].",'".$rand->data['firstname']."',".
				$rand->data['state_id'].",'".$rand->title."','".$rand->content."','1'), \n";
		$count++;
	}	
	shuffle($data);
	foreach($data as $review)
	{	
		//print_r($review);
		$title = $review['children'][0]['tagData'];
		$content = $review['children'][1]['tagData'];
		
		$rand->assign($title,$content);
		$rand->apply();
		echo "(".$rand->data['comp_id'].','.$rand->data['review'].",'".$rand->data['firstname']."',".
				$rand->data['state_id'].",'".$rand->title."','".$rand->content."','1'), \n";
		$count++;
	}	
	
	echo "<Br/><Br/>$count total reviews";
	$keyword = locate($reviews);
	
	//print_r($keyword);
?>