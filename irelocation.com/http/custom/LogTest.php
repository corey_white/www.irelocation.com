<?php
/*
    Set LOG4PHP_* constants here 
*/

define(LOG4PHP_DIR,"log4php");
define(LOG4PHP_CONFIGURATION,LOG4PHP_DIR . '/log4php.xml');
require_once(LOG4PHP_DIR . '/LoggerManager.php');

/*
    In a class
*/
class Log4phpTest {
    /*
        Your public and private vars
    */
    var $_logger;
    
    function Log4phpTest()
    {
        $this->_logger =& LoggerManager::getLogger('Log4phpTest');
        $this->_logger->debug('Hello!');
    }

}

function Log4phpTestFunction()
{
    $logger =& LoggerManager::getLogger('Log4phpTestFunction');
    $logger->debug('Hello again!');    
}

/*
    Your PHP code
*/

Log4phpTestFunction();

//Safely close all appenders with...

LoggerManager::shutdown();

?>