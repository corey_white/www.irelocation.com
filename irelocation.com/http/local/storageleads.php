<?
	define(LIVE,false);
	if(LIVE)
		define(URL,"http://www.vanlines.com/xml/receive_lead.asp");
	else
		define(URL,"http://www.vanlines.com/xml_test/test_lead.asp");		

	include_once "../inc_mysql.php";

	function sendStorageLead($array)
	{
		$xml = build_vanlines_xml($array);
		echo "<textarea cols='50' rows='20'>$xml</textarea><br/>";
		$ch=curl_init(URL);				
		$this_header = array("Content-Type: x-www-form-urlencoded; charset=utf-8");
		//curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$xmlresponse=curl_exec($ch);		
		echo "Response: ".$xmlresponse."<Br/>\n";
		curl_close($ch);					
		if (is_numeric($xmlresponse) && $xmlresponse > 0)//valid
			$success = 1;			
		else
			$success = 0;
		echo "Success: ".$success."<Br/>\n";
	}

	function build_vanlines_xml($array)
	{
		extract($array);
		list($fname,$lname) = split(" ",$name,2);
		
		if ($fname == "")
			$fname = $name;
		
		if ($lname == "")
			$lname = "n-a";

		$startat = strpos($comments,"Furnished Rooms:")+16;
		$endat = strpos($comments,"Elevator") - $startat;
		//echo $startat." ".$endat."<br/>";
		$size = substr($comments,$startat,$endat);
		$size = str_replace("\n","",$size);
		$size = str_replace(" ","",$size);
		$size = changeSize($size);
		$date = split("-",$est_move_date);
		$est_move_date = $date[1]."/".$date[2]."/".$date[0];	
		
		$xml = "<VLData><Source><Company>www.irelocation.com</Company><LeadId>$quote_id</LeadId></Source>";
		$xml .= "<LeadDetail><ContactDetails><FirstName>".xmlentities($fname)."</FirstName><LastName>".xmlentities($lname)."</LastName>";
		$xml .= "<Email>".xmlentities($email)."</Email><DayPhone>$phone_home</DayPhone><EvePhone>$phone_work</EvePhone><CellPhone></CellPhone></ContactDetails>";
		$xml .= "<ServiceType>Storage</ServiceType><RequestDetails><City>".xmlentities($origin_city)."</City>";
		$xml .= "<State>$origin_state</State><Zip>$origin_zip</Zip>";
		$xml .= "<StartDate>$est_move_date</StartDate><EstimatedTime>$storagetime</EstimatedTime><EstimatedSize>$size</EstimatedSize></RequestDetails>";
		$xml .= "<Comments></Comments></LeadDetail></VLData>";
		return $xml;
	}
	
	if (!function_exists("xmlentities"))
	{
		function xmlentities ( $string )
		{
		   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
		}
	}
	
	function changeSize($insize)
	{
		$insize = strtolower($insize);
		if ($insize == "4+")
			$size = "4";
		else
			$size = substr($insize,0,strpos($insize,"bed"));
		//echo $insize." ".$size;
		
		switch ($size)
		{
			case "no":
				return "5x5";
				break;
			case "":
				return "5x8";
				break;
			case "1":
			case "one": 
				return "5x10";
			case "2":
			case "two":
				return "8x10";
			case "3":
			case "three":
				return "10x10";
			case "4":
			case "four":
				return "10x12";
			default:
				return "10x15";
		}
	}
	
	$array = array();
	$array[name] = "Testy McTesterson";
	$array[comments] = "Type of Move: Household Move
Type of Home: House
Furnished Rooms: one bedroom
Elevator: no
Stairs: yes

Comments/Unique Items:  ";
	$array[est_move_date] = '2008-01-31';
	$array[quote_id] = '1311891';
	$array[phone_home] = '4807857400';
	$array[phone_work] = '4807636980';
	$array[origin_city] = 'Tempe';
	$array[origin_state] = 'AZ';
	$array[origin_zip] = '85282';
	$array[destination_city] = 'New York';
	$array[destination_state] = 'NY';
	$array[destination_zip] = '10001';
	$array[email] = 'test@test.com';
	$array[storagetime] = '1 month';
	echo "<pre>".print_r($array,true)."</pre>";
	sendStorageLead($array);

	/*
		<VLData>
			<Source>
				<Company>www.irelocation.com</Company>
				<LeadId>1311891</LeadId>
			</Source>
			<LeadDetail>
				<ContactDetails>	
					<FirstName>Testy</FirstName>
					<LastName>McTesterson</LastName>
					<Email>test@test.com</Email>
					<DayPhone>4807857400</DayPhone>
					<EvePhone>4807636980</EvePhone>
					<CellPhone></CellPhone>
				</ContactDetails>
				<ServiceType>Storage</ServiceType>
				<RequestDetails>
					<MovingFrom>
						<City>Tempe</City>
						<State>AZ</State>
						<Zip>85282</Zip>
					</MovingFrom>
					<MovingTo>
						<City>New York</City>
						<State>NY</State>
						<Zip>10001</Zip>
					</MovingTo>
					<TypeOfMove>1 BR Small 3000 lbs</TypeOfMove>
					<MovingDate>01/31/2008</MovingDate>
				</RequestDetails>
				<Comments></Comments>
			</LeadDetail>
		</VLData>
	*/	

?>