<?php
	

	include_once "../inc_mysql.php";
	include_once "serviceobjects.php";
	include_once "../cronlib/.functions.php";
	//include_once "../reporting/thankyou_function.php";
	
	include_once "../moving/CompanyClasses.php";
	include_once "../logging/Logger.php";
	$localLog = new Logger("local.txt");
	
	//mail("code@irelocation.com","Cron Local",date("YmdHis"),"From: The iRelocation Network <no_reply@irelocation.com>\n");	
	define(VANLINES,1516);
	if (!defined(LIVE)) define(LIVE,true);
	updateCronStatus("local");
	
/*
	
	<VLData>
		<Source>
			<Company>www.irelocation.com</Company>
			<LeadId>123456</LeadId>
		</Source>
		<LeadDetail>
			<ContactDetails>
				<FirstName>Igor</FirstName>
				<LastName>Nasonovzzz</LastName>
				<Email>igorn@vanlines.com</Email>
				<DayPhone>2121112222</DayPhone>
				<EvePhone>7185553232</EvePhone>
				<CellPhone></CellPhone>
			</ContactDetails>
			<ServiceType>FullService</ServiceType>
			<RequestDetails>
				<MovingFrom>
					<City>AGANA HEIGHTS</City>
					<State>GU</State>
					<Zip>96919</Zip>
				</MovingFrom>
				<MovingTo>
					<City>CHRISTIANSTED</City>
					<State>VI</State>
					<Zip>00820</Zip>
				</MovingTo>
				<TypeOfMove>4 Large Bedrooms 12000 lbs</TypeOfMove>
				<MovingDate>08/01/2006</MovingDate>
			</RequestDetails>
			<Comments>This is test (from vanlines.com )</Comments>
		</LeadDetail>
	</VLData>
	number 5-6 digits = success
	-1 invalid
	-2 duplicate
	Duplcates: email, service, move_date


	dirty string = <VLData><Source><Company>www.irelocation.com</Company><LeadId>123456</LeadId></Source><LeadDetail><ContactDetails>
					<FirstName>Igor</FirstName><LastName>Nasonovzzz</LastName><Email>igorn@vanlines.com</Email>
					<DayPhone>2121112222</DayPhone><EvePhone>7185553232</EvePhone><CellPhone></CellPhone></ContactDetails>
					<ServiceType>FullService</ServiceType><RequestDetails>
					<MovingFrom><City>AGANAHEIGHTS</City><State>GU</State><Zip>96919</Zip></MovingFrom>
					<MovingTo><City>CHRISTIANSTED</City><State>VI</State><Zip>00820</Zip></MovingTo>
					<TypeOfMove>4LargeBedrooms12000lbs</TypeOfMove>
					<MovingDate>08/01/2006</MovingDate>
					</RequestDetails>
					<Comments>Thisistest(fromvanlines.com)</Comments>
					</LeadDetail></VLData>
	
	*/
	
	function build_vanlines_xml($array)
	{
		extract($array);
		list($fname,$lname) = split(" ",$name,2);
		
		if ($fname == "")
			$fname = $name;
		
		if ($lname == "")
			$lname = "n-a";

		$startat = strpos($comments,"Furnished Rooms:")+16;
		if (substr_count($source,"smq") > 0)
			$size = $endat = strpos($comments,"Comments") - $startat;
		else
			$endat = strpos($comments,"Elevator") - $startat;
		echo $startat." ".$endat."<br/>";
		$size = substr($comments,$startat,$endat);
		$size = str_replace("\n","",$size);
		$size = str_replace(" ","",$size);
		$size = changeSize($size);
		$date = split("-",$est_move_date);
		$est_move_date = $date[1]."/".$date[2]."/".$date[0];
		
		//$newcomments = substr($comments,strpos($comments,"comments:"));
		if (substr_count($array['source'],"tmc") > 0)
			$campaign = "www.TopMovingCompanies.com";
		else
			$campaign = "www.irelocation.com";
		
		$xml = "<VLData><Source><Company>$campaign</Company><LeadId>$quote_id</LeadId></Source>";
		$xml .= "<LeadDetail><ContactDetails><FirstName>".xmlentities($fname)."</FirstName><LastName>".xmlentities($lname)."</LastName>";
		$xml .= "<Email>".xmlentities($email)."</Email><DayPhone>$phone_home</DayPhone><EvePhone>$phone_work</EvePhone><CellPhone></CellPhone></ContactDetails>";
		$xml .= "<ServiceType>".((substr_count($source,"smq") > 0)?"ULoad":"FullService")."</ServiceType><RequestDetails><MovingFrom><City>".xmlentities($origin_city)."</City>";
		$xml .= "<State>$origin_state</State><Zip>$origin_zip</Zip></MovingFrom><MovingTo><City>".xmlentities($destination_city)."</City>";
		$xml .= "<State>$destination_state</State><Zip>$destination_zip</Zip></MovingTo>";
		$xml .= "<TypeOfMove>".xmlentities($size)."</TypeOfMove><MovingDate>$est_move_date</MovingDate></RequestDetails>";
		$xml .= "<Comments></Comments></LeadDetail></VLData>";
		if (substr_count($array['source'],"tmc") > 0)
			mail("code@irelocation.com","Vanlines XML",$xml);
		return $xml;
	}
	
	if (!function_exists("xmlentities"))
	{
		function xmlentities ( $string )
		{
		   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
		}
	}

	//delete fake leads.
	$sql = "delete from movingdirectory.quotes_local where name like '% test' or name like 'test%' or email like 'test@test.com'";
	$rs = new mysql_recordset($sql);
	
	
	$sql = "select * from movingdirectory.quotes_local where lead_ids = '' ".
			" and email not like '%randomkeystrokes@gmail.com%' limit 20 ";	

	echo "Main Select: ".$sql."<Br/>";
	$rs = new mysql_recordset($sql);
	
	$vanlines_url = "http://www.vanlines.com/xml/receive_lead.asp";
		
	while ($rs->fetch_array())
	{
		//this is really just a vanlines cron, so they got more than
		//just local, so here we check, if it is local, now we have to 		
		//look for Affiliates...
		
		/********************************************
		 * NEW AFFILIATE CODING STARTED
		 * 
		 */
		
		$local = ($rs->myarray['origin_state'] == $rs->myarray['destination_state']);
		$dzip = $rs->myarray['destination_zip'];
		$ozip = $rs->myarray['origin_zip'];
		if (($dlead_id = testForAffiliate($dzip)) != 0 || ($olead_id = testForAffiliate($ozip)) != 0)
		{
			//send to affiliate.
			$localLog->debug("affiliate found: $dlead_id,$olead_id");
			$result = sendToAffiliate($dlead_id,$rs->myarray);
			if ($result == 1)
			{
				$localLog->debug("lead sent succesfully to affiliate.");			
				continue;
			}	
			else
			{
				$localLog->error("lead NOT SENT!");				
				mail("code@irelocation.com","AFF Local Lead",
				"Lead was supposed to go to $dlead, but they didnt want it or" .
				"they aren't setup correctly. quote_id: ".$rs->myarray['quote_id']);
				//an error happened.
			} 	
		} 
		
		/**
		 * NEW AFFILIATE CODING ENDED
		 * 
		 ********************************************/
		$xml = build_vanlines_xml($rs->myarray); 
		
		$local = ($rs->myarray['origin_state'] == $rs->myarray['destination_state']);
		if (!$local)
			$response = validateLeadQuality($rs->myarray);
		else
			$response = true;
			
		if ($response)
		{			
			echo "<textarea cols='50' rows='20'>$xml</textarea><br/>";			
			$ch=curl_init($vanlines_url);							
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);			
			curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			$xmlresponse=curl_exec($ch);		
			curl_close($ch);					
			if (is_numeric($xmlresponse) && $xmlresponse > 0)//valid
				$success = 1;			
			else
				$success = 0;
			
			updateXML($rs->myarray["quote_id"],$xml,$xmlresponse,$success);
			
			if (LIVE)
			{
				$array = $rs->myarray;
				$array['cat_id'] = 2;				
				thankyouEmail($array);
			}
			
		}
		else
		{
			$quote_id = $rs->myarray["quote_id"];
			$sql = "update movingdirectory.quotes_local set ".
					" lead_ids = 'SO', received = '".date("YmdHis")."' where ".
					" quote_id = $quote_id";
			echo "$sql<br/> ";
			mail("code@irelocation.com","vanlines lead failed validation",
				 $quote_id);
			$rs2 = new mysql_recordset($sql);	
		}				
	}

	/* Added 1/26/08 5pm by David Haveman
	 * 
	 * give me a zip code and i will return the lead_id of the 
	 * company that wants this lead if there is such a company 
	 * other than vanlines. i also print out a message if there
	 * are more than one companies that want this lead.
	 * 
	 * if no other company wants this lead, i return 0;
	 * 
	 * If I return a 0, send the lead to vanlines, if not send
	 *  it to the other company whose lead id i give you.
	 * 
	 * setup formats for each of the moving partners in the 
	 * movingdirectory.lead_format table with their lead_id
	 * and information just like the main movers and the auto guys
	 */

	function testForAffiliate($zip)
	{
		$sql = "select * from marble.moving_zip_codes where zip = '".$zip."' and extra = 'local';";
		$rs = new mysql_recordset($sql);
		if ($rs->fetch_array())
			return $rs->myarray['lead_id'];		
		else
			return 0;	
	}
	/********************************************
	 * NEW AFFILIATE CODING STARTED
	 * 
	 */
	function sendToAffiliate($lead_id,$myarray)
	{		
		global $localLog;
		$localLog->debug("sending lead to $lead_id");
		$data = new MovingQuoteData($myarray);
		$data->isLocal = true;
		$company = getLocalCompanySQL($lead_id);
		echo "company sql = $company<br />";
		$lead_rs = new mysql_recordset($company);
		if ($lead_rs->fetch_array())
		{				
			$data->process($leads_rs->myarray);
			$localLog->debug("lead sent, updating lead_id field.");
			updateQuoteWithLeadId($myarray['quote_id'],$lead_id);
			//$leads_rs->close();
			return 1;
		}
		else
			return 0;	
	}

	function updateQuoteWithLeadId($quote_id,$lead_id)
	{		
		$sql = " update movingdirectory.quotes_local set lead_ids = '$lead_id' " .
				" where quote_id = '$quote_id' ";
		$rs = new mysql_recordset($sql);
		
		global $localLog;	
		$localLog->debug("lead_id field updated.");
	}

	function getLocalCompanySQL($lead_id)
	{		
		return "select 	format_id, 
			(select email from movingdirectory.rules 
				where lead_id = lf.lead_id) as 'email',
			lf.lead_id,
			format_type,
			lf.cat_id, 
			if(lead_body < 0,
				(select lead_body from movingdirectory.lead_format 
					where lead_id = lf.lead_body),
				lf.lead_body) as 'lead_body' 
		from
			movingdirectory.lead_format as lf 
			join 
			movingdirectory.directleads as d
			on d.lead_id = lf.lead_id 
			join 
			movingdirectory.campaign as ca
			on d.lead_id = ca.lead_id	
		where	
			ca.active
			and lf.cat_id = 2
			and ca.month = '".date("Ym")."'
			and ca.site_id = 'local'
			and ca.lead_id = '$lead_id'
		group by	
			ca.lead_id";			
	}


	/**
	 * NEW AFFILIATE CODING ENDED
	 * 
	 *********************************************/

	function changeSize($insize)
	{
		$insize = strtolower($insize);
		if ($insize == "4+")
			$size = "4";
		else
			$size = substr($insize,0,strpos($insize,"bed"));
		//echo $insize." ".$size;
		
		switch ($size)
		{
			case "no":
				return "Partial Home 500-1000 lbs";
				break;
			case "":
				return "Studio 1500 lbs";
				break;
			case "1":
			case "one": 
				if (strlen($insize) > 20)
					return "1 BR Large 4000 lbs";
				else
					return "1 BR Small 3000 lbs";
			case "2":
			case "two":
				if (strlen($insize) > 20)
					return "2 BR Large 6500 lbs";
				else
					return "2 BR Small 4500 lbs";
			case "3":
			case "three":
				if (strlen($insize) > 23)
					return "3 BR Large 9000 lbs";
				else
					return "3 BR Small 8000 lbs";
			case "4":
			case "four":
				if (strlen($insize) > 23)
					return "4 BR Large 12000 lbs";
				else
					return "4 BR Small 10000 lbs";
			default:
				return "Over 12000 lbs";
		}
	}

	function updateXML($quote_id,$xml,$xmlresponse,$success)
	{
		$mytime = date("YmdHis");
		$sql = "update movingdirectory.quotes_local set xml = '$xml', xmlresponse = '$xmlresponse', success = $success, lead_ids = '".VANLINES."', received = '$mytime' where quote_id = $quote_id";
		echo "<Br/>\n".$sql."\n<br/>";
		$rs = new mysql_recordset($sql);	
	}
	
	updateCronStatus("local","ended");
	checkCronStatus("topautoleads","local");
	//checkCronStatus("topautoshippers","local");
	
	
?>