<?php
	include_once "../inc_mysql.php";
	include "../cronlib/.functions.php";
	//mail("code@irelocation.com","Cron Local",date("YmdHis"),"From: The iRelocation Network <no_reply@irelocation.com>\n");	
	define(VANLINES,1516);
	
	updateCronStatus("local");
	
/*
	
	<VLData>
		<Source>
			<Company>www.irelocation.com</Company>
			<LeadId>123456</LeadId>
		</Source>
		<LeadDetail>
			<ContactDetails>
				<FirstName>Igor</FirstName>
				<LastName>Nasonovzzz</LastName>
				<Email>igorn@vanlines.com</Email>
				<DayPhone>2121112222</DayPhone>
				<EvePhone>7185553232</EvePhone>
				<CellPhone></CellPhone>
			</ContactDetails>
			<ServiceType>FullService</ServiceType>
			<RequestDetails>
				<MovingFrom>
					<City>AGANA HEIGHTS</City>
					<State>GU</State>
					<Zip>96919</Zip>
				</MovingFrom>
				<MovingTo>
					<City>CHRISTIANSTED</City>
					<State>VI</State>
					<Zip>00820</Zip>
				</MovingTo>
				<TypeOfMove>4 Large Bedrooms 12000 lbs</TypeOfMove>
				<MovingDate>08/01/2006</MovingDate>
			</RequestDetails>
			<Comments>This is test (from vanlines.com )</Comments>
		</LeadDetail>
	</VLData>
	number 5-6 digits = success
	-1 invalid
	-2 duplicate
	Duplcates: email, service, move_date


	dirty string = <VLData><Source><Company>www.irelocation.com</Company><LeadId>123456</LeadId></Source><LeadDetail><ContactDetails>
					<FirstName>Igor</FirstName><LastName>Nasonovzzz</LastName><Email>igorn@vanlines.com</Email>
					<DayPhone>2121112222</DayPhone><EvePhone>7185553232</EvePhone><CellPhone></CellPhone></ContactDetails>
					<ServiceType>FullService</ServiceType><RequestDetails>
					<MovingFrom><City>AGANAHEIGHTS</City><State>GU</State><Zip>96919</Zip></MovingFrom>
					<MovingTo><City>CHRISTIANSTED</City><State>VI</State><Zip>00820</Zip></MovingTo>
					<TypeOfMove>4LargeBedrooms12000lbs</TypeOfMove>
					<MovingDate>08/01/2006</MovingDate>
					</RequestDetails>
					<Comments>Thisistest(fromvanlines.com)</Comments>
					</LeadDetail></VLData>
	
	*/
	
	function build_vanlines_xml($array)
	{
		extract($array);
		list($fname,$lname) = split(" ",$name,2);
		
		if ($fname == "")
			$fname = $name;
		
		if ($lname == "")
			$lname = "n-a";

		$startat = strpos($comments,"Furnished Rooms:")+16;
		if (substr_count($source,"smq") > 0)
			$size = $endat = strpos($comments,"Comments") - $startat;
		else
			$endat = strpos($comments,"Elevator") - $startat;
		echo $startat." ".$endat."<br/>";
		$size = substr($comments,$startat,$endat);
		$size = str_replace("\n","",$size);
		$size = str_replace(" ","",$size);
		$size = changeSize($size);
		$date = split("-",$est_move_date);
		$est_move_date = $date[1]."/".$date[2]."/".$date[0];
		
		//$newcomments = substr($comments,strpos($comments,"comments:"));
		if (substr_count($array['source'],"tmc") > 0)
			$campaign = "www.TopMovingCompanies.com";
		else
			$campaign = "www.irelocation.com";
		
		$xml = "<VLData><Source><Company>$campaign</Company><LeadId>$quote_id</LeadId></Source>";
		$xml .= "<LeadDetail><ContactDetails><FirstName>".xmlentities($fname)."</FirstName><LastName>".xmlentities($lname)."</LastName>";
		$xml .= "<Email>".xmlentities($email)."</Email><DayPhone>$phone_home</DayPhone><EvePhone>$phone_work</EvePhone><CellPhone></CellPhone></ContactDetails>";
		$xml .= "<ServiceType>".((substr_count($source,"smq") > 0)?"ULoad":"FullService")."</ServiceType><RequestDetails><MovingFrom><City>".xmlentities($origin_city)."</City>";
		$xml .= "<State>$origin_state</State><Zip>$origin_zip</Zip></MovingFrom><MovingTo><City>".xmlentities($destination_city)."</City>";
		$xml .= "<State>$destination_state</State><Zip>$destination_zip</Zip></MovingTo>";
		$xml .= "<TypeOfMove>".xmlentities($size)."</TypeOfMove><MovingDate>$est_move_date</MovingDate></RequestDetails>";
		$xml .= "<Comments></Comments></LeadDetail></VLData>";
		if (substr_count($array['source'],"tmc") > 0)
			mail("code@irelocation.com","Vanlines XML",$xml);
		return $xml;
	}
	
	if (!function_exists("xmlentities"))
	{
		function xmlentities ( $string )
		{
		   return str_replace ( array ( '&', '"', "'", '<', '>' ), array ( '&amp;' , '&quot;', '&apos;' , '&lt;' , '&gt;' ), $string );
		}
	}

	
	$sql = "select * from movingdirectory.quotes_local where quote_id = 15071 ";
	echo $sql."<Br/>";
	$rs = new mysql_recordset($sql);
	
	$vanlines_url = "http://www.vanlines.com/xml/receive_lead.asp";
	
	while ($rs->fetch_array())
	{
		$xml = build_vanlines_xml($rs->myarray); 
		
		echo "<textarea cols='50' rows='20'>$xml</textarea><br/>";
		
		$ch=curl_init($vanlines_url);				
		//$this_header = array("Host: www.protectionone.com", "Content-Type: text/xml; charset=utf-8","Content-Length: ".strlen($body), "SOAPAction: \"http://tempuri.org/ProspectUpdateDMZ/Pro1/ProtectionOne\"");
		//curl_setopt($ch, CURLOPT_HEADER, 1);	
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		//curl_setopt($ch, CURLOPT_HTTPHEADER, $this_header );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
		curl_setopt($ch, CURLOPT_TIMEOUT, 3000);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		$xmlresponse=curl_exec($ch);		
		curl_close($ch);					
		if (is_numeric($xmlresponse) && $xmlresponse > 0)//valid
			$success = 1;			
		else
			$success = 0;
		
		updateXML($rs->myarray["quote_id"],$xml,$xmlresponse,$success);
		
	}

	function changeSize($insize)
	{
		$insize = strtolower($insize);
		if ($insize == "4+")
			$size = "4";
		else
			$size = substr($insize,0,strpos($insize,"bed"));
		//echo $insize." ".$size;
		
		switch ($size)
		{
			case "no":
				return "Partial Home 500-1000 lbs";
				break;
			case "":
				return "Studio 1500 lbs";
				break;
			case "1":
			case "one": 
				if (strlen($insize) > 20)
					return "1 BR Large 4000 lbs";
				else
					return "1 BR Small 3000 lbs";
			case "2":
			case "two":
				if (strlen($insize) > 20)
					return "2 BR Large 6500 lbs";
				else
					return "2 BR Small 4500 lbs";
			case "3":
			case "three":
				if (strlen($insize) > 23)
					return "3 BR Large 9000 lbs";
				else
					return "3 BR Small 8000 lbs";
			case "4":
			case "four":
				if (strlen($insize) > 23)
					return "4 BR Large 12000 lbs";
				else
					return "4 BR Small 10000 lbs";
			default:
				return "Over 12000 lbs";
		}
	}

	function updateXML($quote_id,$xml,$xmlresponse,$success)
	{
		$mytime = date("YmdHis");
		$sql = "update movingdirectory.quotes_local set xml = '$xml', xmlresponse = '$xmlresponse', success = $success, lead_ids = '".VANLINES."', received = '$mytime' where quote_id = $quote_id";
		echo "<Br/>\n".$sql."\n<br/>";
		$rs = new mysql_recordset($sql);	
	}
	
	updateCronStatus("local","ended");
	checkCronStatus("topautoleads","local");
	checkCronStatus("topautoshippers","local");
	
	
?>