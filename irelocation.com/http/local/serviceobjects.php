<?
	define(THRESHOLD,70);
	define(ADDRESS_COMP,1.23);
	define(WIRELESS_COMP,1.5);
	
	include_once "../inc_mysql.php";
	require_once('../nusoap/nusoap.php');
	
	function validateLead($params)
	{
		//Set Up Soap Client for LeadValidation
		$soapClient = new SoapClient( SO_WSDL_URL, "wsdl" );
		
		//Set up GetContactInfo's parameters
		
		
		//Call ValidateLead Operation
		$proxy = $soapClient->getProxy();
		$startTime = time() + microtime();
		$result = $proxy->ValidateLead($params);
		$queryTime = time() + microtime() - $startTime;
		$result = $result['ValidateLeadResult'];
		
		return $result;
	}
	
	
	define(SO_IRELO_KEY,"WS40-MPV1-FLT4");
	define(SO_WSDL_URL,"http://ws.serviceobjects.com/lv/leadvalidation.asmx?WSDL");
	
	function getParamsFromArray($rsarray)
	{
		//Setup Parameter Values
		$name = $rsarray['name'];
		$city = $rsarray['origin_city'];
		$state = $rsarray['origin_state'];
		$zip = $rsarray['origin_zip'];
		$phone = $rsarray['phone_home'];
		
		if ($rsarray['phone_work'] != "")
		{
			$phone2 = $rsarray['phone_work'];
			$type = 'full2p';
		}
		else
			$type = 'full1p'; //full2p if 2 phone numbers are specified	
			
		$email = $rsarray['email'];
		if ($rsarray['remote_ip'] == "")
			$ip = '70.56.176.249';
		else
			$ip = $rsarray['remote_ip'];
		
		$key = SO_IRELO_KEY;
	
		$params['Name'] = $name;
		$params['Address1'] = '';
		$params['Address2'] = '';
		$params['City'] = $city;
		$params['State'] = $state;
		$params['Zip'] = $zip;
		$params['Country'] = 'USA';
		$params['Phone1'] = $phone;
		$params['Phone2'] = $phone2;
		$params['Email'] = $email;
		$params['IP'] = $ip;
		$params['TestType'] = $type;
		$params['LicenseKey'] = $key;
		
		return $params;
	}
	
	function getParams($quote_id)
	{
		$sql = "select * from movingdirectory.quotes_local where ".
				" quote_id = '".$quote_id."'";
		echo "SO.getParams:".$sql."<br/>";
		$rs = new mysql_recordset($sql);		
		$rs->fetch_array();
		
		return getParamsFromArray($rs->myarray);	
	}
		
	function validateLeadQuality($rsarray)
	{
		$params = getParamsFromArray($rsarray);
		$result = validateLead($params);
		return processResults($rsarray['quote_id'],$result);
	}
	
	function processResults($quote_id,$result)
	{
		$result['OverallCertainty'] += 20;//no address..
		$overall = $result['OverallCertainty'];
		
		if ($result['Phone1LineOut'] == "WIRELESS")
		{
			$overall += 20;//wireless numbers are ok.
		}
		
		mail("code@irelocation.com","S.O. VAN",print_r($result,true));		
	
		$strresult = print_r($result,true);
		$strresult = ereg_replace("(\[|\])","",$strresult);
		$strresult = ereg_replace("  *"," ",$strresult);
		$strresult = ereg_replace("'","\'",$strresult);
		if (is_array($strresult))
			echo "WHY IS IT AN ARRAY!!<br/>";
		$sql = "insert into movingdirectory.so_results ".
		"(quote_id, so_score, adjusted_score, rawata) values ".
		"('$quote_id', '".$result['OverallCertainty']."','".$overall."'".
		",'".$strresult."')";
		echo "SO Results: ".$sql."<br/>";
		$rs = new mysql_recordset($sql);
		$rs->close();
		
		return ($overall > THRESHOLD);
	}
?>