<?
	include("../inc_mysql.php");

	

	function distance($zip1,$zip2)
	{
		$latitudes = array();
		$longitudes = array();
		
		$PI = atan2(1,1) * 4;
		
		if (!is_array($zip1))
		{
			$rs = new mysql_recordset("select * from vacationhomes.zip_codes where zip = '$zip1'");
			if ($rs->fetch_array())
			{
				extract($rs->myarray);
				if ($latitude < 0)
					$latitude *= -1;
				if ($longitude < 0)
					$longitude = floatval($longitude) * -1;
					
				
				$latitudes[0] = $latitude * ($PI / 180);
				$longitudes[0] = $longitude * ($PI / 180);
			}
		}
		else
		{
			$latitudes[0] = $zip1[0]* ($PI / 180);;
			$longitudes[0] = $zip1[1] * ($PI / 180);;		
		}
		
		if (!is_array($zip2))
		{
			$rs = new mysql_recordset("select * from vacationhomes.zip_codes where zip = '$zip2'");
			if ($rs->fetch_array())
			{
				extract($rs->myarray);	
				
				if ($latitude < 0)
					$latitude *= -1;
				if ($longitude < 0)
					$longitude = floatval($longitude) * -1;
		
				
				$latitudes[1] = $latitude * ($PI / 180);
				$longitudes[1] = $longitude * ($PI / 180);
			}
		}
		else
		{
			$latitudes[1] = $zip2[0] * ($PI / 180);
			$longitudes[1] = $zip2[1] * ($PI / 180);		
		}
		
	
		$PI = atan2(1,1) * 4;
		//echo $PI."<Br/>";
		$r = 3963.1;
		
		$distance = acos( 						
						 cos($latitudes[0]) * cos($longitudes[0]) *cos($latitudes[1]) * cos($longitudes[1]) + 
						 cos($latitudes[0]) * sin($longitudes[0]) *cos($latitudes[1]) * sin($longitudes[1]) +
						 sin($latitudes[0]) * sin($latitudes[1]) 
					  ) * $r;
			
		return $distance;
	}
	
	
	$sql = "select v.latitude, abs(v.longitude) as longitude, dz.latitude as latitude2, abs(dz.longitude) as longitude2, l.quote_id, l.destination_zip from 
	vacationhomes.zip_codes as v 
	join irelocation.leads_selfmove 
	as l on l.origin_zip = v.zip and v.city = l.origin_city
	join vacationhomes.zip_codes as dz on l.destination_zip = dz.zip
where
	l.origin_state = l.destination_state
	and l.origin_zip != l.destination_zip
	and l.destination_zip != ''
group by l.quote_id;";

	$count = 0;
	$rs = new mysql_recordset($sql);
	while ($rs->fetch_array())
	{
		extract($rs->myarray);
		$distance = distance(array($latitude,$longitude),array($latitude2,$longitude2));
		if ($distance > 50)
		{
			echo "$quote_id: $distance miles<Br/>\n";
			$count++;
		}
	
	}
	
	echo "$count '> 100 mile' intrastate moves.";	
?>
