<? 
	session_start();	
	include_once $_SERVER['DOCUMENT_ROOT']."/project/security.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_top.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/inc_mysql.php";
	include_once $_SERVER['DOCUMENT_ROOT']."/project/project.lib.php";
	
	if (countProjects() == 0)
	{
	?> 
		<table class="filterfont">
			<tr>
				<td>
					There are currnently no active projects. <br/>
					click <a href='create/project.php'>here</a> to create one.
				</td>
			</tr>
		</table>
	<?
		include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";
		exit();
	}
	
	
	$sql = "select p.*, DATEDIFF(finishby,now()) 'due_in', u.id as 'cid', u.displayname as 'creator_name' from marble.projects as p join marble.users as u on p.userid = u.id where p.active ";
	
	
	
	getProjects($sql); 
	include_once $_SERVER['DOCUMENT_ROOT']."/project/skin_bottom.php";
?>